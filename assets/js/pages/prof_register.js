function loginWithFb(form_id) {
    FB.login(function (respons) {
        if (respons.status == 'connected') {
            FB.api('/me?fields=id,name,email', function (response) {
                var social_id = response.id;
                var email = response.email;
                var full_name = response.name.split(" ");

                var first_name = '';
                var last_name = '';
                if (full_name[0].length > 0) {
                    first_name = full_name[0];
                }

                if (full_name[1].length > 0) {
                    last_name = full_name[1];
                }

                jQuery.ajax({
                    type: 'POST',
                    url: base_url + "/login",
                    data: {"social_id": social_id, "user_type": 2},
                    dataType: 'json',
                    success: function (ajax_response) {
                        if (ajax_response.success) {
                            window.location = jQuery(form_id + " #redirect_to_url").val();
                        } else {
                            if (ajax_response.no_user) {
                                jQuery("#signin-popup").modal("hide");
                                jQuery("#signup_form #su_firstname").val(first_name);
                                jQuery("#signup_form #su_lastname").val(last_name);
                                jQuery("#signup_form #su_email").val(email);
                                jQuery("#signup_form #su_cemail").val(email);
                                jQuery("#signup_form #su_password").val("");
                                jQuery("#signup_form #su_user_type").val("2");
                                jQuery("#signup_form #su_social_id").val(social_id);
                                jQuery("#registration-popup").modal();
                                setTimeout(function () {
                                    jQuery("body").addClass("modal-open");
                                }, 2000);
                            } else {
                                toastr.error(ajax_response.message, {timeOut: 5000});
                            }
                        }
                    },
                    error: function () {
                        toastr.error(internal_error_message, {timeOut: 5000});
                    }
                });

            });
        }
    }, {scope: 'email'});
}

// Setup an event listener to make an API call once auth is complete
function onLinkedInLoad() {
    IN.Event.on(IN, "auth", getProfileData);
}

// Handle the successful return from the API call
function onSuccess(data) {
    
    var social_id = '';
    var email = '';
    var first_name = '';
    var last_name = '';

    if (data.id != undefined) {
        social_id = data.id;
    }
    if (data.emailAddress != undefined) {
        email = data.emailAddress;
    }
    if (data.firstName != undefined) {
        first_name = data.firstName;
    }
    if (data.lastName != undefined) {
        last_name = data.lastName;
    }

    jQuery.ajax({
        type: 'POST',
        url: base_url + "/login",
        data: {"social_id": social_id, "user_type": 3},
        dataType: 'json',
        success: function (ajax_response) {
            if (ajax_response.success) {
                window.location = jQuery("#redirect_to_url").val();
            } else {
                if (ajax_response.no_user) {
                    jQuery("#signin-popup").modal("hide");
                    jQuery("#signup_form #su_firstname").val(first_name);
                    jQuery("#signup_form #su_lastname").val(last_name);
                    jQuery("#signup_form #su_email").val(email);
                    jQuery("#signup_form #su_cemail").val(email);
                    jQuery("#signup_form #su_password").val("");
                    jQuery("#signup_form #su_user_type").val("3");
                    jQuery("#signup_form #su_social_id").val(social_id);
                    jQuery("#registration-popup").modal();
                    setTimeout(function () {
                        jQuery("body").addClass("modal-open");
                    }, 2000);
                } else {
                    toastr.error(ajax_response.message, {timeOut: 5000});
                }
            }
        },
        error: function () {
            toastr.error(internal_error_message, {timeOut: 5000});
        }
    });
}

// Handle an error response from the API call
function onError(error) {
    console.log(error);
}

// Use the API call wrapper to request the member's basic profile data
function getProfileData() {
    IN.API.Raw("/people/~").result(onSuccess).error(onError);
}

jQuery(document).ready(function () {

    // Open login popup model and redirect to respected page
    jQuery(document).on('click', '.login-popup-model', function () {
        var get_redirect_url = jQuery(this).attr('redirect_to_url');
        jQuery("#signin-popup").modal('show');
        jQuery("#signin_form #redirect_to_url").val(get_redirect_url);
        jQuery("#signup_form #redirect_to_url").val(get_redirect_url);
    });

    // Open register popup model and redirect to respected page
    jQuery(document).on('click', '.registration-popup-model', function () {
        var get_redirect_url = jQuery(this).attr('redirect_to_url');
        jQuery("#registration-popup").modal('show');
        jQuery("#signup_form #redirect_to_url").val(get_redirect_url);
        jQuery("#signin_form #redirect_to_url").val(get_redirect_url);
    });


    jQuery(window).scroll(function () {
        var top = jQuery(window).scrollTop();
        if (top > 50) {
            jQuery("#top_arrow_img").fadeIn("slow");
        } else {
            jQuery("#top_arrow_img").fadeOut("slow");
        }
    });

    jQuery("#go_top").click(function () {
        jQuery('html, body').animate({
            scrollTop: 0
        }, 900);
    });

    jQuery("#mouse_icon").click(function () {
        var element_position = jQuery("#next-page").offset().top;
        var header_position = jQuery("header nav").height();

        var scroll = element_position - header_position;

        jQuery('html, body').animate({
            scrollTop: scroll
        }, 900);
    });

    jQuery('input[type=radio][name=user_account_type]').change(function () {
        if (this.value == '1') {
            jQuery(".social_div_content").fadeIn();
        } else if (this.value == '2') {
            jQuery(".social_div_content").fadeOut();
        }
    });

    jQuery('#signin_form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
        },
        messages: {
        },
        invalidHandler: function (event, validator) { //display error alert on form submit   
            $('.alert-danger', $('.login-form')).show();
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                {
                    controls.append(error);
                } else {
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
            } else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            } else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            } else if (element.hasClass('dual_multiselect')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            } else {
                if (element.parent().hasClass('input-group'))
                {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }

        }
    });

    jQuery("#signin_form").submit(function () {
        if (jQuery('#signin_form').valid()) {
            var email = jQuery("#signin_form #si_email").val();
            var password = jQuery("#signin_form #si_password").val();
            var password = jQuery("#signin_form #si_password").val();
            var user_account_type = jQuery('input[name=user_account_type]:checked', '#signin_form').val();
            password = jQuery.sha1(password);
            jQuery.ajax({
                type: 'POST',
                url: base_url + "/login",
                data: {"email": email, "password": password, "user_type": 1, "user_account_type": user_account_type},
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        window.location = jQuery("#signin_form #redirect_to_url").val();
                    } else {
                        toastr.error(response.message, {timeOut: 5000});
                    }
                },
                error: function () {
                    toastr.error(internal_error_message, {timeOut: 5000});
                }
            });
        }
    });

    $.validator.addMethod("lettersonly", function (value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    });

    $.validator.addMethod("pwcheck", function (value) {
        return /^[A-Za-z0-9\d=!@#$%^&*()_\-]*$/.test(value) // consists of only these
                && /[a-z]/.test(value) // has a lowercase letter
                && /[A-Z]/.test(value) // has a upper case letter
                && /[=!@#$%^&*()_\-]/.test(value) // has a special char
                && /\d/.test(value) // has a digit
    });

    jQuery('#signup_form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            "su_firstname": {
                lettersonly: true,
                minlength: 2,
                maxlength: 25,
            },
            "su_lastname": {
                lettersonly: true,
                minlength: 2,
                maxlength: 25,
            },
            "su_email": {
                email: true
            },
            "su_cemail": {
                equalTo: "#su_email"
            },
            su_password: {
                minlength: 6
            },
        },
        messages: {
            su_cemail: {
                equalTo: confirmation_email,
            },
        },
        invalidHandler: function (event, validator) { //display error alert on form submit   
            $('.alert-danger', $('.login-form')).show();
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                {
                    controls.append(error);
                } else {
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
            } else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            } else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            } else if (element.hasClass('dual_multiselect')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            } else {
                if (element.parent().hasClass('input-group'))
                {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }

        }
    });

    jQuery("#signup_form").submit(function () {
        if (jQuery('#signup_form').valid()) {
            var first_name = jQuery("#signup_form #su_firstname").val();
            var last_name = jQuery("#signup_form #su_lastname").val();
            var email = jQuery("#signup_form #su_email").val();
            var password = jQuery("#signup_form #su_password").val();
            var user_type = jQuery("#signup_form #su_user_type").val();
            var social_id = jQuery("#signup_form #su_social_id").val();
            var invitation_code = jQuery("#signup_form #invitation_code").val();
            var redirect_to_url = jQuery("#signup_form #redirect_to_url").val();

            password = jQuery.sha1(password);

            var ajax_send_data = {
                "first_name": first_name,
                "last_name": last_name,
                "email": email,
                "password": password,
                "user_type": user_type,
                "social_id": social_id,
                "invitation_code": invitation_code,
            };

            jQuery.ajax({
                type: 'POST',
                url: base_url + "/register",
                data: ajax_send_data,
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        window.location = redirect_to_url;
                    } else {
                        toastr.error(response.message, {timeOut: 5000});
                    }
                },
                error: function () {
                    toastr.error(internal_error_message, {timeOut: 5000});
                }
            });
        }
    });

    jQuery("#btn_newslatter_email").click(function () {
        var email = jQuery("#newslatter_email").val();

        if (email.trim() != "") {
            if (validateEmail(email)) {
                jQuery.ajax({
                    type: 'POST',
                    url: base_url + "/register_newslatters",
                    data: {"email": email},
                    success: function (response) {
                        if (response.success) {
                            jQuery("#newslatter_email").val("");
                            toastr.success(response.message, {timeOut: 5000});
                        } else {
                            toastr.error(response.message, {timeOut: 5000});
                        }
                        $("#subjecterror_required").hide();
                        $("#subjecterror_email").hide();
                    },
                    error: function () {
                        toastr.error(internal_error_message, {timeOut: 5000});
                    }
                });

            } else {
                $("#subjecterror_required").hide();
                $("#subjecterror_email").show();
            }
        } else {
            $("#subjecterror_required").show();
            $("#subjecterror_email").hide();
        }
    });


    jQuery('#forgot_password_form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            "rf_email": {
                email: true
            }
        },
        messages: {
        },
        invalidHandler: function (event, validator) { //display error alert on form submit   
            $('.alert-danger', $('.login-form')).show();
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    jQuery("#forgot_password_form").submit(function () {
        if (jQuery('#forgot_password_form').valid()) {
            var email = jQuery("#rf_email").val();
            jQuery.ajax({
                type: 'POST',
                url: base_url + "/forgot_password",
                data: {
                    email: email
                },
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        jQuery("#forgot_password").modal('hide');
                        toastr.success(response.message, {timeOut: 5000});
                    } else {
                        toastr.error(response.message, {timeOut: 5000});
                    }
                },
                error: function () {
                    toastr.error(problem_in_action_message, {timeOut: 5000});
                }
            });
        }
    });

    jQuery("#reset_password").validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: false,
        rules: {
            password: {
                required: true,
                minlength: 6,
                pwcheck: true
            },
            cpassword: {
                required: true,
                equalTo: "#password",
            },
        },
        messages: {
        },
        highlight: function (element) {
            jQuery(element).parent().addClass('has-error');
        },
        unhighlight: function (element) {
            jQuery(element).parent().removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    jQuery("#search_company_form").validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: false,
        rules: {
            q: {
                required: true,
            }
        },
        messages: {
        },
        highlight: function (element) {
            jQuery(element).parent().addClass('has-error');
        },
        unhighlight: function (element) {
            jQuery(element).parent().removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            //
        }
    });

    jQuery(document).change("#activity_area", function () {
        jQuery('input:hidden[name="activity_area[]"]').val($("#activity_area").val());
    });
    jQuery(document).keyup("#autocomplete", function () {
        jQuery('input:hidden[name="q"]').val($("#autocomplete").val());
    });




    jQuery('#profession_registration_form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            first_name: {
                lettersonly: true,
                "minlength": 2,
                "maxlength": 25

            },
            last_name: {
                lettersonly: true,
                "minlength": 2,
                "maxlength": 25
            },
            email: {
                email: true,
                "maxlength": 255
            },
            cemail: {
                equalTo: "#pr_email",
                "maxlength": 255
            },
            password: {
                minlength: 6,
                maxlength: 25
            },
            cpassword: {
                equalTo: "#pr_password"
            },
            civility: {
                required: true
            },
            social_reason: {
                required: true,
                minlength: 2,
                maxlength: 150,
            },
            "activity_area[]": {
                required: true
            }
        },
        messages: {
            civility: {
                required: required_field_message
            },
            "activity_area[]": {
                required: required_field_message
            }
        },
        invalidHandler: function (event, validator) { //display error alert on form submit   
            $('.alert-danger', $('.login-form')).show();
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                {
                    controls.append(error);
                } else {
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
            } else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            } else if (element.is('#pr_civility')) {
                error.insertAfter(element.parent());
            } else if (element.hasClass('dual_multiselect')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            } else {
                if (element.parent().hasClass('input-group'))
                {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }

        }
    });

    jQuery("#profession_registration_form").submit(function () {
        if (jQuery('#profession_registration_form').valid()) {
            var first_name = jQuery("#profession_registration_form #pr_firstname").val();
            var last_name = jQuery("#profession_registration_form #pr_lastname").val();
            var email = jQuery("#profession_registration_form #pr_email").val();
            var password = jQuery("#profession_registration_form #pr_password").val();
            var social_reason = jQuery("#profession_registration_form #pr_social_reason").val();
            var activity_area = jQuery("#profession_registration_form #activity_area").val();
            var civility = jQuery("#profession_registration_form #pr_civility").val();
            password = jQuery.sha1(password);

            var ajax_send_data = {
                "first_name": first_name,
                "last_name": last_name,
                "email": email,
                "password": password,
                "social_reason": social_reason,
                "activity_area": activity_area,
                "civility": civility
            };

            jQuery.ajax({
                type: 'POST',
                url: base_url + "/professional_register",
                data: ajax_send_data,
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        toastr.success(response.message, {timeOut: 7000});
                        window.location = response.url;
                    } else {
                        toastr.error(response.message, {timeOut: 5000});
                    }
                },
                error: function () {
                    toastr.error(internal_error_message, {timeOut: 5000});
                }
            });

        }
    });


});


function validateEmail($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
}