jQuery(document).ready(function () {
    jQuery(document).change("#country", function () {
        jQuery('input:hidden[name="nationality"]').val(jQuery('#country option:selected', this).attr('country_id'));
    });

    jQuery('#edit_profile_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: false,
        ignore: "select:hidden",
        rules: {
            first_name: {
                required: true,
                lettersonly: true,
                minlength: 2,
                maxlength: 25,
            },
            last_name: {
                required: true,
                lettersonly: true,
                minlength: 2,
                maxlength: 25,
            },
            email: {
                required: true,
                email: true,
                maxlength: 255,
            },
            address: {
                required: true,
                minlength: 2,
                maxlength: 200,
            },
            city: {
                required: true,
                maxlength: 50,
            },
            postal_code: {
                required: true,
                number: true,
                minlength: 4,
                maxlength: 15,
            },
            country: {
                required: true,
            },
            civility: {
                required: true,
            },
            phone: {
                required: true,
                number: true,
                minlength: 8,
                maxlength: 15,
            },
            social_reason: {
                required: true,
                minlength: 2,
                maxlength: 150,
            },
            "activity_area[]": {
                required: true,
            }
        },
        messages: {
            civility: {
                required: required_field_message
            },
            "activity_area[]": {
                required: required_field_message
            },
            "country": {
                required: required_field_message
            }
        },
        highlight: function (element) {
            jQuery(element).parent().addClass('has-error');
            jQuery(element).parent().find('.dropdown-toggle').addClass('form-control');
        },
        unhighlight: function (element) {
            jQuery(element).parent().removeClass('has-error');
            jQuery(element).parent().find('.dropdown-toggle').removeClass('form-control');
        },
        errorPlacement: function (error, element) {
            if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
            jQuery('html,body').stop(true, false).animate({scrollTop: $("#edit_profile_form").offset().top}, 'slow');
        }
    });

    $("#change_password_form").validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: false,
        rules: {
            old_password: {
                required: true,
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 25
            },
            cpassword: {
                required: true,
                equalTo: "#new_password",
            },
        },
        messages: {
        },
        highlight: function (element) {
            jQuery(element).parent().addClass('has-error');
        },
        unhighlight: function (element) {
            jQuery(element).parent().removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
});