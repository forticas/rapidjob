jQuery(document).ready(function () {

    if (user_cv_type == '2') {
        jQuery(".user_cv_type").show();
        jQuery('.my_hidden_rapidjob_cv').val(rapidjob_cv_file_name);
        jQuery(".cv_file_name").text(rapidjob_cv_file_name);
        if (rapidjob_cv_file_name == '') {
            jQuery(".cv_file_name").text(choose_file);
        }
        jQuery(".edit_my_cv_btn").addClass('disabled');
        jQuery("#my_cv").attr('disabled', 'disabled');
    } else {
        jQuery(".user_cv_type").hide();
    }

    jQuery('#attach_my_cv').click(function () {
        jQuery(".show_error_msg").html('');
        if (jQuery("#attach_my_cv").is(":checked")) {
            if (
                    user_training_data == '0' ||
                    user_training_data == '' ||
                    user_skill_data == '' ||
                    user_skill_data == '0' ||
                    user_exp_data == '0' ||
                    user_exp_data == '' ||
                    user_activity_area == '' ||
                    isNaN(user_exp_data) ||
                    isNaN(user_skill_data) ||
                    isNaN(user_training_data)
                    ) {
                jQuery("#attach_my_cv").attr('checked', false);
                toastr.options = {
                    "preventDuplicates": true,
                    "preventOpenDuplicates": true,
                    "closeButton": true,
                    "progressBar": true,
                    "timeOut": 10000
                };
                toastr.error(provide_cv_details, notice_message, {timeOut: 10000});
            } else if (rapidjob_cv_file_name == '') {
                jQuery("#attach_my_cv").attr('checked', false);
                toastr.options = {
                    "preventDuplicates": true,
                    "preventOpenDuplicates": true,
                    "closeButton": true,
                    "progressBar": true,
                    "timeOut": 10000
                };
                toastr.error(generate_your_cv_message, notice_message, {timeOut: 10000});
            } else {
                jQuery(".user_cv_type").show();
                jQuery('.my_hidden_rapidjob_cv').val(rapidjob_cv_file_name);
                jQuery(".cv_file_name").text(rapidjob_cv_file_name);
                if (rapidjob_cv_file_name == '') {
                    jQuery(".cv_file_name").text(choose_file);
                }
                jQuery(".edit_my_cv_btn").addClass('disabled');
                jQuery("#my_cv").attr('disabled', 'disabled');
            }
        } else {
            jQuery(".user_cv_type").hide();
            jQuery('.my_hidden_rapidjob_cv').val(cv_file_name);
            jQuery(".cv_file_name").text(cv_file_name);
            if (cv_file_name == '') {
                jQuery(".cv_file_name").text(choose_file);
            }
            jQuery(".edit_my_cv_btn").removeClass('disabled');
            jQuery("#my_cv").removeAttr('disabled');
            jQuery("#my_cv").addClass('required');
        }
    });

    // change cv file
    jQuery('#my_cv').change(function () {
        if (jQuery('#my_cv').valid()) {
            jQuery("#attach_my_cv").attr('checked', false);
            jQuery(".cv_file_name").text(this.value);
            jQuery(".my_hidden_cv").val(this.value);
        }
    });

    // remove cv file
    jQuery('.remove-cv-file').click(function () {
        jQuery("#attach_my_cv").attr('checked', false);
        jQuery(".edit_my_cv_btn").removeClass('disabled');
        jQuery("#my_cv").removeAttr('disabled');
        jQuery(".cv_file_name").text(choose_file);
        jQuery(".my_hidden_cv").val('');
        jQuery("#my_cv").addClass('required');
        jQuery(".user_cv_type").hide();
    });

    // change cover latter file
    jQuery('#my_cover_latter').change(function () {
        if (jQuery('#my_cover_latter').valid()) {
            jQuery(".cover_file_name").text(this.value);
            jQuery(".my_hidden_cover_latter").val(this.value);
        }
    });
    // remove cover latter file
    jQuery('.remove_cover_file').click(function () {
        jQuery(".cover_file_name").text(choose_file);
        jQuery(".my_hidden_cover_latter").val('');
        jQuery("#my_cover_latter").addClass('required');
    });

    jQuery.validator.addMethod("uploadFile", function (val, element) {
        if (val !== '') {
            var size = element.files[0].size;
            if (size > size_limit) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }, invalid_file);

    jQuery('#send_application_now_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: false,
        ignore: "select:hidden",
        rules: {
            user_message: {
                required: true,
                minlength: 5,
                maxlength: 500,
            },
            my_cv: {
                //required: false,
                extension: "jpg|jpeg|png|pdf",
                uploadFile: true,
            },
            my_cover_latter: {
                extension: "jpg|jpeg|png",
                uploadFile: true,
            }
        },
        messages: {
            my_cv: {
                extension: invalid_file_extension,
            },
            my_cover_latter: {
                extension: invalid_cover_extension,
            },
        },
        invalidHandler: function (event, validator) { //display error alert on form submit   
            $('.alert-danger', $('.login-form')).show();
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is(':checkbox')) {
                error.insertAfter(element.parent());
            } else if (element.is('#my_cv')) {
                error.insertAfter($('#send_application_now_form').find('.my_cv_label'));
            } else if (element.is('#my_cover_latter')) {
                error.insertAfter($('#send_application_now_form').find('.my_cover_latter_label'));
            } else {
                if (element.parent().hasClass('input-group'))
                {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
            jQuery('html,body').stop(true, false).animate({scrollTop: $("#my_app_form").offset().top}, 'slow');

        }
    });


    jQuery("#send_application_now_form").submit(function (e) {
        jQuery(".show_error_msg").html('');
        if (jQuery('#send_application_now_form').valid(e)) {

            var flag = 0;
            var get_file_value = jQuery('#my_cv').val();
            var get_checkbox_value = $("#attach_my_cv").is(":checked");

            if (doc_id == '') {

                // add validation
                if (get_file_value == '' && get_checkbox_value == false) {
                    flag = 1;
                }
                if (flag == 1) {
                    jQuery(".show_error_msg").html('<div class="text-danger">' + required_field_title + ', ' + required_field_message + '</div>');
                    jQuery("#my_cv").rules("add", {
                        extension: "jpg|jpeg|png|pdf",
                        uploadFile: true,
                    });
                    jQuery('html,body').stop(true, false).animate({scrollTop: $("#my_app_form").offset().top}, 'slow');
                    return false;
                }
            } else {

                // edit validation
                var get_hidden_file_value = jQuery('.my_hidden_cv').val();
                if (get_hidden_file_value == '' && get_checkbox_value == false) {
                    flag = 1;
                }
                if (flag == 1) {
                    jQuery(".show_error_msg").html('<div class="text-danger">' + required_field_title + ', ' + required_field_message + '</div>');
                    jQuery("#my_cv").rules("add", {
                        extension: "jpg|jpeg|png|pdf",
                        uploadFile: true,
                    });

                    jQuery('html,body').stop(true, false).animate({scrollTop: $("#my_app_form").offset().top}, 'slow');
                    return false;
                }
            }
            jQuery('#loading').show();
        }
    });
});