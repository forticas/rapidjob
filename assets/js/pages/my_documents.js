$(document).ready(function () {
    
    // change cv file
    jQuery("#my_new_cv").change(function () {
        $(".my_cv_label span").text(choose_file);
        if (jQuery('#my_new_cv').valid()) {
            $(".my_cv_label span").text(this.value);
        }
    });

    // edit cv
    jQuery("#edit_my_cv").change(function () {
        $('.edit_my_cv_label').nextAll('.text-danger:first').remove();
        if (jQuery('#edit_my_cv').valid()) {
            jQuery("#edit_my_cv_form").submit();
        }
    });

    // remove cv file
    $('.remove-cv-file-btn').click(function () {
        bootbox.confirm({
            title: confirmation_title,
            message: remove_confirmation_cv_message,
            buttons: {
                'cancel': {
                    label: cancel_button,
                    className: 'btn-default pull-left'
                },
                'confirm': {
                    label: remove_button,
                    className: 'btn-danger pull-right'
                }
            },
            callback: function (result) {
                if (result) {
                    window.location = $(".remove-cv-file-btn").attr('data');
                }
            }
        });
    });

    // change cover letter file
    $('#my_new_cover_latter').change(function () {
        $(".my_cover_latter_label span").text(choose_file);
        if (jQuery('#my_new_cover_latter').valid()) {
            $(".my_cover_latter_label span").text(this.value);
        }
    });

    // edit cover letter
    jQuery("#edit_my_cover").change(function () {
        $('.edit_my_cover_label').nextAll('.text-danger:first').remove();
        if (jQuery('#edit_my_cover').valid()) {
            jQuery("#edit_my_cover_form").submit();
        }
    });


    // remove cover letter file
    $('.remove-cover-file-btn').click(function () {
        bootbox.confirm({
            title: confirmation_title,
            message: remove_confirmation_cover_message,
            buttons: {
                'cancel': {
                    label: cancel_button,
                    className: 'btn-default pull-left'
                },
                'confirm': {
                    label: remove_button,
                    className: 'btn-danger pull-right'
                }
            },
            callback: function (result) {
                if (result) {
                    window.location = $(".remove-cover-file-btn").attr('data');
                }
            }
        });
    });

    jQuery.validator.addMethod("uploadFile", function (val, element) {
        if (val !== '') {
            var size = element.files[0].size;
            if (size > size_limit) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }, invalid_file);
    
     // change cv form
    jQuery('#change_my_cv').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: false,
        ignore: "select:hidden",
        rules: {
            my_cv: {
                required: true,
                extension: "jpg|jpeg|png|pdf",
                uploadFile: true,
            }
        },
        messages: {
            my_cv: {
                extension: invalid_file_extension,
            }
        },
        invalidHandler: function (event, validator) { //display error alert on form submit   
            $('.alert-danger', $('.login-form')).show();
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is('#my_new_cv')) {
                error.insertAfter($('#change_my_cv').find('.my_cv_label'));
            } else {
                if (element.parent().hasClass('input-group'))
                {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }

        }
    });

    // change cover form
    jQuery('#change_my_cover_latter').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: false,
        ignore: "select:hidden",
        rules: {
            my_cover_latter: {
                required: true,
                extension: "jpg|jpeg|png|pdf",
                uploadFile: true,
            }
        },
        messages: {
            my_cover_latter: {
                extension: invalid_file_extension,
            }
        },
        invalidHandler: function (event, validator) { //display error alert on form submit   
            $('.alert-danger', $('.login-form')).show();
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is('#my_new_cover_latter')) {
                error.insertAfter($('#change_my_cover_latter').find('.my_cover_latter_label'));
            } else {
                if (element.parent().hasClass('input-group'))
                {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }

        }
    });
    
    // edit cv form
    jQuery('#edit_my_cv_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: false,
        ignore: "select:hidden",
        rules: {
            my_cv: {
                required: true,
                extension: "jpg|jpeg|png|pdf",
                uploadFile: true,
            }
        },
        messages: {
            my_cv: {
                extension: invalid_file_extension,
            }
        },
        invalidHandler: function (event, validator) { //display error alert on form submit   
            $('.alert-danger', $('.login-form')).show();
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is('#edit_my_cv')) {
                error.insertAfter($('#edit_my_cv_form').parents('.edit_my_cv_label'));
            } else {
                if (element.parent().hasClass('input-group'))
                {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }

        }
    });
    
    // edit cover form
    jQuery('#edit_my_cover_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: false,
        ignore: "select:hidden",
        rules: {
            my_cover_latter: {
                required: true,
                extension: "jpg|jpeg|png|pdf",
                uploadFile: true,
            }
        },
        messages: {
            my_cover_latter: {
                extension: invalid_file_extension,
            }
        },
        invalidHandler: function (event, validator) { //display error alert on form submit   
            $('.alert-danger', $('.login-form')).show();
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is('#edit_my_cover')) {
                error.insertAfter($('#edit_my_cover_form').parents('.edit_my_cover_label'));
            } else {
                if (element.parent().hasClass('input-group'))
                {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }

        }
    });
});