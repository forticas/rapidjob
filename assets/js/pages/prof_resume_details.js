jQuery(document).ready(function () {

    $('.star-section .full').css({'pointer-events': 'none'});
    $('.star-section .half').css({'pointer-events': 'none'});
    $('.star-section .star-rate').css('cursor', 'none');



    jQuery('#send_email_to_candidate_form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            experience_description: {
                required: true,
                minlength: 5
            }
        },
        messages: {
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    jQuery("#send_email_to_candidate_form").submit(function () {
        if (jQuery('#send_email_to_candidate_form').valid()) {
            var experience_description = jQuery("#send_email_to_candidate_form #experience_description").val();
            if (get_user_id != '' && !isNaN(get_user_id)) {
                jQuery.ajax({
                    type: 'POST',
                    url: professional_url + "/send_mail_to_candidate",
                    data: {"description": experience_description, "candidate_id": get_user_id},
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            jQuery("#experience-popup").modal('hide');
                            jQuery("#experience_description").val('');
                            
                            toastr.success(response.message + " " + get_user_name, {timeOut: 5000});
                            
                        } else {
                            toastr.error(response.message + " " + get_user_name, {timeOut: 5000});
                        }
                    },
                    error: function () {
                        toastr.error(internal_error_message, {timeOut: 5000});
                    }
                });
            } else {
                toastr.error(internal_error_message, {timeOut: 5000});
            }
        }
    });

    jQuery(document).on('click', ".add_to_favourite", function () {
        var get_user_id = jQuery(this).attr('candidate_id');
        var this_action = jQuery(this);
        if (get_user_id != '' && !isNaN(get_user_id)) {
            var ajax_send_data = {
                "user_id": get_user_id,
            };
            jQuery.ajax({
                type: 'POST',
                url: professional_url + "/add_to_favourite",
                data: ajax_send_data,
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        
                        toastr.success(response.message, {timeOut: 5000});
                        jQuery(".selected_image_" + get_user_id).prop('src', favourite_image);
                        this_action.find(".add_remove_favourite_text:first").text(remove_favourite_text);
                        this_action.toggleClass('add_to_favourite remove_from_favourite');
                    } else {
                        toastr.error(response.message, {timeOut: 5000});
                    }
                },
                error: function () {
                    toastr.error(internal_error_message, {timeOut: 5000});
                }
            });
        } else {
            toastr.error(problem_in_action_message, {timeOut: 5000});
        }
    });

    jQuery(document).on('click', ".remove_from_favourite", function () {
        var get_user_id = jQuery(this).attr('candidate_id');
        var this_action = jQuery(this);
        if (get_user_id != '' && !isNaN(get_user_id)) {
            var ajax_send_data = {
                "user_id": get_user_id,
            };
            jQuery.ajax({
                type: 'POST',
                url: professional_url + "/remove_from_favourite",
                data: ajax_send_data,
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        toastr.success(response.message, {timeOut: 10000});
                        jQuery(".selected_image_" + get_user_id).prop('src', unfavourite_image);
                        this_action.find(".add_remove_favourite_text:first").text(add_favourite_text);
                        this_action.toggleClass('remove_from_favourite add_to_favourite');
                    } else {
                        toastr.error(response.message, {timeOut: 5000});
                    }
                },
                error: function () {
                    toastr.error(internal_error_message, {timeOut: 5000});
                }
            });
        } else {
            toastr.error(problem_in_action_message, {timeOut: 5000});
        }
    }
    );
});