jQuery(document).ready(function () {

    jQuery(document).change("#job_type", function () {
        jQuery('input:hidden[name="job_type"]').val($("#job_type").val());
    });
    jQuery(document).change("#activity_area", function () {
        jQuery('input:hidden[name="activity_area[]"]').val($("#activity_area").val());
    });
    jQuery(document).change("#work_place", function () {
        jQuery('input:hidden[name="work_place[]"]').val($("#work_place").val());
    });


    jQuery(document).on('click', ".remove_from_favourite", function () {
        var get_user_id = jQuery(this).attr('candidate_id');
        if (get_user_id != '' && !isNaN(get_user_id)) {
            var ajax_send_data = {
                "user_id": get_user_id,
            };
            jQuery.ajax({
                type: 'POST',
                url: professional_url + "/remove_from_favourite",
                data: ajax_send_data,
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        toastr.success(response.message, {timeOut: 10000});
                        jQuery(".my_favourite_cv_" + get_user_id).remove();
                        var update_fav_count = parseInt(jQuery(".update_fav_count").text());
                        if (update_fav_count == 1) {
                            jQuery(".main_favourite_resume_div").html("<label class='search-bx-label_empty'>0 " + no_favourite_resume + "</label>");
                        } else {
                            jQuery(".update_count").text(parseInt(update_fav_count - 1));
                        }

                    } else {
                        toastr.error(response.message, {timeOut: 5000});
                    }
                },
                error: function () {
                    toastr.error(problem_in_action_message, {timeOut: 5000});
                }
            });
        } else {
            toastr.error(problem_in_action_message, {timeOut: 5000});
        }
    });
});