function checkEmail() {
    var email_flag = true;
    var email_value = $("#user_comment").val();
    if (email_value != '') {
        var addresses = email_value.replace(/\s/g, '').split(',');
        for (i = 0; i < addresses.length; i++) {
            if (addresses[i] === '') {
                return false;
            }
            email_flag = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(addresses[i]);
            if (!email_flag) {
                return false;
            }
        }
    }
    return email_flag;
}

jQuery(document).ready(function () {
    jQuery(".show_error_msg").html('');

    jQuery('#my_cv').change(function () {
        jQuery(".my_cv_span").text();
        if (jQuery('#my_cv').valid()) {
            jQuery(".show_error_msg").html('');
            jQuery(".my_cv_span").text(this.value);
            jQuery('.my_hidden_cv').val(this.value);
        }
    });

    jQuery('#my_cover_latter').change(function () {
        jQuery(".my_cover_latter_span").text(choose_file);
        if (jQuery('#my_cover_latter').valid()) {
            jQuery(".my_cover_latter_span").text(this.value);
        }
    });

    jQuery('#attach_my_cv').click(function () {
        jQuery(".show_error_msg").html('');
        if (jQuery("#attach_my_cv").is(":checked")) {

            if (
                    user_training_data == '0' ||
                    user_training_data == '' ||
                    user_skill_data == '' ||
                    user_skill_data == '0' ||
                    user_exp_data == '0' ||
                    user_exp_data == '' ||
                    user_activity_area == '' ||
                    isNaN(user_exp_data) ||
                    isNaN(user_skill_data) ||
                    isNaN(user_training_data)
                    ) {
                jQuery("#attach_my_cv").attr('checked', false);
                toastr.options = {
                    "preventDuplicates": true,
                    "preventOpenDuplicates": true,
                    "closeButton": true,
                    "progressBar": true,
                    "timeOut": 10000
                };
                toastr.error(provide_cv_details, notice_message, {timeOut: 10000});
            } else if (rapidjob_cv_file_name == '') {
                jQuery("#attach_my_cv").attr('checked', false);
                toastr.options = {
                    "preventDuplicates": true,
                    "preventOpenDuplicates": true,
                    "closeButton": true,
                    "progressBar": true,
                    "timeOut": 10000
                };
                toastr.error(generate_your_cv_message, notice_message, {timeOut: 10000});
            } else {
                jQuery('.my_hidden_rapidjob_cv').val(rapidjob_cv_file_name);
                jQuery(".my_cv_span").text(rapidjob_cv_file_name);
                if (rapidjob_cv_file_name == '') {
                    jQuery(".my_cv_span").text(choose_file);
                }
                jQuery(".my_cv_label").attr('disabled', 'disabled');
                jQuery("#my_cv").attr('disabled', 'disabled');
            }
        } else {
            jQuery('.my_hidden_cv').val(cv_file_name);
            jQuery(".my_cv_span").text(cv_file_name);
            if (cv_file_name == '') {
                jQuery(".my_cv_span").text(choose_file);
            }
            jQuery(".my_cv_label").removeAttr('disabled');
            jQuery("#my_cv").removeAttr('disabled');
        }
    });

    jQuery('#accept_term').click(function () {
        jQuery('#accept_term').valid();
    });

    jQuery.validator.addMethod("uploadFile", function (val, element) {
        if (val !== '') {
            var size = element.files[0].size;
            if (size > size_limit) {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }, invalid_file);

    jQuery.validator.addMethod('emailList', function (value, element) {
        if (this.optional(element))
            return true;
        var flag = true;
        var addresses = value.replace(/\s/g, '').split(',');
        for (i = 0; i < addresses.length; i++) {
            if (addresses[i] === '') {
                return false;
            }
            flag = /^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i.test(addresses[i]);
        }
        return flag;
    }, invalid_email_message);

    jQuery('#send_application_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: false,
        ignore: "select:hidden",
        rules: {
            user_message: {
                required: true,
                minlength: 5,
                maxlength: 500,
                
            },
            attach_my_cv: {
                //required: false,
                //required: true,
                //minlength: 1
            },
            my_cv: {
                //required: false,
                extension: "jpg|jpeg|png|pdf",
                uploadFile: true,
            },
            my_cover_latter: {
                extension: "jpg|jpeg|png",
                uploadFile: true,
            },
            user_comment: {
                required: false,
                emailList: true,
                maxlength: 1000,
            },
            accept_term: {
                required: true,
                minlength: 1
            }
        },
        messages: {
            my_cv: {
                extension: invalid_file_extension,
            },
            my_cover_latter: {
                extension: invalid_cover_extension,
            },
        },
        invalidHandler: function (event, validator) { //display error alert on form submit   
            $('.alert-danger', $('.login-form')).show();
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.is(':checkbox')) {
                error.insertAfter(element.parent());
            } else if (element.is('#my_cv')) {
                jQuery('html,body').stop(true, false).animate({scrollTop: $("#user_message").offset().top}, 'slow');
                error.insertAfter(element.parent());
            } else if (element.is('#my_cover_latter')) {
                jQuery('html,body').stop(true, false).animate({scrollTop: $("#user_message").offset().top}, 'slow');
                error.insertAfter(element.parent());
            } else if (element.is('.select2')) {
                error.insertAfter(element.siblings('[class*="select2-container"]:eq(0)'));
            } else if (element.is('.chosen-select')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            } else if (element.hasClass('dual_multiselect')) {
                error.insertAfter(element.siblings('[class*="chosen-container"]:eq(0)'));
            } else {
                if (element.parent().hasClass('input-group'))
                {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }

        }
    });

    jQuery("#send_application_form").submit(function (e) {
        jQuery(".show_error_msg").html('');
        if (jQuery('#send_application_form').valid(e)) {

            var flag = 0;
            var get_file_value = jQuery('#my_cv').val();
            var get_checkbox_value = $("#attach_my_cv").is(":checked");

            if (doc_id == '') {

                // add validation
                if (get_file_value == '' && get_checkbox_value == false) {
                    flag = 1;
                }
                if (flag == 1) {
                    jQuery(".show_error_msg").html('<div class="text-danger">' + required_field_title + ', ' + required_field_message + '</div>');
                    $("#my_cv").rules("add", {
                        extension: "jpg|jpeg|png|pdf",
                        uploadFile: true,
                    });
                    jQuery('html,body').stop(true, false).animate({scrollTop: $("#user_message").offset().top},'slow');
                    return false;
                }
            } else {

                // edit validation
                var get_hidden_file_value = jQuery('.my_hidden_cv').val();
                if (get_hidden_file_value == '' && get_checkbox_value == false) {
                    flag = 1;
                }
                if (flag == 1) {
                    jQuery(".show_error_msg").html('<div class="text-danger">' + required_field_title + ', ' + required_field_message + '</div>');
                    jQuery("#my_cv").rules("add", {
                        extension: "jpg|jpeg|png|pdf",
                        uploadFile: true,
                    });

                    jQuery('html,body').stop(true, false).animate({scrollTop: $("#user_message").offset().top}, 'slow');

                    return false;
                }
            }

            if (!checkEmail()) {
                jQuery(".invalid_emails").html('<div class="text-danger">' + invalid_email_message + '</div>');
                return false;
            }
            jQuery('#loading').show();
        }
    });
});