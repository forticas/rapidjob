jQuery(document).ready(function () {

    jQuery(document).change("#job_type", function () {
        jQuery('input:hidden[name="job_type"]').val($("#job_type").val());
    });
    jQuery(document).change("#activity_area", function () {
        jQuery('input:hidden[name="activity_area[]"]').val($("#activity_area").val());
    });
    jQuery(document).change("#work_place", function () {
        jQuery('input:hidden[name="work_place[]"]').val($("#work_place").val());
    });
    jQuery(document).on('click', ".add_to_favourite", function () {
        var get_user_id = jQuery(this).attr('candidate_id');
        var this_action = jQuery(this);
        if (get_user_id != '' && !isNaN(get_user_id)) {
            var ajax_send_data = {
                "user_id": get_user_id,
            };
            jQuery.ajax({
                type: 'POST',
                url: professional_url + "/add_to_favourite",
                data: ajax_send_data,
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        toastr.success(response.message, {timeOut: 5000});
                        jQuery(".selected_image_" + get_user_id).prop('src', favourite_image);
                        this_action.find(".add_remove_favourite_text:first").text(remove_favourite_text);
                        this_action.toggleClass('add_to_favourite remove_from_favourite');
                    } else {
                        toastr.error(response.message, {timeOut: 5000});
                    }
                },
                error: function () {
                    toastr.error(internal_error_message, {timeOut: 5000});
                }
            });
        } else {
            toastr.error(problem_in_action_message, {timeOut: 5000});
        }
    });

    jQuery(document).on('click', ".remove_from_favourite", function () {
        var get_user_id = jQuery(this).attr('candidate_id');
        var this_action = jQuery(this);
        if (get_user_id != '' && !isNaN(get_user_id)) {
            var ajax_send_data = {
                "user_id": get_user_id,
            };
            jQuery.ajax({
                type: 'POST',
                url: professional_url + "/remove_from_favourite",
                data: ajax_send_data,
                dataType: 'json',
                success: function (response) {
                    if (response.success) {
                        toastr.success(response.message, {timeOut: 10000});
                        jQuery(".selected_image_" + get_user_id).prop('src', unfavourite_image);
                        this_action.find(".add_remove_favourite_text:first").text(add_favourite_text);
                        this_action.toggleClass('remove_from_favourite add_to_favourite');
                    } else {
                        toastr.error(response.message, {timeOut: 5000});
                    }
                },
                error: function () {
                    toastr.error(internal_error_message, {timeOut: 5000});
                }
            });
        } else {
            toastr.error(problem_in_action_message, {timeOut: 5000});
        }
    });
});