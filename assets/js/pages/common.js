jQuery(".close_modal").click(function () {
    jQuery("#signup_form")[0].reset();
    jQuery(".signup_modal_box .has-error").removeClass('has-error');
    jQuery(".signup_modal_box .help-block").remove();
});

jQuery(".close_model_login").click(function () {
    jQuery("#signin_form")[0].reset();
    jQuery("#signin_form .has-error").removeClass('has-error');
    jQuery("#signin_form .help-block").remove();
});

jQuery(document).ajaxSuccess(function (event, xhr, settings) {
    if (settings.url != base_url + "/get_csrf_value") {
        jQuery.ajax({
            type: 'POST',
            async: false,
            url: base_url + "/get_csrf_value",
            success: function (data) {
                if (data.success) {
                    jQuery('input:hidden[name="' + get_csrf_token_name + '"]').val(data.value);
                } else {
                    window.location.reload();
                }
            },
            error: function () {
                window.location.reload();
            }
        });
    }
});
jQuery(document).ajaxError(function (event, xhr, settings) {
    if (settings.url != base_url + "/get_csrf_value") {
        jQuery.ajax({
            type: 'POST',
            async: false,
            url: base_url + "/get_csrf_value",
            success: function (data) {
                if (data.success) {
                    jQuery('input:hidden[name="' + get_csrf_token_name + '"]').val(data.value);
                } else {
                    window.location.reload();
                }
            },
            error: function () {
                window.location.reload();
            }
        });
    }
});
jQuery.ajaxSetup({
    beforeSend: function (xhr, data) {
        var token_val = jQuery('input:hidden[name="' + get_csrf_token_name + '"]').val();
        data.data += '&' + get_csrf_token_name + '=' + token_val;
        if (get_method != 'get_csrf_value') {
            $('#loading').show();
        }
    },
    complete: function ()
    {
        $('#loading').hide();
    }
});
jQuery(".change_language").change(function () {
    var language = jQuery(this).val();
    jQuery.ajax({
        type: 'POST',
        async: false,
        url: base_url + "/change_language",
        data: {"language": language},
        success: function (data) {
            if (data.success) {
                window.location.reload();
            } else {
                toastr.error(change_language_error_message, {timeOut: 5000});
            }
        },
        error: function () {
            toastr.error(internal_error_message, {timeOut: 5000});
        }
    });
});