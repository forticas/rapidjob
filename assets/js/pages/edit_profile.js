jQuery(document).ready(function () {
    jQuery('#edit_profile_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: false,
        ignore: "select:hidden",
        rules: {
            first_name: {
                required: true,
                minlength: 2,
                lettersonly: true
            },
            last_name: {
                required: true,
                minlength: 2,
                lettersonly: true
            },
            email: {
                required: true,
                email: true,
                maxlength:254
            },
            address: {
                required: true,
                minlength: 2,
            },
            city: {
                required: true,
            },
            civility: {
                required: true,
            },
            nationality: {
                required: true,
            },
            postal_code: {
                required: true,
                number: true,
                minlength: 4,
                maxlength: 15,
            },
            country: {
                required: true,
            }
        },
        messages: {
        },
        highlight: function (element) {
            jQuery(element).parent().addClass('has-error');
            jQuery(element).parent().find('.dropdown-toggle').addClass('form-control');
        },
        unhighlight: function (element) {
            jQuery(element).parent().removeClass('has-error');
            jQuery(element).parent().find('.dropdown-toggle').removeClass('form-control');
        },
        errorPlacement: function (error, element) {
            if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    $("#change_password_form").validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: false,
        rules: {
            old_password: {
                required: true,
                pwcheck: true
            },
            password: {
                required: true,
                minlength: 6,
                pwcheck: true
            },
            cpassword: {
                required: true,
                equalTo: "#password",
            },
        },
        messages: {
        },
        highlight: function (element) {
            jQuery(element).parent().addClass('has-error');
        },
        unhighlight: function (element) {
            jQuery(element).parent().removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
});