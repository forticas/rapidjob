jQuery(document).ready(function () {


    $('#job_type').change(function () {
        jQuery('#job_type').valid();
    });

    $('#activity_area').change(function () {
        jQuery('#activity_area').valid();
    });
    
    $('#recruiter_activity_area').change(function () {
        jQuery('#recruiter_activity_area').valid();
    });

    $('#work_place').change(function () {
        jQuery('#work_place').valid();
    });

    $('#education_level').change(function () {
        jQuery('#education_level').valid();
    });

    jQuery('#search_priority_resume_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        ignore: "select:hidden",
        focusInvalid: false,
        debug: false,
        rules: {
            "job_type": {
                required: true
            },
            "activity_area[]": {
                required: true
            },
            "work_place[]": {
                required: true
            },
            education_level: {
                required: true
            }
        },
        messages: {
        },
        highlight: function (element) {
            jQuery(element).parent().addClass('has-error');
            jQuery(element).parent().find('.dropdown-toggle').addClass('form-control');
        },
        unhighlight: function (element) {
            jQuery(element).parent().removeClass('has-error');
            jQuery(element).parent().find('.dropdown-toggle').removeClass('form-control');
        },
        errorPlacement: function (error, element) {
            if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
                jQuery('html,body').animate({
                    scrollTop: $("#search_priority_resume_form").offset().top},
                        'slow');
            } else {
                error.insertAfter(element);
                jQuery('html,body').animate({
                    scrollTop: $("#search_priority_resume_form").offset().top},
                        'slow');
            }
        }
    });
    jQuery('#show_resume_to_candidate_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        ignore: "select:hidden",
        focusInvalid: false,
        debug: false,
        rules: {
            "social_reason": {
                required: true,
                minlength: 2,
                maxlength: 150,
            },
            "activity_area[]": {
                required: true
            }
        },
        messages: {
            "activity_area[]": {
                required: required_field_message
            }
        },
        highlight: function (element) {
            jQuery(element).parent().addClass('has-error');
            jQuery(element).parent().find('.dropdown-toggle').addClass('form-control');
        },
        unhighlight: function (element) {
            jQuery(element).parent().removeClass('has-error');
            jQuery(element).parent().find('.dropdown-toggle').removeClass('form-control');
        },
        errorPlacement: function (error, element) {
            if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    jQuery("#search_priority_resume_form").submit(function () {
        if (jQuery('#search_priority_resume_form').valid()) {
            $('#loading').show();
        }
    });
    
    jQuery("#show_resume_to_candidate_form").submit(function () {
        if (jQuery('#show_resume_to_candidate_form').valid()) {
            $('#loading').show();
        }
    });
});