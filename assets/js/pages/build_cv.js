jQuery(document).ready(function () {

    jQuery(".other_workjob_title, .other_activity_title, .other_workplace_title, .other_education_title").hide();

    jQuery(document).change("#job_type", function () {
        jQuery(".other_workjob_title").hide();
        var get_other_workjob = $("#job_type").find('option:selected').val();
        jQuery("#job_type_title").removeClass('required');
        if (get_other_workjob == 'other') {
            jQuery(".other_workjob_title").show();
            jQuery("#job_type_title").addClass('required');
        }
    });
    
    jQuery(document).change("#education_level", function () {
        jQuery(".other_education_title").hide();
        var get_other_education = $("#education_level").find('option:selected').val();
        jQuery("#other_education_title").removeClass('required');
        if (get_other_education == 'other') {
            jQuery(".other_education_title").show();
            jQuery("#other_education_title").addClass('required');
        }
    });

    jQuery(document).change("#activity_area", function () {
        jQuery(".other_activity_title").hide();
        jQuery("#activity_area_title").removeClass('required');
        $("#activity_area option:selected").each(function () {
            if ($(this).val() == 'other') {
                jQuery(".other_activity_title").show();
                jQuery("#activity_area_title").addClass('required');
//                $('.bootstrap-select.open').removeClass('open');
            }
        });
    });
    
    jQuery(document).change("#work_place", function () {
        jQuery(".other_workplace_title").hide();
        jQuery("#other_workplace_title").removeClass('required');
        $("#work_place option:selected").each(function () {
            if ($(this).val() == 'other') {
                jQuery(".other_workplace_title").show();
                jQuery("#other_workplace_title").addClass('required');
//                $('.bootstrap-select.open').removeClass('open');
            }
        });
    });
    
    $('.star-section .full').css({'pointer-events': 'none'});
    $('.star-section .half').css({'pointer-events': 'none'});
    $('.star-section .star-rate').css('cursor', 'none');

    // change profile picture
    jQuery(".change_photo").click(function () {
        jQuery("#form_image").trigger('click');
    });
    jQuery("#form_image").change(function () {
        jQuery("#photo_form").submit();
        $('#loading').show();
    });

    $('#job_type').change(function () {
        jQuery('#job_type').valid();
    });

    $('#activity_area').change(function () {
        jQuery('#activity_area').valid();
    });

    $('#work_place').change(function () {
        jQuery('#work_place').valid();
    });

    $('#education_level').change(function () {
        jQuery('#education_level').valid();
    });

    var language_count = 111;
    jQuery("#add_language").click(function () {
        language_count++;
        var language_box = '<div class="row count_total_language removebutton_' + language_count + '">'
                + '<div class="col-lg-6 col-md-6 col-sm-12">'
                + '<div class="form-group">'
                + '<input type="text" placeholder="' + enter_language + '" id="" name="my_language[' + language_count + ']" class="form-control required user_languages" data-rule-minlength="2" data-rule-maxlength="30">'
                + '</div>'
                + '</div>'
                + '<div class="col-lg-6 col-md-6 col-sm-12">'
                + '<div class="star-ratting-bx">'
                + '<div class="star-rate" style="margin-right: 17px; float: left;">'
                + '<fieldset class="rating">'
                + '<input class="user_language_rating" type="radio" value="5" name="my_language_rating[' + language_count + ']" id="my_language_rating5' + language_count + '"><label title="Awesome - 5 stars" for="my_language_rating5' + language_count + '" class="full required"></label>'
                + '<input class="user_language_rating" type="radio" value="4.5" name="my_language_rating[' + language_count + ']" id="my_language_rating4' + language_count + 'half"><label title="Pretty good - 4.5 stars" for="my_language_rating4' + language_count + 'half" class="half"></label>'
                + '<input class="user_language_rating" type="radio" value="4" name="my_language_rating[' + language_count + ']" id="my_language_rating4' + language_count + '"><label title="Pretty good - 4 stars" for="my_language_rating4' + language_count + '" class="full"></label>'
                + '<input class="user_language_rating" type="radio" value="3.5" name="my_language_rating[' + language_count + ']" id="my_language_rating3' + language_count + 'half"><label title="Meh - 3.5 stars" for="my_language_rating3' + language_count + 'half" class="half"></label>'
                + '<input class="user_language_rating" type="radio" value="3" name="my_language_rating[' + language_count + ']" id="my_language_rating3' + language_count + '"><label title="Meh - 3 stars" for="my_language_rating3' + language_count + '" class="full"></label>'
                + '<input class="user_language_rating" type="radio" value="2.5" name="my_language_rating[' + language_count + ']" id="my_language_rating2' + language_count + 'half"><label title="Kinda bad - 2.5 stars" for="my_language_rating2' + language_count + 'half" class="half"></label>'
                + '<input class="user_language_rating" type="radio" value="2" name="my_language_rating[' + language_count + ']" id="my_language_rating2' + language_count + '"><label title="Kinda bad - 2 stars" for="my_language_rating2' + language_count + '" class="full"></label>'
                + '<input class="user_language_rating" type="radio" value="1.5" name="my_language_rating[' + language_count + ']" id="my_language_rating1' + language_count + 'half"><label title="Meh - 1.5 stars" for="my_language_rating1' + language_count + 'half" class="half"></label>'
                + '<input class="user_language_rating" type="radio" value="1" name="my_language_rating[' + language_count + ']" id="my_language_rating1' + language_count + '"><label title="Sucks big time - 1 star" for="my_language_rating1' + language_count + '" class="full"></label>'
                + '<input class="user_language_rating" type="radio" value="0.5" name="my_language_rating[' + language_count + ']" id="my_language_rating0' + language_count + 'half"><label title="Sucks big time - 0.5 stars" for="my_language_rating0' + language_count + 'half" class="half"></label>'
                + '</fieldset>'
                + '</div>'
                + '<div style="font-size: 22px;cursor: pointer;" class="removebutton" data-langugid="removebutton_' + language_count + '"><i class="fa fa-times" aria-hidden="true"></i></div>'
                + '</div>'
                + '</div>'
                + '</div>';
        jQuery("#add_language_dynamic").append(language_box);
        $('.user_languages').each(function () {
            $(this).rules("add", {
                required: true
            });
        });
        $('.user_language_rating').each(function () {
            $(this).rules("add", {
                required: true
            });
        });
    });

    var star_count = 222;
    jQuery("#add_user_skill").click(function () {
        star_count++;
        var skill_box = '<div class="row removebutton_' + star_count + '">'
                + '<div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">'
                + '<div class="form-group">'
                + '<input type="text" placeholder="' + enter_skill + '" id="" name="my_skill[' + star_count + ']" class="form-control required user_skills" data-rule-minlength="2" data-rule-maxlength="100">'
                + '</div>'
                + '</div>'
                + '<div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">'
                + '<div class="star-ratting-bx">'
                + '<div class="star-rate" style="margin-right: 17px; float: left;">'
                + '<fieldset class="rating">'
                + '<input class="skill_rating_input" type="radio" value="5" name="my_skill_rating[' + star_count + ']" id="user_add_skill_star5' + star_count + '"><label title="Awesome - 5 stars" for="user_add_skill_star5' + star_count + '" class="full"></label>'
                + '<input class="skill_rating_input" type="radio" value="4.5" name="my_skill_rating[' + star_count + ']" id="user_add_skill_star4' + star_count + 'half"><label title="Pretty good - 4.5 stars" for="user_add_skill_star4' + star_count + 'half" class="half"></label>'
                + '<input class="skill_rating_input" type="radio" value="4" name="my_skill_rating[' + star_count + ']" id="user_add_skill_star4' + star_count + '"><label title="Pretty good - 4 stars" for="user_add_skill_star4' + star_count + '" class="full"></label>'
                + '<input class="skill_rating_input" type="radio" value="3.5" name="my_skill_rating[' + star_count + ']" id="user_add_skill_star3' + star_count + 'half"><label title="Meh - 3.5 stars" for="user_add_skill_star3' + star_count + 'half" class="half"></label>'
                + '<input class="skill_rating_input" type="radio" value="3" name="my_skill_rating[' + star_count + ']" id="user_add_skill_star3' + star_count + '"><label title="Meh - 3 stars" for="user_add_skill_star3' + star_count + '" class="full"></label>'
                + '<input class="skill_rating_input" type="radio" value="2.5" name="my_skill_rating[' + star_count + ']" id="user_add_skill_star2' + star_count + 'half"><label title="Kinda bad - 2.5 stars" for="user_add_skill_star2' + star_count + 'half" class="half"></label>'
                + '<input class="skill_rating_input" type="radio" value="2" name="my_skill_rating[' + star_count + ']" id="user_add_skill_star2' + star_count + '"><label title="Kinda bad - 2 stars" for="user_add_skill_star2' + star_count + '" class="full"></label>'
                + '<input class="skill_rating_input" type="radio" value="1.5" name="my_skill_rating[' + star_count + ']" id="user_add_skill_star1' + star_count + 'half"><label title="Meh - 1.5 stars" for="user_add_skill_star1' + star_count + 'half" class="half"></label>'
                + '<input class="skill_rating_input" type="radio" value="1" name="my_skill_rating[' + star_count + ']" id="user_add_skill_star1' + star_count + '"><label title="Sucks big time - 1 star" for="user_add_skill_star1' + star_count + '" class="full"></label>'
                + '<input class="skill_rating_input" type="radio" value="0.5" name="my_skill_rating[' + star_count + ']" id="user_add_skill_star0' + star_count + 'half"><label title="Sucks big time - 0.5 stars" for="user_add_skill_star0' + star_count + 'half" class="half"></label>'
                + '</fieldset>'
                + '</div>'
                + '<div style="font-size: 22px;cursor: pointer;" class="removebutton" data-langugid="removebutton_' + star_count + '"><i class="fa fa-times" aria-hidden="true"></i></div>'
                + '</div>'
                + '</div>'
                + '</div>';
        jQuery("#add_skill_dynamic").append(skill_box);
        $('.user_skills').each(function () {
            $(this).rules("add", {
                required: true
            });
        });
        $('.skill_rating_input').each(function () {
            $(this).rules("add", {
                required: true
            });
        });
    });



    jQuery('#build_cv_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        ignore: "select:hidden",
        focusInvalid: false,
        debug: false,
        rules: {
            "job_type": {
                required: true
            },
            "activity_area[]": {
                required: true
            },
            "work_place[]": {
                required: true
            },
            education_level: {
                required: true
            }
        },
        messages: {
        },
        highlight: function (element) {
            jQuery(element).parent().addClass('has-error');
            jQuery(element).parent().find('.dropdown-toggle').addClass('form-control');
        },
        unhighlight: function (element) {
            jQuery(element).parent().removeClass('has-error');
            jQuery(element).parent().find('.dropdown-toggle').removeClass('form-control');
        },
        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                {
                    controls.append(error);
                } else {
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
            } else {
                if (element.parent().hasClass('input-group'))
                {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
            jQuery('html,body').stop(true, false).animate({scrollTop: $("#build_cv_main_form").offset().top}, 'slow');
        }
    });

    jQuery("#build_cv_form").submit(function () {
        if (jQuery('#build_cv_form').valid()) {
            jQuery('#loading').show();
        }
    });

    jQuery("body").on("click", ".removebutton", function () {
        var removebuttonclass = $(this).attr('data-langugid');
        jQuery("." + removebuttonclass).remove();
    });

});