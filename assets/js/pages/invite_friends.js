var total_selected_companies_email, total_selected_companies_post, total_selected_companies, get_payment_type;

function manage_slider() {
    total_selected_companies = $('#send_application input:checkbox:checked').map(function () {
        return this.value;
    }).get().length;

    console.log("total_selected_companies : " + total_selected_companies);
    get_payment_type = jQuery("input[type=radio][name=user_send_mail_type]:checked").val();

    if (get_payment_type == '1') {

        $("#ex1").slider('option', {
            value: total_selected_companies
        });

        $("#ex1").slider("refresh");

        if (total_selected_companies == send_by_email_points) {
            jQuery(".update_price_email").text(0);
        } else if (total_selected_companies > send_by_email_points) {
            total_selected_companies_email = parseInt(parseInt(total_selected_companies) - parseInt(send_by_email_points));
            jQuery(".update_price_email").text(parseInt(parseInt(total_selected_companies_email) * parseInt(send_by_email_rate)));
        } else {
            jQuery(".update_price_email").text(0);
        }

        if (total_selected_companies == 0) {
            jQuery(".email_value").text(company_data);
            jQuery(".update_price_email").text(parseInt(parseInt(total_selected_companies) * parseInt(send_by_email_rate)));
        } else {
            jQuery(".email_value").text(total_selected_companies);
        }

    } else if (get_payment_type == '2') {
        $("#ex2").slider('option', {
            value: total_selected_companies
        });

        $("#ex2").slider("refresh");
        if (total_selected_companies == send_by_post_points) {
            jQuery(".update_price_post").text(0);
        } else if (total_selected_companies > send_by_post_points) {
            total_selected_companies_post = parseInt(parseInt(total_selected_companies) - parseInt(send_by_post_points));
            jQuery(".update_price_post").text(parseInt(parseInt(total_selected_companies_post) * parseInt(send_by_post_rate)));
        } else {
            jQuery(".update_price_post").text(0);
        }

        if (total_selected_companies == 0) {
            jQuery(".post_value").text(company_data);
            jQuery(".update_price_post").text(parseInt(parseInt(total_selected_companies) * parseInt(send_by_post_rate)));
        } else {
            jQuery(".post_value").text(total_selected_companies);
        }
    }
}

jQuery(document).ready(function () {
    jQuery(".company_error_message").hide();

    // manage slider teriff on check / uncheck company from list

    $('#select_all_companies').on('change', function () {
        $('.select_company_global').prop('checked', this.checked);
        if (jQuery("#select_all_companies").is(':checked')) {
            $('.select_all_companies').text(common_deselect_all);
        } else {
            $('.select_all_companies').text(common_select_all);
        }
        manage_slider();
    });

    $('.select_company_global').on('change', function () {
        $('#select_all_companies').prop('checked', $('.select_company_global:checked').length === $('.select_company_global').length);
        if (jQuery("#select_all_companies").is(':checked')) {
            $('.select_all_companies').text(common_deselect_all);
        } else {
            $('.select_all_companies').text(common_select_all);
        }
        manage_slider();
    });

    // send application type options 1 : email, 2 : post
    jQuery('input[type=radio][name=user_send_mail_type]').change(function () {
        jQuery(".company_error_message").hide();
        jQuery("#send_application").hide();
        if (company_data != '' || company_data != '0') {
            jQuery("#send_application").show();
            manage_slider();
        } else {
            jQuery(".company_error_message").show().html(no_company_found);
        }
    });

    jQuery("#send_application").submit(function () {
        jQuery("#payment_options_popup").modal('hide');
        jQuery(".payment_tariff_error_message").html('');

        var get_send_application_list = jQuery('#send_application input[type=checkbox]:checked').length;
        var get_payment_type = jQuery("input[type=radio][name=user_send_mail_type]:checked").val();

        var final_payment_teriff_email = parseInt(jQuery('#ex1').attr('value'));
        var final_payment_teriff_post = parseInt(jQuery('#ex2').attr('value'));

        console.log("get_send_application_list : " + get_send_application_list);
        console.log("get_payment_type : " + get_payment_type);
        console.log("final_payment_teriff_email : " + final_payment_teriff_email);
        console.log("send_by_email_points : " + send_by_email_points);
        console.log("final_payment_teriff_post : " + final_payment_teriff_post);
        console.log("send_by_post_points : " + send_by_post_points);

        if (
                get_send_application_list == 0 && (
                        get_payment_type == 1 &&
                        (
                                final_payment_teriff_email == 0 ||
                                final_payment_teriff_email == ''
                                ) ||
                        (
                                get_payment_type == 2 &&
                                (
                                        final_payment_teriff_post == 0 ||
                                        final_payment_teriff_post == ''
                                        )
                                )
                        )
                ) {

            jQuery('html,body').stop(true, false).animate({scrollTop: $(".payment_tariff_error_message").offset().top - parseInt(500)}, 'slow');
            jQuery(".payment_tariff_error_message").show().html(company_select_required);
            return false;
        } else {
            jQuery(".application_send_by").val(get_payment_type);
            if (get_payment_type == '1') {
                jQuery(".payment_teriff").val(final_payment_teriff_email);
            } else if (get_payment_type == '2') {
                jQuery(".payment_teriff").val(final_payment_teriff_post);
            }

            if (
                    get_payment_type == 1 &&
                    (
                            final_payment_teriff_email > send_by_email_points ||
                            get_send_application_list > send_by_email_points
                            )
                    ) {
                jQuery("#payment_options_popup").modal('show');

                var selected_company_ids = jQuery("#send_application input[type=checkbox]:checked").map(function () {
                    return this.value;
                }).get().join(",");

                jQuery(".all_selected_ids").val(selected_company_ids);
                return false;
            } else if (
                    get_payment_type == 2 &&
                    (
                            final_payment_teriff_post > send_by_post_points ||
                            get_send_application_list > send_by_post_points
                            )
                    ) {
                jQuery("#payment_options_popup").modal('show');
                var selected_company_ids = jQuery("#send_application input[type=checkbox]:checked").map(function () {
                    return this.value;
                }).get().join(",");

                jQuery(".all_selected_ids").val(selected_company_ids);
                return false;
            } else {
                return true;
            }
        }
        return true;
    });



    jQuery.validator.addMethod("email", function (value, element) {
        if (value != '') {
            var reg_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return reg_email.test(value);
        } else {
            return true;
        }
    });

    jQuery('#invite_friend_form').validate({
        errorElement: 'div',
        errorClass: 'help-block',
        focusInvalid: false,
        rules: {
            invitation_mail_1: {
                email: true,
                maxlength: 255
            },
            invitation_mail_2: {
                email: true,
                maxlength: 255
            },
            invitation_mail_3: {
                email: true,
                maxlength: 255
            }
        },
        messages: {
        },
        invalidHandler: function (event, validator) { //display error alert on form submit   
            $('.alert-danger', $('.login-form')).show();
        },
        highlight: function (e) {
            $(e).closest('.form-group').removeClass('has-info').addClass('has-error');
        },
        success: function (e) {
            $(e).closest('.form-group').removeClass('has-error').addClass('has-info');
            $(e).remove();
        },
        errorPlacement: function (error, element) {
            if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    jQuery("#invite_friend_form").submit(function (e) {
        if (jQuery('#invite_friend_form').valid(e)) {
            $('#loading').show();
        }
    });

    //user activity form
    jQuery('#user_activity_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: true,
        rules: {
            "activity_area[]": {
                required: true
            },
            "work_place[]": {
                required: true
            }
        },
        messages: {
        },
        errorPlacement: function (error, element) {
            if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    jQuery(".other_activity_title").hide();

    jQuery(document).change("#activity_area", function () {
        jQuery(".other_activity_title").hide();
        jQuery("#activity_area_title").removeClass('required');
        jQuery("#activity_area option:selected").each(function () {
            if (jQuery(this).val() == 'other') {
                jQuery(".other_activity_title").show();
                jQuery("#activity_area_title").addClass('required');
            }
        });
    });

    jQuery("#user_activity_form").submit(function (e) {
        if (jQuery('#user_activity_form').valid(e)) {
            $('#loading').show();
        }
    });

//    $('.selectpicker').on(".bs-select-all", function () {
//        console.log("all selected");
//    });

//    $('#activity_area').on("change", function () {
//        console.log($("#activity_area option:selected").text());
//    });


//    jQuery('.bs-select-all').change(function () {
//        alert("ALL SELECTED");
//    });

//    $(document).on('click', ".bs-deselect-all", function () {
//        alert("ALL DESELECTED");
//    });
//
//    $('#activity_area').on('change', function () {
//        var selected = $(this).find("option:selected").val();
//        alert(selected);
//    });
//    
//    $("#activity_area").on("changed.bs.select", function(e, clickedIndex, newValue, oldValue) {
//    var selectedD = $(this).find('option').eq(clickedIndex).text();
//    console.log('selectedD: ' + selectedD + '  newValue: ' + newValue + ' oldValue: ' + oldValue);
//  });
//
//    $('#activity_area').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
//        console.log(e);
//    });
//    $('#activity_area').on('changed.bs.select', function (e, clickedIndex, newValue, oldValue) {
//        var selected = $(e.currentTarget).val();
//        console.log(selected);
//    });
});