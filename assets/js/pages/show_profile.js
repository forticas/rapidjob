jQuery(document).ready(function () {
    $(".close_modal").click(function () {
        $("#user_experience_form")[0].reset();
        var validator = $("#user_experience_form").validate();
        validator.resetForm();
    });

    $(".close_training_model").click(function () {
        $("#user_training_form")[0].reset();
        var validator = $("#user_training_form").validate();
        validator.resetForm();
    });

    $(".close_skill_model").click(function () {
        $("#user_skill_form")[0].reset();
        var validator = $("#user_skill_form").validate();
        validator.resetForm();
    });

    $(".close_hobbies_model").click(function () {
        $("#user_hobbies_form")[0].reset();
        var validator = $("#user_hobbies_form").validate();
        validator.resetForm();
    });

    jQuery(".other_company_title").hide();
    $('.star-section .full').css({'pointer-events': 'none'});
    $('.star-section .half').css({'pointer-events': 'none'});
    $('.star-section .star-rate').css('cursor', 'none');

    jQuery(document).on('click', ".generate_cv_url", function () {
        user_exp_data = jQuery("#experience-section").attr('total_experiences');
        user_skill_data = jQuery("#skills-section").attr('total_skills');
        user_training_data = jQuery("#cources-section").attr('total_trainings');
        if (
                user_training_data == '0' ||
                user_training_data == '' ||
                user_skill_data == '' ||
                user_skill_data == '0' ||
                user_exp_data == '0' ||
                user_exp_data == '' ||
                user_activity_area == '' ||
                isNaN(user_exp_data) ||
                isNaN(user_skill_data) ||
                isNaN(user_training_data)
                ) {
            toastr.options = {
                "preventDuplicates": true,
                "preventOpenDuplicates": true,
                "closeButton": true,
                "progressBar": true,
                "timeOut": 10000
            };
            toastr.error(provide_cv_details, notice_message, {timeOut: 10000});
        } else {
            toastr.options = {
                "preventDuplicates": true,
                "preventOpenDuplicates": true,
                "closeButton": true,
                "progressBar": true,
                "timeOut": 5000
            };
            window.open(generate_cv_url, '_self');
        }
    });

    jQuery(document).on('click', ".open-download-cv-popup", function () {
        user_exp_data = jQuery("#experience-section").attr('total_experiences');
        user_skill_data = jQuery("#skills-section").attr('total_skills');
        if (user_skill_data == '0' || user_skill_data == '' || isNaN(user_skill_data) || user_exp_data == '0' || user_exp_data == '' || isNaN(user_exp_data)) {
            jQuery("#download-cv-popup").modal('hide');
            toastr.options = {
                "preventDuplicates": true,
                "preventOpenDuplicates": true,
                "closeButton": true,
                "progressBar": true,
                "timeOut": 10000
            };
            toastr.error(provide_cv_details, notice_message, {timeOut: 10000});
        } else {
            toastr.options = {
                "preventDuplicates": true,
                "preventOpenDuplicates": true,
                "closeButton": true,
                "progressBar": true,
                "timeOut": 5000
            };
            jQuery("#download-cv-popup").modal('show');
        }
    });

    jQuery(document).change("#job_type", function () {
        jQuery(".other_company_title").hide();
        var get_other = $("#job_type").find('option:selected').val();
        jQuery("#company_title").removeClass('required');
        if (get_other == 'other') {
            jQuery(".other_company_title").show();
            jQuery("#company_title").addClass('required');
        }
    });

    $('#download_as_jpeg').click(function () {
        jQuery('#loading').show();
        jQuery(".movable_div").css('display', 'block');
        jQuery("#canvas").css('height', jQuery(".movable_div").height());
        //get the div content
        var div_content = document.querySelector("#canvas")

        //make it as html5 canvas
        html2canvas(div_content).then(function (canvas) {
            //change the canvas to jpeg image
            var data = canvas.toDataURL('image/jpeg');
            //then call a super hero php to save the image
            jQuery.ajaxSetup({
                beforeSend: function (xhr, data) {
                    data.data += '&rapidjob_csrf_token=' + jQuery("#csrf_token_data").val() + '&download_file_type=jpeg&document_id=' + doc_id;
                    $('#loading').show();
                },
                complete: function ()
                {
                    $('#loading').hide();
                }
            });
            save_img(data, '.jpeg');
        });
    });

    $('#download_as_png').click(function () {
        jQuery('#loading').show();
        jQuery(".movable_div").css('display', 'block');
        jQuery("#canvas").css('height', jQuery(".movable_div").height());
        //get the div content
        var div_content = document.querySelector("#canvas")

        //make it as html5 canvas
        html2canvas(div_content).then(function (canvas) {
            //change the canvas to jpeg image
            var data = canvas.toDataURL('image/png');
            //then call a super hero php to save the image
            jQuery.ajaxSetup({
                beforeSend: function (xhr, data) {
                    data.data += '&rapidjob_csrf_token=' + jQuery("#csrf_token_data").val() + '&download_file_type=png&document_id=' + doc_id;
                    $('#loading').show();
                },
                complete: function ()
                {
                    $('#loading').hide();
                }
            });
            save_img(data, '.png');
        });
    });

    //to save the canvas image
    function save_img(data, download_file_ext) {
        jQuery(".movable_div").css('display', 'none');
        //ajax method.
        jQuery.post(users_url + '/generate_user_cv_image', {
            data: data,
        }, function (response) {
            //if the file saved properly, trigger a popup to the user.
            if (response.success) {
                jQuery('#loading').hide();

                jQuery("#download-cv-popup").modal('hide');
                jQuery('a#open_image').attr({href: response.file_name,
                    download: response.file_name});

                jQuery('#open_image')[0].click();
            } else {
                toastr.error(internal_error_message, {timeOut: 5000});
            }
        });
    }

    // on ckick icon scroll window
    $('body').scrollspy({target: ".navbar", offset: 100});
    $("#my_nav_bar a").on('click', function (event) {
        if (this.hash !== "") {
            event.preventDefault();
            var hash = this.hash;
            $('html, body').animate({scrollTop: $(hash).offset().top}, 800, function () {
                window.location.hash = hash;
            });
        }
    });

    var star_count = 111;
    jQuery("#add_user_skill").click(function () {
        star_count++;
        var skill_box = '<div class="row removebutton_' + star_count + '"">' +
                '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mob-input-width">' +
                '<div class="form-group">' +
                '<input type="text" placeholder="" id="" name="user_skill_establishment[' + star_count + ']" class="form-control required skill_establishment_input" data-rule-minlength="2" data-rule-maxlength="50">' +
                '</div>' +
                '</div>' +
                '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 mob-input-width skill_star_div set_skill_star_main_div">' +
                '<div class="star-rate extrap-top add_more_skill_div">' +
                '<fieldset class="rating set_skill_star_ratting_div">' +
                '<input class="skill_rating_input" type="radio" value="5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star5' + star_count + '"><label title="Awesome - 5 stars" for="user_add_skill_star5' + star_count + '" class="full"></label>' +
                '<input class="skill_rating_input" type="radio" value="4.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star4' + star_count + 'half"><label title="Pretty good - 4.5 stars" for="user_add_skill_star4' + star_count + 'half" class="half"></label>' +
                '<input class="skill_rating_input" type="radio" value="4" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star4' + star_count + '"><label title="Pretty good - 4 stars" for="user_add_skill_star4' + star_count + '" class="full"></label>' +
                '<input class="skill_rating_input" type="radio" value="3.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star3' + star_count + 'half"><label title="Meh - 3.5 stars" for="user_add_skill_star3' + star_count + 'half" class="half"></label>' +
                '<input class="skill_rating_input" type="radio" value="3" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star3' + star_count + '"><label title="Meh - 3 stars" for="user_add_skill_star3' + star_count + '" class="full"></label>' +
                '<input class="skill_rating_input" type="radio" value="2.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star2' + star_count + 'half"><label title="Kinda bad - 2.5 stars" for="user_add_skill_star2' + star_count + 'half" class="half"></label>' +
                '<input class="skill_rating_input" type="radio" value="2" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star2' + star_count + '"><label title="Kinda bad - 2 stars" for="user_add_skill_star2' + star_count + '" class="full"></label>' +
                '<input class="skill_rating_input" type="radio" value="1.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star1' + star_count + 'half"><label title="Meh - 1.5 stars" for="user_add_skill_star1' + star_count + 'half" class="half"></label>' +
                '<input class="skill_rating_input" type="radio" value="1" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star1' + star_count + '"><label title="Sucks big time - 1 star" for="user_add_skill_star1' + star_count + '" class="full"></label>' +
                '<input class="skill_rating_input" type="radio" value="0.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star0' + star_count + 'half"><label title="Sucks big time - 0.5 stars" for="user_add_skill_star0' + star_count + 'half" class="half"></label>' +
                '</fieldset>' +
                '</div><div style="font-size: 22px;cursor: pointer;" class="removebutton" data-langugid="removebutton_' + star_count + '"><i class="fa fa-times" aria-hidden="true"></i></div>' +
                '</div>' +
                '</div>';
        jQuery("#add_skill_dynamic").append(skill_box);

        $('.skill_rating_input').each(function () {
            $(this).rules("add", {
                required: true
            });
        });
    });

    var hobbies_count = 1;
    jQuery("#add_more_hobbies").click(function () {
        hobbies_count++;
        var hobbies_box = '<div class="row">' +
                '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hm-model-mob-width">' +
                '<div class="form-group">' +
                '<label for="hobbies_title">' + hobbies_title + '</label>' +
                '<input type="text" placeholder="" id="" name="user_hobbies_title[' + hobbies_count + ']" class="form-control required user_hobbies_title">' +
                '</div>' +
                '</div>' +
                '<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mob-input-width">' +
                '<label for="hobbies_description" class="select-label">' + hobbies_desc + '</label>' +
                '<div class="form-group">' +
                '<input type="text" placeholder="" id="" name="user_hobbies_description[' + hobbies_count + ']" class="form-control required user_hobbies_description">' +
                '</div>' +
                '</div>' +
                '</div>';
        jQuery("#add_hobbies_dynamic").append(hobbies_box);
    });

    // user bio form
    jQuery('#user_bio_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: true,
        rules: {
            ud_user_bio: {
                required: true,
                minlength: 20,
                maxlength: 450,
            }
        },
        messages: {
        },
        errorPlacement: function (error, element) {
            if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    //user experience form
    jQuery('#user_experience_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: true,
        rules: {
            experience_start_month: {
                required: true
            },
            experience_start_year: {
                required: true
            },
            experience_end_month: {
                required: true
            },
            experience_end_year: {
                required: true
            },
        },
        messages: {
            experience_start_month: {
                required: select_start_month
            },
            experience_start_year: {
                required: select_start_year
            },
            experience_end_month: {
                required: select_end_month
            },
            experience_end_year: {
                required: select_end_year
            },
        },
        errorPlacement: function (error, element) {
            if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    //user training form
    jQuery('#user_training_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: true,
        rules: {
            training_start_month: {
                required: true
            },
            training_start_year: {
                required: true
            },
            training_end_month: {
                required: true
            },
            training_end_year: {
                required: true
            },
        },
        messages: {
            training_start_month: {
                required: select_start_month
            },
            training_start_year: {
                required: select_start_year
            },
            training_end_month: {
                required: select_end_month
            },
            training_end_year: {
                required: select_end_year
            },
        },
        errorPlacement: function (error, element) {
            if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    //user skill form
    jQuery('#user_skill_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: false,
        ignore: "select:hidden",
        debug: false,
        rules: {
            "user_skill_establishment[]": {
                required: true
            },
            "user_skill_rating[]": {
                required: true
            }

        },
        messages: {
        },
        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                {
                    controls.append(error);
                } else {
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
            } else {
                if (element.parent().hasClass('input-group'))
                {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }

        }
    });

    //user hobbies form
    jQuery('#user_hobbies_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: true,
        //ignore: "select:hidden",
        rules: {
            "user_hobbies_title[]": {
                required: true
            },
            "user_hobbies_description[]": {
                required: false
            }

        },
        messages: {
        },
        errorPlacement: function (error, element) {
            if (element.is(':checkbox') || element.is(':radio')) {
                var controls = element.closest('div[class*="col-"]');
                if (controls.find(':checkbox,:radio').length > 1)
                {
                    controls.append(error);
                } else {
                    error.insertAfter(element.nextAll('.lbl:eq(0)').eq(0));
                }
            } else {
                if (element.parent().hasClass('input-group'))
                {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }

        }
    });

    //user activity form
    jQuery('#user_activity_form').validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: true,
        rules: {
            "activity_area[]": {
                required: true
            }
        },
        messages: {
        },
        errorPlacement: function (error, element) {
            if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });

    jQuery("#user_activity_form").submit(function (e) {
        if (jQuery('#user_activity_form').valid(e)) {
            $('#loading').show();
        }
    });

    $("#experience-popup #experience_start_year").attr('disabled', 'disabled');
    $("#experience-popup #experience_end_year").attr('disabled', 'disabled');

    $("#training-popup #training_start_year").attr('disabled', 'disabled');
    $("#training-popup #training_end_year").attr('disabled', 'disabled');

    // current position check box
    $("#in_current_position").click(function () {
        if ($("#in_current_position").is(':checked')) {
            $("#experience-popup #experience_end_month").attr('disabled', 'disabled');
            $("#experience-popup #experience_end_year").attr('disabled', 'disabled');
        } else {
            $("#experience-popup #experience_end_month").removeAttr('disabled');
            $("#experience-popup #experience_end_year").removeAttr('disabled');
        }
        $("#experience_end_month").selectpicker("refresh");
        $("#experience_end_year").selectpicker("refresh");
    });

    var set_end_date = new Date();
    // START #training-popup form
    var training_end_year = '';
    $('#training-popup .training_start_month').change(function () {
        $("#training-popup #training_start_year").removeAttr('disabled');
        $(".tr_start_date").remove();
        $('#training_start_month').valid();
    });

    $('#training-popup .training_start_year').datepicker({
        format: "yyyy",
        startView: 2,
        minViewMode: 2,
        maxViewMode: 2,
        language: current_lang,
        endDate: set_end_date,
        autoclose: true
    }).on('changeDate', function (selected) {
        //set end year
        training_end_year = new Date(selected.date.valueOf());
        training_end_year.setDate(training_end_year.getDate(new Date(selected.date.valueOf())));
        $('#training-popup .training_end_year').datepicker('setStartDate', training_end_year);
        $(".tr_start_date").remove();
        $('#training_start_year').valid();
    });

    $('#training-popup .training_end_month').change(function () {
        $("#training-popup #training_end_year").removeAttr('disabled');
        $(".tr_start_date").remove();
        $('#training_start_month').valid();
    });

    $('#training-popup .training_end_year').datepicker({
        format: "yyyy",
        startView: 2,
        minViewMode: 2,
        maxViewMode: 2,
        language: current_lang,
        endDate: set_end_date,
        autoclose: true
    }).on('changeDate', function () {
        $(".tr_start_date").remove();
        $('#training_end_year').valid();
    });


    jQuery("#user_training_form").submit(function (e) {
        if (jQuery('#user_training_form').valid(e)) {
            // validate training date
            // get current date value object
            var current_date = new Date(),
                    current_month = current_date.getMonth(),
                    current_year = current_date.getFullYear();

            current_date = current_year + '-' + parseInt((current_month + 1)) + '-' + '01';
            var current_date_value = current_date.split('-');
            var first_current_date = new Date();
            first_current_date.setFullYear(current_date_value[0], (current_date_value[1] - 1), current_date_value[2]);

            var get_tr_start_date = $("#training_start_year").val() + '-' + $("#training_start_month").val() + '-' + '01';
            var get_tr_end_date = $("#training_end_year").val() + '-' + $("#training_end_month").val() + '-' + '01';

            var current_tr_start_date = get_tr_start_date.split('-');
            var current_tr_end_date = get_tr_end_date.split('-');

            var second_tr_date = new Date();
            second_tr_date.setFullYear(current_tr_start_date[0], (current_tr_start_date[1] - 1), current_tr_start_date[2]);

            var third_tr_date = new Date();
            third_tr_date.setFullYear(current_tr_end_date[0], (current_tr_end_date[1] - 1), current_tr_end_date[2]);

            // compair training start month and year with current month and year 
            if (first_current_date < second_tr_date) {
                $(".tr_start_date").remove('div');
                $('.training_start_month select').after('<div class="text-danger tr_start_date">' + select_valid_start_date + '</div>');
                e.preventDefault();
            } else if (first_current_date < third_tr_date || third_tr_date < second_tr_date) {
                $(".tr_start_date").remove('div');
                $('.training_end_month select').after('<div class="text-danger tr_start_date">' + select_valid_end_date + '</div>');
                e.preventDefault();
            }
        }
    });
    // END #training-popup form

    // START #experience-popup form
    var experience_end_year = '';
    $('#experience-popup .experience_start_month').change(function () {
        $("#experience-popup #experience_start_year").removeAttr('disabled');
        $(".ex_start_date").remove();
        $('#experience_start_month').valid();
    });

    $('#experience-popup .experience_start_year').datepicker({
        format: "yyyy",
        startView: 2,
        minViewMode: 2,
        maxViewMode: 2,
        language: current_lang,
        endDate: set_end_date,
        autoclose: true
    }).on('changeDate', function (selected) {
        $(".ex_start_date").remove();
        //set end year
        experience_end_year = new Date(selected.date.valueOf());
        experience_end_year.setDate(experience_end_year.getDate(new Date(selected.date.valueOf())));
        $('#experience-popup .experience_end_year').datepicker('setStartDate', experience_end_year);
        $('#experience_start_year').valid();
    });

    $('#experience-popup .experience_end_month').change(function () {
        $("#experience-popup #experience_end_year").removeAttr('disabled');
        $(".ex_start_date").remove();
        $('#experience_end_month').valid();
    });

    $('#experience-popup .experience_end_year').datepicker({
        format: "yyyy",
        startView: 2,
        minViewMode: 2,
        maxViewMode: 2,
        language: current_lang,
        endDate: set_end_date,
        autoclose: true
    }).on('changeDate', function () {
        $(".ex_start_date").remove();
        $('#experience_end_year').valid();
    });

    jQuery("#user_experience_form").submit(function (e) {
        $(".ex_start_date").remove('div');
        if (jQuery('#user_experience_form').valid(e)) {
            // validate experience date

            // get current date value object
            var current_date = new Date(),
                    current_month = current_date.getMonth(),
                    current_year = current_date.getFullYear();

            current_date = current_year + '-' + parseInt((current_month + 1)) + '-' + '01';
            var current_date_value = current_date.split('-');
            var first_current_date = new Date();
            first_current_date.setFullYear(current_date_value[0], (current_date_value[1] - 1), current_date_value[2]);

            var get_exp_start_date = $("#experience_start_year").val() + '-' + $("#experience_start_month").val() + '-' + '01';
            var get_exp_end_date = $("#experience_end_year").val() + '-' + $("#experience_end_month").val() + '-' + '01';

            var current_exp_start_date = get_exp_start_date.split('-');
            var current_exp_end_date = get_exp_end_date.split('-');

            var second_exp_date = new Date();
            second_exp_date.setFullYear(current_exp_start_date[0], (current_exp_start_date[1] - 1), current_exp_start_date[2]);

            var third_exp_date = new Date();
            third_exp_date.setFullYear(current_exp_end_date[0], (current_exp_end_date[1] - 1), current_exp_end_date[2]);


            var get_checkbox_value = $("#in_current_position").is(":checked");
            // compair experience start month and year with current month and year 
            if (first_current_date < second_exp_date) {
                $('.experience_start_month select').after('<div class="text-danger ex_start_date">' + select_valid_start_date + '</div>');
                e.preventDefault();
            }
            // add validation
            if (get_checkbox_value == false) {
                if (first_current_date < third_exp_date || third_exp_date < second_exp_date) {
                    $('.experience_end_month select').after('<div class="text-danger ex_start_date">' + select_valid_end_date + '</div>');
                    e.preventDefault();
                }
            }
        }
    });
    // END #experience-popup form

    // change profile picture
    jQuery(".change_photo").click(function () {
        jQuery("#form_image").trigger('click');
    });

    jQuery("#form_image").change(function () {
        jQuery("#photo_form").submit();
        $('#loading').show();
    });

    // delete experience
    jQuery(document).on('click', '.delete_experience', function () {
        var id = jQuery(this).attr('data-id');
        user_exp_data = jQuery("#experience-section").attr('total_experiences');

        swal({
            title: delete_experience_text,
            text: delete_message_text,
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: delete_yes_text,
            cancelButtonText: delete_no_text,
            closeOnConfirm: false,
        }, function () {
            if (!isNaN(id)) {
                jQuery.ajax({
                    "url": users_url + '/delete_experience',
                    type: "POST",
                    data: {
                        'id': id
                    },
                    dataType: 'json',
                    cache: false,
                    success: function (response) {
                        if (response.success) {
                            var remaining_exp = parseInt(parseInt(user_exp_data) - 1);
                            jQuery("#experience-section").attr('total_experiences', remaining_exp);
                            if (remaining_exp == 0) {
                                var exp_html = '<h3 class="no_exp_set">' + add_your_experience + '</h3>';
                                jQuery(".experience_row_" + id).html(exp_html);
                                jQuery(".open-download-cv-popup").hide();
                            } else {
                                jQuery(".experience_row_" + id).remove();
                            }

                            swal(response.message);
                        } else {
                            swal(response.message);
                        }
                    },
                    error: function () {
                        setTimeout(function () {
                            swal(internal_error_message);
                        }, 2000);
                    }
                });
            } else {
                setTimeout(function () {
                    swal(problem_in_action_message);
                }, 2000);
            }

        });
    });

    // delete training
    jQuery(document).on('click', '.delete_training', function () {
        var id = jQuery(this).attr('data-id');
        user_training_data = jQuery("#cources-section").attr('total_trainings');

        swal({
            title: delete_training_text,
            text: delete_message_text,
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: delete_yes_text,
            cancelButtonText: delete_no_text,
            closeOnConfirm: false,
        }, function () {
            if (!isNaN(id)) {
                jQuery.ajax({
                    "url": users_url + '/delete_training',
                    type: "POST",
                    data: {
                        'id': id
                    },
                    dataType: 'json',
                    cache: false,
                    success: function (response) {
                        if (response.success) {
                            var remaining_training = parseInt(parseInt(user_training_data) - 1);
                            jQuery("#cources-section").attr('total_trainings', remaining_training);
                            if (remaining_training == 0) {
                                var trn_html = '<h3 class="no_training_set">' + add_your_training + '</h3>';
                                jQuery(".training_row_" + id).html(trn_html);
                            } else {
                                jQuery(".training_row_" + id).remove();
                            }
                            swal(response.message);
                        } else {
                            swal(response.message);
                        }
                    },
                    error: function () {
                        setTimeout(function () {
                            swal(internal_error_message);
                        }, 2000);
                    }
                });
            } else {
                setTimeout(function () {
                    swal(problem_in_action_message);
                }, 2000);
            }

        });
    });

    // delete skill
    jQuery(document).on('click', '.delete_skill', function () {
        var id = jQuery(this).attr('data-id');
        user_skill_data = jQuery("#skills-section").attr('total_skills');

        swal({
            title: delete_skill_text,
            text: delete_message_text,
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: delete_yes_text,
            cancelButtonText: delete_no_text,
            closeOnConfirm: false,
        }, function () {
            if (!isNaN(id)) {
                jQuery.ajax({
                    "url": users_url + '/delete_skill',
                    type: "POST",
                    data: {
                        'id': id
                    },
                    dataType: 'json',
                    cache: false,
                    success: function (response) {
                        if (response.success) {
                            var remaining_skill = parseInt(parseInt(user_skill_data) - 1);
                            jQuery("#skills-section").attr('total_skills', remaining_skill);
                            if (remaining_skill == 0) {
                                var exp_html = '<h3 class="no_skill_set">' + add_your_skill + '</h3>';
                                jQuery(".skill_row_" + id).html(exp_html);
                                jQuery(".open-download-cv-popup").hide();
                            } else {
                                jQuery(".skill_row_" + id).remove();
                            }

                            swal(response.message);
                        } else {
                            swal(response.message);
                        }
                    },
                    error: function () {
                        setTimeout(function () {
                            swal(internal_error_message);
                        }, 2000);
                    }
                });
            } else {
                setTimeout(function () {
                    swal(problem_in_action_message);
                }, 2000);
            }

        });
    });

    // delete hobby
    jQuery(document).on('click', '.delete_hobby', function () {
        var id = jQuery(this).attr('data-id');
        user_hobby_data = jQuery("#hobbies-section").attr('total_hobbies');

        swal({
            title: delete_hobby_text,
            text: delete_message_text,
            type: "warning",
            showCancelButton: true,
            showLoaderOnConfirm: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: delete_yes_text,
            cancelButtonText: delete_no_text,
            closeOnConfirm: false,
        }, function () {
            if (!isNaN(id)) {
                jQuery.ajax({
                    "url": users_url + '/delete_hobby',
                    type: "POST",
                    data: {
                        'id': id
                    },
                    dataType: 'json',
                    cache: false,
                    success: function (response) {
                        if (response.success) {
                            var remaining_hobby = parseInt(parseInt(user_hobby_data) - 1);
                            jQuery("#hobbies-section").attr('total_hobbies', remaining_hobby);
                            if (remaining_hobby == 0) {
                                var hobby_html = '<h3 class="no_hobbies_set">' + add_your_hobby + '</h3>';
                                jQuery(".hobby_row_" + id).html(hobby_html);
                            } else {
                                jQuery(".hobby_row_" + id).remove();
                            }
                            swal(response.message);
                        } else {
                            swal(response.message);
                        }
                    },
                    error: function () {
                        setTimeout(function () {
                            swal(internal_error_message);
                        }, 2000);
                    }
                });
            } else {
                setTimeout(function () {
                    swal(problem_in_action_message);
                }, 2000);
            }

        });
    });

    jQuery(document).on("click", "#add_experience_popup", function () {
        $('#experience-popup').find('.model_heading_text').html(add_experience_heading);
        $('#user_experience_form').attr('action', add_exp_url);
        $(".selectpicker").selectpicker("refresh");
        $("#experience-popup").modal({backdrop: 'static', keyboard: false});
        jQuery("#user_experience_form").trigger('reset');
    });

    jQuery(document).on("click", ".edit_experience", function () {
        $('#experience-popup').find('.model_heading_text').html(edit_experience_heading);
        $('#user_experience_form').attr('action', edit_exp_url);
        $('#user_experience_form').find('#company_name').val($(this).parents('.profile-info-bx').attr('data-company'));
        $('#user_experience_form').find('#job_type').val($(this).parents('.profile-info-bx').attr('data-job'));
        $('#user_experience_form').find('#company_place').val($(this).parents('.profile-info-bx').attr('data-place'));
        $('#user_experience_form').find('#experience_start_month').val($(this).parents('.profile-info-bx').attr('data-startmonth'));
        $('#user_experience_form').find('#experience_start_year').val($(this).parents('.profile-info-bx').attr('data-startyear'));
        $('#user_experience_form').find('#experience_end_month').val($(this).parents('.profile-info-bx').attr('data-endmonth'));
        $('#user_experience_form').find('#experience_end_year').val($(this).parents('.profile-info-bx').attr('data-endyear'));
        $('#user_experience_form').find('#exp_experience_id').val($(this).parents('.profile-info-bx').attr('data-exp-id'));

        if ($(this).parents('.profile-info-bx').attr('data-in-current-position') == 1) {
            $('#user_experience_form').find('#in_current_position').prop('checked', true);
            $('#user_experience_form').find('#experience_end_month').val('');
            $('#user_experience_form').find('#experience_end_year').val('');
            $("#experience-popup #experience_end_month").attr('disabled', 'disabled');
            $("#experience-popup #experience_end_year").attr('disabled', 'disabled');
        } else {
            $('#user_experience_form').find('#in_current_position').prop('checked', false);
            $("#experience-popup #experience_end_month").removeAttr('disabled');
            $("#experience-popup #experience_end_year").removeAttr('disabled');
        }
        $('#user_experience_form').find('#experience_description').val($(this).parents('.profile-info-bx').attr('data-desc'));
        $("#job_type").selectpicker("refresh");
        $("#experience-popup #experience_start_year").removeAttr('disabled');
        $("#experience_start_month").selectpicker("refresh");
        $("#experience_end_month").selectpicker("refresh");
        $("#experience-popup").modal({backdrop: 'static', keyboard: false});
    });


    jQuery(document).on("click", ".training_popup_open", function () {
        $('#user_training_form').attr('action', add_exp_url);
        $('#user_training_form').find('h3').html(add_training_heading);
        jQuery("#user_training_form").trigger('reset');
        $('#training-popup').modal({backdrop: 'static', keyboard: false});
    });

    jQuery(document).on("click", ".edit_training", function () {
        $('#user_training_form').attr('action', edit_exp_url);
        $('#user_training_form').find('h3').html(edit_training_heading);

        $('#user_training_form').find('#user_training').val($(this).parents('.profile-info-bx').attr('data-training'));
        $('#user_training_form').find('#user_establishment').val($(this).parents('.profile-info-bx').attr('data-establishment'));
        $('#user_training_form').find('#user_training_desc').val($(this).parents('.profile-info-bx').attr('data-desc'));
        $('#user_training_form').find('#training_start_month').val($(this).parents('.profile-info-bx').attr('data-train-start-month'));
        $('#user_training_form').find('#training_start_year').val($(this).parents('.profile-info-bx').attr('data-train-start-year'));
        $('#user_training_form').find('#training_end_month').val($(this).parents('.profile-info-bx').attr('data-train-end-month'));
        $('#user_training_form').find('#training_end_year').val($(this).parents('.profile-info-bx').attr('data-train-end-year'));
        $('#user_training_form').find('#training_id').val($(this).parents('.profile-info-bx').attr('data-train-id'));
        $("#training-popup #training_start_year").removeAttr('disabled');
        $("#training-popup #training_end_year").removeAttr('disabled');
        $("#training_start_month").selectpicker("refresh");
        $("#training_end_month").selectpicker("refresh");
        $('#training-popup').modal({backdrop: 'static', keyboard: false});
    });

    jQuery(document).on("click", ".add-skill-popup", function () {
        $('#user_skill_form').attr('action', add_exp_url);
        $('#user_skill_form').find('h3').html(add_your_skill);
        jQuery("#user_skill_form").trigger('reset');
        star_count = 111;
        var skill_box = '<div class="row">' +
                '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mob-input-width">' +
                '<div class="form-group">' +
                '<input type="text" placeholder="" name="user_skill_establishment[' + star_count + ']" class="form-control required skill_establishment_input" data-rule-minlength="2" data-rule-maxlength="50">' +
                '</div>' +
                '</div>' +
                '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 mob-input-width skill_star_div set_skill_star_main_div">' +
                '<div class="star-rate extrap-top">' +
                '<fieldset class="rating set_skill_star_ratting_div">' +
                '<input class="skill_rating_input" type="radio" value="5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star5' + star_count + '"><label title="Awesome - 5 stars" for="user_add_skill_star5' + star_count + '" class="full"></label>' +
                '<input class="skill_rating_input" type="radio" value="4.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star4' + star_count + 'half"><label title="Pretty good - 4.5 stars" for="user_add_skill_star4' + star_count + 'half" class="half"></label>' +
                '<input class="skill_rating_input" type="radio" value="4" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star4' + star_count + '"><label title="Pretty good - 4 stars" for="user_add_skill_star4' + star_count + '" class="full"></label>' +
                '<input class="skill_rating_input" type="radio" value="3.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star3' + star_count + 'half"><label title="Meh - 3.5 stars" for="user_add_skill_star3' + star_count + 'half" class="half"></label>' +
                '<input class="skill_rating_input" type="radio" value="3" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star3' + star_count + '"><label title="Meh - 3 stars" for="user_add_skill_star3' + star_count + '" class="full"></label>' +
                '<input class="skill_rating_input" type="radio" value="2.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star2' + star_count + 'half"><label title="Kinda bad - 2.5 stars" for="user_add_skill_star2' + star_count + 'half" class="half"></label>' +
                '<input class="skill_rating_input" type="radio" value="2" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star2' + star_count + '"><label title="Kinda bad - 2 stars" for="user_add_skill_star2' + star_count + '" class="full"></label>' +
                '<input class="skill_rating_input" type="radio" value="1.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star1' + star_count + 'half"><label title="Meh - 1.5 stars" for="user_add_skill_star1' + star_count + 'half" class="half"></label>' +
                '<input class="skill_rating_input" type="radio" value="1" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star1' + star_count + '"><label title="Sucks big time - 1 star" for="user_add_skill_star1' + star_count + '" class="full"></label>' +
                '<input class="skill_rating_input" type="radio" value="0.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star0' + star_count + 'half"><label title="Sucks big time - 0.5 stars" for="user_add_skill_star0' + star_count + 'half" class="half"></label>' +
                '</fieldset>' +
                '</div>' +
                '</div>' +
                '</div>';
        star_count++;
        jQuery("#add_skill_dynamic").html(skill_box);
        $('#skills-popup').modal({backdrop: 'static', keyboard: false});
        $('.skill_rating_input').each(function () {
            $(this).rules("add", {
                required: true
            });
        });
    });

    jQuery(document).on("click", ".edit_skill", function () {
        $('#user_skill_form').attr('action', edit_exp_url);
        $('#user_skill_form').find('h3').html(edit_skill_heading);

        $('#user_skill_form').find('#user_skill').val($(this).parents('.profile-info-bx').attr('data-skill-area'));
        $('#user_skill_form').find('#skill_id').val($(this).parents('.profile-info-bx').attr('data-skill-id'));
        var skill_box = '';
        star_count = 111;
        for (i = 1; i <= $(this).parents('.profile-info-bx').attr('data-skill-count'); i++) {
            skill_box += '<div class="row">' +
                    '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 mob-input-width">' +
                    '<div class="form-group">' +
                    '<input type="text" placeholder="" value="' + $(this).parents('.profile-info-bx').attr('data-skill-' + i) + '" name="user_skill_establishment[' + star_count + ']" class="form-control required skill_establishment_input" data-rule-minlength="2" data-rule-maxlength="50">' +
                    '</div>' +
                    '</div>' +
                    '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 mob-input-width skill_star_div set_skill_star_main_div">' +
                    '<div class="star-rate extrap-top">' +
                    '<fieldset class="rating set_skill_star_ratting_div">';

            if ($(this).parents('.profile-info-bx').attr('data-star-' + i) == 5) {
                skill_box += '<input class="skill_rating_input" checked="checked" type="radio" value="5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star5' + star_count + '"><label title="Awesome - 5 stars" for="user_add_skill_star5' + star_count + '" class="full"></label>';
            } else {
                skill_box += '<input class="skill_rating_input" type="radio" value="5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star5' + star_count + '"><label title="Awesome - 5 stars" for="user_add_skill_star5' + star_count + '" class="full"></label>';
            }
            if ($(this).parents('.profile-info-bx').attr('data-star-' + i) == 4.5) {
                skill_box += '<input class="skill_rating_input" checked="checked" type="radio" value="4.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star4' + star_count + 'half"><label title="Pretty good - 4.5 stars" for="user_add_skill_star4' + star_count + 'half" class="half"></label>';
            } else {
                skill_box += '<input class="skill_rating_input" type="radio" value="4.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star4' + star_count + 'half"><label title="Pretty good - 4.5 stars" for="user_add_skill_star4' + star_count + 'half" class="half"></label>';
            }
            if ($(this).parents('.profile-info-bx').attr('data-star-' + i) == 4) {
                skill_box += '<input class="skill_rating_input" checked="checked" type="radio" value="4" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star4' + star_count + '"><label title="Pretty good - 4 stars" for="user_add_skill_star4' + star_count + '" class="full"></label>';
            } else {
                skill_box += '<input class="skill_rating_input" type="radio" value="4" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star4' + star_count + '"><label title="Pretty good - 4 stars" for="user_add_skill_star4' + star_count + '" class="full"></label>';
            }
            if ($(this).parents('.profile-info-bx').attr('data-star-' + i) == '3.5') {
                skill_box += '<input class="skill_rating_input" checked="checked" type="radio" value="3.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star3' + star_count + 'half"><label title="Meh - 3.5 stars" for="user_add_skill_star3' + star_count + 'half" class="half"></label>';
            } else {
                skill_box += '<input class="skill_rating_input" type="radio" value="3.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star3' + star_count + 'half"><label title="Meh - 3.5 stars" for="user_add_skill_star3' + star_count + 'half" class="half"></label>';
            }
            if ($(this).parents('.profile-info-bx').attr('data-star-' + i) == 3) {
                skill_box += '<input class="skill_rating_input" checked="checked" type="radio" value="3" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star3' + star_count + '"><label title="Meh - 3 stars" for="user_add_skill_star3' + star_count + '" class="full"></label>';
            } else {
                skill_box += '<input class="skill_rating_input" type="radio" value="3" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star3' + star_count + '"><label title="Meh - 3 stars" for="user_add_skill_star3' + star_count + '" class="full"></label>';
            }
            if ($(this).parents('.profile-info-bx').attr('data-star-' + i) == 2.5) {
                skill_box += '<input class="skill_rating_input" checked="checked" type="radio" value="2.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star2' + star_count + 'half"><label title="Kinda bad - 2.5 stars" for="user_add_skill_star2' + star_count + 'half" class="half"></label>';
            } else {
                skill_box += '<input class="skill_rating_input" type="radio" value="2.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star2' + star_count + 'half"><label title="Kinda bad - 2.5 stars" for="user_add_skill_star2' + star_count + 'half" class="half"></label>';
            }
            if ($(this).parents('.profile-info-bx').attr('data-star-' + i) == 2) {
                skill_box += '<input class="skill_rating_input" checked="checked" type="radio" value="2" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star2' + star_count + '"><label title="Kinda bad - 2 stars" for="user_add_skill_star2' + star_count + '" class="full"></label>';
            } else {
                skill_box += '<input class="skill_rating_input" type="radio" value="2" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star2' + star_count + '"><label title="Kinda bad - 2 stars" for="user_add_skill_star2' + star_count + '" class="full"></label>';
            }
            if ($(this).parents('.profile-info-bx').attr('data-star-' + i) == 1.5) {
                skill_box += '<input class="skill_rating_input" checked="checked" type="radio" value="1.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star1' + star_count + 'half"><label title="Meh - 1.5 stars" for="user_add_skill_star1' + star_count + 'half" class="half"></label>';
            } else {
                skill_box += '<input class="skill_rating_input" type="radio" value="1.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star1' + star_count + 'half"><label title="Meh - 1.5 stars" for="user_add_skill_star1' + star_count + 'half" class="half"></label>';
            }
            if ($(this).parents('.profile-info-bx').attr('data-star-' + i) == 1) {
                skill_box += '<input class="skill_rating_input" checked="checked" type="radio" value="1" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star1' + star_count + '"><label title="Sucks big time - 1 star" for="user_add_skill_star' + star_count + '" class="full"></label>';
            } else {
                skill_box += '<input class="skill_rating_input" type="radio" value="1" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star1' + star_count + '"><label title="Sucks big time - 1 star" for="user_add_skill_star' + star_count + '" class="full"></label>';
            }
            if ($(this).parents('.profile-info-bx').attr('data-star-' + i) == 0.5) {
                skill_box += '<input class="skill_rating_input" checked="checked" type="radio" value="0.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star0' + star_count + 'half"><label title="Sucks big time - 0.5 stars" for="user_add_skill_star0' + star_count + 'half" class="half"></label>';
            } else {
                skill_box += '<input class="skill_rating_input" type="radio" value="0.5" name="user_skill_rating[' + star_count + ']" id="user_add_skill_star0' + star_count + 'half"><label title="Sucks big time - 0.5 stars" for="user_add_skill_star0' + star_count + 'half" class="half"></label>';
            }
            skill_box += '</fieldset>' +
                    '</div>' +
                    '</div>' +
                    '</div>';
            star_count++;

            $('.skill_rating_input').each(function () {
                $(this).rules("add", {
                    required: true
                });
            });
        }
        jQuery("#add_skill_dynamic").html(skill_box);

        $('#skills-popup').modal({backdrop: 'static', keyboard: false});
    });


    jQuery(document).on("click", ".hobby_popup_open", function () {
        $('#user_training_form').attr('action', add_exp_url);
        $('#user_training_form').find('h3').html(add_hobby_heading);
        jQuery("#user_training_form").trigger('reset');
        $('#hobbies-popup').modal({backdrop: 'static', keyboard: false});
    });

    jQuery(document).on("click", ".edit_hobby", function () {
        $('#user_hobbies_form').attr('action', edit_exp_url);
        $('#user_hobbies_form').find('h3').html(edit_hobby_heading);

        $('#user_hobbies_form').find('#hobbies_title').val($(this).attr('data-title'));
        $('#user_hobbies_form').find('#hobbies_description').val($(this).attr('data-desc'));
        $('#user_hobbies_form').find('#hobbies_id').val($(this).attr('data-id'));
        $('#hobbies-popup').modal({backdrop: 'static', keyboard: false});
    });

    $("body").on("click", ".removebutton", function () {
        var removebuttonclass = $(this).attr('data-langugid');
        $("." + removebuttonclass).remove();
    });

});