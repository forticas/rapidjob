$(window).scroll(function() {    
    var scroll = $(window).scrollTop();

    if (scroll >= 100) {
        $(".navbar-fixed-top").addClass("animate-nav");
    } else {
        $(".navbar-fixed-top").removeClass("animate-nav");
    }
});

$(".down-scroll-btn").click(function() {
    $('html,body').animate({
        scrollTop: $(".hm-about-text").offset().top -70},
        'slow');
});

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})