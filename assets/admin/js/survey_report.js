$(document).ready(function () {

    $(document).on("click", "#export", function (e) {
        e.preventDefault();
        $('#survey_report_form').submit();

    });

    $(document).on("click", "#sr_report_filter", function () {

        s_status = $("#s_status").val();
        s_user = $("#s_user").val();
        s_settlement = $("#s_settlement").val();
        start = $("#start").val();
        end = $("#end").val();
        $("#survey").setGridParam(
                {
                    url: base_url + "/Report/get_surveys",
                    datatype: "JSON",
                    postData: {s_status: s_status, s_user: s_user, s_settlement: s_settlement, start: start, end: end},
                    page: 1
                }
        ).trigger("reloadGrid");//Reload grid trigger



    });


//vaccination filter end*/

    s_status = $("#s_status").val();
    s_user = $("#s_status").val();
    s_settlement = $("#s_settlement").val();
    start = $("#start").val();
    end = $("#end").val();



    $("#survey").jqGrid({
        url: base_url + "/Report/get_surveys",
        datatype: "JSON",
        postData: {s_status: s_status, s_user: s_user, s_settlement: s_settlement, start: start, end: end},
        height: 300,
        autowidth: true,
        shrinkToFit: true,
        rowNum: 10,
        rowList: [10, 20, 30],
        colNames: ['Status', 'Settlement', 'UserName', "Vaccinated Counts", "Unvaccinated Counts", "Total Survey counts", 'Assigned By', 'Created date', ],
        colModel: [
            {name: 'SR_STATUS', index: 'SR_STATUS', width: 30, sorttype: "int"},
            {name: 'st_name', index: 'st_name', width: 50, sorttype: "int"},
            {name: 'u_username', index: 'u_username', width: 60, sorttype: "int"},
            {name: 'sr_vaccinated_count', index: 'sr_vaccinated_count', width: 60, sorttype: "int"},
            {name: 'sr_unvaccinated_count', index: 'sr_unvaccinated_count', width: 60, sorttype: "int"},
            {name: 'sr_total_count', index: 'sr_total_count', width: 60, sorttype: "int"},
            {name: 'a_name', index: 'a_name', width: 60, sorttype: "int"},
            {name: 'readableDate', index: 'readableDate', width: 60, sorttype: "int"},
        ],
        pager: "#survey_pager",
        viewrecords: true,
        caption: "survey",
        add: false,
        edit: true,
        addtext: 'Add',
        edittext: 'Edit',
        hidegrid: false,
        loadComplete: function () {

            var table = this;
            setTimeout(function () {
                jQuery(".ui-pg-table").find(".ui-icon-refresh").addClass('fa fa-refresh btn btn-primary').removeClass('ui-icon-refresh ui-icon');
                jQuery(".ui-pg-table").find(".ui-icon-seek-next").addClass('fa fa-step-forward btn btn-default').removeClass('ui-icon ui-icon-seek-next');
                jQuery(".ui-pg-table").find(".ui-icon-seek-end").addClass('fa fa-fast-forward btn btn-default').removeClass('ui-icon ui-icon-seek-end');
                jQuery(".ui-pg-table").find(".ui-icon-seek-prev").addClass('fa fa-step-backward btn btn-default').removeClass('ui-icon ui-icon-seek-prev');
                jQuery(".ui-pg-table").find(".ui-icon-seek-first").addClass('fa fa-fast-backward btn btn-default').removeClass('ui-icon ui-icon-seek-first');
                jQuery(".ui-pg-table").find(".ui-pg-input").addClass('form-control');
                jQuery(".ui-pg-table").find(".ui-pg-selbox").addClass('form-control').removeClass('ui-pg-selbox');


            }, 0);

        }
    });



    // Add selection
    $("#survey").setSelection(4, true);


    // Setup buttons
    $("#survey").jqGrid('navGrid', '#survey_pager',
            {edit: false, add: false, del: false, search: false},
    {height: 200, reloadAfterSubmit: true}
    );


    jQuery('.ui-jqgrid-title').after('<div class="pull-right"><a href="#" id="export" type="button" class="btn btn-primary btn-xs" role="button"><span class="fa fa-plus-circle"></span>&nbsp; Export </a></div>');
    var FromEndDate = new Date();

    $('#datepicker').datepicker({
        keyboardNavigation: false,
        forceParse: false,
        autoclose: true,
        format: 'dd-mm-yyyy',
        endDate: FromEndDate,
        autoclose: true

    });
    /*end vaccination*/



});