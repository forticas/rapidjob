jQuery(document).ready(function () {
    jQuery(document).ajaxSuccess(function (event, xhr, settings) {
        var response = jQuery.parseJSON(xhr.responseText);
        jQuery("#csrfTokenData").val(response.csrfValue);
    });

    loadJQgrid();
    function loadJQgrid() {

        var jqGrid = jQuery("#jqgrid_table").jqGrid({
            url: controller_url + "/view",
            datatype: "JSON",
            mtype: 'POST',
            defaults: {
                recordtext: "View {0} - {1} of {2}",
                emptyrecords: "No records to view",
                loadtext: "Loading...",
                pgtext: "Page {0} of {1}"
            },
            postData: {
                name: jQuery("#name").val(),
                email: jQuery("#email").val(),
                status: jQuery("#status").val(),
                "secure_csrf": function () {
                    return jQuery("#csrfTokenData").val()
                }
            },
            height: 300,
            autowidth: true,
            shrinkToFit: true,
            rowNum: 10,
            rowList: [10, 20, 30],
            colNames: ['Name', 'Email', 'Join Date', 'Status', 'Action'],
            colModel: [
                {name: 'u_name', index: 'u_name', width: 150, sorttype: "string"},
                {name: 'u_email', index: 'u_email', width: 150, sorttype: "string"},
                {name: 'u_created_date', index: 'u_created_date', width: 60, sorttype: "int"},
                {name: 'status_html', index: 'status_html', width: 60, sorttype: "int", sortable: false},
                {name: 'action_html', index: 'action_html', width: 40, sorttype: "int", sortable: false},
            ],
            pager: "#pager_category",
            viewrecords: true,
            caption: "Users",
            add: false,
            edit: true,
            addtext: 'Add',
            edittext: 'Edit',
            hidegrid: false,
            loadComplete: function () {
                var table = this;
                setTimeout(function () {
                    jQuery(".ui-pg-table").find(".ui-icon-refresh").addClass('fa fa-refresh btn btn-primary').removeClass('ui-icon-refresh ui-icon');
                    jQuery(".ui-pg-table").find(".ui-icon-seek-next").addClass('fa fa-step-forward btn btn-default').removeClass('ui-icon ui-icon-seek-next');
                    jQuery(".ui-pg-table").find(".ui-icon-seek-end").addClass('fa fa-fast-forward btn btn-default').removeClass('ui-icon ui-icon-seek-end');
                    jQuery(".ui-pg-table").find(".ui-icon-seek-prev").addClass('fa fa-step-backward btn btn-default').removeClass('ui-icon ui-icon-seek-prev');
                    jQuery(".ui-pg-table").find(".ui-icon-seek-first").addClass('fa fa-fast-backward btn btn-default').removeClass('ui-icon ui-icon-seek-first');
                    jQuery(".ui-pg-table").find(".ui-pg-input").addClass('form-control');
                    jQuery(".ui-pg-table").find(".ui-pg-selbox").addClass('form-control').removeClass('ui-pg-selbox');

                    var elem = document.querySelectorAll('.ct_switch');

                    for (i = 0; i < elem.length; i++)
                    {
                        new Switchery(elem[i], {color: '#1AB394'});
                    }

                    //convert time stemp to date
                    var rows = jqGrid.getDataIDs();
                    for (var i = 0; i < rows.length; i++)
                    {
                        var timestemp = jqGrid.getCell(rows[i], "u_created_date");
                        var timestemp = parseInt(timestemp);
                        if (timestemp > 0) {
                            var date = formatDateLocal("yyyy-MM-dd hh:mm", timestemp * 1000);
                            jqGrid.jqGrid('setCell', rows[i], 'u_created_date', date);
                        }
                    }

                }, 0);

            }
        });


        var create_add_html = "\
                                <div class='pull-right'>\
                                    <a href='" + controller_url + "/add' type='button' class='btn btn-primary btn-xs' role='button'>\
                                        <span class='fa fa-plus-circle'></span>&nbsp; Add New \
                                    </a>\
                                </div>";
        jQuery('#gbox_jqgrid_table .ui-jqgrid-title').after(create_add_html);

        // Setup buttons
        jQuery("#jqgrid_table").jqGrid('navGrid', '#pager_category',
                {edit: false, add: false, del: false, search: false},
                {height: 200, reloadAfterSubmit: true}
        );

        // Add responsive to jqGrid
        jQuery(window).bind('resize', function () {
            var width = jQuery('.jqGrid_wrapper').width();
            jQuery('#jqgrid_table').setGridWidth(width);
        });

    }

    //delete settlement code
    jQuery(document).on("click", ".delete", function () {
        var id = jQuery(this).data("id");
        var self = jQuery(this);
        swal({
            title: "",
            text: "Are you sure want to delete?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false
        },
                function () {
                    //delete ajax request
                    jQuery.ajax({
                        url: controller_url + '/delete',
                        type: 'post',
                        dataType: 'json',
                        async:"false",
                        data: {
                            id: id,
                            "secure_csrf": function () {
                                return jQuery("#csrfTokenData").val()
                            }
                        },
                        success: function (data) {
                            if (data.success) {
                                swal("Deleted!", "Deleted sucessfully.", "success");
                            } else {
                                swal("Can't Delete", "Error while perform delete operation", "error");
                            }
                        },
                        error: function () {
                            swal("", "Error while perform delete operation", "error");
                        },
                    });

                    jQuery('#jqgrid_table').jqGrid('GridUnload');
                    loadJQgrid();

                });
    });


    //status change code
    jQuery(document).on("change", ".status_change", function () {
        var id = jQuery(this).data("id");
        var status = jQuery(this).val();
        if ($(this).is(':checked')) {
            status = 1;
        } else {
            status = 2;
        }
        self = jQuery(this);
        //ajax request
        jQuery.ajax({
            url: controller_url + '/change_status',
            type: 'post',
            dataType: 'json',
            data: {
                id: id,
                status: status,
                "secure_csrf": function () {
                    return jQuery("#csrfTokenData").val()
                }
            },
            success: function (data) {
                if (data.success) {
                    swal("", "Category status updated", "success");
                } else {
                    swal("", "Problem while update status", "error");
                }
            },
            error: function () {
                swal("", "Problem while update status", "error");
            }
        });

        jQuery('#jqgrid_table').jqGrid('GridUnload');
        loadJQgrid();

    });


    //category validation
    jQuery("#add_form").validate({
        rules: {
            "cpassword": {
                equalTo: "#password"
            }
        },
        messages: {
            "cpassword": {
                equalTo: "Password and confirm password must be same"
            }
        },
        errorPlacement: function (error, element) {

            var next = element;

            if (element.hasClass('file'))
                jQuery(error).insertAfter(jQuery(element).parents(".file-input"));
            else if (element.hasClass('chosen-select'))
                jQuery(error).insertAfter(jQuery(element).siblings(".chosen-container"));
            else
                jQuery(error).insertAfter(element);

        },
        errorClass: 'error',
    });

    //edit category form validation
    jQuery("#edit_form").validate({
        rules: {
            "cpassword": {
                equalTo: "#password"
            }
        },
        messages: {
            "cpassword": {
                equalTo: "Password and confirm password must be same"
            }
        },
        errorPlacement: function (error, element) {

            var next = element;

            if (element.hasClass('file'))
                jQuery(error).insertAfter(jQuery(element).parents(".file-input"));
            else if (element.hasClass('chosen-select'))
                jQuery(error).insertAfter(jQuery(element).siblings(".chosen-container"));
            else
                jQuery(error).insertAfter(element);

        },
        errorClass: 'error',
    });


    //filter code
    jQuery("#filter").click(function () {
        jQuery("#jqgrid_table").setGridParam(
                {
                    url: controller_url + "/view",
                    datatype: "JSON",
                    postData: {
                        name: jQuery("#name").val(),
                        email: jQuery("#email").val(),
                        "status": jQuery("#status").val(),
                        "secure_csrf": function () {
                            return jQuery("#csrfTokenData").val()
                        }
                    },
                }
        ).trigger("reloadGrid");//Reload grid trigger
    });


    //clear filter code
    jQuery("#filter_clear").click(function () {
        jQuery("#name").val('');
        jQuery("#status").val('');
        jQuery("#email").val('');
        jQuery('#jqgrid_table').jqGrid('GridUnload');
        loadJQgrid();
    });
});

