var data_table = '#datatable_list';
function get_all_data() {
    var name_filter = $('.name_filter').val();
    jQuery(data_table).DataTable({
        responsive: true,
        processing: true,
        serverSide: true,
        pagingType: "full_numbers",
        scrollY: '50vh',
        scrollX: true,
        scrollCollapse: true,
        searching: false,
        order: [1, 'DESC'],
        "columns": [
            {"data": "candidate_name"},
            {"data": "recruiter_name"},
            {"data": "company_address"},
            {"data": "created_at"},
            {"data": "status"},
            {"data": "action_cv"},
            {"data": "action_cover_latter"}
        ],
        columnDefs: [
            {
                targets: [0],
                searchable: true,
                sortable: true,
                sWidth: "110px"
            },
            {
                targets: [1],
                searchable: true,
                sortable: true,
                sWidth: "110px"
            },
            {
                targets: [2],
                searchable: false,
                sortable: false,
                sWidth: "110px"
            },
            {
                targets: [3],
                searchable: true,
                sortable: true,
                sWidth: "110px"
            },
            {
                targets: [4],
                searchable: true,
                sortable: false,
                sWidth: "80px"
            },
            {
                targets: [5],
                searchable: false,
                sortable: false,
                sWidth: "70px"
            },
            {
                targets: [6],
                searchable: false,
                sortable: false,
                sWidth: "90px"
            }
        ],
        language: {
            emptyTable: "No data available",
            zeroRecords: "No matching records found...",
            infoEmpty: "No records available",
        },
        oLanguage: {
            sProcessing: "Processing",
        },
        ajax: {
            "url": controller_url + '/get_all_data',
            "type": "POST",
            "async": false,
            "data": {name_filter: name_filter, application_type: application_type},
        },
        drawCallback: function () {
            jQuery('<li><a onclick="refresh_tab()" style="cursor:pointer" title="Refresh"><i class="material-icons">cached</i></a></li>').prependTo('div.dataTables_paginate ul.pagination');
        }
    });
}

function refresh_tab() {
    jQuery(data_table).dataTable().fnDestroy();
    get_all_data();
    jQuery("#datatable_list_filter").css('display', 'none');
}

function filterGlobal() {
    jQuery(data_table).DataTable().search(
            jQuery('#global_filter').val()
            ).draw();
}

function filterColumn(i) {
    jQuery(data_table).DataTable().column(i).search(
            jQuery('#col' + i + '_filter').val()
            ).draw();
}

jQuery(document).ready(function () {
    get_all_data();

//    $(".nav-tabs.tab-nav-right li a:first").click();

    // change active / inactive status
    jQuery(document).on('change', '.status_change', function () {
        var this_app_row = jQuery(this);
        var status = $(this).val();
        var user_app_id = jQuery(this).attr('data-id');
        var recruiter_id = jQuery(this).attr('recruiter-id');
        var candidate_id = jQuery(this).attr('candidate-id');

        if (!isNaN(candidate_id) && !isNaN(user_app_id) && !isNaN(recruiter_id) && status == 1) {
            jQuery.ajax({
                "url": controller_url + '/change_status',
                type: "POST",
                data: {
                    'user_app_id': user_app_id, 'status': status, 'recruiter_id': recruiter_id, 'candidate_id': candidate_id
                },
                dataType: 'json',
                cache: false,
                async: false,
                success: function (response) {
                    if (response.success == 'true') {
                        jQuery(data_table).DataTable().row(this_app_row).remove().draw(false);
                        toastr.success(response.message);
                    } else {
                        toastr.error(response.message);
                    }
                },
                error: function () {
                    toastr.error("Problem in performing your action.");
                }
            });
        } else {
            toastr.error("Invalid request...!");
        }
    });

    // delete data
    jQuery(document).on('click', '.delete_data_button', function () {
        var id;
        var this_row = jQuery(this);
        id = jQuery(this).attr('data-id');
        swal({
            title: "Are you sure you want to delete this application?",
            text: "This data will be permanently removed, and not revertible.",
            type: "info",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
        }, function () {
            if (!isNaN(id)) {
                jQuery.ajax({
                    "url": controller_url + '/delete',
                    type: "POST",
                    data: {
                        'id': id
                    },
                    dataType: 'json',
                    cache: false,
                    success: function (response) {
                        if (response.success == 'true') {
                            setTimeout(function () {
                                swal(response.message);
                                jQuery(data_table).DataTable().row(this_row).remove().draw(false);
                            }, 2000);

                        } else {
                            setTimeout(function () {
                                swal(response.message);
                            }, 2000);
                        }
                    },
                    error: function () {
                        setTimeout(function () {
                            swal("Problem in performing your action.");
                        }, 2000);
                    }
                });
            } else {
                setTimeout(function () {
                    swal("Invalid request...!");
                }, 2000);
            }
        });
    });

    jQuery('.search_filter').click(function () {
        jQuery(data_table).dataTable().fnDestroy();
        get_all_data();
    });

    jQuery('.reset_filter').click(function () {
        jQuery('select#col5_filter').selectpicker('deselectAll');
        jQuery('.name_filter').val('');
        setTimeout(function () {
            jQuery(data_table).dataTable().fnDestroy();
            get_all_data();
        }, 100);
    });

});