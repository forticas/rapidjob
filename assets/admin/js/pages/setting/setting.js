jQuery("#setting_add_form").validate({
    rules: {
        payment_in_cent_per_email:{
            number:true
        },
        payment_rate_for_send_application_by_post:{
            number:true
        },
        recruiter_subscription_plan_price:{
            number:true
        },
        recruiter_subscription_plan_duration:{
            number:true
        },
    },
    messages: {
        payment_in_cent_per_email:{
            number:"Please enter valid payment rate."
        },
        payment_rate_for_send_application_by_post:{
            number:"Please enter valid payment rate."
        },
        recruiter_subscription_plan_price:{
            number:"Please enter valid plan price"
        },
        recruiter_subscription_plan_duration:{
            number:"Please enter valid plan duration"
        },
    },
    errorPlacement: function (error, element) {
        if (element.hasClass('file')) {
            jQuery(error).insertAfter(jQuery(element).parents(".file-input"));
        } else if (element.hasClass('chosen-select')) {
            jQuery(error).insertAfter(jQuery(element).siblings(".chosen-container"));
        } else if (element.hasClass('form-control')) {
            jQuery(error).insertAfter(jQuery(element).parents(".payment_in_cent_per_email"));
        } else {
            jQuery(error).insertAfter(jQuery(element).parent());
        }
    },
});