jQuery("#file-0b").fileinput({
    showCaption: true,
    showUpload: false,
    previewFileType: "xls",
    allowedFileExtensions: ["xls", "xlsx"],
    showRemove: true,
    initialPreview: "<div class='file-preview-text'>" +
            "<h2><i class='glyphicon glyphicon-file'></i></h2>" +
            "sample.xlsx" + "</div>"
});

var flag = 1;
jQuery('#file-0b').on('fileerror', function (event, data, msg) {
    flag = 2;
});
jQuery('#file-0b').on('fileloaded', function (event, file, previewId, index, reader) {
    flag = 1;
});
jQuery("#import_recruiter_form").on('submit', function () {
    if (flag == 2) {
        return false;
    } else {
        return true;
    }
});

jQuery(document).ready(function () {
    jQuery.validator.addMethod('filesize', function (value, element, param) {
        return this.optional(element) || (element.files[0].size <= param)
    }, 'File size must be less than 2 MB');

    jQuery.validator.addMethod("fileextension", function (value, element) {
        return this.optional(element) || /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/i.test(value);
    });
    jQuery("#import_recruiter_form").validate({
        errorElement: 'div',
        errorClass: 'text-danger',
        focusInvalid: false,
        ignore: "select:hidden",
        rules: {
            recruiter_file: {
                required: true,
                fileextension: true,
                filesize: 20000000,
            },
        },
        messages: {
            recruiter_file: {
                fileextension: 'Invalid recruiter file extention, Upload only xls or xlsx file.',
            },
        },
        highlight: function (element) {
            jQuery(element).parent().addClass('has-error');
        },
        unhighlight: function (element) {
            jQuery(element).parent().removeClass('has-error');
        },
        errorPlacement: function (error, element) {
            if (element.hasClass('file')) {
                jQuery(error).insertAfter(jQuery(element).parents(".file-input"));
            } else if (element.parent().hasClass('input-group')) {
                error.insertAfter(element.parent());
            } else {
                error.insertAfter(element);
            }
        }
    });
});