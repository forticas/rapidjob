jQuery("#file-0b").fileinput({
    showCaption: true,
    showUpload: false,
    previewFileType: "image",
    allowedFileExtensions: ["jpg", "png", "jpeg"],
    showRemove: true,
    initialPreview: [
        "<img src='" + image_url + "' class='file-preview-image' height='100' width='100'>"
    ]
});

var flag = 1;
jQuery('#file-0b').on('fileerror', function(event, data, msg) {
    flag = 2;
});

jQuery('#file-0b').on('fileloaded', function(event, file, previewId, index, reader) {
    flag = 1;
});

jQuery("#custom_form").on('submit', function() {
    if (flag == 2) {
        return false;
    } else {
        return true;
    }
});


$.validator.addMethod("required", function(value, element) {
    var value1 = value.replace(/&nbsp;/g, '');
    if (value1.trim() === "") {
        return false;
    } else {
        return true;
    }
});

jQuery.validator.addMethod('filesize', function(value, element, param) {
    return this.optional(element) || (element.files[0].size <= param)
}, 'File size must be less than 2 MB');


jQuery(document).ready(function() {

    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    }, 'Please enter only alphabetical letters.');


    $.validator.addMethod("email", function(value, element) {
        var reg_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return reg_email.test(value);
    }, 'Please enter a valid email address');




    jQuery("#custom_form_add").validate({
        rules: {
            news_name_en: {
                required: true,
                minlength: 2                
            },
            news_description_en: {
                required: true,
                minlength: 2
            },
            news_name_fr: {
                required: true,
                minlength: 2                
            },
            news_description_fr: {
                required: true,
                minlength: 2
            },
            photo: {
                required: true,
                accept: "image/jpg,image/jpeg,image/png,image/gif",
                filesize: 2000000,
            }
        },
        messages: {
            news_name_en: {
                required: 'News name field cannot be blank!',
            },
            news_description_en: {
                required: 'News description field cannot be blank!',
            },
            news_name_fr: {
                required: 'News name field cannot be blank!',
            },
            news_description_fr: {
                required: 'News description field cannot be blank!',
            },
            photo: {
                required: 'Please select image',
                accept: 'Please select only image',
            }
        },
        errorPlacement: function(error, element) {
            var next = element;
            if (element.hasClass('file'))
                jQuery(error).insertAfter(jQuery(element).parents(".file-input"));
            else if (element.hasClass('chosen-select'))
                jQuery(error).insertAfter(jQuery(element).siblings(".chosen-container"));
            else
                jQuery(error).insertAfter(jQuery(element).parent());
        },
    });

    jQuery("#custom_form_edit").validate({
        rules: {
            news_name_en: {
                required: true,
                minlength: 2,
            },
            news_description_en: {
                required: true,
                minlength: 2
            },
            news_name_fr: {
                required: true,
                minlength: 2,
            },
            news_description_fr: {
                required: true,
                minlength: 2
            }
        },
        messages: {
            news_name_en: {
                required: 'News name field cannot be blank!',
            },
            news_description_en: {
                required: 'News description field cannot be blank!',
            },
            news_name_fr: {
                required: 'News name field cannot be blank!',
            },
            news_description_fr: {
                required: 'News description field cannot be blank!',
            }
        },
        errorPlacement: function(error, element) {
            var next = element;
            if (element.hasClass('file'))
                jQuery(error).insertAfter(jQuery(element).parents(".file-input"));
            else if (element.hasClass('chosen-select'))
                jQuery(error).insertAfter(jQuery(element).siblings(".chosen-container"));
            else
                jQuery(error).insertAfter(jQuery(element).parent());
        },
    });
});
