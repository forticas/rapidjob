jQuery(document).ready(function() {

    $.validator.addMethod("email", function(value, element) {
        var reg_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return reg_email.test(value);
    }, 'Please enter a valid email address');

    $("#faq_form").validate({
        rules:{
            question_en:{
                required:true
            },
            question_fr:{
                required:true
            },
            question_description_en:{
                required:true
            },
            question_description_fr:{
                required:true
            }
        },
        messages: {
        },
        errorPlacement: function(error, element) {
            if (element.hasClass('file'))
                jQuery(error).insertAfter(jQuery(element).parents(".file-input"));
            else
                jQuery(error).insertAfter(jQuery(element).parent());
        }
    });
});