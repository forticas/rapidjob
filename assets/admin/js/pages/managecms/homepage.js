if (cms_form == 1) {

    jQuery("#testimonial_photo").fileinput({
        showCaption: true,
        showUpload: false,
        previewFileType: "image",
        allowedFileExtensions: ["jpg", "png", "jpeg"],
        showRemove: true,
        maxFileSize: 2000000,
        initialPreview: [
            "<img src='" + image_url + "' class='file-preview-image' height='300' width='500'>"
        ]
    });

    var testimonial_flag = 1;
    jQuery('#testimonial_photo').on('fileerror', function (event, data, msg) {
        testimonial_flag = 2;
    });

    jQuery('#testimonial_photo').on('fileloaded', function (event, file, previewId, index, reader) {
        testimonial_flag = 1;
    });

    jQuery("#testimonial_form").on('submit', function () {
        if (testimonial_flag == 2) {
            return false;
        } else {
            return true;
        }
    });


} else if (cms_form == 2) {

    jQuery("#file-0b").fileinput({
        showCaption: true,
        showUpload: false,
        previewFileType: "image",
        allowedFileExtensions: ["jpg", "png", "jpeg"],
        showRemove: true,
        maxFileSize: 2000000,
        maxImageWidth: 2600,
        minImageWidth: 1500,
        minImageHeight: 500,
        maxImageHeight: 600,
        initialPreview: [
            "<img src='" + image_url + "' class='file-preview-image' height='300' width='768'>"
        ]
    });

    var flag = 1;
    jQuery('#file-0b').on('fileerror', function (event, data, msg) {
        flag = 2;
    });

    jQuery('#file-0b').on('fileloaded', function (event, file, previewId, index, reader) {
        flag = 1;
    });

    jQuery("#homepage_content_form").on('submit', function () {
        if (flag == 2) {
            return false;
        } else {
            return true;
        }
    });

} else {

    jQuery("#prof_image").fileinput({
        showCaption: true,
        showUpload: false,
        previewFileType: "image",
        allowedFileExtensions: ["jpg", "png", "jpeg"],
        showRemove: true,
        maxFileSize: 2000000,
        maxImageWidth: 2600,
        minImageWidth: 1500,
        minImageHeight: 500,
        maxImageHeight: 600,
        initialPreview: [
            "<img src='" + image_url + "' class='file-preview-image' height='300' width='768'>"
        ]
    });

    var flag = 1;
    jQuery('#prof_image').on('fileerror', function (event, data, msg) {
        flag = 2;
    });

    jQuery('#prof_image').on('fileloaded', function (event, file, previewId, index, reader) {
        flag = 1;
    });

    jQuery("#professional_content_form").on('submit', function () {
        if (flag == 2) {
            return false;
        } else {
            return true;
        }
    });

}



jQuery(document).ready(function () {

    $.validator.addMethod("email", function (value, element) {
        var reg_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return reg_email.test(value);
    }, 'Please enter a valid email address');

    jQuery.validator.addMethod("required", function (value, element) {
        var value1 = value.replace(/&nbsp;/g, '');
        if (value1.trim() === "") {
            return false;
        } else {
            return true;
        }
    });


    $("#professional_content_form").validate({
        rules: {
            /*prof_banner_content_en:{
             required:true
             },
             prof_banner_content_fr:{
             required:true
             },*/
            prof_database_en: {
                required: true
            },
            prof_database_fr: {
                required: true
            },
            prof_broadcast_en: {
                required: true
            },
            prof_broadcast_fr: {
                required: true
            },
            prof_premium_mode_en: {
                required: true
            },
            prof_premium_mode_fr: {
                required: true
            }
        },
        messages: {
        },
        errorPlacement: function (error, element) {
            if (element.hasClass('file'))
                jQuery(error).insertAfter(jQuery(element).parents(".file-input"));
            else
                jQuery(error).insertAfter(jQuery(element).parent());
        }
    });

    $("#edit_testimonial").validate({
        rules: {
            testimonial_title_en: {
                required: true
            },
            testimonial_description_en: {
                required: true
            },
            testimonial_title_fr: {
                required: true
            },
            testimonial_description_fr: {
                required: true
            }
        },
        messages: {
        },
        errorPlacement: function (error, element) {
            if (element.hasClass('file'))
                jQuery(error).insertAfter(jQuery(element).parents(".file-input"));
            else
                jQuery(error).insertAfter(jQuery(element).parent());
        }
    });
    $("#add_testimonial").validate({
        rules: {
            testimonial_title_en: {
                required: true
            },
            testimonial_description_en: {
                required: true
            },
            testimonial_title_fr: {
                required: true
            },
            testimonial_description_fr: {
                required: true
            },
            testimonial_photo: {
                required: true
            }
        },
        messages: {
        },
        errorPlacement: function (error, element) {
            if (element.hasClass('file'))
                jQuery(error).insertAfter(jQuery(element).parents(".file-input"));
            else
                jQuery(error).insertAfter(jQuery(element).parent());
        }
    });

    $("#homepage_content_form").validate({
        rules: {
            /*banner_content_en: {
             required: true
             },
             banner_content_fr: {
             required: true
             },*/
            create_account_en: {
                required: true
            },
            create_account_fr: {
                required: true
            },
            /*news_title_en: {
             required: true
             },
             news_title_fr: {
             required: true
             },*/
            subscribe_text_en: {
                required: true
            },
            subscribe_text_fr: {
                required: true
            },
            copy_right_text_en: {
                required: true
            },
            copy_right_text_fr: {
                required: true
            },
            contact_us: {
                email: true,
                required: true
            },
            facebook_link: {
                url: true,
            },
            twitter_link: {
                url: true
            },
            google_link: {
                url: true
            },
            linkedin_link: {
                url: true
            }
        },
        messages: {
        },
        errorPlacement: function (error, element) {
            if (element.hasClass('file'))
                jQuery(error).insertAfter(jQuery(element).parents(".file-input"));
            else
                jQuery(error).insertAfter(jQuery(element).parent());
        }
    });

});