$.validator.addMethod("required", function (value, element) {
    var value1 = value.replace(/&nbsp;/g, '');
    if (value1.trim() === "") {
        return false;
    } else {
        return true;
    }
});

jQuery(document).ready(function () {
    jQuery("#custom_form_add").validate({
        rules: {
            subject: {
                required: false,
                minlength: 2,
                maxlength: 1000
            },
            message: {
                required: false,
                minlength: 2,
                maxlength: 10000
            }
        },
        messages: {
            subject: {
                required: 'Newsletter field cannot be blank!',
            },
            message: {
                required: 'Newsletter message field cannot be blank!',
            }
        },
        errorPlacement: function (error, element) {
            var next = element;
            if (element.hasClass('file'))
                jQuery(error).insertAfter(jQuery(element).parents(".file-input"));
            else if (element.hasClass('chosen-select'))
                jQuery(error).insertAfter(jQuery(element).siblings(".chosen-container"));
            else
                jQuery(error).insertAfter(jQuery(element).parent());
        },
    });
});
