$.validator.addMethod("required", function(value, element) {
    var value1 = value.replace(/&nbsp;/g, '');
    if (value1.trim() === "") {
        return false;
    } else {
        return true;
    }
});

jQuery(document).ready(function() {

    $.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
    }, 'Please enter only alphabetical letters.');


    $.validator.addMethod("email", function(value, element) {
        var reg_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return reg_email.test(value);
    }, 'Please enter a valid email address');




    jQuery("#custom_form_add").validate({
        rules: {
            subject: {
                required: true,
                minlength: 2,                
            },
            message: {
                required: true,
                minlength: 2
            }
        },
        messages: {
            subject: {
                required: 'Newsletter field cannot be blank!',
            },
            message: {
                required: 'Newsletter message field cannot be blank!',
            }
        },
        errorPlacement: function(error, element) {
            var next = element;
            if (element.hasClass('file'))
                jQuery(error).insertAfter(jQuery(element).parents(".file-input"));
            else if (element.hasClass('chosen-select'))
                jQuery(error).insertAfter(jQuery(element).siblings(".chosen-container"));
            else
                jQuery(error).insertAfter(jQuery(element).parent());
        },
    });
});
