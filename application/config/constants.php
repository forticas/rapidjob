<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('DOCROOT', realpath(dirname(__FILE__) . '/../') . "/"); // /var/www/html/rapidjob/application/

/*
  User defined Constants
 */

if (ENVIRONMENT == 'production') {
    define('DB_SERVER', 'localhost');
    define('DB_USER', 'forticas');
    define('DB_PASS', 'Forticas_18');
    define('DB_DATABASE', 'rapidjob');
    define('DOMAIN_URL', 'https://onlinecoin.io/');
    define('BASE_URL', DOMAIN_URL . '');
    define('PHP_PATH', '/usr/bin/php7.2');
} else {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    define('DB_SERVER', 'localhost');
    define('DB_USER', 'forticas');
    define('DB_PASS', 'Forticas_18');
    define('DB_DATABASE', 'rapidjob');
    define('DOMAIN_URL', 'https://onlinecoin.io/');
    define('BASE_URL', DOMAIN_URL . '');
    define('PHP_PATH', '/usr/bin/php7.2');
}



//Database Table name Constants
define("TBL_PREFIX", "rj_");
define("TBL_ADMINS", TBL_PREFIX . "admins");
define("TBL_COMPANY_DETAILS", TBL_PREFIX . "company_details");
define("TBL_COMPANY_FAVOURITES", TBL_PREFIX . "company_favourites");
define("TBL_EDUCATIONS", TBL_PREFIX . "educations");
define("TBL_ERROR_LOD", TBL_PREFIX . "error_logs");
define("TBL_NEWSLETTERS", TBL_PREFIX . "newsletters");
define("TBL_SETTINGS", TBL_PREFIX . "settings");
define("TBL_USERS", TBL_PREFIX . "users");
define("TBL_USER_APPLICATIONS", TBL_PREFIX . "user_applications");
define("TBL_USER_CVS", TBL_PREFIX . "user_cvs");
define("TBL_USER_DETAILS", TBL_PREFIX . "user_details");
define("TBL_USER_DOCUMENT_FILES", TBL_PREFIX . "user_document_files");
define("TBL_USER_EXPERIANCES", TBL_PREFIX . "user_experiances");
define("TBL_USER_FAVOURITES", TBL_PREFIX . "user_favourites");
define("TBL_USER_HOBBIES", TBL_PREFIX . "user_hobbies");
define("TBL_USER_LANGUAGES", TBL_PREFIX . "user_languages");
define("TBL_USER_SKILLS", TBL_PREFIX . "user_skills");
define("TBL_USER_TRAININGS", TBL_PREFIX . "user_trainings");
define("TBL_WORK_ACTIVITY_AREA", TBL_PREFIX . "work_activity_areas");
define("TBL_WORK_JOB", TBL_PREFIX . "work_jobs");
define("TBL_WORK_PLACES", TBL_PREFIX . "work_places");
define("TBL_INVITE_FRIENDS", TBL_PREFIX . "invite_friends");
define("TBL_USER_PAYMENTS", TBL_PREFIX . "user_payments");
define("TBL_NEWS", TBL_PREFIX . "news");
define("TBL_USER_ALL_DOCUMENT_FILES", TBL_PREFIX . "user_all_document_files");
define("TBL_NATIONALITIES", TBL_PREFIX . "nationalities");


//Path Constants
define("ASSETS_PATH", DOMAIN_URL . 'assets/');
define("HOME_PATH", BASE_URL . 'home');
define("USERS_PATH", BASE_URL . 'users');
define("PROFESSIONAL_PATH", BASE_URL . 'professional');
define("CRON_PATH", BASE_URL . 'myCron');


//Folder Constant
define("UPLOAD_FILE_FOLDER", 'uploads');
define("DEFAULT_IMAGE_FOLDER", 'images');
define("UPLOAD_USERS_FOLDER", 'users');
define("UPLOAD_DOCUMENTS_FOLDER", 'documents');
define("UPLOAD_NEWS_FOLDER", 'news');

//path constant
define("UPLOAD_REL_PATH", '../' . UPLOAD_FILE_FOLDER);
define("UPLOAD_ABS_PATH", DOMAIN_URL . 'uploads/');

//Email Constants

define('EMAIL_HOST', 'smtp.gmail.com');
define('EMAIL_USER', 'tanvimehta405@gmail.com');
define('EMAIL_PASS', 'imobdev1234');
define("EMAIL_PORT", "587");
define('EMAIL_FROM', 'no-reply@emaildomain.com');
define('MAIL_CERTI', "tls");
define('SWIFT_MAILER_PATH', 'swiftmailer/lib/swift_required.php');


// my testing mail
define('M1', 'test@gmail.com');
define('NM', 'Vin dev');
define('M2', 'test@gmail.com');
define('RJ_EMAIL', 'contact@rapid-job.fr');

//Other contains (AWS S3 Bucket, SMS Gateway, Google MAP KEY, etc.)
define('APP_NAME', 'Rapid Job');
define('APP_SHORT_NAME', 'RJ');
define('APP_VERSION', '0.1.2');
define('DATE_FORMAT', 'Y-m-d H:i:s');
define('DATE_FORMAT_JS', "dd MMMM yyyy");
define('DATE_FORMAT_JS_LIST', "dd MMMM yyyy, hh:mm TT");
define("IMAGE_MANIPULATION_URL", DOMAIN_URL . "image.php?");
define("DEFAULT_IMAGE_NAME", 'default.png');
define("DEFAULT_COMPANY_IMAGE_NAME", 'recruitor_placeholder.png'); //placeholder_icon.png
define("DEFAULT_COVER_LATTER_IMAGE_NAME", 'doc_image.png');
define("DEFAULT_USER_IMAGE_ABS", UPLOAD_ABS_PATH . 'default.png');
define("DEFAULT_USER_IMAGE_REL", UPLOAD_FILE_FOLDER . '/default.png');
define("DEFAULT_FULL_STAR", ASSETS_PATH . DEFAULT_IMAGE_FOLDER . '/star_full.png');
define("DEFAULT_HALF_STAR", ASSETS_PATH . DEFAULT_IMAGE_FOLDER . '/star_half.png');
define("DEFAULT_FAVICON_ICON", ASSETS_PATH . DEFAULT_IMAGE_FOLDER . '/favicon.ico');


//Default Language setting
define('DEFAULT_LANG', 'fr'); // en,fr
//Default Points for invite friends
define('DEFAULT_INVITATION_POINTS', 100);
define('DEFAULT_MAXIMUM_CV_SIZE_LIMIT_PHP', 2); //MB
define('DEFAULT_MAXIMUM_CV_SIZE_LIMIT_JS', 2048576); //MB
define('DEFAULT_MAXIMUM_COMPANY_SELECT', 3);
define('DEFAULT_IMAGE_SIZE_LIMIT_MB', 2); // 2MB
define('DEFAULT_IMAGE_SIZE_LIMIT', 2048576); // 2MB
define('DEFAULT_RESUME_LIMIT', 5); // SHOW 5 RESUME IN RECRITER RESUME DATABASE PAGE
define('DEFAULT_SEARCH_COMPANY_LIMIT', 5); // SHOW 5 COMPANY IN SEARCH RESULT PAGE
define('DEFAULT_PAYMENT_RATE_PER_EMAIL', 3); // DEFAULT 3 CENT PER SENDING MAIL TO COMPANY BY EMAIL
define('DEFAULT_PAYMENT_RATE_PER_POST', 3); // DEFAULT 3 CENT PER SENDING MAIL TO COMPANY BY POST
define('DEFAULT_RECRUITER_SUBSCRITION_PLAN_PRICE', 50); // DEFAULT 50 CENT 
define('DEFAULT_RECRUITER_SUBSCRITION_PLAN_DURATION', 6); // DEFAULT 50 CENT 
define('DEFAULT_SOCIAL_REASON_TEXT_LIMIT', 50); // DEFAULT 50 CENT 
define('DEFAULT_USER_NAME_TEXT_LIMIT', 10); // DEFAULT 50 CENT 
define('DEFAULT_NEWS_TITLE_TEXT_LIMIT', 20);
define('DEFAULT_NEWS_DESCRIPTION_TEXT_LIMIT', 80);
define('DEFAULT_SHOW_COMPANY_LIMIT', 100); // SHOW 100 COMPANY IN SEND APPLICATION LIST

//LinkedIn  details
//define("FB_APP_ID", "1300050463401599");
define("FB_APP_ID", "242234089531382");
define('LINKEDIN_APP', '78ev6kvpru6mm2'); // need to cange in view/home.php
define("GOOGLE_API_KEY", "AIzaSyCY9Es7HdaX-EYVFzw3hQUO6XRMSCU9iKI");

// paypal credentials
define('PAYPAL_CLIENT_ID', 'ASnc7fkIGMRH1U-DoFvb4yAqWlpELNEli0K6_upkoWqEKDJ8Iyh-wIMDafTRbtAO4Dlo_GJtiVS1h5Zv');
define('PAYPAL_CLIENT_SECRET', 'EJIOSm9OUUmE8XbU9CoXpNY0iIkDbHB89mJmFN8innLCRpRgC5NYlhgbwR46j_qknqz4rgnPP9-GvfIp');
define('PAYPAL_MODE', 'sandbox');


// systempay credentials
define('SP_SHOP_ID', 28779738);
define('SP_DEBUG', TRUE); // true
define('SP_MODE', 'TEST'); // PRODUCTION

if (SP_MODE == 'PRODUCTION') {
    define('SP_CERTIFICATION_KEY', 3333333333333333);
} else {
    define('SP_CERTIFICATION_KEY', 6194724578198741);
}

define('SP_CONFIG', serialize(array(
    'debug' => SP_DEBUG,
//    'vads_contrib' => 'Pack_PSP_1.0f',
    'vads_site_id' => SP_SHOP_ID,
    'key' => SP_CERTIFICATION_KEY,
    'vads_ctx_mode' => SP_MODE,
    'vads_version' => "V2",
    'vads_language' => "fr",
    'vads_currency' => 978,
    'vads_page_action' => "PAYMENT",
    'vads_action_mode' => "INTERACTIVE",
    'vads_payment_config' => "SINGLE",
    'vads_url_return' => USERS_PATH . '/payment_response',
    'vads_return_mode' => "GET",
    'vads_redirect_success_timeout' => 1,
    'vads_redirect_success_message' => "Redirection vers la boutique dans quelques instants",
    'vads_redirect_error_timeout' => 1,
    'vads_redirect_error_message' => "Redirection vers la boutique dans quelques instants"
        ))
);


define("UPLOAD_CMS_FOLDER", 'managecms');
define("TBL_TESTIMONIAL", TBL_PREFIX . "cms_testimonials");
define("TBL_FAQ",TBL_PREFIX."cms_faq");
define("TBL_CMS", TBL_PREFIX . "manage_cms");