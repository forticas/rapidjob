<?php

require_once "vendor/paypal/autoload.php"; //include PayPal SDK

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payment;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Transaction;
use PayPal\Api\ItemList;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use PayPal\Api\CreditCard;
use PayPal\Api\CreditCardToken;
use PayPal\Api\FundingInstrument;
//create plan
use PayPal\Api\Plan;
use PayPal\Api\PaymentDefinition;
use PayPal\Api\MerchantPreferences;
use PayPal\Api\Currency;
//use PayPal\Api\ChargeModel;
//update plan
use PayPal\Api\PatchRequest;
use PayPal\Api\Patch;
use PayPal\Common\PayPalModel;
//create profile
use PayPal\Api\Agreement;
use PayPal\Api\ShippingAddress;
use PayPal\Api\PayerInfo;
use PayPal\Api\Details;
use PayPal\Api\Item;

if ($this->user_type == 1) {
    define('SUCCESS_URL', USERS_PATH . '/paypal_success');
    define('FAILURE_URL', USERS_PATH . '/paypal_failure');
} else {
    define('SUCCESS_URL', PROFESSIONAL_PATH . '/paypal_success');
    define('FAILURE_URL', PROFESSIONAL_PATH . '/paypal_failure');
}

class CdoPay {

    private $CLIENT_ID = PAYPAL_CLIENT_ID; //'AdSP9uQ3geRKWkATuC5Aejod16DsyFWsU8rs1OpoiV0lHW5PK38YOQYhInM-VN5Ad5VXOZA5fMwVdazf'; //your PayPal client ID
    private $CLIENT_SECRET = PAYPAL_CLIENT_SECRET; //'EJMMWwPk3NHJUji0KVKHz3wGD2HL0rVDdWKzdnL1pPkyXDqPo3noKMzFQQ2_Nk1dkJKfGG0ohRzG8bmI'; //PayPal Secret

    public function payment($fnm, $lnm, $cardNumber, $cardType, $month, $year, $cvv, $paymentAMT) {
        try {
            //echo "fnm=".$fnm."--lnm=".$lnm.'----cardNumber'.$cardNumber.'cardType='.$cardType.'$month='.$month.'$year='.$year.'$cvv='.$cvv;
            $cardType = strtolower($cardType);
            $sdkConfig = array(
                "mode" => PAYPAL_MODE//"sandbox"
            );
            $cred = new OAuthTokenCredential($this->CLIENT_ID, $this->CLIENT_SECRET, $sdkConfig);

            $apiContext = new ApiContext($cred, 'Request' . time());
            $apiContext->setConfig($sdkConfig);


            $card = new CreditCard();

            $card->setType($cardType);
            $card->setNumber($cardNumber);
            $card->setExpireMonth($month);
            $card->setExpireYear($year);
            $card->setCvv2($cvv);
            $card->setFirstName($fnm);
            $card->setLastName($lnm);


            $card->create($apiContext);

            $creditCardToken = new CreditCardToken();
            //echo "tojen:".$card->getId();
            $creditCardToken->setCreditCardId($card->getId());

            $fundingInstrument = new FundingInstrument();
            $fundingInstrument->setCreditCardToken($creditCardToken);

            $payer = new Payer();
            $payer->setPaymentMethod("credit_card");
            $payer->setFundingInstruments(array($fundingInstrument));


            $amount = new Amount();
            $amount->setCurrency("USD");
            $amount->setTotal($paymentAMT);

            $transaction = new Transaction();
            $transaction->setAmount($amount);
            $transaction->setDescription("creating a direct payment with credit card");

            $payment = new Payment();
            $payment->setIntent("sale");
            $payment->setPayer($payer);
            $payment->setTransactions(array($transaction));




            $response = $payment->create($apiContext);
            //print_r($response);exit;
        } catch (Exception $pce) {
            // Don't spit out errors or use "exit" like this in production code
            echo "<pre>";
            print_r($pce);
            exit;
            //echo "come";exit;
            header('location:' . A_REFERAL_URL . '/failure');
            exit;
            //$pay_error= json_decode($pce->getData());
        }

        //print_r($response);exit;

        if ($response->state == 'approved') {
            return $response;
        } else {
            return false;
        }


//        $response->state;
//        $response->payer->funding_instruments->credit_card->number.' card';
//        if($response->state=='approved')
//        {
//            $transfer_id=$response->id;
//            $card_no=$response->payer->funding_instruments[0]->credit_card->number;
//            $card_type=$response->payer->funding_instruments[0]->credit_card->type;
//            $card_month=$response->payer->funding_instruments[0]->credit_card->expire_month;
//            $card_year=$response->payer->funding_instruments[0]->credit_card->expire_year;
//            $card_f_name=$response->payer->funding_instruments[0]->credit_card->first_name;
//            $card_l_name=$response->payer->funding_instruments[0]->credit_card->last_name;
//            //$total_amount = 0;
//        }
    }

    // for recurring payment
    public function recurringPayment($name, $lname, $cardNumber, $cardType, $month, $year, $cvv, $email) {


        $apiContext = new ApiContext(
                new OAuthTokenCredential(
                $this->CLIENT_ID, $this->CLIENT_SECRET
                )
        );

        // Comment this line out and uncomment the PP_CONFIG_PATH
        // 'define' block if you want to use static file
        // based configuration

        $apiContext->setConfig(
                array(
                    'mode' => 'sandbox',
                )
        );

        // Create a new instance of Plan object
        $plan = new Plan();

        $plan->setName('Additional user activation plan')
                ->setDescription('After creating 2 child account additional charge for $0.99 for each user per year')
                ->setType('fixed');

        // # Payment definitions for this billing plan.
        $paymentDefinition = new PaymentDefinition();

        $paymentDefinition->setName('Upgrade account Payments')
                ->setType('REGULAR')
                ->setFrequency('Year')
                ->setFrequencyInterval("1")
                ->setCycles("9999")
                ->setAmount(new Currency(array('value' => 0.99, 'currency' => 'USD')));

        $merchantPreferences = new MerchantPreferences();

        $merchantPreferences->setReturnUrl(SUCCESS_URL)
                ->setCancelUrl(FAILURE_URL)
                ->setAutoBillAmount("yes")
                ->setInitialFailAmountAction("CONTINUE")
                ->setMaxFailAttempts("10"); //0 
        //->setSetupFee(new Currency(array('value' => 0.00, 'currency' => 'USD')));


        $plan->setPaymentDefinitions(array($paymentDefinition));
        $plan->setMerchantPreferences($merchantPreferences);

        try {
            $createdPlan = $plan->create($apiContext);
        } catch (Exception $ex) {
            header('location:' . A_REFERAL_URL . '/failure');
        }

        try {
            $patch = new Patch();

            $value = new PayPalModel('{
               "state":"ACTIVE"
             }');

            $patch->setOp('replace')
                    ->setPath('/')
                    ->setValue($value);
            $patchRequest = new PatchRequest();
            $patchRequest->addPatch($patch);

            $createdPlan->update($patchRequest, $apiContext);

            $createdPlan = Plan::get($createdPlan->getId(), $apiContext);
        } catch (Exception $ex) {
            //return $ex;
            header('location:' . A_REFERAL_URL . '/failure');
        }

        $agreement = new Agreement();
        $day = date("d") + 1;
        $date = date("Y-m");
        $agreement->setName('Payment')
                ->setDescription('upgrade plan - payment with credit Card')
                ->setStartDate($date . '-' . $day . 'T' . date('H:i:s') . 'Z');

        // Add Plan ID
        // Please note that the plan Id should be only set in this case.
        $plan = new Plan();
        $plan->setId($createdPlan->getId());
        $agreement->setPlan($plan);

        // Add Payer
        $payer = new Payer();
        $payer->setPaymentMethod('credit_card')
                ->setPayerInfo(new PayerInfo(array('email' => $email)));

        // Add Credit Card to Funding Instruments
        $creditCard = new CreditCard();
        $creditCard->setType($cardType)
                ->setNumber($cardNumber)
                ->setExpireMonth($month)
                ->setExpireYear($year)
                ->setCvv2($cvv)
                ->setFirstName($name)
                ->setLastName($lname);

        $fundingInstrument = new FundingInstrument();
        $fundingInstrument->setCreditCard($creditCard);
        $payer->setFundingInstruments(array($fundingInstrument));
        //Add Payer to Agreement
        $agreement->setPayer($payer);

        try {
            // Please note that as the agreement has not yet activated, we wont be receiving the ID just yet.
            $agreement = $agreement->create($apiContext);
        } catch (Exception $ex) {
            //return $ex;
            header('location:' . A_REFERAL_URL . '/failure');
        }

        $returnValur = array(
            'planId' => $createdPlan->getId(),
            'profileId' => $agreement->getId()
        );

        return $returnValur;
    }

    public function payment_using_web($amountToCut) {

        $sdkConfig = array(
            "mode" => PAYPAL_MODE//"sandbox"
        );
        $cred = new OAuthTokenCredential($this->CLIENT_ID, $this->CLIENT_SECRET, $sdkConfig);

        $apiContext = new ApiContext($cred, 'Request' . time());
        $apiContext->setConfig($sdkConfig);

        $payer = new Payer();
        $payer->setPaymentMethod("paypal");

        // ### Itemized information
        // (Optional) Lets you specify item wise
        // information
        $item1 = new Item();
        $item1->setName('Payment')
                ->setCurrency('USD')
                ->setQuantity(1)
                ->setSku('123') // Similar to `item_number` in Classic API
                ->setPrice($amountToCut);
        $itemList = new ItemList();
        $itemList->setItems(array($item1));


        // ### Amount
        // Lets you specify a payment amount.
        // You can also specify additional details
        // such as shipping, tax.
        $amount = new Amount();
        $amount->setCurrency("USD")
                ->setTotal($amountToCut)
                ->setDetails('');

        // ### Transaction
        // A transaction defines the contract of a
        // payment - what is the payment for and who
        // is fulfilling it. 
        $transaction = new Transaction();
        $transaction->setAmount($amount)
                ->setItemList($itemList)
                ->setDescription("My App CHarges")
                ->setInvoiceNumber(uniqid());

        // ### Redirect urls
        // Set the urls that the buyer must be redirected to after 
        // payment approval/ cancellation.
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl(SUCCESS_URL)
                ->setCancelUrl(FAILURE_URL);

        // ### Payment
        // A Payment Resource; create one using
        // the above types and intent set to 'sale'
        $payment = new Payment();
        $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions(array($transaction));


        // For Sample Purposes Only.
        $request = clone $payment;

        // ### Create Payment
        // Create a payment by calling the 'create' method
        // passing it a valid apiContext.
        // (See bootstrap.php for more on `ApiContext`)
        // The return object contains the state and the
        // url to which the buyer must be redirected to
        // for payment approval
        try {
            $payment->create($apiContext);
        } catch (Exception $ex) {
            // NOTE: PLEASE DO NOT USE RESULTPRINTER CLASS IN YOUR ORIGINAL CODE. FOR SAMPLE ONLY
            echo "<pre>";
            print_r($ex);
            exit;
            header('location:' . FAILURE_URL);
            exit(1);
        }

        // ### Get redirect url
        // The API response provides the url that you must redirect
        // the buyer to. Retrieve the url from the $payment->getApprovalLink()
        // method
        $approvalUrl = $payment->getApprovalLink();

        return $approvalUrl;
    }

}
