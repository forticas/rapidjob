<?php

if (!function_exists('footer_details')) {
    
    function footer_details($language) {
        $ci = & get_instance();
        $ci->load->model('Common_model');
        
        $column_slider = array(
            'cms_id',            
            'cms_footer_contactus',
            'cms_footer_facebook_link',
            'cms_footer_twitter_link',
            'cms_footer_google_link',
            'cms_footer_linkedin_link',            
            'cms_home_banner_content_' . $language . ' as banner_content',
            'cms_hm_create_account_text_' . $language . ' as create_account_text',
            'cms_home_news_title_' . $language . ' as news_title',
            'cms_home_subscribe_text_' . $language . ' as subscribe_text',
            'cms_footer_copyright_' . $language. ' as footer_copyright',
            
        );
        
        $get_data = $ci ->Common_model->get_single_row(TBL_CMS,$column_slider,array('cms_id' => 1));
        
        return $get_data;
    }

}

?>