<?php

/**
 * 
 * This model use for import recruiters
 * 
 * @author Nitinkumar Vaghani
 * Modified Date :- 2017-05-09
 * 
 */
class Importrecruiters_model extends MY_Model {

    /**
     * Base table name
     * @var string 
     */
    protected $recruiter_table_name;

    /**
     * Base table name
     * @var string 
     */
    protected $recruiter_details_table;

    /**
     *
     * Most use table filed list
     * 
     * @var string 
     */
    protected $activity_table;

    /**
     *
     * Most use table filed list
     * 
     * @var string 
     */
    protected $failed_import_recruiter_table_name;

    /**
     *
     * Most use table filed list
     * 
     * @var string 
     */
    protected $recruiter_table_filed;

    /**
     *
     * Most use table filed list
     * 
     * @var string 
     */
    protected $recruiter_details_table_filed;

    /**
     *
     * Most use table filed list
     * 
     * @var string 
     */
    protected $failed_import_recruiter_table_filed;

    /**
     *
     * Most use table filed list
     * 
     * @var string 
     */
    protected $activity_table_filed;
    public $log_file;

    public function __construct() {
        parent::__construct();

        $this->recruiter_table_name = TBL_USERS;
        $this->recruiter_table_filed = array("*");

        $this->recruiter_details_table = TBL_COMPANY_DETAILS;
        $this->recruiter_details_table_filed = array("*");

        $this->activity_table = TBL_WORK_ACTIVITY_AREA;
        $this->activity_table_filed = array("*");

        $this->failed_import_recruiter_table_name = TBL_FAILED_IMPORT_RECRUITERS;
        $this->failed_import_recruiter_table_filed = array("*");

        $this->log_file = DOCROOT . 'logs/log_model_store_recruiters_data_' . date('d-m-Y') . '.txt';
    }

    /**
     * 
     * This function use for store imported recruiter
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     * @param type $request_data
     * @return boolean
     */
    public function store_imported_recruiter($request_data) {
        $response = false;
        if (!empty($request_data) && count($request_data) > 0) {

            $this->db->trans_start();
            $recruiter_data = array(
                'u_first_name' => $request_data['u_first_name'],
                'u_email' => $request_data['u_email'],
                'u_type' => 2,
                'u_created_date' => time()
            );

            $recruiter_id = $this->insert($this->recruiter_table_name, $recruiter_data);
            if ($recruiter_id > 0) {
                // check activity is exist or not, if not then insert new
                $activity_area_id = $this->manage_activity($request_data['cd_activity_area']);
                $recruiter_details_data = array(
                    'cd_user_id' => $recruiter_id,
                    'cd_address' => $request_data['cd_address'],
                    'cd_postal_code' => $request_data['cd_postal_code'],
                    'cd_city' => $request_data['cd_city'],
                    'cd_created_date' => time()
                );

                if (!empty($activity_area_id) && is_numeric($activity_area_id)) {
                    $recruiter_details_data['cd_activity_area'] = $activity_area_id;
                }

                if (!empty($request_data['cd_phone'])) {
                    $recruiter_details_data['cd_phone'] = $request_data['cd_phone'];
                }

                if (!empty($request_data['cd_alternate_phone'])) {
                    $recruiter_details_data['cd_alternate_phone'] = $request_data['cd_alternate_phone'];
                }

                if (!empty($request_data['cd_fax'])) {
                    $recruiter_details_data['cd_fax'] = $request_data['cd_fax'];
                }
                if (!empty($request_data['cd_naf'])) {
                    $recruiter_details_data['cd_agreement'] = $request_data['cd_naf'];
                }
                if (!empty($request_data['cd_siret'])) {
                    $recruiter_details_data['cd_identification_code'] = $request_data['cd_siret'];
                }
                if (!empty($request_data['cd_effectif'])) {
                    $recruiter_details_data['cd_effectif'] = $request_data['cd_effectif'];
                }

                $recruiter_detail_id = $this->insert($this->recruiter_details_table, $recruiter_details_data);

                if ($recruiter_detail_id > 0) {
                    $response = TRUE;
                }
            }

            $this->db->trans_complete();
            if ($this->db->trans_status() === FALSE || !$recruiter_id || !$recruiter_detail_id) {
                file_put_contents($this->log_file, "\n\n ======== thisdb-trans_status false : " . json_encode($this->db->trans_status()) . "===== \n\n", FILE_APPEND | LOCK_EX);
                $this->db->trans_rollback();
            } else {
                $this->db->trans_commit();
            }
        }

        return $response;
    }

    /**
     * 
     * This function use for get activity id by name or insert new activity
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     * @param type $cd_activity_area
     * @return type
     */
    public function manage_activity($cd_activity_area) {
        $response_id = '';
        if (!empty($cd_activity_area)) {

            // check activity exist or not
            $activity_query = ' SELECT 
                                    `wa_id` 
                                FROM 
                                    ' . $this->activity_table . '
                                WHERE 
                                    wa_status != 9 
                                AND 
                                    (LOWER(wa_name_en)="' . strtolower($cd_activity_area) . '" OR LOWER(wa_name_fr)="' . strtolower($cd_activity_area) . '") ';

            $activity_data = $this->get_single_row_by_query($activity_query);

            if (!empty($activity_data) && isset($activity_data['wa_id']) && is_numeric($activity_data['wa_id'])) {
                $response_id = $activity_data['wa_id'];
            } else {
                // insert new activity
                $activity_area_data = array(
                    'wa_name_en' => $cd_activity_area,
                    'wa_name_fr' => $cd_activity_area,
                    'wa_created_date' => time()
                );
                $response_id = $this->insert($this->activity_table, $activity_area_data);
            }
        }

        return $response_id;
    }

    /**
     * 
     * This function use for store failed import recruiter
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     * @param type $request_data
     * @return boolean
     */
    public function store_failed_import_recruiter($request_data) {
        if (!empty($request_data) && count($request_data) > 0) {
            $recruiter_details_data = array();

            if (!empty($request_data['u_first_name'])) {
                $recruiter_details_data['fir_first_name'] = $request_data['u_first_name'];
            }

            if (!empty($request_data['cd_address'])) {
                $recruiter_details_data['fir_address'] = $request_data['cd_address'];
            }
            if (!empty($request_data['cd_postal_code'])) {
                $recruiter_details_data['fir_postal_code'] = $request_data['cd_postal_code'];
            }
            if (!empty($request_data['cd_city'])) {
                $recruiter_details_data['fir_city'] = $request_data['cd_city'];
            }
            if (!empty($request_data['cd_phone'])) {
                $recruiter_details_data['fir_phone'] = $request_data['cd_phone'];
            }
            if (!empty($request_data['cd_alternate_phone'])) {
                $recruiter_details_data['fir_alternate_phone'] = $request_data['cd_alternate_phone'];
            }
            if (!empty($request_data['cd_fax'])) {
                $recruiter_details_data['fir_fax'] = $request_data['cd_fax'];
            }
            if (!empty($request_data['cd_activity_area'])) {
                $recruiter_details_data['fir_activity_area'] = $request_data['cd_activity_area'];
            }
            if (!empty($request_data['cd_naf'])) {
                $recruiter_details_data['fir_naf'] = $request_data['cd_naf'];
            }
            if (!empty($request_data['cd_siret'])) {
                $recruiter_details_data['fir_siret'] = $request_data['cd_siret'];
            }
            if (!empty($request_data['cd_effectif'])) {
                $recruiter_details_data['fir_effectif'] = $request_data['cd_effectif'];
            }
            if (!empty($request_data['u_email'])) {
                $recruiter_details_data['fir_email'] = $request_data['u_email'];
            }
            if (!empty($request_data['error_message'])) {
                $recruiter_details_data['fir_message'] = $request_data['error_message'];
            }

            $recruiter_details_data['fir_created_date'] = time();

            $this->insert($this->failed_import_recruiter_table_name, $recruiter_details_data);
        }
    }

}
