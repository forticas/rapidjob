<?php

/**
 * 
 * This model use for do user related all database activity
 * 
 * @author Nitinkumar Vaghani
 * Modified Date :- 2017-05-09
 * 
 */
class Postapplication_model extends MY_Model {

    /**
     * Base table name
     * @var string 
     */
    protected $table_name;

    /**
     *
     * Most use table filed list
     * 
     * @var string 
     */
    protected $table_filed;

    public function __construct() {
        parent::__construct();

        $this->table_name = TBL_USER_APPLICATIONS;
        $this->table_filed = "ua_id,ua_company_id,ua_sent_post_company_id";
    }

    /**
     * 
     * This function is use for get all data process it and send as json formate
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-30
     * 
     */
    public function get_all_data() {

        $return_data = array(
            "data" => array(),
            "total" => 0
        );

        $page = !empty($this->input->get_post("start")) ? $this->input->get_post("start") : 0;
        $rows = !empty($this->input->get_post("length")) ? $this->input->get_post("length") : 10;

        $sidx = !empty($this->input->get_post("order")[0]['column']) ? $this->input->get_post("order")[0]['column'] : 0;
        $sord = !empty($this->input->get_post("order")[0]['dir']) ? $this->input->get_post("order")[0]['dir'] : 'ASC';

        $search = !empty($this->input->get_post("name_filter")) ? $this->input->get_post("name_filter") : '';
        $type = !empty($this->input->get_post("application_type")) ? $this->input->get_post("application_type") : 'pending';

        if ($sidx == 0) {
            $sidx = "candidate_name";
        } else if ($sidx == 1) {
            $sidx = "recruiter_name";
        } else if ($sidx == 3) {
            $sidx = 'ua_created_date';
        }

        $where = " candidate_application.ua_send_by=2 AND candidate_application.ua_status !=9 AND recruiter.u_id is not null";

        if ($type == 'delivered') {
            $where_application = " FIND_IN_SET(recruiter.u_id, candidate_application.ua_sent_post_company_id) and candidate_application.ua_status != 9 and candidate_application.ua_sent_post_company_id is not null ";
        } else {
            $where_application = " FIND_IN_SET(recruiter.u_id, candidate_application.ua_company_id) and candidate_application.ua_status != 9 and candidate_application.ua_company_id is not null ";
        }

        $order_by = $sidx . ' ' . $sord;
        $limit = $page . ',' . $rows;

        if (!empty($search)) {
            $where .= ' AND (
                                CONCAT(LOWER(recruiter.u_first_name)," ",LOWER(recruiter.u_last_name)) LIKE "%' . strtolower($search) . '%" 
                            OR 
                                CONCAT(LOWER(candidate.u_first_name)," ",LOWER(candidate.u_last_name)) LIKE "%' . strtolower($search) . '%" 
                            )';
        }

        $group_by = '';

        $columns = " CONCAT(if(recruiter.u_first_name is null ,'',recruiter.u_first_name),' ',if(recruiter.u_last_name is null ,'',recruiter.u_last_name)) as recruiter_name,
                         CONCAT(if(candidate.u_first_name is null ,'',candidate.u_first_name),' ',if(candidate.u_last_name is null ,'',candidate.u_last_name)) as candidate_name,
                        `recruiter`.`u_id` as `recruiter_id`, 
                        `recruiter`.`u_type` as `recruiter_type`, 
                        `recruiter`.`u_email` as `recruiter_email`, 
                        `recruiter`.`u_image` as `recruiter_image`, 
                        `recruiter`.`u_status` as `recruiter_status`, 
                        `candidate`.`u_id` as `candidate_id`, 
                        `candidate`.`u_type` as `candidate_type`, 
                        `candidate`.`u_email` as `candidate_email`, 
                        `candidate`.`u_image` as `candidate_image`, 
                        `candidate`.`u_status` as `candidate_status`,
                        cd_id,cd_social_reason,cd_phone,cd_alternate_phone,
                        cd_fax,cd_address,cd_city,cd_state,cd_country,cd_postal_code,
                        cd_agreement,cd_identification_code,cd_effectif,cd_is_prime,
                        ua_id,ua_user_id,ua_company_id,ua_send_by,ua_status,udf_id,
                        udf_user_id,udf_message,udf_type,udf_cv_file_name,udf_rapidjob_cv_file_name,
                        udf_cover_latter_file,ud_id,ua_created_date,ua_sent_post_company_id";

        $application_query = "SELECT 
                                " . $columns . " 
                            FROM 
                                " . TBL_USERS . " as `recruiter` 
                            RIGHT JOIN 
                                " . TBL_COMPANY_DETAILS . " as `recruiter_details` 
                            ON 
                                `recruiter_details`.`cd_user_id` = `recruiter`.`u_id` and `recruiter_details`.`cd_status` != 9 
                            RIGHT JOIN 
                                " . TBL_USER_APPLICATIONS . " as `candidate_application` 
                            ON 
                                " . $where_application . "
                            RIGHT JOIN 
                                " . TBL_USERS . " as `candidate` 
                            ON 
                                `candidate_application`.`ua_user_id` = `candidate`.`u_id` and `candidate`.`u_status` != 9 
                            RIGHT JOIN 
                                " . TBL_USER_DETAILS . " as `candidate_details` 
                            ON 
                                `candidate_details`.`ud_user_id` = `candidate`.`u_id` and `candidate_details`.`ud_status` != 9 
                            RIGHT JOIN 
                                " . TBL_USER_DOCUMENT_FILES . " as `candidate_document_files` 
                            ON 
                                `candidate_document_files`.`udf_user_id` = `candidate`.`u_id` and `candidate_document_files`.`udf_status` != 9 
                            WHERE 
                                    " . $where . " ";

        $return_data['total'] = $this->Common_model->get_count_by_query($application_query);

        if (!empty($return_data['total']) && $return_data['total'] > 0) {
            $application_query.=" ORDER BY 
                                " . $order_by . " 
                              LIMIT 
                                " . $limit;

            $userData = $this->Common_model->get_all_rows_by_query($application_query);
            
            $resultArr = array();

            if (!empty($userData) && is_array($userData) && count($userData) > 0) {
                foreach ($userData as $key => $value) {
                    $address = '';
                    if (!empty($value['cd_address'])) {
                        $address.=$value['cd_address'];
                    }
                    if (!empty($value['cd_city'])) {
                        $address.=",<br/>" . $value['cd_city'];
                    }
                    if (!empty($value['cd_state'])) {
                        $address.=", " . $value['cd_state'];
                    }
                    if (!empty($value['cd_postal_code'])) {
                        $address.=" - " . $value['cd_postal_code'];
                    }
                    if (!empty($value['cd_country'])) {
                        $address.=",<br>" . $value['cd_country'];
                    }

                    if (!empty($value['candidate_name'])) {
                        $resultArr[$key]['candidate_name'] = "<a href='" . USERS_PATH . "/view/" . $value['candidate_id'] . "'>" . get_slash_formatted_text(ucfirst($value['candidate_name'])) . "</a>";
                    } else {
                        $resultArr[$key]['candidate_name'] = "<a href='" . USERS_PATH . "/view/" . $value['candidate_id'] . "'> - - - - </a>";
                    }

                    if (!empty($value['recruiter_name'])) {
                        $resultArr[$key]['recruiter_name'] = "<a href='" . USERS_PATH . "/view/" . $value['recruiter_id'] . "'>" . get_slash_formatted_text(ucfirst($value['recruiter_name'])) . "</a>";
                    } else {
                        $resultArr[$key]['recruiter_name'] = "<a href='" . USERS_PATH . "/view/" . $value['recruiter_id'] . "'> - - - - </a>";
                    }

                    $resultArr[$key]['company_address'] = get_slash_formatted_text($address);
                    $resultArr[$key]['created_at'] = date(DATE_FORMAT, (int) ($value['ua_created_date'] + $this->timestamp));

                    if ($type == 'delivered') {
                        $resultArr[$key]['status'] = '<span class="label label-success" style="padding: 4px 6px;font-size:14px;">Delivered</span>';
                    } else {
                        $resultArr[$key]['status'] = '<select class="form-control a_status status_change" data-id=' . $value['ua_id'] . '  recruiter-id="' . $value['recruiter_id'] . '" candidate-id="' . $value['candidate_id'] . '">
                                                    <option value="1">Sent</option>
                                                    <option value="2" selected="selected">Pending</option>
                                                </select>';
                    }


                    $user_cv_file = $user_cover_file = '';

                    if ($value['udf_type'] == 1):
                        $cv_file = UPLOAD_REL_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $value['udf_id'] . '/' . $value['udf_cv_file_name'];
                        if (!empty($value['udf_cv_file_name']) && file_exists($cv_file)):
                            $cv_file = UPLOAD_ABS_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $value['udf_id'] . '/' . $value['udf_cv_file_name'];
                            $user_cv_file = '<a target="_blank" download="' . $value['candidate_name'] . "'s CV file" . '" href="' . $cv_file . '" class="btn bg-green waves-effect set_datatable_view_button">
                                                <i class="material-icons icon_15">file_download</i>
                                             </a>';
                        endif;
                    elseif ($value['udf_type'] == 2):
                        $cv_file = UPLOAD_REL_PATH . '/' . UPLOAD_DOCUMENTS_FOLDER . '/' . $value['udf_id'] . '/' . $value['udf_rapidjob_cv_file_name'];
                        if (!empty($value['udf_rapidjob_cv_file_name']) && file_exists($cv_file)):
                            $cv_file = UPLOAD_ABS_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $value['udf_id'] . '/' . $value['udf_rapidjob_cv_file_name'];
                            $user_cv_file = '<a target="_blank" download="' . $value['candidate_name'] . "'s CV file" . '" href="' . $cv_file . '" class="btn bg-green waves-effect set_datatable_view_button">
                                                <i class="material-icons icon_15">file_download</i>
                                             </a>';
                        endif;
                    endif;

                    $cover_file = UPLOAD_REL_PATH . '/' . UPLOAD_DOCUMENTS_FOLDER . '/' . $value['udf_id'] . '/' . $value['udf_cover_latter_file'];
                    if (!empty($value['udf_cover_latter_file']) && file_exists($cover_file)):
                        $cover_file = UPLOAD_ABS_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $value['udf_id'] . '/' . $value['udf_cover_latter_file'];
                        $user_cover_file = '<a target="_blank" download="' . $value['candidate_name'] . "'s Cover latter" . '" href="' . $cover_file . '" class="btn bg-green waves-effect set_datatable_view_button">
                                        <i class="material-icons icon_15">file_download</i>
                                     </a>';
                    endif;

                    $resultArr[$key]['action_cv'] = !empty($user_cv_file) ? $user_cv_file : '<span class="label label-danger">CV file missing</span>';
                    $resultArr[$key]['action_cover_latter'] = !empty($user_cover_file) ? $user_cover_file : '<span class="label label-danger">Cover file missing</span>';
                }
                $return_data['data'] = $resultArr;
            }
        }


        return $return_data;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for get company application details
     * 
     * Created date : 2017-05-29
     */
    public function get_user_applications() {

        $user_app_id = $this->Common_model->escape_data($this->input->get_post("user_app_id"));
        $status = $this->Common_model->escape_data($this->input->get_post("status"));
        $recruiter_id = $this->Common_model->escape_data($this->input->get_post("recruiter_id"));
        $candidate_id = $this->Common_model->escape_data($this->input->get_post("candidate_id"));

        $user_application = array();
        try {
            if (
                    !empty($user_app_id) ||
                    !empty($candidate_id) ||
                    !empty($recruiter_id) ||
                    !empty($status) ||
                    $status != 1
            ) {

                $query = "  SELECT 
                            " . $this->table_filed . " 
                        FROM 
                            " . $this->table_name . " as candidate_application
                        WHERE 
                            candidate_application.ua_send_by=2 
                        AND 
                            candidate_application.ua_status !=9  
                        AND 
                            candidate_application.ua_id = " . $user_app_id . " 
                        AND 
                            candidate_application.ua_user_id = " . $candidate_id . " 
                        AND 
                            FIND_IN_SET(" . $recruiter_id . ", candidate_application.ua_company_id)";

                $user_application = $this->Common_model->get_single_row_by_query($query);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", "Problem in perform your action");
            redirect(POST_APPLICATION_PATH);
        }
        return $user_application;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for get company application details
     * 
     * Created date : 2017-05-29
     */
    public function check_user_applications() {

        $user_app_id = $this->Common_model->escape_data($this->input->get_post("user_app_id"));
        $candidate_id = $this->Common_model->escape_data($this->input->get_post("candidate_id"));
        $status = $this->Common_model->escape_data($this->input->get_post("status"));

        $user_application = array();
        try {
            if (
                    !empty($user_app_id) ||
                    !empty($candidate_id) ||
                    !empty($status) ||
                    $status != 1
            ) {

                $query = "  SELECT 
                                ua_company_id
                        FROM 
                            " . $this->table_name . " as candidate_application
                        WHERE 
                            candidate_application.ua_send_by=2 
                        AND 
                            candidate_application.ua_status !=9  
                        AND 
                            candidate_application.ua_id = " . $user_app_id . " 
                        AND 
                            candidate_application.ua_user_id = " . $candidate_id;

                $user_application = $this->Common_model->get_single_row_by_query($query);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", "Problem in perform your action");
            redirect(POST_APPLICATION_PATH);
        }
        return $user_application;
    }

}
