<?php

class Common_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * 
     * Description : use this function for check user email exist or not
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     * @param type $email
     * @param type $id
     * @return type
     */
    public function get_customer_by_email($email, $id = '') {
        $user_data = array();

        if (!empty($email)) {
            $where_user = array(
                'LOWER(u_email)' => strtolower($email),
                'u_status !=' => 9);

            if (!empty($id) && $id > 0 && is_numeric($id)) {
                $where_user['u_id !='] = $id;
            }

            $user_data = $this->get_single_row(TBL_USERS, "u_id", $where_user);
        }
        return $user_data;
    }
    
    
    /**
     * 
     * Description : use this function for get count of news 
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-15
     *      
     */
    public function get_count_news() {
        $news_data = array();
        $news_where = array(
                            'news_status !=' => 9
                            );
        $news_data = $this->get_count(TBL_NEWS, "news_id", $news_where);
        
        return $news_data;
    }
     /**
     * 
     * Description : use this function for get count of newsletter
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-15
     *      
     */
    public function get_count_newsletter() {
        $newsletter_data = array();
        $newsletter_where = array(
                            'nl_status !=' => 9
                            );
        $newsletter_data = $this->get_count(TBL_NEWSLETTERS, "nl_id", $newsletter_where);
        
        return $newsletter_data;
    }
    
    /**
     * 
     * Description : use this function for get count of faq
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-26
     *      
     */
    public function get_count_faq() {
        
        $faq_data = array();
        $faq_where = array(
                            'cms_faq_status !=' => 9
                            );
        $faq_data = $this->get_count(TBL_FAQ, "cms_faq_id", $faq_where);
        
        return $faq_data;
    }
    
    /**
     * 
     * Description : use this function for get count of testimonials
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-26
     *      
     */
    public function get_count_testimonials() {
        
        $testimonials_data = array();
        $testimonials_where = array(
                            'cms_t_status !=' => 9
                            );
        $testimonials_data = $this->get_count(TBL_TESTIMONIAL, "cms_t_id", $testimonials_where);
        
        return $testimonials_data;
    }
}
