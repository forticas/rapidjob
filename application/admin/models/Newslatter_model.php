<?php

/**
 * 
 * This model use for import recruiters
 * 
 * @author Nitinkumar Vaghani
 * Modified Date :- 2017-05-09
 * 
 */
class Newslatter_model extends MY_Model {

    /**
     * Base table name
     * @var string 
     */
    protected $send_newslatter_table_name;

    /**
     * Base table name
     * @var string 
     */
    protected $send_newslatter_field;

    /**
     *
     * Most use table filed list
     * 
     * @var string 
     */
    public $log_file;

    public function __construct() {
        parent::__construct();

        $this->send_newslatter_table_name = TBL_SEND_NEWSLATTER;
        $this->send_newslatter_field = array("*");

        $this->log_file = DOCROOT . 'logs/log_model_newslatter_' . date('d-m-Y') . '.txt';
    }

    /**
     * 
     * This function use for store imported recruiter
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     * @param type $request_data
     * @return boolean
     */
    public function store_newslatter() {

        $subject = get_slash_formatted_text(trim($this->Common_model->escape_data($this->input->get_post("subject"))));
        $message = get_slash_formatted_text(trim($this->Common_model->escape_data($this->input->get_post("message"))));

        $newslatter_data = array(
            'snl_subject' => $subject,
            'snl_message' => $message,
            'snl_created_date' => time()
        );

        $inserted_id = $this->insert($this->send_newslatter_table_name, $newslatter_data);

        if ($inserted_id > 0) {
            return $inserted_id;
        } else {
            return false;
        }
    }

}
