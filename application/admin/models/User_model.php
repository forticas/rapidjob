<?php

/**
 * 
 * This model use for do user related all database activity
 * 
 * @author Nitinkumar Vaghani
 * Modified Date :- 2017-05-09
 * 
 */
class User_model extends MY_Model {

    /**
     * Base table name
     * @var string 
     */
    protected $table_name;

    /**
     *
     * Most use table filed list
     * 
     * @var string 
     */
    protected $table_filed;

    public function __construct() {
        parent::__construct();

        $this->table_name = TBL_USERS;
        $this->table_filed = "u_id, CONCAT(if(u_first_name is null ,'',u_first_name),' ',if(u_last_name is null ,'',u_last_name)) as u_name,u_email,u_status,u_created_date";
    }

    /**
     * 
     * This function help you to get all user data with filter and pagination
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     * @param array $data
     * @return array
     */
    public function get_all_users($request_data = array()) {
        $return_data = array(
            "data" => array(),
            "total" => 0
        );

        $where = array();
        if (!empty($request_data['name'])) {
            $where["CONCAT(u_first_name,' ',u_last_name) LIKE "] = "%" . $request_data['name'] . "%";
        }
        if (!empty($request_data['email'])) {
            $where['u_email LIKE '] = "%" . $request_data['email'] . "%";
        }
        if (!empty($request_data['status'])) {
            $where['u_status'] = $request_data['status'];
        } else {
            $where['u_status !='] = 9;
        }

        //lets count total number of recoreds
        $record_count = $this->get_count($this->table_name, $this->table_filed, $where);

        $total_page = ceil($record_count / $request_data['per_page']);

        if ($record_count > 0) {

            $order_by = array(
                $request_data['sort_key'] => $request_data['sort_order']
            );

            $limit = array(
                (($request_data['page'] - 1) * $request_data['per_page']),
                $request_data['per_page']
            );

            $record_data = $this->get_all_rows($this->table_name, $this->table_filed, $where, array(), $order_by, $limit);

            $return_data['data'] = $record_data;
        }

        $return_data['total'] = $total_page;

        return $return_data;
    }

    /**
     * 
     * This function help you to register new user
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     * @param array $request_data
     * @return integer
     */
    public function add_user($request_data) {
        $ci_instance = & get_instance();
        $return_data = 0;
        $where = array(
            "LOWER(u_email)" => strtolower($request_data['email'])
        );
        /* check email exists or not */
        $user_data = $this->User_model->get_single_row(TBL_USERS, "u_id", $where);
        if (!empty($user_data)) {
            $ci_instance->form_validation->set_rules('email', "email", "exist");
            $ci_instance->form_validation->set_message('exist', 'This email already registered.');
            $ci_instance->form_validation->run();
        } else {
            $insert_data = array();
            $insert_data['u_first_name'] = $request_data['first_name'];
            $insert_data['u_last_name'] = $request_data['last_name'];
            $insert_data['u_email'] = $request_data['email'];
            $insert_data['u_password'] = password_hash(sha1($request_data['password']), PASSWORD_BCRYPT);
            $insert_data['u_created_date'] = time();

            $return_data = $this->insert($this->table_name, $insert_data);
        }
        return $return_data;
    }

    /**
     * 
     * This function help you to update user information
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     * @param array $request_data
     * @return integer
     */
    public function edit_user($request_data) {
        $ci_instance = & get_instance();
        $return_data = 0;
        $where = array(
            "LOWER(u_email)" => strtolower($request_data['email']),
            "u_id !=" => $request_data['user_id']
        );
        /* check email exists or not */
        $user_data = $this->User_model->get_single_row(TBL_USERS, "u_id", $where);
        if (!empty($user_data)) {
            $ci_instance->form_validation->set_rules('email', "email", "exist");
            $ci_instance->form_validation->set_message('exist', 'This email already registered.');
            $ci_instance->form_validation->run();
        } else {
            $insert_data = array();
            $insert_data['u_first_name'] = $request_data['first_name'];
            $insert_data['u_last_name'] = $request_data['last_name'];
            $insert_data['u_email'] = $request_data['email'];
            if (!empty($request_data['password'])) {
                $insert_data['u_password'] = password_hash(sha1($request_data['password']), PASSWORD_BCRYPT);
            }
            $insert_data['u_updated_date'] = time();

            $where = array();
            $where['u_id'] = $request_data['user_id'];

            $return_data = $this->update($this->table_name, $insert_data, $where);
        }
        return $return_data;
    }

    /**
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     * @description Get all active users count
     * @param integer $user_id
     * @return array
     */
    public function get_all_users_count() {
        $where = array(
            'u_status != ' => "9"
        );

        $userCount = $this->get_count($this->table_name, "*", $where);

        return $userCount;
    }

    /**
     * Description : Use this function for get all details of user
     * @author Nitinkumar vaghani
     * 
     * Last modified date : 19-01-2017
     * 
     * @param type $id => Candidate ID
     * @param type $company_id => recruiter ID
     * @return type
     */
    public function get_user_all_details($id, $company_id = '') {
        $data = array();
        if (!empty($id) && is_numeric($id)) {
            // get user details with job and documents
            $join_array = array(
                TBL_USER_DETAILS => "u_id = ud_user_id AND ud_status=1",
                TBL_WORK_JOB => "wj_id = ud_job AND wj_status=1",
                TBL_USER_DOCUMENT_FILES => "udf_user_id = u_id AND udf_status=1",
            );

            $where_user = array(
                'u_id' => $id,
                'u_status' => 1
            );

            // select user languages
            $column_language = array('ul_id', 'ul_language');
            $where_language = array('ul_status' => 1, 'ul_user_id' => $id);
            $data['user_languages'] = $this->Common_model->get_all_rows(TBL_USER_LANGUAGES, $column_language, $where_language);


            $column_user = "*, CONCAT(if(u_first_name is null ,'',u_first_name),' ',if(u_last_name is null ,'',u_last_name)) as user_name";
            $data['user_data'] = $this->Common_model->get_single_row(TBL_USERS, $column_user, $where_user, $join_array, 'LEFT');

            // get user experiences
            $where_exp = array(
                'ue_user_id' => $id,
                'ue_status' => 1
            );

            $join_exp_array = array(
                TBL_WORK_JOB => "wj_id = ue_job AND wj_status=1",
            );


            $column_user_exp = "*";
            $data['user_exp_data'] = $this->Common_model->get_all_rows(TBL_USER_EXPERIANCES, $column_user_exp, $where_exp, $join_exp_array, 'LEFT');

            // get user training
            $where_training = array(
                'ut_user_id' => $id,
                'ut_status' => 1
            );

            $column_user_training = "*";
            $data['user_training_data'] = $this->Common_model->get_all_rows(TBL_USER_TRAININGS, $column_user_training, $where_training);

            // get user skill
            $where_skill = array(
                'us_user_id' => $id,
                'us_status' => 1
            );

            $column_user_skill = "*";
            $data['user_skill_data'] = $this->Common_model->get_all_rows(TBL_USER_SKILLS, $column_user_skill, $where_skill);

            // get user hobbies
            $where_hobbies = array(
                'uh_user_id' => $id,
                'uh_status' => 1
            );

            $column_user_hobbies = "*";
            $data['user_hobbies_data'] = $this->Common_model->get_all_rows(TBL_USER_HOBBIES, $column_user_hobbies, $where_hobbies);


            if (!empty($company_id) && is_numeric($company_id)) {
                // get favourite user
                $where_favourite = array(
                    'cf_user_id' => $id,
                    'cf_company_id' => $company_id,
                    'cf_status' => 1
                );

                $column_user_favourite = 'COUNT(cf_id) as user_is_favourite';
                $user_favourite = $this->Common_model->get_single_row(TBL_COMPANY_FAVOURITES, $column_user_favourite, $where_favourite);
                $data['user_favourite'] = 0;

                if (!empty($user_favourite) && $user_favourite['user_is_favourite'] > 0) {
                    $data['user_favourite'] = $user_favourite['user_is_favourite'];
                }

                // get user application
                $application_query = "SELECT 
                                            * 
                                      FROM 
                                            " . TBL_USER_APPLICATIONS . " 
                                      WHERE 
                                            ua_user_id=" . $id . " 
                                      AND 
                                            ua_company_id IN(" . $company_id . ") 
                                      AND 
                                            ua_status=1 
                                      ORDER BY 
                                            ua_id DESC";
                $data['user_application'] = $this->Common_model->get_single_row_by_query($application_query);

                // get user document files
                $document_query = "SELECT 
                                            * 
                                      FROM 
                                            " . TBL_USER_DOCUMENT_FILES . " 
                                      WHERE 
                                            udf_user_id=" . $id . " 
                                      AND 
                                            udf_status=1 
                                      ORDER BY 
                                            udf_id DESC";
                $data['user_documents'] = $this->Common_model->get_single_row_by_query($document_query);
            }
        }

        return $data;
    }

    /**
     * Description : Use this function for get work job, activity area, work places and level of education
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     * 
     * @return type
     */
    public function get_default_details() {
        $data = array();
        // select user your job type
        $column_job = array('wj_id', 'wj_name_' . $this->current_lang . ' as job_name');
        $where_job = array('wj_status' => 1);
        $jobtype_orderby = array(
            'wj_name_' . $this->current_lang => 'ASC'
        );
        $data['work_job'] = $this->Common_model->get_all_rows(TBL_WORK_JOB, $column_job, $where_job, array(), $jobtype_orderby);

        // select user activity area
        $column_activity_area = array('wa_id', 'wa_name_' . $this->current_lang . ' as area_name');
        $where_activity_area = array('wa_status' => 1);
        $activity_orderby = array(
            'wa_name_' . $this->current_lang => 'ASC'
        );
        $data['activity_area'] = $this->Common_model->get_all_rows(TBL_WORK_ACTIVITY_AREA, $column_activity_area, $where_activity_area, array(), $activity_orderby);

        // select user work places
        $column_work_places = array('wp_id', 'wp_name');
        $where_work_places = array('wp_status' => 1);
        $workplace = array(
            'wp_name' => 'ASC'
        );
        $data['work_places'] = $this->Common_model->get_all_rows(TBL_WORK_PLACES, $column_work_places, $where_work_places, array(), $workplace);

        // select user educations
        $column_educations = array('ed_id', 'ed_name_' . $this->current_lang . ' as ed_name');
        $where_educations = array('ed_status' => 1);
        $education_orderby = array(
            'ed_name_' . $this->current_lang => 'ASC'
        );
        $data['level_of_educations'] = $this->Common_model->get_all_rows(TBL_EDUCATIONS, $column_educations, $where_educations, array(), $education_orderby);

        return $data;
    }

    /**
     * Description : Use this function for get nationalities
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     * 
     * @return type
     */
    public function get_nationalities() {
        $data = array();
        $column_nationality = array('n_id', 'n_code', 'n_name_' . $this->current_lang . ' as n_name');
        $where_nationality = array('n_status' => 1);
        $orderby_nationality = array(
            'n_name_' . $this->current_lang => 'ASC'
        );
        $data['nationalities'] = $this->Common_model->get_all_rows(TBL_NATIONALITIES, $column_nationality, $where_nationality, array(), $orderby_nationality);

        return $data;
    }

    /**
     * Description : Use this function for get user documents detail only
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     * 
     * @param type $doc_id
     * @param type $user_id
     * @return type
     */
    public function get_user_document_details($doc_id = 0, $user_id = 0) {
        $data = array();

        if (!empty($doc_id) && is_numeric($doc_id) || !empty($user_id) && is_numeric($user_id)) {
            if (!empty($doc_id) && is_numeric($doc_id)) {
                $where_user = array(
                    'udf_id' => $doc_id,
                    'udf_status' => 1,
                );
                $column_user = "*";
                $data = $this->Common_model->get_single_row(TBL_USER_DOCUMENT_FILES, $column_user, $where_user);
            } else if (!empty($user_id) && is_numeric($user_id)) {
                $where_user = array(
                    'udf_user_id' => $user_id,
                    'udf_status' => 1,
                );

                $join_user = array(TBL_USERS => 'u_id=udf_user_id AND u_status=1');

                $column_user = "*";
                $data = $this->Common_model->get_single_row(TBL_USER_DOCUMENT_FILES, $column_user, $where_user, $join_user, 'RIGHT');
            }
        }
        return $data;
    }

    /**
     * Description : Use this function for get user details with work and documents files.
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     * 
     * @param type $id
     * @return type
     */
    public function get_user_details($id = 0) {
        $data = array();

        if (!empty($id) && is_numeric($id)) {
            $where_user = array(
                'u_id' => $id,
                'u_status !=' => 9,
            );

            $data = $this->Common_model->get_single_row(TBL_USERS, 'u_type', $where_user);

            if (!empty($data)) {

                if ($data['u_type'] == 1) {
                    $join_user = array(
                        TBL_USER_DETAILS => "u_id = ud_user_id AND ud_status=1",
                        TBL_WORK_JOB => "wj_id = ud_job AND wj_status=1",
                        TBL_USER_DOCUMENT_FILES => "udf_user_id = u_id AND udf_status=1",
                    );
                    $column_user = "*";
                    $data = $this->Common_model->get_single_row(TBL_USERS, $column_user, $where_user, $join_user, 'LEFT');
                } elseif ($data['u_type'] == 2) {
                    $column_company = "CONCAT(
                                                if(
                                                    u_first_name is null ,'',u_first_name
                                                   ),' ',
                                                if(
                                                    u_last_name is null ,'',u_last_name
                                                   )
                                              ) as user_name, 
                                       u_id, u_type, u_email, u_image, u_created_date, u_status, cd_social_reason,
                                       cd_phone, cd_alternate_phone, cd_fax, cd_address, cd_city, cd_state, cd_country,
                                       cd_postal_code ";

                    $join_company = array(
                        TBL_COMPANY_DETAILS => 'cd_user_id=u_id and cd_status !=9'
                    );
                    $data = $this->Common_model->get_single_row(TBL_USERS, $column_company, $where_user, $join_company, 'LEFT');
                }
            }
        }
        return $data;
    }

    /**
     * Description : Use this function for get user details with work and documents files.
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     * 
     * 
     * @param type $id => Candidate / recruiter ID
     * @param type $doc_id => uadf_id
     * @return type
     */
    public function get_user_all_documents($user_id = 0) {
        $data = array();

        $join_array = array(
            //TBL_USER_ALL_DOCUMENT_FILES => "uadf_user_id = u_id AND uadf_status=1",
            TBL_USER_APPLICATIONS => 'ua_user_id = udf_user_id'
        );
        $where = array(
            'udf_status' => 1,
            'ua_status' => 1,
            'udf_user_id' => $user_id
        );

        $column_user = "*";
        $data = $this->Common_model->get_single_row(TBL_USER_DOCUMENT_FILES, $column_user, $where, $join_array);

        return $data;
    }

    /**
     * Description : Use this function for get company details selected by user
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     *
     * @param type $request_data
     * @return type
     */
    public function get_company_list_by_priority($request_data = NULL) {
        $data = array();
        // get company details
        $where = '';
        if ($request_data) {
            $where .=" AND u_id IN(" . implode(',', $request_data) . ") ";
        }

        $get_company_query = "SELECT 
                                    u_id, u_email, CONCAT(if(u_first_name is null ,'',u_first_name),' ',if(u_last_name is null ,'',u_last_name)) as company_name
                              FROM 
                                    " . TBL_USERS . " LEFT JOIN " . TBL_COMPANY_DETAILS . " 
                              ON 
                                    u_id = cd_user_id WHERE u_status=1 AND u_type=2 " . $where;
        $data = $this->get_all_rows_by_query($get_company_query);
        return $data;
    }

    /**
     * Description : Use this function for get company details except selected company
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     *
     * @param type $request_data
     * @return type
     */
    public function get_company_list($request_data = NULL) {
        $data = array();
        // get company details
        $where = '';

        if (!empty($request_data)) {
            $where .=" AND u_id IN(" . $request_data . ") ";
        }

        $get_company_query = "SELECT 
                                    u_id, 
                                    u_email, 
                                    CONCAT(if(u_first_name is null ,'',u_first_name),' ',if(u_last_name is null ,'',u_last_name)) as company_name 
                              FROM 
                                    " . TBL_USERS . " 
                              LEFT JOIN 
                                    " . TBL_COMPANY_DETAILS . " 
                                        ON u_id = cd_user_id 
                              WHERE 
                                    u_status=1 AND 
                                    u_type=2 " . $where;

        $data = $this->get_all_rows_by_query($get_company_query);
        return $data;
    }

    /**
     * Description : Use this function for get user workplaces
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     * 
     * @param type $request_data
     * @return type
     */
    public function get_user_workplaces($request_data = NULL) {
        $data = array();
        // get workplaces
        $where = '';
        if ($request_data) {
            $where .=" AND wp_id IN(" . implode(',', $request_data) . ") ";
        }

        $get_workplaces_query = "SELECT 
                                     GROUP_CONCAT(wp_name) AS workplace_name
                                 FROM 
                                     " . TBL_WORK_PLACES . " 
                                 WHERE
                                     wp_status=1 " . $where;
        $data = $this->get_single_row_by_query($get_workplaces_query);

        return $data;
    }

    /**
     * Description : Use this function for get user activities
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     * 
     * @param type $request_data
     * @return type
     */
    public function get_user_activities($request_data = NULL) {
        $data = array();
        // get activity
        $where = '';
        if ($request_data) {
            $where .=" AND wa_id IN(" . implode(',', $request_data) . ") ";
        }

        $get_workplaces_query = "SELECT 
                                    GROUP_CONCAT(wa_name_" . $this->current_lang . ") AS activity_name
                                 FROM 
                                    " . TBL_WORK_ACTIVITY_AREA . " 
                                 WHERE 
                                    wa_status=1 " . $where;
        $data = $this->get_single_row_by_query($get_workplaces_query);
        return $data;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for get company application details
     * 
     * Created date : 2017-05-29
     */
    public function get_user_applications($user_id = 0) {

        $user_application = array();
        try {
            if (!empty($user_id) && is_numeric($user_id)) {
                $columns = " CONCAT(if(recruiter.u_first_name is null ,'',recruiter.u_first_name),' ',if(recruiter.u_last_name is null ,'',recruiter.u_last_name)) as recruiter_name,
                         CONCAT(if(candidate.u_first_name is null ,'',candidate.u_first_name),' ',if(candidate.u_last_name is null ,'',candidate.u_last_name)) as candidate_name,
                        `recruiter`.`u_id` as `recruiter_id`, 
                        `recruiter`.`u_type` as `recruiter_type`, 
                        `recruiter`.`u_email` as `recruiter_email`, 
                        `recruiter`.`u_image` as `recruiter_image`, 
                        `recruiter`.`u_status` as `recruiter_status`, 
                        `candidate`.`u_id` as `candidate_id`, 
                        `candidate`.`u_type` as `candidate_type`, 
                        `candidate`.`u_email` as `candidate_email`, 
                        `candidate`.`u_image` as `candidate_image`, 
                        `candidate`.`u_status` as `candidate_status`, 
                        `candidate_application`.*, 
                        `candidate_document_files`.*
                        ";

                $where = " `recruiter`.`u_id` = " . $user_id . " AND `recruiter`.`u_status` != 9 ";

                $query = "  SELECT 
                            " . $columns . "
                        FROM 
                            " . TBL_USERS . " as `recruiter` 
                        RIGHT JOIN 
                            " . TBL_USER_APPLICATIONS . " as `candidate_application` 
                        ON 
                            `recruiter`.`u_id` IN(candidate_application.ua_company_id)
                        AND 
                            `candidate_application`.`ua_status` != 9 
                        RIGHT JOIN 
                            " . TBL_USERS . " as `candidate` 
                        ON 
                            `candidate_application`.`ua_user_id` = `candidate`.`u_id` and `candidate`.`u_status` != 9 
                        RIGHT JOIN 
                            " . TBL_USER_DOCUMENT_FILES . " as `candidate_document_files` 
                        ON 
                            `candidate_document_files`.`udf_user_id` = `candidate`.`u_id` and `candidate_document_files`.`udf_status` != 9 
                        WHERE 
                                " . $where . "
                        GROUP BY 
                                `candidate_application`.`ua_user_id`
                        ORDER BY 
                                `candidate_application`.`ua_id` DESC ";

                $user_application = $this->Common_model->get_all_rows_by_query($query);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", "problem in perform action");
            redirect(USERS_PATH);
        }
        return $user_application;
    }

    /**
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-29
     * 
     * @description Get all active candidate count
     * @param integer $user_id
     * @return array
     */
    public function get_all_company_count() {
        $where = array(
            'u_status != ' => "9",
            'u_type' => "2"
        );

        $companyCount = $this->get_count($this->table_name, "*", $where);

        return $companyCount;
    }

    /**
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-29
     * 
     * @description Get all active user payment
     * @param integer $user_id
     * @return array
     */
    public function get_all_user_payment_count() {

        $where = array(
            'up_status !=' => 9,
            'u_status !=' => 9
        );
        $join = array(
            TBL_USERS => 'u_id = up_id'
        );

        $userpaymentCount = $this->get_count(TBL_USER_PAYMENTS, 'up_id', $where, $join);

        return $userpaymentCount;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for get users application details
     * 
     * Created date : 2017-04-19
     */
    public function get_candidate_applications($candidate_id = '') {
        $user_application = array();

        try {
            if (!empty($candidate_id) && is_numeric($candidate_id)) {
                $columns = "uadf_id,uadf_user_id,uadf_document_file_name,uadf_udf_id";
                $where = " `candidate`.`u_id` = " . $candidate_id . " AND `candidate`.`u_status` != 9 ";
                $query = "  SELECT 
                            " . $columns . " 
                        FROM 
                            " . TBL_USER_ALL_DOCUMENT_FILES . " as `candidate_all_application` 
                        RIGHT JOIN 
                            " . TBL_USERS . " as `candidate` 
                        ON 
                            `candidate_all_application`.`uadf_user_id` = `candidate`.`u_id` and `candidate`.`u_status` != 9 
                        WHERE 
                                " . $where . "
                        GROUP BY 
                                `candidate_all_application`.`uadf_id`
                        ORDER BY 
                                `candidate_all_application`.`uadf_id` DESC ";

                $user_application = $this->Common_model->get_all_rows_by_query($query);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", "problem in perform action");
            redirect(USERS_PATH);
        }
        return $user_application;
    }

}
