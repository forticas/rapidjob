<?php

/**
 * Description :- This model is used to manipulate the homepage, faq and professional space content.
 * 
 * @author Manish Ramnani
 * 
 * Modified Date :- 2017-05-25
 * 
 */
class Managecms_model extends MY_Model{
    
    protected $home_table_name;
    protected $testimonial_table_name;


    public function __construct() {
        parent::__construct();
        $this->home_table_name = TBL_CMS;
        $this->testimonial_table_name = TBL_TESTIMONIAL;
        $this->faq_table_name = TBL_FAQ;
    }
    
    /**
     * Description :- This function is used to update the homepage content.
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     * 
     * @param type $data
     * @return type
     */
    public function update_homepage_content($data) {
        $where = array(
            'cms_id' => 1
        );
        $is_update = $this->update($this->home_table_name, $data, $where);
        return $is_update;
    }
 
    /**
     * Description :- This function is used to get the homepage content.
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     * 
     * @return type
     */
    public function get_cms_content(){
        $get_data = $this->get_single_row($this->home_table_name, "*",array('cms_id' => 1));
        return $get_data;
    }
    
    /**
     * Description :- This function is used to add the testimonial
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     * 
     * @param type $data
     * @return type
     */
    public function add_testimonials($data){
        $inserted_id = $this->insert($this->testimonial_table_name, $data);
        return $inserted_id;
    }
    
    /**
     * Description :- This function is used to get the testimonial
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     * 
     * @param type $id
     * @return type
     */
    public function get_testimonial($id){
        
        $where = array(
            'cms_t_id' => $id
        );
        
        $columns = "*";
        
        $get_testimonial = $this->get_single_row($this->testimonial_table_name,$columns,$where);
        
        return $get_testimonial;
    }
    
    /**
     * Description :- This function is used to update the testimonial
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     * 
     * @param type $data
     * @param type $where
     * @return type
     */
    public function update_testimonial($data,$where){
        $is_update = $this->update($this->testimonial_table_name,$data,$where);
        return $is_update;
    }
    
    /**
     * Description :- This function get all the testimonials for display
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     * 
     * @param type $where
     * @param type $order_by
     * @param type $limit
     * @param type $like_array
     * @return type
     */
    public function get_all_testimonials($where, $order_by, $limit, $like_array) {

        $send_data = array();

        $columns = "*";

        $total_rows = $this->get_all_rows($this->testimonial_table_name, 'cms_t_id', $where, array(), $order_by, '', '', $like_array, '');
        $testimonial_data = $this->get_all_rows($this->testimonial_table_name, $columns, $where, array(), $order_by, $limit, '', $like_array, '');

        if (!empty($testimonial_data)) {
            $send_data['testimonial_data'] = $testimonial_data;
            $send_data['total_rows'] = count($total_rows);
        }

        return $send_data;
    }
    
    
    /**
     * Description :- This function get all the testimonials for display
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     * 
     * @param type $where
     * @param type $order_by
     * @param type $limit
     * @param type $like_array
     * @return type
     */
    public function get_all_faq($where, $order_by, $limit, $like_array) {

        $send_data = array();

        $columns = "*";

        $total_rows = $this->get_all_rows($this->faq_table_name, 'cms_faq_id', $where, array(), $order_by, '', '', $like_array, '');
        $faq_data = $this->get_all_rows($this->faq_table_name, $columns, $where, array(), $order_by, $limit, '', $like_array, '');

        if (!empty($faq_data)) {
            $send_data['faq_data'] = $faq_data;
            $send_data['total_rows'] = count($total_rows);
        }
        
        return $send_data;
    }
    
    
    /**
     * Description :- This function is used to update the faq
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     * 
     * @param type $data
     * @param type $where
     * @return type
     */
    public function update_faq($data,$where){
        $is_update = $this->update($this->faq_table_name,$data,$where);
        return $is_update;
    }
    
    /**
     * Description :- This function is used to add the faq
     * 
     * @author Hardik Patel
     * 
     * Modified Date :- 2017-05-26
     * 
     * @param type $data
     * @return type
     */
    public function add_faq($data){
        $inserted_id = $this->insert($this->faq_table_name, $data);
        return $inserted_id;
    }
    
    /**
     * Description :- This function is used to get the faq
     * 
     * @author Hardik Patel
     * 
     * Modified Date :- 2017-05-26
     * 
     * @param type $id
     * @return type
     */
    public function get_faq($id){
        
        $where = array(
            'cms_faq_id' => $id,
            'cms_faq_status !=' => 9
        );
        
        $columns = "*";
        
        $get_faq = $this->get_single_row($this->faq_table_name,$columns,$where);
        
        return $get_faq;
    }
    
}

