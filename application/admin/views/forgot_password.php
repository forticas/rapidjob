<?php
if ($this->session->userdata('admin_id') != NULL) {
    redirect(DASHBOARD_PATH);
}
?>
<body class="gray-bg">

    <div class="middle-box text-center loginscreen  animated fadeInDown">
        <div >
            <div>
                <h1 class="logo-name margin_bottom">
                    <img src="<?= ASSETS_PATH ?>images/logo.png">
                </h1>

            </div>
            <?php echo form_open('login/forgot_password', 'id="forgot_form"'); ?>

            <div class="form-group text-left">
                <?php
                if ($this->session->flashdata('flashSuccess')) {
                    ?>
                    <div class="row">
                        <div class="alert alert-success fade in col-lg-12 col-md-12 col-sm-12 col-xs-12" id="success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <?= $this->session->flashdata('flashSuccess') ?>
                        </div>
                    </div>
                    <?php
                }
                if ($this->session->flashdata('flashError')) {
                    ?>
                    <div class="row">
                        <div class="alert alert-danger fade in col-lg-12 col-md-12 col-sm-12 col-xs-12" id="success">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <?= $this->session->flashdata('flashError') ?>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <label for="email">Enter email for recover password</label>
                <?php
                $email = array(
                    'name' => 'email',
                    'id' => 'email',
                    'class' => 'form-control required email',
                    'value' => set_value('email') ? set_value('email') : '',
                    'placeholder' => 'Enter email',
                    'data-rule-regex_email' => 'true',
                    'data-msg-regex_email' => 'Please enter a valid email address',
                );
                echo form_input($email);
                ?>
            </div>


            <?php
            $submit_button = array(
                'type' => 'submit',
                'class' => 'btn btn-primary block full-width m-b',
                'content' => 'Get Password',
            );
            echo form_button($submit_button);
            ?>
            <a href="<?= LOGIN_PATH ?>"><h5><i class="fa fa-arrow-left"></i> login</h5></a>
            <?php echo form_close(); ?>
        </div>
    </div>

</body>
