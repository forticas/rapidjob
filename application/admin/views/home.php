<div class="row">
    <div class="col-lg-12">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                <div class="col-lg-12">
                    <?php
                    if ($this->session->flashdata('flashSuccess')) {
                        ?>
                        <div class="row">
                            <div class="alert alert-success fade in col-lg-12 col-md-12 col-sm-12 col-xs-12" id="success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <?= $this->session->flashdata('flashSuccess') ?>
                            </div>
                        </div>
                        <?php
                    }
                    if ($this->session->flashdata('flashError')) {
                        ?>

                        <div class="row">
                            <div class="alert alert-danger fade in col-lg-12 col-md-12 col-sm-12 col-xs-12" id="success">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <?= $this->session->flashdata('flashError') ?>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
</div>