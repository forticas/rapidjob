<link href="<?= ADMIN_ASSETS_PATH ?>plugins/waitme/waitMe.css" rel="stylesheet" />
<link href="<?= ADMIN_ASSETS_PATH ?>css/fileinput.css" rel="stylesheet">
<section class="content">
    <div class="container-fluid">
        <?= $breadcrumb ?>
        <?php
        $admin_name = $this->session->userdata("admin_name");
        $admin_last_name = $this->session->userdata("admin_last_name");
        $admin_email = $this->session->userdata("admin_email");
        ?>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Edit Profile
                        </h2>
                    </div>
                    <div class="body">
                        <?php echo form_open_multipart('dashboard/edit_profile', 'id="edit_profile_form"'); ?>
                        <label for="first_name">First Name</label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $admin_name = array(
                                    'name' => 'admin_name',
                                    'id' => 'first_name',
                                    'class' => 'form-control required',
                                    'value' => set_value('admin_name') ? set_value('admin_name') : $admin_name,
                                    'placeholder' => 'Enter first name',
                                );
                                echo form_input($admin_name);
                                ?>
                            </div>
                            <label class="error"><?php if (!empty(form_error('admin_name'))) echo form_error('admin_name'); ?></label>
                        </div>
                        <label for="admin_last_name">Last Name</label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $admin_last_name = array(
                                    'name' => 'admin_last_name',
                                    'id' => 'admin_last_name',
                                    'class' => 'form-control required',
                                    'value' => set_value('admin_last_name') ? set_value('admin_last_name') : $admin_last_name,
                                    'placeholder' => 'Enter last name',
                                );
                                echo form_input($admin_last_name);
                                ?>
                            </div>
                            <label class="error"><?php if (!empty(form_error('admin_last_name'))) echo form_error('admin_last_name'); ?></label>
                        </div>
                        <label for="email_address">Email Address</label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $admin_email = array(
                                    'name' => 'admin_email',
                                    'id' => 'admin_email',
                                    'class' => 'form-control required email',
                                    'value' => set_value('admin_email') ? set_value('admin_email') : $admin_email,
                                    'placeholder' => 'Enter email',
                                    'data-rule-regex_email' => 'true',
                                    'data-msg-regex_email' => 'Please enter a valid email address',
                                );
                                echo form_input($admin_email);
                                ?>
                            </div>
                            <label class="error"><?php if (!empty(form_error('admin_email'))) echo form_error('admin_email'); ?></label>
                        </div>
                        <label class="required" for="file-0b">Profile Image </label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $admin_photo = array(
                                    'name' => 'photo',
                                    'id' => 'file-0b',
                                    'class' => 'form-control file',
                                    'accept' => 'image/*',
                                );
                                echo form_upload($admin_photo);
                                ?>
                            </div>
                            <label class="error"><?php if (!empty(form_error('photo'))) echo form_error('photo'); ?></label>
                        </div>
                        <a href="<?= DASHBOARD_PATH ?>" class="btn btn-default waves-effect margin_left_10">CANCEL</a>
                        <?php
                        $submit_button = array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary waves-effect margin_left_10',
                            'content' => 'EDIT PROFILE',
                        );
                        echo form_button($submit_button);
                        ?>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var controller_url = '<?= DASHBOARD_PATH ?>';
    var image_url = '<?= UPLOAD_ABS_PATH . $this->session->userdata("admin_profile") ?>';
</script>
<script src="<?= ADMIN_ASSETS_PATH ?>js/fileinput.js"></script>
<script src="<?= ASSETS_PATH ?>admin/js/pages/admin/edit_profile.js" type="text/javascript"></script>
<link href="<?= ADMIN_ASSETS_PATH ?>css/custom_bottom.css" rel="stylesheet">