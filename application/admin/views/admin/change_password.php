<section class="content">
    <div class="container-fluid">
        <?= $breadcrumb ?>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Change Password
                        </h2>
                    </div>
                    <div class="body">
                        <?php echo form_open_multipart('dashboard/change_password', 'id="change_password_form"'); ?>
                        <input type="password" style="display: none;" name="tmp"> 
                        <label for="old_password">Current Password</label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $old_password = array(
                                    'name' => 'old_password',
                                    'id' => 'old_password',
                                    'class' => 'form-control required',
                                    'value' => '',
                                    'placeholder' => 'Enter current password',
                                    'autocomplete' => 'off',
                                );
                                echo form_password($old_password);
                                ?>
                            </div>
                            <label class="error" id="old_password"><?php if (!empty(form_error('old_password'))) echo form_error('old_password'); ?></label>
                        </div>
                        <label for="password">New Password</label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $password = array(
                                    'name' => 'password',
                                    'id' => 'password',
                                    'class' => 'form-control required',
                                    'value' => '',
                                    'placeholder' => 'Enter new password',
                                    'autocomplete' => 'off',
                                );
                                echo form_password($password);
                                ?>
                            </div>
                            <label class="error" id="password-error"><?php if (!empty(form_error('password'))) echo form_error('password'); ?></label>
                        </div>
                        <label for="cpassword">Confirm Password</label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $cpassword = array(
                                    'name' => 'cpassword',
                                    'id' => 'cpassword',
                                    'class' => 'form-control required',
                                    'value' => '',
                                    'placeholder' => 'Confirm password',
                                    'autocomplete' => 'off',
                                );
                                echo form_password($cpassword);
                                ?>
                            </div>
                            <label class="error" id="cpassword-error"><?php if (!empty(form_error('cpassword'))) echo form_error('cpassword'); ?>
                                <?php if (!empty(form_error('not_match'))) echo form_error('not_match'); ?></label>
                        </div>

                        <a href="<?= DASHBOARD_PATH ?>" class="btn btn-default waves-effect margin_left_10">CANCEL</a>
                        <?php
                        $submit_button = array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary waves-effect margin_left_10',
                            'content' => 'CHANGE PASSWORD',
                        );
                        echo form_button($submit_button);
                        ?>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= ASSETS_PATH ?>admin/js/pages/admin/change_password.js" type="text/javascript"></script>


