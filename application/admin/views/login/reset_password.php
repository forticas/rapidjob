<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if ($this->session->userdata('admin_id') != NULL) {
    redirect(DASHBOARD_PATH);
}
?>
<body class="fp-page">
    <?php if ($this->session->flashdata('success')) { ?>
        <script>toastr.success('<?= $this->session->flashdata('success') ?>')</script>
    <?php } if ($this->session->flashdata('failure')) { ?>
        <script>toastr.error('<?= $this->session->flashdata('failure') ?>')</script>                            
    <?php } ?>
    <div class="fp-box">
        <div class="logo">
            <a href="javascript:void(0);"><img src="<?= DEFAULT_LOGO; ?>"></a>
        </div>
        <div class="card">
            <div class="body">
                <?php echo form_open("login/verify_token/" . $token . "/" . $id, 'id="reset_password"'); ?>
                <div class="msg">Reset password</div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">lock</i>
                    </span>
                    <div class="form-line">
                        <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="" value="<?php echo empty($email) ? "" : $email; ?>" autofocus="true">
                    </div>
                    <label class="error" id="password-error"><?php if (!empty(form_error('password'))) echo form_error('password'); ?>
                        <?php if (!empty(form_error('not_registered'))) echo form_error('not_registered'); ?></label>
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">lock</i>
                    </span>
                    <div class="form-line">
                        <input type="password" id="cpassword" name="cpassword" class="form-control" placeholder="Confirm Password" required="">
                    </div>
                    <label class="error" id="cpassword-error"><?php if (!empty(form_error('cpassword'))) echo form_error('cpassword'); ?>
                        <?php if (!empty(form_error('not_match'))) echo form_error('not_match'); ?></label>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-block bg-pink waves-effect">CHANGE PASSWORD</button>
                    </div>
                </div>
                <div class="row m-t-15 m-b--20">
                    <div class="col-xs-12 align-right">
                        <a href="<?= LOGIN_PATH . "/forgot_password" ?>">Forgot password?</a>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>