<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if ($this->session->userdata('admin_id') != NULL) {
    redirect(DASHBOARD_PATH);
}
?>

<body class="fp-page">
    <div class="fp-box">
        <div class="logo">
            <a href="javascript:void(0);"><img src="<?= DEFAULT_LOGO; ?>"></a>
            <?php if ($this->session->flashdata('success')) { ?>
                <script>toastr.success('<?= $this->session->flashdata('success') ?>')</script>
            <?php } if ($this->session->flashdata('failure')) { ?>
                <script>toastr.error('<?= $this->session->flashdata('failure') ?>')</script>                            
            <?php } ?>
        </div>
        <div class="card">
            <div class="body">
                <?php echo form_open('login/forgot_password', 'id="forgot_password"'); ?>
                <div class="msg">
                    Enter your email address that you used to register. We'll send you an email with your username and a
                    link to reset your password.
                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">email</i>
                    </span>
                    <div class="form-line">
                        <?php
                        $email = array(
                            'name' => 'email',
                            'id' => 'email',
                            'class' => 'form-control required email',
                            'value' => set_value('email') ? set_value('email') : '',
                            'placeholder' => 'Enter email',
                            'autofocus' => 'true',
                            'data-rule-regex_email' => 'true',
                            'data-msg-regex_email' => 'Please enter a valid email address',
                        );
                        echo form_input($email);
                        ?>
                    </div>
                </div>
                <?php
                $submit_button = array(
                    'type' => 'submit',
                    'class' => 'btn btn-block btn-lg bg-pink waves-effect',
                    'content' => 'RESET MY PASSWORD',
                );
                echo form_button($submit_button);
                ?>
                <div class="row m-t-20 m-b--5 align-center">
                    <a href="<?= LOGIN_PATH ?>">Sign In!</a>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
