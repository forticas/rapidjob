<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if ($this->session->userdata('admin_id') != NULL) {
    redirect(DASHBOARD_PATH);
}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title><?= APP_NAME ?> <?= APP_VERSION ?></title>
        <!-- Favicon-->
        <link rel="icon" href="<?= DEFAULT_FAVICON_ICON; ?>" type="image/x-icon">
        <!-- Bootstrap Core Css -->
        <link href="<?= ADMIN_ASSETS_PATH ?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Google Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

        <!-- Waves Effect Css -->
        <link href="<?= ADMIN_ASSETS_PATH ?>plugins/node-waves/waves.css" rel="stylesheet" />

        <!-- Animation Css -->
        <link href="<?= ADMIN_ASSETS_PATH ?>plugins/animate-css/animate.css" rel="stylesheet" />
        <link href="<?= ADMIN_ASSETS_PATH ?>css/toastr.min.css" rel="stylesheet" />
        <!-- Custom Css -->
        <link href="<?= ADMIN_ASSETS_PATH ?>css/style.css" rel="stylesheet">

        <!-- Jquery Core Js -->
        <script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core Js -->
        <script src="<?= ADMIN_ASSETS_PATH ?>plugins/bootstrap/js/bootstrap.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="<?= ADMIN_ASSETS_PATH ?>plugins/node-waves/waves.js"></script>

        <!-- Validation Plugin Js -->
        <script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-validation/jquery.validate.js"></script>
        <script src="<?= ADMIN_ASSETS_PATH ?>plugins/toastr/toastr.min.js"></script>

        <!-- Custom Js -->
        <script src="<?= ADMIN_ASSETS_PATH ?>js/admin.js"></script>
        <script src="<?= ADMIN_ASSETS_PATH ?>js/pages/examples/sign-in.js"></script>
        <script src="<?= ADMIN_ASSETS_PATH ?>js/pages/examples/forgot-password.js"></script>
    </head>
