<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if ($this->session->userdata('admin_id') != NULL) {
    redirect(DASHBOARD_PATH);
}
?>
<script>
    $(document).ready(function () {
        setTimeout(function () {
            $('#errs').hide();
        }, 3000);
    });
</script>
<style>
    #errs p{
        text-align: center;
        margin-bottom: 20px;
    }
</style>
<body class="login-page">
    <?php if ($this->session->flashdata('success')) { ?>
        <script>toastr.success('<?= $this->session->flashdata('success') ?>')</script>
    <?php } if ($this->session->flashdata('failure')) { ?>
        <script>toastr.error('<?= $this->session->flashdata('failure') ?>')</script>                            
    <?php } ?>
    <div class="login-box">
        <div class="logo">
            <a href="javascript:void(0);"><img src="<?= DEFAULT_LOGO; ?>"></a>
        </div>
        <div class="card">
            <div class="body">
                <?php echo form_open('login', 'id="sign_in"'); ?>

                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">person</i>
                    </span>
                    <div class="form-line">
                        <?php
                        $email = array(
                            'id' => 'email',
                            'class' => 'form-control required',
                            'name' => 'email',
                            'value' => set_value('email'),
                            'placeholder' => 'Enter email',
                            'autofocus' => 'true',
                            'data-rule-regex_email' => 'true',
                            'data-msg-regex_email' => 'Please enter a valid email address',
                        );
                        echo form_input($email);
                        ?>
                    </div>

                </div>
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="material-icons">lock</i>
                    </span>
                    <div class="form-line">
                        <?php
                        $password = array(
                            'id' => 'password',
                            'class' => 'form-control required',
                            'name' => 'password',
                            'value' => '',
                            'placeholder' => 'Enter password',
                        );
                        echo form_password($password);
                        ?>
                    </div>
                </div>
                <div class="error" style="color:red;font-weight: normal !important;text-align: center;" id="errs">
                    <?php if (!empty(form_error('email'))) echo form_error('email'); ?>
                    <?php if (!empty(form_error('not_registered'))) echo form_error('not_registered'); ?>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <?php
                        $submit_button = array(
                            'type' => 'submit',
                            'class' => 'btn btn-block bg-pink waves-effect',
                            'content' => 'SIGN IN',
                        );
                        echo form_button($submit_button);
                        ?>
                    </div>
                </div>
                <div class="row m-t-15 m-b--20">
                    <div class="col-xs-12 align-right">
                        <a href="<?= LOGIN_PATH . "/forgot_password" ?>">Forgot password?</a>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>