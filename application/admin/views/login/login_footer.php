<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(document).on("focusout", "#email", function () {
            jQuery("#email_error").html("");
        });
        jQuery(document).on("focusout", "#password", function () {
            jQuery("#password_error").html("");
        });
        jQuery("#sign_in").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                },
            },
            messages: {
                email: {
                    required: "Please provide email address",
                    email: "Please enter a valid email address",
                },
                password: {
                    required: "Please provide a password",
                },
            }
        });

        jQuery("#forgot_password").validate({
            rules: {
                email: {
                    required: true,
                    email: true,
                }
            },
            messages: {
                email: {
                    required: "Enter email address",
                    email: "Please enter a valid email address",
                }
            }
        });

        jQuery("#reset_password").validate({
            rules: {
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 12,
                },
                cpassword: {
                    required: true,
                    equalTo: "#password",
                },
            },
            messages: {
                password: {
                    required: 'Please enter new password',
                    minlength: 'Password must contain minimum 6 characters',
                    maxlength: 'Password must contain maximum 12 characters',
                },
                cpassword: {
                    required: 'Please enter confirm password',
                    equalTo: 'Password and confirm password must be same',
                },
            }
        });

        jQuery.validator.addMethod("regex_email", function (value, element, regexpr) {
            if (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i.test(value)) {
                return true;
            } else {
                return false;
            }
        });
    });
</script>
</body>  
</html>