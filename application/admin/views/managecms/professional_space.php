<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<link href="<?= ADMIN_ASSETS_PATH ?>css/fileinput.css" rel="stylesheet">

<section class="content">
    <div class="container-fluid">
        <?= $breadcrumb ?>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Professional Space</h2>
                    </div>
                    <div class="body">
                        <?php echo form_open_multipart(MANAGE_CMS_PATH.'/professional','id="professional_content_form" class="form-horizontal" autocomplete="off"') ?>
                       
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="banner_image">Banner image*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $banner_image = array(
                                                'name' => 'prof_image',
                                                'id' => 'prof_image',
                                                'class' => 'form-control file',
                                                'accept' => 'image/*',
                                            );
                                            echo form_upload($banner_image);
                                         ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix" style="display:none;">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="prof_banner_content_en">Banner content in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $prof_banner_content_en = array(
                                                'name' => 'prof_banner_content_en',
                                                'id' => 'prof_banner_content_en',
                                                'class' => 'form-control',
                                                'placeholder' => 'Banner content in english',
                                                'maxlength' => 100,
                                                'value' => !empty($professional_content['cms_prof_banner_title_en']) ? get_slash_formatted_text($professional_content['cms_prof_banner_title_en']) : ''
                                            );
                                            echo form_input($prof_banner_content_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="row clearfix" style="display:none;">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="prof_banner_content_fr">Banner content in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $prof_banner_content_fr = array(
                                                'name' => 'prof_banner_content_fr',
                                                'id' => 'prof_banner_content_fr',
                                                'class' => 'form-control',
                                                'maxlength' => 100,
                                                'placeholder' => 'Banner content in french',
                                                'value' => !empty($professional_content['cms_prof_banner_title_fr']) ? get_slash_formatted_text($professional_content['cms_prof_banner_title_fr']) : ''
                                            );
                                            echo form_input($prof_banner_content_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="prof_database_en"> Access to our CV Database text in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $prof_database_en = array(
                                                'name' => 'prof_database_en',
                                                'id' => 'prof_database_en',
                                                'maxlength' => 600,
                                                'class' => 'form-control custom_textarea_class',
                                                'placeholder' => 'Access to our CV Database text in english',
                                                'value' => !empty($professional_content['cms_prof_cv_title_en']) ? get_slash_formatted_text($professional_content['cms_prof_cv_title_en']) : ''
                                            );
                                            echo form_textarea($prof_database_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="prof_database_fr">Access to our CV Database text in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $prof_database_fr = array(
                                                'name' => 'prof_database_fr',
                                                'id' => 'prof_database_fr',
                                                'maxlength' => 600,
                                                'class' => 'form-control custom_textarea_class',
                                                'placeholder' => 'Access to our CV Database text in french',
                                                'value' => !empty($professional_content['cms_prof_cv_title_fr']) ? get_slash_formatted_text($professional_content['cms_prof_cv_title_fr']) : ''
                                            );
                                            echo form_textarea($prof_database_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="prof_broadcast_en">Register your business in our broadcast text in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $prof_broadcast_en = array(
                                                'name' => 'prof_broadcast_en',
                                                'id' => 'prof_broadcast_en',
                                                'maxlength' => 600,
                                                'class' => 'form-control custom_textarea_class',
                                                'placeholder' => 'Register your business in our broadcast text in english',
                                                'value' => !empty($professional_content['cms_prof_broadcast_en']) ? get_slash_formatted_text($professional_content['cms_prof_broadcast_en']) : ''
                                            );
                                            echo form_textarea($prof_broadcast_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="prof_broadcast_fr">Register your business in our broadcast text in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $prof_broadcast_fr = array(
                                                'name' => 'prof_broadcast_fr',
                                                'id' => 'prof_broadcast_fr',
                                                'maxlength' => 600,
                                                'class' => 'form-control custom_textarea_class',
                                                'placeholder' => 'Register your business in our broadcast text in french',
                                                'value' => !empty($professional_content['cms_prof_broadcast_fr']) ? get_slash_formatted_text($professional_content['cms_prof_broadcast_fr']) : ''
                                            );
                                            echo form_textarea($prof_broadcast_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="prof_premium_mode_en">Premium mode text in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $prof_premium_mode_en = array(
                                                'name' => 'prof_premium_mode_en',
                                                'id' => 'prof_premium_mode_en',
                                                'maxlength' => 600,
                                                'class' => 'form-control custom_textarea_class',
                                                'placeholder' => 'Premium mode text in english',
                                                'value' => !empty($professional_content['cms_prof_mode_en']) ? get_slash_formatted_text($professional_content['cms_prof_mode_en']) : ''
                                            );
                                            echo form_textarea($prof_premium_mode_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="prof_premium_mode_fr">Premium mode text in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $prof_premium_mode_fr = array(
                                                'name' => 'prof_premium_mode_fr',
                                                'id' => 'prof_premium_mode_fr',
                                                'maxlength' => 600,
                                                'class' => 'form-control custom_textarea_class',
                                                'placeholder' => 'Premium mode text in french',
                                                'value' => !empty($professional_content['cms_prof_mode_fr']) ? get_slash_formatted_text($professional_content['cms_prof_mode_fr']) : ''
                                            );
                                            echo form_textarea($prof_premium_mode_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="clearfix"></div>
                        <?php
                        $submit_button = array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary waves-effect submit_button pull-right',
                            'content' => 'Save',
                        );
                        echo form_button($submit_button);
                        ?>
                        <?php echo form_close(); ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var cms_form = 3;
    var image_url = '<?= $professional_content['image_url'];  ?>';
</script>
<script src="<?= ADMIN_ASSETS_PATH ?>js/fileinput.js"></script>
<script src="<?= ASSETS_PATH ?>admin/js/pages/managecms/homepage.js" type="text/javascript"></script>
<link href="<?= ADMIN_ASSETS_PATH ?>css/custom_bottom.css" rel="stylesheet">