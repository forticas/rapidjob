<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<link href="<?= ADMIN_ASSETS_PATH ?>css/fileinput.css" rel="stylesheet">

<section class="content">
    <div class="container-fluid">
        <?= $breadcrumb ?>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Add FAQ</h2>
                    </div>
                    <div class="body">
                        <?php echo form_open_multipart(MANAGE_CMS_PATH.'/add_faq','id="faq_form" class="form-horizontal" autocomplete="off" ') ?>
                       
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="question_en">Question in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $question_en = array(
                                                'name' => 'question_en',
                                                'id' => 'question_en',
                                                'class' => 'form-control required',
                                                'placeholder' => 'Question in english',
                                                'maxlength' => 120,
                                            );
                                            echo form_input($question_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="title">Question in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $question_fr = array(
                                                'name' => 'question_fr',
                                                'id' => 'question_fr',
                                                'class' => 'form-control required',
                                                'placeholder' => 'Question in french',
                                                'maxlength' => 120,
                                            );
                                            echo form_input($question_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="question_description_en">Description in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $question_description_en = array(
                                                'name' => 'question_description_en',
                                                'id' => 'question_description_en',
                                                'class' => 'form-control custom_textarea_class required',
                                                'placeholder' => 'Description',
                                                'maxlength' => 350,
                                            );
                                            echo form_textarea($question_description_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="question_description_fr">Description in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $question_description_fr = array(
                                                'name' => 'question_description_fr',
                                                'id' => 'question_description_fr',
                                                'class' => 'form-control custom_textarea_class required',
                                                'placeholder' => 'Description',
                                                'maxlength' => 350,
                                            );
                                            echo form_textarea($question_description_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <?php
                        $submit_button = array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary waves-effect submit_button pull-right',
                            'content' => 'Save',
                        );
                        echo form_button($submit_button);
                        ?>
                        
                        <?php form_close(); ?>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= ASSETS_PATH ?>admin/js/pages/managecms/faq_add.js" type="text/javascript"></script>
<link href="<?= ADMIN_ASSETS_PATH ?>css/custom_bottom.css" rel="stylesheet">