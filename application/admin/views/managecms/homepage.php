<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<link href="<?= ADMIN_ASSETS_PATH ?>css/fileinput.css" rel="stylesheet">

<section class="content">
    <div class="container-fluid">
        <?= $breadcrumb ?>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Home Page</h2>
                    </div>
                    <div class="body">
                        <?php echo form_open_multipart(MANAGE_CMS_PATH.'/homepage','id="homepage_content_form" class="form-horizontal" autocomplete="off"') ?>
                       
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="banner_image">Banner image*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $banner_image = array(
                                                'name' => 'photo',
                                                'id' => 'file-0b',
                                                'class' => 'form-control file',
                                                'accept' => 'image/*',
                                            );
                                            echo form_upload($banner_image);
                                         ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix" style="display:none;">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="banner_content_en">Banner content in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $banner_content_en = array(
                                                'name' => 'banner_content_en',
                                                'id' => 'banner_content_en',
                                                'class' => 'form-control',
                                                'placeholder' => 'Banner content in english',
                                                'maxlength' => 100,
                                                'value' => !empty($homepage_content['cms_home_banner_content_en']) ? get_slash_formatted_text($homepage_content['cms_home_banner_content_en']) : ''
                                            );
                                            echo form_input($banner_content_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="row clearfix" style="display:none;">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="banner_content_fr">Banner content in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $banner_content_fr = array(
                                                'name' => 'banner_content_fr',
                                                'id' => 'banner_content_fr',
                                                'class' => 'form-control',
                                                'maxlength' => 100,
                                                'placeholder' => 'Banner content in french',
                                                'value' => !empty($homepage_content['cms_home_banner_content_fr']) ? get_slash_formatted_text($homepage_content['cms_home_banner_content_fr']) : ''
                                            );
                                            echo form_input($banner_content_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="create_account_en">Create account text in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $create_account_en = array(
                                                'name' => 'create_account_en',
                                                'id' => 'create_account_en',
                                                'maxlength' => 600,
                                                'class' => 'form-control custom_textarea_class',
                                                'placeholder' => 'Create account text in english',
                                                'value' => !empty($homepage_content['cms_hm_create_account_text_en']) ? get_slash_formatted_text($homepage_content['cms_hm_create_account_text_en']) : ''
                                            );
                                            echo form_textarea($create_account_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="create_account_en">Create account text in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $create_account_fr = array(
                                                'name' => 'create_account_fr',
                                                'id' => 'create_account_fr',
                                                'maxlength' => 600,
                                                'class' => 'form-control custom_textarea_class',
                                                'placeholder' => 'Create account text in french',
                                                'value' => !empty($homepage_content['cms_hm_create_account_text_fr']) ? get_slash_formatted_text($homepage_content['cms_hm_create_account_text_fr']) : ''
                                            );
                                            echo form_textarea($create_account_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix" style="display:none;">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="news_title_en">News title in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $news_title_en = array(
                                                'name' => 'news_title_en',
                                                'id' => 'news_title_en',
                                                'class' => 'form-control',
                                                'maxlength' => 100,
                                                'placeholder' => 'News title in english',
                                                'value' => !empty($homepage_content['cms_home_news_title_en']) ? get_slash_formatted_text($homepage_content['cms_home_news_title_en']) : ''
                                            );
                                            echo form_input($news_title_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix" style="display:none;">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="news_title_fr">News title in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $news_title_fr = array(
                                                'name' => 'news_title_fr',
                                                'id' => 'news_title_fr',
                                                'class' => 'form-control',
                                                'maxlength' => 100,
                                                'placeholder' => 'News title in french',
                                                'value' => !empty($homepage_content['cms_home_news_title_fr']) ? get_slash_formatted_text($homepage_content['cms_home_news_title_fr']) : ''
                                            );
                                            echo form_input($news_title_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                         <!--  featured First start -->
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Featured first title in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $featured_one_text_en = array(
                                                'name' => 'featured_one_title_en',
                                                'id' => 'featured_one_title_en',
                                                'class' => 'form-control required',
                                                'maxlength' => 15,
                                                'placeholder' => 'Featured first title in english',
                                                'value' => !empty($homepage_content['cms_featured_one_title_en']) ? get_slash_formatted_text($homepage_content['cms_featured_one_title_en']) : ''
                                            );
                                            echo form_input($featured_one_text_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Featured first title in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $featured_one_text_fr = array(
                                                'name' => 'featured_one_title_fr',
                                                'id' => 'featured_one_title_fr',
                                                'class' => 'form-control required',
                                                'maxlength' => 15,
                                                'placeholder' => 'Featured first title in french',
                                                'value' => !empty($homepage_content['cms_featured_one_title_fr']) ? get_slash_formatted_text($homepage_content['cms_featured_one_title_fr']) : ''
                                            );
                                            echo form_input($featured_one_text_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Featured first description in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $featured_one_description_en = array(
                                                'name' => 'featured_one_description_en',
                                                'id' => 'featured_one_description_en',
                                                'class' => 'form-control required',
                                                'maxlength' => 50,
                                                'placeholder' => 'Featured first description in english',
                                                'value' => !empty($homepage_content['cms_featured_one_description_en']) ? get_slash_formatted_text($homepage_content['cms_featured_one_description_en']) : ''
                                            );
                                            echo form_input($featured_one_description_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Featured first description in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $featured_one_description_fr = array(
                                                'name' => 'featured_one_description_fr',
                                                'id' => 'featured_one_description_fr',
                                                'class' => 'form-control required',
                                                'maxlength' => 50,
                                                'placeholder' => 'Featured first description in french',
                                                'value' => !empty($homepage_content['cms_featured_one_description_fr']) ? get_slash_formatted_text($homepage_content['cms_featured_one_description_fr']) : ''
                                            );
                                            echo form_input($featured_one_description_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  featured First end -->
                        
                        
                        <!--  featured second start -->
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Featured second title in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $featured_second_text_en = array(
                                                'name' => 'featured_second_title_en',
                                                'id' => 'featured_second_title_en',
                                                'class' => 'form-control required',
                                                'maxlength' => 15,
                                                'placeholder' => 'Featured second title in english',
                                                'value' => !empty($homepage_content['cms_featured_second_title_en']) ? get_slash_formatted_text($homepage_content['cms_featured_second_title_en']) : ''
                                            );
                                            echo form_input($featured_second_text_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Featured second title in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $featured_second_text_fr = array(
                                                'name' => 'featured_second_title_fr',
                                                'id' => 'featured_second_title_fr',
                                                'class' => 'form-control required',
                                                'maxlength' => 15,
                                                'placeholder' => 'Featured second title in french',
                                                'value' => !empty($homepage_content['cms_featured_second_title_fr']) ? get_slash_formatted_text($homepage_content['cms_featured_second_title_fr']) : ''
                                            );
                                            echo form_input($featured_second_text_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Featured second description in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $featured_second_description_en = array(
                                                'name' => 'featured_second_description_en',
                                                'id' => 'featured_second_description_en',
                                                'class' => 'form-control required',
                                                'maxlength' => 50,
                                                'placeholder' => 'Featured second description in english',
                                                'value' => !empty($homepage_content['cms_featured_second_description_en']) ? get_slash_formatted_text($homepage_content['cms_featured_second_description_en']) : ''
                                            );
                                            echo form_input($featured_second_description_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Featured second description in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $featured_second_description_fr = array(
                                                'name' => 'featured_second_description_fr',
                                                'id' => 'featured_second_description_fr',
                                                'class' => 'form-control required',
                                                'maxlength' => 50,
                                                'placeholder' => 'Featured second description in french',
                                                'value' => !empty($homepage_content['cms_featured_second_description_fr']) ? get_slash_formatted_text($homepage_content['cms_featured_second_description_fr']) : ''
                                            );
                                            echo form_input($featured_second_description_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!--  featured Second end -->
                        
                        <!--  featured third start -->
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Featured third title in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $featured_third_text_en = array(
                                                'name' => 'featured_third_title_en',
                                                'id' => 'featured_third_title_en',
                                                'class' => 'form-control required',
                                                'maxlength' => 15,
                                                'placeholder' => 'Featured third title in english',
                                                'value' => !empty($homepage_content['cms_featured_third_title_en']) ? get_slash_formatted_text($homepage_content['cms_featured_third_title_en']) : ''
                                            );
                                            echo form_input($featured_third_text_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Featured third title in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $featured_third_text_fr = array(
                                                'name' => 'featured_third_title_fr',
                                                'id' => 'featured_third_title_fr',
                                                'class' => 'form-control required',
                                                'maxlength' => 15,
                                                'placeholder' => 'Featured third title in french',
                                                'value' => !empty($homepage_content['cms_featured_third_title_fr']) ? get_slash_formatted_text($homepage_content['cms_featured_third_title_fr']) : ''
                                            );
                                            echo form_input($featured_third_text_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Featured third description in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $featured_third_description_en = array(
                                                'name' => 'featured_third_description_en',
                                                'id' => 'featured_third_description_en',
                                                'class' => 'form-control required',
                                                'maxlength' => 50,
                                                'placeholder' => 'Featured third description in english',
                                                'value' => !empty($homepage_content['cms_featured_third_description_en']) ? get_slash_formatted_text($homepage_content['cms_featured_third_description_en']) : ''
                                            );
                                            echo form_input($featured_third_description_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label>Featured third description in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $featured_third_description_fr = array(
                                                'name' => 'featured_third_description_fr',
                                                'id' => 'featured_third_description_fr',
                                                'class' => 'form-control required',
                                                'maxlength' => 50,
                                                'placeholder' => 'Featured third description in french',
                                                'value' => !empty($homepage_content['cms_featured_third_description_fr']) ? get_slash_formatted_text($homepage_content['cms_featured_third_description_fr']) : ''
                                            );
                                            echo form_input($featured_third_description_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <!--  featured third end -->
                        

                        
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="subscribe_text_en">Subscribe text in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $subscribe_text_en = array(
                                                'name' => 'subscribe_text_en',
                                                'id' => 'subscribe_text_en',
                                                'class' => 'form-control',
                                                'maxlength' => 100,
                                                'placeholder' => 'Subscribe text in english',
                                                'value' => !empty($homepage_content['cms_home_subscribe_text_en']) ? get_slash_formatted_text($homepage_content['cms_home_subscribe_text_en']) : ''
                                            );
                                            echo form_input($subscribe_text_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="subscribe_text_fr">Subscribe text in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $subscribe_text_fr = array(
                                                'name' => 'subscribe_text_fr',
                                                'id' => 'subscribe_text_fr',
                                                'class' => 'form-control',
                                                'maxlength' => 100,
                                                'placeholder' => 'Subscribe text in french',
                                                'value' => !empty($homepage_content['cms_home_subscribe_text_fr']) ? get_slash_formatted_text($homepage_content['cms_home_subscribe_text_fr']) : ''
                                            );
                                            echo form_input($subscribe_text_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="copy_right_text_en">Copyright text in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $copy_right_text_en = array(
                                                'name' => 'copy_right_text_en',
                                                'id' => 'copy_right_text_en',
                                                'class' => 'form-control',
                                                'maxlength' => 100,
                                                'placeholder' => 'Copyright text in english',
                                                'value' => !empty($homepage_content['cms_footer_copyright_en']) ? get_slash_formatted_text($homepage_content['cms_footer_copyright_en']) : ''
                                            );
                                            echo form_input($copy_right_text_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="copy_right_text_fr">Copyright text in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $copy_right_text_fr = array(
                                                'name' => 'copy_right_text_fr',
                                                'id' => 'copy_right_text_fr',
                                                'class' => 'form-control',
                                                'maxlength' => 100,
                                                'placeholder' => 'Copyright text in french',
                                                'value' => !empty($homepage_content['cms_footer_copyright_fr']) ? get_slash_formatted_text($homepage_content['cms_footer_copyright_fr']) : ''
                                            );
                                            echo form_input($copy_right_text_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="header" style="margin-bottom: 20px;">
                            <h2>Footer Settings</h2>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="contact_us">Contact us*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $contact_us = array(
                                                'name' => 'contact_us',
                                                'id' => 'contact_us',
                                                'class' => 'form-control',
                                                'placeholder' => 'Contact us',
                                                'value' => !empty($homepage_content['cms_footer_contactus']) ? $homepage_content['cms_footer_contactus'] : ''
                                            );
                                            echo form_input($contact_us);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="facebook_link">Facebook link*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $facebook_link = array(
                                                'name' => 'facebook_link',
                                                'id' => 'facebook_link',
                                                'class' => 'form-control',
                                                'placeholder' => 'Facebook link',
                                                'value' => !empty($homepage_content['cms_footer_facebook_link']) ? $homepage_content['cms_footer_facebook_link'] : ''
                                            );
                                            echo form_input($facebook_link);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="contact_us">Twitter Link*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $twitter_link = array(
                                                'name' => 'twitter_link',
                                                'id' => 'twitter_link',
                                                'class' => 'form-control',
                                                'placeholder' => 'Twitter Link',
                                                'value' => !empty($homepage_content['cms_footer_twitter_link']) ? $homepage_content['cms_footer_twitter_link'] : ''
                                            );
                                            echo form_input($twitter_link);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="google_link">Google plus link*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $google_link = array(
                                                'name' => 'google_link',
                                                'id' => 'google_link',
                                                'class' => 'form-control',
                                                'placeholder' => 'Google plus link',
                                                'value' => !empty($homepage_content['cms_footer_google_link']) ? $homepage_content['cms_footer_google_link'] : ''
                                            );
                                            echo form_input($google_link);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="linkedin_link">Linkedin link*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $linkedin_link = array(
                                                'name' => 'linkedin_link',
                                                'id' => 'linkedin_link',
                                                'class' => 'form-control',
                                                'placeholder' => 'Linkedin link',
                                                'value' => !empty($homepage_content['cms_footer_linkedin_link']) ? $homepage_content['cms_footer_linkedin_link'] : ''
                                            );
                                            echo form_input($linkedin_link);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <?php
                        $submit_button = array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary waves-effect submit_button pull-right',
                            'content' => 'Save',
                        );
                        echo form_button($submit_button);
                        ?>
                        <?php echo form_close(); ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var image_url = '<?= $homepage_content['image_url']; ?>';
    var cms_form = 2;
</script>
<script src="<?= ADMIN_ASSETS_PATH ?>js/fileinput.js"></script>
<script src="<?= ASSETS_PATH ?>admin/js/pages/managecms/homepage.js" type="text/javascript"></script>
<link href="<?= ADMIN_ASSETS_PATH ?>css/custom_bottom.css" rel="stylesheet">