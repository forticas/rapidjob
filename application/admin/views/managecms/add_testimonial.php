<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<link href="<?= ADMIN_ASSETS_PATH ?>css/fileinput.css" rel="stylesheet">

<section class="content">
    <div class="container-fluid">
        <?= $breadcrumb ?>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Testimonials</h2>
                    </div>
                    <div class="body">
                        <?php echo form_open_multipart(MANAGE_CMS_PATH.'/add_testimonial','id="add_testimonial" class="form-horizontal" autocomplete="off" ') ?>
                       
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="image">Image*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $image = array(
                                                'name' => 'testimonial_photo',
                                                'id' => 'testimonial_photo',
                                                'class' => 'form-control file',
                                                'accept' => 'image/*',
                                            );
                                            echo form_upload($image);
                                         ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="testimonial_title_en">Name in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $testimonial_title_en = array(
                                                'name' => 'testimonial_title_en',
                                                'id' => 'testimonial_title_en',
                                                'class' => 'form-control',
                                                'placeholder' => 'Title in english',
                                                'maxlength' => 50,
                                            );
                                            echo form_input($testimonial_title_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="title">Name in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $testimonial_title_fr = array(
                                                'name' => 'testimonial_title_fr',
                                                'id' => 'testimonial_title_fr',
                                                'class' => 'form-control',
                                                'placeholder' => 'Title in english',
                                                'maxlength' => 50,
                                            );
                                            echo form_input($testimonial_title_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="testimonial_description_en">Description in english*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $testimonial_description_en = array(
                                                'name' => 'testimonial_description_en',
                                                'id' => 'testimonial_description_en',
                                                'class' => 'form-control custom_textarea_class',
                                                'placeholder' => 'Description',
                                                'maxlength' => 150,
                                            );
                                            echo form_textarea($testimonial_description_en);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="row clearfix">
                            <div class="col-lg-2 col-md-2 col-sm-4 col-xs-5 form-control-label">
                                <label for="testimonial_description_fr">Description in french*</label>
                            </div>
                            <div class="col-lg-10 col-md-10 col-sm-8 col-xs-7">
                                <div class="form-group">
                                    <div class="form-line">
                                        <?php
                                            $testimonial_description_fr = array(
                                                'name' => 'testimonial_description_fr',
                                                'id' => 'testimonial_description_fr',
                                                'class' => 'form-control custom_textarea_class',
                                                'placeholder' => 'Description',
                                                'maxlength' => 150,
                                            );
                                            echo form_textarea($testimonial_description_fr);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <?php
                        $submit_button = array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary waves-effect submit_button pull-right',
                            'content' => 'Save',
                        );
                        echo form_button($submit_button);
                        ?>
                        
                        <?php form_close(); ?>
                        
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    var cms_form = 1;    
    var image_url = '<?= DEFAULT_USER_IMAGE_ABS ?>';
</script>
<script src="<?= ADMIN_ASSETS_PATH ?>js/fileinput.js"></script>
<script src="<?= ASSETS_PATH ?>admin/js/pages/managecms/homepage.js" type="text/javascript"></script>
<link href="<?= ADMIN_ASSETS_PATH ?>css/custom_bottom.css" rel="stylesheet">