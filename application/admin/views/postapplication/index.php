<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$application_type = !empty($type) && ($type == 'pending' || $type == 'delivered') ? trim($type) : 'pending';
?>

<!-- JQuery DataTable Css -->
<link href="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<!-- Jquery DataTable Plugin Js -->
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>

<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

<section class="content">
    <div class="container-fluid ">
        <?= $breadcrumb ?>
        <!-- #START# FILTER -->
        <div class="row clearfix smart_search">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel-group" id="accordion_4" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-danger">
                        <div class="panel-heading" role="tab" id="headingOne_4">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion_4" href="#collapseOne_4" aria-expanded="false" aria-controls="collapseOne_4">
                                    Smart Search
                                </a>
                                <a aria-controls="collapseOne_4" aria-expanded="true" href="#collapseOne_4" data-parent="#accordion_4" data-toggle="collapse" role="button" class="drop_arrow"> <i class="material-icons set_arrow">keyboard_arrow_down</i></a>
                            </h4>
                        </div>
                        <div id="collapseOne_4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_4">
                            <div class="panel-body">
                                <form method="post">
                                    <div class="row clearfix">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control name_filter" placeholder="Search by candidate or recruiter name" id="global_filter">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <button type="button" class="btn btn-default btn-lg waves-effect reset_filter">RESET</button>
                                            <button type="button" class="btn btn-primary btn-lg waves-effect search_filter">SEARCH</button>
                                        </div>
                                    </div>                       
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# FILTER-->

        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Post Application
                            <small>You can manage pending and delivered application here.</small>
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <ul role="tablist" class="nav nav-tabs tab-nav-right post_application" style="margin-bottom: 20px;">
                                    <li class="<?php echo!empty($type) && $type == 'pending' ? 'active' : '' ?>"><a href="<?= POST_APPLICATION_PATH ?>/index/pending">PENDING APPLICATION</a></li>
                                    <li class="<?php echo!empty($type) && $type == 'delivered' ? 'active' : '' ?>"><a href="<?= POST_APPLICATION_PATH ?>/index/delivered">DELIVERED APPLICATION</a></li>
                                </ul>
                                <div class="clearfix"></div>
                                <table class="table table-bordered table-hover dataTable table-responsive display" id="datatable_list" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>Candidate Name</th>
                                            <th>Company Name</th>
                                            <th>Company Address</th>
                                            <th>Created Date</th>
                                            <th>Status</th>
                                            <th>Download CV</th>
                                            <th>Download Cover</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">

    var controller_url = '<?= POST_APPLICATION_PATH ?>';
    var application_type = '<?= $application_type ?>';

    var toggle_flag = 0;

    jQuery(".panel-title").click(function () {
        $(".set_arrow").text("keyboard_arrow_up");
        if (toggle_flag == 0) {
            toggle_flag = 1;
        } else {
            $(".set_arrow").text("keyboard_arrow_down");
            toggle_flag = 0;
        }
    });
</script>
<script src="<?= ADMIN_ASSETS_PATH ?>js/pages/postapplication/index.js"></script>
<link href="<?= ADMIN_ASSETS_PATH ?>css/custom_bottom.css" rel="stylesheet">