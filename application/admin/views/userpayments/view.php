<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<section class="content">
    <div class="container-fluid">
        <?= $breadcrumb; ?>
        <div class="row clearfix">
            <!-- Linked Items -->
            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>News details</h2>
                        <div class="pull-right">
                            <div class="switch panel-switch-btn add_button_panel">
                                <a href="<?= NEWS_PATH ?>" class="btn btn-primary waves-effect add_button_on_list">
                                    <i class="material-icons matirial_icon_add">keyboard_backspace</i> <span class="set_add_button">BACK</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="body table-responsive" style="width: 100%">
                        <table class="table table-striped table-bordered">
                            <tbody>
                                <tr>
                                    <th scope="row" style="width: 16%;">News Title</th>
                                    <td><?= $news_name ?></td>
                                </tr>
                                <tr>
                                    <th scope="row">Description</th>
                                    <td><?= $news_description ?></td>
                                </tr>
                                <!-- <tr>
                                    <th scope="row">Created Date</th>
                                    <td><?php // $created_date ?></td>
                                </tr> -->
                                <tr>
                                    <th scope="row">Status</th>
                                    <td>
                                        <?php if ($status == 1) { ?>
                                            <span class="label label-success">Active</span>
                                        <?php } else if ($status == 2) { ?>
                                            <span class="label label-warning">In-Active</span>
                                        <?php } ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- #END# Linked Items -->
            <!-- Linked Items -->
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Profile Photo</h2>
                    </div>
                    <div class="body">
                        <img src="<?= $image_url ?>" class="img-responsive">
                    </div>
                </div>
            </div>
            <!-- #END# Linked Items -->
        </div>
    </div>
</section>
<link href="<?= ADMIN_ASSETS_PATH ?>css/custom_bottom.css" rel="stylesheet">
