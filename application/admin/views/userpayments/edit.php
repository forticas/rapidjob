<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link href="<?= ADMIN_ASSETS_PATH ?>plugins/waitme/waitMe.css" rel="stylesheet" />
<link href="<?= ADMIN_ASSETS_PATH ?>css/fileinput.css" rel="stylesheet">
<section class="content">
    <div class="container-fluid">
        <?= $breadcrumb; ?>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Edit news
                        </h2>
                    </div>
                    <div class="body">
                        <?php
                        echo form_open_multipart("news/edit/" . $news_id, 'id="custom_form_edit"');
                        echo form_hidden("news_id", $news_id);
                        ?>
                        <label for="first_name">Title English</label>  
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $news_name = array(
                                    'name' => 'news_name_en',
                                    'id' => 'news_name_en',
                                    'class' => 'form-control required',
                                    'placeholder' => 'Enter news title english',
                                    "value" => !empty($news_name_en) ? $news_name_en : ""
                                );
                                echo form_input($news_name);
                                ?>
                            </div>
                            <label class="error"><?php if (!empty(form_error('news_name_en'))) echo form_error('news_name_en'); ?></label>
                        </div>
                        
                        <label for="first_name">Title French</label>  
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $news_name_fr = array(
                                    'name' => 'news_name_fr',
                                    'id' => 'news_name_fr',
                                    'class' => 'form-control required',
                                    'placeholder' => 'Enter news title french',
                                    "value" => !empty($news_name_fr) ? $news_name_fr : ""
                                );
                                echo form_input($news_name_fr);
                                ?>
                            </div>
                            <label class="error"><?php if (!empty(form_error('news_name_fr'))) echo form_error('news_name_fr'); ?></label>
                        </div>
                        <label for="last_name">Description English</label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                
                                $news_description = array(
                                    'name'          => 'news_description_en',
                                    'id'            => 'news_description_en',
                                    'class'         => 'form-control no-resize required',
                                    'rows'          => '3',
                                    'value'         => !empty($news_description_en) ? $news_description_en : "",
                                    'placeholder'   => 'Enter news description english',
                                );
                                echo form_textarea($news_description); ?>                                
                            </div>
                            <label class="error"><?php if (!empty(form_error('news_description_en'))) echo form_error('news_description_en'); ?></label>
                        </div>
                        <label for="last_name">Description French</label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                
                                $news_description_fr = array(
                                    'name'          => 'news_description_fr',
                                    'id'            => 'news_description_fr',
                                    'class'         => 'form-control no-resize required',
                                    'rows'          => '3',
                                    'value'         => !empty($news_description_fr) ? $news_description_fr : "",
                                    'placeholder'   => 'Enter news description french',
                                );
                                echo form_textarea($news_description_fr); ?>                                
                            </div>
                            <label class="error"><?php if (!empty(form_error('news_description_fr'))) echo form_error('news_description_fr'); ?></label>
                        </div>
                        <label for="file-0b">Profile Image </label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $news_photo = array(
                                    'name' => 'photo',
                                    'id' => 'file-0b',
                                    'class' => 'form-control file',
                                    'accept' => 'image/*',
                                );
                                echo form_upload($news_photo);
                                ?>
                            </div>  
                            <label class="error"><?php if (!empty(form_error('photo'))) echo form_error('photo'); ?></label>
                        </div>
                        <a href="<?= NEWS_PATH ?>" class="btn btn-default waves-effect cancel_button">CANCEL</a>
                        <?php
                        $submit_button = array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary waves-effect submit_button',
                            'content' => 'EDIT',
                        );
                        echo form_button($submit_button);
                        ?>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var controller_url = '<?= NEWS_PATH ?>';
    var image_url = '<?= $image_url ?>';
</script>
<script src="<?= ADMIN_ASSETS_PATH ?>js/fileinput.js"></script>
<script src="<?= ASSETS_PATH ?>admin/js/pages/news/add_edit.js" type="text/javascript"></script>
<link href="<?= ADMIN_ASSETS_PATH ?>css/custom_bottom.css" rel="stylesheet">
