<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<link href="<?= ADMIN_ASSETS_PATH ?>plugins/waitme/waitMe.css" rel="stylesheet" />
<link href="<?= ADMIN_ASSETS_PATH ?>css/fileinput.css" rel="stylesheet">
<section class="content report-container">
    <div class="container-fluid">
        <div class="row clearfix">
            <?= $breadcrumb ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2 class="text-uppercase">Import recruiters</h2>
                    </div>
                    <div class="body">
                        <?php echo form_open_multipart(IMPORT_RECRUITERS_PATH.'/store_recruiters_data', 'id="import_recruiter_form" autocomplete="off"') ?>
                        <label class="required" for="file-0b">Upload recruiters file</label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $recruiter_file = array(
                                    'name' => 'recruiter_file',
                                    'id' => 'file-0b',
                                    'class' => 'form-control file',
                                    'accept'=>"application/vnd.ms-excel, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                                );
                                echo form_upload($recruiter_file);
                                ?>
                            </div>
                            <label class="error"><?php if (!empty(form_error('recruiter_file'))) echo form_error('recruiter_file'); ?></label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 form-control-label">
                            <a href="<?= DASHBOARD_PATH ?>" class="btn btn-default waves-effect cancel_button text-uppercase">Cancel</a>
                            <?php
                            $submit_button = array(
                                'type' => 'submit',
                                'class' => 'btn btn-primary waves-effect submit_button text-uppercase',
                                'content' => 'SEND',
                            );
                            echo form_button($submit_button);
                            ?>
                        </div>
                        <div class="clearfix"></div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="<?= ADMIN_ASSETS_PATH ?>js/fileinput.js"></script>
<script src="<?= ASSETS_PATH ?>admin/js/pages/import_recruiters/import_recruiters.js" type="text/javascript"></script>
<link href="<?= ADMIN_ASSETS_PATH ?>css/custom_bottom.css" rel="stylesheet">