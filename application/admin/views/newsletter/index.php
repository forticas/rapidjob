<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- JQuery DataTable Css -->
<link href="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<!-- Jquery DataTable Plugin Js -->
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>

<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/extensions/export/buttons.flash.min.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/extensions/export/jszip.min.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/extensions/export/pdfmake.min.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/extensions/export/vfs_fonts.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/extensions/export/buttons.html5.min.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/extensions/export/buttons.print.min.js"></script>

<section class="content">
    <div class="container-fluid ">
       <?= $breadcrumb ?>
        <!-- #START# FILTER -->
        <div class="row clearfix smart_search">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="panel-group" id="accordion_4" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-danger">
                        <div class="panel-heading" role="tab" id="headingOne_4">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion_4" href="#collapseOne_4" aria-expanded="false" aria-controls="collapseOne_4">
                                    Smart Search
                                </a>
                                <a aria-controls="collapseOne_4" aria-expanded="true" href="#collapseOne_4" data-parent="#accordion_4" data-toggle="collapse" role="button" class="drop_arrow"> <i class="material-icons set_arrow">keyboard_arrow_down</i></a>
                            </h4>
                        </div>
                        <div id="collapseOne_4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne_4">
                            <div class="panel-body">
                                <form method="post">
                                    <div class="row clearfix">
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <input type="text" class="form-control global_filter" placeholder="Global search" id="global_filter">
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                            <div class="form-group">
                                                <div class="form-line">
                                                    <select class="form-control show-tick status_filter" id="col5_filter">
                                                        <option value="0">--- Status ----</option>
                                                        <option value="1">Active</option>
                                                        <option value="2">Deactive</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                            <button type="button" class="btn btn-default btn-lg waves-effect reset_filter">RESET</button>
                                            <button type="button" class="btn btn-primary btn-lg waves-effect search_filter">SEARCH</button>
                                        </div>
                                    </div>                       
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# FILTER-->

        <!-- List table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            NEWSLETTER LIST
                        </h2>
                        <div class="pull-right">
                            <div class="switch panel-switch-btn add_button_panel">
                                <a href="<?= NEWSLETTER_PATH . '/add' ?>" class="btn btn-primary waves-effect add_button_on_list">
                                    <i class="material-icons matirial_icon_add">add</i> <span class="set_add_button">SEND MAIL</span>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="body">
                        <table class="table table-bordered table-hover dataTable table-responsive display" id="datatable_list" width="100%" cellspacing="0">
                            <thead>
                                <tr>
                                    <th>User Email</th>                                    
                                    <th>Created Date</th>
                                    <th>Status</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# List table -->
    </div>
</section>
<script type="text/javascript">
    var controller_url = '<?= NEWSLETTER_PATH ?>';
    var toggle_flag = 0;
    jQuery(".panel-title").click(function() {
        $(".set_arrow").text("keyboard_arrow_up");
        if (toggle_flag == 0) {
            toggle_flag = 1;
        } else {
            $(".set_arrow").text("keyboard_arrow_down");
            toggle_flag = 0;
        }
    });
</script>
<script src="<?= ADMIN_ASSETS_PATH ?>js/pages/newsletter/index.js"></script>
<link href="<?= ADMIN_ASSETS_PATH ?>css/custom_bottom.css" rel="stylesheet">