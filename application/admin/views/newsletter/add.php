<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<section class="content">
    <div class="container-fluid">
        <?= $breadcrumb ?>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Add newsletter
                        </h2>
                    </div>
                    <div class="body">
                        <?php echo form_open_multipart('newsletter/add', 'id="custom_form_add" autocomplete="off"'); ?>
                        <label for="subject">Subject</label>  
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $subject = array(
                                    'name' => 'subject',
                                    'id' => 'subject',
                                    'class' => 'form-control required',
                                    'value' => set_value('subject') ? set_value('subject') : '',
                                    'placeholder' => 'Enter subject',
                                );
                                echo form_input($subject);
                                ?>
                            </div>                            
                        </div>
                        <label for="last_name">Message</label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $message = array(
                                    'name'          => 'message',
                                    'id'            => 'message',
                                    'class'         => 'form-control no-resize required',
                                    'rows'          => '3',
                                    'value'         => set_value('message') ? set_value('message') : '',
                                    'placeholder'   => 'Enter message',
                                );
                                echo form_textarea($message); 
                                ?>
                            </div>                            
                        </div>
                        <a href="<?= NEWSLETTER_PATH ?>" class="btn btn-default waves-effect cancel_button">CANCEL</a>
                        <?php
                        $submit_button = array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary waves-effect submit_button',
                            'content' => 'SEND',
                        );
                        echo form_button($submit_button);
                        ?>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var controller_url = '<?= NEWSLETTER_PATH ?>';    
</script>
<script src="<?= ASSETS_PATH ?>admin/js/pages/newsletter/add_edit.js" type="text/javascript"></script>
<link href="<?= ADMIN_ASSETS_PATH ?>css/custom_bottom.css" rel="stylesheet">