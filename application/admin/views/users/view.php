<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!-- JQuery DataTable Css -->
<link href="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css" rel="stylesheet">
<!-- Jquery DataTable Plugin Js -->
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/jquery.dataTables.js"></script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js"></script>

<section class="content">
    <div class="container-fluid">
        <?= $breadcrumb; ?>

        <div class="container-fluid">

            <!-- Tabs With Custom Animations -->
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>User information</h2>
                            <div class="pull-right">
                                <div class="switch panel-switch-btn add_button_panel">
                                    <a href="<?= USERS_PATH ?>" class="btn btn-primary waves-effect add_button_on_list">
                                        <i class="material-icons matirial_icon_add">keyboard_backspace</i> <span class="set_add_button">Back</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="body">
                            <div class="row clearfix">
                                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs tab-nav-right" role="tablist">
                                        <li role="presentation" class="active"><a href="#home_animation_2" data-toggle="tab">PROFILE</a></li>
                                        <li role="presentation"><a href="#messages_animation_2" data-toggle="tab">APPLICATION</a></li>
                                    </ul>

                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane animated fadeInRight active" id="home_animation_2">
                                            <div class="row clearfix">
                                                <!-- User profile -->
                                                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                    <div class="body table-responsive">
                                                        <table class="table table-striped table-bordered">
                                                            <tbody>
                                                                <tr>
                                                                    <th scope="row">User Type</th>
                                                                    <td><?php if ($user_data['u_type'] == 1) : ?>
                                                                            Candidate
                                                                        <?php elseif ($user_data['u_type'] == 2) : ?>
                                                                            Recruiter
                                                                        <?php endif; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">User name</th>
                                                                    <td><?= !empty($user_data['user_name']) ? trim($user_data['user_name']) : " - - -"; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Email address</th>
                                                                    <td><?= $user_data['u_email'] ?></td>
                                                                </tr>
                                                                <?php if (isset($user_data['cd_phone']) && !empty($user_data['cd_phone'])) : ?>
                                                                    <tr>
                                                                        <th scope="row">Phone number</th>
                                                                        <td><?= $user_data['cd_phone'] ?></td>
                                                                    </tr>
                                                                <?php endif; ?>
                                                                <tr>
                                                                    <th scope="row">Gender</th>
                                                                    <td>
                                                                        <?php if (isset($user_data['ud_gender']) && $user_data['u_type'] == 1) : ?>
                                                                            <?php if ($user_data['ud_gender'] == 1) : ?>
                                                                                Male
                                                                            <?php elseif ($user_data['ud_gender'] == 2) : ?>
                                                                                Female
                                                                            <?php else: ?>
                                                                                - - -
                                                                            <?php endif; ?>
                                                                        <?php elseif (isset($user_data['cd_gender']) && $user_data['u_type'] == 2) : ?>
                                                                            <?php if ($user_data['cd_gender'] == 1) : ?>
                                                                                Male
                                                                            <?php elseif ($user_data['cd_gender'] == 2) : ?>
                                                                                Female
                                                                            <?php else: ?>
                                                                                - - -
                                                                            <?php endif; ?>

                                                                        <?php else: ?>
                                                                            - - -
                                                                        <?php endif; ?>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Created date</th>
                                                                    <td class="convert_time"><?= $user_data['u_created_date']; ?></td>
                                                                </tr>
                                                                <tr>
                                                                    <th scope="row">Status</th>
                                                                    <td>
                                                                        <?php if ($user_data['u_status'] == 1) { ?>
                                                                            <span class="label label-success">Active</span>
                                                                        <?php } else if ($user_data['u_status'] == 2) { ?>
                                                                            <span class="label label-warning">In-active</span>
                                                                        <?php } ?>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                                <!-- #END# User profile -->

                                                <!-- User profile picture -->
                                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                                                    <div class="body">
                                                        <img src="<?= $image_url; ?>" class="img-responsive">
                                                    </div>
                                                </div>
                                            </div>
                                            <!-- #END# User profile picture -->
                                        </div>


                                        <div role="tabpanel" class="tab-pane animated fadeInRight" id="messages_animation_2">
                                            <?php
                                            if (!empty($user_all_application)) :

                                                if ($user_data['u_type'] == 1) :
                                                    ?>
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <div class="body table-responsive">
                                                            <table class="table table-striped table-bordered dataTable no-footer table-responsive display" id="datatable_list_view">
                                                                <thead>
                                                                    <tr>
                                                                        <th scope="row">Created Date</th>
                                                                        <th scope="row">Download</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                    foreach ($user_all_application as $user_application) :
                                                                        $cv_file = UPLOAD_REL_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_application['uadf_udf_id'] . '/' . $user_application['uadf_document_file_name'];
                                                                        if (!empty($user_application['uadf_document_file_name']) && file_exists($cv_file)) :
                                                                            $cv_file = UPLOAD_ABS_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_application['uadf_udf_id'] . '/' . $user_application['uadf_document_file_name'];
                                                                            ?>
                                                                            <tr>
                                                                                <td class="convert_time"><?= $user_application['uadf_created_date']; ?></td>
                                                                                <td>
                                                                                    <a class="btn bg-green waves-effect set_datatable_view_button" title="Download cv file" download="<?= $cv_file ?>" href="<?= $cv_file ?>">
                                                                                        <i class="material-icons icon_15">file_download</i> 
                                                                                    </a>
                                                                                </td>
                                                                            </tr>
                                                                            <?php
                                                                        endif;
                                                                    endforeach;
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

                                                <?php elseif ($user_data['u_type'] == 2) : ?>
                                                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                                        <div class="body table-responsive">
                                                            <table class="table table-striped table-bordered">
                                                                <tbody>
                                                                    <tr>
                                                                        <th scope="row">User Name</th>
                                                                        <th scope="row">Download CV</th>
                                                                        <th scope="row">Download Cover latter</th>
                                                                    </tr>

                                                                    <?php
                                                                    foreach ($user_all_application as $user_application) :
                                                                        ?>
                                                                        <tr>
                                                                            <td><?= $user_application['candidate_name']; ?></td>
                                                                            <td>
                                                                                <?php
                                                                                if ($user_application['udf_type'] == 1):
                                                                                    $cv_file = UPLOAD_REL_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_application['udf_id'] . '/' . $user_application['udf_cv_file_name'];
                                                                                    if (!empty($user_application['udf_cv_file_name']) && file_exists($cv_file)):
                                                                                        $cv_file = UPLOAD_ABS_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_application['udf_id'] . '/' . $user_application['udf_cv_file_name'];
                                                                                        ?>
                                                                                        <a class="btn bg-green waves-effect set_datatable_view_button" title="Download cv file" download="<?= $cv_file ?>" href="<?= $cv_file ?>">
                                                                                            <i class="material-icons icon_15">file_download</i> 
                                                                                        </a>
                                                                                        <?php
                                                                                    endif;
                                                                                elseif ($user_application['udf_type'] == 2):
                                                                                    $cv_file = UPLOAD_REL_PATH . '/' . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_application['udf_id'] . '/' . $user_application['udf_rapidjob_cv_file_name'];
                                                                                    if (!empty($user_application['udf_rapidjob_cv_file_name']) && file_exists($cv_file)):
                                                                                        $cv_file = UPLOAD_ABS_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_application['udf_id'] . '/' . $user_application['udf_rapidjob_cv_file_name'];
                                                                                        ?>
                                                                                        <a class="btn bg-green waves-effect set_datatable_view_button" title="Download cv file" download="<?= $cv_file ?>" href="<?= $cv_file ?>">
                                                                                            <i class="material-icons icon_15">file_download</i> 
                                                                                        </a>
                                                                                        <?php
                                                                                    endif;
                                                                                endif;
                                                                                ?>
                                                                            </td>
                                                                            <td>
                                                                                <?php
                                                                                $cover_file = UPLOAD_REL_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_application['udf_id'] . '/' . $user_application['udf_cover_latter_file'];
                                                                                if (!empty($user_application['udf_cover_latter_file']) && file_exists($cover_file)):
                                                                                    $cover_file = UPLOAD_ABS_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_application['udf_id'] . '/' . $user_application['udf_cover_latter_file'];
                                                                                    ?>
                                                                                    <a class="btn bg-green waves-effect set_datatable_view_button" title="Download cover letter" download="<?= $cover_file ?>" href="<?= $cover_file ?>">
                                                                                        <i class="material-icons icon_15">file_download</i> 
                                                                                    </a>
                                                                                <?php endif; ?>                                                                      
                                                                            </td>
                                                                        </tr>
                                                                        <?php
                                                                    endforeach;
                                                                    ?>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <?php
                                                endif;
                                            else :
                                                ?>
                                                <h3 class="text-center">User application is empty</h3>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- #END# Tabs With Custom Animations -->
        </div>
    </div>
</section>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".convert_time").each(function () {
            var timestemp = parseInt(jQuery(this).text());
            var date = formatDateLocal("<?= DATE_FORMAT_JS ?>", timestemp * 1000, false);
            jQuery(this).text(date);
        });
    });
    $('#datatable_list_view').DataTable({"pageLength": 8, searching: false, "bFilter": false, "bLengthChange": false, "ordering": false});
</script>
<script src="<?= ADMIN_ASSETS_PATH ?>plugins/momentjs/moment.js"></script>
<link href="<?= ADMIN_ASSETS_PATH ?>css/custom_bottom.css" rel="stylesheet">
