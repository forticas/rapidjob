<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<link href="<?= ADMIN_ASSETS_PATH ?>plugins/waitme/waitMe.css" rel="stylesheet" />
<link href="<?= ADMIN_ASSETS_PATH ?>css/fileinput.css" rel="stylesheet">
<section class="content">
    <div class="container-fluid">
        <?= $breadcrumb; ?>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Edit user
                        </h2>
                    </div>
                    <div class="body">
                        <?php
                        echo form_open_multipart("users/edit/" . $user_id, 'id="custom_form_edit"');
                        echo form_hidden("user_id", $user_id);
                        ?>
                        <label for="first_name">First Name</label>  
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $first_name = array(
                                    'name' => 'first_name',
                                    'id' => 'first_name',
                                    'class' => 'form-control required',
                                    'placeholder' => 'Enter first name',
                                    "value" => !empty($first_name) ? $first_name : ""
                                );
                                echo form_input($first_name);
                                ?>
                            </div>
                            <label class="error"><?php if (!empty(form_error('first_name'))) echo form_error('first_name'); ?></label>
                        </div>
                        <label for="last_name">Last Name</label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $last_name = array(
                                    'name' => 'last_name',
                                    'id' => 'last_name',
                                    'class' => 'form-control required',
                                    'placeholder' => 'Enter last name',
                                    "value" => !empty($last_name) ? $last_name : ""
                                );
                                echo form_input($last_name);
                                ?>
                            </div>
                            <label class="error"><?php if (!empty(form_error('last_name'))) echo form_error('last_name'); ?></label>
                        </div>
                        <label for="email">Email Address</label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $email = array(
                                    'name' => 'email',
                                    'id' => 'email',
                                    'class' => 'form-control required email',
                                    'placeholder' => 'Enter email',
                                    "value" => !empty($email) ? $email : "",
                                    'data-rule-regex_email' => 'true',
                                    'data-msg-regex_email' => 'Please enter a valid email address',
                                );
                                echo form_input($email);
                                ?>
                            </div>
                            <label class="error"> <?php
                                if (!empty(form_error('email'))) {
                                    echo form_error('email');
                                }
                                if (!empty(form_error('exist'))) {
                                    echo form_error('exist');
                                }
                                ?></label>
                        </div>
                        <label for="password">Password</label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $password = array(
                                    'name' => 'password',
                                    'id' => 'password',
                                    'class' => 'form-control',
                                    'value' => '',
                                    'placeholder' => 'Enter new password',
                                    'autocomplete' => 'off',
                                    "data-rule-minlength" => "6",
                                    "data-msg-minlength" => "Password must contain minimum 6 characters",
                                );
                                echo form_password($password);
                                ?>
                            </div>
                            <label class="error"><?php if (!empty(form_error('password'))) echo form_error('password'); ?></label>
                        </div>
                        <label for="cpassword">Confirm Password</label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $cpassword = array(
                                    'name' => 'cpassword',
                                    'id' => 'cpassword',
                                    'class' => 'form-control',
                                    'value' => '',
                                    'placeholder' => 'Confirm password',
                                    'autocomplete' => 'off',
                                );
                                echo form_password($cpassword);
                                ?>
                            </div>
                            <label class="error"><?php if (!empty(form_error('cpassword'))) echo form_error('cpassword'); ?></label>
                        </div>
                        <label class="required" for="file-0b">Profile Image </label>
                        <div class="form-group">
                            <div class="form-line">
                                <?php
                                $admin_photo = array(
                                    'name' => 'photo',
                                    'id' => 'file-0b',
                                    'class' => 'form-control file',
                                    'accept' => 'image/*',
                                );
                                echo form_upload($admin_photo);
                                ?>
                            </div>  
                            <label class="error"><?php if (!empty(form_error('photo'))) echo form_error('photo'); ?></label>
                        </div>
                        <a href="<?= USERS_PATH ?>" class="btn btn-default waves-effect cancel_button">CANCEL</a>
                        <?php
                        $submit_button = array(
                            'type' => 'submit',
                            'class' => 'btn btn-primary waves-effect submit_button',
                            'content' => 'EDIT',
                        );
                        echo form_button($submit_button);
                        ?>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script type="text/javascript">
    var controller_url = '<?= USERS_PATH ?>';
    var image_url = '<?= $image_url ?>';
</script>
<script src="<?= ADMIN_ASSETS_PATH ?>js/fileinput.js"></script>
<script src="<?= ASSETS_PATH ?>admin/js/pages/users/add_edit.js" type="text/javascript"></script>
<link href="<?= ADMIN_ASSETS_PATH ?>css/custom_bottom.css" rel="stylesheet">
