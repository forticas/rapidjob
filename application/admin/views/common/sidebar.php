<section>
    <!-- Left Sidebar -->
    <aside id="leftsidebar" class="sidebar">
        <!-- User Info -->
        <div class="user-info">
            <div class="image">
                <img alt="profie photo" class="img-circle" height="48px" width="48px" src="<?= IMAGE_MANIPULATION_URL . "url=" . $this->session->userdata("admin_profile") . "&h=48&w=48&default=true" ?>" />
            </div>
            <div class="info-container">
                <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?= ucwords($this->session->userdata("admin_name") . ' ' . $this->session->userdata("admin_last_name")) ?></div>
                <div class="btn-group user-helper-dropdown">
                    <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                    <ul class="dropdown-menu pull-right user_profile_menu">
                        <li><a href="<?= DOMAIN_URL . 'admin/dashboard/edit_profile'; ?>"><i class="material-icons">person</i>Edit Profile</a></li>
                        <li role="seperator" class="divider"></li>
                        <li><a href="<?= DOMAIN_URL . 'admin/dashboard/change_password'; ?>"><i class="material-icons">lock</i>Change Password</a></li>
                        <li role="seperator" class="divider"></li>
                        <li><a href="<?= DOMAIN_URL . 'admin/dashboard/logout'; ?>"><i class="material-icons">input</i>Sign Out</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- #User Info -->
        <!-- Menu -->
        <div class="menu">
            <ul class="list">
                <li class="<?= ($this->controller_name == 'dashboard') ? 'active' : '' ?>">
                    <a href="<?= DOMAIN_URL . 'admin'; ?>">
                        <i class="material-icons">home</i>
                        <span>Home</span>
                    </a>
                </li>
                <li class="<?= ($this->controller_name == 'users') ? 'active' : '' ?>">
                    <a href="<?= DOMAIN_URL . 'admin/users'; ?>">
                        <i class="material-icons">person</i>
                        <span>Users</span>
                    </a>
                </li>
                <li class="<?= ($this->controller_name == 'import_recruiters') ? 'active' : '' ?>">
                    <a href="<?= DOMAIN_URL . 'admin/import_recruiters'; ?>">
                        <i class="material-icons">import_export</i>
                        <span>Import Recruiters</span>
                    </a>
                </li>
                <li class="<?= ($this->controller_name == 'news') ? 'active' : '' ?>">
                    <a href="<?= DOMAIN_URL . 'admin/news'; ?>">
                        <i class="material-icons">forum</i>
                        <span>News</span>
                    </a>
                </li>

                <li class="<?= ($this->controller_name == 'newsletter') ? 'active' : '' ?>">
                    <a href="<?= DOMAIN_URL . 'admin/newsletter'; ?>">
                        <i class="material-icons">call_made</i>
                        <span>Newsletter</span>
                    </a>
                </li>

                <li class="<?= ($this->controller_name == 'userpayments') ? 'active' : '' ?>">
                    <a href="<?= DOMAIN_URL . 'admin/userpayments'; ?>">
                        <i class="material-icons">payment</i>
                        <span>User Payments</span>
                    </a>
                </li>

                <li class="<?= ($this->controller_name == 'postapplication') ? 'active' : '' ?>">
                    <a href="<?= DOMAIN_URL . 'admin/postapplication'; ?>">
                        <i class="material-icons">local_post_office</i>
                        <span>Post Application</span>
                    </a>
                </li>

                <li class="<?= ($this->controller_name == 'settings') ? 'active' : '' ?>">
                    <a href="<?= SETTING_PATH; ?>">
                        <i class="material-icons">settings</i>
                        <span>Settings</span>
                    </a>
                </li>

                <li class="<?= ($this->controller_name == 'managecms') ? 'active' : '' ?>">
                    <a href="javascript:void(0);" class="menu-toggle waves-effect waves-block">
                        <i class="material-icons">content_paste</i>
                        <span>Manage Cms</span>
                    </a>
                    <ul class="ml-menu">
                        <?php
                        $ci = & get_instance();
                        ?>
                        <li class="<?= ($ci->router->fetch_method() == 'homepage') ? 'active' : '' ?>">
                            <a href="<?php echo MANAGE_CMS_PATH . '/homepage' ?>" class=" waves-effect waves-block">Home page</a>
                        </li>
                        <li class="<?= ($ci->router->fetch_method() == 'professional') ? 'active' : '' ?>">
                            <a href="<?php echo MANAGE_CMS_PATH . '/professional' ?>" class=" waves-effect waves-block">Professional Space</a>
                        </li>
                        <li class="<?=
                        ($ci->router->fetch_method() == 'testimonials' ||
                        $ci->router->fetch_method() == 'add_testimonial' || $ci->router->fetch_method() == 'edit_testimonial') ? 'active' : ''
                        ?>">
                            <a href="<?php echo MANAGE_CMS_PATH . '/testimonials' ?>" class=" waves-effect waves-block">Testimonials</a>
                        </li>
                        <li class="<?=
                        ($ci->router->fetch_method() == 'faq' ||
                        $ci->router->fetch_method() == 'add_faq' ||
                        $ci->router->fetch_method() == 'edit_faq') ? 'active' : ''
                        ?>">
                            <a href="<?php echo MANAGE_CMS_PATH . '/faq' ?>" class=" waves-effect waves-block">FAQ</a>
                        </li>

                    </ul>
                </li>


            </ul>
        </div>
        <!-- #Menu -->
        <!-- Footer -->
        <div class="legal">
            <div class="copyright">
                Copyright <?= APP_NAME ?> <?= APP_VERSION; ?> &copy; <?php echo date("Y") . " - " . date("Y", strtotime("+1 Years")) . " "; ?>
            </div>
        </div>
        <!-- #Footer -->
    </aside>
    <!-- #END# Left Sidebar -->
</section>
