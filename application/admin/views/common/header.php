﻿<!DOCTYPE html>
<?php
$all_languages = get_all_languages();
$direction = $all_languages[get_session_language()];
?>
<html dir="<?= $direction ?>">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <title><?= APP_NAME ?> <?= APP_VERSION ?></title>
        <link rel="icon" href="<?= DEFAULT_FAVICON_ICON; ?>" type="image/x-icon">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">
        <link href="<?= ADMIN_ASSETS_PATH ?>plugins/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="<?= ADMIN_ASSETS_PATH ?>plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
        <link href="<?= ADMIN_ASSETS_PATH ?>plugins/node-waves/waves.css" rel="stylesheet" />
        <link href="<?= ADMIN_ASSETS_PATH ?>plugins/animate-css/animate.css" rel="stylesheet" />
        <link href="<?= ADMIN_ASSETS_PATH ?>plugins/morrisjs/morris.css" rel="stylesheet" />
        <link href="<?= ADMIN_ASSETS_PATH ?>plugins/sweetalert/sweetalert.css" rel="stylesheet" />
        <link href="<?= ADMIN_ASSETS_PATH ?>css/style.css" rel="stylesheet">
        <link href="<?= ADMIN_ASSETS_PATH ?>css/toastr.min.css" rel="stylesheet" />
        <link href="<?= ADMIN_ASSETS_PATH ?>css/themes/all-themes.css" rel="stylesheet" />
        <link href="<?= ADMIN_ASSETS_PATH ?>css/custom.css" rel="stylesheet">
        <link href="<?= ADMIN_ASSETS_PATH ?>plugins/animate-css/animate.css" rel="stylesheet" />
        <script type="text/javascript">
            var base_url = '<?php echo BASE_URL; ?>';
            var date_format_js = '<?php echo DATE_FORMAT_JS; ?>';
        </script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery/jquery.min.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>plugins/bootstrap/js/bootstrap.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>plugins/bootstrap-select/js/bootstrap-select.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-slimscroll/jquery.slimscroll.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>plugins/node-waves/waves.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-countto/jquery.countTo.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>plugins/raphael/raphael.min.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>plugins/morrisjs/morris.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-sparkline/jquery.sparkline.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>plugins/sweetalert/sweetalert.min.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>js/admin.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>js/pages/ui/animations.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>js/pages/ui/dialogs.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>js/demo.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>plugins/toastr/toastr.min.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>js/local-time.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>js/fileinput.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>js/jquery.validate.js"></script>
        <script type="text/javascript" src="<?= ADMIN_ASSETS_PATH ?>js/additional-methods.min.js"></script>
        <style>
            #datatable_list_filter{
                display: none;
            }
        </style>
    </head>

    <body class="theme-teal">
        <script>
            toastr.options = {
                "preventDuplicates": true,
                "preventOpenDuplicates": true,
                "closeButton": true,
                "progressBar": true
            };
        </script>
        <?php if ($this->session->flashdata('success')) { ?>
            <script>toastr.success('<?= $this->session->flashdata('success') ?>')</script>
        <?php } if ($this->session->flashdata('failure')) { ?>
            <script>toastr.error('<?= $this->session->flashdata('failure') ?>')</script>                            
        <?php } ?>
        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>" id="csrf_token_data">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="preloader">
                    <div class="spinner-layer pl-red">
                        <div class="circle-clipper left">
                            <div class="circle"></div>
                        </div>
                        <div class="circle-clipper right">
                            <div class="circle"></div>
                        </div>
                    </div>
                </div>
                <p>Please wait...</p>
            </div>
        </div>
        <!-- #END# Page Loader -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
        <!-- #END# Overlay For Sidebars -->
        <!-- Top Bar -->
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="javascript:void(0);" class="bars"></a>
                    <a class="navbar-brand" href="<?= BASE_URL ?>" style="padding: 0"><img src="<?= DEFAULT_LOGO; ?>" height="50" width="200"></a>
                </div>
                <div style="margin-top: 20px;">
                    <?php if (IS_MULTI_SITE): ?>
                        <?php
                        $language = get_session_language();
                        ?>
                        <select class="pull-right" id="language_dropdown">
                            <?php foreach ($all_languages as $lang_key => $lang_value): ?>
                                <option value="<?= $lang_key ?>" <?php
                                if ($language == $lang_key || $language == '') {
                                    echo "selected='selected'";
                                }
                                ?>><?= ucfirst($lang_key); ?></option>
                                    <?php endforeach; ?>
                        </select>
                    <?php endif; ?>
                </div>
            </div>
        </nav>
        <!-- #Top Bar -->

        <script type="text/javascript">
            jQuery(document).ready(function () {
                $('#language_dropdown').change(function () {
                    location.href = "<?= DASHBOARD_PATH ?>/change_language/" + $(this).val();
                });

                jQuery(document).ajaxSuccess(function (event, xhr, settings) {
                    if (settings.url != "<?= DASHBOARD_PATH ?>/get_csrf_value") {
                        jQuery.ajax({
                            type: 'POST',
                            async: false,
                            url: "<?= DASHBOARD_PATH ?>/get_csrf_value",
                            success: function (data) {
                                if (data.success) {
                                    jQuery("#csrf_token_data").val(data.value);
                                } else {
                                    window.location.reload();
                                }
                                $("#datatable_list_filter").css('display', 'none');
                                $('select[name=datatable_list_length]').addClass('selectpicker');
                                $('select[name=datatable_list_length]').addClass('show-tick');
                                $('select[name=datatable_list_length]').selectpicker('refresh');
                            },
                            error: function () {
                                window.location.reload();
                            }
                        });
                    }
                });

                jQuery(document).ajaxError(function (event, xhr, settings) {
                    if (settings.url != "<?= DASHBOARD_PATH ?>/get_csrf_value") {
                        jQuery.ajax({
                            type: 'POST',
                            async: false,
                            url: "<?= DASHBOARD_PATH ?>/get_csrf_value",
                            success: function (data) {
                                if (data.success) {
                                    jQuery("#csrf_token_data").val(data.value);
                                } else {
                                    window.location.reload();
                                }
                            },
                            error: function () {
                                window.location.reload();
                            }
                        });
                    }
                });

                var csrf_token_name = '<?php echo $this->security->get_csrf_token_name(); ?>';

                var todayDate = new Date();
                var timestamp = -todayDate.getTimezoneOffset() * 60;
                jQuery.ajaxSetup({
                    beforeSend: function (xhr, data) {
                        data.data += '&' + csrf_token_name + '=' + jQuery("#csrf_token_data").val() + '&timestamp=' + timestamp;
                        $('#loading').show();
                    },
                    complete: function ()
                    {
                        $('#loading').hide();
                    }
                });
            });
        </script>
