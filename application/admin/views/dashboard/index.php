<script src="<?= ADMIN_ASSETS_PATH ?>plugins/jquery-countto/jquery.countTo.js"></script>
<section class="content">
    <div class="container-fluid">
        <?= $breadcrumb; ?>
        <!-- Widgets -->
        <div class="row clearfix">
            <a href="<?= USERS_PATH; ?>">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-orange hover-expand-effect" style="cursor: pointer;">
                        <div class="icon">
                            <i class="material-icons">person</i>
                        </div>
                        <div class="content">
                            <div class="text">USERS</div>
                            <div class="number count-to" data-from="0" data-to="<?= $userCount ?>" data-speed="1000" data-fresh-interval="20"><?= $userCount ?></div>
                        </div>
                    </div>

                </div>
            </a>
            
            <a href="<?= USERS_PATH; ?>">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-grey hover-expand-effect" style="cursor: pointer;">
                        <div class="icon">
                            <i class="material-icons">people</i>
                        </div>
                        <div class="content">
                            <div class="text">RECRUITER</div>
                            <div class="number count-to" data-from="0" data-to="<?= $companyCount ?>" data-speed="1000" data-fresh-interval="20"><?= $companyCount ?></div>
                        </div>
                    </div>

                </div>
            </a>
            
            <a href="<?= USER_PAYMENTS_PATH; ?>">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-blue hover-expand-effect" style="cursor: pointer;">
                        <div class="icon">
                            <i class="material-icons">payment</i>
                        </div>
                        <div class="content">
                            <div class="text">USER PAYMENT</div>
                            <div class="number count-to" data-from="0" data-to="<?= $userpaymentCount ?>" data-speed="1000" data-fresh-interval="20"><?= $userpaymentCount ?></div>
                        </div>
                    </div>

                </div>
            </a>
            
            <a href="<?= NEWS_PATH; ?>">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-light-green hover-expand-effect" style="cursor: pointer;">
                        <div class="icon">
                            <i class="material-icons">forum</i>
                        </div>
                        <div class="content">
                            <div class="text">NEWS</div>
                            <div class="number count-to" data-from="0" data-to="<?= $newsCount ?>" data-speed="1000" data-fresh-interval="20"><?= $newsCount ?></div>
                        </div>
                    </div>

                </div>
            </a>
            
            <a href="<?= NEWSLETTER_PATH; ?>">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-teal hover-expand-effect" style="cursor: pointer;">
                        <div class="icon">
                            <i class="material-icons">call_made</i>
                        </div>
                        <div class="content">
                            <div class="text">NEWSLETTER</div>
                            <div class="number count-to" data-from="0" data-to="<?= $newsletterCount ?>" data-speed="1000" data-fresh-interval="20"><?= $newsletterCount ?></div>
                        </div>
                    </div>

                </div>
            </a>
            
            <a href="<?= FAQ_PATH; ?>">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-cyan hover-expand-effect" style="cursor: pointer;">
                        <div class="icon">
                            <i class="material-icons">help</i>
                        </div>
                        <div class="content">
                            <div class="text">FAQ</div>
                            <div class="number count-to" data-from="0" data-to="<?= $faqCount ?>" data-speed="1000" data-fresh-interval="20"><?= $faqCount ?></div>
                        </div>
                    </div>

                </div>
            </a>
            
            <a href="<?= TESTIMONIALS_PATH; ?>">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box bg-pink hover-expand-effect" style="cursor: pointer;">
                        <div class="icon">
                            <i class="material-icons">format_quote</i>
                        </div>
                        <div class="content">
                            <div class="text">TESTIMONIALS</div>
                            <div class="number count-to" data-from="0" data-to="<?= $testimonialsCount ?>" data-speed="1000" data-fresh-interval="20"><?= $testimonialsCount ?></div>
                        </div>
                    </div>

                </div>
            </a>
            
        </div>
        <!-- #END# Widgets -->
    </div>
</section>
<script>
    jQuery(document).ready(function () {
        //Widgets count
        $('.count-to').countTo();
    });
</script>