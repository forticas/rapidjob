<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
        <div style="width: 80%; margin: auto;font-size: 18px;color:#505050;font-family: Arial, sans-serif;">
            <div style="background-color: #E5E5E5;height: 60px;margin-bottom: 30px;padding: 10px;text-align: left;width: 100%">
                <img src="<?= ASSETS_PATH ?>images/logo_en.png" alt="<?= APP_NAME; ?>" width="200" height="50" />
            </div>
            <h4 style="font-size: 22px;">You have received a <span style="color: #29ABA3;">Newsletter E-mail</span></h4>
            <h4>Hello <?= !empty($name) ? trim($name) : "Dear" ?>,</h4>
            <div style="clear: both"></div>
            <p style="background-color: rgb(229, 229, 229); margin: 0px auto; height: 35px; padding: 14px 0px 0px 16px;"><?= trim($subject); ?></p>
            <div style="clear: both"></div>
            <div style="margin: 50px 0 0 30px;padding: 0;width: 100%;background-color: #ffffff;float: left;">
                <p style="margin:10px 0;"><?= trim($message); ?></p>
                <a style='background-color: #00A8E6;border-radius: 3px;color: rgb(255, 255, 255);display: block;font-size: 18px;font-weight: 700;line-height: 1.5em;margin-left: auto;margin-right: auto;padding: 8px 16px;text-decoration: none; width:150px;text-align: center;' href="<?= $unsubscribe_url; ?>">unsubscribe</a>
            </div>
            <div style="clear: both;"></div>
            <p>Regards,
                <br />Team <?= APP_NAME; ?>
            </p>
            <div style="clear: both;"></div>
            <div style="background-color: #E5E5E5; margin-bottom: 30px; margin-top: 20px;text-align: left; width: 100%; padding: 0px; height: 57px;">
                <p style="font-size: 14px; text-align: left; padding: 20px; color: rgb(255, 255, 255); background-color: rgba(0, 0, 0, 0.3);"><?= APP_NAME; ?> © <?= date("Y") ?> - All rights reserved - Legal notice - CGV</p>
            </div>
            <div style="clear: both"></div>
        </div>
    </body>
</html>