<?php
if ($this->session->userdata('admin_id') != NULL) {
    redirect(DASHBOARD_PATH);
}
?>
<body class="gray-bg">

    <div class="middle-box text-center loginscreen  animated fadeInDown">
        <div >
            <div>
                <h2>NETFISH</h2>
                <h1 class="logo-name margin_bottom">

                    <!--<img alt="Login" class="img-circle" height="180px" width="180px" src="<?php //echo APP_LOGO;             ?>" />-->
                </h1>

            </div>
            <h2>Password Reset</h2>
            <form method="post" class="form-horizontal" action="<?= LOGIN_PATH . "/verify_token/" . $token . '/' . $id; ?>" id="reset_password">

                <div class="form-group text-left">
                    <input type="password" id="password" name="password" class="form-control" placeholder="Password" required="" value="<?php echo empty($email) ? "" : $email; ?>" autofocus="true">
                    
                        <span class="error"><?php if (!empty(form_error('password'))) echo form_error('password'); ?></span>
                </div>
                <div class="form-group text-left">
                    <input type="password" id="cpassword" name="cpassword" class="form-control" placeholder="Confirm Password" required="">
                    <span class="error"><?php if (!empty(form_error('cpassword'))) echo form_error('cpassword'); ?></span>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary block full-width m-b">Change Password</button>
                </div>
                <?php echo form_close(); ?>

        </div>
    </div>
    <script>

    </script>