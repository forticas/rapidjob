<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<link href="<?= ADMIN_ASSETS_PATH ?>plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="<?= ADMIN_ASSETS_PATH ?>plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

<section class="content report-container">
    <div class="container-fluid">
        <?= $breadcrumb; ?>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>Settings</h2>
                    </div>
                    <div class="body">
                        <?php echo form_open_multipart(SETTING_PATH . '/add', 'id="setting_add_form" autocomplete="off"') ?>

                        <label for="payment_in_cent_per_email">Payment rate for send application by email (rate in euro)*</label>
                        <div class="col-sm-12 mp0">
                            <div class="payment_in_cent_per_email">
                                <div class="input-group">
                                    <span class="input-group-addon">&#8364;</span>
                                    <div class="form-line">
                                        <?php
                                        $payment_in_cent_per_email = array(
                                            "name" => "payment_in_cent_per_email",
                                            "id" => "payment_in_cent_per_email",
                                            "class" => "form-control required",
                                            "type" => "text",
                                            'value' => $setting_array['payment_in_cent_per_email']
                                        );
                                        echo form_input($payment_in_cent_per_email);
                                        ?>
                                    </div>
                                    <span class="input-group-addon">.00</span>
                                    <?php if (!empty(form_error('payment_in_cent_per_email'))) : ?>
                                        <label class="error"><?php echo form_error('payment_in_cent_per_email'); ?></label>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <label for="payment_rate_for_send_application_by_post">Payment rate for send application by post (rate in euro)*</label>
                        <div class="col-sm-12 mp0">
                            <div class="payment_in_cent_per_email">
                                <div class="input-group">
                                    <span class="input-group-addon">&#8364;</span>
                                    <div class="form-line">
                                        <?php
                                        $payment_rate_for_send_application_by_post = array(
                                            "name" => "payment_rate_for_send_application_by_post",
                                            "id" => "payment_rate_for_send_application_by_post",
                                            "class" => "form-control required",
                                            "type" => "text",
                                            'value' => $setting_array['payment_rate_for_send_application_by_post']
                                        );
                                        echo form_input($payment_rate_for_send_application_by_post);
                                        ?>
                                    </div>
                                    <span class="input-group-addon">.00</span>
                                    <?php if (!empty(form_error('payment_rate_for_send_application_by_post'))) : ?>
                                        <label class="error"><?php echo form_error('payment_rate_for_send_application_by_post'); ?></label>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <label for="recruiter_subscription_plan_price">Recruiter Subscription Plan Price (rate in euro) *</label>
                        <div class="col-sm-12 mp0">
                            <div class="payment_in_cent_per_email">
                                <div class="input-group">
                                    <span class="input-group-addon">&#8364;</span>
                                    <div class="form-line">
                                        <?php
                                        $recruiter_subscription_plan_price = array(
                                            "name" => "recruiter_subscription_plan_price",
                                            "id" => "recruiter_subscription_plan_price",
                                            "class" => "form-control required",
                                            "type" => "text",
                                            'value' => $setting_array['recruiter_subscription_plan_price']
                                        );
                                        echo form_input($recruiter_subscription_plan_price);
                                        ?>
                                    </div>
                                    <span class="input-group-addon">.00</span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <label for="recruiter_subscription_plan_duration" class="required_star">Recruiter Subscription Plan Duration (Duration in month Ex. 3,6,9,12)*</label>
                        <div class="col-sm-12 mp0">
                            <div class="payment_in_cent_per_email">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">access_time</i>
                                    </span>
                                    <div class="form-line">
                                        <?php
                                        $recruiter_subscription_plan_duration = array(
                                            "name" => "recruiter_subscription_plan_duration",
                                            "id" => "recruiter_subscription_plan_duration",
                                            "class" => "form-control required",
                                            "type" => "text",
                                            'value' => $setting_array['recruiter_subscription_plan_duration']
                                        );
                                        echo form_input($recruiter_subscription_plan_duration);
                                        ?>
                                    </div>
                                    <span class="input-group-addon">
                                        <i class="material-icons">date_range</i>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
<!--                        <label for="contact_us_email" class="required_star">Contact us email *</label>  
                        <div class="col-sm-12 mp0">
                            <div class="payment_in_cent_per_email">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">email</i>
                                    </span>
                                    <div class="form-line">
                                        <?php
                                        $contact_us_email = array(
                                            "name" => "contact_us_email",
                                            "id" => "contact_us_email",
                                            "class" => "form-control required email",
                                            "type" => "text",
                                            'value' => $setting_array['contact_us_email']
                                        );
                                        echo form_input($contact_us_email);
                                        ?>
                                    </div>
                                    <span class="input-group-addon">
                                        <i class="material-icons">@</i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>-->

                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 form-control-label">
                            <a href="<?= DASHBOARD_PATH ?>" class="btn btn-default waves-effect cancel_button text-uppercase">Cancel</a>
                            <?php
                            $submit_button = array(
                                'type' => 'submit',
                                'class' => 'btn btn-primary waves-effect submit_button text-uppercase',
                                'content' => 'Update',
                            );
                            echo form_button($submit_button);
                            ?>
                        </div>
                        <div class="clearfix"></div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="<?= ASSETS_PATH ?>admin/js/pages/setting/setting.js" type="text/javascript"></script>