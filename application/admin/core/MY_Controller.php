<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $controller_name;
    public $method_name;

    public function __construct() {
        parent::__construct();
        $lang = !empty($this->session->userdata('language')) ? $this->session->userdata('language') : 'en';
        $this->config->set_item('language', $lang);
        $this->lang->load($lang,$lang);

        $controller_name = $this->router->fetch_class();

        $this->get_instance()->method_name = $this->router->fetch_method();
        $this->get_instance()->controller_name = $controller_name;

        $admin_without_security_token = array(
            'login'
        );
        if (!in_array($controller_name, $admin_without_security_token)) {
            if ($this->session->userdata('admin_id') == NULL) {
                redirect(LOGIN_PATH);
            }
        }
    }

    public function notfound() {

        $this->load->view('/errors/404'); //loading view
    }

    public function internal_server() {
        $this->load->view('/errors/500'); //loading view
        return false;
    }

    /**
     * This function is use for send email
     * @param array $to_email_address
     * @param string $subject
     * @param text $message
     */
    public function send_email($to_email_address, $subject, $message) {
        try {
            require_once SWIFT_MAILER_PATH;

            
            $transport = Swift_SmtpTransport::newInstance(EMAIL_HOST, EMAIL_PORT,MAIL_CERTI)
                    ->setUsername(EMAIL_USER)
                    ->setPassword(EMAIL_PASS);
            
            $mailer = Swift_Mailer::newInstance($transport);

            $mailLogger = new \Swift_Plugins_Loggers_ArrayLogger();
            $mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($mailLogger));
            
            $sendMessage = Swift_Message::newInstance($subject)
                    ->setFrom(array(EMAIL_FROM => APP_NAME))
                    ->setTo($to_email_address)
                    ->setBody($message, 'text/html');
            
            if (!$mailer->send($sendMessage)) {
                $headers = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
                $headers .= "X-Priority: 3\r\n";
                $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";
                $headers .= "From:" . APP_NAME . "<" . EMAIL_FROM . ">" . "\r\n";

                foreach ($to_email_address as $to) {
                    mail($to, $subject, $message, $headers);
                }
            }
        } catch (Exception $e) {            
            log_message("ERROR", $e->getMessage());
        }
    }

    /**
     * Removing extra slashes from the response string recursively
     * @param type $variable
     * @return type
     */
    function strip_slashes_recursive($variable) {
        if (is_null($variable))
            $variable = "";
        if (is_string($variable))
            return stripslashes($variable);
        if (is_array($variable))
            foreach ($variable as $i => $value)
                $variable[$i] = $this->strip_slashes_recursive($value);

        return $variable;
    }
    
    public function pr($arr){
        echo "<pre>";print_r($arr);die;
    }
    
    public function breadcrumb(){
        $controller = $this->router->fetch_class();
        $action = $this->router->fetch_method();
        $controller_icon  = '';
        $view_icon = '';
        $breadcrumb_str = '';
        
        if($action == 'add_testimonial') {
            $action = 'add testimonial';
        }
        
        if($action == 'edit_testimonial') {
            $action = 'edit testimonial';
        }
        
        if($action == 'add_faq') {
            $action = 'add FAQ';
        }
        
        if($action == 'edit_faq') {
            $action = 'edit FAQ';
        }
        

        
        switch ($controller){
            case 'users':
                $controller_icon = 'person';
                break;
            case 'settings':
                $controller_icon = 'settings';
                break;
            case 'dashboard':
                $controller_icon = 'home';
                break;
            case 'import_recruiters':
                $controller_icon = 'import_export';
                break;
            case 'news':
                $controller_icon = 'forum';
                break;
            case 'newsletter':
                $controller_icon = 'call_made';
                break;
            case 'userpayments':
                $controller_icon = 'payment';
                break;
            case 'managecms':
                $controller_icon = 'content_paste';
                break;

        }
        
        switch ($action){
            case 'add':
                $view_icon = 'add';
                break;
            case 'edit':
                $view_icon = 'edit';
                break;
            case 'view':
                $view_icon = 'view_list';
                break;
            case 'change_password':
                $action = 'Change Password';
                $view_icon = 'lock';
                break;
            case 'edit_profile':
                $action = 'Edit Profile';
                $view_icon = 'person';
                break;
        }
        
        if($controller == 'dashboard' && $action == 'index'){
            $breadcrumb_str = '<ol class="breadcrumb">
                <li class="active">
                    <i class="material-icons">home</i> Home
                </li>';
        }else{
            $breadcrumb_str = '<ol class="breadcrumb">
                <li class="active">
                    <a href="'.DASHBOARD_PATH.'"><i class="material-icons">home</i> Home</a>
                </li>';
        }
        
        if($action != 'index'){
            if($controller != 'dashboard'){
                $breadcrumb_str .= '<li class="active">
                                    <a href="'.BASE_URL.'/'.$controller.'"><i class="material-icons">'.$controller_icon.'</i> '.  ucfirst($controller).'</a>
                                </li>';
            }
            $breadcrumb_str .= '<li class="active">
                                    <i class="material-icons">'.$view_icon.'</i> '.  ucfirst($action).'
                                </li>';
        }else{
            if($controller != 'dashboard'){
                $breadcrumb_str .= '<li class="active">
                                        <i class="material-icons">'.$controller_icon.'</i> '.  ucfirst($controller).'
                                    </li>';
            }
        }
        
        $breadcrumb_str .= '</ol>';
        
        return $breadcrumb_str;
    }

}
