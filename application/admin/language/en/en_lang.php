<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//my controller message
$lang['mycontroller_invalid_request'] = "Invalid request data!";
$lang['mycontroller_invalid_token'] = "Invalid security token";
$lang['mycontroller_problem_request'] = "Problem in processing request!";
$lang['mycontroller_bad_request'] = "Invalid request parameters!";

//common controller
//resetpassword controller
$lang['resetpassword_token_empty'] = "Oops, something went wrong. Missing security token.";
$lang['resetpassword_token_invalid'] = "Oops, something went wrong. Invalid security code!";
$lang['resetpassword_reset_empty'] = "Direct access not allowed";
$lang['resetpassword_reset_success'] = "Password reset successfully.Use new password for login";
$lang['resetpassword_reset_fail'] = "Problem while reset password";

//User controller
$lang['user_login_success'] = "Successfully logged in";
$lang['user_login_token_generate_error'] = "Error in generate access token";
$lang['user_login_fail'] = "Invalid email or password";
$lang['user_login_block'] = "Your account is block by admin";

$lang['user_register_email_exist'] = "This email address already register with us.";
$lang['user_register_success'] = "Registration done successfully.";
$lang['user_register_fail'] = "Error while registering user.";

$lang['user_logout_success'] = "User is successfully logged out.";
$lang['user_logout_fail'] = "Error while perform logout.";

$lang['user_forgotpassword_success'] = "Reset password mail send successfully.";
$lang['user_forgotpassword_fail'] = "This email address is not registered with us.";


/*
 * Controller : Users
 * 
 */

// Method : change_status()
$lang['user_status_change_success'] = "Status change successfully.";
$lang['user_status_change_failure'] = "Problem in change status.";

// Method : delete()
$lang['user_delete_success'] = "User deleted successfully.";
$lang['user_delete_failure'] = "Problem in delete user.";


$lang['news_status_change_success'] = "Status change successfully.";
$lang['news_status_change_failure'] = "Problem in change status.";

// Method : delete()
$lang['news_delete_success'] = "News deleted successfully.";
$lang['news_delete_failure'] = "Problem in delete news.";


$lang['newsletter_status_change_success'] = "Status change successfully.";
$lang['newsletter_status_change_failure'] = "Problem in change status.";

// Method : delete()
$lang['newsletter_delete_success'] = "Newsletter deleted successfully.";
$lang['newsletter_delete_failure'] = "Problem in delete Newsletter.";