<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('DOCROOT', realpath(dirname(__FILE__) . '/../') . "/");
define('CRON_DOCROOT', realpath(dirname(__FILE__) . '/../../../') . "/admin/");


/*
  User defined Constants
 */


if (ENVIRONMENT == 'production') {
    define('DB_SERVER', 'localhost');
    define('DB_USER', 'forticas');
    define('DB_PASS', 'Forticas_18');
    define('DB_DATABASE', 'rapidjob');
    define('DOMAIN_URL', 'https://onlinecoin.io/');
    define('BASE_URL', DOMAIN_URL . 'admin');
    define('PHP_PATH', '/usr/bin/php7.2');
} else {
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    define('DB_SERVER', 'localhost');
    define('DB_USER', 'forticas');
    define('DB_PASS', 'Forticas_18');
    define('DB_DATABASE', 'rapidjob');
    define('DOMAIN_URL', 'https://onlinecoin.io/');
    define('BASE_URL', DOMAIN_URL . 'admin');
    define('PHP_PATH', '/usr/bin/php7.2');
}

//Database Table name Constants
define("TBL_PREFIX", "rj_");
define("TBL_ADMINS", TBL_PREFIX . "admins");
define("TBL_COMPANY_DETAILS", TBL_PREFIX . "company_details");
define("TBL_COMPANY_FAVOURITES", TBL_PREFIX . "company_favourites");
define("TBL_EDUCATIONS", TBL_PREFIX . "educations");
define("TBL_ERROR_LOD", TBL_PREFIX . "error_logs");
define("TBL_NEWSLETTERS", TBL_PREFIX . "newsletters");
define("TBL_SETTINGS", TBL_PREFIX . "settings");
define("TBL_USERS", TBL_PREFIX . "users");
define("TBL_USER_APPLICATIONS", TBL_PREFIX . "user_applications");
define("TBL_USER_CVS", TBL_PREFIX . "user_cvs");
define("TBL_USER_DETAILS", TBL_PREFIX . "user_details");
define("TBL_USER_DOCUMENT_FILES", TBL_PREFIX . "user_document_files");
define("TBL_USER_EXPERIANCES", TBL_PREFIX . "user_experiances");
define("TBL_USER_FAVOURITES", TBL_PREFIX . "user_favourites");
define("TBL_USER_HOBBIES", TBL_PREFIX . "user_hobbies");
define("TBL_USER_LANGUAGES", TBL_PREFIX . "user_languages");
define("TBL_USER_SKILLS", TBL_PREFIX . "user_skills");
define("TBL_USER_TRAININGS", TBL_PREFIX . "user_trainings");
define("TBL_WORK_ACTIVITY_AREA", TBL_PREFIX . "work_activity_areas");
define("TBL_WORK_JOB", TBL_PREFIX . "work_jobs");
define("TBL_WORK_PLACES", TBL_PREFIX . "work_places");
define("TBL_INVITE_FRIENDS", TBL_PREFIX . "invite_friends");
define("TBL_FAILED_IMPORT_RECRUITERS", TBL_PREFIX . "failed_import_recruiters");
define("TBL_NEWS", TBL_PREFIX . "news");
define("TBL_USER_PAYMENTS", TBL_PREFIX . "user_payments");
define("TBL_SEND_NEWSLATTER", TBL_PREFIX . "send_newslatter");
define("TBL_CMS", TBL_PREFIX . "manage_cms");
define("TBL_TESTIMONIAL", TBL_PREFIX . "cms_testimonials");
define("TBL_FAQ",TBL_PREFIX."cms_faq");
define("TBL_USER_ALL_DOCUMENT_FILES", TBL_PREFIX . "user_all_document_files");

//Path Constants
define("ASSETS_PATH", DOMAIN_URL . 'assets/');
define("ADMIN_ASSETS_PATH", ASSETS_PATH . 'admin/');

define("LOGIN_PATH", BASE_URL . '/login');
define("DASHBOARD_PATH", BASE_URL . '/dashboard');
define("USERS_PATH", BASE_URL . '/users');
define("NOTIFICATIONS_PATH", BASE_URL . '/notifications');
define("SETTING_PATH", BASE_URL . '/settings');
define("IMPORT_RECRUITERS_PATH", BASE_URL . '/import_recruiters');
define("NEWS_PATH", BASE_URL . '/news');
define("NEWSLETTER_PATH", BASE_URL . '/newsletter');
define("USER_PAYMENTS_PATH", BASE_URL . '/userpayments');
define("POST_APPLICATION_PATH", BASE_URL . '/postapplication');
define("MANAGE_CMS_PATH", BASE_URL.'/managecms');
define("FAQ_PATH", MANAGE_CMS_PATH.'/faq');
define("TESTIMONIALS_PATH", MANAGE_CMS_PATH.'/testimonials');

//Upload Constants
define("UPLOAD_REL_PATH", '../uploads/');
define("UPLOAD_ABS_PATH", DOMAIN_URL . 'uploads/');

//Folder Constant
define("UPLOAD_FILE_FOLDER", 'uploads');
define("DEFAULT_IMAGE_FOLDER", 'images');
define("UPLOAD_USERS_FOLDER", 'users');
define("UPLOAD_DOCUMENTS_FOLDER", 'documents');
define("UPLOAD_TEMP_FILE_FOLDER", 'temp');
define("UPLOAD_NEWS_FOLDER", 'news');
define("UPLOAD_CMS_FOLDER", 'managecms');

//Email Constants

define('EMAIL_HOST', 'smtp.gmail.com');
define('EMAIL_USER', 'tanvimehta405@gmail.com');
define('EMAIL_PASS', 'imobdev1234');
define("EMAIL_PORT","587");
define('EMAIL_FROM', 'no-reply@emaildomain.com');
define('MAIL_CERTI', "tls");
define('SWIFT_MAILER_PATH', '../swiftmailer/lib/swift_required.php');

//Push Notification Constants
//Android
define('ANDROID_FCM_KEY', '');


//IOS
define('PUSH_ENVIRONMENT', true);
if (PUSH_ENVIRONMENT) {
    define('IOS_PEM_PATH', '../pem/apns_prod.pem');
    define('APP_IS_LIVE', 'true');
} else {
    define('IOS_PEM_PATH', '../pem/apns_dev.pem');
    define('APP_IS_LIVE', 'false');
}

//Other contains (AWS S3 Bucket, SMS Gateway, Google MAP KEY, etc.)
define('APP_NAME', 'Rapid Job');
define('APP_SHORT_NAME', 'RJ');
define('APP_VERSION', '0.1.2');
define('ALLOW_IOS_PUSH', false); // true or false
define('ALLOW_ANDROID_PUSH', false); // true or false
define('DATE_FORMAT', 'd-m-Y h:i a');
define('DATE_FORMAT_JS', 'dd-MM-yyyy hh:mm:ss TT');
define("ALLOW_MULTI_LOGIN", false); // true or false
define("IS_MULTI_SITE", false); // true or false
define("DEFAULT_LANGUAGE", 'english'); // true or false
define('DEFAULT_IMPORT_EXCEL_SIZE_LIMIT', 20000000); // 20MB
define('DEFAULT_NEWS_TITLE_TEXT_LIMIT', 20);


//IMAGE Constants
define("IMAGE_MANIPULATION_URL", DOMAIN_URL . "image.php?");
define('DEFAULT_USER_IMAGE_ABS', ASSETS_PATH . DEFAULT_IMAGE_FOLDER . '/default.png');
define('DEFAULT_NO_IMAGE_ABS', ASSETS_PATH . DEFAULT_IMAGE_FOLDER . '/no-image.png');

define('DEFAULT_USER_IMAGE_REL', 'assets/' . DEFAULT_IMAGE_FOLDER . '/default.png');
define('DEFAULT_NO_IMAGE_REL', 'assets/' . DEFAULT_IMAGE_FOLDER . '/no-image.png');

define("DEFAULT_COMPANY_IMAGE_NAME", 'recruitor_placeholder.png'); //placeholder_icon.png
define("DEFAULT_COVER_LATTER_IMAGE_NAME", 'doc_image.png');

define("DEFAULT_FULL_STAR", ASSETS_PATH . DEFAULT_IMAGE_FOLDER . '/star_full.png');
define("DEFAULT_HALF_STAR", ASSETS_PATH . DEFAULT_IMAGE_FOLDER . '/star_half.png');

define("DEFAULT_LOGO", ASSETS_PATH . DEFAULT_IMAGE_FOLDER . '/logo_en.png');
define("DEFAULT_FAVICON_ICON", ASSETS_PATH . DEFAULT_IMAGE_FOLDER . '/favicon.ico');

/*************************** ANIMATION *************************************/
define('THEME_PAGE_ANIMATION','animated fadeInRight');
