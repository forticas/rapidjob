<?php

/**
 * 
 * This controller is for handle news master activity
 * 
 * @author Hardik Patel
 * Modified Date :- 2017-05-09
 * 
 */
class News extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->timestamp = 0;
        $this->utc_time = time();
        if (isset($_POST) && !empty($_POST['timestamp'])) {
            $this->timestamp = $this->Common_model->escape_data($this->input->get_post("timestamp"));
        }
    }

    /**
     * 
     * This function is use for show jqgrid show data view
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-09
     * 
     */
    public function index() {
        $data = array(
            'breadcrumb' => $this->breadcrumb()
        );

        $this->load->view("common/header");
        $this->load->view("common/sidebar");
        $this->load->view("news/index", $data);
        $this->load->view("common/footer");
    }

    /**
     * 
     * This function is use for get all data process it and send as json formate
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-09
     * 
     */
    public function get_all_data() {
        $page = !empty($this->input->get_post("start")) ? $this->input->get_post("start") : 0;
        $rows = !empty($this->input->get_post("length")) ? $this->input->get_post("length") : 10;
        $draw = !empty($this->input->get_post("draw")) ? $this->input->get_post("draw") : 1;

        $sidx = !empty($this->input->get_post("order")[0]['column']) ? $this->input->get_post("order")[0]['column'] : 0;
        $sord = !empty($this->input->get_post("order")[0]['dir']) ? $this->input->get_post("order")[0]['dir'] : 'ASC';

        $status = !empty($this->input->get_post("status")) ? $this->input->get_post("status") : '';
        $status_filter = !empty($this->input->get_post("status_filter")) ? $this->input->get_post("status_filter") : '';
        $global_search = !empty($this->input->get_post("global_filter")) ? $this->input->get_post("global_filter") : '';

        if ($sidx == 0) {
            $sidx = 'news_name_en';
        } elseif ($sidx == 1) {
            $sidx = 'news_name_fr';
        } else {
            $sidx = 'news_created_date';
        }

        $where = '';
        $order_by = $sidx . ' ' . $sord;
        $limit = $page . ',' . $rows;

        if (!empty($global_search)) {
            $where .= ' AND (
                            news_name_en like "%' . $global_search . '%" 
                        OR 
                            news_name_fr like "%' . $global_search . '%"
                        ) ';
        }

        if (!empty($status_filter)) {
            $where .= ' AND news_status=' . $status_filter . ' ';
        }

        $group_by = '';
        $columns = "news_id,news_name_en,news_name_fr,news_created_date,news_status";

        $news_list_sql = "SELECT " . $columns . " FROM " . TBL_NEWS . " WHERE news_status !=9" . $where . ' ORDER BY ' . $order_by . ' LIMIT ' . $limit;

        $news_count_sql = "SELECT news_id FROM " . TBL_NEWS . " WHERE news_status !=9" . $where;

        $total_rows = $this->Common_model->get_count_by_query($news_count_sql);
        $newsData = $this->Common_model->get_all_rows_by_query($news_list_sql);

        $resultArr = array();
        if (!empty($newsData) && is_array($newsData) && count($newsData) > 0) {
            $i = 0;
            foreach ($newsData as $value) {

                $news_name_en = get_slash_formatted_text($value['news_name_en']);
                $news_name_en = !empty($news_name_en) ? strlen($news_name_en) > DEFAULT_NEWS_TITLE_TEXT_LIMIT ? substr($news_name_en, 0, DEFAULT_NEWS_TITLE_TEXT_LIMIT) . "..." : $news_name_en : '';

                $news_name_fr = get_slash_formatted_text($value['news_name_fr']);
                $news_name_fr = !empty($news_name_fr) ? strlen($news_name_fr) > DEFAULT_NEWS_TITLE_TEXT_LIMIT ? mb_substr($news_name_fr, 0, DEFAULT_NEWS_TITLE_TEXT_LIMIT) . "..." : $news_name_fr : '';

                $status = ($value['news_status'] == 1) ? "checked" : '';
                $resultArr[$i]['name_en'] = $news_name_en;
                $resultArr[$i]['name_fr'] = $news_name_fr;
                $resultArr[$i]['created_at'] = date(DATE_FORMAT, (int) ($value['news_created_date'] + $this->timestamp));
                $resultArr[$i]['status'] = '<div class="switch"><label><input type="checkbox" class="status_change ct_switch"  data-id="' . $value['news_id'] . '" value="' . $value['news_status'] . '" ' . $status . '><span class="lever switch-col-blue"></span></label></div>';
                $resultArr[$i]['action'] = '<a class="btn bg-green waves-effect set_datatable_view_button" 
                                                href="' . NEWS_PATH . '/view/' . $value['news_id'] . '">
                                                    <i class="material-icons icon_15">remove_red_eye</i> 
                                            </a> 
                                            
                                            <a class="btn btn-primary waves-effect set_datatable_edit_button" 
                                                href="' . NEWS_PATH . '/edit/' . $value['news_id'] . '">
                                                    <i class="material-icons icon_15">edit</i> 
                                            </a> 
                                            
                                            <a class="btn btn-danger btn-xs delete_data_button" 
                                                href="javascript:void(0)" data-id=' . $value['news_id'] . '> 
                                                    <i class="material-icons icon_15">delete_forever</i> 
                                            </a>';
                $i++;
            }
        }

        $response = array();
        $response['draw'] = (int) $draw;
        $response['recordsTotal'] = (int) $total_rows;
        $response['recordsFiltered'] = (int) $total_rows;
        $response['data'] = $resultArr;
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * 
     * This function is use for add new user
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-09
     * 
     */
    public function add() {
        $this->form_validation->set_rules('news_name_en', 'News Name', 'required');
        $this->form_validation->set_rules('news_name_fr', 'News Name', 'required');
        $this->form_validation->set_rules('news_description_en', 'Description', 'required');
        $this->form_validation->set_rules('news_description_fr', 'Description', 'required');

        if ($this->form_validation->run() !== FALSE) {
            if ($this->input->post() != NULL) {

                $request_data['news_name_en'] = get_slash_formatted_text($this->Common_model->escape_data($this->input->get_post("news_name_en")));
                $request_data['news_name_fr'] = get_slash_formatted_text($this->Common_model->escape_data($this->input->get_post("news_name_fr")));
                $request_data['news_description_en'] = get_slash_formatted_text($this->Common_model->escape_data($this->input->get_post("news_description_en")));
                $request_data['news_description_fr'] = get_slash_formatted_text($this->Common_model->escape_data($this->input->get_post("news_description_fr")));
                $request_data['news_created_date'] = $this->utc_time;

                $news_id = $this->Common_model->insert(TBL_NEWS, $request_data);
                if ($news_id > 0) {
                    if (!empty($_FILES['photo']['name']) && $_FILES['photo']['error'] == 0) {
                        $upload_path = UPLOAD_REL_PATH . "/" . UPLOAD_NEWS_FOLDER . "/" . $news_id;
                        $new_image = do_upload($upload_path, $_FILES, true);
                        if (!empty($new_image)) {
                            $where = array(
                                "news_id" => $news_id
                            );
                            $update_array = array(
                                "news_image_name" => $new_image
                            );
                            $is_updated = $this->Common_model->update(TBL_NEWS, $update_array, $where);
                        }
                    }
                    $this->session->set_flashdata("success", "News Added successfully");
                    redirect(NEWS_PATH);
                }
            }
        }

        $dataArr = array(
            'breadcrumb' => $this->breadcrumb()
        );
        $this->load->view("common/header");
        $this->load->view("common/sidebar");
        $this->load->view("news/add", $dataArr);
        $this->load->view("common/footer");
    }

    /**
     * 
     * This function is use for edit user data
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-09
     * 
     */
    public function edit($news_id = '') {
        if (empty($news_id)) {
            $this->session->set_flashdata("failure", "News Not Exist");
            redirect(NEWS_PATH);
        }
        $this->form_validation->set_rules('news_id', 'News Id', 'required');
        $this->form_validation->set_rules('news_name_en', 'News Name', 'required');
        $this->form_validation->set_rules('news_name_fr', 'News Name', 'required');
        $this->form_validation->set_rules('news_description_en', 'News Description', 'required');
        $this->form_validation->set_rules('news_description_fr', 'News Description', 'required');

        if ($this->form_validation->run() !== FALSE) {
            if ($this->input->post() != NULL) {

                $request_data['news_name_en'] = $this->Common_model->escape_data($this->input->get_post("news_name_en"));
                $request_data['news_name_fr'] = $this->Common_model->escape_data($this->input->get_post("news_name_fr"));
                $request_data['news_description_en'] = $this->Common_model->escape_data($this->input->get_post("news_description_en"));
                $request_data['news_description_fr'] = $this->Common_model->escape_data($this->input->get_post("news_description_fr"));
                $request_data['news_modified_date'] = $this->utc_time;

                $where = array(
                    "news_id" => $news_id
                );

                if (!empty($_FILES['photo']['name']) && $_FILES['photo']['error'] == 0) {
                    $upload_path = UPLOAD_REL_PATH . "/" . UPLOAD_NEWS_FOLDER . "/" . $news_id;
                    $new_image = do_upload($upload_path, $_FILES, true);
                    if (!empty($new_image)) {
                        $file_name = $this->Common_model->get_single_row(TBL_NEWS, "news_image_name", $where);
                        @unlink($upload_path . '/' . $file_name['news_image_name']);
                        $request_data["news_image_name"] = $new_image;
                    }
                }
                $update_id = $this->Common_model->update(TBL_NEWS, $request_data, $where);
                if ($update_id > 0) {
                    $this->session->set_flashdata("success", "News Updated successfully");
                    redirect(NEWS_PATH);
                }
            }
        }
        $where = array(
            "news_id" => $news_id
        );

        $news_data = $this->Common_model->get_single_row(TBL_NEWS, '*', $where);
        if (empty($news_data)) {
            $this->session->set_flashdata("failure", "News Not Exist");
            redirect(NEWS_PATH);
        }

        $view_data = array();
        $view_data['news_id'] = $news_data['news_id'];
        $view_data['news_name_en'] = $news_data['news_name_en'];
        $view_data['news_name_fr'] = $news_data['news_name_fr'];
        $view_data['news_description_en'] = $news_data['news_description_en'];
        $view_data['news_description_fr'] = $news_data['news_description_fr'];

        if (!empty($news_data['news_image_name'])) {
            if (file_exists(UPLOAD_REL_PATH . UPLOAD_NEWS_FOLDER . '/' . $news_id . "/" . $news_data['news_image_name'])) {
                $view_data['image_url'] = UPLOAD_ABS_PATH . UPLOAD_NEWS_FOLDER . '/' . $news_id . "/" . $news_data['news_image_name'];
            } else {
                $view_data['image_url'] = DEFAULT_USER_IMAGE_ABS;
            }
        } else {
            $view_data['image_url'] = DEFAULT_USER_IMAGE_ABS;
        }

        $view_data['breadcrumb'] = $this->breadcrumb();

        $this->load->view("common/header");
        $this->load->view("common/sidebar");
        $this->load->view("news/edit", $view_data);
        $this->load->view("common/footer");
    }

    /**
     * 
     * This function is use for view user data
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-09
     * 
     */
    public function view($news_id = '') {
        if (empty($news_id)) {
            $this->session->set_flashdata("failure", "News Not Exist");
            redirect(NEWS_PATH);
        }
        $where = array(
            "news_id" => $news_id
        );

        $news_data = $this->Common_model->get_single_row(TBL_NEWS, '*', $where);
        if (empty($news_data)) {
            $this->session->set_flashdata("failure", "News Not Exist");
            redirect(NEWS_PATH);
        }

        $view_data = array();
        $view_data['news_id'] = $news_data['news_id'];
        $view_data['news_name_en'] = $news_data['news_name_en'];
        $view_data['news_name_fr'] = $news_data['news_name_fr'];
        $view_data['news_description_en'] = $news_data['news_description_en'];
        $view_data['news_description_fr'] = $news_data['news_description_fr'];
        $view_data['created_date'] = date(DATE_FORMAT, (int) ($news_data['news_created_date'] + $this->timestamp));
        $view_data['status'] = $news_data['news_status'];

        if (!empty($news_data['news_image_name'])) {
            if (file_exists(UPLOAD_REL_PATH . UPLOAD_NEWS_FOLDER . '/' . $news_id . "/" . $news_data['news_image_name'])) {
                $view_data['image_url'] = UPLOAD_ABS_PATH . UPLOAD_NEWS_FOLDER . '/' . $news_id . "/" . $news_data['news_image_name'];
            } else {
                $view_data['image_url'] = DEFAULT_USER_IMAGE_ABS;
            }
        } else {
            $view_data['image_url'] = DEFAULT_USER_IMAGE_ABS;
        }

        $view_data['breadcrumb'] = $this->breadcrumb();
        $this->load->view("common/header");
        $this->load->view("common/sidebar");
        $this->load->view("news/view", $view_data);
        $this->load->view("common/footer");
    }

    /**
     * 
     * This function is use for active / inactive user status
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-09
     * 
     * @return type
     * 
     */
    public function change_status() {
        $news_id = $this->Common_model->escape_data($this->input->get_post("id"));
        $status = $this->Common_model->escape_data($this->input->get_post("status"));
        $response = array();
        if (empty($news_id) || empty($status)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = 'false';
                $response['message'] = lang("mycontroller_invalid_request");
            }
        } else {
            $where = array();
            $where['news_id'] = $news_id;

            $request_data = array();
            $request_data['news_status'] = $status;
            $request_data['news_modified_date'] = $this->utc_time;

            $update_state = $this->Common_model->update(TBL_NEWS, $request_data, $where);
            if ($update_state > 0) {
                $response['success'] = 'true';
                $response['message'] = lang("news_status_change_success");
            } else {
                $response['message'] = lang("news_status_change_failure");
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * 
     * This function is use for delete user
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-09
     * 
     * @return type
     * 
     */
    public function delete() {
        $news_id = $this->Common_model->escape_data($this->input->get_post("id"));

        $response = array();
        if (empty($news_id)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = 'false';
                $response['message'] = lang("mycontroller_invalid_request");
            }
        } else {
            $where = array();
            $where['news_id'] = $news_id;

            $request_data = array();
            $request_data['news_status'] = 9;
            $request_data['news_modified_date'] = $this->utc_time;

            $update_state = $this->Common_model->update(TBL_NEWS, $request_data, $where);
            if ($update_state > 0) {
                $response['success'] = 'true';
                $response['message'] = lang("news_delete_success");
            } else {
                $response['message'] = lang("news_delete_failure");
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

}
