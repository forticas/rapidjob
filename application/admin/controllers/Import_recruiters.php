<?php

/**
 * 
 * This controller manage dashboard
 * 
 * @author Nitinkumar Vaghani
 * Modified Date :- 2017-05-09
 * 
 */
class Import_recruiters extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * This function is use to display send push notification form
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     */
    public function index() {
        $dataArr = array(
            'breadcrumb' => $this->breadcrumb()
        );
        $this->load->view("common/header");
        $this->load->view("common/sidebar");
        $this->load->view("import_recruiters/index", $dataArr);
        $this->load->view("common/footer");
    }

    /**
     * This function is use to display send push notification form
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     */
    public function store_recruiters_data() {
        $response_message = $success = '';

        try {
            if (isset($_FILES) && count($_FILES) > 0 && !empty($_FILES['recruiter_file']['name'])) {
                if (isset($_FILES['recruiter_file']['name']) && $_FILES['recruiter_file']['error'] == 0) {

                    $file_name = $_FILES['recruiter_file']['name'];
                    $ext = pathinfo($file_name, PATHINFO_EXTENSION);

                    if ($_FILES['recruiter_file']['size'] > DEFAULT_IMPORT_EXCEL_SIZE_LIMIT) {
                        $this->session->set_flashdata("failure", "Recruiter file size is must be less then " . DEFAULT_IMPORT_EXCEL_SIZE_LIMIT . "MB.");
                        redirect(IMPORT_RECRUITERS_PATH);
                    }

                    //validate file extension
                    if ($ext == "xlsx" || $ext == "xls") {
                        $upload_path = UPLOAD_REL_PATH . "/" . UPLOAD_TEMP_FILE_FOLDER;
                        $uploaded_file = do_upload($upload_path, $_FILES, false);

                        if (!empty($uploaded_file)) {
                            chmod($upload_path . '/' . $uploaded_file, 0777);

                            // send uploaded file to myCron controller and import recruiter data
//                            $file = CRON_DOCROOT . "index.php myCron/store_recruiters_data/";
                            $file = CRON_DOCROOT . "index.php myCron/store_recruiters_data/" . $uploaded_file;
                            exec(PHP_PATH . " " . $file . " > /dev/null &");
                            $this->session->set_flashdata('success', 'Your recruters data importing soon....');
                        } else {
                            $this->session->set_flashdata('failure', 'Problem while import recruiter file. Try again..!');
                        }
                    } else {
                        $this->session->set_flashdata('failure', 'Recruiter file type or extention is invalid. File type must be xls or xlsx');
                    }
                } else {
                    $this->session->set_flashdata('failure', 'Recruiter file type or extention is invalid.');
                }
            } else {
                $this->session->set_flashdata('failure', 'Recruiter file is missing');
            }
        } catch (Exception $e) {
            $this->session->set_flashdata('failure', 'Problem in import recruiter data');
            redirect(IMPORT_RECRUITERS_PATH);
        }
        redirect(IMPORT_RECRUITERS_PATH);
    }

}

?>
