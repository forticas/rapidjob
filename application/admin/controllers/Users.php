<?php

/**
 * 
 * This controller is for handle user master activity
 * 
 * @author Nitinkumar Vaghani
 * Modified Date :- 2017-05-09
 * 
 */
class Users extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("User_model");

        $this->timestamp = 0;
        $this->utc_time = time();
        if (isset($_POST) && !empty($_POST['timestamp'])) {
            $this->timestamp = $this->Common_model->escape_data($this->input->get_post("timestamp"));
        }
    }

    /**
     * 
     * This function is use for show jqgrid show data view
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     */
    public function index() {
        $data = array(
            'breadcrumb' => $this->breadcrumb()
        );

        $this->load->view("common/header");
        $this->load->view("common/sidebar");
        $this->load->view("users/index", $data);
        $this->load->view("common/footer");
    }

    /**
     * 
     * This function is use for get all data process it and send as json formate
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     */
    public function get_all_data() {
        $page = !empty($this->input->get_post("start")) ? $this->input->get_post("start") : 0;
        $rows = !empty($this->input->get_post("length")) ? $this->input->get_post("length") : 10;
        $draw = !empty($this->input->get_post("draw")) ? $this->input->get_post("draw") : 1;

        $sidx = !empty($this->input->get_post("order")[0]['column']) ? $this->input->get_post("order")[0]['column'] : 0;
        $sord = !empty($this->input->get_post("order")[0]['dir']) ? $this->input->get_post("order")[0]['dir'] : 'ASC';

        $status = !empty($this->input->get_post("status")) ? $this->input->get_post("status") : '';
        $status_filter = !empty($this->input->get_post("status_filter")) ? $this->input->get_post("status_filter") : '';
        $email_filter = !empty($this->input->get_post("email_filter")) ? $this->input->get_post("email_filter") : '';
        $search = !empty($this->input->get_post("name_filter")) ? $this->input->get_post("name_filter") : '';
        $user_type = !empty($this->input->get_post("user_type")) ? $this->input->get_post("user_type") : '';

        if ($sidx == 0) {
            $sidx = 'u_first_name';
        } else if ($sidx == 1) {
            $sidx = 'u_email';
        } else if ($sidx == 2) {
            $sidx = 'u_created_date';
        }

        $where = '';
        $order_by = $sidx . ' ' . $sord;
        $limit = $page . ',' . $rows;

        if (!empty($search)) {
            $where .= ' AND ((u_first_name like "%' . $search . '%") OR (u_last_name like "%' . $search . '%") OR (u_email like "%' . $search . '%"))';
        }

        if (!empty($email_filter)) {
            $where .= ' AND u_email like "%' . $email_filter . '%"';
        }

        if (!empty($status_filter)) {
            $where .= ' AND u_status=' . $status_filter;
        }
        if (!empty($user_type)) {
            $where .= ' AND u_type = '.$user_type;
        }

        $group_by = '';
        $columns = "u_id,u_type,u_first_name,u_last_name,u_email,u_created_date,u_status";

        $users_list_sql = "SELECT " . $columns . " FROM " . TBL_USERS . " WHERE u_status !=9" . $where . ' ORDER BY ' . $order_by . ' LIMIT ' . $limit;

        $users_count_sql = "SELECT u_id FROM " . TBL_USERS . " WHERE u_status !=9 " . $where;

        $total_rows = $this->Common_model->get_count_by_query($users_count_sql);
        
        $userData = $this->Common_model->get_all_rows_by_query($users_list_sql);
        
        $resultArr = array();
        if (!empty($userData) && is_array($userData) && count($userData) > 0) {
            $i = 0;
            foreach ($userData as $value) {
                $status = ($value['u_status'] == 1) ? "checked" : '';
                
                if($value['u_type']==1){
                    $value['user_type'] = "Candidate";
                }else{
                    $value['user_type'] = "Recruiter";
                }
                
                $resultArr[$i]['name'] = !empty($value['u_first_name']) ? get_slash_formatted_text(trim(ucfirst($value['u_first_name']." ".$value['u_first_name']))) : ' - - - ';
                $resultArr[$i]['email'] = $value['u_email'];
                $resultArr[$i]['user_type'] = $value['user_type'];
                $resultArr[$i]['created_at'] = date(DATE_FORMAT, (int) ($value['u_created_date'] + $this->timestamp));
                $resultArr[$i]['status'] = '<div class="switch"><label><input type="checkbox" class="status_change ct_switch"  data-id="' . $value['u_id'] . '" value="' . $value['u_status'] . '" ' . $status . '><span class="lever switch-col-blue"></span></label></div>';
                $resultArr[$i]['action'] = '<a class="btn bg-green waves-effect set_datatable_view_button" 
                                                href="' . USERS_PATH . '/view/' . $value['u_id'] . '">
                                                    <i class="material-icons icon_15">remove_red_eye</i> 
                                            </a> 
                                            
                                            <a class="btn btn-danger btn-xs delete_data_button" 
                                                href="javascript:void(0)" data-id=' . $value['u_id'] . '> 
                                                    <i class="material-icons icon_15">delete_forever</i> 
                                            </a>';
                // Edit user
//                <a class="btn btn-primary waves-effect set_datatable_edit_button" 
//                                                href="' . USERS_PATH . '/edit/' . $value['u_id'] . '">
//                                                    <i class="material-icons icon_15">edit</i> 
//                                            </a> 
                $i++;
            }
        }

        $response = array();
        $response['draw'] = (int) $draw;
        $response['recordsTotal'] = (int) $total_rows;
        $response['recordsFiltered'] = (int) $total_rows;
        $response['data'] = $resultArr;
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * 
     * This function is use for add new user
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     */
    public function add() {
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() !== FALSE) {
            if ($this->input->post() != NULL) {
                $request_data = array();
                $request_data['u_email'] = $this->Common_model->escape_data($this->input->get_post("email"));
                $where = array(
                    "LOWER(u_email)" => strtolower($request_data['u_email'])
                );
                /* check email exists or not */
                $user_data = $this->Common_model->get_single_row(TBL_USERS, "u_id", $where);
                if (!empty($user_data)) {
                    $this->form_validation->set_rules('email', "email", "exist");
                    $this->form_validation->set_message('exist', 'This email already registered.');
                    $this->form_validation->run();
                } else {
                    $request_data['u_first_name'] = $this->Common_model->escape_data($this->input->get_post("first_name"));
                    $request_data['u_last_name'] = $this->Common_model->escape_data($this->input->get_post("last_name"));
                    $request_data['u_password'] = password_hash(sha1($this->Common_model->escape_data($this->input->get_post("password"))), PASSWORD_BCRYPT);
                    $request_data['u_created_date'] = $this->utc_time;
                    $request_data['u_status'] = 1;

                    $user_id = $this->Common_model->insert(TBL_USERS, $request_data);
                    if ($user_id > 0) {
                        if (!empty($_FILES['photo']['name']) && $_FILES['photo']['error'] == 0) {
                            $upload_path = UPLOAD_REL_PATH . "/" . UPLOAD_USERS_FOLDER . "/" . $user_id;
                            $new_image = do_upload($upload_path, $_FILES, true);
                            if (!empty($new_image)) {
                                $where = array(
                                    "u_id" => $user_id
                                );
                                $update_array = array(
                                    "u_image" => $new_image
                                );
                                $is_updated = $this->Common_model->update(TBL_USERS, $update_array, $where);
                            }
                        }
                        $this->session->set_flashdata("success", "User Added successfully");
                        redirect(USERS_PATH);
                    }
                }
            }
        }

        $dataArr = array(
            'breadcrumb' => $this->breadcrumb()
        );
        $this->load->view("common/header");
        $this->load->view("common/sidebar");
        $this->load->view("users/add", $dataArr);
        $this->load->view("common/footer");
    }

    /**
     * 
     * This function is use for edit user data
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     */
    public function edit($user_id = '') {
        if (empty($user_id)) {
            $this->session->set_flashdata("failure", "User Not Exist");
            redirect(USERS_PATH);
        }
        $this->form_validation->set_rules('user_id', 'User Id', 'required');
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        if ($this->form_validation->run() !== FALSE) {
            if ($this->input->post() != NULL) {
                $request_data = array();
                $request_data['u_email'] = $this->Common_model->escape_data($this->input->get_post("email"));
                $where = array(
                    "u_id !=" => $user_id,
                    "LOWER(u_email)" => strtolower($request_data['u_email'])
                );

                /* check email exists or not */
                $user_data = $this->Common_model->get_single_row(TBL_USERS, "u_id", $where);
                if (!empty($user_data)) {
                    $this->form_validation->set_rules('email', "email", "exist");
                    $this->form_validation->set_message('exist', 'This email already registered.');
                    $this->form_validation->run();
                } else {
                    $request_data['u_first_name'] = $this->Common_model->escape_data($this->input->get_post("first_name"));
                    $request_data['u_last_name'] = $this->Common_model->escape_data($this->input->get_post("last_name"));
                    $password = $this->Common_model->escape_data($this->input->get_post("password"));
                    if (!empty($password)) {
                        $request_data['u_password'] = password_hash(sha1($password), PASSWORD_BCRYPT);
                    }
                    $request_data['u_modified_date'] = $this->utc_time;

                    $where = array(
                        "u_id" => $user_id
                    );

                    if (!empty($_FILES['photo']['name']) && $_FILES['photo']['error'] == 0) {
                        $upload_path = UPLOAD_REL_PATH . "/" . UPLOAD_USERS_FOLDER . "/" . $user_id;
                        $new_image = do_upload($upload_path, $_FILES, true);
                        if (!empty($new_image)) {
                            $file_name = $this->Common_model->get_single_row(TBL_USERS, "u_image", $where);
                            @unlink($upload_path . '/' . $file_name['u_image']);
                            $request_data["u_image"] = $new_image;
                        }
                    }
                    $update_id = $this->Common_model->update(TBL_USERS, $request_data, $where);
                    if ($update_id > 0) {
                        $this->session->set_flashdata("success", "User Updated successfully");
                        redirect(USERS_PATH);
                    }
                }
            }
        }
        $where = array(
            "u_id" => $user_id
        );

        $user_data = $this->Common_model->get_single_row(TBL_USERS, '*', $where);
        if (empty($user_data)) {
            $this->session->set_flashdata("failure", "User Not Exist");
            redirect(USERS_PATH);
        }

        $view_data = array();
        $view_data['user_id'] = $user_data['u_id'];
        $view_data['first_name'] = $user_data['u_first_name'];
        $view_data['last_name'] = $user_data['u_last_name'];
        $view_data['email'] = $user_data['u_email'];

        if (!empty($user_data['u_image'])) {
            if (file_exists(UPLOAD_REL_PATH . UPLOAD_USERS_FOLDER . '/' . $user_id . "/" . $user_data['u_image'])) {
                $view_data['image_url'] = UPLOAD_ABS_PATH . UPLOAD_USERS_FOLDER . '/' . $user_id . "/" . $user_data['u_image'];
            } else {
                $view_data['image_url'] = DEFAULT_USER_IMAGE_ABS;
            }
        } else {
            $view_data['image_url'] = DEFAULT_USER_IMAGE_ABS;
        }

        $view_data['breadcrumb'] = $this->breadcrumb();

        $this->load->view("common/header");
        $this->load->view("common/sidebar");
        $this->load->view("users/edit", $view_data);
        $this->load->view("common/footer");
    }

    /**
     * 
     * This function is use for view user data
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     */
    public function view($user_id = '') {
        
        if (empty($user_id) || !is_numeric($user_id)) {
            $this->session->set_flashdata("failure", "User Not Exist");
            redirect(USERS_PATH);
        }
        
        $user_data = $this->User_model->get_user_details($user_id);
        
        if (empty($user_data)) {
            $this->session->set_flashdata("failure", "User Not Exist");
            redirect(USERS_PATH);
        }
        
        $user_all_documents = array();
        
        if ($user_data['u_type'] == 1) {
            $user_all_documents = $this->User_model->get_candidate_applications($user_id);
        } elseif ($user_data['u_type'] == 2) {
            $user_all_documents = $this->User_model->get_user_applications($user_id);
        }

        $view_data['image_url'] = DEFAULT_USER_IMAGE_ABS;

        if (!empty($user_data['u_image']) && file_exists(UPLOAD_REL_PATH . UPLOAD_USERS_FOLDER . '/' . $user_id . "/" . $user_data['u_image'])) {
            $view_data['image_url'] = UPLOAD_ABS_PATH . UPLOAD_USERS_FOLDER . '/' . $user_id . "/" . $user_data['u_image'];
        }
        
        $view_data['breadcrumb'] = $this->breadcrumb();
        $view_data['user_data'] = $user_data;
        $view_data['user_all_application'] = $user_all_documents;
        
        $this->load->view("common/header");
        $this->load->view("common/sidebar");
        $this->load->view("users/view", $view_data);
        $this->load->view("common/footer");
    }
    
    /**
     * 
     * This function is use for active / inactive user status
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     * @return type
     * 
     */
    public function change_status() {
        $user_id = $this->Common_model->escape_data($this->input->get_post("id"));
        $status = $this->Common_model->escape_data($this->input->get_post("status"));
        $response = array();
        if (empty($user_id) || empty($status)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = 'false';
                $response['message'] = lang("mycontroller_invalid_request");
            }
        } else {
            $where = array();
            $where['u_id'] = $user_id;

            $request_data = array();
            $request_data['u_status'] = $status;
            $request_data['u_modified_date'] = $this->utc_time;

            $update_state = $this->Common_model->update(TBL_USERS, $request_data, $where);
            if ($update_state > 0) {
                $response['success'] = 'true';
                $response['message'] = lang("user_status_change_success");
            } else {
                $response['message'] = lang("user_status_change_failure");
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * 
     * This function is use for delete user
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     * @return type
     * 
     */
    public function delete() {
        $user_id = $this->Common_model->escape_data($this->input->get_post("id"));

        $response = array();
        if (empty($user_id)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = 'false';
                $response['message'] = lang("mycontroller_invalid_request");
            }
        } else {
            $where = array();
            $where['u_id'] = $user_id;

            $request_data = array();
            $request_data['u_status'] = 9;
            $request_data['u_modified_date'] = $this->utc_time;

            $update_state = $this->Common_model->update(TBL_USERS, $request_data, $where);
            if ($update_state > 0) {
                $response['success'] = 'true';
                $response['message'] = lang("user_delete_success");
            } else {
                $response['message'] = lang("user_delete_failure");
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

}
