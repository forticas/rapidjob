<?php

/**
 * Description: Use this controller for background process
 * 
 * @author Nitinkumar Vaghani
 * Modified Date :- 2017-05-09
 * 
 */
class MyCron extends CI_Controller {

    public $log_file = "";

    public function __construct() {
        parent::__construct();
        $this->log_file = DOCROOT . 'logs/';
    }

    /**
     * Description: Use this function import company company details
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     * 
     * @param type $uploaded_file
     */
    public function store_recruiters_data($uploaded_file = '') {

        set_time_limit(0);

        $log_file = $this->log_file . $uploaded_file . '.txt';

        $this->benchmark->mark('code_start');
        $data = array();
        file_put_contents($log_file, "\n\n ============================================ Cron Start >__store_recruiters_data... Date[" . date('h:i:s a d-m-Y') . "] ===================================== \n\n", FILE_APPEND | LOCK_EX);

        file_put_contents($log_file, "\n ======== Currently importing " . $uploaded_file . " file ===== \n", FILE_APPEND | LOCK_EX);
        if (!empty($uploaded_file)) {
            $file_path = UPLOAD_REL_PATH . UPLOAD_TEMP_FILE_FOLDER . '/' . $uploaded_file;
            if (file_exists($file_path)) {
                $inputFileName = $file_path;
                //  Read your Excel sheet
                try {
                    $this->load->model('Importrecruiters_model');
                    $this->load->library('excel');
                    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                    $objPHPExcel = $objReader->load($inputFileName);
                } catch (Exception $e) {
                    file_put_contents($log_file, "\n ======== Exception." . json_encode($e->getMessage()) . " ....===== \n", FILE_APPEND | LOCK_EX);
                }

                $sheet = $objPHPExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();
                $success_count = $failure_count = 0;

                for ($row = 2; $row <= $highestRow; $row++) {
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    $data = array();
                    $alumny_data = array();
                    foreach ($rowData[0] as $k => $v) {

                        if ($k == 0 && !empty($v)) {
                            $data['u_first_name'] = trim(trim($this->Common_model->escape_data($v)));
                        }
                        if ($k == 1 && !empty($v)) {
                            $data['cd_address'] = trim($this->Common_model->escape_data($v));
                        }
                        if ($k == 2 && !empty($v)) {
                            $data['cd_address'] = trim($this->Common_model->escape_data($v));
                        }

                        if ($k == 3 && !empty($v)) {
                            $data['cd_postal_code'] = trim($this->Common_model->escape_data($v));
                        }

                        if ($k == 4 && !empty($v)) {
                            $data['cd_city'] = trim($this->Common_model->escape_data($v));
                        }

                        if ($k == 5 && !empty($v)) {
                            $data['cd_phone'] = trim($this->Common_model->escape_data($v));
                        }
                        if ($k == 6 && !empty($v)) {
                            $data['cd_alternate_phone'] = trim($this->Common_model->escape_data($v));
                        }
                        if ($k == 7 && !empty($v)) {
                            $data['cd_fax'] = trim($this->Common_model->escape_data($v));
                        }
                        if ($k == 8 && !empty($v)) {
                            $data['cd_activity_area'] = trim($v);
                        }
                        if ($k == 9 && !empty($v)) {
                            $data['cd_naf'] = trim($this->Common_model->escape_data($v));
                        }
                        if ($k == 10 && !empty($v)) {
                            $data['cd_siret'] = trim($this->Common_model->escape_data($v));
                        }
                        if ($k == 11 && !empty($v)) {
                            $data['cd_effectif'] = trim($this->Common_model->escape_data($v));
                        }
                        if ($k == 12 && !empty($v)) {
                            $data['u_email'] = trim($this->Common_model->escape_data($v));
                        }
                    }

                    // Validate recruiter data
                    $is_valid = $this->validate_recruiter_data($data);
                    if ($is_valid['status']) {
                        $result = $this->Importrecruiters_model->store_imported_recruiter($data);
                        if (!$result) {
                            $data['error_message'] = "Problem while store company details";
                            $this->Importrecruiters_model->store_failed_import_recruiter($data);
                        }
                    } else {
                        $data['error_message'] = $is_valid['message'];
                        $this->Importrecruiters_model->store_failed_import_recruiter($data);
                    }
                }
            } else {
                file_put_contents($log_file, "\n ======== file_path." . $file_path . " not exits....===== \n", FILE_APPEND | LOCK_EX);
            }
        } else {
            file_put_contents($log_file, "\n ======== uploaded_file." . $uploaded_file . " not found....===== \n", FILE_APPEND | LOCK_EX);
        }

        file_put_contents($log_file, "\n ======== End of " . $uploaded_file . " file ===== \n", FILE_APPEND | LOCK_EX);

        $this->benchmark->mark('code_end');
        file_put_contents($log_file, "\n ======== Duration : " . json_encode($this->benchmark->elapsed_time('code_start', 'code_end')) . " ===== \n", FILE_APPEND | LOCK_EX);
        file_put_contents($log_file, "\n ============================================= Cron End >__store_recruiters_data... Date[" . date('h:i:s a d-m-Y') . "] =========================== \n\n\n", FILE_APPEND | LOCK_EX);
    }

    /**
     * Description : Use this function for validate recruiter data
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     * @param type $data
     * @return type
     */
    protected function validate_recruiter_data($data) {


        $response_data['status'] = true;
        $response_data['message'] = '';

        if (!isset($data['cd_phone']) || (strlen($data['cd_phone']) < 1) || (strlen(trim($data['cd_phone'])) > 255)) {
            $response_data['status'] = FALSE;
            $response_data['message'].= 'Invalid company contact number, ';
        }

        if (!isset($data['u_email']) || (strlen($data['u_email']) > 255) || !preg_match('/^[^\@]+@.*\.[a-z]{2,6}$/i', $data['u_email'])) {
            $response_data['status'] = FALSE;
            $response_data['message'].= 'Invalid company email address, ';
        }

        $customer_info = 1;

        if (isset($data['u_email'])) {
            $customer_info = $this->Common_model->get_customer_by_email($data['u_email']);
        }

        if ($customer_info) {
            $response_data['status'] = FALSE;
            $response_data['message'].= 'Invalid company email address already registered, ';
        }

        return $response_data;
    }

    /**
     * Description: Use this function for send email newletter
     * 
     * @author Hardik Patel
     * Last modified date : 2016-01-09
     * 
     * @param type $id
     */
    public function send_newletter_email($id = 0) {
        $log_file = $this->log_file . "send_newletter_email_" . date('d-m-Y') . '.txt';
        file_put_contents($log_file, "\n\n ============================================ Cron Start >__send_newletter_email... Date[" . date('h:i:s a d-m-Y') . "] ===================================== \n\n", FILE_APPEND | LOCK_EX);
        $where = array(
            'nl_status' => 1
        );
        $columns = "nl_id, nl_email";
        $newsletter_data = $this->Common_model->get_all_rows(TBL_NEWSLETTERS, $columns, $where);
        if (!empty($newsletter_data) && !empty($id) && is_numeric($id)) {
            $where_news = array(
                'snl_id' => $id,
                'snl_status' => 1,
            );
            $columns_news = "snl_subject, snl_message";
            $newsletter_mail_data = $this->Common_model->get_single_row(TBL_SEND_NEWSLATTER, $columns_news, $where_news);
            if (!empty($newsletter_mail_data) && !empty($newsletter_mail_data['snl_subject']) && !empty($newsletter_mail_data['snl_message'])) {
                $view_array['message'] = $newsletter_mail_data['snl_message'];
                $view_array['subject'] = $newsletter_mail_data['snl_subject'];
                foreach ($newsletter_data as $row_newletter) {
                    $token = $row_newletter['nl_id'] + (1998 * 19);
                    $view_array['unsubscribe_url'] = DOMAIN_URL . "home/unsubscribe/" . $token;
                    $view_array['name'] = strstr($row_newletter['nl_email'], '@', true);
                    $message = $this->load->view("emails/newslatter", $view_array, TRUE);
                    $this->send_email_newletter(array($row_newletter['nl_email'] => $row_newletter['nl_email']), $view_array['subject'], $message);
                }
            }
        }

        file_put_contents($log_file, "\n\n ======== Cron End " . date('d-m-Y') . " \n\n", FILE_APPEND | LOCK_EX);
    }

    /**
     * This function is use for send email
     * @param array $to_email_address
     * @param string $subject
     * @param text $message
     */
    public function send_email_newletter($to_email_address, $subject, $message) {
        try {
            require_once SWIFT_MAILER_PATH;


            $transport = Swift_SmtpTransport::newInstance(EMAIL_HOST, EMAIL_PORT, MAIL_CERTI)
                    ->setUsername(EMAIL_USER)
                    ->setPassword(EMAIL_PASS);

            $mailer = Swift_Mailer::newInstance($transport);

            $mailLogger = new \Swift_Plugins_Loggers_ArrayLogger();
            $mailer->registerPlugin(new \Swift_Plugins_LoggerPlugin($mailLogger));

            $sendMessage = Swift_Message::newInstance($subject)
                    ->setFrom(array(EMAIL_FROM => APP_NAME))
                    ->setTo($to_email_address)
                    ->setBody($message, 'text/html');

            if (!$mailer->send($sendMessage)) {
                $headers = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
                $headers .= "X-Priority: 3\r\n";
                $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";
                $headers .= "From:" . APP_NAME . "<" . EMAIL_FROM . ">" . "\r\n";

                foreach ($to_email_address as $to) {
                    mail($to, $subject, $message, $headers);
                }
            }
        } catch (Exception $e) {

            $log_file = $this->log_file . "email_" . date('d-m-Y') . '.txt';
            file_put_contents($log_file, "\n\n " . json_encode($e->getMessage()) . " \n\n", FILE_APPEND | LOCK_EX);
            log_message("ERROR", $e->getMessage());
        }
    }

}
