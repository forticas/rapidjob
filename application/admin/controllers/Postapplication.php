<?php

/**
 * 
 * This controller is for handle user master activity
 * 
 * @author Nitinkumar Vaghani
 * Modified Date :- 2017-05-09
 * 
 */
class Postapplication extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Postapplication_model");
        $this->timestamp = 0;
        $this->utc_time = time();
        if (isset($_POST) && !empty($_POST['timestamp'])) {
            $this->timestamp = $this->Common_model->escape_data($this->input->get_post("timestamp"));
        }
    }

    /**
     * 
     * This function is use for show jqgrid show data view
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     */
    public function index($type = 'pending') {

        $data = array(
            'breadcrumb' => $this->breadcrumb()
        );


        if (empty($type)) {
            $data['type'] = 'pending';
        } elseif ($type != 'pending' && $type != 'delivered') {
            $data['type'] = 'pending';
        } else {
            $data['type'] = $type;
        }

        $this->load->view("common/header");
        $this->load->view("common/sidebar");
        $this->load->view("postapplication/index", $data);
        $this->load->view("common/footer");
    }

    /**
     * 
     * This function is use for get all data process it and send as json formate
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     */
    public function get_all_data() {

        $draw = !empty($this->input->get_post("draw")) ? $this->input->get_post("draw") : 1;

        $result = $this->Postapplication_model->get_all_data();

        $response = array();
        $response['draw'] = (int) $draw;
        $response['recordsTotal'] = (int) $result['total'];
        $response['recordsFiltered'] = (int) $result['total'];
        $response['data'] = $result['data'];
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * 
     * This function is use for sent / pending user applcation status
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-29
     * 
     * @return type
     * 
     */
    public function change_status() {

        $user_app_id = $this->Common_model->escape_data($this->input->get_post("user_app_id"));
        $status = $this->Common_model->escape_data($this->input->get_post("status"));
        $recruiter_id = $this->Common_model->escape_data($this->input->get_post("recruiter_id"));
        $candidate_id = $this->Common_model->escape_data($this->input->get_post("candidate_id"));

        $response = array();
        if (
                !empty($user_app_id) ||
                !empty($candidate_id) ||
                !empty($recruiter_id) ||
                !empty($status) ||
                $status == 1
        ) {
            $get_app_data = $this->Postapplication_model->get_user_applications();

            if (!empty($get_app_data)) {
                $all_company_ids = $all_delivered_company_ids = array();

                if (!empty($get_app_data['ua_company_id'])) {
                    $all_company_ids = explode(',', $get_app_data['ua_company_id']);
                    if (($key = array_search($recruiter_id, $all_company_ids)) !== false) {
                        unset($all_company_ids[$key]);
                    }
                }

                if (!empty($get_app_data['ua_sent_post_company_id'])) {
                    $all_delivered_company_ids = explode(',', $get_app_data['ua_sent_post_company_id']);
                    if (($key_app = array_search($recruiter_id, $all_delivered_company_ids)) !== true) {
                        array_push($all_delivered_company_ids, $recruiter_id);
                    }
                } else {
                    $all_delivered_company_ids = array(0 => $recruiter_id);
                }

                $where_app = array();
                $where_app['ua_send_by'] = 2;
                $where_app['ua_id'] = $user_app_id;
                $where_app['ua_user_id'] = $candidate_id;

                $request_data = array();
                $request_data['ua_company_id'] = implode(',', $all_company_ids);
                $request_data['ua_sent_post_company_id'] = implode(',', $all_delivered_company_ids);
                $request_data['ua_modified_date'] = $this->utc_time;


                $update_state = $this->Common_model->update(TBL_USER_APPLICATIONS, $request_data, $where_app);


                if ($update_state > 0) {
                    $check_app_data = $this->Postapplication_model->check_user_applications();
                    
                    if (empty($check_app_data['ua_company_id'])) {
                        $request_data['ua_status'] = 1;
                        $request_data['ua_modified_date'] = $this->utc_time;
                        $update_state = $this->Common_model->update(TBL_USER_APPLICATIONS, $request_data, $where_app);
                    }

                    $response['success'] = 'true';
                    $response['message'] = "Application status successfully changed.";
                } else {
                    $response['success'] = 'true';
                    $response['message'] = "Problem while changing application status.";
                }
            } else {
                $response['success'] = 'false';
                $response['message'] = "User application details not found..!";
            }
        } else {
            $response['success'] = 'false';
            $response['message'] = "Invalid request parameters..!";
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

}
