<?php
/**
 * Description :- This class is used for the manage the front side content.
 * 
 * @author Manish Ramnani
 * 
 * Modified Date :- 2017-05-25
 * 
 */
Class Managecms extends MY_Controller {

    /**
     * Description :- Here we load the prerequisite model and views
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     */
    
    public function __construct() {
        parent::__construct();
        $this->load->model('Managecms_model', 'Managecms');  
        $this->timestamp = 0;
        $this->utc_time = time();       
        if (isset($_POST) && !empty($_POST['timestamp'])) {
            $this->timestamp = $this->Common_model->escape_data($this->input->get_post("timestamp"));
        }
    }

    /**
     * Description :- This function is used to load the homepage view.
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     */
    
    public function index(){
        redirect(MANAGE_CMS_PATH.'/homepage');
    }
    
    /**
     * Description :- This function is used to manage the home page and footer content
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     */
    
    public function homepage() {

        if ($this->input->post() != NULL) {

            $request_data = array(
                'cms_home_banner_content_en' => trim($this->Common_model->escape_data($this->input->get_post('banner_content_en'))),
                'cms_home_banner_content_fr' => trim($this->Common_model->escape_data($this->input->get_post('banner_content_fr'))),
                'cms_hm_create_account_text_en' => trim($this->Common_model->escape_data($this->input->get_post('create_account_en'))),
                'cms_hm_create_account_text_fr' => trim($this->Common_model->escape_data($this->input->get_post('create_account_fr'))),
                'cms_home_news_title_en' => trim($this->Common_model->escape_data($this->strip_slashes_recursive($this->input->get_post('news_title_en')))),
                'cms_home_news_title_fr' => trim($this->Common_model->escape_data($this->strip_slashes_recursive($this->input->get_post('news_title_fr')))),
                'cms_home_subscribe_text_en' => trim($this->Common_model->escape_data($this->input->get_post('subscribe_text_en'))),
                'cms_home_subscribe_text_fr' => trim($this->Common_model->escape_data($this->input->get_post('subscribe_text_fr'))),
                
                'cms_featured_one_title_en' => trim($this->Common_model->escape_data($this->input->get_post('featured_one_title_en'))),
                'cms_featured_one_title_fr' => trim($this->Common_model->escape_data($this->input->get_post('featured_one_title_fr'))),
                'cms_featured_one_description_en' => trim($this->Common_model->escape_data($this->input->get_post('featured_one_description_en'))),
                'cms_featured_one_description_fr' => trim($this->Common_model->escape_data($this->input->get_post('featured_one_description_fr'))),
                
                'cms_featured_second_title_en' => trim($this->Common_model->escape_data($this->input->get_post('featured_second_title_en'))),
                'cms_featured_second_title_fr' => trim($this->Common_model->escape_data($this->input->get_post('featured_second_title_fr'))),
                'cms_featured_second_description_en' => trim($this->Common_model->escape_data($this->input->get_post('featured_second_description_en'))),
                'cms_featured_second_description_fr' => trim($this->Common_model->escape_data($this->input->get_post('featured_second_description_fr'))),
                
                'cms_featured_third_title_en' => trim($this->Common_model->escape_data($this->input->get_post('featured_third_title_en'))),
                'cms_featured_third_title_fr' => trim($this->Common_model->escape_data($this->input->get_post('featured_third_title_fr'))),
                'cms_featured_third_description_en' => trim($this->Common_model->escape_data($this->input->get_post('featured_third_description_en'))),
                'cms_featured_third_description_fr' => trim($this->Common_model->escape_data($this->input->get_post('featured_third_description_fr'))),
                

                'cms_footer_copyright_en' => trim($this->Common_model->escape_data($this->input->get_post('copy_right_text_en'))),
                'cms_footer_copyright_fr' => trim($this->Common_model->escape_data($this->input->get_post('copy_right_text_fr'))),
                'cms_footer_contactus' => trim($this->Common_model->escape_data($this->input->get_post('contact_us'))),
                'cms_footer_facebook_link' => trim($this->Common_model->escape_data($this->input->get_post('facebook_link'))),
                'cms_footer_twitter_link' => trim($this->Common_model->escape_data($this->input->get_post('twitter_link'))),
                'cms_footer_google_link' => trim($this->Common_model->escape_data($this->input->get_post('google_link'))),
                'cms_footer_linkedin_link' => trim($this->Common_model->escape_data($this->input->get_post('linkedin_link'))),
                'cms_modified_date' => time()
            );

            if (!empty($_FILES['photo']['name']) && $_FILES['photo']['error'] == 0) {
                $upload_path = UPLOAD_REL_PATH . "/" . UPLOAD_CMS_FOLDER;
                $new_image = do_upload($upload_path, $_FILES, true);
                if (!empty($new_image)) {
                    $file_name = $this->Managecms->get_cms_content();
                    @unlink($upload_path . '/' . $file_name['cms_home_banner_image']);
                    $request_data["cms_home_banner_image"] = $new_image;
                }
            }

            $is_update = $this->Managecms->update_homepage_content($request_data);
            if ($is_update) {                  
                $this->session->set_flashdata("success", "Content updated successfully");
            } else {
                $this->session->set_flashdata("failure", "Something went wrong");
            }
        }

        $get_homepage_content = $this->Managecms->get_cms_content();
        $get_homepage_content['image_url'] = UPLOAD_ABS_PATH . UPLOAD_CMS_FOLDER . '/' . $get_homepage_content['cms_home_banner_image'];
        $get_homepage_content['cms_hm_create_account_text_en'] = $this->convert_into_br($get_homepage_content['cms_hm_create_account_text_en']);
        $get_homepage_content['cms_hm_create_account_text_fr'] = $this->convert_into_br($get_homepage_content['cms_hm_create_account_text_fr']);
        $data = array(
            'breadcrumb' => $this->breadcrumb(),
            'homepage_content' => $get_homepage_content
        );
        $this->load->view('common/header');
        $this->load->view('common/footer');
        $this->load->view('common/sidebar');
        $this->load->view('managecms/homepage', $data);
    }

    
    /**
     * Description :- This function is used to load the testimonial view.
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     * 
     */
    public function testimonials(){
        $data = array(
            'breadcrumb' => $this->breadcrumb(),
        );
        $this->load->view('common/header');
        $this->load->view('common/footer');
        $this->load->view('common/sidebar');
        $this->load->view('managecms/testimonials',$data);
    }
    
    /**
     * Description :- This function is used to get all the testimonials
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     */
    public function get_all_testimonials() {

        $page = !empty($this->input->get_post("start")) ? $this->input->get_post("start") : 0;
        $rows = !empty($this->input->get_post("length")) ? $this->input->get_post("length") : 10;
        $draw = !empty($this->input->get_post("draw")) ? $this->input->get_post("draw") : 1;

        $sidx = !empty($this->input->get_post("order")[0]['column']) ? $this->input->get_post("order")[0]['column'] : 0;
        $sord = !empty($this->input->get_post("order")[0]['dir']) ? $this->input->get_post("order")[0]['dir'] : 'ASC';


        $status_filter = !empty($this->input->get_post("status_filter")) ? $this->input->get_post("status_filter") : '';
        $search = !empty($this->input->get_post("global_filter")) ? $this->input->get_post("global_filter") : '';
        

        $where = array(
            'cms_t_status !=' => 9
        );

        if ($sidx == 0) {
            $sidx = 'cms_t_title_en';
        } else {
            $sidx = 'cms_t_created_date';
        }

        $order_by = array($sidx => $sord);
        $limit = $page . ',' . $rows;

        $like_array = array();
        if (!empty($search)) {
            $like_array['cms_t_title_en'] = $search;
            $like_array['cms_t_title_fr'] = $search;
        }

        if(!empty($status_filter)){
            $like_array['cms_t_status'] = $status_filter;
        }
        
        $get_testimonial_data = $this->Managecms->get_all_testimonials($where, $order_by, $limit, $like_array);

        if (!empty($get_testimonial_data)) {
            $testimonial_data = $get_testimonial_data['testimonial_data'];
            $total_rows = $get_testimonial_data['total_rows'];
        } else {
            $total_rows = 0;
        }

        $resultArr = array();
        if (!empty($testimonial_data) && is_array($testimonial_data) && count($testimonial_data) > 0) {
            $i = 0;
            foreach ($testimonial_data as $value) {
                $status = ($value['cms_t_status'] == 1) ? "checked" : '';
                $resultArr[$i]['title_en'] = substr($value['cms_t_title_en'], 0, 20);
                $resultArr[$i]['title_fr'] = mb_substr($value['cms_t_title_fr'], 0, 20);
                $resultArr[$i]['description_en'] = substr($value['cms_t_description_en'], 0, 20);                
                $resultArr[$i]['description_fr'] = mb_substr($value['cms_t_description_fr'], 0, 20);
                $resultArr[$i]['created_at'] = date(DATE_FORMAT, (int) ($value['cms_t_created_date'] + $this->timestamp));
                $resultArr[$i]['status'] = '<div class="switch"><label><input type="checkbox" class="status_change ct_switch"  data-id="' . $value['cms_t_id'] . '" value="' . $value['cms_t_status'] . '" ' . $status . '><span class="lever switch-col-blue"></span></label></div>';
                $resultArr[$i]['action'] = '<a class="btn btn-primary waves-effect set_datatable_edit_button" 
                                                href="' . MANAGE_CMS_PATH . '/edit_testimonial/' . $value['cms_t_id'] . '">
                                                    <i class="material-icons icon_15">edit</i> 
                                            </a> 
                                            
                                            <a class="btn btn-danger btn-xs delete_data_button" 
                                                href="javascript:void(0)" data-id=' . $value['cms_t_id'] . '> 
                                                    <i class="material-icons icon_15">delete_forever</i> 
                                            </a>';
                $i++;
            }
        }

        $response = array();
        $response['draw'] = (int) $draw;
        $response['recordsTotal'] = (int) $total_rows;
        $response['recordsFiltered'] = (int) $total_rows;
        $response['data'] = $resultArr;
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }
    
    
    /**
     * Description :- This function is used to add the testimonial
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     */
    public function add_testimonial() {

        if ($this->input->post() != NULL) {

            $request_data = array(
                'cms_t_title_en' => trim($this->Common_model->escape_data($this->input->get_post('testimonial_title_en'))),
                'cms_t_title_fr' => trim($this->Common_model->escape_data($this->input->get_post('testimonial_title_fr'))),
                'cms_t_description_en' => trim($this->Common_model->escape_data($this->input->get_post('testimonial_description_en'))),
                'cms_t_description_fr' => trim($this->Common_model->escape_data($this->input->get_post('testimonial_description_fr'))),
                'cms_t_created_date' => time(),
            );

            $inserted_id = $this->Managecms->add_testimonials($request_data);

            if ($inserted_id) {

                if (!empty($_FILES['testimonial_photo']['name']) && $_FILES['testimonial_photo']['error'] == 0) {
                    $upload_path = UPLOAD_REL_PATH . "/" . UPLOAD_CMS_FOLDER;
                    $new_image = do_upload($upload_path, $_FILES, true);
                    if (!empty($new_image)) {
                        $file_name = $this->Managecms->get_testimonial($inserted_id);
                        @unlink($upload_path . '/' . $file_name['cms_t_image']);
                        
                        $data = array(
                            'cms_t_modified_date' => time(),
                            'cms_t_image' => $new_image
                        );
                        
                        $where = array(
                            'cms_t_id' => $inserted_id
                        );
                        
                        $is_update = $this->Managecms->update_testimonial($data,$where);
                    }
                }
                if($is_update) {
                    $this->session->set_flashdata("success", "Testimonial added successfully");
                }else{
                    $this->session->set_flashdata("failure", "Something went wrong");
                }
            } else {
                $this->session->set_flashdata("failure", "Something went wrong");
            }
            redirect(MANAGE_CMS_PATH.'/testimonials');
        }

        $data = array(
            'breadcrumb' => $this->breadcrumb(),
        );
        $this->load->view('common/header');
        $this->load->view('common/footer');
        $this->load->view('common/sidebar');
        $this->load->view('managecms/add_testimonial', $data);
    }

    /**
     * Descripion :- This function is used to edit the testimonial
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     * 
     * @param type $id
     */
    public function edit_testimonial($id="") {

        if(empty($id) && !is_numeric($id)){
            $this->session->set_flashdata("failure", "No Testimonial found.");
            redirect(MANAGE_CMS_PATH.'/testimonials');
        }
        
        if ($this->input->post() != NULL) {

            $testimonial_id = trim($this->Common_model->escape_data($this->input->get_post('testimonial_id')));
                    
            $request_data = array(
                'cms_t_title_en' => trim($this->Common_model->escape_data($this->input->get_post('testimonial_title_en'))),
                'cms_t_title_fr' => trim($this->Common_model->escape_data($this->input->get_post('testimonial_title_fr'))),
                'cms_t_description_en' => trim($this->Common_model->escape_data($this->input->get_post('testimonial_description_en'))),
                'cms_t_description_fr' => trim($this->Common_model->escape_data($this->input->get_post('testimonial_description_fr'))),
                'cms_t_modified_date' => time(),
            );

            $where = array(
                'cms_t_id' => $testimonial_id
            );
            $update_id = $this->Managecms->update_testimonial($request_data,$where);

            if ($update_id) {
                
                if (!empty($_FILES['testimonial_photo']['name']) && $_FILES['testimonial_photo']['error'] == 0) {
                    
                    $upload_path = UPLOAD_REL_PATH . "/" . UPLOAD_CMS_FOLDER;
                    
                    $new_image = do_upload($upload_path, $_FILES, true);
                    
                    if (!empty($new_image)) {
                        
                        $file_name = $this->Managecms->get_testimonial($testimonial_id);
                        @unlink($upload_path . '/' . $file_name['cms_t_image']);
                        
                        $data = array(
                            'cms_t_modified_date' => time(),
                            'cms_t_image' => $new_image
                        );
                        
                        $where = array(
                            'cms_t_id' => $testimonial_id
                        );
                        
                        $update_id = $this->Managecms->update_testimonial($data,$where);
                    }
                }
                if($update_id) {
                    $this->session->set_flashdata("success", "Testimonial edited successfully");
                }else{
                    $this->session->set_flashdata("failure", "Something went wrong");
                }
            } else {
                $this->session->set_flashdata("failure", "Something went wrong");
            }
            redirect(MANAGE_CMS_PATH.'/testimonials');
        }

        //get the testimonial
        $get_testimonial_data = $this->Managecms->get_testimonial($id);
        
        if(empty($get_testimonial_data)){
            redirect(MANAGE_CMS_PATH.'/testimonials');
        }
        
        if(!empty($get_testimonial_data['cms_t_image'])){
            $get_testimonial_data['image_url'] = UPLOAD_ABS_PATH . "/" . UPLOAD_CMS_FOLDER."/".$get_testimonial_data['cms_t_image'];
        }
        $data = array(
            'breadcrumb' => $this->breadcrumb(),
            'testimonial_data' => $get_testimonial_data
        );
        $this->load->view('common/header');
        $this->load->view('common/footer');
        $this->load->view('common/sidebar');
        $this->load->view('managecms/edit_testimonial', $data);
    }
    
    /**
     * Description :- This function is used to change the status of the testimonial
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     */
    
    public function change_status_testimonial() {
        $testimonial_id = $this->Common_model->escape_data($this->input->get_post("id"));
        $status = $this->Common_model->escape_data($this->input->get_post("status"));
        $response = array();
        if (empty($testimonial_id) || empty($testimonial_id)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = 'false';
                $response['message'] = "Invalid Request!";
            }
        } else {
            $where = array();
            $where['cms_t_id'] = $testimonial_id;
            
            $data = array();
            $data['cms_t_status'] = $status;
            $data['cms_t_modified_date'] = time();

            $update_state = $this->Managecms->update_testimonial($data, $where);
            if ($update_state > 0) {
                $response['success'] = 'true';
                $response['message'] = "Status change succesfully";
            } else {
                $response['message'] = "Something went wrong";
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * Descriptoin :- This function is used to delete the testimonial
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     */
    public function delete_testimonial() {
        
        $testimonial_id = $this->Common_model->escape_data($this->input->get_post("id"));

        $response = array();
        if (empty($testimonial_id)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = 'false';
                $response['message'] = 'Invalid Request!';
            }
        } else {
            $where = array();
            $where['cms_t_id'] = $testimonial_id;
            
            $data = array();
            $data['cms_t_status'] = 9;
            $data['cms_t_modified_date'] = time();

            $update_state = $this->Managecms->update_testimonial($data, $where);
            
            if ($update_state > 0) {
                $response['success'] = 'true';
                $response['message'] = 'Testimonial deleted succesfully';
            } else {
                $response['message'] = "Something went wrong";
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }
    
    
    /**
     * Description :- This function is used to update the content of the professional space
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     * 
     */
    public function professional(){
        
        if ($this->input->post() != NULL) {

            $request_data = array(
                'cms_prof_banner_title_en' => trim($this->Common_model->escape_data($this->input->get_post('prof_banner_content_en'))),
                'cms_prof_banner_title_fr' => trim($this->Common_model->escape_data($this->input->get_post('prof_banner_content_fr'))),
                'cms_prof_cv_title_en' => trim($this->Common_model->escape_data($this->input->get_post('prof_database_en'))),
                'cms_prof_cv_title_fr' => trim($this->Common_model->escape_data($this->input->get_post('prof_database_fr'))),
                'cms_prof_broadcast_en' => trim($this->Common_model->escape_data($this->input->get_post('prof_broadcast_en'))),
                'cms_prof_broadcast_fr' => trim($this->Common_model->escape_data($this->input->get_post('prof_broadcast_fr'))),
                'cms_prof_mode_en' => trim($this->Common_model->escape_data($this->input->get_post('prof_premium_mode_en'))),
                'cms_prof_mode_fr' => trim($this->Common_model->escape_data($this->input->get_post('prof_premium_mode_fr'))),
                'cms_modified_date' => time()
            );
            
            
            if (!empty($_FILES['prof_image']['name']) && $_FILES['prof_image']['error'] == 0) {
                
                $upload_path = UPLOAD_REL_PATH . "/" . UPLOAD_CMS_FOLDER;
                $new_image = do_upload($upload_path, $_FILES, true);
                if (!empty($new_image)) {
                    $file_name = $this->Managecms->get_cms_content();
                    @unlink($upload_path . '/' . $file_name['cms_prof_banner_image']);
                    $request_data["cms_prof_banner_image"] = $new_image;
                }
            }
            
            $is_update = $this->Managecms->update_homepage_content($request_data);

            if ($is_update) {
                $this->session->set_flashdata("success", "Content updated successfully");
            } else {
                $this->session->set_flashdata("failure", "Something went wrong");
            }
        }
        
        $get_professional_content = $this->Managecms->get_cms_content();
        $get_professional_content['image_url'] = UPLOAD_ABS_PATH . UPLOAD_CMS_FOLDER . '/' . $get_professional_content['cms_prof_banner_image'];
        $get_professional_content['cms_prof_cv_title_en'] = $this->convert_into_br($get_professional_content['cms_prof_cv_title_en']);
        $get_professional_content['cms_prof_cv_title_fr'] = $this->convert_into_br($get_professional_content['cms_prof_cv_title_fr']);
        $get_professional_content['cms_prof_broadcast_en'] = $this->convert_into_br($get_professional_content['cms_prof_broadcast_en']);
        $get_professional_content['cms_prof_broadcast_fr'] = $this->convert_into_br($get_professional_content['cms_prof_broadcast_fr']);
        $get_professional_content['cms_prof_mode_en'] = $this->convert_into_br($get_professional_content['cms_prof_mode_en']);
        $get_professional_content['cms_prof_mode_fr'] = $this->convert_into_br($get_professional_content['cms_prof_mode_fr']);
        
        
        $data = array(
            'breadcrumb' => $this->breadcrumb(),
            'professional_content' => $get_professional_content
        );
        $this->load->view('common/header');
        $this->load->view('common/footer');
        $this->load->view('common/sidebar');
        $this->load->view('managecms/professional_space',$data);
    }
    
    
    /**
     * Description :- This function is used to convert the \r\n into new line
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     * 
     * @param type $input_str
     * @return type
     */
    public function convert_into_br($input_str) {
        $return_string = str_replace(array("\\r\\n"), "\n", $input_str);
        return $return_string;
    }
    
    
    /**
     * Description :- This function is used to load the fqa view.
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     * 
     */
    public function faq(){
        $data = array(
            'breadcrumb' => $this->breadcrumb(),
        );
        $this->load->view('common/header');
        $this->load->view('common/footer');
        $this->load->view('common/sidebar');
        $this->load->view('managecms/faq',$data);
    }
    
    /**
     * Description :- This function is used to get all the faq's
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     */
    public function get_all_faq() {

        $page = !empty($this->input->get_post("start")) ? $this->input->get_post("start") : 0;
        $rows = !empty($this->input->get_post("length")) ? $this->input->get_post("length") : 10;
        $draw = !empty($this->input->get_post("draw")) ? $this->input->get_post("draw") : 1;

        $sidx = !empty($this->input->get_post("order")[0]['column']) ? $this->input->get_post("order")[0]['column'] : 0;
        $sord = !empty($this->input->get_post("order")[0]['dir']) ? $this->input->get_post("order")[0]['dir'] : 'ASC';


        $status_filter = !empty($this->input->get_post("status_filter")) ? $this->input->get_post("status_filter") : '';
        $search = !empty($this->input->get_post("global_filter")) ? $this->input->get_post("global_filter") : '';
        

        $where = array(
            'cms_faq_status !=' => 9
        );
        
        if ($sidx == 0) {
            $sidx = 'cms_faq_question_en';
        } else {
            $sidx = 'cms_faq_created_date';
        }

        $order_by = array($sidx => $sord);
        $limit = $page . ',' . $rows;
        
        $like_array = array();
        if (!empty($search)) {
            $like_array['cms_faq_question_en'] = $search;
            $like_array['cms_faq_question_fr'] = $search;
        }

        if(!empty($status_filter)){
            $like_array['cms_faq_status'] = $status_filter;
        }
        
        $get_faq_data = $this->Managecms->get_all_faq($where, $order_by, $limit, $like_array);

        if (!empty($get_faq_data)) {
            $faq_data = $get_faq_data['faq_data'];
            $total_rows = $get_faq_data['total_rows'];
        } else {
            $total_rows = 0;
        }
        
        $resultArr = array();
        if (!empty($faq_data) && is_array($faq_data) && count($faq_data) > 0) {
            $i = 0;
            foreach ($faq_data as $value) {
                $status = ($value['cms_faq_status'] == 1) ? "checked" : '';
                $resultArr[$i]['title_en'] = substr($value['cms_faq_question_en'], 0, 20);
                $resultArr[$i]['title_fr'] = mb_substr($value['cms_faq_question_fr'], 0, 20);
                $resultArr[$i]['description_en'] = substr(get_slash_formatted_text($value['cms_faq_desc_en']), 0, 20);                
                $resultArr[$i]['description_fr'] = mb_substr(get_slash_formatted_text($value['cms_faq_desc_fr']), 0, 20);
                $resultArr[$i]['created_at'] = date(DATE_FORMAT, (int) ($value['cms_faq_created_date'] + $this->timestamp));
                $resultArr[$i]['status'] = '<div class="switch"><label><input type="checkbox" class="status_change ct_switch"  data-id="' . $value['cms_faq_id'] . '" value="' . $value['cms_faq_status'] . '" ' . $status . '><span class="lever switch-col-blue"></span></label></div>';
                $resultArr[$i]['action'] = '<a class="btn btn-primary waves-effect set_datatable_edit_button" 
                                                href="' . MANAGE_CMS_PATH . '/edit_faq/' . $value['cms_faq_id'] . '">
                                                    <i class="material-icons icon_15">edit</i> 
                                            </a> 
                                            
                                            <a class="btn btn-danger btn-xs delete_data_button" 
                                                href="javascript:void(0)" data-id=' . $value['cms_faq_id'] . '> 
                                                    <i class="material-icons icon_15">delete_forever</i> 
                                            </a>';
                $i++;
            }
        }

        $response = array();
        $response['draw'] = (int) $draw;
        $response['recordsTotal'] = (int) $total_rows;
        $response['recordsFiltered'] = (int) $total_rows;
        $response['data'] = $resultArr;
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }
    
    /**
     * Description :- This function is used to change the status of the testimonial
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     */
    
    public function change_status_faq() {
        
        $faq_id = $this->Common_model->escape_data($this->input->get_post("id"));
        $status = $this->Common_model->escape_data($this->input->get_post("status"));
        $response = array();
        if (empty($faq_id) || empty($faq_id)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = 'false';
                $response['message'] = "Invalid Request!";
            }
        } else {
            $where = array();
            $where['cms_faq_id'] = $faq_id;
            
            $data = array();
            $data['cms_faq_status'] = $status;
            $data['cms_faq_modified_date'] = time();

            $update_state = $this->Managecms->update_faq($data, $where);
            if ($update_state > 0) {
                $response['success'] = 'true';
                $response['message'] = "Status change succesfully";
            } else {
                $response['message'] = "Something went wrong";
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }
    
    
    /**
     * Descriptoin :- This function is used to delete the faq
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     */
    public function delete_faq() {
        
        $faq_id = $this->Common_model->escape_data($this->input->get_post("id"));

        $response = array();
        if (empty($faq_id)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = 'false';
                $response['message'] = 'Invalid Request!';
            }
        } else {
            $where = array();
            $where['cms_faq_id'] = $faq_id;
            
            $data = array();
            $data['cms_faq_status'] = 9;
            $data['cms_faq_modified_date'] = time();

            $update_state = $this->Managecms->update_faq($data, $where);
            
            if ($update_state > 0) {
                $response['success'] = 'true';
                $response['message'] = 'Testimonial deleted succesfully';
            } else {
                $response['message'] = "Something went wrong";
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }
    
    /**
     * Description :- This function is used to add the testimonial
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     */
    public function add_faq() {

        if ($this->input->post() != NULL) {

            $request_data = array(
                'cms_faq_question_en' => trim($this->Common_model->escape_data($this->input->get_post('question_en'))),
                'cms_faq_question_fr' => trim($this->Common_model->escape_data($this->input->get_post('question_fr'))),
                'cms_faq_desc_en' => trim($this->Common_model->escape_data($this->input->get_post('question_description_en'))),
                'cms_faq_desc_fr' => trim($this->Common_model->escape_data($this->input->get_post('question_description_fr'))),
                'cms_faq_created_date' => time(),
            );

            $inserted_id = $this->Managecms->add_faq($request_data);

            if ($inserted_id) {
                
                $this->session->set_flashdata("success", "FAQ added successfully");
                
            } else {
                $this->session->set_flashdata("failure", "Something went wrong");
            }
            redirect(MANAGE_CMS_PATH.'/faq');
        }

        $data = array(
            'breadcrumb' => $this->breadcrumb(),
        );
        $this->load->view('common/header');
        $this->load->view('common/footer');
        $this->load->view('common/sidebar');
        $this->load->view('managecms/add_faq', $data);
    }
    
    /**
     * Descripion :- This function is used to edit the testimonial
     * 
     * @author Manish Ramnani
     * 
     * Modified Date :- 2017-05-25
     * 
     * @param type $id
     */
    public function edit_faq($id="") {

        if(empty($id) && !is_numeric($id)){
            $this->session->set_flashdata("failure", "No FAQ found.");
            redirect(MANAGE_CMS_PATH.'/faq');
        }
        
        if ($this->input->post() != NULL) {

            $faq_id = trim($this->Common_model->escape_data($this->input->get_post('faq_id')));
                    
            $request_data = array(
                'cms_faq_question_en' => trim($this->Common_model->escape_data($this->input->get_post('question_en'))),
                'cms_faq_question_fr' => trim($this->Common_model->escape_data($this->input->get_post('question_fr'))),
                'cms_faq_desc_en' => trim($this->Common_model->escape_data($this->input->get_post('question_description_en'))),
                'cms_faq_desc_fr' => trim($this->Common_model->escape_data($this->input->get_post('question_description_fr'))),
                'cms_faq_modified_date' => time(),
            );

            $where = array(
                'cms_faq_id' => $faq_id
            );
            $update_id = $this->Managecms->update_faq($request_data,$where);

            if ($update_id) {

                $this->session->set_flashdata("success", "FAQ edited successfully");
                
            } else {
                $this->session->set_flashdata("failure", "Something went wrong");
            }
            redirect(MANAGE_CMS_PATH.'/faq');
        }

        //get the faq
        $get_faq_data = $this->Managecms->get_faq($id);
        
        if(empty($get_faq_data)){
            $this->session->set_flashdata("failure", "No FAQ found.");
            redirect(MANAGE_CMS_PATH.'/faq');
        }
        
        $data = array(
            'breadcrumb' => $this->breadcrumb(),
            'faq_data' => $get_faq_data
        );
        $this->load->view('common/header');
        $this->load->view('common/footer');
        $this->load->view('common/sidebar');
        $this->load->view('managecms/edit_faq', $data);
    }
    
    
}
