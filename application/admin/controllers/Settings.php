<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * 
 * This controller is for handle settings master activity
 * 
 * @author Nitinkumar Vaghani
 * Modified Date :- 2017-05-09
 * 
 */
class Settings extends MY_Controller {

    private $keys = array('payment_in_cent_per_email', 'recruiter_subscription_plan_price', 'recruiter_subscription_plan_duration',  'payment_rate_for_send_application_by_post');

    public function __construct() {
        parent::__construct();
    }

    /**
     * 
     * This function is use for show settings page
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     */
    public function index() {

        $this->load->model("Setting_model");
        $setting_array = $this->Setting_model->get_settings();
        foreach ($this->keys as $key) {
            if (empty($setting_array) || !key_exists($key, $setting_array)) {
                $setting_array[$key] = '';
            }
        }

        $view_data = array();
        $view_data['setting_array'] = $setting_array;
        $view_data['breadcrumb'] = $this->breadcrumb();
        $this->load->view("common/header");
        $this->load->view('common/sidebar');
        $this->load->view("setting/add", $view_data);
        $this->load->view("common/footer");
    }

    /**
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     */
    public function add() {

        $filtered_request_array = array();
        foreach ($this->keys as $key) {
            if (!empty($this->input->post($key))) {
                $filtered_request_array[$key] = trim($this->Common_model->escape_data($this->input->post($key)));
            }
        }

        $where = array(
            's_id' => 1
        );

        $insert_array = array(
            's_description' => serialize($filtered_request_array),
            's_modified_date' => time()
        );

        $is_updated = $this->Common_model->update(TBL_SETTINGS, $insert_array, $where);

        if ($is_updated > 0) {
            $this->session->set_flashdata("success", "Your settings successfully saved.");
        } else {
            $this->session->set_flashdata("failure", "Problem while changing your settings.");
        }
        redirect(SETTING_PATH);
    }

}
