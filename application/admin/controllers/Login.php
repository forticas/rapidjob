<?php

/**
 * 
 * This controller manage dashboard
 * 
 * @author Nitinkumar Vaghani
 * Modified Date :- 2017-05-09
 * 
 */
class Login extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 
     * This function is use for login admin user
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     */
    public function index() {
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() !== FALSE) {
            if ($this->input->post() != NULL) {
                $email = $this->input->post("email");
                $password = trim($this->Common_model->escape_data($this->input->post("password")));
                $where = array(
                    "a_email" => $email
                );
                /* check admin exists or not */
                $admin = $this->Common_model->get_single_row(TBL_ADMINS, "*", $where);
                if (count($admin) > 0) {
                    //check password
                    if (password_verify(sha1($password), $admin['a_password'])) {
                        //if credential is true
                        $where = array(
                            "a_email" => $admin['a_email']
                        );
                        $data = array(
                            "a_last_login_at" => time(),
                        );
                        
                            $photo = DEFAULT_USER_IMAGE_REL;
                       

                        /* updating admin last login  */
                        $this->Common_model->update(TBL_ADMINS, $data, $where);
                        $admin_row = array(
                            "admin_name" => $admin['a_first_name'],
                            "admin_last_name" => $admin['a_last_name'],
                            "admin_id" => $admin['a_id'],
                            "admin_email" => $admin['a_email'],
                            "admin_profile" => $photo,
                        );

                        //load default language
                        $this->lang->load(DEFAULT_LANGUAGE, DEFAULT_LANGUAGE);

                        $this->session->set_userdata($admin_row);
                        $this->session->set_flashdata('success', 'You have been successfully logged in.');
                        redirect(DASHBOARD_PATH);
                    } else {
                        $this->form_validation->set_rules('email', "username", "not_registered");
                        $this->form_validation->set_message('not_registered', 'Provided credentials are invalid.');
                        $this->form_validation->run();
                    }
                } else {
                    /* if admin not exists */
                    $this->form_validation->set_rules('email', "username", "not_registered");
                    $this->form_validation->set_message('not_registered', 'Provided credentials are invalid.');
                    $this->form_validation->run();
                }
            }
        }

        $this->load->view("login/login_header");
        $this->load->view('login/login');
        $this->load->view("login/login_footer");
    }

    /**
     * 
     * This function is use for forgot admin user password
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     */
    public function forgot_password() {
        $this->form_validation->set_rules('email', 'Email', 'required');
        if ($this->form_validation->run() === False) {
            $this->load->view('login/login_header');
            $this->load->view("login/forgot_password");
            $this->load->view("login/login_footer");
        } else {
            //check email is registred or not?
            $email = $this->input->post("email");
            $where = array(
                "a_email" => $email
            );
            $admin = $this->Common_model->get_single_row(TBL_ADMINS, "*", $where);
            if (count($admin) <= 0) {
                $this->session->set_flashdata('failure', 'Email is not registered with us');
            } else {
                //send password reset link
                //generate random security code for password reset
                $data['token'] = get_raw_password();
                $data['id'] = $admin['a_id'];
                //update token into database
                $reset_token = str_rand_access_token(20);
                $this->Common_model->update(TBL_ADMINS, array('a_token' => $reset_token), $where);

                $reset_password_url = LOGIN_PATH . "/verify_token/" . $reset_token;

                $view_data = array();
                $view_data['first_name'] = $admin['a_first_name'] . " " . $admin['a_last_name'];
                $view_data['reset_password_url'] = $reset_password_url;

                $message = $this->load->view("emails/reset_password", $view_data, true);
                $subject = APP_NAME . " - Reset password instructions.";

                $this->send_email(array($email => $email), $subject, $message);
                $this->session->set_flashdata('success', 'Password reset link has been sent to your mail');
            }
            redirect(LOGIN_PATH . '/forgot_password');
        }
    }

    /**
     * 
     * This function is use for check forgot password token and update new password
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     */
    public function verify_token($token = NULL, $id = 0) {
        if (!isset($token) || $token == NULL) {
            $this->session->set_flashdata('failure', 'Invalid Token');
            redirect(LOGIN_PATH . '/forgot_password');
        }

        //check token is valid or not
        $valid_token = $this->Common_model->get_single_row(TBL_ADMINS, "a_id", array("a_token" => $token));
        if (count($valid_token) <= 0) {
            $this->session->set_flashdata('failure', 'Invalid Token');
            redirect(LOGIN_PATH . '/forgot_password');
        }
        $data['token'] = $token;
        $data['id'] = $valid_token['a_id'];
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]');


        if ($id != FALSE) {
            $valid_token = $this->Common_model->get_single_row(TBL_ADMINS, "a_id", array("a_token" => $token, 'a_id' => $id));
            if (count($valid_token) <= 0) {
                $this->session->set_flashdata('failure', 'Invalid Token or Invalid ID');
                redirect(LOGIN_PATH . '/forgot_password');
            }
        }
        if ($this->form_validation->run() === False) {
            $this->load->view('login/login_header');
            $this->load->view("login/reset_password", $data);
            $this->load->view("login/login_footer");
        } else {
            if ($this->input->post() != NULL) {
                $new_password = trim($this->input->post('password'));
                $udpate_array = array(
                    "a_password" => password_hash(sha1($new_password), PASSWORD_BCRYPT),
                    "a_updated_at" => time(),
                );
                $where = array("a_id" => $id);
                $is_update = $this->Common_model->update(TBL_ADMINS, $udpate_array, $where);
                if ($is_update > 0) {
                    //destroy old token
                    $this->Common_model->update(TBL_ADMINS, array('a_token' => NULL), $where);
                    $this->session->set_flashdata('success', 'Password changed successfully, Please login with new password');
                } else {
                    $this->session->set_flashdata('failure', 'Error to change password');
                }
                redirect(LOGIN_PATH);
            }
        }
    }
    
    /**
     * 
     * This function is use for unsubscribe from newsletter
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-15
     */
    public function unsubscribe($token) {
        
        if (!isset($token) || $token == NULL) {
            $this->session->set_flashdata('failure', 'Invalid Token');
            redirect(BASE_URL);
        }
       
        $nl_id = $token - (1998 * 19);
        
        //check token is valid or not
        $valid_token = $this->Common_model->get_single_row(TBL_NEWSLETTERS, "nl_id", array("nl_id" => $nl_id));
        if (count($valid_token) <= 0) {
            $this->session->set_flashdata('failure', 'Invalid Token');
            redirect(BASE_URL);
        }
        
        $update_array = array('nl_status' => 2 , 'nl_modified_date' => time());
        $where_array =  array('nl_id' => $nl_id);
        
        $valid_token = $this->Common_model->update(TBL_NEWSLETTERS, $update_array, $where_array);
        
        if (count($valid_token) <= 0) {
            
            $this->session->set_flashdata('failure', 'Problem while Unsubscribe');            
        }else{
            $this->session->set_flashdata('success', 'Unsubscribe Successfully.'); 
        }
        redirect(BASE_URL);
        
    }
    
}
