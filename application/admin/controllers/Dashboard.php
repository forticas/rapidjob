<?php

/**
 * 
 * This controller manage dashboard
 * 
 * @author Nitinkumar Vaghani
 * Modified Date :- 2017-05-09
 * 
 */
class Dashboard extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 
     * This function is use for display dashboard
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     */
    public function index() {
        
        $this->load->model('User_model');
        $this->load->model('Common_model');
        $userCount = $this->User_model->get_all_users_count();
        $companyCount = $this->User_model->get_all_company_count();
        $userpaymentCount = $this->User_model->get_all_user_payment_count();
        
        $newsCount = $this->Common_model->get_count_news();
        $newsletterCount = $this->Common_model->get_count_newsletter();
        $faqCount = $this->Common_model->get_count_faq();
        $testimonialsCount = $this->Common_model->get_count_testimonials();
        $data = array(
            'userCount' => $userCount,
            'newsCount' => $newsCount,
            'newsletterCount' => $newsletterCount,
            'faqCount' => $faqCount,
            'testimonialsCount' => $testimonialsCount,
            'companyCount' => $companyCount,
            'userpaymentCount' => $userpaymentCount,
            'breadcrumb' => $this->breadcrumb()
        );
        $this->load->view("common/header");
        $this->load->view("common/sidebar");
        $this->load->view("dashboard/index", $data);
        $this->load->view("common/footer");
    }

    /**
     * 
     * This function is use for logged out user from system
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     */
    public function logout() {
        $this->session->unset_userdata('admin_id');
        $this->session->set_flashdata('success', 'You have successfully logged out!');
        redirect(LOGIN_PATH);
    }

    /**
     * 
     * This function is use for edit admin profile
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     */
    public function edit_profile() {

        $this->form_validation->set_rules('admin_name', 'First Name', 'required');
        $this->form_validation->set_rules('admin_email', 'Email', 'required');
        $this->form_validation->set_rules('admin_last_name', 'Last Name', 'required');

        if ($this->form_validation->run() === FALSE) {
            $data = $this->session->all_userdata();

            $dataArr = array(
                'breadcrumb' => $this->breadcrumb()
            );
            $this->load->view("common/header", $data);
            $this->load->view("common/sidebar");
            $this->load->view('admin/edit_profile', $dataArr);
            $this->load->view("common/footer");
        } else {
            if ($this->input->post() != NULL) {
                //update profile into db
                $update_array = array(
                    "a_email" => trim($this->Common_model->escape_data($this->input->get_post('admin_email'))),
                    "a_first_name" => trim(($this->input->get_post('admin_name'))),
                    "a_last_name" => trim($this->input->get_post("admin_last_name")),
                    "a_updated_at" => time(),
                );

                $where = array(
                    "a_id" => $this->session->userdata('admin_id'),
                );

                if (!empty($_FILES['photo']['name']) && $_FILES['photo']['error'] == 0) {
                    $upload_path = UPLOAD_REL_PATH . "/admin/" . $this->session->userdata('admin_id');
                    $admin_new_profile = do_upload($upload_path, $_FILES, true);
                    if (isset($admin_new_profile) && $admin_new_profile != '') {
                        $file_name = $this->Common_model->get_single_row(TBL_ADMINS, "a_photo", $where);
                        @unlink($upload_path . '/' . $file_name['a_photo']);
                        //update profile name to db
                        $update_array['a_photo'] = $admin_new_profile;
                        $photo = "admin/" . $this->session->userdata('admin_id') . "/" . $admin_new_profile;
                        $this->session->set_userdata("admin_profile", $photo);
                    }
                }

                $is_updated = $this->Common_model->update(TBL_ADMINS, $update_array, $where);
                if ($is_updated > 0) {
                    $this->session->set_userdata("admin_name", trim(($this->input->get_post('admin_name'))));
                    $this->session->set_userdata("admin_last_name", trim($this->input->get_post('admin_last_name')));
                    $this->session->set_userdata("admin_email", trim($this->Common_model->escape_data($this->input->get_post('admin_email'))));
                    $this->session->set_flashdata('success', 'Profile is updated');
                } else {
                    $this->session->set_flashdata('failure', 'Error in updating your profile');
                }
            }
            redirect(DASHBOARD_PATH);
        }
    }

    /**
     * 
     * This function is use for change admin password
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     */
    public function change_password() {
        $this->form_validation->set_rules('old_password', 'Current Password', 'required');
        $this->form_validation->set_rules('password', 'New Password', 'required');
        $this->form_validation->set_rules('cpassword', 'Confirm Password', 'required|matches[password]');

        if ($this->form_validation->run() === FALSE) {
            $dataArr = array(
                'breadcrumb' => $this->breadcrumb()
            );
            $this->load->view("common/header");
            $this->load->view("common/sidebar");
            $this->load->view('admin/change_password', $dataArr);
            $this->load->view("common/footer");
        } else {
            if ($this->input->post() != NULL) {
                //check current password is ok?
                $admin = $this->Common_model->get_single_row(TBL_ADMINS, "a_password", array("a_email" => $this->session->userdata('admin_email')));
                $current_password = trim($this->Common_model->escape_data($this->input->get_post('old_password')));
                if (password_verify(sha1($current_password), $admin['a_password'])) {
                    //change new password
                    $new_password = trim($this->Common_model->escape_data($this->input->get_post('password')));
                    $udpate_array = array(
                        "a_password" => password_hash(sha1($new_password), PASSWORD_BCRYPT),
                        "a_updated_at" => time(),
                    );
                    $where = array("a_email" => $this->session->userdata('admin_email'));
                    $is_update = $this->Common_model->update(TBL_ADMINS, $udpate_array, $where);
                    if ($is_update > 0) {
                        $this->session->set_flashdata('success', 'Password changed successfully');
                    } else {
                        $this->session->set_flashdata('failure', 'Error to change password');
                    }
                    redirect(DASHBOARD_PATH);
                } else {
                    $this->session->set_flashdata('failure', 'Current password is incorrect');
                    redirect(DASHBOARD_PATH . '/change_password');
                }
            }
        }
    }

    /**
     * 
     * This function is use for get new csrf token after success ajax request
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     * @return boolean
     */
    public function get_csrf_value() {
        $ajax_response = array(
            "success" => TRUE,
            "value" => $this->security->get_csrf_hash()
        );
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($ajax_response));
    }

    public function change_language($language) {
        if (trim($language) == '') {
            redirect(DASHBOARD_PATH);
        }
        $this->session->set_userdata("language", $language);
        redirect(DASHBOARD_PATH);
    }

}

?>
