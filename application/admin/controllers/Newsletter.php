<?php

/**
 * 
 * This controller is for handle newsletter master activity
 * 
 * @author Hardik Patel
 * Modified Date :- 2017-05-15
 * 
 */
class Newsletter extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->timestamp = 0;
        $this->utc_time = time();
        if (isset($_POST) && !empty($_POST['timestamp'])) {
            $this->timestamp = $this->Common_model->escape_data($this->input->get_post("timestamp"));
        }
    }

    /**
     * 
     * This function is use for show jqgrid show data view
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-09
     * 
     */
    public function index() {
        $data = array(
            'breadcrumb' => $this->breadcrumb()
        );

        $this->load->view("common/header");
        $this->load->view("common/sidebar");
        $this->load->view("newsletter/index", $data);
        $this->load->view("common/footer");
    }

    /**
     * 
     * This function is use for get all data process it and send as json formate
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-09
     * 
     */
    public function get_all_data() {
        $page = !empty($this->input->get_post("start")) ? $this->input->get_post("start") : 0;
        $rows = !empty($this->input->get_post("length")) ? $this->input->get_post("length") : 10;
        $draw = !empty($this->input->get_post("draw")) ? $this->input->get_post("draw") : 1;

        $sidx = !empty($this->input->get_post("order")[0]['column']) ? $this->input->get_post("order")[0]['column'] : 0;
        $sord = !empty($this->input->get_post("order")[0]['dir']) ? $this->input->get_post("order")[0]['dir'] : 'ASC';

        $status = !empty($this->input->get_post("status")) ? $this->input->get_post("status") : '';
        $status_filter = !empty($this->input->get_post("status_filter")) ? $this->input->get_post("status_filter") : '';
        $global_search = !empty($this->input->get_post("global_filter")) ? $this->input->get_post("global_filter") : '';

        if ($sidx == 0) {
            $sidx = 'nl_email';
        } else {
            $sidx = 'nl_created_date';
        }

        $order_by = $sidx . ' ' . $sord;
        $limit = $page . ',' . $rows;

        $like_where = array();

        $where = array(
            'nl_status !=' => 9
        );

        if (!empty($global_search)) {
            $like_where['nl_email'] = $global_search;
        }

        if (!empty($status_filter)) {
            $where['nl_status'] = $status_filter;
        }

        $columns = "nl_id,nl_email,nl_created_date,nl_status";
        $total_rows = $this->Common_model->get_count(TBL_NEWSLETTERS, 'nl_id', $where, array(), $like_where);
        $newsletterData = $this->Common_model->get_all_rows(TBL_NEWSLETTERS, $columns, $where, array(), $order_by, $limit, '', $like_where);

        $resultArr = array();
        if (!empty($newsletterData) && is_array($newsletterData) && count($newsletterData) > 0) {
            $i = 0;
            foreach ($newsletterData as $value) {
                $status = ($value['nl_status'] == 1) ? "checked" : '';
                $resultArr[$i]['email'] = $value['nl_email'];
                $resultArr[$i]['created_at'] = date(DATE_FORMAT, (int) ($value['nl_created_date'] + $this->timestamp));
                $resultArr[$i]['status'] = '<div class="switch"><label><input type="checkbox" class="status_change ct_switch"  data-id="' . $value['nl_id'] . '" value="' . $value['nl_status'] . '" ' . $status . '><span class="lever switch-col-blue"></span></label></div>';
                $resultArr[$i]['action'] = '<a class="btn btn-danger btn-xs delete_data_button" 
                                                href="javascript:void(0)" data-id=' . $value['nl_id'] . '> 
                                                    <i class="material-icons icon_15">delete_forever</i> 
                                            </a>';
                $i++;
            }
        }

        $response = array();
        $response['draw'] = (int) $draw;
        $response['recordsTotal'] = (int) $total_rows;
        $response['recordsFiltered'] = (int) $total_rows;
        $response['data'] = $resultArr;
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * 
     * This function is use for active / inactive newletter email status
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-15
     * 
     * @return type
     * 
     */
    public function change_status() {
        $newsletter_id = $this->Common_model->escape_data($this->input->get_post("id"));
        $status = $this->Common_model->escape_data($this->input->get_post("status"));
        $response = array();
        if (empty($newsletter_id) || empty($status)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = 'false';
                $response['message'] = lang("mycontroller_invalid_request");
            }
        } else {
            $where = array();
            $where['nl_id'] = $newsletter_id;

            $request_data = array();
            $request_data['nl_status'] = $status;
            $request_data['nl_modified_date'] = $this->utc_time;

            $update_state = $this->Common_model->update(TBL_NEWSLETTERS, $request_data, $where);
            if ($update_state > 0) {
                $response['success'] = 'true';
                $response['message'] = lang("newsletter_status_change_success");
            } else {
                $response['message'] = lang("newsletter_status_change_failure");
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * 
     * This function is use for delete newletter
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-15
     * 
     * @return type
     * 
     */
    public function delete() {
        $newsletter_id = $this->Common_model->escape_data($this->input->get_post("id"));

        $response = array();
        if (empty($newsletter_id)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = 'false';
                $response['message'] = lang("mycontroller_invalid_request");
            }
        } else {
            $where = array();
            $where['nl_id'] = $newsletter_id;

            $request_data = array();
            $request_data['nl_status'] = 9;
            $request_data['nl_modified_date'] = $this->utc_time;

            $update_state = $this->Common_model->update(TBL_NEWSLETTERS, $request_data, $where);
            if ($update_state > 0) {
                $response['success'] = 'true';
                $response['message'] = lang("newsletter_delete_success");
            } else {
                $response['message'] = lang("newsletter_delete_failure");
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * 
     * This function is use for add new newsletter
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-09
     * 
     */
    public function add() {

        if ($this->input->post() != NULL) {

            $subject = $this->input->get_post("subject");
            $message = $this->input->get_post("message");

            if (empty($subject) || empty($message)) {
                $this->session->set_flashdata("failure", "Invalid subject or message..!");
                redirect(NEWSLETTER_PATH . '/add');
            }

            $this->load->model('Newslatter_model');

            $inserted_id = $this->Newslatter_model->store_newslatter();

            if ($inserted_id > 0) {
                $file = CRON_DOCROOT . "index.php myCron/send_newletter_email/" . $inserted_id;
                exec(PHP_PATH . " " . $file . " > /dev/null &");
                $this->session->set_flashdata("success", "Mail send successfully");
            } else {
                $this->session->set_flashdata("failure", "Problem in sending ");
                redirect(NEWSLETTER_PATH . '/add');
            }

            redirect(NEWSLETTER_PATH);
        }

        $dataArr = array(
            'breadcrumb' => $this->breadcrumb()
        );
        $this->load->view("common/header");
        $this->load->view("common/sidebar");
        $this->load->view("newsletter/add", $dataArr);
        $this->load->view("common/footer");
    }

}
