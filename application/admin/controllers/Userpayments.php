<?php

/**
 * 
 * This controller is for handle user payments master activity
 * 
 * @author Hardik Patel
 * Modified Date :- 2017-05-15
 * 
 */
class Userpayments extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->timestamp = 0;
        $this->utc_time = time();
        if (isset($_POST) && !empty($_POST['timestamp'])) {
            $this->timestamp = $this->Common_model->escape_data($this->input->get_post("timestamp"));
        }
    }

    /**
     * 
     * This function is use for show jqgrid show data view
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-15
     * 
     */
    public function index() {
        $data = array(
            'breadcrumb' => $this->breadcrumb()
        );

        $this->load->view("common/header");
        $this->load->view("common/sidebar");
        $this->load->view("userpayments/index", $data);
        $this->load->view("common/footer");
    }

    /**
     * 
     * This function is use for get all data process it and send as json formate
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-15
     * 
     */
    public function get_all_data() {
        $page = !empty($this->input->get_post("start")) ? $this->input->get_post("start") : 0;
        $rows = !empty($this->input->get_post("length")) ? $this->input->get_post("length") : 10;
        $draw = !empty($this->input->get_post("draw")) ? $this->input->get_post("draw") : 1;

        $sidx = !empty($this->input->get_post("order")[0]['column']) ? $this->input->get_post("order")[0]['column'] : 0;
        $sord = !empty($this->input->get_post("order")[0]['dir']) ? $this->input->get_post("order")[0]['dir'] : 'ASC';

        $status = !empty($this->input->get_post("status")) ? $this->input->get_post("status") : '';
        $status_filter = !empty($this->input->get_post("status_filter")) ? $this->input->get_post("status_filter") : '';
        $username_filter = !empty($this->input->get_post("username_filter")) ? $this->input->get_post("username_filter") : '';
        $amount_filter = !empty($this->input->get_post("amount_filter")) ? $this->input->get_post("amount_filter") : '';
        $totalamount_filter = !empty($this->input->get_post("totalamount_filter")) ? $this->input->get_post("totalamount_filter") : '';

        if ($sidx == 0) {
            $sidx = 'up_id';
        } else {
            $sidx = 'up_created_date';
        }

        $order_by = $sidx . ' ' . $sord;
        $limit = $page . ',' . $rows;

        $like_where = array();

        $where = array(
            'up_status !=' => 9,
            'u_status !=' => 9
        );

        if (!empty($username_filter)) {
            $like_where["CONCAT(u_first_name,' ',u_last_name)"] = $username_filter;
        }

        if (!empty($status_filter)) {
            $where['up_status'] = $status_filter;
        }

        if (!empty($amount_filter)) {
            $where['up_amount'] = $amount_filter;
        }

        if (!empty($totalamount_filter)) {
            $where['up_total_amount'] = $totalamount_filter;
        }
        $join = array(
            TBL_USERS => 'u_id = up_id'
        );
        $columns = "u_id,up_id,up_amount,up_total_amount,up_status,u_first_name,u_last_name,up_created_date";

        $total_rows = $this->Common_model->get_count(TBL_USER_PAYMENTS, 'up_id', $where, $join, $like_where);
        $paymentsData = $this->Common_model->get_all_rows(TBL_USER_PAYMENTS, $columns, $where, $join, $order_by, $limit, '', $like_where);

        $resultArr = array();
        if (!empty($paymentsData) && is_array($paymentsData) && count($paymentsData) > 0) {
            $i = 0;
            foreach ($paymentsData as $value) {

                if ($value['up_status'] != 1) {
                    $status = '<span class="label label-danger"> Failure</span>';
                } else {
                    $status = '<span class="label label-success"> Success</span>';
                }

                if (!empty($value['u_first_name'])) {
                    $resultArr[$i]['username'] = "<a href='" . USERS_PATH . "/view/" . $value['u_id'] . "'>" . get_slash_formatted_text($value['u_first_name']) . " " . get_slash_formatted_text($value['u_last_name']) . "</a>";
                } else {
                    $resultArr[$i]['username'] = "<a href='" . USERS_PATH . "/view/" . $value['candidate_id'] . "'> - - - - </a>";
                }
                $resultArr[$i]['amount'] = $value['up_amount'];
                $resultArr[$i]['total_amount'] = $value['up_total_amount'];
                $resultArr[$i]['created_at'] = date(DATE_FORMAT, (int) ($value['up_created_date'] + $this->timestamp));
                $resultArr[$i]['status'] = $status;
                $resultArr[$i]['action'] = '<a class="btn btn-danger btn-xs delete_data_button" 
                                                href="javascript:void(0)" data-id=' . $value['up_id'] . '> 
                                                    <i class="material-icons icon_15">delete_forever</i> 
                                            </a>';
                $i++;
            }
        }

        $response = array();
        $response['draw'] = (int) $draw;
        $response['recordsTotal'] = (int) $total_rows;
        $response['recordsFiltered'] = (int) $total_rows;
        $response['data'] = $resultArr;
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * 
     * This function is use for active / inactive newletter email status
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-15
     * 
     * @return type
     * 
     */
    public function change_status() {
        $payment_id = $this->Common_model->escape_data($this->input->get_post("id"));
        $status = $this->Common_model->escape_data($this->input->get_post("status"));
        $response = array();
        if (empty($payment_id) || empty($status)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = 'false';
                $response['message'] = lang("mycontroller_invalid_request");
            }
        } else {
            $where = array();
            $where['up_id'] = $payment_id;

            $request_data = array();
            $request_data['up_status'] = $status;
            $request_data['up_modified_date'] = $this->utc_time;

            $update_state = $this->Common_model->update(TBL_USER_PAYMENTS, $request_data, $where);
            if ($update_state > 0) {
                $response['success'] = 'true';
                $response['message'] = lang("userpayment_status_change_success");
            } else {
                $response['message'] = lang("userpayment_status_change_failure");
            }
        }
        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * 
     * This function is use for delete newletter
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-15
     * 
     * @return type
     * 
     */
    public function delete() {
        $payment_id = $this->Common_model->escape_data($this->input->get_post("id"));

        $response = array();
        if (empty($payment_id)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = 'false';
                $response['message'] = lang("mycontroller_invalid_request");
            }
        } else {
            $where = array();
            $where['up_id'] = $payment_id;

            $request_data = array();
            $request_data['up_status'] = 9;
            $request_data['up_modified_date'] = $this->utc_time;

            $update_state = $this->Common_model->update(TBL_USER_PAYMENTS, $request_data, $where);
            if ($update_state > 0) {
                $response['success'] = 'true';
                $response['message'] = lang("userpayment_delete_success");
            } else {
                $response['message'] = lang("userpayment_delete_failure");
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

}
