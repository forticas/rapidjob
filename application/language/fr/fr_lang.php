<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* Header */
$lang['HEADER_MENU_HELP']                                       = "CONSEILS/AIDE";
$lang['HEADER_MENU_SIGNUP']                                     = "Créer un Compte";
$lang['HEADER_MENU_CONTACTUP']                                  = "S'identifier";
$lang['HEADER_MENU_MYBASKET']                                   = "Mes commandes";
$lang['HEADER_MENU_PROSPACE']                                   = "Espace pro";
$lang['HEADER_MENU_REGISTER_RECRUITER']                         = "REGISTRE LE RECRUTEUR";

/* Candidate Header */
$lang['CANDIDATE_HEADER_USER_MENU']                             = 'Bonjour ';
$lang['CANDIDATE_HEADER_USER_SUB_MENU_PROFILE']                 = 'Mon profil';
$lang['CANDIDATE_HEADER_USER_SUB_MENU_PERSONAL_INFO']           = 'Mes informations PERSONNELLES';
$lang['CANDIDATE_HEADER_USER_SUB_MENU_MY_DOCUMENTS']            = 'Mes documents';
$lang['CANDIDATE_HEADER_USER_SUB_MENU_MY_APPLICATION']          = 'Mes candidatures spontanées';
$lang['CANDIDATE_HEADER_USER_SUB_MENU_DISCONNECT']              = 'Déconnexion';

/* Professtional Header */
$lang['PROF_HEADER_USER_MENU']                                  = 'Bonjour ';
$lang['PROF_HEADER_USER_SUB_MENU_PROFILE']                      = 'Mon profil ENTREPRISE';
$lang['PROF_HEADER_USER_SUB_MENU_ACCESS_CV']                    = 'ACCÈS À LA CVTHÈQUE';
$lang['PROF_HEADER_USER_SUB_MENU_MY_FAVORITE_PROFILES']         = 'MES PROFILS FAVORIS';
$lang['PROF_HEADER_USER_SUB_MENU_RECEIVING_RESULTS']            = 'RECEVOIR LES CV EN PRIORITÉ';
$lang['PROF_HEADER_USER_SUB_MENU_DISCONNECT']                   = 'DÉCONNEXION';

/* Footer */
$lang['FOOTER_SEND_APPLICATION']                                = "ENVOYER MA CANDIDATURE";
$lang['FOOTRE_MY_CV']                                           = "MON CV";
$lang['FOOTER_MOTIVATION_LETTER']                               = "MA LETTRE DE MOTIVATION";
$lang['FOOTER_COMPANY_AREA']                                    = "ESPACE ENTREPRISE";
$lang['FOOTER_SHOOPING_CART']                                   = "Mon panier";
$lang['FOOTRE_SUBSCRIBE']                                       = "M'inscrire";
$lang['FOOTER_CONTACTUS']                                       = "S'identifier";
$lang['FOOTER_TIPS_WRITING']                                    = "Conseils/Rédaction";
$lang['FOOTER_CONTACT_TITLE']                                   = "CONTACTEZ-NOUS";
$lang['FOOTER_SOCIAL_SHARING']                                  = "SUIVEZ-NOUS SUR";
$lang['FOOTER_COYRIGHT_TEXT']                                   = 'Rapid job © %1$s - Tous droits réservés - Mentions légales - CGV';

/* Common */
$lang['EDIT_BUTTON']                                            = "Modifier";
$lang['MESSAGE_MISSING']                                        = "Le message manque.";
$lang['COMMON_EDIT_INFO_BUTTON']                                = "Modifier les informations";
$lang['COMMON_REMOVE_BUTTON']                                   = "Supprimer";
$lang['COMMON_PERIOD']                                          = "Période";
$lang['COMMON_TO']                                              = "Au";
$lang['COMMON_TITLE']                                           = "Titre";
$lang['COMMON_ADD_BTN']                                         = "Enregistrer";
$lang['COMMON_CANCEL_BTN']                                      = "Annuler";
$lang['COMMON_ADD_MORE']                                        = "Ajoutez plus";
$lang['COMMON_DESCRIPTION']                                     = "Description";
$lang['COMMON_INVALID_PARAMS']                                  = "Paramètres non valides";
$lang['COMMON_EMAIL_ALREADY_REGISTERED']                        = "Cette adresse email déjà enregistrée avec nous.";
$lang['COMMON_FREE']                                            = "gratuit";
$lang['COMMON_FREES']                                           = "gratuits";
$lang['COMMON_RATE']                                            = "Tarif";
$lang['COMMON_TO_SEND']                                         = "Envoyez";
$lang['COMMON_SENT']                                            = "Envoyé";
$lang['COMMON_PENDING']                                         = "en attendant";
$lang['COMMON_VALIDATE']                                        = "Valider";
$lang['COMMON_PROBLEM_IN_ACTION']                               = "Problème dans l'exécution de l'action.";
$lang['COMMON_DURATION']                                        = "Fargamel";
$lang['COMMON_SAVE']                                            = "Enregistrer";
$lang['COMMON_SELECT_VALID_START_DATE']                         = "Sélectionnez la date de début valide.";
$lang['COMMON_SELECT_VALID_END_DATE']                           = "Sélectionnez la date d'expiration valide.";
$lang['COMMON_SELECT_INVALID']                                  = "Sélection non valide";
$lang['COMMON_CHOOSE_FILE']                                     = "Choisissez un fichier..";
$lang['COMMON_APPLICATION_TITLE']                               = "candidature";
$lang['COMMON_REQUIRED_FIELD']                                  = "Champs requis";
$lang['COMMON_THIS_REQUIRED_FIELD']                             = "Ce champ est obligatoire.";
$lang['COMMON_BUSINESS_NAME']                                   = "entreprise";
$lang['COMMON_SHOW']                                            = "Visualiser";
$lang['COMMON_EDIT']                                            = "Modifier";
$lang['COMMON_CONFIRM']                                         = "Confirmation";
$lang['COMMON_GO_TO_HOME']                                      = "Aller à la page d'accueil";
$lang['COMMON_404']                                             = "Cette page n'existe pas";
$lang['COMMON_AND']                                             = "ET";
$lang['COMMON_CIVILITY']                                        = "Choisissez votre civilité";
$lang['COMMON_CONFIRM_PASSWORD']                                = "Confirmation Mot de passe";
$lang['COMMON_INTERNAL_ERROR']                                  = "Quelque chose s'est mal passé, essayez plus tard ...";
$lang['COMMON_MY']                                              = "Mon";
$lang['COMMON_PHONE']                                           = "Téléphone";
$lang['COMMON_ADDRESS']                                         = "Adresse";
$lang['COMMON_CITY']                                            = "Ville";
$lang['COMMON_POSTAL_CODE']                                     = "Code postal";
$lang['COMMON_COUNTRY']                                         = "Pays";
$lang['COMMON_SAVE_CHANGES']                                    = "Enregistrer les modifications";
$lang['COMMON_RECORD']                                          = "Enregistrer";
$lang['COMMON_MADAM']                                           = "Madame";
$lang['COMMON_SIR']                                             = "Monsieur";
$lang['COMMON_SEARCH']                                          = "Rechercher";
$lang['COMMON_MORE']                                            = "Voir plus";
$lang['COMMON_ADD_TO_FAVOURITE']                                = "Ajoutez aux favoris";
$lang['COMMON_DOWNLOAD_CV']                                     = "Télécharger le cv";
$lang['COMMON_SEARCH_BY_JOB']                                   = "Recherche par métier";
$lang['COMMON_SEARCH_BY_ACTIVITY_AREA']                         = "Recherche par zone d'activité";
$lang['COMMON_SEARCH_BY_LOCATION']                              = "Recherche par lieu";
$lang['COMMON_SEARCH_BY_LOCATION']                              = "Tout sélectionner";
$lang['COMMON_SELECT_ALL']                                      = "Tout sélectionner";
$lang['COMMON_DESELECT_ALL']                                    = "Tout déselectionner";
$lang['COMMON_ITEMS_SELECTED']                                  = "items sélectionnés";
$lang['COMMON_USER_CANDIDATE']                                  = "Candidat";
$lang['COMMON_USER_RECRUITER']                                  = "Recruteur";
$lang['COMMON_FAVOURITE_REMOVE']                                = "Supprimer le favori";
$lang['COMMON_CV']                                              = "CV";
$lang['COMMON_OR']                                              = "OU";
$lang['COMMON_TITLE_HOBBIE']                                    = "Loisir";
$lang['COMMON_INVALID_EMAIL']                                   = "Entrez une adresse e-mail valide.";
$lang['COMMON_NOTICE']                                          = "Remarque";
$lang['COMMON_SUBSCRIBE']                                       = "Souscrire";
$lang['COMMON_PRICES']                                          = "Tarifs";
$lang['COMMON_SUBSCRIBE_SERVICE']                               = "Souscrire au service";
$lang['COMMON_SEARCH_JOB']                                      = "Métier recherché";
$lang['COMMON_SEARCH_AREA']                                     = "Secteur d'activité";
$lang['COMMON_SEARCH_LOCATION']                                 = "Lieu recherché";
$lang['COMMON_SEARCH_WORK_PLACES_LABEL']                        = "Lieux de travail";
$lang['COMMON_SEARCH_WORK_PLACES_LIMIT']                        = "(vous pouvez selectionner juqu'à 5 lieux)";
$lang['COMMON_SEARCH_EDUCATION_LEVEL']                          = "Niveau d'éducation";
$lang['COMMON_SEARCH_NO_ACTIVITY_AREA_SET']                     = "Zone d'activité non disponible";
$lang['COMMON_SEARCH_CHOOSE_ACTIVITY_AREA']                     = "Secteur d'activité";
$lang['COMMON_JOB_NOT_DEFINED']                                 = "Les détails de l'emploi ne sont pas disponibles";
$lang['COMMON_EXPERIENCE_NOT_DEFINED']                          = "Détails de l'expérience non disponibles";
$lang['COMMON_TRAINING_NOT_DEFINED']                            = "Détails de formation non disponibles";
$lang['COMMON_SKILL_NOT_DEFINED']                               = "Détails de compétences non disponibles";
$lang['COMMON_HOBBIES_NOT_DEFINED']                             = "Les détails de loisirs ne sont pas disponibles";
$lang['COMMON_USER_DETAILS_NOT_DEFINED']                        = "Détails de l'utilisateur non disponibles";
$lang['COMMON_ERROR_CHANGE_LANGUAGE']                           = "Erreur lors du changement de langue";
$lang['COMMON_OTHER']                                           = "Autre";
$lang['COMMON_GENERATE_CV_MESSAGE']                             = "Avant d'utiliser le travail rapide cv, vous devez générer un travail rapide cv à partir de votre profil.";
$lang['COMMON_EDIT_CV']                                         = "Modifier CV";
$lang['COMMON_EDIT_MY_CV']                                      = "Modifier mon CV";
$lang['COMMON_CV_FILE_MISSING']                                 = "Le fichier CV manque";
$lang['COMMON_JOB_WANTED']                                      = "Emploi recherché";
$lang['COMMON_ACTIVE']                                          = "Actif";
$lang['COMMON_INACTIVE']                                        = "Inactif";
$lang['COMMON_CANCELED']                                        = "Annulé";
$lang['COMMON_MES']                                             = "Mes";
$lang['COMMON_SELECT_NATIONALITY']                              = "Sélectionner la nationalité";
$lang['COMMON_SELECT_COUNTRY']                                  = "Sélectionner le pays";
$lang['COMMON_NATIONALITY_EMPTY']                               = "Aucune nationalité disponible";
$lang['COMMON_COUNTRY_EMPTY']                                   = "Aucun pays disponible";
$lang['COMMON_MONTH']                                           = "Mois";
$lang['COMMON_PLAN_EXPIRE']                                     = "Votre plan est expiré";
$lang['COMMON_ADD_OTHER_WORKJOB']                               = "Ajouter votre titre d'emploi";
$lang['COMMON_ADD_OTHER_ACTIVITY_AREA']                         = "Ajouter votre titre de zone d'activité";
$lang['COMMON_ADD_OTHER_WORKPLACE']                             = "Ajouter votre titre de lieu de travail";
$lang['COMMON_ADD_OTHER_EDUCATION']                             = "Ajoutez votre titre d'éducation";

/* login */
$lang['LOGIN_INVALID_EMAIL_OR_PASSWORD']                        = "email ou mot de passe invalide";
$lang['LOGIN_ACCOUNT_BLOCKED']                                  = "Votre compte est bloqué par admin";

/* register */
$lang['REGISTER_SUCCESS']                                       = "Votre inscription réussie.";
$lang['REGISTER_FAILURE']                                       = "Erreur lors de l'enregistrement de vos coordonnées";
$lang['REGISTER_EXPIRE_CODE']                                   = "Votre code d'invitation a expiré.";
$lang['REGISTER_PASSWORD_VALIDATION_MESSAGE']                   = "Saisissez une combinaison d’au moins 6 caractères (chiffres et lettres en minuscules et majuscules avec un caractère spécial obligatoire)";

/* NEWS LATTER */
$lang['NEWSLATTER_SUCCESS']                                     = "Vous êtes inscrit avec succès";
$lang['NEWSLATTER_SUBJECT']                                     = "Bulletin d'information";
$lang['NEWSLATTER_ALREADY_REGISTERED']                          = "Vous avez déjà enregistré le bulletin";

/** Home  Page **/
$lang['HOME_COVERPAGE_TXT_1']                                   = "la meilleure ";
$lang['HOME_COVERPAGE_TXT_2']                                   = "solution ";
$lang['HOME_COVERPAGE_TXT_3']                                   = "pour ";
$lang['HOME_COVERPAGE_TXT_4']                                   = "tous ";
$lang['HOME_COVERPAGE_SEARCH_PLACEHOLDER']                      = "*** Région, Département, Ville, …";
$lang['HOME_COVERPAGE_SEARCH_BUTTON']                           = "Trouver des entreprises";
$lang['HOME_COVERPAGE_SEARCH_BUTTON']                           = "Trouver des entreprises";
$lang['HOME_STEPS_TITLE_1']                                     = "Ciblez";
$lang['HOME_STEPS_TEXT_1']                                      = "Les entreprises et les cabinets de recrutement souhaités";
$lang['HOME_STEPS_TITLE_2']                                     = "Envoyez";
$lang['HOME_STEPS_TEXT_2']                                      = "Votre candidature à des miliers de recruteurs";
$lang['HOME_STEPS_TITLE_3']                                     = "Recevez";
$lang['HOME_STEPS_TEXT_3']                                      = "Des réponses positives et passez des entretiens d’embauche";
$lang['HOME_STEPS_BUTTON']                                      = "Envoyer ma candidature";
$lang['HOME_BANNER_BLOCK_1_TITLE']                              = "Envoyez votre candidature";
$lang['HOME_BANNER_BLOCK_1_SUB_TITLE']                          = "Jusqu’à 125 adresses gratuites";
$lang['HOME_BANNER_BLOCK_1_LIST1']                              = "Créez votre CV";
$lang['HOME_BANNER_BLOCK_1_LIST2']                              = "Rédigez une lettre de motivation";
$lang['HOME_BANNER_BLOCK_1_LIST3']                              = "Envoyez le tout !";
$lang['HOME_BANNER_BLOCK_2_TITLE']                              = "Créez votre cv";
$lang['HOME_BANNER_BLOCK_2_SUB_TITLE']                          = "Plus de 20 modèles différents";
$lang['HOME_BANNER_BLOCK_2_LIST1']                              = "Créez votre CV en ligne";
$lang['HOME_BANNER_BLOCK_2_LIST2']                              = "Personnalisez le à volonté";
$lang['HOME_BANNER_BLOCK_2_LIST3']                              = "Envoyez le directement";
$lang['HOME_SLIDER_CAPTION_TEXT']                               = "J’ai reçu une réponse d’un employeur dans les 48 heures. Plutôt rassurant et franchement sérieux Merci.";
$lang['HOME_SLIDER_QUOTE']                                      = "ILS ONT TROUVÉ UN JOB GRÂCE a candidature spontanee";
$lang['HOME_SLIDER_TESTIMONIAL']                                = "Paul, 25 ans, fraîchement diplômé de son école d’ingénieur, est à la recherche de son premier emploi. Paul a toujours rêvé de travailler dans l’aéronautique… Après des mois de recherches infructueuses, Paul tombe sur Rapid Job. Il sélectionne les entreprises et cabinets de recrutement qui l’intéressent, envoie son CV et sa lettre de motivation, reçoit des réponses positives, passe des entretiens d’embauche et décroche le job de ses rêves ! Grâce à Rapid Job, Paul a trouvé son emploi idéal. Alors, pourquoi pas vous ?";
$lang['HOME_SLIDER_TESTIMONIAL_BUTTON']                         = "Créer mon compte";
$lang['HOME_NEWS_TITLE_1']                                      = "L'ACTUALITÉ DE ";
$lang['HOME_NEWS_TITLE_2']                                      = "L'emploi ";
$lang['HOME_NEWS_TITLE_3']                                      = "& Du ";
$lang['HOME_NEWS_TITLE_4']                                      = "Recrutement ";
$lang['HOME_NEWS_IMAGE_HOVER_TEXT']                             = "en savoir plus ";
$lang['HOME_NEWS_IMAGE_TITLE_1']                                = "Comment trouver un emploi au jour d'aujourd’hui?";
$lang['HOME_NEWS_IMAGE_TEXT_1']                                 = "5 points clés pour trouver un travail  en utilisant internet et les réseaux sociaux";
$lang['HOME_NEWS_IMAGE_TITLE_2']                                = "Comment trouver un emploi au jour d'aujourd’hui?";
$lang['HOME_NEWS_IMAGE_TEXT_2']                                 = "5 points clés pour trouver un travail  en utilisant internet et les réseaux sociaux";
$lang['HOME_NEWS_IMAGE_TITLE_3']                                = "Comment rebondir face au chômage ?";
$lang['HOME_NEWS_IMAGE_TEXT_3']                                 = "Se retrouver au chômage est une véritable épreuve.  Il faut faire face aux sentiments contradictoires, la joie de retrouver du temps pour soi mais aussi  le stress considérable de ne pas retrouver d’emploi.";
$lang['HOME_NEWSLETTER_TITLE']                                  = "NE RATEZ AUCUNE ACTU, POUR CELA SOUSCRIVEZ À NOTRE NEWSLETTER !";
$lang['HOME_NEWSLETTER_INPUT_PLACEHOLDER']                      = "Votre Email";
$lang['HOME_NEWSLETTER_BUTTON']                                 = "Je M'inscris";
$lang['HOME_NO_NEWS_AVAILABLE']                                 = "Aucune nouvelle disponible";

/* Sign up Form */
$lang['SIGNUP_FORM_TITLE']                                      = "Inscrivez-vous Gratuitement";
$lang['SIGNUP_FORM_FIRST_NAME']                                 = "Prénom";
$lang['SIGNUP_FORM_LAST_NAME']                                  = "Nom";
$lang['SIGNUP_FORM_EMAIL']                                      = "Email";
$lang['SIGNUP_FORM_CONFIRM_EMAIL']                              = "Email de confirmation";
$lang['SIGNUP_FORM_PASSWORD']                                   = "Mot de passe";
$lang['SIGNUP_FORM_BUTTON']                                     = "Inscription";
$lang['SIGNUP_FORM_VIA_SOCIAL_TITLE']                           = "Ou inscrivez-vous via";

/* Sign in Form */
$lang['SIGNIN_FORM_TITLE']                                      = "Connexion";
$lang['SIGNIN_FORM_SOCIAL_TITLE']                               = "Me connecter avec";
$lang['SIGNIN_FORM_EMAIL']                                      = "Email";
$lang['SIGNIN_FORM_PASSWORD']                                   = "Mot de passe";
$lang['SIGNIN_FORM_BUTTON']                                     = "Connexion";
$lang['SIGNIN_FORM_ACCOUNT_TYPE']                               = "Type de compte de connexion";
$lang['SIGNIN_FORM_USER_CANDIDATE']                             = "Connectez-vous au compte candidat";
$lang['SIGNIN_FORM_USER_RECUITER']                              = "Connectez-vous au compte recruteur";
$lang['SIGNIN_FORM_USER_FORGOT_PASSWORD']                       = "Vous avez oublié votre mot de passe?";

/* Build CV page */
$lang['BUILDCV_PAGE_TITLE']                                     = 'Créez votre';
$lang['BUILDCV_PAGE_TITLE_RED']                                 = 'CV';
$lang['BUILDCV_PAGE_TAP_PHOTO_TEXT']                            = 'Télécharger votre photo';
$lang['BUILDCV_PAGE_SELECT_JOB']                                = 'Votre Métier';
$lang['BUILDCV_PAGE_SELECT_ACTIVITY_AREA']                      = "Secteur d'activité";
$lang['BUILDCV_PAGE_SELECT_WORK_PLACES']                        = "Lieux de travail ( vous pouvez selectionner juqu'à  5 lieux )";
$lang['BUILDCV_PAGE_SELECT_WORK_PLACES_LABEL']                  = "Lieux de travail";
$lang['BUILDCV_PAGE_SELECT_WORK_PLACES_OPT_1']                  = "Toulouse";
$lang['BUILDCV_PAGE_SELECT_WORK_PLACES_OPT_2']                  = "Marseille";
$lang['BUILDCV_PAGE_SELECT_WORK_PLACES_OPT_3']                  = "Bordeaux";
$lang['BUILDCV_PAGE_CONTRACT_TITLE']                            = "Type de contrat";
$lang['BUILDCV_PAGE_CONTRACT_TYPE_1']                           = "CDI";
$lang['BUILDCV_PAGE_CONTRACT_TYPE_2']                           = "CDD";
$lang['BUILDCV_PAGE_CONTRACT_TYPE_3']                           = "Stage";
$lang['BUILDCV_PAGE_CONTRACT_TYPE_4']                           = "Mission intérim";
$lang['BUILDCV_PAGE_CONTRACT_TYPE_5']                           = "Freelance";
$lang['BUILDCV_PAGE_EDUCATION_LEVEL']                           = "Niveau de formation";
$lang['BUILDCV_PAGE_MY_LANGUAGES']                              = "Mes langues";
$lang['BUILDCV_PAGE_ADD_LANGUAGES']                             = "Ajoutez une langue";
$lang['BUILDCV_PAGE_MY_SKILLS']                                 = "Mes compétences";
$lang['BUILDCV_PAGE_ADD_SKILLS']                                = "Ajoutez mes compétences";
$lang['BUILDCV_PAGE_MY_SKILLS']                                 = "Ajoutez mes compétences";
$lang['BUILDCV_PAGE_VIEW_PROFILE_BTN']                          = "Voir mon profil";
$lang['BUILDCV_PAGE_SEND_APPLICATION']                          = "Envoyer ma candidature";

/** added by Nitinkumar vaghani * */
$lang['BUILDCV_PAGE_ENTER_LANGUAGES']                           = "Entrez votre langue";
$lang['BUILDCV_PAGE_ENTER_SKILL']                               = "Entrez votre compétence";
$lang['BUILDCV_PAGE_NO_JOB_TYPE_SET']                           = "Type d'emploi non disponible";
$lang['BUILDCV_PAGE_NO_ACTIVITY_AREA_SET']                      = "Zone d'activité non disponible";
$lang['BUILDCV_PAGE_NO_WORK_PLACE_SET']                         = "Lieu de travail non disponible";
$lang['BUILDCV_PAGE_NO_EDU_LEVEL_SET']                          = "Niveau d'éducation non disponible";
$lang['BUILDCV_PAGE_CHOOSE_JOB']                                = "Choisissez votre métier";
$lang['BUILDCV_PAGE_CHOOSE_ACTIVITY_AREA']                      = "Choisissez le domaine d'activité";
$lang['BUILDCV_PAGE_CHOOSE_EDUCATION_LEVEL']                    = "Choisir le niveau d'éducation";
$lang['BUILDCV_PAGE_CHOOSE_WORKPLACE']                          = "Choisir le lieu de travail";
$lang['NOTHING_SELECTED']                                       = "Rien sélectionné";
$lang['NO_RESULT_MATCHED']                                      = "Aucun résultat ne correspond à";

/* Edit Profile*/
$lang['EDITPROFILE_PAGE_TITLE']                                 = "mes informations";
$lang['EDITPROFILE_PAGE_TITLE_RED']                             = "personelles";
$lang['EDITPROFILE_PAGE_EMAIL_INFO']                            = "Email de connexion";
$lang['EDITPROFILE_PAGE_EMAIL_LABEL']                           = "Email";
$lang['EDITPROFILE_PAGE_CIVIL_INFO']                            = "Etat civil";
$lang['EDITPROFILE_PAGE_CIVIL_CIVILITY']                        = "Civilité";
$lang['EDITPROFILE_PAGE_CIVIL_CIVILITY_MADAM']                  = "Madame";
$lang['EDITPROFILE_PAGE_CIVIL_CIVILITY_SIR']                    = "Monsieur";
$lang['EDITPROFILE_PAGE_CIVIL_CIVILITY_FIRSTNAME']              = "Prénom";
$lang['EDITPROFILE_PAGE_CIVIL_CIVILITY_LASTNAME']               = "Nom";
$lang['EDITPROFILE_PAGE_CIVIL_CIVILITY_NATIONALITY']            = "Nationalité";
$lang['EDITPROFILE_PAGE_NATIONALITY']                           = "Sélectionner la nationalité";
$lang['EDITPROFILE_PAGE_CIVILITY']                              = "Choisir la civilité";
$lang['EDITPROFILE_PAGE_COUNTRY']                               = "Sélectionner le pays";
$lang['EDITPROFILE_PAGE_ADDRESS_INFO']                          = "Adresse postal";
$lang['EDITPROFILE_PAGE_ADDRESS_INFO_ADDRESS']                  = "Adresse";
$lang['EDITPROFILE_PAGE_ADDRESS_INFO_CITY']                     = "Ville";
$lang['EDITPROFILE_PAGE_ADDRESS_INFO_POSTAL_CODE']              = "Code postal";
$lang['EDITPROFILE_PAGE_ADDRESS_INFO_COUNTRY']                  = "Pays";
$lang['EDITPROFILE_PAGE_SAVE_BTN']                              = "Enregistrer les modifications";
$lang['EDITPROFILE_PAGE_CHANGE_PASSWORD']                       = "Changer mon mot de passe";
$lang['EDITPROFILE_PAGE_OLD_PASSWORD']                          = "Mot de passe actuel";
$lang['EDITPROFILE_PAGE_NEW_PASSWORD']                          = "Nouveau mot de passe";
$lang['EDITPROFILE_PAGE_CONFIRM_PASSWORD']                      = "Confirmer le nouveau mot de passe";
$lang['EDITPROFILE_PAGE_CHANGE_PASSWORD_BTN']                   = "Enregistrer";
$lang['EDITPROFILE_PAGE_CHANGE_PASSWORD_FAILED']                = "Problème de modification du mot de passe.";
$lang['EDITPROFILE_PAGE_CHANGE_PASSWORD_INVALID']               = "Votre mot de passe actuel est incorrect. Essayez à nouveau.";
$lang['EDITPROFILE_PAGE_CHANGE_PASSWORD_SUCCESS']               = "Mot de passe changé avec succès.";

/* Search Result */
$lang['SEARCH_PAGE_TITLE']                                      = "RÉSULTATS DE";
$lang['SEARCH_PAGE_TITLE_RED']                                  = "RECHERCHE";
$lang['SEARCH_PAGE_TITLE_TEXT']                                 = "Utilisez ce moteur de recherche pour trouver l’offre qui vous correspond";
$lang['SEARCH_PAGE_SEARCH_BTN']                                 = "Trouver des entreprises";
$lang['SEARCH_PAGE_SEARCH_RESULT_TITLE']                        = "entreprises correspondent à votre recherche";
$lang['SEARCH_PAGE_SEARCH_RESULT_PURPOSE']                      = "À PROPOS";
$lang['SEARCH_PAGE_SEARCH_RESULT_BTN']                          = "Inscription";
$lang['SEARCH_PAGE_SNED_CV_STEP_TITLE_1']                       = "Ciblez";
$lang['SEARCH_PAGE_SNED_CV_STEP_TEXT_1']                        = "Les entreprises et les cabinets de recrutement Souhaites";
$lang['SEARCH_PAGE_SNED_CV_STEP_TITLE_2']                       = "Envoyez";
$lang['SEARCH_PAGE_SNED_CV_STEP_TEXT_2']                        = "Votre candidature à des miliers de recruteurs";
$lang['SEARCH_PAGE_SNED_CV_STEP_TITLE_3']                       = "Recevez";
$lang['SEARCH_PAGE_SNED_CV_STEP_TEXT_3']                        = "Des réponses positives et entretiens d'embauche";
$lang['SEARCH_PAGE_SNED_CV_STEP_BUTTON']                        = "Envoyer ma candidature";
$lang['SEARCH_PAGE_NO_RESULT']                                  = "résultats pour vos critères de recherche";
$lang['SEARCH_PAGE_CREATE_CV']                                  = "Créer mon CV";
$lang['SEARCH_PAGE_ADD_TO_FAVORITE_SUCCESS']                    = "Société ajoutée avec succès dans votre liste préférée.";
$lang['SEARCH_PAGE_ADD_TO_FAVORITE_FAILED']                     = "Problème tout en ajoutant de l'entreprise dans votre liste préférée.";
$lang['SEARCH_PAGE_REMOVE_FROM_FAVORITE_SUCCESS']               = "Société retirée avec succès de votre liste préférée.";
$lang['SEARCH_PAGE_REMOVE_FROM_FAVORITE_FAILED']                = "Problème tout en supprimant Company de votre liste préférée.";

/* Show Profile Page */
$lang['SHOW_PROFILE_PAGE_TITLE']                                = 'Mon';
$lang['SHOW_PROFILE_PAGE_TITLE_RED']                            = 'Profil';
$lang['SHOW_PROFILE_PAGE_DOWNLOAD_PROFILE_PICTURE']             = 'Téléchargez votre photo de profil';
$lang['SHOW_PROFILE_PAGE_UPLOAD_PROFILE_PICTURE']               = 'Téléchargez votre photo';
$lang['SHOW_PROFILE_PAGE_INTRODUSE_YOUSELF']                    = 'Présentez-vous';
$lang['SHOW_PROFILE_PAGE_SEND_YOUR_APPLICATION_BTN']            = 'Envoyez une candidature spontanée';
$lang['SHOW_PROFILE_PAGE_DOWNLOAD_MY_CV']                       = 'Téléchargez mon CV';
$lang['SHOW_PROFILE_PAGE_MY_EXPERIANCES']                       = 'Mes expériences';
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES']                      = 'Ajoutez une expérience';
$lang['SHOW_PROFILE_PAGE_MY_TRAININGS']                         = 'Mes formations';
$lang['SHOW_PROFILE_PAGE_ADD_TRAININGS']                        = 'Ajoutez une formation';
$lang['SHOW_PROFILE_PAGE_ADD_TRAININGS_HEADING']                = 'AJOUTER UNE FORMATION';
$lang['SHOW_PROFILE_PAGE_MY_SKILLS']                            = 'Mes compétences';
$lang['SHOW_PROFILE_PAGE_ADD_SKILLS_HEADING']                   = 'AJOUTER UNE COMPÉTENCE';
$lang['SHOW_PROFILE_PAGE_ADD_SKILLS']                           = 'Ajoutez une compétence';
$lang['SHOW_PROFILE_PAGE_ADD_SKILLS_MORE']                      = 'Ajoutez une compétences';
$lang['SHOW_PROFILE_PAGE_MY_HOBBIES']                           = 'Mes loisirs';
$lang['SHOW_PROFILE_PAGE_ADD_HOBBIES']                          = 'Ajoutez un loisir';
$lang['SHOW_PROFILE_PAGE_ADD_HOBBIES_HEADING']                  = 'AJOUTER UN LOISIR';
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_COMPANY_NAME']         = "Nom de l'entreprise";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_TITLE']                = "Titre";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PLACE']                = "Lieu";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD']               = "Période";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD_MONTH']         = "Mois";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD_YEAR']          = "Année";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD_TO']            = "Au";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_CHECKBOX_POSITION']    = "Toujours en poste actuellement";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_DESCRIPTION_LBL']      = "Description";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_ADD_BTN']              = "Enregistrer";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_CANCEL_BTN']           = "Annuler";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_LABEL']                  = "Formation";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_ESTABLISSMENT']          = "Etablissement";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_DESCRIPTION_LABEL']      = "Description de la formation";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_PERIOD']                 = "Période";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_PERIOD_MONTH']           = "Mois";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_PERIOD_YEAR']            = "Année";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_PERIOD_TO']              = "Au";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_ADD_BTN']                = "Enregistrer";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_CANCEL_BTN']             = "Annuler";
$lang['SHOW_PROFILE_PAGE_ADD_SKILL_AREA_LABEL']                 = "Domaine de Compétence";
$lang['SHOW_PROFILE_PAGE_ADD_SKILL_ESTABLISSMENT']              = "Compétence";
$lang['SHOW_PROFILE_PAGE_ADD_SKILL_ADD_BTN']                    = "Ajoutez une ecompétences";
$lang['SHOW_PROFILE_PAGE_ADD_SKILL_ADD_BTN_NEW']                = "Ajoutez une compétence";
$lang['SHOW_PROFILE_PAGE_ADD_SKILL_ADD_BTN']                    = "Enregistrer";
$lang['SHOW_PROFILE_PAGE_ADD_SKILL_CANCEL_BTN']                 = "Annuler";
$lang['SHOW_PROFILE_PAGE_BIO_PLACEHOLDER']                      = "Entrez quelques mots à votre sujet.";
$lang['SHOW_PROFILE_PAGE_NO_JOB_TYPE_SET']                      = "Définissez votre type de travail";
$lang['SHOW_PROFILE_PAGE_NO_ACTIVITY_AREA_SET']                 = "Définir votre zone d'activité";
$lang['SHOW_PROFILE_PAGE_NO_WORK_PLACE_SET']                    = "Définir votre lieu de travail";
$lang['SHOW_PROFILE_PAGE_NO_EDU_LEVEL_SET']                     = "Définir votre niveau d'éducation";
$lang['SHOW_PROFILE_PAGE_ADD_YOUR_EXP']                         = "Ajoutez les détails de vos expériences";
$lang['SHOW_PROFILE_PAGE_ADD_YOUR_TRAINING']                    = "Ajoutez vos informations de formation";
$lang['SHOW_PROFILE_PAGE_ADD_YOUR_SKILL']                       = "Ajoutez vos compétences";
$lang['SHOW_PROFILE_PAGE_ENTER_BIO_PLACEHOLDER']                = "Entrez quelques mots à votre sujet.";
$lang['SHOW_PROFILE_PAGE_BIO_UPDATE_SUCCESS']                   = "Vos détails ont été enregistrés avec succès.";
$lang['SHOW_PROFILE_PAGE_BIO_UPDATE_FAILED']                    = "Problème lors de la mise à jour de vos informations.";
$lang['SHOW_PROFILE_PAGE_ADD_YOUR_HOBBIES']                     = "Ajoutez vos loisirs";
$lang['SHOW_PROFILE_PAGE_BIO_UPDATE_SAME']                      = "Aucune modification n'a été trouvée avec la description spécifiée";
$lang['SHOW_PROFILE_PAGE_EXP_SUCESS']                           = "Votre expérience a été ajoutée avec succès.";
$lang['SHOW_PROFILE_PAGE_EXP_FAILED']                           = "Problème lors de l'insertion de votre expérience.";
$lang['SHOW_PROFILE_PAGE_EXP_DATE_INVALID']                     = "La sélection de votre période d'expérience n'est pas valide.";
$lang['SHOW_PROFILE_PAGE_TRAINING_SUCESS']                      = "Vos données de formation ont été ajoutées avec succès.";
$lang['SHOW_PROFILE_PAGE_TRAINING_FAILED']                      = "Problème dans l'insertion de vos données de formation.";
$lang['SHOW_PROFILE_PAGE_SKILL_SUCESS']                         = "Votre compétence a été ajoutée avec succès.";
$lang['SHOW_PROFILE_PAGE_SKILL_FAILED']                         = "Problème dans l'insertion de votre compétence.";
$lang['SHOW_PROFILE_PAGE_HOBBIES_SUCESS']                       = "Vos passe-temps ont été ajoutés avec succès.";
$lang['SHOW_PROFILE_PAGE_HOBBIES_FAILED']                       = "Problème dans insérer vos hobbies.";
$lang['SHOW_PROFILE_PAGE_HOBBIES_NAME']                         = "Nom de hobby";
$lang['SHOW_PROFILE_PAGE_LANGUAGES_FAILED']                     = "Problème dans l'insertion de vos langues.";
$lang['SHOW_PROFILE_PAGE_WORKING_HERE']                         = "Toujours en train de travailler ici";
$lang['SHOW_PROFILE_PAGE_YOUR_LANGUAGES']                       = "Vos langues";
$lang['SHOW_PROFILE_SELECT_START_MONTH']                        = "Sélectionnez le mois de début";
$lang['SHOW_PROFILE_SELECT_START_YEAR']                         = "Sélectionnez l'année de début";
$lang['SHOW_PROFILE_SELECT_END_MONTH']                          = "Sélectionnez le mois de fin";
$lang['SHOW_PROFILE_SELECT_END_YEAR']                           = "Sélectionner l'année de fin";
$lang['SHOW_PROFILE_PROVIDE_DETAILS']                           = "Vous devez d'abord créer CV à partir de mes documents et fournir vos expériences, cours, compétences et loisirs avant de télécharger cv ...!";
$lang['SHOW_PROFILE_CHOOSE_DOWNLOAD_FORMAT']                    = "Choisissez le format";
$lang['SHOW_PROFILE_DOWNLOAD_AS_PDF']                           = "Télécharger CV en format PDF";
$lang['SHOW_PROFILE_DOWNLOAD_AS_JPEG']                          = "Télécharger CV en JPEG";
$lang['SHOW_PROFILE_DOWNLOAD_AS_PNG']                           = "Télécharger CV en PNG";
$lang['SHOW_PROFILE_JOB_TITLE']                                 = "Profession";
$lang['SHOW_PROFILE_GENERATE_CV_ERROR']                         = "Vous devez d’abord créer votre CV et renseigner vos expériences, compétences, loisirs Et zone d’activité avant de générer ou de télécharger un CV";
$lang['SHOW_PROFILE_GENERATE_CV']                               = "Enregistrer le CV";
$lang['SHOW_PROFILE_GENERATE_CV_SUCCESS']                       = "Votre CV a été généré avec succès.";
$lang['SHOW_PROFILE_GENERATE_CV_FAILED']                        = "Problème lors de la génération de votre cv, essayez à nouveau ...!";
$lang['SHOW_PROFILE_HELP_TEXT']                                 = "Vous devez d’abord créer votre CV afin d’enregistrer les modifications ci-dessus";
$lang['SHOW_PROFILE_MY_ACTIVITY_AREA']                          = "Mes domaines d'activité";

/** free item page content */
$lang['FREE_ITEM_TITLE']                                        = "PROFITEZ DE VOS 100 PREMIERS ENVOIS";
$lang['FREE_ITEM_MATCH_CRITERIA']                               = "entreprises correspondent à vos critères";
$lang['FREE_ITEM_SEND_TO_FRIENDS']                              = "Envoyez à vos amis";
$lang['FREE_ITEM_EMAIL_PLACEHOLDER']                            = "Email numéro";
$lang['FREE_ITEM_ADD_POINTS']                                   = 'Et obtenez %1$s envois supplémentaires';
$lang['FREE_ITEM_ADD_POINTS_NEW']                               = 'Partagez pour profiter de %1$s envois gratuits !';
$lang['FREE_ITEM_TO_SELECT']                                    = "Sélectionner";
$lang['FREE_ITEM_CHOOSE_FORMULA']                               = "OU Choisissez votre formule";
$lang['FREE_ITEM_MODIFY_TERIFF']                                = "Modulez votre tarif";
$lang['FREE_ITEM_RATE_MESSAGE']                                 = "Faites varier votre tarif en fonction du nombre d'envois.";
$lang['FREE_ITEM_NUMBER_OF_POINTS']                             = "Nombre d'envois";
$lang['FREE_ITEM_SEND_YOUR_CV']                                 = "Sélectionnez les entreprises qui vous correspondent";
$lang['FREE_ITEM_SEND_YOUR_CV_TO_COMPANY']                      = "Envoyez leur votre candidature";
$lang['FREE_ITEM_SEND_UPTO']                                    = 'Vous pouvez envoyer jusqu\'à %1$s applications spontanées par courrier électronique';
$lang['FREE_ITEM_SEND_NO_COMPANY']                              = 'Société non trouvée.';
$lang['FREE_ITEM_SEND_MAX_SELECT_COMPANY']                      = 'Vous pouvez sélectionner jusqu\'à '.DEFAULT_MAXIMUM_COMPANY_SELECT.' entreprises seulement.';
$lang['FREE_ITEM_INVITATION_LINK']                              = "Lien de l'invitation de travail rapide";
$lang['FREE_ITEM_INVITATION_LINK_SHARE']                        = "Partagez le lien d'invitation sur facebook.";
$lang['FREE_ITEM_INVITATION_SENT_SUCCESS']                      = 'Invitation envoyée avec succès.';
$lang['FREE_ITEM_UPGRADE_TARIFF']                               = 'Vous devez mettre à niveau votre tarif pour envoyer vos applications spontanées';
$lang['FREE_ITEM_TARIFF_LIMIT_OVER']                            = 'Votre limite tarifaire pour les demandes spontanées envoyées par courrier électronique est terminée.';
$lang['FREE_ITEM_INVOICE_PER_EMAIL']                            = 'Envois par e-mail';
$lang['FREE_ITEM_INVOICE_PER_POST_OFFICE']                      = 'Envois par courrier postal';
$lang['FREE_ITEM_CHOOSE_FORMULA_AND_LIMIT']                     = "Choisissez le type de formule de votre demande et la limite de tarif valide";
$lang['FREE_ITEM_TARIF_LIMIT_REQUIRED']                         = 'La limite tarifaire est requise';
$lang['FREE_ITEM_TARIF_LIMIT_POST_OVER']                        = "Votre limite de tarif pour la demande d'envoi par courrier postal est terminée.";
$lang['FREE_ITEM_SEND_POST_UPTO']                               = 'Vous pouvez envoyer jusqu\'à %1$s applications spontanées par la poste';
$lang['FREE_ITEM_SHARE_POST']                                   = 'Bénéficiez des %1$s premiers envois gratuits en partageant le site RapidJob sur Facebook';
$lang['FREE_ITEM_CURRENT_TERIFF']                               = 'Tarif actuel:'; 
$lang['FREE_ITEM_COMPANY_REQUIRED']                             = "Soit sélectionnez la société de la liste ou choisissez votre type de formule d'application et la limite de tarif de mise à niveau";
$lang['FREE_ITEM_SELECT_PAYMENT_METHOD']                        = 'Sélectionnez le mode de paiement';
$lang['FREE_ITEM_SELECT_PAYMENT_TEXT']                          = 'Sélectionnez votre méthode de paiement et envoyez votre demande maintenant ...';
$lang['FREE_ITEM_ARE_YOU_SURE']                                 = 'Êtes-vous sûr?';
$lang['FREE_ITEM_ALERT_TEXT']                                   = 'Si vous modifiez votre tarif, la société sélectionnée sera désactivée dans la liste';
$lang['FREE_ITEM_ALERT_YES']                                    = 'Oui';
$lang['FREE_ITEM_ALERT_NO']                                     = 'Non, annuler';

/* send application */
$lang['SEND_APPLICATION_VALIDATE_AND_SEE']                      = "Valider et voir l’aperçu";
$lang['SEND_APPLICATION_ACCEPT_CGU']                            = "J'accepte les CGU";
$lang['SEND_APPLICATION_COMPANY_TO_EXCLUDED']                   = "Entreprises à exclure";
$lang['SEND_APPLICATION_USE_YOUR_CV']                           = "Utilisez votre CV Rapid job";
$lang['SEND_APPLICATION_JOIN_MY_COVER_LATTER']                  = "Joindre ma lettre de motivation";
$lang['SEND_APPLICATION_SENT_TO_COMPANY']                       = "Message envoyé aux entreprises";
$lang['SEND_APPLICATION_JOIN_MY_CV']                            = "Joindre mon CV";
$lang['SEND_APPLICATION_SEND_YOUR']                             = "Envoyez votre";
$lang['SEND_APPLICATION_APP']                                   = "SPONTANÉE";
$lang['SEND_APPLICATION_SELECT_CV']                             = "Attachez votre cv ou utilisez votre rapidjob cv";
$lang['SEND_APPLICATION_EMAIL_PLACEHOLDER']                     = "Entrez un email séparé par des virgules";
$lang['SEND_APPLICATION_EMAIL_INVALID']                         = "Ce champ ne permet que les emails séparés par une virgule";
$lang['SEND_APPLICATION_INSERT_ERROR']                          = "Problème lors de l'insertion des détails de votre application, Essayez à nouveau.";
$lang['SEND_APPLICATION_UPDATE_ERROR']                          = "Problème lors de la mise à jour des détails de votre application, Essayez de nouveau.";
$lang['SEND_APPLICATION_PROVIDE_DETAILS']                       = "Si vous souhaitez utiliser le CV rapide de Job CV, créez le CV à partir de mes documents et fournissez vos expériences, vos cours, vos compétences et vos détails de loisirs ...!";
$lang['SEND_APPLICATION_MESSAGE_PLACEHOLDER']                   = "Bonjour,

Vous trouverez ma candidature en pièce jointe. Je vous confirme ma forte motivation pour rejoindre votre entreprise.
Je me tiens à votre disposition pour échanger avec vous.

Cordialement,";

/* send application now */
$lang['SEND_APPLICATION_NOW_COMPANY_TO_EXCLUDED']               = "Entreprise à exclure";
$lang['SEND_APPLICATION_NOW_SELECTED_CV']                       = "CV sélectionné";
$lang['SEND_APPLICATION_NOW_SELECTED_LATTER']                   = "Lettre de motivation sélectionnée";
$lang['SEND_APPLICATION_NOW_SENT_MESSAGE']                      = "Message envoyé aux entreprises";
$lang['SEND_APPLICATION_NOW_SENT_SUCCESS']                      = "Votre demande a été envoyée avec succès.";
$lang['SEND_APPLICATION_NOW_SENT_FAILURE']                      = "Problème lors de l'envoi de votre application, essayez à nouveau ...!";
$lang['SEND_APPLICATION_NOW_SAVE_SUCCESS']                      = "Les données de votre application ont été enregistrées avec succès";
$lang['SEND_APPLICATION_NOW_CV_DETAILS_NOT_FOUND']              = "Vos détails cv n'ont pas été trouvés.";

/* my documents */
$lang['MY_DOCUMENTS_MY']                                        = "Mes";
$lang['MY_DOCUMENTS_DOCUMENT']                                  = "Documents";
$lang['MY_DOCUMENTS_JOIN_MY_CV']                                = "Joindre mon CV";
$lang['MY_DOCUMENTS_JOIN_MY_COVER_LATTER']                      = "Joindre ma lettre de motivation";
$lang['MY_DOCUMENTS_ADD_THIS_FILE']                             = 'Ajoutez ce fichier ('.DEFAULT_MAXIMUM_CV_SIZE_LIMIT_PHP.' Mo max)';
$lang['MY_DOCUMENTS_LIST']                                      = "Liste de mes documents";
$lang['MY_DOCUMENTS_CV_NOT_AVAILABLE']                          = "Fichier non disponible, Vous devez télécharger un nouveau fichier ....!";
$lang['MY_DOCUMENTS_DOC_NOT_FOUND']                             = "Les détails de votre document n'ont pas été trouvés.";
$lang['MY_DOCUMENTS_DOC_UPDATE_ERROR']                          = "Problème lors de la mise à jour des détails de votre document.";
$lang['MY_DOCUMENTS_CV_REMOVE_SUCCESS']                         = "Votre CV a été supprimé.";
$lang['MY_DOCUMENTS_COVER_REMOVE_SUCCESS']                      = "Votre lettre d'accompagnement a été supprimée.";
$lang['MY_DOCUMENTS_CV_REMOVE_FAILURE']                         = "Problème lors de la suppression de votre cv.";
$lang['MY_DOCUMENTS_COVER_REMOVE_FAILURE']                      = "Problème lors de la suppression de votre lettre de motivation.";
$lang['MY_DOCUMENTS_REMOVE_CV_CONFIRM']                         = "Voulez-vous vraiment supprimer ce cv? Si ce n'est pas le cas, cliquez sur Annuler. Il n'y a pas de défaire ...!";
$lang['MY_DOCUMENTS_REMOVE_COVER_CONFIRM']                      = "Voulez-vous vraiment supprimer cette lettre d'accompagnement? Si ce n'est pas le cas, cliquez sur Annuler. Il n'y a pas d'annulation ...! ";
$lang['MY_DOCUMENTS_CREATE_CV']                                 = "Créer mon CV";

/* send application list */
$lang['SEND_APPLICATION_LIST_MY']                               = "Mes";
$lang['SEND_APPLICATION_LIST_APPLICATION']                      = "CANDIDATURES";
$lang['SEND_APPLICATION_LIST_SPONTANEOUS']                      = "SPONTANÉES";
$lang['SEND_APPLICATION_LIST_CANDIDATURE']                      = "Candidature le";
$lang['SEND_APPLICATION_LIST_NO_OF_COMPANY']                    = "Nombre d'entreprises";
$lang['SEND_APPLICATION_LIST_STATE']                            = "État";
$lang['SEND_APPLICATION_LIST_APP_NOT_FOUND']                    = "Candidaturess spontanées non disponible.";

/** ALL MAIL CONSTANT */
/** invite friends */
$lang['MAIL_HELLO']                                             = "Bonjour";
$lang['MAIL_DEAR']                                              = "Cher";
$lang['MAIL_NEW_INVITATION']                                    = "Nouvel email d'invitation";
$lang['MAIL_WELCOME_TO']                                        = "Bienvenue à ";
$lang['MAIL_INVITE_YOU']                                        = "Votre ami vous a invité";
$lang['MAIL_REGARDS']                                           = "Cordialement";
$lang['MAIL_TEAM']                                              = "Équipe";
$lang['MAIL_INVITATION_LINK']                                   = "Lien d'invitation:";
$lang['MAIL_CLICK_HERE']                                        = "Cliquez ici pour vous inscrire";
$lang['MAIL_INVITATION_SEND_SUCCESS']                           = "Votre e-mail d'invitation a été envoyé avec succès.";
$lang['MAIL_INVITATION_SEND_FAILED']                            = "Problème lors de l'envoi de votre e-mail d'invitation.";
$lang['MAIL_INVITATION_SEND_SUCCESS_WITH_EXISTS']               = 'Votre e-mail d\'invitation a été envoyé avec succès à %1$s, et le courrier électronique %2$s est déjà invité.';
$lang['MAIL_INVITATION_ALREADY_SENT']                           = "Vos e-mails demandés déjà invités";
$lang['MAIL_NEWSLATTER_TEXT']                                   = "Vous enregistrez avec succès le bulletin d'information.<br>Maintenant vous obtenez toutes les mises à jour de votre compte de messagerie.";


/** send application mail **/
$lang['MAIL_APPLICATION_SUBJECT']                               = "Nouvelle demande d'emploi courrier";
$lang['MAIL_APPLICATION_INTEREST']                              = "Je suis très intéressé par votre entreprise. Mes qualifications et expérience correspondent à vos spécifications presque exactement.";
$lang['MAIL_APPLICATION_DOC_TEXT']                              = "Veuillez prendre quelques instants pour consulter mes documents de candidature:";
$lang['MAIL_APPLICATION_BACK_TO_SOON_TEXT']                     = "Ce serait un plaisir sincère de recevoir des nouvelles de vous bientôt pour discuter de cette occasion passionnante.";
$lang['MAIL_SINCERELY']                                         = "Cordialement";

/* forgot password */
$lang['FORGOT_PASSWORD_SUBJECT']                                = "Email de récupération de mot de passe";
$lang['FORGOT_PASSWORD_LINK_TEXT']                              = "Réinitialisez votre mot de passe";
$lang['FORGOT_PASSWORD_TEXT_1']                                 = "Nous avons reçu une demande de changement de mot de passe de votre compte.";
$lang['FORGOT_PASSWORD_TEXT_2']                                 = "Si vous ne l'avez pas demandé, ignorez ce courriel.";
$lang['FORGOT_PASSWORD_TEXT_3']                                 = "Votre mot de passe ne changera pas jusqu'à ce que vous accédez au lien ci-dessus et créez un nouveau.";
$lang['FORGOT_PASSWORD_EMAIL_NOT_REGISTERED']                   = "Ce courriel n'est pas enregistré avec nous.";
$lang['FORGOT_PASSWORD_EMAIL_SUCCESS']                          = "Le lien de réinitialisation du mot de passe a été envoyé à votre courrier électronique.";
$lang['FORGOT_PASSWORD_TOKEN_EXPIRED']                          = "Ce lien a expiré.";
$lang['FORGOT_PASSWORD_SEND']                                   = "Réinitialiser mon mot de passe";
$lang['FORGOT_PASSWORD_BACK_TO_LOGIN']                          = "Retour connexion";
$lang['FORGOT_PASSWORD_DESCRIPTION']                            = "Vous avez oublié votre mot de passe? Entrez votre adresse e-mail et nous vous enverrons un lien de récupération.";
/** END OF ALL MAIL CONSTANT */


/** ALL FILE VALIDATION CONSTANT */
$lang['ERROR_INVALID_FILE']                                     = 'Fichier non valide, le fichier doit être image et inférieur à '.DEFAULT_IMAGE_SIZE_LIMIT_MB.' Mo';
$lang['ERROR_INVALID_EXTENSION']                                = 'Type de fichier ou extension non valide, le fichier doit être image!';
$lang['ERROR_INVALID_SIZE']                                     = 'La taille de l\'image doit être inférieure à '.DEFAULT_IMAGE_SIZE_LIMIT_MB.' Mo';
$lang['ERROR_INVALID_DIMENSION']                                = "La dimension de l'image doit être supérieure à 200 x 200 pixels.";
$lang['ERROR_PROBLEM_IN_UPLOAD']                                = "Problème lors du chargement de l'image!";
$lang['PHOTO_UPLOAD_SUCCESS']                                   = 'Votre photo de profil a été téléchargée avec succès.';

$lang['ERROR_CV_INVALID_FILE']                                  = 'Le fichier doit être pdf, jpg, jpeg ou png, moins de '.DEFAULT_MAXIMUM_CV_SIZE_LIMIT_PHP.' Mo';
$lang['ERROR_CV_INVALID_EXTENSION']                             = 'Type de fichier ou extension non valide, le fichier doit être pdf, jpg, jpeg ou png';
$lang['ERROR_COVER_INVALID_EXTENSION']                          = "Le type de fichier ou l'extension non valide, le fichier lettre d'accompagnement doit être jpg, jpeg ou png";
$lang['ERROR_CV_INVALID_SIZE']                                  = 'La taille du fichier doit être inférieure à '.DEFAULT_MAXIMUM_CV_SIZE_LIMIT_PHP.' Mo';
$lang['ERROR_CV_PROBLEM_IN_UPLOAD']                             = "Problème lors du téléchargement du fichier d'application!";
$lang['CV_UPLOAD_SUCCESS']                                      = "Votre fichier d'application a été téléchargé avec succès.";
$lang['UPLOAD_CV_UPLOAD_SUCCESS']                               = 'Votre CV mis à jour avec succès.';
$lang['UPLOAD_COVER_UPLOAD_SUCCESS']                            = 'Votre lettre d\'accompagnement a été mise à jour avec succès.';
/** END OF FILE VALIDATION CONSTANT */



/** Professtional Registration **/
$lang['PROF_REGISTER_TITLE_RECRUIT']                            = "RECRUTEZ";
$lang['PROF_REGISTER_TITLE_QUICKLY']                            = "RAPIDEMENT";
$lang['PROF_REGISTER_TITLE_FREE']                               = "GRATUITEMENT !";
$lang['PROF_REGISTER_ACCESS_TO']                                = "accès à notre";
$lang['PROF_REGISTER_CV_DATABASE']                              = "cvthéque";
$lang['PROF_REGISTER_REGISTER_YOUR']                            = "ENREGISTRER VOTRE";
$lang['PROF_REGISTER_IN_OUR']                                   = "DANS NOTRE";
$lang['PROF_REGISTER_BROADCAST_LIST']                           = "LISTE DE DIFFUSION";
$lang['PROF_REGISTER_GOTO']                                     = "Passez en";
$lang['PROF_REGISTER_PREMIUM_MODE']                             = "mode premium";
$lang['PROF_REGISTER_SOCIAL_REASON_OF_COMPANY']                 = "Raison sociale de l'entreprise";
$lang['PROF_REGISTER_SOCIAL_THEME']                             = "Thématique / secteur d'activité";
$lang['PROF_REGISTER_NO_ACTIVITY_AREA_SET']                     = "Zone d'activité non disponible";
$lang['PROF_REGISTER_SUBSCRIBE']                                = "INSCRIVEZ-VOUS";
$lang['PROF_REGISTER_FREE']                                     = "GRATUITEMENT";
$lang['PROF_REGISTER_SUBSCRIBE']                                = "INSCRIVEZ-VOUS";
$lang['PROF_REGISTER_FREE']                                     = "GRATUITEMENT";

/** END OF Professtional Registration **/

/** Professtional my company profile */
$lang['PROF_MY_PROFILE_COMPANY_PROFILE']                        = "profil ENTREPRISE";
$lang['PROF_MY_PROFILE_PREMIUM_MEMBER']                         = 'DEVENEZ UN MEMBRE PREMIUM POUR %1$s€';
$lang['PROF_MY_PROFILE_LOGIN_EMAIL']                            = "Email de connexion";
$lang['PROF_MY_PROFILE_CIVIL_STATUS']                           = "Etat civil";
$lang['PROF_MY_PROFILE_COMPANY_ADDRESS']                        = "Adresse de l'entreprise";
$lang['PROF_MY_PROFILE_COMPANY_IMPORT_LOGO_TEXT']               = "Vous pourrez importer votre logo si vous êtes membre premium";
$lang['PROF_MY_PROFILE_ACTIVITY_AREA']                          = "Thématique / secteur d'activité";
$lang['PROF_MY_PROFILE_COMPANY_SEARCH_ADDRESS']                 = "Adresse de recherche";
$lang['PROF_MY_PROFILE_PREMIUM']                                = "Votre plan premium est activé";
$lang['PROF_MY_PROFILE_PREMIUM_PLAN_EXPIRE']                    = "Votre plan premium est expiré, devenez membre premium pour 50 €";
$lang['PROF_MY_PROFILE_PREMIUM_PLAN_EXPIRE_DATE']               = "Votre plan expirera le";
/** END OF Professtional my company profile */

/** CHANGE PROF PASSWORD */

$lang['CHANGE_PASSWORD_HEADING']                                = "Changer mon mot de passe";
$lang['CHANGE_PASSWORD_CURRENT_PASSWORD']                       = "Mot de passe actuel";
$lang['CHANGE_PASSWORD_NEW_PASSWORD']                           = "Nouveau mot de passe";
$lang['CHANGE_PASSWORD_CONFIRM_PASSWORD']                       = "Confirmer le nouveau mot de passe";

/** END OF CHANGE PROF PASSWORD */

/** Professional resume database */
$lang['PROF_RESUME_DATABASE_HEADING_CV']                        = "CV";
$lang['PROF_RESUME_DATABASE_HEADING']                           = "Thèque";
$lang['PROF_RESUME_DATABASE_SEARCH']                            = "Utilisez ce moteur de recherche pour trouver le candidat qui répond à vos atttentes";
$lang['PROF_RESUME_DATABASE_SEARCH_JOB']                        = "Métier recherché";
$lang['PROF_RESUME_DATABASE_SEARCH_AREA']                       = "Secteur d'activité";
$lang['PROF_RESUME_DATABASE_SEARCH_LOCATION']                   = "Lieu recherché";
$lang['PROF_RESUME_DATABASE_SEARCH_MATCH']                      = "résultats pour vos critères de recherche";
$lang['PROF_RESUME_DATABASE_AVAILABLE']                         = "Disponibilité immédiate - Mis à jours le";
$lang['PROF_RESUME_DATABASE_NO_JOB_TYPE_SET']                   = "Type d'emploi non disponible";
$lang['PROF_RESUME_DATABASE_NO_ACTIVITY_AREA_SET']              = "Zone d'activité non disponible";
$lang['PROF_RESUME_DATABASE_NO_WORK_PLACE_SET']                 = "Lieu de travail non disponible";
$lang['PROF_RESUME_DATABASE_WORK_EXPERIENCE']                   = "ANS D'EXPéRIENCES";
$lang['PROF_RESUME_DATABASE_ADD_TO_FAVORITE_SUCCESS']           = "Candidat ajouté avec succès dans votre liste préférée.";
$lang['PROF_RESUME_DATABASE_ADD_TO_FAVORITE_FAILED']            = "Problème tout en ajoutant un candidat dans votre liste préférée.";
$lang['PROF_RESUME_DATABASE_REMOVE_FROM_FAVORITE_SUCCESS']      = "Candidate successfully removed from your favourite list.";
$lang['PROF_RESUME_DATABASE_REMOVE_FROM_FAVORITE_FAILED']       = "Problem while removing candidate from your favourite list.";
/** END OF Professional resume database */


/** Professional resume details */
$lang['PROF_RESUME_DETAILS_HEADING']                            = "thÉque";
$lang['PROF_RESUME_DETAILS_CONTACT_TO_CANDIDATE']               = "Contacter ce Candidat";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_TO_CANDIDATE']             = "Envoyez un Email à";
$lang['PROF_RESUME_DETAILS_CANDIDATE_EMPTY']                    = "Les détails du CV ne sont pas trouvés ...";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_SUBJECT']                  = "Nouveau recruteur";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_TEXT_1']                   = "a passé en revue votre profil et vous a envoyé un message ci-dessous:";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_RECRUITER_EMAIL']          = "Courriel du recruteur:";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_RECRUITER_PHONE']          = "Téléphone recruteur:";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_RECRUITER_ADDRESS']        = "Adresse du recruteur:";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_SUCCESS']                  = "Votre email a été envoyé avec succès à";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_FAILED']                   = "Problème lors de l'envoi de courrier électronique à";
/** END OF Professional resume details*/

/** Professional favourite resume  */
$lang['PROF_RESUME_FAVOURITE_HEADING']                          = "MES PROFILS";
$lang['PROF_RESUME_FAVOURITE_HEADING_2']                        = "FAVORIS";
$lang['PROF_RESUME_FAVOURITE_SEARCH_MATCH']                     = "profils dans votre liste de favoris";
$lang['PROF_RESUME_FAVOURITE_WORK_EXPERIENCE']                  = "ANS D’EXPéRIENCES";
$lang['PROF_RESUME_FAVOURITE_AVAILABLE']                        = "Disponibilité immédiate - Mis à jours le";
$lang['PROF_RESUME_FAVOURITE_CANDIDATE_EMPTY']                  = "Les détails du CV ne sont pas trouvés ...";
$lang['PROF_RESUME_FAVOURITE_REMOVE_FROM_FAVORITE_SUCCESS']     = "Candidat retiré avec succès de votre liste préférée.";
$lang['PROF_RESUME_FAVOURITE_REMOVE_FROM_FAVORITE_FAILED']      = "Problème tout en supprimant le candidat de votre liste préférée.";
$lang['PROF_RESUME_FAVOURITE']                                  = "Profils dans votre liste de favoris";
/** END OF Professional favourite resume */

/** Professional receive priority resume  */
$lang['PROF_PRIORITY_RESUME_HEADING']                           = "RECEVOIR LES CV";
$lang['PROF_PRIORITY_RESUME_HEADING_2']                         = "EN PRIORITÉ";
$lang['PROF_PRIORITY_RESUME_DESCRIPTION']                       = "Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression. 
                                                                  Le Lorem Ipsum est le faux texte standard de l'imprimerie depuis les années 1500, quand un peintre anonyme assembla 
                                                                  ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte. Il n'a pas fait que survivre cinq siècles, 
                                                                  mais s'est aussi adapté à la bureautique informatique, sans que son contenu n'en soit modifié. Il a été popularisé dans les 
                                                                  années 1960 grâce à la vente de feuilles Letraset contenant des passages du Lorem Ipsum, et, 
                                                                  plus récemment, par son inclusion dans des applications de mise en page de texte, comme Aldus PageMaker.";

/** END OF Professional receive priority resume */

/** Professional search receive priority resume  */
$lang['PROF_SEARCH_PRIORITY_RESUME_HEADING']                    = "RECEVOIR LES CV";
$lang['PROF_SEARCH_PRIORITY_RESUME_HEADING_2']                  = "EN PRIORITÉ";
$lang['PROF_SEARCH_PRIORITY_RESUME_SEND_REQUEST']               = "Envoyez ma demande";
$lang['PROF_SEARCH_PRIORITY_RESUME_INFORMATION']                = "Informations vues par les candidats";
$lang['PROF_SEARCH_PRIORITY_RESUME_SOCIAL_REASON_OF_COMPANY']   = "Raison sociale de l'entreprise";
$lang['PROF_SEARCH_PRIORITY_RESUME_ACTIVITY_AREA']              = "Thématique / secteur d'activité";
$lang['PROF_SEARCH_PRIORITY_RESUME_MODIFY']                     = "Modifier les informations";
$lang['PROF_SEARCH_PRIORITY_RESUME_NOT_SUBSCRIBED']             = "Demande non autorisée, vous devez souscrire un plan pour continuer";  
$lang['PROF_SEARCH_PRIORITY_SUCCESS']                           = "Votre priorité de recherche de CV sauvegardée avec succès.";  
$lang['PROF_SEARCH_PRIORITY_FAILED']                            = "Problème lors de la modification de votre priorité de recherche de curriculum vitae.";
/** END OF Professional receive priority resume */


/* Nationality */
$lang['NATIONALITY_1']                                          = "Indien";
$lang['NATIONALITY_2']                                          = "Française";

/* Country */
$lang['COUNTRY_1']                                              = "France";
$lang['COUNTRY_2']                                              = "Inde";

/* Verify password and update new password */
$lang['VERIFY_PASSWORD_CHANGE_SUCCESS']                         = "Mot de passe changé avec succès, maintenant vous pouvez vous connecter en utilisant un nouveau mot de passe";
$lang['VERIFY_PASSWORD_CHANGE_FAILED']                          = "Problème de modification de votre mot de passe, Réessayez plus tard.";
$lang['VERIFY_PASSWORD_HEAD_1']                                 = "Réinitialiser";
$lang['VERIFY_PASSWORD_HEAD_2']                                 = "mon mot de passe";
$lang['VERIFY_PASSWORD_SUBMIT']                                 = "Changer le mot de passe";


/* Edit candidate details model pop-up*/
$lang['UPDATE_EXPERIENCE_TITLE']                                = "Modifier l'expérience";
$lang['UPDATE_SKILL_TITLE']                                     = "Modifier la compétence";
$lang['UPDATE_TRAINING_TITLE']                                  = "Modifier la formation";
$lang['UPDATE_HOBBY_TITLE']                                     = "Modifier le passe-temps";

$lang['UPDATE_EXPERIENCE_SUCCESS']                              = "Votre expérience a été mise à jour avec succès.";
$lang['UPDATE_EXPERIENCE_FAILED']                               = "Problème tout en actualisant les détails de votre expérience.";
$lang['UPDATE_SKILL_SUCCESS']                                   = "Vos compétences ont été mises à jour avec succès.";
$lang['UPDATE_SKILL_FAILED']                                    = "Problème lors de la mise à jour de vos détails de compétences.";
$lang['UPDATE_TRAINING_SUCCESS']                                = "Votre formation a été mise à jour avec succès.";
$lang['UPDATE_TRAINING_FAILED']                                 = "Problème lors de la mise à jour des détails de votre formation.";
$lang['UPDATE_HOBBIY_SUCCESS']                                  = "Votre hobby a été mis à jour avec succès.";
$lang['UPDATE_HOBBY_FAILED']                                    = "Problème lors de la mise à jour de vos détails de loisir.";


/* Delete candidate details model pop-up*/
$lang['DELETE_ARE_YOU_SURE_COMMON']                             = "Êtes-vous sûr de vouloir supprimer cet enregistrement?";
$lang['DELETE_MESSAGE_TEXT']                                    = "Ces données seront supprimées en permanence et ne seront pas réversibles.";
$lang['DELETE_ARE_YOU_SURE_EXPERIENCE']                         = "Êtes-vous sûr de vouloir supprimer cette expérience?";
$lang['DELETE_EXPERIENCE_SUCCESS']                              = "Votre expérience a été supprimée avec succès.";
$lang['DELETE_EXPERIENCE_FAILED']                               = "Problème tout en supprimant les détails de votre expérience.";
$lang['DELETE_ARE_YOU_SURE_SKILL']                              = "Êtes-vous sûr de vouloir supprimer cette compétence?";
$lang['DELETE_SKILL_SUCCESS']                                   = "Votre compétence a été supprimée avec succès.";
$lang['DELETE_SKILL_FAILED']                                    = "Problème lors de la suppression des détails de vos compétences.";
$lang['DELETE_ARE_YOU_SURE_TRAINING']                           = "Êtes-vous sûr de vouloir supprimer cette formation?";
$lang['DELETE_TRAINING_SUCCESS']                                = "Votre formation a été supprimée avec succès.";
$lang['DELETE_TRAINING_FAILED']                                 = "Problème tout en supprimant les détails de votre formation.";
$lang['DELETE_ARE_YOU_SURE_HOBBY']                              = "Êtes-vous sûr de vouloir supprimer ce passe-temps?";
$lang['DELETE_HOBBY_SUCCESS']                                   = "Votre passe-temps a été supprimé avec succès.";
$lang['DELETE_HOBBY_FAILED']                                    = "Problème lors de la suppression de vos détails de loisir.";
$lang['DELETE_YES_DELETE_IT']                                   = "Oui, supprimez-le!";
$lang['DELETE_NO_CANCEL_IT']                                    = "Non, annulez";

/* News */
$lang['NEWS_DETAILS']                                           = "Détails des nouvelles";
$lang['TITLE']                                                  = "Titre";
$lang['DESCRIPTION']                                            = "La description";
$lang['HOME_NEWSLETTER_ERROR_REQUIRED']                         = "Entrez l'adresse e-mail.";
$lang['HOME_NEWSLETTER_ERROR_EMAIL']                            = "Entrez une adresse email valide.";

/** payment */
$lang['PAYMENT_SUCCESS']                                        = "Votre paiement effectué avec succès";
$lang['PAYMENT_FAILED']                                         = "Votre transaction est échouée, contactez notre administrateur";
$lang['PAYMENT_STORE_FAILED']                                   = "Problème tout en stockant les détails de votre transaction";
$lang['PAYMENT_STORE_FAILED_DETAILS']                           = "Votre transaction est échouée, Problème tout en stockant les détails de votre transaction";

/** orders */
$lang['MY_ORDERS']                                              = "commandes";
$lang['MY_ORDERS_NUMBER_OF_SHIPMENTS']                          = "Nombre d'expéditions";
$lang['MY_ORDERS_PAID_AMOUNT']                                  = "Montant payé";
$lang['MY_ORDERS_STATUS']                                       = "Statut de la commande";
$lang['MY_ORDERS_CREATED_DATE']                                 = "Candidature le";
$lang['MY_ORDERS_PAYMENT_BY']                                   = "Paiement par";
$lang['MY_ORDERS_PAYMENT_BY_PAYPAL']                            = "Pay Pal";
$lang['MY_ORDERS_PAYMENT_BY_SYSTEMPAY']                         = "SystemPay";
$lang['MY_ORDERS_EMPTY']                                        = "Les commandes ne sont pas disponibles.";

$lang['CONFIRMATION_EMAIL']                                     = "Le courrier électronique et le courrier électronique doivent être identiques";
$lang['LETTER_ONLY']                                            = "Veuillez saisir uniquement des lettres alphabétiques.";
