<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/* Header */
$lang['HEADER_MENU_HELP']                               = "Tips / Help";
$lang['HEADER_MENU_SIGNUP']                             = "Create an account";
$lang['HEADER_MENU_CONTACTUP']                          = "Login";
$lang['HEADER_MENU_MYBASKET']                           = "My Orders";
$lang['HEADER_MENU_PROSPACE']                           = "Professional space";
$lang['HEADER_MENU_REGISTER_RECRUITER']                 = "REGISTER RECRUITER";


/* Candidate Header */
$lang['CANDIDATE_HEADER_USER_MENU']                     = 'Hello ';
$lang['CANDIDATE_HEADER_USER_SUB_MENU_PROFILE']         = 'My profile';
$lang['CANDIDATE_HEADER_USER_SUB_MENU_PERSONAL_INFO']   = 'My personal information';
$lang['CANDIDATE_HEADER_USER_SUB_MENU_MY_DOCUMENTS']    = 'My Documents';
$lang['CANDIDATE_HEADER_USER_SUB_MENU_MY_APPLICATION']  = 'My spontaneous applications';
$lang['CANDIDATE_HEADER_USER_SUB_MENU_DISCONNECT']      = 'Disconnection';


/* Professtional Header */
$lang['PROF_HEADER_USER_MENU']                          = 'Hello ';
$lang['PROF_HEADER_USER_SUB_MENU_PROFILE']              = 'My company profile';
$lang['PROF_HEADER_USER_SUB_MENU_ACCESS_CV']            = 'Access CV to database';
$lang['PROF_HEADER_USER_SUB_MENU_MY_FAVORITE_PROFILES'] = 'MY FAVORITE PROFILES';
$lang['PROF_HEADER_USER_SUB_MENU_RECEIVING_RESULTS']    = 'Receive priority CV';
$lang['PROF_HEADER_USER_SUB_MENU_DISCONNECT']           = 'Disconnection';


/* Footer */
$lang['FOOTER_SEND_APPLICATION']                        = "SEND MY APPLICATION";
$lang['FOOTRE_MY_CV']                                   = "MY CV";
$lang['FOOTER_MOTIVATION_LETTER']                       = "MY LETTER OF MOTIVATION";
$lang['FOOTER_COMPANY_AREA']                            = "COMPANY AREA";
$lang['FOOTER_SHOOPING_CART']                           = "Shopping Cart";
$lang['FOOTRE_SUBSCRIBE']                               = "Subscribe";
$lang['FOOTER_CONTACTUS']                               = "Contact Us |";
$lang['FOOTER_TIPS_WRITING']                            = "Tips / Writing";
$lang['FOOTER_CONTACT_TITLE']                           = "CONTACT US";
$lang['FOOTER_SOCIAL_SHARING']                          = "FOLLOW US ON";
$lang['FOOTER_COYRIGHT_TEXT']                           = 'Rapid job © %1$s - All rights reserved - Legal notice - CGV';

/* Common */
$lang['EDIT_BUTTON']                                    = "Edit";
$lang['MESSAGE_MISSING']                                = "Message is missing.";
$lang['COMMON_EDIT_INFO_BUTTON']                        = "Edit information";
$lang['COMMON_REMOVE_BUTTON']                           = "Remove";
$lang['COMMON_PERIOD']                                  = "Period";
$lang['COMMON_TO']                                      = "To";
$lang['COMMON_TITLE']                                   = "Title";
$lang['COMMON_ADD_BTN']                                 = "Add";
$lang['COMMON_CANCEL_BTN']                              = "Cancel";
$lang['COMMON_ADD_MORE']                                = "Add more";
$lang['COMMON_DESCRIPTION']                             = "Description";
$lang['COMMON_INVALID_PARAMS']                          = "Invalid parameters";
$lang['COMMON_EMAIL_ALREADY_REGISTERED']                = "This email address already registered with us.";
$lang['COMMON_FREE']                                    = "Free";
$lang['COMMON_FREES']                                   = "Free";
$lang['COMMON_RATE']                                    = "Rate";
$lang['COMMON_VALIDATE']                                = "Validate";
$lang['COMMON_TO_SEND']                                 = "Send";
$lang['COMMON_SENT']                                    = "Sent";
$lang['COMMON_PENDING']                                 = "Pending";
$lang['COMMON_PROBLEM_IN_ACTION']                       = "Problem in performing action.";
$lang['COMMON_DURATION']                                = "Duration";
$lang['COMMON_SAVE']                                    = "Save";
$lang['COMMON_SELECT_VALID_START_DATE']                 = "Select valid start date.";
$lang['COMMON_SELECT_VALID_END_DATE']                   = "Select valid end date.";
$lang['COMMON_SELECT_INVALID']                          = "Invalid selection";
$lang['COMMON_CHOOSE_FILE']                             = "Choose a file ..";
$lang['COMMON_APPLICATION_TITLE']                       = "application";
$lang['COMMON_REQUIRED_FIELD']                          = "Required fields";
$lang['COMMON_THIS_REQUIRED_FIELD']                     = "This field is required";
$lang['COMMON_BUSINESS_NAME']                           = "Business";
$lang['COMMON_SHOW']                                    = "Show";
$lang['COMMON_EDIT']                                    = "Edit";
$lang['COMMON_CONFIRM']                                 = "Confirmation";
$lang['COMMON_GO_TO_HOME']                              = "Go to homepage";
$lang['COMMON_404']                                     = "This page doesn't exist";
$lang['COMMON_AND']                                     = "AND";
$lang['COMMON_CIVILITY']                                = "Choose your civility";
$lang['COMMON_CONFIRM_PASSWORD']                        = "Confirmation password";
$lang['COMMON_INTERNAL_ERROR']                          = "Something went wrong, Try again later...";
$lang['COMMON_MY']                                      = "My";
$lang['COMMON_PHONE']                                   = "Phone";
$lang['COMMON_ADDRESS']                                 = "Address";
$lang['COMMON_CITY']                                    = "City";
$lang['COMMON_POSTAL_CODE']                             = "ZIP code";
$lang['COMMON_COUNTRY']                                 = "Country";
$lang['COMMON_SAVE_CHANGES']                            = "Save Changes";
$lang['COMMON_RECORD']                                  = "Record";
$lang['COMMON_MADAM']                                   = "Female";
$lang['COMMON_SIR']                                     = "Male";
$lang['COMMON_SEARCH']                                  = "Search";
$lang['COMMON_MORE']                                    = "See more";
$lang['COMMON_ADD_TO_FAVOURITE']                        = "Add CV to Favorite";
$lang['COMMON_DOWNLOAD_CV']                             = "Download Resume";
$lang['COMMON_SEARCH_BY_JOB']                           = "Search by job";
$lang['COMMON_SEARCH_BY_ACTIVITY_AREA']                 = "Search by activity area";
$lang['COMMON_SEARCH_BY_LOCATION']                      = "Search by location";
$lang['COMMON_SELECT_ALL']                              = "Select All";
$lang['COMMON_DESELECT_ALL']                            = "Deselect All";
$lang['COMMON_ITEMS_SELECTED']                          = "items selected";
$lang['COMMON_USER_CANDIDATE']                          = "Candidate";
$lang['COMMON_USER_RECRUITER']                          = "Recruiter";
$lang['COMMON_FAVOURITE_REMOVE']                        = "Remove Favourite";
$lang['COMMON_CV']                                      = "CV";
$lang['COMMON_OR']                                      = "OR";
$lang['COMMON_TITLE_HOBBIE']                            = "Hobby";
$lang['COMMON_INVALID_EMAIL']                           = "Enter valid email address.";
$lang['COMMON_NOTICE']                                  = "Notice";
$lang['COMMON_SUBSCRIBE']                               = "Subscribe";
$lang['COMMON_PRICES']                                  = "Prices";
$lang['COMMON_SUBSCRIBE_SERVICE']                       = "Subscribe service";
$lang['COMMON_SEARCH_JOB']                              = "Searched for job";
$lang['COMMON_SEARCH_AREA']                             = "Activity area";
$lang['COMMON_SEARCH_LOCATION']                         = "Location search";
$lang['COMMON_SEARCH_EDUCATION_LEVEL']                  = "Level of education";
$lang['COMMON_SEARCH_WORK_PLACES_LABEL']                = "Workplaces";
$lang['COMMON_SEARCH_WORK_PLACES_LIMIT']                = "(you can select up to 5 places)";
$lang['COMMON_SEARCH_NO_ACTIVITY_AREA_SET']             = "Activity area not available";
$lang['COMMON_SEARCH_CHOOSE_ACTIVITY_AREA']             = "Activity area";
$lang['COMMON_JOB_NOT_DEFINED']                         = "Job details not available";
$lang['COMMON_EXPERIENCE_NOT_DEFINED']                  = "Experience details not available";
$lang['COMMON_TRAINING_NOT_DEFINED']                    = "Training details not available";
$lang['COMMON_SKILL_NOT_DEFINED']                       = "Skill details not available";
$lang['COMMON_HOBBIES_NOT_DEFINED']                     = "Hobbies details not available";
$lang['COMMON_USER_DETAILS_NOT_DEFINED']                = "User details not available";
$lang['COMMON_ERROR_CHANGE_LANGUAGE']                   = "Error while change language";
$lang['COMMON_OTHER']                                   = "Other";
$lang['COMMON_GENERATE_CV_MESSAGE']                     = "Before use rapid job cv you need to generate rapid job cv from your profile.";
$lang['COMMON_EDIT_CV']                                 = "Edit CV";
$lang['COMMON_EDIT_MY_CV']                              = "Edit my CV";
$lang['COMMON_CV_FILE_MISSING']                         = "CV file missing";
$lang['COMMON_JOB_WANTED']                              = "Jobs wanted";
$lang['COMMON_ACTIVE']                                  = "Active";
$lang['COMMON_INACTIVE']                                = "In-active";
$lang['COMMON_CANCELED']                                = "Canceled";
$lang['COMMON_MES']                                     = "My";
$lang['COMMON_SELECT_NATIONALITY']                      = "Select nationality";
$lang['COMMON_SELECT_COUNTRY']                          = "Select country";
$lang['COMMON_NATIONALITY_EMPTY']                       = "No nationality available";
$lang['COMMON_COUNTRY_EMPTY']                           = "No country available";
$lang['COMMON_MONTH']                                   = "Month";
$lang['COMMON_PLAN_EXPIRE']                             = "Your plan is expired";
$lang['COMMON_ADD_OTHER_WORKJOB']                       = "Add your job title";
$lang['COMMON_ADD_OTHER_ACTIVITY_AREA']                 = "Add your activity area title";
$lang['COMMON_ADD_OTHER_WORKPLACE']                     = "Add your workplace title";
$lang['COMMON_ADD_OTHER_EDUCATION']                     = "Add your education title";

/* login */
$lang['LOGIN_INVALID_EMAIL_OR_PASSWORD']                = "Invalid email or password";
$lang['LOGIN_ACCOUNT_BLOCKED']                          = "Your account is blocked by admin";

/* register */
$lang['REGISTER_SUCCESS']                               = "Your registration successfully done.";
$lang['REGISTER_FAILURE']                               = "Error while registering your details";
$lang['REGISTER_EXPIRE_CODE']                           = "Your Invitation code expired.";
$lang['REGISTER_PASSWORD_VALIDATION_MESSAGE']           = "Enter a combination of at least 6 characters (numbers and lowercase and uppercase letters with a special character required)";

/* NEWS LATTER */
$lang['NEWSLATTER_SUCCESS']                             = "You are successfully registered newsletter";
$lang['NEWSLATTER_SUBJECT']                             = "Newsletter update";
$lang['NEWSLATTER_ALREADY_REGISTERED']                  = "You have already registered newsletter";
$lang['HOME_COVERPAGE_TXT_1']                           = "the best ";
$lang['HOME_COVERPAGE_TXT_2']                           = "solution ";
$lang['HOME_COVERPAGE_TXT_3']                           = "for ";
$lang['HOME_COVERPAGE_TXT_4']                           = "all";
$lang['HOME_COVERPAGE_SEARCH_PLACEHOLDER']              = "*** Region, Department, City, ...";
$lang['HOME_COVERPAGE_SEARCH_BUTTON']                   = "Find Companies";
$lang['HOME_STEPS_TITLE_1']                             = "Target";
$lang['HOME_STEPS_TEXT_1']                              = "Desired companies and recruitment agencies";
$lang['HOME_STEPS_TITLE_2']                             = "Submit";
$lang['HOME_STEPS_TEXT_2']                              = "Your application to thousands of recruiters";
$lang['HOME_STEPS_TITLE_3']                             = "Receive";
$lang['HOME_STEPS_TEXT_3']                              = "Positive answers and hiring interviews";
$lang['HOME_STEPS_BUTTON']                              = "Send my application";
$lang['HOME_BANNER_BLOCK_1_TITLE']                      = "Send your application";
$lang['HOME_BANNER_BLOCK_1_SUB_TITLE']                  = "Up to 125 free addresses";
$lang['HOME_BANNER_BLOCK_1_LIST1']                      = "Create your resume";
$lang['HOME_BANNER_BLOCK_1_LIST2']                      = "Write a cover letter";
$lang['HOME_BANNER_BLOCK_1_LIST3']                      = "Send it all!";
$lang['HOME_BANNER_BLOCK_2_TITLE']                      = "Create your resume";
$lang['HOME_BANNER_BLOCK_2_SUB_TITLE']                  = "More than 20 different models";
$lang['HOME_BANNER_BLOCK_2_LIST1']                      = "Create your resume online";
$lang['HOME_BANNER_BLOCK_2_LIST2']                      = "Customize it at will";
$lang['HOME_BANNER_BLOCK_2_LIST3']                      = "Send it directly";
$lang['HOME_SLIDER_CAPTION_TEXT']                       = "I received a reply from an employer within 48 hours. Rather reassuring and frankly serious thank you.";
$lang['HOME_SLIDER_QUOTE']                              = "THEY HAVE FOUND A JOB GRACE with spontaneous candidature";
$lang['HOME_SLIDER_TESTIMONIAL']                        = "Paul, 25, freshly graduated from his engineering school, is looking for his first job. Paul has always dreamed of working in aeronautics ... After months of unsuccessful searches, Paul falls on Rapid Job. He selects companies and recruitment firms that interest him, sends his CV and cover letter, receives positive answers, passes interviews and hires the job of his dreams! Thanks to Rapid Job, Paul found his ideal job. So why not you ?";
$lang['HOME_SLIDER_TESTIMONIAL_BUTTON']                 = "Create my account";
$lang['HOME_NEWS_TITLE_1']                              = "THE NEWS ";
$lang['HOME_NEWS_TITLE_2']                              = "OF THE EMPLOYMENT ";
$lang['HOME_NEWS_TITLE_3']                              = "& ";
$lang['HOME_NEWS_TITLE_4']                              = "RECRUITMENT ";
$lang['HOME_NEWS_IMAGE_HOVER_TEXT']                     = "Read more";
$lang['HOME_NEWS_IMAGE_TITLE_1']                        = "How to find a job today?";
$lang['HOME_NEWS_IMAGE_TEXT_1']                         = "5 key points to finding a job using internet and social networks";
$lang['HOME_NEWS_IMAGE_TITLE_2']                        = "How to find a job today?";
$lang['HOME_NEWS_IMAGE_TEXT_2']                         = "5 key points to finding a job using internet and social networks";
$lang['HOME_NEWS_IMAGE_TITLE_3']                        = "How can we rebound against unemployment?";
$lang['HOME_NEWS_IMAGE_TEXT_3']                         = "Unemployment is a real challenge. You have to face conflicting feelings, the joy of finding time for yourself but also the considerable stress of not finding a job.";
$lang['HOME_NEWSLETTER_TITLE']                          = "DO NOT RETURN ANY EVENT, TO SUBSCRIBE TO OUR NEWSLETTER!";
$lang['HOME_NEWSLETTER_INPUT_PLACEHOLDER']              = "Your email";
$lang['HOME_NEWSLETTER_BUTTON']                         = "I'm registering";
$lang['HOME_NO_NEWS_AVAILABLE']                         = "No news available";


/* Sign up Form */
$lang['SIGNUP_FORM_TITLE']                              = "Register for free";
$lang['SIGNUP_FORM_FIRST_NAME']                         = "First name";
$lang['SIGNUP_FORM_LAST_NAME']                          = "Last name";
$lang['SIGNUP_FORM_EMAIL']                              = "Email";
$lang['SIGNUP_FORM_CONFIRM_EMAIL']                      = "Confirmation email";
$lang['SIGNUP_FORM_PASSWORD']                           = "Password";
$lang['SIGNUP_FORM_BUTTON']                             = "Registration";
$lang['SIGNUP_FORM_VIA_SOCIAL_TITLE']                   = "Or subscribe via";


/* Sign in Form */
$lang['SIGNIN_FORM_TITLE']                              = "Connection";
$lang['SIGNIN_FORM_SOCIAL_TITLE']                       = "Connect with";
$lang['SIGNIN_FORM_EMAIL']                              = "Email";
$lang['SIGNIN_FORM_PASSWORD']                           = "Password";
$lang['SIGNIN_FORM_BUTTON']                             = "Connection";
$lang['SIGNIN_FORM_ACCOUNT_TYPE']                       = "Login account type";

$lang['SIGNIN_FORM_USER_CANDIDATE']                     = "Login into candidate account";
$lang['SIGNIN_FORM_USER_RECUITER']                      = "Login into recruiter account";
$lang['SIGNIN_FORM_USER_FORGOT_PASSWORD']               = "Forgot your account's password?";

/* Build CV page */
$lang['BUILDCV_PAGE_TITLE']                             = 'Create your';
$lang['BUILDCV_PAGE_TITLE_RED']                         = 'CV';
$lang['BUILDCV_PAGE_TAP_PHOTO_TEXT']                    = 'Upload your photo';
$lang['BUILDCV_PAGE_SELECT_JOB']                        = 'Your job';
$lang['BUILDCV_PAGE_SELECT_ACTIVITY_AREA']              = "Activity area";
$lang['BUILDCV_PAGE_SELECT_WORK_PLACES']                = "Workplaces (you can select up to 5 locations)";
$lang['BUILDCV_PAGE_SELECT_WORK_PLACES_LABEL']          = "Workplaces";
$lang['BUILDCV_PAGE_SELECT_WORK_PLACES_OPT_1']          = "Toulouse";
$lang['BUILDCV_PAGE_SELECT_WORK_PLACES_OPT_2']          = "Marseille";
$lang['BUILDCV_PAGE_SELECT_WORK_PLACES_OPT_3']          = "Bordeaux";
$lang['BUILDCV_PAGE_CONTRACT_TITLE']                    = "Type of Contract";
$lang['BUILDCV_PAGE_CONTRACT_TYPE_1']                   = "CDI";
$lang['BUILDCV_PAGE_CONTRACT_TYPE_2']                   = "CDD";
$lang['BUILDCV_PAGE_CONTRACT_TYPE_3']                   = "Stage";
$lang['BUILDCV_PAGE_CONTRACT_TYPE_4']                   = "Interim Mission";
$lang['BUILDCV_PAGE_CONTRACT_TYPE_5']                   = "Freelance";
$lang['BUILDCV_PAGE_EDUCATION_LEVEL']                   = "Level of education";
$lang['BUILDCV_PAGE_MY_LANGUAGES']                      = "My languages";
$lang['BUILDCV_PAGE_ADD_LANGUAGES']                     = "Add a language";
$lang['BUILDCV_PAGE_MY_SKILLS']                         = "My skills";
$lang['BUILDCV_PAGE_ADD_SKILLS']                        = "Add my skills";
$lang['BUILDCV_PAGE_VIEW_PROFILE_BTN']                  = "See my profile";
$lang['BUILDCV_PAGE_SEND_APPLICATION']                  = "Send my application";

/** added by Nitinkumar vaghani **/
$lang['BUILDCV_PAGE_ENTER_LANGUAGES']                   = "Enter your language";
$lang['BUILDCV_PAGE_ENTER_SKILL']                       = "Enter your skill";
$lang['BUILDCV_PAGE_NO_JOB_TYPE_SET']                   = "Job type not available";
$lang['BUILDCV_PAGE_NO_ACTIVITY_AREA_SET']              = "Activity area not available";
$lang['BUILDCV_PAGE_NO_WORK_PLACE_SET']                 = "Workplace not available";
$lang['BUILDCV_PAGE_NO_EDU_LEVEL_SET']                  = "Education level not available";
$lang['BUILDCV_PAGE_CHOOSE_JOB']                        = "Choose your job";
$lang['BUILDCV_PAGE_CHOOSE_ACTIVITY_AREA']              = "Choose activity area";
$lang['BUILDCV_PAGE_CHOOSE_WORKPLACE']                  = "Choose the workplace";
$lang['BUILDCV_PAGE_CHOOSE_EDUCATION_LEVEL']            = "Choose level of education";
$lang['NOTHING_SELECTED']                               = "Nothing selected";
$lang['NO_RESULT_MATCHED']                              = "No results matched";

/* Edit Profile*/
$lang['EDITPROFILE_PAGE_TITLE']                         = "my personal";
$lang['EDITPROFILE_PAGE_TITLE_RED']                     = "information";
$lang['EDITPROFILE_PAGE_EMAIL_INFO']                    = "Login Email";
$lang['EDITPROFILE_PAGE_EMAIL_LABEL']                   = "Email";
$lang['EDITPROFILE_PAGE_CIVIL_INFO']                    = "Civil status";
$lang['EDITPROFILE_PAGE_CIVIL_CIVILITY']                = "Gender";
$lang['EDITPROFILE_PAGE_CIVIL_CIVILITY_MADAM']          = "Female";
$lang['EDITPROFILE_PAGE_CIVIL_CIVILITY_SIR']            = "Male";
$lang['EDITPROFILE_PAGE_CIVIL_CIVILITY_FIRSTNAME']      = "First name";
$lang['EDITPROFILE_PAGE_CIVIL_CIVILITY_LASTNAME']       = "Last name";
$lang['EDITPROFILE_PAGE_CIVIL_CIVILITY_NATIONALITY']    = "Nationality";
$lang['EDITPROFILE_PAGE_NATIONALITY']                   = "Select nationality";
$lang['EDITPROFILE_PAGE_CIVILITY']                      = "Select gender";
$lang['EDITPROFILE_PAGE_COUNTRY']                       = "Select country";
$lang['EDITPROFILE_PAGE_ADDRESS_INFO']                  = "Postal address";
$lang['EDITPROFILE_PAGE_ADDRESS_INFO_ADDRESS']          = "Address";
$lang['EDITPROFILE_PAGE_ADDRESS_INFO_CITY']             = "City";
$lang['EDITPROFILE_PAGE_ADDRESS_INFO_POSTAL_CODE']      = "Postal code";
$lang['EDITPROFILE_PAGE_ADDRESS_INFO_COUNTRY']          = "Country";
$lang['EDITPROFILE_PAGE_SAVE_BTN']                      = "Save Changes";
$lang['EDITPROFILE_PAGE_CHANGE_PASSWORD']               = "Change my password";
$lang['EDITPROFILE_PAGE_OLD_PASSWORD']                  = "Current password";
$lang['EDITPROFILE_PAGE_NEW_PASSWORD']                  = "New Password";
$lang['EDITPROFILE_PAGE_CONFIRM_PASSWORD']              = "Confirm the new password";
$lang['EDITPROFILE_PAGE_CHANGE_PASSWORD_BTN']           = "Record";
$lang['EDITPROFILE_PAGE_CHANGE_PASSWORD_SUCCESS']       = "Password successfully changed.";
$lang['EDITPROFILE_PAGE_CHANGE_PASSWORD_INVALID']       = "Your current password is incorrect, Try again.";
$lang['EDITPROFILE_PAGE_CHANGE_PASSWORD_FAILED']        = "Problem in changing your password.";


/* Search Result */
$lang['SEARCH_PAGE_TITLE']                              = "SEARCH";
$lang['SEARCH_PAGE_TITLE_RED']                          = "RESULTS";
$lang['SEARCH_PAGE_TITLE_TEXT']                         = "Use this search engine to find the matching offer";
$lang['SEARCH_PAGE_SEARCH_BTN']                         = "Find Companies";
$lang['SEARCH_PAGE_SEARCH_RESULT_TITLE']                = "Companies match your search";
$lang['SEARCH_PAGE_SEARCH_RESULT_PURPOSE']              = "ABOUT";
$lang['SEARCH_PAGE_SEARCH_RESULT_BTN']                  = "Registration";
$lang['SEARCH_PAGE_SNED_CV_STEP_TITLE_1']               = "Target";
$lang['SEARCH_PAGE_SNED_CV_STEP_TEXT_1']                = "Companies and recruitment agencies";
$lang['SEARCH_PAGE_SNED_CV_STEP_TITLE_2']               = "Submit";
$lang['SEARCH_PAGE_SNED_CV_STEP_TEXT_2']                = "Your application to thousands of recruiters";
$lang['SEARCH_PAGE_SNED_CV_STEP_TITLE_3']               = "Receive";
$lang['SEARCH_PAGE_SNED_CV_STEP_TEXT_3']                = "Positive answers and interviews";
$lang['SEARCH_PAGE_SNED_CV_STEP_BUTTON']                = "Send my application";
$lang['SEARCH_PAGE_NO_RESULT']                          = "results for your search criteria";
$lang['SEARCH_PAGE_CREATE_CV']                          = "Create my CV";
$lang['SEARCH_PAGE_ADD_TO_FAVORITE_SUCCESS']            = "Company successfully added in your favourite list.";
$lang['SEARCH_PAGE_ADD_TO_FAVORITE_FAILED']             = "Problem while adding company in your favourite list.";
$lang['SEARCH_PAGE_REMOVE_FROM_FAVORITE_SUCCESS']       = "Company successfully removed from your favourite list.";
$lang['SEARCH_PAGE_REMOVE_FROM_FAVORITE_FAILED']        = "Problem while removing Company from your favourite list.";


/* Show Profile Page */
$lang['SHOW_PROFILE_PAGE_TITLE']                                = 'My';
$lang['SHOW_PROFILE_PAGE_TITLE_RED']                            = 'Profile';
$lang['SHOW_PROFILE_PAGE_DOWNLOAD_PROFILE_PICTURE']             = 'Download your profile picture';
$lang['SHOW_PROFILE_PAGE_UPLOAD_PROFILE_PICTURE']               = 'Upload your photo';
$lang['SHOW_PROFILE_PAGE_INTRODUSE_YOUSELF']                    = 'Introduce yourself';
$lang['SHOW_PROFILE_PAGE_SEND_YOUR_APPLICATION_BTN']            = 'Send an unsolicited application';
$lang['SHOW_PROFILE_PAGE_DOWNLOAD_MY_CV']                       = 'Download my CV';
$lang['SHOW_PROFILE_PAGE_MY_EXPERIANCES']                       = 'My experiences';
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES']                      = 'Add an experience';
$lang['SHOW_PROFILE_PAGE_MY_TRAININGS']                         = 'My trainings';
$lang['SHOW_PROFILE_PAGE_ADD_TRAININGS']                        = 'Add training';
$lang['SHOW_PROFILE_PAGE_MY_SKILLS']                            = 'My skills';
$lang['SHOW_PROFILE_PAGE_ADD_SKILLS_HEADING']                   = 'Add Skill';
$lang['SHOW_PROFILE_PAGE_ADD_SKILLS_MORE']                      = 'Add more skill';
$lang['SHOW_PROFILE_PAGE_ADD_SKILLS']                           = 'Add Skill';
$lang['SHOW_PROFILE_PAGE_MY_HOBBIES']                           = 'My hobbies';
$lang['SHOW_PROFILE_PAGE_ADD_HOBBIES']                          = 'Add hobby';
$lang['SHOW_PROFILE_PAGE_ADD_TRAININGS_HEADING']                = 'Add training';
$lang['SHOW_PROFILE_PAGE_ADD_HOBBIES_HEADING']                  = 'Add hobbies';
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_COMPANY_NAME']         = "Company Name";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_TITLE']                = "Title";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PLACE']                = "Place";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD']               = "Period";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD_MONTH']         = "Month";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD_YEAR']          = "Year";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD_TO']            = "To";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_CHECKBOX_POSITION']    = "Also in current position";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_DESCRIPTION_LBL']      = "Description";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_ADD_BTN']              = "Record";
$lang['SHOW_PROFILE_PAGE_ADD_EXPERIANCES_CANCEL_BTN']           = "Cancel";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_LABEL']                  = "Training";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_ESTABLISSMENT']          = "Establishment";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_DESCRIPTION_LABEL']      = "Training Description";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_PERIOD_MONTH']           = "Month";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_PERIOD_YEAR']            = "Year";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_PERIOD_TO']              = "To";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_ADD_BTN']                = "Record";
$lang['SHOW_PROFILE_PAGE_ADD_FORMATION_CANCEL_BTN']             = "Cancel";
$lang['SHOW_PROFILE_PAGE_ADD_SKILL_AREA_LABEL']                 = "Skills area";
$lang['SHOW_PROFILE_PAGE_ADD_SKILL_ESTABLISSMENT']              = "Skill";
$lang['SHOW_PROFILE_PAGE_ADD_SKILL_ADD_BTN_NEW']                = "Add more skill";
$lang['SHOW_PROFILE_PAGE_ADD_SKILL_ADD_BTN']                    = "Record";
$lang['SHOW_PROFILE_PAGE_ADD_SKILL_CANCEL_BTN']                 = "Cancel";
$lang['SHOW_PROFILE_PAGE_NO_JOB_TYPE_SET']                      = "Set your job type";
$lang['SHOW_PROFILE_PAGE_NO_ACTIVITY_AREA_SET']                 = "Set your activity area";
$lang['SHOW_PROFILE_PAGE_NO_WORK_PLACE_SET']                    = "Set your workplace";
$lang['SHOW_PROFILE_PAGE_NO_EDU_LEVEL_SET']                     = "Set your education level";
$lang['SHOW_PROFILE_PAGE_ADD_YOUR_EXP']                         = "Add your experience details";
$lang['SHOW_PROFILE_PAGE_ADD_YOUR_TRAINING']                    = "Add your training details";
$lang['SHOW_PROFILE_PAGE_ADD_YOUR_SKILL']                       = "Add your skill details";
$lang['SHOW_PROFILE_PAGE_ADD_YOUR_HOBBIES']                     = "Add your hobbies";
$lang['SHOW_PROFILE_PAGE_BIO_PLACEHOLDER']                      = "Enter few words about you.";
$lang['SHOW_PROFILE_PAGE_BIO_UPDATE_SUCCESS']                   = "Your details successfully saved.";
$lang['SHOW_PROFILE_PAGE_BIO_UPDATE_FAILED']                    = "Problem in updating your details.";
$lang['SHOW_PROFILE_PAGE_BIO_UPDATE_SAME']                      = "No changes found with the specified description";
$lang['SHOW_PROFILE_PAGE_EXP_SUCESS']                           = "Your experience added successfully.";
$lang['SHOW_PROFILE_PAGE_EXP_FAILED']                           = "Problem in inserting your experience.";
$lang['SHOW_PROFILE_PAGE_EXP_DATE_INVALID']                     = "Your experience period selection is invalid.";
$lang['SHOW_PROFILE_PAGE_TRAINING_SUCESS']                      = "Your training data added successfully.";
$lang['SHOW_PROFILE_PAGE_TRAINING_FAILED']                      = "Problem in insert your training data.";
$lang['SHOW_PROFILE_PAGE_SKILL_SUCESS']                         = "Your skill added successfully.";
$lang['SHOW_PROFILE_PAGE_SKILL_FAILED']                         = "Problem in insert your skill.";
$lang['SHOW_PROFILE_PAGE_HOBBIES_SUCESS']                       = "Your hobbies added successfully.";
$lang['SHOW_PROFILE_PAGE_HOBBIES_FAILED']                       = "Problem in insert your hobbies.";
$lang['SHOW_PROFILE_PAGE_HOBBIES_NAME']                         = "Hobbies name";
$lang['SHOW_PROFILE_PAGE_LANGUAGES_FAILED']                     = "Problem in insert your languages.";
$lang['SHOW_PROFILE_PAGE_WORKING_HERE']                         = "Still working here";
$lang['SHOW_PROFILE_PAGE_YOUR_LANGUAGES']                       = "Your languages";
$lang['SHOW_PROFILE_SELECT_START_MONTH']                        = "Select start month";
$lang['SHOW_PROFILE_SELECT_START_YEAR']                         = "Select start year";
$lang['SHOW_PROFILE_SELECT_END_MONTH']                          = "Select end month";
$lang['SHOW_PROFILE_SELECT_END_YEAR']                           = "Select end year";
$lang['SHOW_PROFILE_PROVIDE_DETAILS']                           = "You need to first build CV from my documents and provide your experiences, courses, skills and hobbies before download cv...!";
$lang['SHOW_PROFILE_CHOOSE_DOWNLOAD_FORMAT']                    = "Choose format";
$lang['SHOW_PROFILE_DOWNLOAD_AS_PDF']                           = "Download CV as PDF";
$lang['SHOW_PROFILE_DOWNLOAD_AS_JPEG']                          = "Download CV as JPEG";
$lang['SHOW_PROFILE_DOWNLOAD_AS_PNG']                           = "Download CV as PNG";
$lang['SHOW_PROFILE_JOB_TITLE']                                 = "Job title";
$lang['SHOW_PROFILE_GENERATE_CV_ERROR']                         = "You must first create your resume and inform your experiences, skills, trainings and activity area before generating or uploading a resume";
$lang['SHOW_PROFILE_GENERATE_CV']                               = "Save the CV";
$lang['SHOW_PROFILE_GENERATE_CV_SUCCESS']                       = "Your CV successfully generated.";
$lang['SHOW_PROFILE_GENERATE_CV_FAILED']                        = "Problem while generating your cv, Try again...!";
$lang['SHOW_PROFILE_HELP_TEXT']                                 = "You must first create your CV to save the above changes";
$lang['SHOW_PROFILE_MY_ACTIVITY_AREA']                          = "My activity areas";

/** free item page content */
$lang['FREE_ITEM_TITLE']                                        = "ENJOY YOUR FIRST 100 MAILINGS";
$lang['FREE_ITEM_MATCH_CRITERIA']                               = "companies match your criteria";
$lang['FREE_ITEM_SEND_TO_FRIENDS']                              = "Send to friends";
$lang['FREE_ITEM_EMAIL_PLACEHOLDER']                            = "Email number";
$lang['FREE_ITEM_ADD_POINTS']                                   = 'And get %1$s more points';
$lang['FREE_ITEM_ADD_POINTS_NEW']                               = 'Share to enjoy %1$s free shipments!';
$lang['FREE_ITEM_TO_SELECT']                                    = "Send invitation";
$lang['FREE_ITEM_CHOOSE_FORMULA']                               = "OR Choose your formula";
$lang['FREE_ITEM_MODIFY_TERIFF']                                = "Modify your tariff";
$lang['FREE_ITEM_RATE_MESSAGE']                                 = "Vary your rate depending on the number of shipments.";
$lang['FREE_ITEM_NUMBER_OF_POINTS']                             = "Number of shipments";
$lang['FREE_ITEM_SEND_YOUR_CV']                                 = "Select the companies that match your criteria";
$lang['FREE_ITEM_SEND_YOUR_CV_TO_COMPANY']                      = "Submit your application";
$lang['FREE_ITEM_SEND_UPTO']                                    = 'You can send up to %1$s spontaneous applications by email';
$lang['FREE_ITEM_SEND_NO_COMPANY']                              = 'Company not found.';
$lang['FREE_ITEM_SEND_MAX_SELECT_COMPANY']                      = 'You can select upto '.DEFAULT_MAXIMUM_COMPANY_SELECT.' company only.';
$lang['FREE_ITEM_INVITATION_LINK']                              = 'Rapid job Invitation link';
$lang['FREE_ITEM_INVITATION_LINK_SHARE']                        = 'Share invitation link on facebook.';
$lang['FREE_ITEM_INVITATION_SENT_SUCCESS']                      = 'Invitation sent Successfully.';
$lang['FREE_ITEM_UPGRADE_TARIFF']                               = 'You need to upgrade your tariff for send your spontaneous applications';
$lang['FREE_ITEM_TARIFF_LIMIT_OVER']                            = 'Your tariff limit for send by email spontaneous applications is over.';
$lang['FREE_ITEM_INVOICE_PER_EMAIL']                            = 'Send by E-mail';
$lang['FREE_ITEM_INVOICE_PER_POST_OFFICE']                      = 'Send by courier post';
$lang['FREE_ITEM_CHOOSE_FORMULA_AND_LIMIT']                     = 'Choose your application formula type and valid tariff limit';
$lang['FREE_ITEM_TARIF_LIMIT_REQUIRED']                         = 'Tariff limit is required';
$lang['FREE_ITEM_TARIF_LIMIT_POST_OVER']                        = 'Your tariff limit for send by courier post application is over.';
$lang['FREE_ITEM_SEND_POST_UPTO']                               = 'You can send up to %1$s spontaneous applications by post.';
$lang['FREE_ITEM_SHARE_POST']                                   = 'Get the first %1$s free shipments by sharing RapidJob on Facebook';
$lang['FREE_ITEM_CURRENT_TERIFF']                               = 'Current tariff:';
$lang['FREE_ITEM_COMPANY_REQUIRED']                             = 'Either select company from list or choose your application formula type and upgrade tariff limit';
$lang['FREE_ITEM_SELECT_PAYMENT_METHOD']                        = 'Select payment method';
$lang['FREE_ITEM_SELECT_PAYMENT_TEXT']                          = 'Select your payment method and send your application now...';
$lang['FREE_ITEM_ARE_YOU_SURE']                                 = 'Are you sure?';
$lang['FREE_ITEM_ALERT_TEXT']                                   = 'If you modify your tariff then selected company will be uncheck from the list';
$lang['FREE_ITEM_ALERT_YES']                                    = 'Yes';
$lang['FREE_ITEM_ALERT_NO']                                     = 'No, cancel';





/* send application */
$lang['SEND_APPLICATION_SEND_YOUR']                             = "Send your";
$lang['SEND_APPLICATION_APP']                                   = "Spontaneous";
$lang['SEND_APPLICATION_SENT_TO_COMPANY']                       = "Send message to companies";
$lang['SEND_APPLICATION_JOIN_MY_CV']                            = "Join my CV";
$lang['SEND_APPLICATION_JOIN_MY_COVER_LATTER']                  = "Join my cover letter";
$lang['SEND_APPLICATION_USE_YOUR_CV']                           = "Use your Rapid job CV";
$lang['SEND_APPLICATION_COMPANY_TO_EXCLUDED']                   = "Companies to exclude";
$lang['SEND_APPLICATION_ACCEPT_CGU']                            = "I accept the CGU";
$lang['SEND_APPLICATION_VALIDATE_AND_SEE']                      = "Validate and see";
$lang['SEND_APPLICATION_SELECT_CV']                             = "Attach your cv or use your rapidjob cv";
$lang['SEND_APPLICATION_EMAIL_PLACEHOLDER']                     = "Enter comma separated email";
$lang['SEND_APPLICATION_EMAIL_INVALID']                         = "This field allows comma separated emails only";
$lang['SEND_APPLICATION_INSERT_ERROR']                          = "Problem while inserting your application details, Try again.";
$lang['SEND_APPLICATION_UPDATE_ERROR']                          = "Problem while updating your application details, Try again.";
$lang['SEND_APPLICATION_PROVIDE_DETAILS']                       = "If you want to user Rapid job CV then first build CV from my documents and provide your experiences, courses, skills and hobbies details...!";
$lang['SEND_APPLICATION_MESSAGE_PLACEHOLDER']                   ="Hello,

You will find my application as an attachment. I confirm my strong motivation to join your company.
I am at your disposal to exchange with you.

Regards,";

/* send application now */
$lang['SEND_APPLICATION_NOW_COMPANY_TO_EXCLUDED']               = "Company to be excluded";
$lang['SEND_APPLICATION_NOW_SELECTED_CV']                       = "Selected CV";
$lang['SEND_APPLICATION_NOW_SELECTED_LATTER']                   = "Selected cover letter";
$lang['SEND_APPLICATION_NOW_SENT_MESSAGE']                      = "Message sent to companies";
$lang['SEND_APPLICATION_NOW_SENT_SUCCESS']                      = "Your application sent successfully.";
$lang['SEND_APPLICATION_NOW_SENT_FAILURE']                      = "Problem while sending your application, Try again...!";

$lang['SEND_APPLICATION_NOW_SAVE_SUCCESS']                      = "Your application data has been saved successfully";
$lang['SEND_APPLICATION_NOW_CV_DETAILS_NOT_FOUND']              = "Your cv details not found.";

/* my documents */
$lang['MY_DOCUMENTS_MY']                                        = "My";
$lang['MY_DOCUMENTS_DOCUMENT']                                  = "Documents";
$lang['MY_DOCUMENTS_JOIN_MY_CV']                                = "Join my CV";
$lang['MY_DOCUMENTS_JOIN_MY_COVER_LATTER']                      = "Join my cover letter";
$lang['MY_DOCUMENTS_ADD_THIS_FILE']                             = 'Add this file ('.DEFAULT_MAXIMUM_CV_SIZE_LIMIT_PHP.' MB max)';
$lang['MY_DOCUMENTS_LIST']                                      = "List of my documents";
$lang['MY_DOCUMENTS_CV_NOT_AVAILABLE']                          = "File not available, You need to upload new file....!";
$lang['MY_DOCUMENTS_DOC_NOT_FOUND']                             = "Your document details not found.";
$lang['MY_DOCUMENTS_DOC_UPDATE_ERROR']                          = "Problem in updating your document details.";
$lang['MY_DOCUMENTS_CV_REMOVE_SUCCESS']                         = "Your cv successfully removed.";
$lang['MY_DOCUMENTS_COVER_REMOVE_SUCCESS']                      = "Your cover letter successfully removed.";
$lang['MY_DOCUMENTS_CV_REMOVE_FAILURE']                         = "Problem while delete your cv.";
$lang['MY_DOCUMENTS_COVER_REMOVE_FAILURE']                      = "Problem while delete your cover letter.";
$lang['MY_DOCUMENTS_REMOVE_CV_CONFIRM']                         = "Are you sure you want to delete this cv? If not, click Cancel. There is no undo...!";
$lang['MY_DOCUMENTS_REMOVE_COVER_CONFIRM']                      = "Are you sure you want to delete this cover letter? If not, click Cancel. There is no undo...!";
$lang['MY_DOCUMENTS_CREATE_CV']                                 = "Create my CV";

/* send application list */
$lang['SEND_APPLICATION_LIST_MY']                               = "My";
$lang['SEND_APPLICATION_LIST_APPLICATION']                      = "Applications";
$lang['SEND_APPLICATION_LIST_SPONTANEOUS']                      = "Spontaneous";
$lang['SEND_APPLICATION_LIST_CANDIDATURE']                      = "Created date";
$lang['SEND_APPLICATION_LIST_NO_OF_COMPANY']                    = "Number of companies";
$lang['SEND_APPLICATION_LIST_STATE']                            = "Status";
$lang['SEND_APPLICATION_LIST_APP_NOT_FOUND']                    = "Spontaneous Application not available.";

/** ALL MAIL CONSTANT */
/** invite friends */
$lang['MAIL_HELLO']                                             = "Hello";
$lang['MAIL_DEAR']                                              = "Dear";

$lang['MAIL_NEW_INVITATION']                                    = "New invitation email";
$lang['MAIL_WELCOME_TO']                                        = "Welcome to ";
$lang['MAIL_INVITE_YOU']                                        = "Your friend has invited you";
$lang['MAIL_INVITATION_LINK']                                   = "Invitation link :";
$lang['MAIL_CLICK_HERE']                                        = "Click here to register";
$lang['MAIL_REGARDS']                                           = "Regards";
$lang['MAIL_TEAM']                                              = "Team";
$lang['MAIL_INVITATION_SEND_SUCCESS_WITH_EXISTS']               = 'Your invitation email successfully sent to %1$s, and %2$s email is already invited.';
$lang['MAIL_INVITATION_SEND_SUCCESS']                           = "Your invitation email successfully sent.";
$lang['MAIL_INVITATION_ALREADY_SENT']                           = "Your requested emails already invited";
$lang['MAIL_INVITATION_SEND_FAILED']                            = "Problem in sending your invitation email.";
$lang['MAIL_NEWSLATTER_TEXT']                                   = "You successfully register newsletter.<br>now you get all update to your email account.";

/** send application mail **/
$lang['MAIL_APPLICATION_SUBJECT']                               = "New job application mail";
$lang['MAIL_APPLICATION_INTEREST']                              = "I am very interested in your company. My qualifications and experience match your specifications almost exactly.";
$lang['MAIL_APPLICATION_DOC_TEXT']                              = "Please take a moment to review my attached Application Documents:";
$lang['MAIL_APPLICATION_BACK_TO_SOON_TEXT']                     = "It would be a sincere pleasure to hear back from you soon to discuss this exciting opportunity.";
$lang['MAIL_SINCERELY']                                         = "Sincerely";

/* forgot password */
$lang['FORGOT_PASSWORD_SUBJECT']                                = "Password Recovery Email";
$lang['FORGOT_PASSWORD_LINK_TEXT']                              = "Reset your password";
$lang['FORGOT_PASSWORD_TEXT_1']                                 = "We have received a password change request from your account.";
$lang['FORGOT_PASSWORD_TEXT_2']                                 = "If you didn't request this, please ignore this email.";
$lang['FORGOT_PASSWORD_TEXT_3']                                 = "Your password won't change until you access the link above and create a new one.";
$lang['FORGOT_PASSWORD_EMAIL_NOT_REGISTERED']                   = "This email is not registered with us.";
$lang['FORGOT_PASSWORD_EMAIL_SUCCESS']                          = "Reset password link has been sent to your email.";
$lang['FORGOT_PASSWORD_TOKEN_EXPIRED']                          = "This link has been expired.";
$lang['FORGOT_PASSWORD_SEND']                                   = "Reset my password";
$lang['FORGOT_PASSWORD_BACK_TO_LOGIN']                          = "Back to Log in";
$lang['FORGOT_PASSWORD_DESCRIPTION']                            = "Forgot your account's password? Enter your email address and we'll send you a recovery link.";
/** END OF ALL MAIL CONSTANT */

/** FILE VALIDATION CONSTANT */

$lang['ERROR_INVALID_FILE']                                     = 'Invalid file, file must be image and less than '.DEFAULT_IMAGE_SIZE_LIMIT_MB.'MB';
$lang['ERROR_INVALID_EXTENSION']                                = 'Invalid file type or extension, file must be image!';
$lang['ERROR_INVALID_SIZE']                                     = 'Image size must be less than '.DEFAULT_IMAGE_SIZE_LIMIT_MB.'MB';
$lang['ERROR_INVALID_DIMENSION']                                = 'Image dimension must be greater than 200 x 200 pixels.';
$lang['ERROR_PROBLEM_IN_UPLOAD']                                = 'Problem while uploading image!';
$lang['PHOTO_UPLOAD_SUCCESS']                                   = 'Your profile picture uploaded successfully.';

$lang['ERROR_CV_INVALID_FILE']                                  = 'File must be pdf, jpg, jpeg or png, less than '.DEFAULT_MAXIMUM_CV_SIZE_LIMIT_PHP.' MB';
$lang['ERROR_CV_INVALID_EXTENSION']                             = 'Invalid file type or extension, file must be pdf, jpg, jpeg or png';
$lang['ERROR_COVER_INVALID_EXTENSION']                          = 'Invalid file type or extension, cover letter file must be jpg, jpeg or png';
$lang['ERROR_CV_INVALID_SIZE']                                  = 'File size must be less than '.DEFAULT_MAXIMUM_CV_SIZE_LIMIT_PHP.' MB';
$lang['ERROR_CV_PROBLEM_IN_UPLOAD']                             = 'Problem while uploading application file!';
$lang['CV_UPLOAD_SUCCESS']                                      = 'Your application file uploaded successfully.';
$lang['UPLOAD_CV_UPLOAD_SUCCESS']                               = 'Your cv successfully updated.';
$lang['UPLOAD_COVER_UPLOAD_SUCCESS']                            = 'Your cover letter successfully updated.';
/** END OF FILE VALIDATION CONSTANT */

/** Professtional Registration */
$lang['PROF_REGISTER_TITLE_RECRUIT']                            = "RECRUIT";
$lang['PROF_REGISTER_TITLE_QUICKLY']                            = "QUICKLY";
$lang['PROF_REGISTER_TITLE_FREE']                               = "FREE !";
$lang['PROF_REGISTER_ACCESS_TO']                                = "Access to our";
$lang['PROF_REGISTER_CV_DATABASE']                              = "CV Database";
$lang['PROF_REGISTER_REGISTER_YOUR']                            = "REGISTER YOUR";
$lang['PROF_REGISTER_IN_OUR']                                   = "IN OUR";
$lang['PROF_REGISTER_BROADCAST_LIST']                           = "BROADCAST LIST";
$lang['PROF_REGISTER_GOTO']                                     = "Go to";
$lang['PROF_REGISTER_PREMIUM_MODE']                             = "Premium mode";
$lang['PROF_REGISTER_SOCIAL_REASON_OF_COMPANY']                 = "Social reason of the company";
$lang['PROF_REGISTER_SOCIAL_THEME']                             = "Activity area";
$lang['PROF_REGISTER_NO_ACTIVITY_AREA_SET']                     = "Activity area not available";
$lang['PROF_REGISTER_SUBSCRIBE']                                = "REGISTER FOR";
$lang['PROF_REGISTER_FREE']                                     = "FREE";
/** END OF Professtional Registration **/

/** Professtional my company profile */
$lang['PROF_MY_PROFILE_COMPANY_PROFILE']                        = "company profile";
$lang['PROF_MY_PROFILE_PREMIUM_MEMBER']                         = 'BECOME A PREMIUM MEMBER FOR %1$s€';
$lang['PROF_MY_PROFILE_LOGIN_EMAIL']                            = "Login Email";
$lang['PROF_MY_PROFILE_CIVIL_STATUS']                           = "Civil status";
$lang['PROF_MY_PROFILE_COMPANY_ADDRESS']                        = "Company address";
$lang['PROF_MY_PROFILE_COMPANY_IMPORT_LOGO_TEXT']               = "You can import your logo if you are a premium member";
$lang['PROF_MY_PROFILE_ACTIVITY_AREA']                          = "Activity area";
$lang['PROF_MY_PROFILE_COMPANY_SEARCH_ADDRESS']                 = "Search address";
$lang['PROF_MY_PROFILE_PREMIUM']                                = 'Your premium plan is activated';
$lang['PROF_MY_PROFILE_PREMIUM_PLAN_EXPIRE']                    = "Your premium plan is expired, Become a premium member for 50 €";
$lang['PROF_MY_PROFILE_PREMIUM_PLAN_EXPIRE_DATE']               = "Your plan will be expired on";





/** END OF Professtional my company profile */

/** CHANGE PROF PASSWORD */

$lang['CHANGE_PASSWORD_HEADING']                                = "Change my password";
$lang['CHANGE_PASSWORD_CURRENT_PASSWORD']                       = "Current Password";
$lang['CHANGE_PASSWORD_NEW_PASSWORD']                           = "New Password";
$lang['CHANGE_PASSWORD_CONFIRM_PASSWORD']                       = "Confirm Password";

/** END OF CHANGE PROF PASSWORD */

/** Professional resume database */
$lang['PROF_RESUME_DATABASE_HEADING_CV']                        = "ACCESS";
$lang['PROF_RESUME_DATABASE_HEADING']                           = "RESUMES";
$lang['PROF_RESUME_DATABASE_SEARCH']                            = "Use this search engine to find the candidate that meets your expectations";
$lang['PROF_RESUME_DATABASE_SEARCH_JOB']                        = "Searched for job";
$lang['PROF_RESUME_DATABASE_SEARCH_AREA']                       = "Activity area";
$lang['PROF_RESUME_DATABASE_SEARCH_LOCATION']                   = "Location search";
$lang['PROF_RESUME_DATABASE_SEARCH_MATCH']                      = "results for your search criteria";
$lang['PROF_RESUME_DATABASE_AVAILABLE']                         = "Immediate availability - Updated on";
$lang['PROF_RESUME_DATABASE_NO_JOB_TYPE_SET']                   = "Job type not available";
$lang['PROF_RESUME_DATABASE_NO_ACTIVITY_AREA_SET']              = "Activity area not available";
$lang['PROF_RESUME_DATABASE_NO_WORK_PLACE_SET']                 = "Workplace not available";
$lang['PROF_RESUME_DATABASE_WORK_EXPERIENCE']                   = "YEARS OF EXPERIENCE";
$lang['PROF_RESUME_DATABASE_ADD_TO_FAVORITE_SUCCESS']           = "Candidate successfully added in your favourite list.";
$lang['PROF_RESUME_DATABASE_ADD_TO_FAVORITE_FAILED']            = "Problem while adding candidate in your favourite list.";
$lang['PROF_RESUME_DATABASE_REMOVE_FROM_FAVORITE_SUCCESS']      = "Candidate successfully removed from your favourite list.";
$lang['PROF_RESUME_DATABASE_REMOVE_FROM_FAVORITE_FAILED']       = "Problem while removing candidate from your favourite list.";
/** END OF Professional resume database */

/** Professional resume details */
$lang['PROF_RESUME_DETAILS_HEADING']                            = " of candidate";
$lang['PROF_RESUME_DETAILS_CONTACT_TO_CANDIDATE']               = "Contact to candidate";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_TO_CANDIDATE']             = "Send an Email to";
$lang['PROF_RESUME_DETAILS_CANDIDATE_EMPTY']                    = "Candidate resume details not found...";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_SUBJECT']                  = "New recruiter message";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_TEXT_1']                   = "reviewed your profile and sending you below message :";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_RECRUITER_EMAIL']          = "Recruiter email:";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_RECRUITER_PHONE']          = "Recruiter phone:";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_RECRUITER_ADDRESS']        = "Recruiter address:";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_SUCCESS']                  = "Your email successfully sent to";
$lang['PROF_RESUME_DETAILS_SEND_MAIL_FAILED']                   = "Problem while sending email to";
/** END OF Professional resume details*/

/** Professional favourite resume  */
$lang['PROF_RESUME_FAVOURITE_HEADING']                          = "MY FAVOURITE";
$lang['PROF_RESUME_FAVOURITE_HEADING_2']                        = "PROFILES";
$lang['PROF_RESUME_FAVOURITE_SEARCH_MATCH']                     = "profiles in your favorites list";
$lang['PROF_RESUME_FAVOURITE_WORK_EXPERIENCE']                  = "YEARS OF EXPERIENCE";
$lang['PROF_RESUME_FAVOURITE_AVAILABLE']                        = "Immediate availability - Updated on";
$lang['PROF_RESUME_FAVOURITE_CANDIDATE_EMPTY']                  = "Candidate resume details not found...";
$lang['PROF_RESUME_FAVOURITE_REMOVE_FROM_FAVORITE_SUCCESS']     = "Candidate successfully removed from your favourite list.";
$lang['PROF_RESUME_FAVOURITE_REMOVE_FROM_FAVORITE_FAILED']      = "Problem while removing candidate from your favourite list.";
$lang['PROF_RESUME_FAVOURITE']                                  = "Profiles in your favorites list";
/** END OF Professional favourite resume */

/** Professional receive priority resume  */
$lang['PROF_PRIORITY_RESUME_HEADING']                           = "RECEIVE PRIORITY";
$lang['PROF_PRIORITY_RESUME_HEADING_2']                         = "CV";
$lang['PROF_PRIORITY_RESUME_DESCRIPTION']                       = "The Lorem Ipsum is simply false text used in composition and layout before printing.
                                                                  The Lorem Ipsum is the standard false text of printing since the 1500s, when an anonymous painter assembled
                                                                  Set of text pieces to make a specimen book of text fonts. It did not only survive five centuries,
                                                                  But has also adapted to the computer office, without its content being modified. It has been popularized in
                                                                  1960s thanks to the sale of Letraset sheets containing passages from the Lorem Ipsum and,
                                                                  More recently, by its inclusion in text layout applications, such as Aldus PageMaker.";

/** END OF Professional receive priority resume */

/** Professional search receive priority resume  */
$lang['PROF_SEARCH_PRIORITY_RESUME_HEADING']                    = "RECEIVE PRIORITY";
$lang['PROF_SEARCH_PRIORITY_RESUME_HEADING_2']                  = "CV";
$lang['PROF_SEARCH_PRIORITY_RESUME_SEND_REQUEST']               = "Send my request";
$lang['PROF_SEARCH_PRIORITY_RESUME_INFORMATION']                = "Information seen by candidates";
$lang['PROF_SEARCH_PRIORITY_RESUME_SOCIAL_REASON_OF_COMPANY']   = "Social reason of the company";
$lang['PROF_SEARCH_PRIORITY_RESUME_ACTIVITY_AREA']              = "Activity area";
$lang['PROF_SEARCH_PRIORITY_RESUME_MODIFY']                     = "Edit information";  
$lang['PROF_SEARCH_PRIORITY_RESUME_NOT_SUBSCRIBED']             = "Request not allowed, You need to subscribe plan for proceed further";  
$lang['PROF_SEARCH_PRIORITY_SUCCESS']                           = "Your resume search priority successfully saved.";  
$lang['PROF_SEARCH_PRIORITY_FAILED']                            = "Problem while changing your resume search priority.";


/** END OF Professional receive priority resume */


/* Nationality */
$lang['NATIONALITY_1']                                          = "Indian";
$lang['NATIONALITY_2']                                          = "French";

/* Country */
$lang['COUNTRY_1']                                              = "France";
$lang['COUNTRY_2']                                              = "India";

/* Verify password and update new password */
$lang['VERIFY_PASSWORD_CHANGE_SUCCESS']                         = "Password successfully changed, Now you can login using new password";
$lang['VERIFY_PASSWORD_CHANGE_FAILED']                          = "Problem in changing your password, Try again later.";
$lang['VERIFY_PASSWORD_HEAD_1']                                 = "Reset";
$lang['VERIFY_PASSWORD_HEAD_2']                                 = "my password";
$lang['VERIFY_PASSWORD_SUBMIT']                                 = "Change Password";

/* Edit candidate details model pop-up*/

$lang['UPDATE_EXPERIENCE_TITLE']                                = "Edit experience";
$lang['UPDATE_SKILL_TITLE']                                     = "Edit skill";
$lang['UPDATE_TRAINING_TITLE']                                  = "Edit training";
$lang['UPDATE_HOBBY_TITLE']                                     = "Edit hobby";

$lang['UPDATE_EXPERIENCE_SUCCESS']                              = "Your experience updated successfully.";
$lang['UPDATE_EXPERIENCE_FAILED']                               = "Problem while updating your experience details.";
$lang['UPDATE_SKILL_SUCCESS']                                   = "Your skill updated successfully.";
$lang['UPDATE_SKILL_FAILED']                                    = "Problem while updating your skill details.";
$lang['UPDATE_TRAINING_SUCCESS']                                = "Your training updated successfully.";
$lang['UPDATE_TRAINING_FAILED']                                 = "Problem while updating your training details.";
$lang['UPDATE_HOBBIY_SUCCESS']                                  = "Your hobby updated successfully.";
$lang['UPDATE_HOBBY_FAILED']                                    = "Problem while updating your hobby details.";

/* Delete candidate details model pop-up*/
$lang['DELETE_ARE_YOU_SURE_COMMON']                             = "Are you sure you want to delete this record?";
$lang['DELETE_MESSAGE_TEXT']                                    = "This data will be permanently removed, and not revertible.";
$lang['DELETE_ARE_YOU_SURE_EXPERIENCE']                         = "Are you sure you want to delete this experience?";
$lang['DELETE_EXPERIENCE_SUCCESS']                              = "Your experience deleted successfully.";
$lang['DELETE_EXPERIENCE_FAILED']                               = "Problem while deleting your experience details.";
$lang['DELETE_ARE_YOU_SURE_SKILL']                              = "Are you sure you want to delete this skill?";
$lang['DELETE_SKILL_SUCCESS']                                   = "Your skill deleted successfully.";
$lang['DELETE_SKILL_FAILED']                                    = "Problem while deleting your skill details.";
$lang['DELETE_ARE_YOU_SURE_TRAINING']                           = "Are you sure you want to delete this training?";
$lang['DELETE_TRAINING_SUCCESS']                                = "Your training deleted successfully.";
$lang['DELETE_TRAINING_FAILED']                                 = "Problem while deleting your training details.";
$lang['DELETE_ARE_YOU_SURE_HOBBY']                              = "Are you sure you want to delete this hobby?";
$lang['DELETE_HOBBY_SUCCESS']                                   = "Your hobby deleted successfully.";
$lang['DELETE_HOBBY_FAILED']                                    = "Problem while deleting your hobby details.";
$lang['DELETE_YES_DELETE_IT']                                   = "Yes, delete it!";
$lang['DELETE_NO_CANCEL_IT']                                    = "No, cancel";

/* News */
$lang['NEWS_DETAILS']                                           = "News Details";
$lang['TITLE']                                                  = "Title";
$lang['DESCRIPTION']                                            = "Description";
$lang['HOME_NEWSLETTER_ERROR_REQUIRED']                         = "Please enter email address.";
$lang['HOME_NEWSLETTER_ERROR_EMAIL']                            = "Please enter valid email address.";


/** payment */
$lang['PAYMENT_SUCCESS']                                        = "Your payment done successfully";
$lang['PAYMENT_FAILED']                                         = "Your transaction is failed, Contact our administrator";
$lang['PAYMENT_STORE_FAILED']                                   = "Problem while store your transaction details";
$lang['PAYMENT_STORE_FAILED_DETAILS']                           = "Your transaction is failed, Problem while store your transaction details";

/** orders */
$lang['MY_ORDERS']                                              = "Orders";
$lang['MY_ORDERS_NUMBER_OF_SHIPMENTS']                          = "Number of shipments";
$lang['MY_ORDERS_PAID_AMOUNT']                                  = "Paid amount";
$lang['MY_ORDERS_STATUS']                                       = "Order status";
$lang['MY_ORDERS_CREATED_DATE']                                 = "Created date";
$lang['MY_ORDERS_PAYMENT_BY']                                   = "Payment By";
$lang['MY_ORDERS_PAYMENT_BY_PAYPAL']                            = "PayPal";
$lang['MY_ORDERS_PAYMENT_BY_SYSTEMPAY']                         = "SystemPay";
$lang['MY_ORDERS_EMPTY']                                        = "Orders not available.";


$lang['CONFIRMATION_EMAIL']                                     = "Email and confirm email should be same";
$lang['LETTER_ONLY']                                            = "Please enter only alphabetical letters.";


