<?php

/**
 * @author Nitinkumar vaghani
 * 
 * Created date : 2017-04-17
 */
class Professional_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for get professional users details
     * 
     * Created date : 2017-04-17
     */
    public function get_user_data() {
        $user_data = array();
        try {
            $where = array(
                "u_id" => $this->user_id,
                "u_status !=" => 9,
            );

            $join = array(
                TBL_COMPANY_DETAILS => 'cd_user_id=u_id and cd_status !=9'
            );

            $user_data = $this->Common_model->get_single_row(TBL_USERS, "*", $where, $join, 'LEFT');
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(HOME_PATH);
        }
        return $user_data;
    }

    /**
     * Description: Use this function for display my orders
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-05-22
     * 
     * @param type $user_id
     * @return type
     */
    public function get_user_order_details($user_id = '') {
        $user_data = array();
        if (!empty($user_id) && is_numeric($user_id)) {
            $column_order = "up_id,up_user_id,up_amount,up_subscription_number,up_payment_by,up_created_date,up_status";
            $where_order = array(
                'up_user_id' => $user_id,
                'up_status !=' => 9
            );
            $user_data = $this->get_all_rows(TBL_USER_PAYMENTS, $column_order, $where_order);
        }
        return $user_data;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for get candidate details
     * 
     * Created date : 2017-04-27
     */
    public function get_candidate_and_recruiter_data($candidate_id, $recruiter_id) {
        $user_data = array();
        if (!empty($candidate_id) && is_numeric($candidate_id) && !empty($recruiter_id) && is_numeric($recruiter_id)) {
            try {
                $columns = " CONCAT(if(recruiter.u_first_name is null ,'',recruiter.u_first_name),' ',if(recruiter.u_last_name is null ,'',recruiter.u_last_name)) as recruiter_name,
                         CONCAT(if(candidate.u_first_name is null ,'',candidate.u_first_name),' ',if(candidate.u_last_name is null ,'',candidate.u_last_name)) as candidate_name,
                        `recruiter`.`u_id` as `recruiter_id`, 
                        `recruiter`.`u_type` as `recruiter_type`, 
                        `recruiter`.`u_email` as `recruiter_email`, 
                        `recruiter`.`u_image` as `recruiter_image`, 
                        `recruiter`.`u_status` as `recruiter_status`, 
                        `recruiter`.`u_status` as `recruiter_status`, 
                        `candidate`.`u_id` as `candidate_id`, 
                        `candidate`.`u_type` as `candidate_type`, 
                        `candidate`.`u_email` as `candidate_email`, 
                        `candidate`.`u_image` as `candidate_image`, 
                        `candidate`.`u_status` as `candidate_status`,
                        company_details.* ";

                $where = array(
                    "`recruiter`.`u_id`" => $recruiter_id,
                    "`candidate`.`u_id`" => $candidate_id,
                    "`recruiter`.`u_status` !=" => 9
                );
                $join = array(TBL_COMPANY_DETAILS . " as company_details" => "recruiter.u_id=cd_user_id and cd_status!=9");
                $tables_data = TBL_USERS . " as recruiter, " . TBL_USERS . " as candidate";
                $user_data = $this->Common_model->get_single_row($tables_data, $columns, $where, $join, "RIGHT");
            } catch (Exception $e) {
                $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
                redirect(HOME_PATH);
            }
        }
        return $user_data;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for get users application details
     * 
     * Created date : 2017-04-19
     */
    public function get_user_applications($default_data = array()) {
        $user_application = array();
        try {

            $columns = " CONCAT(if(recruiter.u_first_name is null ,'',recruiter.u_first_name),' ',if(recruiter.u_last_name is null ,'',recruiter.u_last_name)) as recruiter_name,
                         CONCAT(if(candidate.u_first_name is null ,'',candidate.u_first_name),' ',if(candidate.u_last_name is null ,'',candidate.u_last_name)) as candidate_name,
                        `recruiter`.`u_id` as `recruiter_id`, 
                        `recruiter`.`u_type` as `recruiter_type`, 
                        `recruiter`.`u_email` as `recruiter_email`, 
                        `recruiter`.`u_image` as `recruiter_image`, 
                        `recruiter`.`u_status` as `recruiter_status`, 
                        `candidate`.`u_id` as `candidate_id`, 
                        `candidate`.`u_type` as `candidate_type`, 
                        `candidate`.`u_email` as `candidate_email`, 
                        `candidate`.`u_image` as `candidate_image`, 
                        `candidate`.`u_status` as `candidate_status`, 
                        `recruiter_details`.*, 
                        `candidate_application`.*, 
                        `candidate_document_files`.*, 
                        `candidate_work_job`.*, 
                        `candidate_details`.* ";

            $candidate_skills_query = ", 
                                (
                                 SELECT 
                                     GROUP_CONCAT(`candidate_user_skills`.`us_skills`)
                                 FROM 
                                    " . TBL_USER_SKILLS . " as `candidate_user_skills` 
                                 WHERE 
                                    `candidate_user_skills`.`us_user_id` = `candidate`.`u_id`) as candidate_skills ";

            $recruiter_favourite_query = " ,(
                                 SELECT 
                                     COUNT(cf_id)
                                 FROM 
                                    " . TBL_COMPANY_FAVOURITES . " as `recruiter_favourite` 
                                 WHERE 
                                    `recruiter_favourite`.`cf_user_id` = `candidate`.`u_id` AND recruiter_favourite.cf_status=1) as user_favourite ";

            $where = " `recruiter`.`u_id` = " . $this->user_id . " AND `recruiter`.`u_status` != 9 ";

            if (!empty($default_data)) {

                if (!empty($default_data['search_by_job_type']) && is_numeric($default_data['search_by_job_type'])) {
                    $where.= " AND candidate_details.ud_job=" . $default_data['search_by_job_type'] . " ";
                }

                if (isset($default_data['search_by_activity_area']) && !empty($default_data['search_by_activity_area'])) {
                    $where.= " AND candidate_details.ud_activity_area IN (" . implode(',', $default_data['search_by_activity_area']) . ") ";
                }

                if (isset($default_data['search_by_work_place']) && !empty($default_data['search_by_work_place'])) {
                    $where.= " AND candidate_details.ud_work_place IN (" . implode(',', $default_data['search_by_work_place']) . ") ";
                }
            }


            $per_page_data = DEFAULT_RESUME_LIMIT;
            $page = $default_data['request_page'];
            if (!empty($page) && is_numeric($page)) {
                $start = $per_page_data * ($page - 1);
                $page++;
            } else {
                $page = 1;
                $start = 0;
            }

            $start = 0;

            $limit = $start . ", " . $per_page_data * $page;

//            `candidate_application`.`ua_company_id` = `recruiter`.`u_id` and `candidate_application`.`ua_status` != 9 

            $query = "  SELECT 
                            " . $columns . " " . $candidate_skills_query . " " . $recruiter_favourite_query . " 
                        FROM 
                            " . TBL_USERS . " as `recruiter` 
                        RIGHT JOIN 
                            " . TBL_COMPANY_DETAILS . " as `recruiter_details` 
                        ON 
                            `recruiter_details`.`cd_user_id` = `recruiter`.`u_id` and `recruiter_details`.`cd_status` != 9 
                        RIGHT JOIN 
                            " . TBL_USER_APPLICATIONS . " as `candidate_application` 
                        ON 
                            FIND_IN_SET(`recruiter`.`u_id`, `candidate_application`.`ua_company_id`) and `candidate_application`.`ua_status` != 9 
                        RIGHT JOIN 
                            " . TBL_USERS . " as `candidate` 
                        ON 
                            `candidate_application`.`ua_user_id` = `candidate`.`u_id` and `candidate`.`u_status` != 9 
                        RIGHT JOIN 
                            " . TBL_USER_DETAILS . " as `candidate_details` 
                        ON 
                            `candidate_details`.`ud_user_id` = `candidate`.`u_id` and `candidate_details`.`ud_status` != 9 
                        RIGHT JOIN 
                            " . TBL_USER_DOCUMENT_FILES . " as `candidate_document_files` 
                        ON 
                            `candidate_document_files`.`udf_user_id` = `candidate`.`u_id` and `candidate_document_files`.`udf_status` != 9 
                        LEFT JOIN 
                            " . TBL_WORK_JOB . " as `candidate_work_job` 
                        ON 
                            `candidate_work_job`.`wj_id` = `candidate_details`.`ud_job` and `candidate_work_job`.`wj_status` != 9 
                        WHERE 
                                " . $where . "
                        GROUP BY 
                                `candidate_application`.`ua_user_id`
                        ORDER BY 
                                `candidate_application`.`ua_id` DESC ";

            $user_data_count = $this->Common_model->get_count_by_query($query);

            $query.=" LIMIT " . $limit;

            $user_data = $this->Common_model->get_all_rows_by_query($query);

            if (!empty($user_data) && is_array($user_data)) {
                $user_data = $this->get_all_users_experience($user_data);
            }
            $user_application['user_application'] = $user_data;
            $user_application['total_count'] = (int) $user_data_count;
            $user_application['total_pages'] = (int) ceil($user_data_count / $per_page_data);
            $user_application['next_page'] = (int) $page;
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(HOME_PATH);
        }
        return $user_application;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for get users experience data
     * 
     * Created date : 2017-04-26
     * 
     * @param type $user_data
     * @return type
     */
    public function get_all_users_experience($user_data = array()) {
        try {
            if (!empty($user_data) && is_array($user_data)) {
                foreach ($user_data as $key => &$value) {

                    $experience_columns = " u_id, 
                                            ue_id,
                                            ue_user_id, 
                                            ue_job, 
                                            ue_company, 
                                            ue_in_current_position, 
                                            wj_name_fr, 
                                            wj_name_en,
                                            TIMESTAMPDIFF(MONTH, FROM_UNIXTIME(`ue_start_date`), CASE WHEN ue_end_date IS null THEN now() ELSE FROM_UNIXTIME(`ue_end_date`) END )/12 as year_of_experience 
                                            ";
                    $experience_where = "   `candidate`.`u_id` = " . $value['candidate_id'] . " 
                                            AND 
                                            `candidate`.`u_status` != 9 ";
                    $experience_query = "   SELECT 
                                                " . $experience_columns . "
                                            FROM 
                                                " . TBL_USERS . " as `candidate` 
                                            RIGHT JOIN 
                                                " . TBL_USER_EXPERIANCES . " as `candidate_experience` 
                                            ON 
                                                `candidate_experience`.`ue_user_id` = `candidate`.`u_id` and `candidate_experience`.`ue_status`=1
                                            RIGHT JOIN 
                                                " . TBL_WORK_JOB . " as `candidate_work_job` 
                                            ON 
                                                `candidate_experience`.`ue_job` = `candidate_work_job`.`wj_id` and `candidate_work_job`.`wj_status` != 9 
                                            WHERE 
                                                    " . $experience_where . "
                                            GROUP BY 
                                                    `candidate_experience`.`ue_id`
                                            ORDER BY 
                                                    `candidate_experience`.`ue_start_date` DESC ";
                    $experience_data = $this->Common_model->get_all_rows_by_query($experience_query);
                    $value['user_experience'] = $experience_data;
                }
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(HOME_PATH);
        }
        return $user_data;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for update professional users details
     * 
     * Created date : 2017-04-17
     */
    public function update_profesional_details() {
        $email = trim($this->Common_model->escape_data($this->input->post("email")));
        $gender = trim($this->Common_model->escape_data($this->input->post("civility")));
        $first_name = trim($this->Common_model->escape_data($this->input->post("first_name")));
        $last_name = trim($this->Common_model->escape_data($this->input->post("last_name")));
        $phone = trim($this->Common_model->escape_data($this->input->post("phone")));
        $social_reason = trim($this->Common_model->escape_data($this->input->post("social_reason")));
        $activity_area = $this->input->post("activity_area[]");
        $address = trim($this->Common_model->escape_data($this->input->post("address")));
        $city = trim($this->Common_model->escape_data($this->input->post("city")));
        $postal_code = trim($this->Common_model->escape_data($this->input->post("postal_code")));
        $country = trim($this->Common_model->escape_data($this->input->post("country")));
        $nationality = trim($this->Common_model->escape_data($this->input->post("nationality")));

        $user_data = array(
            "u_first_name" => $first_name,
            "u_last_name" => $last_name,
            "u_email" => $email,
            "u_modified_date" => $this->utc_time
        );
        $where_user = array(
            "u_id" => $this->user_id,
            "u_status" => 1
        );
        $user_id = $this->Common_model->update(TBL_USERS, $user_data, $where_user);
        if ($user_id > 0) {
            $user_session_data = array(
                "user_first_name" => $first_name,
                "user_last_name" => $last_name
            );

            $this->session->set_userdata($user_session_data);

            if (!empty($gender)) {
                $request_data['cd_gender'] = $gender;
            }

            if (!empty($address)) {
                $request_data['cd_address'] = $address;
            }
            if (!empty($city)) {
                $request_data['cd_city'] = $city;
            }
            if (!empty($postal_code)) {
                $request_data['cd_postal_code'] = $postal_code;
            }
            if (!empty($nationality)) {
                $request_data['cd_nationality'] = $nationality;
            }
            if (!empty($country)) {
                $request_data['cd_country'] = $country;
            }
            if (!empty($social_reason)) {
                $request_data['cd_social_reason'] = $social_reason;
            }
            if (!empty($phone)) {
                $request_data['cd_phone'] = $phone;
            }
            if (!empty($activity_area)) {
                $request_data['cd_activity_area'] = implode(',', $activity_area);
            }
            $request_data['cd_modified_date'] = $this->utc_time;
            $where_detail = array(
                "cd_user_id" => $this->user_id,
                "cd_status" => 1
            );

            return $this->Common_model->update(TBL_COMPANY_DETAILS, $request_data, $where_detail);
        } else {
            return FALSE;
        }
        return FALSE;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for update professional users details
     * 
     * Created date : 2017-05-16
     */
    public function update_profesional_settings() {
        if ($this->input->post() !== NULL && $this->input->post('show_resume_to_candidate_form') == 'true') {
            $social_reason = trim($this->Common_model->escape_data($this->input->post("social_reason")));
            $activity_area = $this->input->post("activity_area[]");
            $cd_id = trim($this->input->post('cd_id'));

            if (!empty($social_reason)) {
                $request_data['cd_social_reason'] = $social_reason;
            }

            if (!empty($activity_area)) {
                $request_data['cd_activity_area'] = implode(',', $activity_area);
            }

            $request_data['cd_modified_date'] = $this->utc_time;
            $where_detail = array(
                "cd_user_id" => $this->user_id,
                "cd_id" => $cd_id,
                "cd_status" => 1
            );
            return $this->Common_model->update(TBL_COMPANY_DETAILS, $request_data, $where_detail);
        } else {
            return FALSE;
        }
        return FALSE;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for update professional seardch priority details
     * 
     * Created date : 2017-05-16
     */
    public function manage_resume_search_priority() {

        if ($this->input->post() !== NULL && $this->input->post('search_priority_resume') == 'true') {
            $activity_area = $this->input->post('activity_area[]');
            $work_place = $this->input->post('work_place[]');

            $job_type = trim($this->input->post('job_type'));
            $activity_area = !empty($activity_area) && count($activity_area) > 0 ? array_filter($activity_area) : '';
            $work_place = !empty($work_place) && count($work_place) > 0 ? array_filter($work_place) : '';
            $education = trim($this->input->post('education_level'));

            $cd_id = trim($this->input->post('cd_id'));

            if (!empty($education)) {
                $request_data['cd_search_education_level'] = $education;
            }

            if (!empty($job_type)) {
                $request_data['cd_search_job_type'] = $job_type;
            }

            if (!empty($activity_area)) {
                $request_data['cd_search_activity_area'] = implode(',', $activity_area);
            }

            if (!empty($work_place)) {
                $request_data['cd_search_work_place'] = implode(',', $work_place);
            }

            $request_data['cd_modified_date'] = $this->utc_time;

            $where_detail = array(
                "cd_user_id" => $this->user_id,
                "cd_id" => $cd_id,
                "cd_status" => 1
            );
            return $this->Common_model->update(TBL_COMPANY_DETAILS, $request_data, $where_detail);
        } else {
            return FALSE;
        }
        return FALSE;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for get favourite users application details
     * 
     * Created date : 2017-04-38
     */
    public function get_favourite_user_applications($default_data = array()) {
        $user_application = array();
        try {

            $columns = "CONCAT(if(recruiter.u_first_name is null ,'',recruiter.u_first_name),' ',if(recruiter.u_last_name is null ,'',recruiter.u_last_name)) as recruiter_name,
                         CONCAT(if(candidate.u_first_name is null ,'',candidate.u_first_name),' ',if(candidate.u_last_name is null ,'',candidate.u_last_name)) as candidate_name,
                        `recruiter`.`u_id` as `recruiter_id`, 
                        `recruiter`.`u_type` as `recruiter_type`, 
                        `recruiter`.`u_email` as `recruiter_email`, 
                        `recruiter`.`u_image` as `recruiter_image`, 
                        `recruiter`.`u_status` as `recruiter_status`, 
                        `candidate`.`u_id` as `candidate_id`, 
                        `candidate`.`u_type` as `candidate_type`, 
                        `candidate`.`u_email` as `candidate_email`, 
                        `candidate`.`u_image` as `candidate_image`, 
                        `candidate`.`u_status` as `candidate_status`, 
                        `recruiter_details`.*, 
                        `candidate_application`.*, 
                        `candidate_document_files`.*, 
                        `candidate_work_job`.*, 
                        `candidate_details`.* ";

            $candidate_skills_query = ", 
                                (
                                 SELECT 
                                     GROUP_CONCAT(`candidate_user_skills`.`us_skills`)
                                 FROM 
                                    " . TBL_USER_SKILLS . " as `candidate_user_skills` 
                                 WHERE 
                                    `candidate_user_skills`.`us_user_id` = `candidate`.`u_id`) as candidate_skills ";

            $where = " `recruiter`.`u_id` = " . $this->user_id . " AND `recruiter`.`u_status` != 9 ";

            if (!empty($default_data)) {

                if (!empty($default_data['search_by_job_type']) && is_numeric($default_data['search_by_job_type'])) {
                    $where.= " AND candidate_details.ud_job=" . $default_data['search_by_job_type'] . " ";
                }

                if (isset($default_data['search_by_activity_area']) && !empty($default_data['search_by_activity_area'])) {
                    $where.= " AND candidate_details.ud_activity_area IN (" . implode(',', $default_data['search_by_activity_area']) . ") ";
                }

                if (isset($default_data['search_by_work_place']) && !empty($default_data['search_by_work_place'])) {
                    $where.= " AND candidate_details.ud_work_place IN (" . implode(',', $default_data['search_by_work_place']) . ") ";
                }
            }


            $per_page_data = DEFAULT_RESUME_LIMIT;
            $page = $default_data['request_page'];
            if (!empty($page) && is_numeric($page)) {
                $start = $per_page_data * ($page - 1);
                $page++;
            } else {
                $page = 1;
                $start = 0;
            }

            $start = 0;

            $limit = $start . ", " . $per_page_data * $page;

            $query = "  SELECT 
                            " . $columns . " " . $candidate_skills_query . " 
                        FROM 
                            " . TBL_USERS . " as `recruiter` 
                        RIGHT JOIN 
                            " . TBL_COMPANY_DETAILS . " as `recruiter_details` 
                        ON 
                            `recruiter_details`.`cd_user_id` = `recruiter`.`u_id` and `recruiter_details`.`cd_status` != 9 
                        RIGHT JOIN 
                            " . TBL_USER_APPLICATIONS . " as `candidate_application` 
                        ON 
                            FIND_IN_SET(`recruiter`.`u_id`, `candidate_application`.`ua_company_id`) and `candidate_application`.`ua_status` != 9 
                        RIGHT JOIN 
                            " . TBL_USERS . " as `candidate` 
                        ON 
                            `candidate_application`.`ua_user_id` = `candidate`.`u_id` and `candidate`.`u_status` != 9 
                        RIGHT JOIN 
                            " . TBL_COMPANY_FAVOURITES . " as `recruiter_favourite` 
                        ON 
                            `recruiter_favourite`.`cf_user_id` = `candidate`.`u_id` and `recruiter_favourite`.`cf_company_id` = `recruiter`.`u_id` and `recruiter_favourite`.`cf_status` = 1
                        RIGHT JOIN 
                            " . TBL_USER_DETAILS . " as `candidate_details` 
                        ON 
                            `candidate_details`.`ud_user_id` = `candidate`.`u_id` and `candidate_details`.`ud_status` != 9 
                        RIGHT JOIN 
                            " . TBL_USER_DOCUMENT_FILES . " as `candidate_document_files` 
                        ON 
                            `candidate_document_files`.`udf_user_id` = `candidate`.`u_id` and `candidate_document_files`.`udf_status` != 9 
                        LEFT JOIN 
                            " . TBL_WORK_JOB . " as `candidate_work_job` 
                        ON 
                            `candidate_work_job`.`wj_id` = `candidate_details`.`ud_job` and `candidate_work_job`.`wj_status` != 9 
                        WHERE 
                                " . $where . "
                        GROUP BY 
                                `candidate_application`.`ua_user_id`
                        ORDER BY 
                                `candidate_application`.`ua_id` DESC ";



            $user_data_count = $this->Common_model->get_count_by_query($query);

            $query.=" LIMIT " . $limit;
            $user_data = $this->Common_model->get_all_rows_by_query($query);

            if (!empty($user_data) && is_array($user_data)) {
                $user_data = $this->get_all_users_experience($user_data);
            }
            $user_application['user_application'] = $user_data;
            $user_application['total_count'] = (int) $user_data_count;
            $user_application['total_pages'] = (int) ceil($user_data_count / $per_page_data);
            $user_application['next_page'] = (int) $page;
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(HOME_PATH);
        }
        return $user_application;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for add cv to favourite
     * 
     * Created date : 2017-04-17
     */
    public function add_to_favourite() {

        $user_id = trim($this->input->post("user_id"));

        $user_data = array(
            "cf_user_id" => $user_id,
            "cf_company_id" => $this->user_id,
            "cf_created_date" => $this->utc_time
        );
        $response_id = $this->Common_model->insert(TBL_COMPANY_FAVOURITES, $user_data);
        if ($response_id > 0) {
            return $response_id;
        } else {
            return FALSE;
        }
        return FALSE;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for remove cv from favourite
     * 
     * Created date : 2017-04-17
     */
    public function remove_from_favourite() {

        $user_id = trim($this->input->post("user_id"));

        $user_data = array(
            "cf_status" => 9,
            "cf_modified_date" => time()
        );

        $where_data = array(
            "cf_user_id" => $user_id,
            "cf_company_id" => $this->user_id
        );

        $response_id = $this->Common_model->update(TBL_COMPANY_FAVOURITES, $user_data, $where_data);
        if ($response_id > 0) {
            return $response_id;
        } else {
            return FALSE;
        }
        return FALSE;
    }

}
