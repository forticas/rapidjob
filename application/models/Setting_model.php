<?php

/**
 * @author Nitinkumar Vaghani
 * Modified Date :- 2017-05-09 
 */
class Setting_model extends MY_Model {

    protected $setting_table;
    protected $table_field;
    protected $task_difficulty_table;

    public function __construct() {
        parent::__construct();
        $this->setting_table = TBL_SETTINGS;
    }

    /**
     * This function get all the reports data with pagination and filter
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-09
     * 
     * @param type $lang
     * @param type $request_data
     * @return type
     */
    public function get_settings() {

        $where = array(
            's_id' => 1
        );

        $settingData = $this->get_single_row($this->setting_table, "*", $where);
        $settingArray = array();

        if (!empty($settingData)) {
            $settingArray = unserialize($settingData['s_description']);
        }

        return $settingArray;
    }

}
