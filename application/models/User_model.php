<?php

/**
 * 
 * This model use for do user related all database activity
 * 
 * @author Dipesh Shihora
 * 
 * Modified Date :- 2016-09-16
 * 
 */
class User_model extends MY_Model {

    /**
     * Base table name
     * @var string 
     */
    protected $table_name;

    /**
     *
     * Most use table filed list
     * 
     * @var string 
     */
    protected $table_filed;
    protected $log_file;

    public function __construct() {
        parent::__construct();

        $this->table_name = TBL_USERS;
        $this->table_filed = "u_id,CONCAT(if(u_first_name is null ,'',u_first_name),' ',if(u_last_name is null ,'',u_last_name)) as u_name,u_email,u_status,u_created_date";
        $this->log_file = DOCROOT . 'logs/log_' . date('d-m-Y') . '.txt';
    }

    /**
     * 
     * This function help you to get all user data with filter and pagination
     * 
     * @param array $data
     * @return array
     */
    public function get_all_users($request_data = array()) {
        $return_data = array(
            "data" => array(),
            "total" => 0
        );

        $where = array();
        if (!empty($request_data['name'])) {
            $where["CONCAT(u_first_name,' ',u_last_name) LIKE "] = "%" . $request_data['name'] . "%";
        }
        if (!empty($request_data['email'])) {
            $where['u_email LIKE '] = "%" . $request_data['email'] . "%";
        }
        if (!empty($request_data['status'])) {
            $where['u_status'] = $request_data['status'];
        } else {
            $where['u_status !='] = 9;
        }

        //lets count total number of recoreds
        $record_count = $this->get_count($this->table_name, $this->table_filed, $where);

        $total_page = ceil($record_count / $request_data['per_page']);

        if ($record_count > 0) {

            $order_by = array(
                $request_data['sort_key'] => $request_data['sort_order']
            );

            $limit = array(
                (($request_data['page'] - 1) * $request_data['per_page']),
                $request_data['per_page']
            );

            $record_data = $this->get_all_rows($this->table_name, $this->table_filed, $where, array(), $order_by, $limit);

            $return_data['data'] = $record_data;
        }

        $return_data['total'] = $total_page;

        return $return_data;
    }

    /**
     * 
     * This function help you to register new user
     * 
     * @param array $request_data
     * @return integer
     */
    public function add_user($request_data) {
        $ci_instance = & get_instance();
        $return_data = 0;
        $where = array(
            "LOWER(u_email)" => strtolower($request_data['u_email'])
        );
        /* check email exists or not */
        $user_data = $this->get_single_row(TBL_USERS, "u_id", $where);
        if (!empty($user_data)) {
            $ci_instance->form_validation->set_rules('email', "email", "exist");
            $ci_instance->form_validation->set_message('exist', 'This email already registered.');
            $ci_instance->form_validation->run();
        } else {
            $return_data = $this->insert($this->table_name, $request_data);
        }
        return $return_data;
    }

    /**
     * 
     * This function help you to update user information
     * 
     * @param array $request_data
     * @return integer
     */
    public function edit_user($request_data) {
        $ci_instance = & get_instance();
        $return_data = 0;
        $where = array(
            "LOWER(u_email)" => strtolower($request_data['email']),
            "u_id !=" => $request_data['user_id']
        );
        /* check email exists or not */
        $user_data = $this->get_single_row(TBL_USERS, "u_id", $where);
        if (!empty($user_data)) {
            $ci_instance->form_validation->set_rules('email', "email", "exist");
            $ci_instance->form_validation->set_message('exist', 'This email already registered.');
            $ci_instance->form_validation->run();
        } else {
            $insert_data = array();
            $insert_data['u_first_name'] = $request_data['first_name'];
            $insert_data['u_last_name'] = $request_data['last_name'];
            $insert_data['u_email'] = $request_data['email'];
            if (!empty($request_data['password'])) {
                $insert_data['u_password'] = password_hash(sha1($request_data['password']), PASSWORD_BCRYPT);
            }
            $insert_data['u_updated_date'] = time();

            $where = array();
            $where['u_id'] = $request_data['user_id'];

            $return_data = $this->update($this->table_name, $insert_data, $where);
        }
        return $return_data;
    }

    /**
     * 
     * @param integer $user_id
     * @return array
     */
    public function get_user_data($user_id) {
        $where = array();
        $where['u_id'] = $user_id;

        $user_data = $this->get_single_row($this->table_name, "*", $where);

        return $user_data;
    }

    /**
     * Description: Use this function for display my orders
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-05-22
     * 
     * @param type $user_id
     * @return type
     */
    public function get_user_order_details($user_id = '') {
        $user_data = array();
        if (!empty($user_id) && is_numeric($user_id)) {

            $column_order = "up_id,up_user_id,up_amount,up_subscription_number,up_payment_by,up_created_date,up_status";
            $where_order = array(
                'up_user_id' => $user_id,
                'up_status !=' => 9
            );
            $user_data = $this->get_all_rows(TBL_USER_PAYMENTS, $column_order, $where_order);
        }
        return $user_data;
    }

    public function get_already_sent_company_list($user_id) {
        $response = false;
        if (!empty($user_id) && is_numeric($user_id)) {
            $get_already_sent = "SELECT 
                                        CONCAT(if(ua_company_id is null ,'',ua_company_id),'',if(ua_sent_post_company_id is null ,'',ua_sent_post_company_id)) as sent_company_list
                                 FROM 
                                    " . TBL_USER_APPLICATIONS . " 
                                 WHERE 
                                     ua_created_date < " . strtotime('+3 month', time()) . " 
                                 AND 
                                     ua_user_id=" . $user_id;

            file_put_contents($this->log_file, "\n\n ========get already sent company_list query: " . $get_already_sent . "===== \n\n", FILE_APPEND | LOCK_EX);

            $get_already_data = $this->Common_model->get_all_rows_by_query($get_already_sent);

            file_put_contents($this->log_file, "\n\n ========get_already_data : " . json_encode($get_already_data) . "===== \n\n", FILE_APPEND | LOCK_EX);

            $sent_company_list = array();
            if (!empty($get_already_data)) {
                foreach ($get_already_data as $key => $value) {
                    if (!empty($value['sent_company_list'])) {
                        $get_arr = explode(',', $value['sent_company_list']);
                        $new_arr = array_filter($get_arr);
                        $sent_company_list = array_merge($sent_company_list, $new_arr);
                    }
                }
            }

            if (!empty($sent_company_list)) {
                $response = rtrim(str_replace(array(" ,", ", ", ",,"), ',', implode(',', array_unique($sent_company_list, SORT_NUMERIC))), ',');
            }
            file_put_contents($this->log_file, "\n\n ========get already sent company_list data: " . $response . "===== \n\n", FILE_APPEND | LOCK_EX);
        }
        return $response;
    }

    /**
     * Description : Use this function for get company details except selected company
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     *
     * @param type $user_id
     * @param type $user_data
     * @param type $limit
     * @param type $search_query
     * @param type $send_application_limit
     * @return type
     */
    public function get_company_list($user_id = 0, $user_data = array(), $search_query = '', $send_application_limit = 0, $ids_only = false) {
        $data = array();
        if (
                isset($user_data['ud_activity_area']) &&
                !empty($user_data['ud_activity_area']) &&
                !empty($user_id)
        ) {

            $sent_company_list = $this->get_already_sent_company_list($user_id);

            if ($ids_only) {
                $company_columns = " CONCAT(u_id) as company_list ";
            } else {
                $company_columns = " u_id, u_email, CONCAT(if(u_first_name is null ,'',u_first_name),' ',if(u_last_name is null ,'',u_last_name)) as company_name, u_type, u_image, cd_social_reason ";
            }
            $get_company_query = "SELECT 
                                " . $company_columns . " 
                            FROM 
                                " . TBL_USERS . " 
                            JOIN 
                                " . TBL_COMPANY_DETAILS . " 
                            ON 
                                `u_id` = `cd_user_id` AND `cd_status`=1 
                            WHERE 
                                `u_type` = 2 
                            AND 
                                `u_status` = 1 ";

            if (!empty($user_data['udf_blocked_email'])) {
                $get_company_query.= " AND u_email NOT IN('" . str_replace(',', "','", $user_data['udf_blocked_email']) . "') ";
            }

            if (!empty($sent_company_list)) {
                $get_company_query.= " AND u_id NOT IN(" . $sent_company_list . ") ";
            }

            if (!empty($search_query)) {
                $get_company_query.= ' AND MATCH (cd_address,cd_city,cd_state,cd_country,cd_postal_code) AGAINST ("+' . $search_query . '" IN BOOLEAN MODE)';
            }

            $get_company_query.= " AND cd_activity_area IN(" . $user_data['ud_activity_area'] . ") ";

            $get_company_query.= "  ORDER BY ";

            $get_company_query.=" cd_is_prime ASC, cd_plan_expiry_date DESC ";

            if (!empty($send_application_limit) && is_numeric($send_application_limit)) {
                $get_company_query.=" LIMIT 0," . $send_application_limit;
            } else {
                $get_company_query.=" LIMIT 0,5000";
            }

            file_put_contents($this->log_file, "\n\n ========user model get_company_query : " . $get_company_query . "===== \n\n", FILE_APPEND | LOCK_EX);
            $data = $this->Common_model->get_all_rows_by_query($get_company_query);
        }
        return $data;
    }

    /**
     * Description : Use this function for manage user tariff point
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 17-05-2017
     *
     * @param type $id
     * @param type $user_terrif
     * @param type $increase
     * @param type $decrease
     */
    public function manage_users_tariff($id = '', $user_terrif = 0, $increase = false, $decrease = false, $type = 1) {
        if (!empty($id) && is_numeric($id)) {
            $where_user = array(
                "u_id" => $id,
                "u_status !=" => 9
            );

            // get user details  
            $join_array = array(
                TBL_USER_DETAILS => "u_id = ud_user_id "
            );

            $column_user = array("u_id", "ud_points", "ud_send_by_post_points");
            $user_data = $this->Common_model->get_single_row(TBL_USERS, $column_user, $where_user, $join_array, 'LEFT');
            if (!empty($user_data)) {
                $where_update = array(
                    'ud_user_id' => $id,
                    'ud_status !=' => 9
                );
                $request_user_data['ud_modified_date'] = time();
                $request_user_data['ud_subscribe_plan'] = 1;
                if ($type == 1) {
                    if ($increase == true) {
                        $request_user_data['ud_points'] = intval($user_data['ud_points'] + $user_terrif);
                    } else if ($decrease == true) {
                        $request_user_data['ud_points'] = intval($user_data['ud_points'] - $user_terrif);
                    }
                } else if ($type == 2) {
                    if ($increase == true) {
                        $request_user_data['ud_send_by_post_points'] = intval($user_data['ud_send_by_post_points'] + $user_terrif);
                    } else if ($decrease == true) {
                        $request_user_data['ud_send_by_post_points'] = intval($user_data['ud_send_by_post_points'] - $user_terrif);
                    }
                }

                $this->Common_model->update(TBL_USER_DETAILS, $request_user_data, $where_update);
            }
        }
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for add cv to favourite
     * 
     * Created date : 2017-04-17
     */
    public function add_to_favourite() {

        $company_id = trim($this->input->post("user_id"));

        $user_data = array(
            "uf_user_id" => $this->user_id,
            "uf_company_id" => $company_id,
            "uf_created_date" => $this->utc_time
        );
        $response_id = $this->Common_model->insert(TBL_USER_FAVOURITES, $user_data);
        if ($response_id > 0) {
            return $response_id;
        } else {
            return FALSE;
        }
        return FALSE;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for remove cv from favourite
     * 
     * Created date : 2017-04-17
     */
    public function remove_from_favourite() {

        $company_id = trim($this->input->post("user_id"));

        $user_data = array(
            "uf_status" => 9,
            "uf_modified_date" => time()
        );

        $where_data = array(
            "uf_user_id" => $this->user_id,
            "uf_company_id" => $company_id
        );

        $response_id = $this->Common_model->update(TBL_USER_FAVOURITES, $user_data, $where_data);
        if ($response_id > 0) {
            return $response_id;
        } else {
            return FALSE;
        }
        return FALSE;
    }

    /**
     * Description : Use this function for get company details
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     *
     * @param type $user_id
     * @param type $user_data
     * @param type $company_list
     * @param type $limit
     * @param type $search_query
     * @param type $send_application_limit
     * @return type
     */
    public function get_users_company_list($user_id = 0, $user_data = array(), $default_data = array(), $search_query = '', $send_application_limit = 0) {
        $data = array();
        if (
                isset($user_data['ud_activity_area']) &&
                !empty($user_data['ud_activity_area']) &&
                !empty($user_id)
        ) {

            $sent_company_list = $this->get_already_sent_company_list($user_id);

            $company_columns = " u_id, u_email, CONCAT(if(u_first_name is null ,'',u_first_name),' ',if(u_last_name is null ,'',u_last_name)) as company_name, u_type, u_image, cd_social_reason ";

            $get_company_query = "SELECT 
                                " . $company_columns . " 
                            FROM 
                                " . TBL_USERS . " 
                            JOIN 
                                " . TBL_COMPANY_DETAILS . " 
                            ON 
                                `u_id` = `cd_user_id` AND `cd_status`=1 
                            WHERE 
                                `u_type` = 2 
                            AND 
                                `u_status` = 1 ";

            if (!empty($user_data['udf_blocked_email'])) {
                $get_company_query.= " AND u_email NOT IN('" . str_replace(',', "','", $user_data['udf_blocked_email']) . "') ";
            }

            if (!empty($sent_company_list)) {
                $get_company_query.= " AND u_id NOT IN(" . $sent_company_list . ") ";
            }

            if (!empty($search_query)) {
                $get_company_query.= ' AND MATCH (cd_address,cd_city,cd_state,cd_country,cd_postal_code) AGAINST ("+' . $search_query . '" IN BOOLEAN MODE)';
            }

            $get_company_query.= " AND cd_activity_area IN(" . $user_data['ud_activity_area'] . ") ";

            $get_company_query.= " ORDER BY cd_is_prime ASC, cd_plan_expiry_date DESC ";


            $per_page_data = DEFAULT_SHOW_COMPANY_LIMIT;
            $page = $default_data['request_page'];

            if (!empty($page) && is_numeric($page)) {
                $start = $per_page_data * ($page - 1);
                $page++;
            } else {
                $page = 1;
                $start = 0;
            }

            $limit = $start . ", " . $per_page_data * $page;

            $user_data_count = $this->Common_model->get_count_by_query($get_company_query);

            $query.=" LIMIT " . $limit;

            file_put_contents($this->log_file, "\n\n ========user model get_company_query : " . $get_company_query . "===== \n\n", FILE_APPEND | LOCK_EX);
//            echo $get_company_query;exit;
//            ini_set('memory_limit', '8192M');
            $data = $this->Common_model->get_all_rows_by_query($get_company_query);
            $user_application['company_list'] = $data;
            $user_application['total_count'] = (int) $user_data_count;
            $user_application['total_pages'] = (int) ceil($user_data_count / $per_page_data);
            $user_application['next_page'] = (int) $page;
        }
        return $data;
    }

}
