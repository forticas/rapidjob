<?php

/**
 * @author Nitinkumar vaghani
 * 
 * Created date : 2017-04-17
 */
class Payment_model extends MY_Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for store systempay success payment
     * 
     * up_payment_by 1: Systempay
     * up_payment_by 2: paypal
     * 
     * Created date : 2017-05-15
     * `up_payment_by``up_paypal_payment_id``up_paypal_payment_token``up_paypel_payer_id`
     */
    public function store_success_payment() {

        $payment_amount = trim($this->Common_model->escape_data($this->input->post("vads_amount")));
        $payment_total_amount = trim($this->Common_model->escape_data($this->input->post("vads_effective_amount")));
        $payment_auth_result = trim($this->Common_model->escape_data($this->input->post("vads_auth_result")));
        $payment_currency_code = trim($this->Common_model->escape_data($this->input->post("vads_currency")));
        $payment_trans_date = trim($this->Common_model->escape_data($this->input->post("vads_trans_date")));
        $payment_trans_uuid = trim($this->Common_model->escape_data($this->input->post("vads_trans_uuid")));
        $payment_order_id = trim($this->Common_model->escape_data($this->input->post("vads_order_id")));
        $payment_sequence_number = trim($this->Common_model->escape_data($this->input->post("vads_sequence_number")));
        $payment_trans_status = trim($this->Common_model->escape_data($this->input->post("vads_trans_status")));
        $customer_terrif = trim($this->session->userdata('tariff'));
        $_POST['application_send_by'] = $this->session->userdata('application_send_by');
        $_POST['tariff'] = $customer_terrif;
        $_POST['send_application_limit'] = $this->session->userdata('total_send_application');
        $company_list = $this->session->userdata('all_selected_ids');

        if (!empty($company_list)) {
            $_POST['company_list'] = explode(',', $company_list);
        } else {
            $_POST['company_list'] = '';
        }

        $_POST['send_application'] = 'true';

        $request_data = array(
            "up_user_id" => $this->user_id,
            "up_amount" => $payment_amount,
            "up_total_amount" => $payment_total_amount,
            "up_auth_result" => $payment_auth_result,
            "up_currency_code" => $payment_currency_code,
            "up_transaction_date" => $payment_trans_date,
            "up_transaction_uuid" => $payment_trans_uuid,
            "up_order_id" => $payment_order_id,
            "up_subscription_number" => $customer_terrif,
            "up_transaction_status" => $payment_trans_status,
            "up_response_message" => json_encode($this->input->post()),
            "up_status" => 1,
            "up_payment_by" => 1,
            "up_created_date" => $this->utc_time
        );

        $payment_id = $this->Common_model->insert(TBL_USER_PAYMENTS, $request_data);

        if ($payment_id > 0) {
            if ($this->user_type == 1) {
                // increase user tariff limit
                $this->load->model('User_model');
                $this->User_model->manage_users_tariff($this->user_id, $customer_terrif, TRUE, false, $this->session->userdata('application_send_by'));
                return true;
            } else if ($this->user_type == 2) {
                //check invitation_code is valid or not
                $where_user = array(
                    "u_id" => $this->user_id,
                    "u_status !=" => 9
                );

                // get user details  
                $join_array = array(
                    TBL_COMPANY_DETAILS => "u_id = cd_user_id "
                );

                $column_user = "*";
                $user_data = $this->Common_model->get_single_row(TBL_USERS, $column_user, $where_user, $join_array, 'LEFT');

                if (!empty($user_data)) {
                    $where_update = array(
                        'cd_user_id' => $this->user_id,
                        'cd_status !=' => 9
                    );

                    $request_user_data = array(
                        'cd_is_prime' => 1,
                        'cd_subscribe_date' => $this->utc_time,
                        'cd_plan_expiry_date' => strtotime("+" . $customer_terrif . " month", $this->utc_time),
                        'cd_modified_date' => $this->utc_time
                    );
                    $this->Common_model->update(TBL_COMPANY_DETAILS, $request_user_data, $where_update);
                    return true;
                } else {
                    $this->session->set_flashdata("failure", lang('COMMON_USER_DETAILS_NOT_DEFINED'));
                    redirect(PROFESSIONAL_PATH . '/priority_resume');
                }
            }
        } else {
            return FALSE;
        }
        return FALSE;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for store failed payment
     * 
     * Created date : 2017-05-15
     */
    public function store_failed_payment() {

        $payment_amount = trim($this->Common_model->escape_data($this->input->post("vads_amount")));
        $payment_total_amount = trim($this->Common_model->escape_data($this->input->post("vads_effective_amount")));
        $payment_auth_result = trim($this->Common_model->escape_data($this->input->post("vads_auth_result")));
        $payment_currency_code = trim($this->Common_model->escape_data($this->input->post("vads_currency")));
        $payment_trans_date = trim($this->Common_model->escape_data($this->input->post("vads_trans_date")));
        $payment_trans_uuid = trim($this->Common_model->escape_data($this->input->post("vads_trans_uuid")));
        $payment_order_id = $this->input->post("vads_order_id");
        $payment_sequence_number = trim($this->Common_model->escape_data($this->input->post("vads_sequence_number")));
        $payment_trans_status = trim($this->Common_model->escape_data($this->input->post("vads_trans_status")));
        $_POST['application_send_by'] = $this->session->userdata('application_send_by');

        $request_data = array(
            "up_user_id" => $this->user_id,
            "up_amount" => $payment_amount,
            "up_total_amount" => $payment_total_amount,
            "up_auth_result" => $payment_auth_result,
            "up_currency_code" => $payment_currency_code,
            "up_transaction_date" => $payment_trans_date,
            "up_transaction_uuid" => $payment_trans_uuid,
            "up_order_id" => $payment_order_id,
            "up_subscription_number" => $payment_sequence_number,
            "up_transaction_status" => $payment_trans_status,
            "up_response_message" => json_encode($this->input->post()),
            "up_status" => 4,
            "up_payment_by" => 1,
            "up_created_date" => $this->utc_time
        );

        $payment_id = $this->Common_model->insert(TBL_USER_PAYMENTS, $request_data);

        if ($payment_id > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
        return FALSE;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for store paypel success payment
     * 
     * Created date : 2017-05-19
     */
    public function store_paypal_success_payment() {
        $payment_amount = trim($this->session->userdata('amount'));
        $customer_terrif = trim($this->session->userdata('tariff'));

        $payment_id = trim($this->Common_model->escape_data($this->input->get("paymentId")));
        $payment_token = trim($this->Common_model->escape_data($this->input->get("token")));
        $payment_payer_id = trim($this->Common_model->escape_data($this->input->get("PayerID")));
        $_GET['application_send_by'] = $this->session->userdata('application_send_by');
        $_GET['tariff'] = $customer_terrif;
        
        $_POST['application_send_by'] = $this->session->userdata('application_send_by');
        $_POST['send_application_limit'] = $this->session->userdata('total_send_application');
        $company_list = $this->session->userdata('all_selected_ids');

        if (!empty($company_list)) {
            $_POST['company_list'] = explode(',', $company_list);
        } else {
            $_POST['company_list'] = '';
        }

        $_POST['send_application'] = 'true';
        
        $all_response_data = $this->input->get();

        $request_data = array(
            "up_user_id" => $this->user_id,
            "up_amount" => $payment_amount,
            "up_total_amount" => $payment_amount,
            "up_paypal_payment_id" => $payment_id,
            "up_paypal_payment_token" => $payment_token,
            "up_paypel_payer_id" => $payment_payer_id,
            "up_subscription_number" => $customer_terrif,
            "up_response_message" => !empty($all_response_data) ? json_encode($all_response_data) : '',
            "up_status" => 1,
            "up_payment_by" => 2,
            "up_created_date" => $this->utc_time
        );

        $payment_id = $this->Common_model->insert(TBL_USER_PAYMENTS, $request_data);

        if ($payment_id > 0) {
            if ($this->user_type == 1) {

                // increase user tariff limit
                $this->load->model('User_model');
                $this->User_model->manage_users_tariff($this->user_id, $customer_terrif, TRUE, false, $this->session->userdata('application_send_by'));
                return true;
            } else if ($this->user_type == 2) {
                //check invitation_code is valid or not
                $where_user = array(
                    "u_id" => $this->user_id,
                    "u_status !=" => 9
                );

                // get user details  
                $join_array = array(
                    TBL_COMPANY_DETAILS => "u_id = cd_user_id "
                );

                $column_user = "*";
                $user_data = $this->Common_model->get_single_row(TBL_USERS, $column_user, $where_user, $join_array, 'LEFT');

                if (!empty($user_data)) {
                    $where_update = array(
                        'cd_user_id' => $this->user_id,
                        'cd_status !=' => 9
                    );

                    $request_user_data = array(
                        'cd_is_prime' => 1,
                        'cd_subscribe_date' => $this->utc_time,
                        'cd_plan_expiry_date' => strtotime("+" . $customer_terrif . " month", $this->utc_time),
                        'cd_modified_date' => $this->utc_time
                    );
                    $this->Common_model->update(TBL_COMPANY_DETAILS, $request_user_data, $where_update);
                    return true;
                } else {
                    $this->session->set_flashdata("failure", lang('COMMON_USER_DETAILS_NOT_DEFINED'));
                    redirect(PROFESSIONAL_PATH . '/priority_resume');
                }
            }
        } else {
            return FALSE;
        }
        return FALSE;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for store paypal failed payment
     * 
     * Created date : 2017-05-15
     */
    public function store_paypal_failed_payment() {
        $customer_terrif = trim($this->session->userdata('tariff'));
        $_GET['application_send_by'] = $this->session->userdata('application_send_by');
        $all_response_data = $this->input->get();
        $request_data = array(
            "up_user_id" => $this->user_id,
            "up_subscription_number" => $customer_terrif,
            "up_response_message" => !empty($all_response_data) ? json_encode($all_response_data) : '',
            "up_status" => 4,
            "up_payment_by" => 2,
            "up_created_date" => $this->utc_time
        );

        $payment_id = $this->Common_model->insert(TBL_USER_PAYMENTS, $request_data);

        if ($payment_id > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
        return FALSE;
    }

}
