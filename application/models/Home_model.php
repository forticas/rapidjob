<?php

/**
 * @author Nitinkumar vaghani
 * 
 * Created date : 2017-04-17
 */
class Home_model extends MY_Model {

    protected $log_file;

    public function __construct() {
        parent::__construct();
        $this->log_file = DOCROOT . 'logs/log_' . date('d-m-Y') . '.txt';
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for register professional users only
     * 
     * Created date : 2017-04-17
     */
    public function professional_register() {
        if (!$this->input->is_ajax_request()) {
            redirect(HOME_PATH);
        }
        try {

            $first_name = trim($this->input->post("first_name"));
            $last_name = trim($this->input->post("last_name"));
            $email = trim($this->input->post("email"));
            $password = trim($this->input->post("password"));
            $social_reason = trim($this->input->post("social_reason"));
            $activity_area = $this->input->post("activity_area");
            $civility = trim($this->input->post("civility"));

            $ajax_response = array();

            if (
                    empty($first_name) ||
                    empty($last_name) ||
                    empty($email) ||
                    empty($social_reason) ||
                    empty($activity_area) ||
                    empty($civility) ||
                    empty($password)
            ) {
                $ajax_response = array(
                    "success" => FALSE,
                    "message" => lang('COMMON_INVALID_PARAMS')
                );
            } else {

                $where = array(
                    "LOWER(u_email)" => strtolower($email),
                    "u_status !=" => 9,
                );

                $check_user = $this->Common_model->get_single_row(TBL_USERS, "u_id", $where);
                if (!empty($check_user)) {
                    $ajax_response = array(
                        "success" => FALSE,
                        "message" => lang('COMMON_EMAIL_ALREADY_REGISTERED')
                    );
                } else {
                    $invitation_code = str_rand_access_token(8);
                    $register_user = array(
                        "u_first_name" => $first_name,
                        "u_last_name" => $last_name,
                        "u_email" => $email,
                        "u_password" => password_hash($password, PASSWORD_BCRYPT),
                        "u_created_date" => $this->utc_time,
                        "u_user_type" => 1,
                        "u_type" => 2,
                        "u_invitation_code" => $invitation_code
                    );

                    $user_id = $this->Common_model->insert(TBL_USERS, $register_user);


                    if ($user_id > 0) {

                        $user_details = array(
                            "cd_user_id" => $user_id,
                            "cd_social_reason" => $social_reason,
                            "cd_activity_area" => implode(',', $activity_area),
                            "cd_gender" => $civility,
                            "cd_created_date" => $this->utc_time
                        );

                        $this->Common_model->insert(TBL_COMPANY_DETAILS, $user_details);

                        $user_session_data = array(
                            "user_id" => $user_id,
                            "user_first_name" => $first_name,
                            "user_last_name" => $last_name,
                            "user_type" => 2,
                            "profile_photo" => DEFAULT_IMAGE_NAME
                        );

                        $this->session->set_flashdata("success", lang('REGISTER_SUCCESS'));
                        $this->session->set_userdata($user_session_data);

                        $ajax_response = array(
                            "success" => TRUE,
                            "message" => lang('REGISTER_SUCCESS'),
                            "url" => HOME_PATH
                        );
                    } else {
                        $ajax_response = array(
                            "success" => FALSE,
                            "message" => lang('REGISTER_FAILURE')
                        );
                    }
                }
            }
        } catch (Exception $e) {
            $ajax_response = array(
                "success" => false,
                "message" => lang("COMMON_PROBLEM_IN_ACTION")
            );
        }
        return $ajax_response;
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for get company list
     * 
     * Created date : 2017-05-03
     */
    public function get_company_list($default_data = array()) {
        $user_application = array();
        try {

            $columns = " recruiter.u_first_name as recruiter_first_name,
                         recruiter.u_last_name as recruiter_last_name, 
                        `recruiter`.`u_id` as `recruiter_id`, 
                        `recruiter`.`u_type` as `recruiter_type`, 
                        `recruiter`.`u_email` as `recruiter_email`, 
                        `recruiter`.`u_image` as `recruiter_image`, 
                        `recruiter`.`u_status` as `recruiter_status`, 
                        `recruiter_details`.cd_id,
                        `recruiter_details`.cd_social_reason,
                        `recruiter_details`.cd_activity_area,
                        `recruiter_details`.cd_search_activity_area ";

            $candidate_favourite_query = " ,(
                                 SELECT 
                                     COUNT(uf_id)
                                 FROM 
                                    " . TBL_USER_FAVOURITES . " as `candidate_favourite` 
                                 WHERE 
                                    `candidate_favourite`.`uf_company_id` = `recruiter`.`u_id` AND candidate_favourite.uf_status=1) as user_favourite ";

            $where = " `recruiter`.`u_type`=2 AND `recruiter`.`u_status` != 9 ";

            if (!empty($default_data)) {
                if (isset($default_data['query']) && !empty($default_data['query'])) {
                    $where.= ' AND MATCH (recruiter_details.cd_address,recruiter_details.cd_city,recruiter_details.cd_state,recruiter_details.cd_country,recruiter_details.cd_postal_code) AGAINST ("+' . $default_data['query'] . '" IN BOOLEAN MODE)';
                }

                if (isset($default_data['search_by_activity_area']) && !empty($default_data['search_by_activity_area'])) {
                    $activity_area = array_filter($default_data['search_by_activity_area']);
                    $where.= " AND recruiter_details.cd_activity_area IN(" . implode(',', $default_data['search_by_activity_area']) . ") ";
                }
            }

            $per_page_data = DEFAULT_SEARCH_COMPANY_LIMIT;
            $page = $default_data['request_page'];
            if (!empty($page) && is_numeric($page)) {
                $start = $per_page_data * ($page - 1);
                $page++;
            } else {
                $page = 1;
                $start = 0;
            }

            $start = 0;

            $limit = $start . ", " . $per_page_data * $page;

            $query = "  SELECT 
                            " . $columns . " " . $candidate_favourite_query . "
                        FROM 
                            " . TBL_USERS . " as `recruiter` 
                        RIGHT JOIN 
                            " . TBL_COMPANY_DETAILS . " as `recruiter_details` 
                        ON 
                            `recruiter_details`.`cd_user_id` = `recruiter`.`u_id` and `recruiter_details`.`cd_status` != 9 
                        WHERE 
                                " . $where . "
                        GROUP BY 
                                `recruiter`.`u_id`
                        ORDER BY 
                                recruiter_details.cd_is_prime ASC, recruiter_details.cd_plan_expiry_date DESC ";

            $user_data_count = $this->Common_model->get_count_by_query($query);

            $query.=" LIMIT " . $limit;

            $user_data = $this->Common_model->get_all_rows_by_query($query);

            $user_application['user_application'] = $user_data;
            $user_application['total_count'] = (int) $user_data_count;
            $user_application['total_pages'] = (int) ceil($user_data_count / $per_page_data);
            $user_application['next_page'] = (int) $page;
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(HOME_PATH);
        }
        return $user_application;
    }

    /**
     * Description :- This function is used to get the homepage content.
     * 
     * @author Hardik Patel
     * 
     * Modified Date :- 2017-05-26
     * 
     * @return type
     */
    public function get_cms_content() {

        $column_slider = array(
            'cms_id',
            'cms_home_banner_image',
            'cms_footer_contactus',
            'cms_footer_facebook_link',
            'cms_footer_twitter_link',
            'cms_footer_google_link',
            'cms_footer_linkedin_link',
            'cms_prof_banner_image',
            'cms_home_banner_content_' . $this->current_lang . ' as banner_content',
            'cms_hm_create_account_text_' . $this->current_lang . ' as create_account_text',
            'cms_home_news_title_' . $this->current_lang . ' as news_title',
            'cms_home_subscribe_text_' . $this->current_lang . ' as subscribe_text',
            'cms_footer_copyright_' . $this->current_lang . ' as footer_copyright',
            'cms_prof_banner_title_' . $this->current_lang . ' as prof_banner_title',
            'cms_prof_cv_title_' . $this->current_lang . ' as prof_cv_title',
            'cms_prof_broadcast_' . $this->current_lang . ' as prof_broadcast',
            'cms_prof_mode_' . $this->current_lang . ' as prof_mode',
            'cms_featured_one_title_' . $this->current_lang . ' as featured_one_title',
            'cms_featured_one_description_' . $this->current_lang . ' as featured_one_description',
            'cms_featured_second_title_' . $this->current_lang . ' as featured_second_title',
            'cms_featured_second_description_' . $this->current_lang . ' as featured_second_description',
            'cms_featured_third_title_' . $this->current_lang . ' as featured_third_title',
            'cms_featured_third_description_' . $this->current_lang . ' as featured_third_description',
        );

        $get_data = $this->get_single_row(TBL_CMS, $column_slider, array('cms_id' => 1));
        return $get_data;
    }

    /**
     * Description :- This function is used to get the homepage testimonial.
     * 
     * @author Hardik Patel
     * 
     * Modified Date :- 2017-05-26
     * 
     * @return type
     */
    public function get_all_testimonials() {

        $column = array(
            'cms_t_id',
            'cms_t_image',
            'cms_t_title_' . $this->current_lang . ' as title',
            'cms_t_description_' . $this->current_lang . ' as description',
        );
        $where = array(
            'cms_t_status' => 1
        );

        $order_by = array('cms_t_created_date' => 'DESC');

        $testimonial_data = $this->get_all_rows(TBL_TESTIMONIAL, $column, $where, array(), $order_by);

        return $testimonial_data;
    }

}
