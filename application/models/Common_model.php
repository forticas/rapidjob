<?php

class Common_model extends MY_Model {

    function __construct() {
        parent::__construct();
    }

    /**
     * Description : Use this function for get all details of user
     * @author Nitinkumar vaghani
     * 
     * Last modified date : 19-01-2017
     * 
     * @param type $id => Candidate ID
     * @param type $company_id => recruiter ID
     * @return type
     */
    public function get_user_all_details($id, $company_id = '') {
        $data = array();
        if (!empty($id) && is_numeric($id)) {
            // get user details with job and documents
            $join_array = array(
                TBL_USER_DETAILS => "u_id = ud_user_id AND ud_status=1",
                TBL_WORK_JOB => "wj_id = ud_job AND wj_status=1",
                TBL_USER_DOCUMENT_FILES => "udf_user_id = u_id AND udf_status=1",
            );

            $where_user = array(
                'u_id' => $id,
                'u_status' => 1
            );

            // select user languages
            $column_language = array('ul_id', 'ul_language');
            $where_language = array('ul_status' => 1, 'ul_user_id' => $id);
            $data['user_languages'] = $this->Common_model->get_all_rows(TBL_USER_LANGUAGES, $column_language, $where_language);


            $column_user = "*, CONCAT(if(u_first_name is null ,'',u_first_name),' ',if(u_last_name is null ,'',u_last_name)) as user_name";
            $data['user_data'] = $this->Common_model->get_single_row(TBL_USERS, $column_user, $where_user, $join_array, 'LEFT');

            // get user experiences
            $where_exp = array(
                'ue_user_id' => $id,
                'ue_status' => 1
            );

            $join_exp_array = array(
                TBL_WORK_JOB => "wj_id = ue_job AND wj_status=1",
            );


            $column_user_exp = "*";
            $data['user_exp_data'] = $this->Common_model->get_all_rows(TBL_USER_EXPERIANCES, $column_user_exp, $where_exp, $join_exp_array, 'LEFT');

            // get user training
            $where_training = array(
                'ut_user_id' => $id,
                'ut_status' => 1
            );

            $column_user_training = "*";
            $data['user_training_data'] = $this->Common_model->get_all_rows(TBL_USER_TRAININGS, $column_user_training, $where_training);

            // get user skill
            $where_skill = array(
                'us_user_id' => $id,
                'us_status' => 1
            );

            $column_user_skill = "*";
            $data['user_skill_data'] = $this->Common_model->get_all_rows(TBL_USER_SKILLS, $column_user_skill, $where_skill);

            // get user hobbies
            $where_hobbies = array(
                'uh_user_id' => $id,
                'uh_status' => 1
            );

            $column_user_hobbies = "*";
            $data['user_hobbies_data'] = $this->Common_model->get_all_rows(TBL_USER_HOBBIES, $column_user_hobbies, $where_hobbies);


            if (!empty($company_id) && is_numeric($company_id)) {
                // get favourite user
                $where_favourite = array(
                    'cf_user_id' => $id,
                    'cf_company_id' => $company_id,
                    'cf_status' => 1
                );

                $column_user_favourite = 'COUNT(cf_id) as user_is_favourite';
                $user_favourite = $this->Common_model->get_single_row(TBL_COMPANY_FAVOURITES, $column_user_favourite, $where_favourite);
                $data['user_favourite'] = 0;

                if (!empty($user_favourite) && $user_favourite['user_is_favourite'] > 0) {
                    $data['user_favourite'] = $user_favourite['user_is_favourite'];
                }

                // get user application
                $application_query = "SELECT 
                                            * 
                                      FROM 
                                            " . TBL_USER_APPLICATIONS . " 
                                      WHERE 
                                            ua_user_id=" . $id . " 
                                      AND 
                                            ua_company_id IN(" . $company_id . ") 
                                      AND 
                                            ua_status=1 
                                      ORDER BY 
                                            ua_id DESC";
                $data['user_application'] = $this->Common_model->get_single_row_by_query($application_query);

                // get user document files
                $document_query = "SELECT 
                                            * 
                                      FROM 
                                            " . TBL_USER_DOCUMENT_FILES . " 
                                      WHERE 
                                            udf_user_id=" . $id . " 
                                      AND 
                                            udf_status=1 
                                      ORDER BY 
                                            udf_id DESC";
                $data['user_documents'] = $this->Common_model->get_single_row_by_query($document_query);
            }
        }

        return $data;
    }

    /**
     * Description : Use this function for get work job, activity area, work places and level of education
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     * 
     * @return type
     */
    public function get_default_details() {
        $data = array();
        // select user your job type
        $column_job = array('wj_id', 'wj_name_' . $this->current_lang . ' as job_name');
        $where_job = array('wj_status' => 1);
        $jobtype_orderby = array(
            'wj_name_' . $this->current_lang => 'ASC'
        );
        $data['work_job'] = $this->Common_model->get_all_rows(TBL_WORK_JOB, $column_job, $where_job, array(), $jobtype_orderby);

        // select user activity area
        $column_activity_area = array('wa_id', 'wa_name_' . $this->current_lang . ' as area_name');
        $where_activity_area = array('wa_status' => 1);
        $activity_orderby = array(
            'wa_name_' . $this->current_lang => 'ASC'
        );
        $data['activity_area'] = $this->Common_model->get_all_rows(TBL_WORK_ACTIVITY_AREA, $column_activity_area, $where_activity_area, array(), $activity_orderby);

        // select user work places
        $column_work_places = array('wp_id', 'wp_name');
        $where_work_places = array('wp_status' => 1);
        $workplace = array(
            'wp_name' => 'ASC'
        );
        $data['work_places'] = $this->Common_model->get_all_rows(TBL_WORK_PLACES, $column_work_places, $where_work_places, array(), $workplace);

        // select user educations
        $column_educations = array('ed_id', 'ed_name_' . $this->current_lang . ' as ed_name');
        $where_educations = array('ed_status' => 1);
        $education_orderby = array(
            'ed_name_' . $this->current_lang => 'ASC'
        );
        $data['level_of_educations'] = $this->Common_model->get_all_rows(TBL_EDUCATIONS, $column_educations, $where_educations, array(), $education_orderby);

        return $data;
    }

    /**
     * Description : Use this function for get all activity area
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 09-06-2017
     * 
     * @return type
     */
    public function get_all_activity_areas() {
        $data = array();

        // select user activity area
        $column_activity_area = array('wa_id', 'wa_name_' . $this->current_lang . ' as area_name');
        $where_activity_area = array('wa_status' => 1);
        $activity_orderby = array(
            'wa_name_' . $this->current_lang => 'ASC'
        );
        $data = $this->Common_model->get_all_rows(TBL_WORK_ACTIVITY_AREA, $column_activity_area, $where_activity_area, array(), $activity_orderby);

        return $data;
    }

    /**
     * Description : Use this function for get nationalities
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     * 
     * @return type
     */
    public function get_nationalities() {
        $data = array();
        $column_nationality = array('n_id', 'n_code', 'n_name_' . $this->current_lang . ' as n_name');
        $where_nationality = array('n_status' => 1);
        $orderby_nationality = array(
            'n_name_' . $this->current_lang => 'ASC'
        );
        $data['nationalities'] = $this->Common_model->get_all_rows(TBL_NATIONALITIES, $column_nationality, $where_nationality, array(), $orderby_nationality);

        return $data;
    }

    /**
     * Description : Use this function for get user documents detail only
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     * 
     * @param type $doc_id
     * @param type $user_id
     * @return type
     */
    public function get_user_document_details($doc_id = 0, $user_id = 0) {
        $data = array();

        if (!empty($doc_id) && is_numeric($doc_id) || !empty($user_id) && is_numeric($user_id)) {
            if (!empty($doc_id) && is_numeric($doc_id)) {
                $where_user = array(
                    'udf_id' => $doc_id,
                    'udf_status' => 1,
                );
                $column_user = "*";
                $data = $this->Common_model->get_single_row(TBL_USER_DOCUMENT_FILES, $column_user, $where_user);
            } else if (!empty($user_id) && is_numeric($user_id)) {
                $where_user = array(
                    'udf_user_id' => $user_id,
                    'udf_status' => 1,
                );

                $join_user = array(TBL_USERS => 'u_id=udf_user_id AND u_status=1');

                $column_user = "*";
                $data = $this->Common_model->get_single_row(TBL_USER_DOCUMENT_FILES, $column_user, $where_user, $join_user, 'RIGHT');
            }
        }
        return $data;
    }

    /**
     * Description : Use this function for get user details with work and documents files.
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     * 
     * @param type $id
     * @return type
     */
    public function get_user_details($id = 0) {
        $data = array();
        $join_array = array(
            TBL_USER_DETAILS => "u_id = ud_user_id AND ud_status=1",
            TBL_WORK_JOB => "wj_id = ud_job AND wj_status=1",
            TBL_USER_DOCUMENT_FILES => "udf_user_id = u_id AND udf_status=1",
        );

        if (!empty($id) && is_numeric($id)) {
            $where_user = array(
                'u_id' => $id,
                'u_status' => 1,
            );
        } else {
            $where_user = array(
                'u_id' => $this->user_id,
                'u_status' => 1,
            );
        }

        $column_user = "*, CONCAT(if(u_first_name is null ,'',u_first_name),' ',if(u_last_name is null ,'',u_last_name)) as user_name";
        $data = $this->Common_model->get_single_row(TBL_USERS, $column_user, $where_user, $join_array, 'LEFT');

        return $data;
    }

    /**
     * Description : Use this function for get user details with work and documents files.
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     * 
     * 
     * @param type $id => Candidate ID
     * @param type $doc_id => uadf_id
     * @return type
     */
    public function get_user_all_documents($id = 0, $doc_id = 0) {
        $data = array();

        $join_array = array(
            TBL_USER_ALL_DOCUMENT_FILES => "uadf_user_id = u_id AND uadf_status=1",
        );

        $where_user['u_status'] = 1;

        if (!empty($id) && is_numeric($id)) {
            $where_user['u_id'] = $id;
        } else {
            $where_user['u_id'] = $this->user_id;
        }

        if (!empty($doc_id) && is_numeric($doc_id)) {
            $where_user['uadf_id'] = $doc_id;
        }

        $column_user = "*";
        $data = $this->Common_model->get_all_rows(TBL_USERS, $column_user, $where_user, $join_array, 'RIGHT');

        return $data;
    }

    /**
     * Description : Use this function for get company details selected by user
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     *
     * @param type $request_data
     * @return type
     */
    public function get_company_list_by_priority($request_data = NULL) {
        $data = array();
        // get company details
        $where = '';
        if ($request_data) {
            $where .=" AND u_id IN(" . implode(',', $request_data) . ") ";
        }

        $get_company_query = "SELECT 
                                    u_id, u_email, CONCAT(if(u_first_name is null ,'',u_first_name),' ',if(u_last_name is null ,'',u_last_name)) as company_name
                              FROM 
                                    " . TBL_USERS . " LEFT JOIN " . TBL_COMPANY_DETAILS . " 
                              ON 
                                    u_id = cd_user_id WHERE u_status=1 AND u_type=2 " . $where;
        $data = $this->get_all_rows_by_query($get_company_query);
        return $data;
    }

    /**
     * Description : Use this function for get company details except selected company
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     *
     * @param type $request_data
     * @return type
     */
    public function get_company_list($request_data = NULL) {
        $data = array();
        // get company details
        $where = '';
        if (!empty($request_data)) {
            $where .=" AND u_id NOT IN(" . implode(',', $request_data) . ") ";
        }

        $get_company_query = "SELECT 
                                    u_id, u_email, CONCAT(if(u_first_name is null ,'',u_first_name),' ',if(u_last_name is null ,'',u_last_name)) as company_name 
                              FROM 
                                    " . TBL_USERS . " LEFT JOIN " . TBL_COMPANY_DETAILS . " 
                              ON 
                                    u_id = cd_user_id WHERE u_status=1 AND u_type=2 " . $where;
        $data = $this->get_all_rows_by_query($get_company_query);
        return $data;
    }

    /**
     * Description : Use this function for get user workplaces
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     * 
     * @param type $request_data
     * @return type
     */
    public function get_user_workplaces($request_data = NULL) {
        $data = array();
        // get workplaces
        $where = '';
        if ($request_data) {
            $where .=" AND wp_id IN(" . implode(',', $request_data) . ") ";
        }

        $get_workplaces_query = "SELECT 
                                     GROUP_CONCAT(wp_name) AS workplace_name
                                 FROM 
                                     " . TBL_WORK_PLACES . " 
                                 WHERE
                                     wp_status=1 " . $where;
        $data = $this->get_single_row_by_query($get_workplaces_query);

        return $data;
    }

    /**
     * Description : Use this function for get user activities
     * 
     * @author Nitinkumar vaghani
     * Last modified date : 19-01-2017
     * 
     * @param type $request_data
     * @return type
     */
    public function get_user_activities($request_data = NULL) {
        $data = array();
        // get activity
        $where = '';
        if ($request_data) {
            $where .=" AND wa_id IN(" . implode(',', $request_data) . ") ";
        }

        $get_workplaces_query = "SELECT 
                                    GROUP_CONCAT(wa_name_" . $this->current_lang . ") AS activity_name
                                 FROM 
                                    " . TBL_WORK_ACTIVITY_AREA . " 
                                 WHERE 
                                    wa_status=1 " . $where;
        $data = $this->get_single_row_by_query($get_workplaces_query);
        return $data;
    }

    /**
     * 
     * This function use for get job id by name or insert new job
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-12
     * 
     * @param type $job_type
     * @return type
     */
    public function manage_job($job_type) {
        $response_id = '';
        if (!empty($job_type)) {

            // check job exist or not
            $job_query = ' SELECT 
                                    `wj_id` 
                                FROM 
                                    ' . TBL_WORK_JOB . '
                                WHERE 
                                    wj_status != 9 
                                AND 
                                    (LOWER(wj_name_en)="' . strtolower($job_type) . '" OR LOWER(wj_name_fr)="' . strtolower($job_type) . '") ';

            $job_data = $this->get_single_row_by_query($job_query);

            if (!empty($job_data) && isset($job_data['wj_id']) && is_numeric($job_data['wj_id'])) {
                $response_id = $job_data['wj_id'];
            } else {
                // insert new job
                $job_data_params = array(
                    'wj_name_en' => $job_type,
                    'wj_name_fr' => $job_type,
                    'wj_created_date' => time()
                );
                $response_id = $this->insert(TBL_WORK_JOB, $job_data_params);
            }
        }

        return $response_id;
    }

    /**
     * 
     * This function use for get job id by name or insert new job
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-12
     * 
     * @param type $education_title
     * @return type
     */
    public function manage_education($education_title) {
        $response_id = '';
        if (!empty($education_title)) {
            // check education exist or not
            $education_query = ' SELECT 
                                    `ed_id` 
                                FROM 
                                    ' . TBL_EDUCATIONS . '
                                WHERE 
                                    ed_status != 9 
                                AND 
                                    (LOWER(ed_name_en)="' . strtolower($education_title) . '" OR LOWER(ed_name_fr)="' . strtolower($education_title) . '") ';

            $education_data = $this->get_single_row_by_query($education_query);
            if (!empty($education_data) && isset($education_data['ed_id']) && is_numeric($education_data['ed_id'])) {
                $response_id = $education_data['ed_id'];
            } else {
                // insert new education
                $education_data_params = array(
                    'ed_name_en' => $education_title,
                    'ed_name_fr' => $education_title,
                    'ed_created_date' => time()
                );
                $response_id = $this->insert(TBL_EDUCATIONS, $education_data_params);
            }
        }
        return $response_id;
    }

    /**
     * 
     * This function use for get area id by name or insert new area
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-12
     * 
     * @param type $activity_area_type
     * @return type
     */
    public function manage_activity_area($activity_area_type) {
        $response_id = '';
        if (!empty($activity_area_type)) {

            // check activity_area exist or not
            $activity_area_query = ' SELECT 
                                    `wa_id` 
                                FROM 
                                    ' . TBL_WORK_ACTIVITY_AREA . '
                                WHERE 
                                    wa_status != 9 
                                AND 
                                    (LOWER(wa_name_en)="' . strtolower($activity_area_type) . '" OR LOWER(wa_name_fr)="' . strtolower($activity_area_type) . '") ';

            $activity_area_data = $this->get_single_row_by_query($activity_area_query);

            if (!empty($activity_area_data) && isset($activity_area_data['wa_id']) && is_numeric($activity_area_data['wa_id'])) {
                $response_id = $activity_area_data['wa_id'];
            } else {
                // insert new activity_area
                $activity_area_data_params = array(
                    'wa_name_en' => $activity_area_type,
                    'wa_name_fr' => $activity_area_type,
                    'wa_created_date' => time()
                );
                $response_id = $this->insert(TBL_WORK_ACTIVITY_AREA, $activity_area_data_params);
            }
        }

        return $response_id;
    }

    /**
     * 
     * This function use for get area id by name or insert new area
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-12
     * 
     * @param type $workplace_title
     * @return type
     */
    public function manage_workplace($workplace_title) {
        $response_id = '';
        if (!empty($workplace_title)) {
            // check workplace exist or not
            $workplace_query = 'SELECT 
                                        `wp_id` 
                                    FROM 
                                        ' . TBL_WORK_PLACES . '
                                    WHERE 
                                        wp_status != 9 
                                    AND 
                                        LOWER(wp_name)="' . strtolower($workplace_title) . '" ';

            $workplace_data = $this->get_single_row_by_query($workplace_query);
            if (!empty($workplace_data) && isset($workplace_data['wp_id']) && is_numeric($workplace_data['wp_id'])) {
                $response_id = $workplace_data['wp_id'];
            } else {
                // insert new workplace
                $workplace_data_params = array(
                    'wp_name' => $workplace_title,
                    'wp_created_date' => time()
                );
                $response_id = $this->insert(TBL_WORK_PLACES, $workplace_data_params);
            }
        }
        return $response_id;
    }

    /**
     * Description : Use this function for get news 
     * 
     * @author Hardik Patel
     * Last modified date : 12-04-2017
     * 
     * @return type
     */
    public function get_news_details() {

        $data = array();
        $column_news = array(
            'news_id',
            'news_name_' . $this->current_lang . ' as news_name',
            'news_description_' . $this->current_lang . ' as news_description',
            'news_image_name'
        );

        $where_news = array('news_status' => 1);
        $order_by = array('news_created_date' => 'DESC');
        $limit = '4,0';
        $response_news = $this->get_all_rows(TBL_NEWS, $column_news, $where_news, array(), $order_by, $limit);

        return $response_news;
    }

    /**
     * 
     * This function use for get company details from application table
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-06-15
     * 
     * @param type $id
     * @param type $application_id
     */
    public function get_my_application($id = 0, $application_id = 0) {
        $return_data = array();
        if (!empty($id) && is_numeric($application_id) && !empty($application_id) && is_numeric($id)) {
            $columns = " CONCAT(if(recruiter.u_first_name is null ,'',recruiter.u_first_name),' ',if(recruiter.u_last_name is null ,'',recruiter.u_last_name)) as company_name,
                        `recruiter`.`u_id` as `recruiter_id`, 
                        `recruiter`.`u_email` as `recruiter_email` ";

            $application_query = "SELECT 
                                " . $columns . " 
                            FROM 
                                " . TBL_USERS . " as `recruiter` 
                            RIGHT JOIN 
                                " . TBL_COMPANY_DETAILS . " as `recruiter_details` 
                            ON 
                                `recruiter_details`.`cd_user_id` = `recruiter`.`u_id` and `recruiter_details`.`cd_status` != 9 
                            RIGHT JOIN 
                                " . TBL_USER_APPLICATIONS . " as `candidate_application` 
                            ON 
                                FIND_IN_SET(recruiter.u_id, candidate_application.ua_company_id) and candidate_application.ua_status=2 and candidate_application.ua_company_id is not null 
                            RIGHT JOIN 
                                " . TBL_USERS . " as `candidate` 
                            ON 
                                `candidate_application`.`ua_user_id` = `candidate`.`u_id` and `candidate`.`u_status` != 9 
                            WHERE 
                                `candidate_application`.`ua_user_id` = ".$id." 
                            AND 
                                candidate_application.ua_id=".$application_id." 
                            AND
                                candidate_application.ua_send_by=1 
                            AND 
                                candidate_application.ua_status=2 
                            AND 
                                recruiter.u_id is not null ";

            $return_data = $this->Common_model->get_all_rows_by_query($application_query);
        }


        return $return_data;
    }

}
