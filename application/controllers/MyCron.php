<?php

/**
 * Description: Use this controller for background process
 * 
 * @author Nitinkumar Vaghani
 * Last modified date : 2016-01-09
 * 
 */
class MyCron extends MY_Controller {

    public $log_file;

    public function __construct() {
        parent::__construct();
        $this->log_file = DOCROOT . 'logs/log_' . date('d-m-Y') . '.txt';
        
    }

    /**
     * Description: Use this function for send application to all company
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-06-15
     * 
     * @param type $id : CANDIDATE ID
     * @param type $current_lang : en or fr
     * @param type $application_id
     */
    public function send_application_to_company($id, $current_lang = 'fr', $application_id) {
        $data = array();
        file_put_contents($this->log_file, "\n\n ======== Cron Start >__send_application_to_company... Date[" . date('h:i:s a d-m-Y') . "] ===== \n\n", FILE_APPEND | LOCK_EX);
        if (!empty($id) && is_numeric($id) && !empty($application_id) && is_numeric($application_id)) {

            // get current language and if empty then set default fr
            if (!empty($current_lang) && $current_lang == 'en') {
                $subject = APP_NAME . " : New job application mail";
            } else {
                $subject = APP_NAME . " : Nouvelle demande d'emploi courrier";
            }

            $user_data = $this->Common_model->get_user_details($id);

            if (!empty($user_data)) {

                $all_company_list = $this->Common_model->get_my_application($id, $application_id);

                file_put_contents($this->log_file, "\n\n ======== total_company_list count:" . count($all_company_list) . "===== \n\n", FILE_APPEND | LOCK_EX);
                file_put_contents($this->log_file, "\n\n ======== all_company_list:" . json_encode($all_company_list) . "===== \n\n", FILE_APPEND | LOCK_EX);

                if (!empty($all_company_list)) {
                    $document_folder_path = UPLOAD_FILE_FOLDER . '/' . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_data['udf_id'];

                    if ($user_data['udf_type'] == 2) {
                        $user_data['udf_cv_file_name'] = $user_data['udf_rapidjob_cv_file_name'];
                    }

                    // set user image
                    $user_image_path = UPLOAD_FILE_FOLDER . "/" . UPLOAD_USERS_FOLDER . "/" . $id . "/" . $user_data['u_image'];
                    $user_image = UPLOAD_ABS_PATH . DEFAULT_IMAGE_NAME;
                    if (!empty($user_data['u_image']) && file_exists($user_image_path)) {
                        $user_image = UPLOAD_ABS_PATH . UPLOAD_USERS_FOLDER . "/" . $id . "/" . $user_data['u_image'];
                    }

                    $first_attachment_data = $second_attachment_data = array();
                    // check user cv exist or not
                    $first_attachment_path = $document_folder_path . '/' . $user_data['udf_cv_file_name'];
                    if (!empty($user_data['udf_cv_file_name']) && file_exists($first_attachment_path)) {
                        $first_attachment_data = array(UPLOAD_ABS_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_data['udf_id'] . '/' . $user_data['udf_cv_file_name'], $user_data['udf_cv_file_name']);
                    }
                    // check user cover latter exist or not
                    $second_attachment_path = $document_folder_path . '/' . $user_data['udf_cover_latter_file'];
                    if (!empty($user_data['udf_cover_latter_file']) && file_exists($second_attachment_path)) {
                        $second_attachment_data = array(UPLOAD_ABS_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_data['udf_id'] . '/' . $user_data['udf_cover_latter_file'], $user_data['udf_cover_latter_file']);
                    }

                    $get_all_company_id = array();
                    foreach ($all_company_list as $key => $value) {
                        $email_params = array(
                            'company_name' => ucfirst($value['company_name']),
                            'message' => $user_data['udf_message'],
                            'user_cv' => !empty($first_attachment_data) && !empty($first_attachment_data[0]) ? $first_attachment_data[0] : '',
                            'user_cover_latter' => !empty($second_attachment_data) && !empty($second_attachment_data[0]) ? $second_attachment_data[0] : '',
                            'user_name' => ucfirst($user_data['user_name']),
                            'user_email' => $user_data['u_email'],
                            'current_lang' => $current_lang,
                            'user_image' => $user_image,
                        );
                        $message = $this->load->view("emails/send_application", $email_params, TRUE);
                        $this->send_attachment_email(array($value['recruiter_email'] => ucfirst($value['company_name'])), $subject, $message, $first_attachment_data, $second_attachment_data);
                        file_put_contents($this->log_file, "\n Mail sent to " . $key . ":" . $value['recruiter_email'], FILE_APPEND | LOCK_EX);
                        $get_all_company_id[] = $value['recruiter_id'];
                    }

                    // update application status in user application table
                    $where_app_data = array(
                        "ua_id" => $application_id,
                        "ua_user_id" => $id,
                    );
                    $request_app_data["ua_modified_date"] = $this->utc_time;
                    $request_app_data["ua_status"] = 1;

                    $this->Common_model->update(TBL_USER_APPLICATIONS, $request_app_data, $where_app_data);
                } else {
                    file_put_contents($this->log_file, "\n\n ======== all company list empty for candidate id : " . $id . " and application id : " . $application_id . " \n\n", FILE_APPEND | LOCK_EX);
                }
            } else {
                file_put_contents($this->log_file, "\n\n ======== user data empty for candidate id : " . $id . " and application id : " . $application_id . " \n\n", FILE_APPEND | LOCK_EX);
            }
        } else {
            file_put_contents($this->log_file, "\n\n ======== request parameters candidate id : " . $id . " and application id : " . $application_id . " \n\n", FILE_APPEND | LOCK_EX);
        }
        file_put_contents($this->log_file, "\n\n ======== Cron End >__send_application_to_company... Date[" . date('h:i:s a d-m-Y') . "] ===== \n\n", FILE_APPEND | LOCK_EX);
    }

}
