<?php

/**
 * Description: Use this controller for 
 * -> Manipulate users details
 * -> Invite friends
 * -> Send application
 * -> Generate PDF
 * -> Download PDF, JPEG or PNG
 * 
 * @author Nitinkumar Vaghani
 * Last modified date : 2017-05-03
 * 
 */
class Users extends MY_Controller {

    public $log_file;

    public function __construct() {
        parent::__construct();
        $this->log_file = DOCROOT . 'logs/log_' . date('d-m-Y') . '.txt';
        $this->load->model('User_model');
     
    }

    /**
     * Description: Use this function for display my profile
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2016-12-17
     * 
     */
    public function index() {
        $data = array();
        try {
            // get user details
            $user_details = $this->Common_model->get_user_all_details($this->user_id);

            if (!empty($user_details['user_exp_data'])) {
                foreach ($user_details['user_exp_data'] as &$value) {
                    $value['ue_description'] = $this->convert_into_br($value['ue_description']);
                }
            }

            if (!empty($user_details['user_training_data'])) {
                foreach ($user_details['user_training_data'] as &$value) {
                    $value['ut_description'] = $this->convert_into_br($value['ut_description']);
                }
            }

            // get work job, activity area, work places and level of education
            $default_details = $this->Common_model->get_default_details();

            $data = array_merge($user_details, $default_details);
            if (empty($data)) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(HOME_PATH);
            }

            // get user cv formate for download as JPEG or PNG
            $data['current_lang'] = $this->current_lang;
            $data['download_type'] = 'image';
            $data['content'] = $this->load->view('users/common_generate_user_cv_' . $this->current_lang, $data, true);
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }
        $this->load->view('common/candidate_header');
        $this->load->view('users/show_profile', $data);
        $this->load->view('common/candidate_footer');
    }

    /**
     * Description: Use this function for display user personal information
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2016-12-17
     * 
     */
    public function edit_profile() {
        $data = array();
        try {
            // get user details
            $data['user_data'] = $this->Common_model->get_user_details($this->user_id);


            if (empty($data['user_data'])) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(USERS_PATH);
            }
            $nationality = $this->Common_model->get_nationalities();
            $data = array_merge($data, $nationality);
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }
        $this->load->view('common/candidate_header');
        $this->load->view('users/edit_profile', $data);
        $this->load->view('common/candidate_footer');
    }

    /**
     * Description: Use this function for add activity area
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2016-12-17
     * 
     */
    public function add_user_activity_area() {
        try {
            if ($this->input->post() != NULL) {
                // validate build cv form
                $ud_id = trim($this->input->post("ud_id"));
                $redirect_url = trim($this->input->post("redirect_url"));
                $activity_area = $this->input->post("activity_area[]");
                $search_keyword = trim($this->input->post("q"));
                $this->session->set_userdata(array('search_query' => $search_keyword));

                $activity_area_title = trim($this->Common_model->escape_data($this->input->post("activity_area_title")));

                //activity area
                if (!empty($activity_area) && (in_array("other", $activity_area) == TRUE)) {
                    $activity_area_id = $this->Common_model->manage_activity_area($activity_area_title);
                    $activity_other_pos = array_search("other", $activity_area);
                    if ($activity_other_pos !== FALSE) {
                        $activity_area[$activity_other_pos] = $activity_area_id;
                    }
                }

                $request_data = array(
                    "ud_activity_area" => implode(',', $activity_area),
                    "ud_modified_date" => $this->utc_time
                );

                $where_cv = array(
                    "ud_id" => $ud_id,
                    "ud_user_id" => $this->user_id
                );
                $cv_detail_id = $this->Common_model->update(TBL_USER_DETAILS, $request_data, $where_cv);
                if ($cv_detail_id > 0) {
                    $this->session->set_flashdata("success", lang('SHOW_PROFILE_PAGE_BIO_UPDATE_SUCCESS') . $response_message);
                } else {
                    $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_BIO_UPDATE_FAILED'));
                }
                redirect($redirect_url);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }
        redirect(USERS_PATH);
    }

    /**
     * Description: Use this function for build cv page
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2016-12-17
     * 
     */
    public function build_cv() {
        $data = array();

        try {
            if ($this->input->post() != NULL) {
                // validate build cv form
                $this->form_validation->set_rules('job_type', lang('BUILDCV_PAGE_SELECT_JOB'), 'required');
                $this->form_validation->set_rules('activity_area[]', lang('BUILDCV_PAGE_SELECT_ACTIVITY_AREA'), 'required');
                $this->form_validation->set_rules('work_place[]', lang('BUILDCV_PAGE_SELECT_WORK_PLACES_LABEL'), 'required');
                $this->form_validation->set_rules('education_level', lang('BUILDCV_PAGE_EDUCATION_LEVEL'), 'required');

                if ($this->form_validation->run() !== FALSE) {
                    $ud_id = trim($this->input->post("ud_id"));
                    $job_type = trim($this->input->post("job_type"));
                    $activity_area = $this->input->post("activity_area[]");
                    $work_place = $this->input->post("work_place[]");
                    $work_place_option = $this->input->post("work_place_option[]");
                    $contract_type = $this->input->post("contract_type[]");
                    $education_level = trim($this->input->post("education_level"));
                    $my_language = $this->input->post("my_language[]");
                    $language_rating = $this->input->post("my_language_rating[]");
                    $my_skill = $this->input->post("my_skill[]");
                    $skill_rating = $this->input->post("my_skill_rating[]");

                    $job_type_title = trim($this->Common_model->escape_data($this->input->post("job_type_title")));
                    $activity_area_title = trim($this->Common_model->escape_data($this->input->post("activity_area_title")));
                    $workplace_title = trim($this->Common_model->escape_data($this->input->post("other_workplace_title")));
                    $education_title = trim($this->Common_model->escape_data($this->input->post("other_education_title")));


                    // job
                    if ($job_type == 'other') {
                        $job_type = $this->Common_model->manage_job($job_type_title);
                    }

                    // education
                    if ($education_level == 'other') {
                        $education_level = $this->Common_model->manage_education($education_title);
                    }

                    //workplace
                    if (!empty($work_place) && (in_array("other", $work_place) == TRUE)) {
                        $work_place_id = $this->Common_model->manage_workplace($workplace_title);
                        $work_place_pos = array_search("other", $work_place);
                        if ($work_place_pos !== FALSE) {
                            $work_place[$work_place_pos] = $work_place_id;
                        }
                    }

                    //activity area
                    if (!empty($activity_area) && (in_array("other", $activity_area) == TRUE)) {
                        $activity_area_id = $this->Common_model->manage_activity_area($activity_area_title);
                        $activity_other_pos = array_search("other", $activity_area);
                        if ($activity_other_pos !== FALSE) {
                            $activity_area[$activity_other_pos] = $activity_area_id;
                        }
                    }

                    $request_data = array(
                        "ud_education_level" => $education_level,
                        "ud_job" => $job_type,
                        "ud_activity_area" => implode(',', $activity_area),
                        "ud_work_place" => implode(',', $work_place),
                        "ud_work_place_option" => implode(',', $work_place_option),
                        "ud_contarct_type" => implode(',', $contract_type),
                        "ud_created_date" => $this->utc_time
                    );
                    $where_cv = array(
                        "ud_id" => $ud_id,
                        "ud_user_id" => $this->user_id
                    );

                    $cv_id = $this->Common_model->update(TBL_USER_DETAILS, $request_data, $where_cv);
                    if ($cv_id > 0) {
                        $response_message = "";

                        // store user languages
                        if (!empty($my_language) && !empty($language_rating)) {
                            $my_language = array_filter($my_language);
                            $language_rating = array_filter($language_rating);
                            if (!empty($my_language) && !empty($language_rating)) {
                                $my_language_data = array_combine($my_language, $language_rating);
                                $request_data = array(
                                    "ul_user_id" => $this->user_id,
                                    "ul_language" => json_encode($my_language_data),
                                    "ul_created_date" => $this->utc_time
                                );
                                $lang_id = $this->Common_model->insert(TBL_USER_LANGUAGES, $request_data);

                                if (empty($lang_id)) {
                                    $response_message .= " ," . lang('SHOW_PROFILE_PAGE_LANGUAGES_FAILED');
                                }
                            }
                        }

                        // store user skills
                        if (!empty($my_skill) && !empty($skill_rating)) {
                            $my_skill = array_filter($my_skill);
                            $skill_rating = array_filter($skill_rating);
                            if (!empty($my_skill) && !empty($skill_rating)) {
                                $my_skill_data = array_combine($my_skill, $skill_rating);
                                $request_data = array(
                                    "us_user_id" => $this->user_id,
                                    "us_name" => '',
                                    "us_skills" => json_encode($my_skill_data),
                                    "us_created_date" => $this->utc_time
                                );
                                $skill_id = $this->Common_model->insert(TBL_USER_SKILLS, $request_data);
                                if (empty($skill_id)) {
                                    $response_message .= " ," . lang('SHOW_PROFILE_PAGE_SKILL_FAILED');
                                }
                            }
                        }

                        $user_doc = $this->Common_model->get_user_document_details(0, $this->user_id);

                        if (!empty($user_doc) && !empty($user_doc['udf_id']) && is_numeric($user_doc['udf_id'])) {
                            $doc_id = $user_doc['udf_id'];
                        } else {
                            // insert user documents detail if not exists
                            $insert_data = array(
                                "udf_user_id" => $this->user_id,
                                "udf_type" => 2,
                                "udf_created_date" => $this->utc_time
                            );

                            $doc_id = $this->Common_model->insert(TBL_USER_DOCUMENT_FILES, $insert_data);
                        }
                        if ($doc_id > 0) {
                            // generate new pdf
                            $this->generate_user_cv($this->user_id, $this->current_lang, $doc_id);
                        } else {
                            $response_message .= " ," . lang('MY_DOCUMENTS_DOC_UPDATE_ERROR');
                        }
                        $this->session->set_flashdata("success", lang('SHOW_PROFILE_PAGE_BIO_UPDATE_SUCCESS') . $response_message);
                        redirect(USERS_PATH . '/my_documents');
                    } else {
                        $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_BIO_UPDATE_FAILED'));
                        redirect(USERS_PATH . '/build_cv');
                    }
                }
            }

            // get user details
            $user_details = $this->Common_model->get_user_all_details($this->user_id);

            // get work job, activity area, work places and level of education
            $default_details = $this->Common_model->get_default_details();

            $data = array_merge($user_details, $default_details);
            if (empty($data)) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(HOME_PATH);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }

        $this->load->view('common/candidate_header');
        $this->load->view('users/build_cv', $data);
        $this->load->view('common/candidate_footer');
    }

    /**
     * Description: Use this function for add user details
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2016-12-17
     * 
     */
    public function add() {

        try {
            /** edit profile * */
            // edit user details
            if ($this->input->post() != NULL && $this->input->post("edit_profile_form") == 'edit_profile_form') {
                $email = trim($this->Common_model->escape_data($this->input->post("email")));
                $gender = trim($this->Common_model->escape_data($this->input->post("civility")));
                $first_name = trim($this->Common_model->escape_data($this->input->post("first_name")));
                $last_name = trim($this->Common_model->escape_data($this->input->post("last_name")));
                $nationality = trim($this->Common_model->escape_data($this->input->post("nationality")));
                $address = trim($this->Common_model->escape_data($this->input->post("address")));
                $city = trim($this->Common_model->escape_data($this->input->post("city")));
                $postal_code = trim($this->Common_model->escape_data($this->input->post("postal_code")));
                $country = trim($this->Common_model->escape_data($this->input->post("country")));

                $where = array(
                    "LOWER(u_email)" => strtolower($email),
                    "u_id !=" => $this->user_id
                );

                $check_user = $this->Common_model->get_single_row(TBL_USERS, "u_id", $where);
                if (!empty($check_user)) {
                    $this->session->set_flashdata('failure', lang('COMMON_EMAIL_ALREADY_REGISTERED'));
                } else {
                    $user_data = array(
                        "u_first_name" => $first_name,
                        "u_last_name" => $last_name,
                        "u_email" => $email,
                        "u_modified_date" => $this->utc_time
                    );
                    $where_user = array(
                        "u_id" => $this->user_id,
                        "u_status" => 1
                    );
                    $user_id = $this->Common_model->update(TBL_USERS, $user_data, $where_user);
                    if ($user_id > 0) {
                        $user_session_data = array(
                            "user_first_name" => $first_name,
                            "user_last_name" => $last_name
                        );

                        $this->session->set_userdata($user_session_data);

                        if (!empty($gender)) {
                            $request_data['ud_gender'] = $gender;
                        }
                        if (!empty($nationality)) {
                            $request_data['ud_nationality'] = $nationality;
                        }
                        if (!empty($address)) {
                            $request_data['ud_address'] = $address;
                        }
                        if (!empty($city)) {
                            $request_data['ud_city'] = $city;
                        }
                        if (!empty($postal_code)) {
                            $request_data['ud_postal_code'] = $postal_code;
                        }
                        if (!empty($country)) {
                            $request_data['ud_country'] = $country;
                        }
                        $request_data['ud_modified_date'] = $this->utc_time;
                        $where_detail = array(
                            "ud_user_id" => $this->user_id,
                            "ud_status" => 1
                        );
                        $this->Common_model->update(TBL_USER_DETAILS, $request_data, $where_detail);

                        $this->session->set_flashdata("success", lang('SHOW_PROFILE_PAGE_BIO_UPDATE_SUCCESS'));
                    } else {
                        $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_BIO_UPDATE_FAILED'));
                    }
                }
                redirect(USERS_PATH . '/edit_profile');
            } else if ($this->input->post() != NULL && $this->input->post("user_hobbies_form") == 'user_hobbies_form') {

                /*                 * ********************* end of edit profile *********************** */
                /*                 * ****************************************************************** */
                /*                 * ******************* insert data of show-profile page  *********** */

                // insert user hobbies
                $hobbies_title = $this->input->post("user_hobbies_title[]");
                $hobbies_description = $this->input->post("user_hobbies_description[]");
                $request_data = array(
                    "uh_user_id" => $this->user_id,
                    "uh_title" => json_encode($hobbies_title),
                    "uh_description" => json_encode($hobbies_description),
                    "uh_created_date" => $this->utc_time
                );
                $hobbies_id = $this->Common_model->insert(TBL_USER_HOBBIES, $request_data);
                if ($hobbies_id > 0) {
                    $this->session->set_flashdata("success", lang('SHOW_PROFILE_PAGE_HOBBIES_SUCESS'));
                    redirect(USERS_PATH);
                } else {
                    $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_HOBBIES_FAILED'));
                    redirect(USERS_PATH);
                }
            } else if ($this->input->post() != NULL && $this->input->post("user_skill_form") == 'user_skill_form') {
                // insert user skill
                $user_skill = trim($this->Common_model->escape_data($this->input->post("user_skill")));
                $user_skill_establishment = $this->input->post("user_skill_establishment[]");
                $skill_rating = $this->input->post("user_skill_rating[]");
                // generate key-value pair of skill establish name and their rating
                $user_skill_array = array_combine($user_skill_establishment, $skill_rating);
                $request_data = array(
                    "us_user_id" => $this->user_id,
                    "us_name" => $user_skill,
                    "us_skills" => json_encode($user_skill_array),
                    "us_created_date" => $this->utc_time
                );
                $skill_id = $this->Common_model->insert(TBL_USER_SKILLS, $request_data);
                if ($skill_id > 0) {
                    $this->session->set_flashdata("success", lang('SHOW_PROFILE_PAGE_SKILL_SUCESS'));
                    redirect(USERS_PATH);
                } else {
                    $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_SKILL_FAILED'));
                    redirect(USERS_PATH);
                }
            } else if ($this->input->post() != NULL && $this->input->post("user_training_form") == 'user_training_form') {
                // insert user training
                $user_training = trim($this->Common_model->escape_data($this->input->post("user_training")));
                $user_establishment = trim($this->Common_model->escape_data($this->input->post("user_establishment")));
                $training_description = trim($this->Common_model->escape_data($this->input->post("user_training_desc")));
                $training_start_month = trim($this->Common_model->escape_data($this->input->post("training_start_month")));
                $training_start_year = trim($this->Common_model->escape_data($this->input->post("training_start_year")));
                $training_end_month = trim($this->Common_model->escape_data($this->input->post("training_end_month")));
                $training_end_year = trim($this->Common_model->escape_data($this->input->post("training_end_year")));

                $start_date = strtotime($training_start_year . "-" . $training_start_month . "-01 00:00");
                $end_date = strtotime($training_end_year . "-" . $training_end_month . "-01 00:00");

                if ($start_date > $this->utc_time || $end_date < $start_date) {
                    $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_EXP_DATE_INVALID'));
                    redirect(USERS_PATH);
                }

                $request_data = array(
                    "ut_user_id" => $this->user_id,
                    "ut_name" => $user_training,
                    "ut_establissment" => $user_establishment,
                    "ut_description" => trim($training_description, "\r\n"),
                    "ut_start_date" => $start_date,
                    "ut_end_date" => $end_date,
                    "ut_created_date" => $this->utc_time
                );

                $training_id = $this->Common_model->insert(TBL_USER_TRAININGS, $request_data);
                if ($training_id > 0) {
                    $this->session->set_flashdata("success", lang('SHOW_PROFILE_PAGE_TRAINING_SUCESS'));
                    redirect(USERS_PATH);
                } else {
                    $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_TRAINING_FAILED'));
                    redirect(USERS_PATH);
                }
            } else if ($this->input->post() != NULL && $this->input->post("user_experience_form") == 'user_experience_form') {
                // insert user experience
                $company_name = trim($this->Common_model->escape_data($this->input->post("company_name")));
                $job_type = trim($this->Common_model->escape_data($this->input->post("job_type")));
                $company_title = trim($this->Common_model->escape_data($this->input->post("company_title")));
                $company_place = trim($this->Common_model->escape_data($this->input->post("company_place")));
                $exp_description = trim($this->Common_model->escape_data($this->input->post("experience_description")));
                $current_position = trim($this->Common_model->escape_data($this->input->post("in_current_position")));
                $experience_start_month = trim($this->Common_model->escape_data($this->input->post("experience_start_month")));
                $experience_start_year = trim($this->Common_model->escape_data($this->input->post("experience_start_year")));
                $experience_end_month = trim($this->Common_model->escape_data($this->input->post("experience_end_month")));
                $experience_end_year = trim($this->Common_model->escape_data($this->input->post("experience_end_year")));

                $start_date = strtotime($experience_start_year . "-" . $experience_start_month . "-01 00:00");
                $end_date = strtotime($experience_end_year . "-" . $experience_end_month . "-01 00:00");

                $current_position = (!empty($current_position) && $current_position == 'on') ? 1 : 2;

                if ($start_date > $this->utc_time || ($end_date < $start_date && $current_position == 2)) {
                    $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_EXP_DATE_INVALID'));
                    redirect(USERS_PATH);
                }

                if ($job_type == 'other') {
                    $job_type = $this->Common_model->manage_job($company_title);
                }

                $request_data = array(
                    "ue_user_id" => $this->user_id,
                    "ue_company" => $company_name,
                    "ue_job" => $job_type,
                    "ue_title" => '',
                    "ue_place" => $company_place,
                    "ue_description" => trim($exp_description, "\r\n"),
                    "ue_start_date" => $start_date,
                    "ue_in_current_position" => $current_position,
                    "ue_created_date" => $this->utc_time
                );

                if (!empty($end_date) && $current_position == 2) {
                    $request_data["ue_end_date"] = $end_date;
                } else {
                    $request_data["ue_end_date"] = null;
                }
                $exp_id = $this->Common_model->insert(TBL_USER_EXPERIANCES, $request_data);
                if ($exp_id > 0) {
                    $this->session->set_flashdata("success", lang('SHOW_PROFILE_PAGE_EXP_SUCESS'));
                    redirect(USERS_PATH);
                } else {
                    $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_EXP_FAILED'));
                    redirect(USERS_PATH);
                }
            } else if ($this->input->post() != NULL && $this->input->post("user_bio_form") == 'user_bio_form') {
                // insert user bio description
                $ud_user_bio = trim($this->input->post("ud_user_bio"));
                $where_bio = array(
                    "ud_user_id" => $this->user_id,
                    "ud_status" => 1,
                    "ud_user_bio" => $this->Common_model->escape_data($ud_user_bio)
                );
                $is_changed = $this->Common_model->get_single_row(TBL_USER_DETAILS, 'ud_id', $where_bio);
                if (!empty($is_changed)) {
                    $this->session->set_flashdata("success", lang('SHOW_PROFILE_PAGE_BIO_UPDATE_SAME'));
                    redirect(USERS_PATH);
                } else {
                    $request_data = array(
                        "ud_user_bio" => trim($this->Common_model->escape_data($ud_user_bio)),
                    );
                    $where_update_bio = array(
                        "ud_user_id" => $this->user_id,
                        "ud_status" => 1
                    );
                    $is_updated = $this->Common_model->update(TBL_USER_DETAILS, $request_data, $where_update_bio);
                    if ($is_updated > 0) {
                        $this->session->set_flashdata("success", lang('SHOW_PROFILE_PAGE_BIO_UPDATE_SUCCESS'));
                        redirect(USERS_PATH);
                    } else {
                        $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_BIO_UPDATE_FAILED'));
                        redirect(USERS_PATH);
                    }
                }
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }
        redirect(USERS_PATH);
    }

    /**
     * Description: Use this function for edit user details
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-05-15
     * 
     */
    public function edit() {
        // update user experience
        if ($this->input->post() != NULL && $this->input->post("user_experience_form") == 'user_experience_form') {
            $company_name = trim($this->Common_model->escape_data($this->input->post("company_name")));
            $job_type = trim($this->Common_model->escape_data($this->input->post("job_type")));
            $company_title = trim($this->Common_model->escape_data($this->input->post("company_title")));
            $company_place = trim($this->Common_model->escape_data($this->input->post("company_place")));
            $exp_description = trim($this->Common_model->escape_data($this->input->post("experience_description")));
            $current_position = trim($this->Common_model->escape_data($this->input->post("in_current_position")));
            $experience_start_month = trim($this->Common_model->escape_data($this->input->post("experience_start_month")));
            $experience_start_year = trim($this->Common_model->escape_data($this->input->post("experience_start_year")));
            $experience_end_month = trim($this->Common_model->escape_data($this->input->post("experience_end_month")));
            $experience_end_year = trim($this->Common_model->escape_data($this->input->post("experience_end_year")));
            $experience_id = trim($this->Common_model->escape_data($this->input->post("experience_id")));

            $start_date = strtotime($experience_start_year . "-" . $experience_start_month . "-01 00:00");
            $end_date = strtotime($experience_end_year . "-" . $experience_end_month . "-01 00:00");

            $current_position = (!empty($current_position) && $current_position == 'on') ? 1 : 2;

            if ($start_date > $this->utc_time || ($end_date < $start_date && $current_position == 2)) {
                $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_EXP_DATE_INVALID'));
                redirect(USERS_PATH);
            }

            if ($job_type == 'other') {
                $job_type = $this->Common_model->manage_job($company_title);
            }


            $request_data = array(
                "ue_company" => $company_name,
                "ue_job" => $job_type,
                "ue_title" => '',
                "ue_place" => $company_place,
                "ue_description" => trim($exp_description, '\r\n'),
                "ue_start_date" => $start_date,
                "ue_in_current_position" => $current_position,
                "ue_modified_date" => $this->utc_time
            );

            if (!empty($end_date) && $current_position == 2) {
                $request_data["ue_end_date"] = $end_date;
            } else {
                $request_data["ue_end_date"] = null;
            }

            $exp_where = array(
                'ue_id' => $experience_id
            );

            $is_updated = $this->Common_model->update(TBL_USER_EXPERIANCES, $request_data, $exp_where);
            if ($is_updated > 0) {
                $this->session->set_flashdata("success", lang('SHOW_PROFILE_PAGE_EXP_SUCESS'));
                redirect(USERS_PATH);
            } else {
                $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_EXP_FAILED'));
                redirect(USERS_PATH);
            }
        } else if ($this->input->post() != NULL && $this->input->post("user_training_form") == 'user_training_form') {
            $user_training = trim($this->Common_model->escape_data($this->input->post("user_training")));
            $user_establishment = trim($this->Common_model->escape_data($this->input->post("user_establishment")));
            $training_description = trim($this->Common_model->escape_data($this->input->post("user_training_desc")));
            $training_start_month = trim($this->Common_model->escape_data($this->input->post("training_start_month")));
            $training_start_year = trim($this->Common_model->escape_data($this->input->post("training_start_year")));
            $training_end_month = trim($this->Common_model->escape_data($this->input->post("training_end_month")));
            $training_end_year = trim($this->Common_model->escape_data($this->input->post("training_end_year")));
            $training_id = trim($this->Common_model->escape_data($this->input->post("training_id")));

            $start_date = strtotime($training_start_year . "-" . $training_start_month . "-01 00:00");
            $end_date = strtotime($training_end_year . "-" . $training_end_month . "-01 00:00");

            if ($start_date > $this->utc_time || $end_date < $start_date) {
                $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_EXP_DATE_INVALID'));
                redirect(USERS_PATH);
            }

            $request_data = array(
                "ut_user_id" => $this->user_id,
                "ut_name" => $user_training,
                "ut_establissment" => $user_establishment,
                "ut_description" => trim($training_description, "\r\n"),
                "ut_start_date" => $start_date,
                "ut_end_date" => $end_date,
                "ut_modified_date" => $this->utc_time
            );

            $training_where = array(
                'ut_id' => $training_id
            );

            $is_training_updated = $this->Common_model->update(TBL_USER_TRAININGS, $request_data, $training_where);
            if ($is_training_updated > 0) {
                $this->session->set_flashdata("success", lang('SHOW_PROFILE_PAGE_TRAINING_SUCESS'));
                redirect(USERS_PATH);
            } else {
                $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_TRAINING_FAILED'));
                redirect(USERS_PATH);
            }
        } else if ($this->input->post() != NULL && $this->input->post("user_skill_form") == 'user_skill_form') {
            $user_skill = trim($this->Common_model->escape_data($this->input->post("user_skill")));
            $user_skill_establishment = $this->input->post("user_skill_establishment[]");
            $skill_rating = $this->input->post("user_skill_rating[]");
            $skill_id = $this->input->post("skill_id");
            // generate key-value pair of skill establish name and their rating
            $user_skill_array = array_combine($user_skill_establishment, $skill_rating);
            $request_data = array(
                "us_name" => $user_skill,
                "us_skills" => json_encode($user_skill_array),
                "us_modified_date" => $this->utc_time
            );

            $skill_where = array(
                'us_id' => $skill_id
            );

            $is_skill_updated = $this->Common_model->update(TBL_USER_SKILLS, $request_data, $skill_where);
            if ($is_skill_updated > 0) {
                $this->session->set_flashdata("success", lang('SHOW_PROFILE_PAGE_SKILL_SUCESS'));
                redirect(USERS_PATH);
            } else {
                $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_SKILL_FAILED'));
                redirect(USERS_PATH);
            }
        } else if ($this->input->post() != NULL && $this->input->post("user_hobbies_form") == 'user_hobbies_form') {
            $hobbies_title = $this->input->post("user_hobbies_title[]");
            $hobbies_description = $this->input->post("user_hobbies_description[]");
            $hobbies_id = $this->input->post("hobbies_id[]");

            $request_data = array(
                "uh_user_id" => $this->user_id,
                "uh_title" => json_encode($hobbies_title),
                "uh_description" => json_encode($hobbies_description),
                "uh_modified_date" => $this->utc_time
            );

            $hobbies_where = array(
                'uh_id' => $hobbies_id
            );

            $is_hobbies_updated = $this->Common_model->update(TBL_USER_HOBBIES, $request_data, $hobbies_where);
            if ($is_hobbies_updated > 0) {
                $this->session->set_flashdata("success", lang('SHOW_PROFILE_PAGE_HOBBIES_SUCESS'));
                redirect(USERS_PATH);
            } else {
                $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_HOBBIES_FAILED'));
                redirect(USERS_PATH);
            }
        } else {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(HOME_PATH);
        }
    }

    /**
     * Description : This function is use for change user's password
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2016-01-05
     * 
     */
    public function change_password() {

        try {
            if ($this->input->post() != NULL) {
                //get user's current password
                $where_user = array(
                    "u_id" => $this->user_id,
                    "u_status" => 1
                );
                $user_data = $this->Common_model->get_single_row(TBL_USERS, "u_password", $where_user);
                if (empty($user_data)) {
                    $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                    redirect(HOME_PATH);
                }
                $current_password = trim($this->Common_model->escape_data($this->input->get_post('old_password')));
                if (password_verify(sha1($current_password), $user_data['u_password'])) {
                    //change new password
                    $new_password = trim($this->Common_model->escape_data($this->input->get_post('password')));
                    $udpate_array = array(
                        "u_password" => password_hash(sha1($new_password), PASSWORD_BCRYPT),
                        "u_modified_date" => $this->utc_time
                    );
                    $is_update = $this->Common_model->update(TBL_USERS, $udpate_array, $where_user);
                    if ($is_update > 0) {
                        $this->session->set_flashdata('success', lang("EDITPROFILE_PAGE_CHANGE_PASSWORD_SUCCESS"));
                    } else {
                        $this->session->set_flashdata('failure', lang("EDITPROFILE_PAGE_CHANGE_PASSWORD_FAILED"));
                    }
                } else {
                    $this->session->set_flashdata('failure', lang("EDITPROFILE_PAGE_CHANGE_PASSWORD_INVALID"));
                }
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }
        redirect(USERS_PATH . '/edit_profile');
    }

    /**
     * Description: Use this function for show send application page
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2016-01-11
     * 
     */
    public function send_application() {
        $data = array();
        try {
            // get user details
            $data['user_data'] = $this->Common_model->get_user_details($this->user_id);
            if (empty($data['user_data'])) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(HOME_PATH);
            }

            $where_user_details = array(
                "udf_user_id" => $this->user_id,
                "udf_status" => 1
            );
            $data['user_docs'] = $this->Common_model->get_single_row(TBL_USER_DOCUMENT_FILES, '*', $where_user_details);

            // get user details
            $user_details = $this->Common_model->get_user_all_details($this->user_id);
            $data['user_exp_data'] = count($user_details['user_exp_data']);
            $data['user_skill_data'] = count($user_details['user_skill_data']);
            $data['user_training_data'] = count($user_details['user_training_data']);

            if ($this->input->post() != NULL && $this->input->post('send_application_form') == "true") {
                // add / edit user application
                $this->manage_application($data['user_docs'], $this->input->post());
                $this->session->set_flashdata("success", lang("SHOW_PROFILE_PAGE_BIO_UPDATE_SUCCESS"));
                redirect(USERS_PATH . '/validate_application');
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH . '/send_application');
        }

        $this->load->view('common/candidate_header');
        $this->load->view('users/send_application', $data);
        $this->load->view('common/candidate_footer');
    }

    /**
     * Description : Use this function for add / edit user application
     * 
     * @authour Nitinkumar vaghani
     * Last Modified date : 2016-01-11
     * 
     * @param type $request_data
     * @param type $request_posts
     */
    public function manage_application($request_data, $request_posts) {
        try {
            if (!empty($request_posts)) {
                $user_message = !empty($request_posts['user_message']) ? $request_posts['user_message'] : '';
                $attached_cv = !empty($request_posts['attach_my_cv']) ? $request_posts['attach_my_cv'] : '';
                $user_comment = !empty($request_posts['user_comment']) ? $request_posts['user_comment'] : '';
                $accept_term = !empty($request_posts['accept_term']) ? $request_posts['accept_term'] : '';

                /**
                 * $attached_cv == 1 : CV attached
                 * $attached_cv == 2 : rapidjob CV
                 */
                $attached_cv = (!empty($attached_cv) && $attached_cv == 2) ? 2 : 1;
                if (empty($request_data)) {

                    $user_doc = $this->Common_model->get_user_document_details(0, $this->user_id);

                    if (empty($user_doc)) {
                        // insert user details
                        $insert_data = array(
                            "udf_user_id" => $this->user_id,
                            "udf_message" => $this->Common_model->escape_data($user_message),
                            "udf_type" => $attached_cv,
                            "udf_blocked_email" => $this->Common_model->escape_data($user_comment),
                            "udf_created_date" => $this->utc_time
                        );

                        $doc_id = $this->Common_model->insert(TBL_USER_DOCUMENT_FILES, $insert_data);
                    } else {
                        $doc_id = $user_doc['udf_id'];
                    }


                    if ($doc_id > 0) {
                        // if cv uploaded by user
                        if ($attached_cv == 1) {
                            if (!empty($_FILES["my_cv"]["name"])) {
                                $my_cv["file"] = $_FILES["my_cv"];
                                $this->upload_cv_files($my_cv, $doc_id, array('field_name' => 'udf_cv_file_name'));
                            }
                        } else {
                            // generate new pdf
                            //$this->generate_user_cv($this->user_id, $this->current_lang, $doc_id);
                        }

                        if (!empty($_FILES["my_cover_latter"]["name"])) {
                            $my_cover_latter["file"] = $_FILES["my_cover_latter"];
                            $this->upload_cv_files($my_cover_latter, $doc_id, array('field_name' => 'udf_cover_latter_file'));
                        }
                    } else {
                        $this->session->set_flashdata("failure", lang("SEND_APPLICATION_INSERT_ERROR"));
                        redirect(USERS_PATH . '/send_application');
                    }
                } else {
                    $doc_id = $request_data['udf_id'];
                    $request_details = array(
                        "udf_message" => $this->Common_model->escape_data($user_message),
                        "udf_blocked_email" => $this->Common_model->escape_data($user_comment),
                        "udf_type" => $attached_cv,
                        "udf_modified_date" => $this->utc_time
                    );
                    $where_doc = array(
                        "udf_id" => $doc_id,
                        "udf_user_id" => $this->user_id,
                        "udf_status" => 1
                    );
                    $is_updated = $this->Common_model->update(TBL_USER_DOCUMENT_FILES, $request_details, $where_doc);
                    if ($is_updated > 0) {
                        // if cv uploaded by user
                        if ($attached_cv == 1) {
                            if (!empty($_FILES["my_cv"]["name"])) {
                                $my_cv["file"] = $_FILES["my_cv"];
                                $this->upload_cv_files($my_cv, $doc_id, array('field_name' => 'udf_cv_file_name'));
                            }
                        } else {
                            // generate new pdf
                            //$this->generate_user_cv($this->user_id, $this->current_lang, $doc_id);
                        }
                        if (!empty($_FILES["my_cover_latter"]["name"])) {
                            $my_cover_latter["file"] = $_FILES["my_cover_latter"];
                            $this->upload_cv_files($my_cover_latter, $doc_id, array('field_name' => 'udf_cover_latter_file'));
                        }
                    } else {
                        $this->session->set_flashdata("failure", lang("SEND_APPLICATION_UPDATE_ERROR"));
                        redirect(USERS_PATH . '/send_application');
                    }
                }
            } else {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(USERS_PATH . '/send_application');
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH . '/send_application');
        }
    }

    /**
     * Description: Use this function for validate and send application
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2016-01-09
     * 
     */
    public function validate_application() {
        $data = array();
        try {
            // get user details
            $data['user_data'] = $this->Common_model->get_user_details($this->user_id);
            if (empty($data['user_data'])) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(USERS_PATH);
            }
            if ($this->input->post() != NULL && $this->input->post('send_application_now_form') == "true") {
                $user_message = $this->input->post('user_message');
                $doc_id = $this->input->post('user_doc_id');
                $attached_cv = !empty($request_posts['attach_my_cv']) ? $request_posts['attach_my_cv'] : '';
                /**
                 * $attached_cv == 1 : CV attached
                 * $attached_cv == 2 : rapidjob CV
                 */
                $attached_cv = (!empty($attached_cv) && $attached_cv == 2) ? 2 : 1;
                if (!empty($data['user_data']['udf_id'])) {
                    $request_details = array(
                        "udf_message" => $this->Common_model->escape_data($user_message),
                        "udf_modified_date" => $this->utc_time
                    );
                    $where_doc = array(
                        "udf_id" => $doc_id,
                        "udf_user_id" => $this->user_id,
                        "udf_status" => 1
                    );
                    $is_updated = $this->Common_model->update(TBL_USER_DOCUMENT_FILES, $request_details, $where_doc);
                    if ($is_updated > 0) {
                        // if cv uploaded by user
                        if ($attached_cv == 1) {
                            if (!empty($_FILES["my_cv"]["name"])) {
                                $my_cv["file"] = $_FILES["my_cv"];
                                $this->upload_cv_files($my_cv, $doc_id, array('field_name' => 'udf_cv_file_name'));
                            }
                        } else {
                            // generate new pdf
                            //$this->generate_user_cv($this->user_id, $this->current_lang, $doc_id);
                        }

                        if (!empty($_FILES["my_cover_latter"]["name"])) {
                            $my_cover_latter["file"] = $_FILES["my_cover_latter"];
                            $this->upload_cv_files($my_cover_latter, $doc_id, array('field_name' => 'udf_cover_latter_file'));
                        }
                        $this->session->set_flashdata('success', lang('SEND_APPLICATION_NOW_SAVE_SUCCESS'));
                        redirect(USERS_PATH . '/invite_friends');
                    } else {
                        $this->session->set_flashdata("failure", lang("SEND_APPLICATION_UPDATE_ERROR"));
                        redirect(USERS_PATH . '/send_application');
                    }
                } else {
                    $this->session->set_flashdata("failure", lang("SEND_APPLICATION_NOW_CV_DETAILS_NOT_FOUND"));
                    redirect(USERS_PATH . '/send_application');
                }
            }

            // get user details
            $user_details = $this->Common_model->get_user_all_details($this->user_id);
            $data['user_exp_data'] = count($user_details['user_exp_data']);
            $data['user_skill_data'] = count($user_details['user_skill_data']);
            $data['user_training_data'] = count($user_details['user_training_data']);

            // get work place details
            if (!empty($data['user_data']['ud_work_place'])) {
                // select user work places
                $where_place = explode(',', $data['user_data']['ud_work_place']);
                $workplace_name = $this->Common_model->get_user_workplaces($where_place);
                if (!empty($workplace_name['workplace_name'])) {
                    $data['user_data']['workplace_name'] = $workplace_name['workplace_name'];
                } else {
                    $data['user_data']['workplace_name'] = '';
                }
            }

            // get activity area details
            if (!empty($data['user_data']['ud_activity_area'])) {
                // select user activity area
                $where_activity_area = explode(',', $data['user_data']['ud_activity_area']);
                $area_name = $this->Common_model->get_user_activities($where_activity_area);
                if (!empty($area_name['activity_name'])) {
                    $data['user_data']['activity_name'] = $area_name['activity_name'];
                } else {
                    $data['user_data']['activity_name'] = '';
                }
            }

            $where_user_details = array(
                "udf_user_id" => $this->user_id,
                "udf_status" => 1
            );
            $data['user_docs'] = $this->Common_model->get_single_row(TBL_USER_DOCUMENT_FILES, '*', $where_user_details);
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("MAIL_INVITATION_SEND_FAILED"));
            redirect(USERS_PATH . '/invite_friends');
        }
        $this->load->view('common/candidate_header');
        $this->load->view('users/send_application_now', $data);
        $this->load->view('common/candidate_footer');
    }

    /**
     * Description: 
     * 1. Use this function for send invitation to friends
     * 2. Show purchase Plan
     * 3. Show priority company list and Send mail to all company
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-03-05
     * 
     */
    public function invite_friends() {

        $data = array();

        try {
            // get user details
            $where_user = array(
                'u_id' => $this->user_id,
                'u_status' => 1,
                'u_invitation_code !=' => ''
            );

            $join_array = array(
                TBL_USER_DETAILS => "u_id = ud_user_id AND ud_status=1",
                TBL_USER_DOCUMENT_FILES => "u_id = udf_user_id AND udf_status=1"
            );

            $column_user = "u_id,
                            u_type,
                            u_first_name,
                            u_last_name,
                            u_email,
                            u_status,
                            u_invitation_code,
                            ud_id,
                            ud_user_id,
                            ud_activity_area,
                            ud_points,
                            ud_send_by_post_points,
                            ud_subscribe_plan,
                            udf_blocked_email
                            ";
            $user_data = $this->Common_model->get_single_row(TBL_USERS, $column_user, $where_user, $join_array, 'LEFT');

            if (empty($user_data)) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(USERS_PATH);
            }

            if ($this->input->post() != NULL) {
                // validate invitation form
                $this->form_validation->set_rules('invitation_mail_1', lang("FREE_ITEM_EMAIL_PLACEHOLDER") . ' 1', 'required|valid_email');
                $this->form_validation->set_rules('invitation_mail_2', lang("FREE_ITEM_EMAIL_PLACEHOLDER") . ' 2', 'valid_email');
                $this->form_validation->set_rules('invitation_mail_3', lang("FREE_ITEM_EMAIL_PLACEHOLDER") . ' 3', 'valid_email');
                if ($this->form_validation->run() !== FALSE) {
                    $data = array();
                    $email_list = $this->input->post();

                    if (!empty($email_list) && is_array($email_list) && count($email_list) > 0) {
                        // remove empty key values from requested params
                        $email_list = array_filter($email_list);
                    }

                    if (empty($email_list)) {
                        $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                    } else {
                        $link = HOME_PATH . "/verify_invitation_code?invitation_code=" . $user_data['u_invitation_code'];
                        $subject = APP_NAME . " : " . lang('MAIL_NEW_INVITATION');
                        foreach ($email_list as $friend_email) {
                            $username = strstr($friend_email, '@', true);
                            $message = $this->load->view("emails/invite_friends", array('invitation_link' => $link, 'name' => $username), TRUE);

                            // send invitation mail
                            $this->send_email($friend_email, $subject, $message);
                        }
                        $this->session->set_flashdata('success', lang('MAIL_INVITATION_SEND_SUCCESS'));
                    }
                    redirect(USERS_PATH . '/invite_friends');
                }
            }

            // get company details

            $this->load->model('Setting_model');

            $data['search_query'] = $this->session->userdata('search_query');
            $data['company_data'] = $this->User_model->get_company_list($this->user_id, $user_data, $data['search_query'], 0);
//            _px($data);
            $setting_array = $this->Setting_model->get_settings();
            $payment_rate_per_email = DEFAULT_PAYMENT_RATE_PER_EMAIL;
            if (!empty($setting_array) && isset($setting_array['payment_in_cent_per_email'])) {
                $payment_rate_per_email = $setting_array['payment_in_cent_per_email'];
            }

            $payment_rate_per_post = DEFAULT_PAYMENT_RATE_PER_POST;
            if (!empty($setting_array) && isset($setting_array['payment_rate_for_send_application_by_post'])) {
                $payment_rate_per_post = $setting_array['payment_rate_for_send_application_by_post'];
            }

            $data['user_data'] = $user_data;
            $data['payment_in_cent_per_email'] = $payment_rate_per_email;
            $data['payment_in_rate_per_post'] = $payment_rate_per_post;
            $data['activity_area'] = $this->Common_model->get_all_activity_areas();
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("MAIL_INVITATION_SEND_FAILED"));
            redirect(USERS_PATH . '/invite_friends');
        }
        $this->load->view('common/candidate_header');
        $this->load->view('users/invite_friends', $data);
        $this->load->view('common/candidate_footer');
    }

    /**
     * Description: Use this function for send application to company
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-05-03
     * 
     */
    public function send_application_to_all_company() {
        $data = array();
        try {
            file_put_contents($this->log_file, "\n\n ====Start  send_application_to_all_company() \n", FILE_APPEND | LOCK_EX);
            $user_data = $this->Common_model->get_user_details($this->user_id);
            file_put_contents($this->log_file, "\n\n User data : " . json_encode($user_data) . " \n\n", FILE_APPEND | LOCK_EX);

            if (empty($user_data)) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(USERS_PATH);
            }

            if ($this->input->post() != NULL && $this->input->post('send_application') == "true") {
                $company_list_data = $this->input->post('company_list');
                $send_application_limit = trim($this->input->post('send_application_limit'));
                $application_send_by = trim($this->input->post('application_send_by'));

                file_put_contents($this->log_file, "\n\n POST data : " . json_encode($_POST) . " \n\n", FILE_APPEND | LOCK_EX);

                if (!empty($send_application_limit) && is_numeric($send_application_limit) && ($application_send_by == 1 || $application_send_by == 2)) {

                    if ($application_send_by == 1) {
                        $user_current_points = (int) $user_data['ud_points'];
                        if (empty($user_current_points) || $send_application_limit > $user_current_points) {
                            $this->session->set_flashdata("failure", lang("FREE_ITEM_CHOOSE_FORMULA_AND_LIMIT"));
                            redirect(USERS_PATH . '/invite_friends');
                        }
                    } else if ($application_send_by == 2) {
                        $user_current_points = (int) $user_data['ud_send_by_post_points'];
                        if (empty($user_current_points) || $send_application_limit > $user_current_points) {
                            $this->session->set_flashdata("failure", lang("FREE_ITEM_CHOOSE_FORMULA_AND_LIMIT"));
                            redirect(USERS_PATH . '/invite_friends');
                        }
                    }

                    if (empty($company_list_data)) {
                        $data['search_query'] = $this->session->userdata('search_query');
                        $all_company_list_data = $this->User_model->get_company_list($this->user_id, $user_data, $data['search_query'], $send_application_limit, true);

                        if (!empty($all_company_list_data) && !empty($all_company_list_data[0])) {
                            foreach ($all_company_list_data as $key => $company_val) {
                                $company_list_data[$key] = $company_val['company_list'];
                            }
                        } else {
                            $this->session->set_flashdata("failure", lang("FREE_ITEM_SEND_NO_COMPANY"));
                            redirect(USERS_PATH . '/invite_friends');
                        }
                    }

                    file_put_contents($this->log_file, "\n\n company_list_data Count: " . count($company_list_data) . " \n\n", FILE_APPEND | LOCK_EX);
                    file_put_contents($this->log_file, "\n\n company_list_data data : " . json_encode($company_list_data) . " \n\n", FILE_APPEND | LOCK_EX);

                    if (empty($company_list_data)) {
                        $this->session->set_flashdata("failure", lang("FREE_ITEM_SEND_NO_COMPANY"));
                        redirect(USERS_PATH . '/invite_friends');
                    }

                    if ($application_send_by == 1) {
                        $application_id = $this->send_application_to_company_by_email($this->user_id, $user_data, $send_application_limit, $company_list_data);
                        if ($application_id) {
                            // send application mail using myCron controller send_application_to_company function
                            $file = DOCROOT . "../index.php /myCron/send_application_to_company/" . $this->user_id . "/" . $this->current_lang . "/" . $application_id;
                            file_put_contents($this->log_file, "\n\n ====Cron url:\n " . $file . " \n", FILE_APPEND | LOCK_EX);
                            exec(PHP_PATH . " " . $file . " > /dev/null &");
                            $this->session->set_flashdata('success', lang('SEND_APPLICATION_NOW_SENT_SUCCESS'));
                        } else {
                            $this->session->set_flashdata('failure', lang('SEND_APPLICATION_NOW_SENT_FAILURE'));
                        }
                    } else {
                        if ($this->send_application_to_company_by_post($this->user_id, $user_data, $send_application_limit, $company_list_data)) {
                            $this->session->set_flashdata('success', lang('SEND_APPLICATION_NOW_SENT_SUCCESS'));
                        } else {
                            $this->session->set_flashdata('failure', lang('SEND_APPLICATION_NOW_SENT_FAILURE'));
                        }
                    }

                    $this->session->unset_userdata('amount');
                    $this->session->unset_userdata('tariff');
                    $this->session->unset_userdata('application_send_by');
                    $this->session->unset_userdata('total_send_application');
                    $this->session->unset_userdata('all_selected_ids');
                } else {
                    $this->session->set_flashdata("failure", lang("FREE_ITEM_CHOOSE_FORMULA_AND_LIMIT"));
                    redirect(USERS_PATH . '/invite_friends');
                }
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH . '/invite_friends');
        }
        redirect(USERS_PATH);
    }

    /**
     * Description: Use this function for send application to all company by post
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-06-15
     * 
     * @param type $id : CANDIDATE ID
     * @param type $all_company_list
     * @return boolean
     */
    public function send_application_to_company_by_email($id, $user_data, $send_application_limit, $all_company_list) {
        try {
            if (!empty($id) && is_numeric($id) && !empty($all_company_list) && !empty($user_data) && !empty($send_application_limit)) {

                if ($user_data['udf_type'] == 1) {
                    $request_all_documents_data = array(
                        'uadf_user_id' => $id,
                        'uadf_udf_id' => $user_data['udf_id'],
                        'uadf_type' => 1,
                        'uadf_document_file_name' => $user_data['udf_cv_file_name'],
                        'uadf_created_date' => $this->utc_time
                    );
                    $this->Common_model->insert(TBL_USER_ALL_DOCUMENT_FILES, $request_all_documents_data);
                }

                $request_data['ua_user_id'] = $id;
                $request_data["ua_company_id"] = implode(',', $all_company_list);
                $request_data["ua_sent_count"] = count($all_company_list);
                $request_data['ua_status'] = 2;
                $request_data['ua_send_by'] = 1;
                $request_data['ua_created_date'] = $this->utc_time;

                file_put_contents($this->log_file, "\n\n send_application_to_company_by_email request_data: " . json_encode($request_data) . " \n\n", FILE_APPEND | LOCK_EX);

                $get_app_id = $this->Common_model->insert(TBL_USER_APPLICATIONS, $request_data);

                if ($get_app_id) {
                    // decrease user tariff limit
                    $this->User_model->manage_users_tariff($id, count($all_company_list), false, true, 1);
                    return $get_app_id;
                } else {
                    return false;
                }
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }
        return false;
    }

    /**
     * Description: Use this function for send application to all company by post
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-06-15
     * 
     * @param type $id : CANDIDATE ID
     * @param type $all_company_list
     * @return boolean
     */
    public function send_application_to_company_by_post($id, $user_data, $send_application_limit, $all_company_list) {
        try {
            if (!empty($id) && is_numeric($id) && !empty($all_company_list) && !empty($user_data) && !empty($send_application_limit)) {

                if ($user_data['udf_type'] == 1) {
                    $request_all_documents_data = array(
                        'uadf_user_id' => $id,
                        'uadf_udf_id' => $user_data['udf_id'],
                        'uadf_type' => 1,
                        'uadf_document_file_name' => $user_data['udf_cv_file_name'],
                        'uadf_created_date' => $this->utc_time
                    );
                    $this->Common_model->insert(TBL_USER_ALL_DOCUMENT_FILES, $request_all_documents_data);
                }


                $request_data['ua_user_id'] = $id;
                $request_data["ua_company_id"] = implode(',', $all_company_list);
                $request_data["ua_sent_count"] = count($all_company_list);
                $request_data['ua_status'] = 2;
                $request_data['ua_send_by'] = 2;
                $request_data['ua_created_date'] = $this->utc_time;

                file_put_contents($this->log_file, "\n\n send_application_to_company_by_post request_data: " . json_encode($request_data) . " \n\n", FILE_APPEND | LOCK_EX);

                $get_app_id = $this->Common_model->insert(TBL_USER_APPLICATIONS, $request_data);

                if ($get_app_id) {
                    // decrease user tariff limit
                    $this->User_model->manage_users_tariff($id, count($all_company_list), false, true, 2);
                    return $get_app_id;
                } else {
                    return false;
                }
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }
        return false;
    }

    /**
     * Description: Use this function for generate user cv image and download
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-05-03
     * 
     */
    public function generate_user_cv_image() {

        if (!$this->input->is_ajax_request()) {
            redirect(HOME_PATH);
        }

        $download_file_type = $this->input->post("download_file_type");
        $doc_id = $this->input->post("document_id");
        $ajax_response = array();
        if (
                !empty($download_file_type) &&
                ($download_file_type == 'jpeg' || $download_file_type == 'png')
        ) {
            $user_doc = $this->Common_model->get_user_document_details($doc_id, $this->user_id);

            if (empty($user_doc)) {
                // insert user details
                $request_data = array(
                    "udf_user_id" => $this->user_id,
                    "udf_created_date" => $this->utc_time
                );
                $doc_id = $this->Common_model->insert(TBL_USER_DOCUMENT_FILES, $request_data);
            } else {
                $doc_id = $user_doc['udf_id'];
            }

            $file_name = $doc_id . "_" . uniqid() . "." . $download_file_type;

            $targetPath = UPLOAD_FILE_FOLDER . '/' . UPLOAD_DOCUMENTS_FOLDER . '/' . $doc_id . '/';

            if (!is_dir($targetPath)) {
                mkdir($targetPath, 0777, true);
                chmod($targetPath, 0777);
            }

            //$_POST[data][1] has the base64 encrypted binary codes. 
            //convert the binary to image using file_put_contents
            $savefile = @file_put_contents($targetPath . $file_name, base64_decode(explode(",", $_POST['data'])[1]));
            if ($savefile) {
                chmod($targetPath . $file_name, 0777);
                $ajax_response = array(
                    "success" => TRUE,
                    "file_name" => UPLOAD_ABS_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $doc_id . '/' . $file_name
                );
            } else {
                $ajax_response = array(
                    "success" => FALSE
                );
            }
        } else {
            $ajax_response = array(
                "success" => FALSE
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($ajax_response));
    }

    /**
     * Description: Use this function for generate user cv as PDF only
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2016-01-18
     * 
     * @param type $id
     * @param type $current_lang
     * @param type $doc_id
     * @param type $download
     */
    public function generate_user_cv($id = 0, $current_lang = 'fr', $doc_id = 0, $download = 'false') {

        if (!empty($id) && is_numeric($id)) {
            $data = $this->Common_model->get_user_all_details($id);
            if (!empty($data) && isset($data['user_data']) && !empty($data['user_data'])) {
                $data['current_lang'] = $current_lang;
                $data['download_type'] = 'pdf';
                $content = $this->load->view('users/common_generate_user_cv_' . $current_lang, $data, true);
                if (!empty($content)) {
                    try {
                        set_time_limit(0);
                        $content = trim($content);
                        $file_name = $doc_id . "_" . uniqid() . ".pdf";
                        $targetPath = UPLOAD_FILE_FOLDER . '/' . UPLOAD_DOCUMENTS_FOLDER . '/' . $doc_id . '/';
                        $destFile = $targetPath . $file_name;
                        require_once DOCROOT . '/third_party/mpdf/mpdf.php';
                        $mpdf = new mPDF('c', 'A4', '', '', 5, 5, 10, 5, 15, 5);
                        $mpdf->SetDisplayMode('fullpage');
                        $mpdf->list_indent_first_level = 0;
                        $stylesheet = file_get_contents(ASSETS_PATH . 'css/mpdf.css');
                        $mpdf->WriteHTML($stylesheet, 1);
                        $mpdf->WriteHTML($content);
                        if ($download == 'true') {
                            header("Content-Type: application/pdf");
                            header("Content-Disposition: attachment; filename=" . $file_name . ";");
                            $pdfData = $mpdf->Output($file_name, 'D');
                            die;
                        } else {

                            if (!is_dir($targetPath)) {
                                mkdir($targetPath, 0777, true);
                                chmod($targetPath, 0777);
                            }

                            $pdfData = $mpdf->Output($destFile, 'F');
                            chmod($destFile, 0777);
                            $where = array(
                                'udf_id ' => $doc_id,
                                'udf_status' => 1
                            );

                            $request_data = array(
                                'udf_type' => 2,
                                'udf_rapidjob_cv_file_name' => $file_name,
                                'udf_modified_date' => $this->utc_time
                            );

                            $this->Common_model->update(TBL_USER_DOCUMENT_FILES, $request_data, $where);
                        }
                    } catch (HTML2PDF_exception $e) {
                        $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
                        redirect(HOME_PATH);
                    }
                } else {
                    $this->session->set_flashdata('failure', lang('SHOW_PROFILE_PROVIDE_DETAILS'));
                    redirect(USERS_PATH);
                }
            } else {
                $this->session->set_flashdata('failure', lang('SHOW_PROFILE_PROVIDE_DETAILS'));
                redirect(USERS_PATH);
            }
        } else {
            $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
            redirect(USERS_PATH . '/send_application');
        }
    }

    /**
     * Description: Use this function for show my documents page
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2016-01-19
     * 
     */
    public function my_documents() {
        $data = array();

        try {
            // get user details
            $data['user_data'] = $this->Common_model->get_user_details($this->user_id);
            $data['user_all_documents'] = $this->Common_model->get_user_all_documents($this->user_id);
            if (empty($data['user_data'])) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(USERS_PATH);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH . '/invite_friends');
        }
//        _px($data);
        $this->load->view('common/candidate_header');
        $this->load->view('users/my_documents', $data);
        $this->load->view('common/candidate_footer');
    }

    /**
     * Description: Use this function for change cv / cover letter
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2016-01-10
     * File Allow Extension : jpg, jpeg, png, pdf
     * File Allow Max size: 2 MB
     * 
     */
    public function change_my_documents() {
        try {
            if ($this->input->post() != NULL) {
                $doc_id = $this->input->post('doc_id');
                $user_doc = $this->Common_model->get_user_document_details($doc_id);
                if (empty($user_doc)) {
                    // insert user details
                    $request_data = array(
                        "udf_user_id" => $this->user_id,
                        "udf_created_date" => $this->utc_time
                    );

                    $doc_id = $this->Common_model->insert(TBL_USER_DOCUMENT_FILES, $request_data);
                    if ($doc_id > 0) {
                        // if cv uploaded by user
                        if (!empty($_FILES["my_cv"]["name"])) {
                            $my_cv["file"] = $_FILES["my_cv"];
                            $this->upload_cv_files($my_cv, $doc_id, array('field_name' => 'udf_cv_file_name'));
                            $this->session->set_flashdata("success", lang("UPLOAD_CV_UPLOAD_SUCCESS"));
                        }

                        if (!empty($_FILES["my_cover_latter"]["name"])) {
                            $my_cover_latter["file"] = $_FILES["my_cover_latter"];
                            $this->upload_cv_files($my_cover_latter, $doc_id, array('field_name' => 'udf_cover_latter_file'));
                            $this->session->set_flashdata("success", lang("UPLOAD_COVER_UPLOAD_SUCCESS"));
                        }
                    } else {
                        $this->session->set_flashdata("failure", lang("ERROR_CV_PROBLEM_IN_UPLOAD"));
                        redirect(USERS_PATH . '/send_application');
                    }
                } else {
                    $request_details = array(
                        "udf_modified_date" => $this->utc_time
                    );

                    $where_doc = array(
                        "udf_id" => $doc_id,
                        "udf_user_id" => $this->user_id,
                        "udf_status" => 1
                    );
                    $is_updated = $this->Common_model->update(TBL_USER_DOCUMENT_FILES, $request_details, $where_doc);
                    if ($is_updated > 0) {

                        // if cv uploaded by user
                        if (!empty($_FILES["my_cv"]["name"])) {
                            $my_cv["file"] = $_FILES["my_cv"];
                            $this->upload_cv_files($my_cv, $doc_id, array('field_name' => 'udf_cv_file_name'));
                            $this->session->set_flashdata("success", lang("UPLOAD_CV_UPLOAD_SUCCESS"));
                        }

                        if (!empty($_FILES["my_cover_latter"]["name"])) {
                            $my_cover_latter["file"] = $_FILES["my_cover_latter"];
                            $this->upload_cv_files($my_cover_latter, $doc_id, array('field_name' => 'udf_cover_latter_file'));
                            $this->session->set_flashdata("success", lang("UPLOAD_COVER_UPLOAD_SUCCESS"));
                        }
                    } else {
                        $this->session->set_flashdata("failure", lang("SEND_APPLICATION_UPDATE_ERROR"));
                        redirect(USERS_PATH . '/send_application');
                    }
                }
            } else {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(USERS_PATH . '/send_application');
            }
            redirect(USERS_PATH . '/my_documents');
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }
        $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
        redirect(USERS_PATH . '/send_application');
    }

    /**
     * Description: Use this function for upload cv files
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2016-01-10
     * File Allow Extension : jpg, jpeg, png, pdf
     * File Allow Max size: 2 MB
     * 
     */
    public function remove_my_documents() {
        try {
            $doc_id = $this->input->get('id');
            if (!empty($doc_id) && is_numeric($doc_id)) {

                //check user document exist?
                $user_doc = $this->Common_model->get_user_all_documents($doc_id);
                if (!empty($user_doc)) {
                    $request_details = array(
                        "uadf_document_file_name" => '',
                        "uadf_status" => 9,
                        "uadf_modified_date" => $this->utc_time
                    );

                    $where_doc = array(
                        "uadf_id" => $doc_id,
                        "uadf_user_id" => $this->user_id,
                        "uadf_status" => 1
                    );

                    $is_updated = $this->Common_model->update(TBL_USER_ALL_DOCUMENT_FILES, $request_details, $where_doc);
                    if ($is_updated > 0) {
                        $this->session->set_flashdata("success", lang('MY_DOCUMENTS_CV_REMOVE_SUCCESS'));
                    } else {
                        $this->session->set_flashdata("success", lang('MY_DOCUMENTS_CV_REMOVE_FAILURE'));
                    }
                    redirect(USERS_PATH . '/my_documents');
                } else {
                    $this->session->set_flashdata("failure", lang('MY_DOCUMENTS_DOC_NOT_FOUND'));
                }
            } else {
                $this->session->set_flashdata("failure", lang('COMMON_INVALID_PARAMS'));
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang('COMMON_PROBLEM_IN_ACTION'));
            redirect(USERS_PATH . '/my_documents');
        }
        $this->session->set_flashdata("failure", lang('MY_DOCUMENTS_DOC_NOT_FOUND'));
        redirect(USERS_PATH . '/my_documents');
    }

    /**
     * Description: Use this function for get user send application list
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2016-01-21
     * 
     */
    public function sent_application_list() {
        $data = array();

        try {
            //check user send application list
            $where_app = array(
                'ua_user_id' => $this->user_id,
                'ua_status !=' => 9
            );
            $column_app = '*';
            $data['user_data'] = $this->Common_model->get_all_rows(TBL_USER_APPLICATIONS, $column_app, $where_app);
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }
        $this->load->view('common/candidate_header');
        $this->load->view('users/sent_application_list', $data);
        $this->load->view('common/candidate_footer');
    }

    /**
     * Description: Use this function for upload cv files into uploads directory
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2016-01-10
     * File Allow Extension : jpg, jpeg, png, pdf
     * File Allow Max size: 2 MB
     * 
     * @param type $request_file
     * @param type $doc_id
     * @param type $data
     */
    public function upload_cv_files($request_file, $doc_id, $data) {
        try {
            if (!empty($request_file["file"]) && !empty($doc_id) && is_numeric($doc_id) && !empty($data)) {
                $temp = explode(".", $request_file["file"]["name"]);
                $extension = strtolower(end($temp));

                if ($request_file["file"]["error"] > 0) {
                    $this->session->set_flashdata("failure", lang('ERROR_CV_INVALID_FILE'));
                    redirect(USERS_PATH . '/send_application');
                }

                if (!empty($data['field_name']) && $data['field_name'] == 'udf_cover_latter_file') {
                    if (!in_array($extension, array("jpg", "jpeg", "png"))) {
                        $this->session->set_flashdata("failure", lang('ERROR_COVER_INVALID_EXTENSION'));
                        redirect(USERS_PATH . '/send_application');
                    }
                } else {
                    if (!in_array($extension, array("jpg", "jpeg", "pdf", "png"))) {
                        $this->session->set_flashdata("failure", lang('ERROR_CV_INVALID_EXTENSION'));
                        redirect(USERS_PATH . '/send_application');
                    }
                }

                if ($request_file["file"]["size"] > DEFAULT_MAXIMUM_CV_SIZE_LIMIT_JS) { // 5 MB
                    $this->session->set_flashdata("failure", lang('ERROR_CV_INVALID_SIZE'));
                    redirect(USERS_PATH . '/send_application');
                }

                $upload_path = UPLOAD_FILE_FOLDER . "/" . UPLOAD_DOCUMENTS_FOLDER . "/" . $doc_id;
                $new_file = do_upload($upload_path, $request_file, false, false);

                if (!empty($new_file)) {
                    $where = array(
                        'udf_id ' => $doc_id,
                        'udf_status' => 1
                    );

                    $request_data = array(
                        'udf_modified_date' => $this->utc_time
                    );

                    if (!empty($data['field_name']) && $data['field_name'] == 'udf_cv_file_name') {
                        $request_data['udf_cv_file_name'] = $new_file;
                    }
                    if (!empty($data['field_name']) && $data['field_name'] == 'udf_cover_latter_file') {
                        $request_data['udf_cover_latter_file'] = $new_file;
                    }
                    $is_updated = $this->Common_model->update(TBL_USER_DOCUMENT_FILES, $request_data, $where);
                } else {
                    $this->session->set_flashdata("failure", lang('ERROR_CV_PROBLEM_IN_UPLOAD'));
                    redirect(USERS_PATH);
                }
            } else {
                $this->session->set_flashdata("failure", lang('ERROR_CV_PROBLEM_IN_UPLOAD'));
                redirect(USERS_PATH);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }
    }

    /**
     * Description: Use this function for upload profile photo into directory 
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2016-12-17
     * 
     */
    public function upload_image() {

        try {
            if (isset($_FILES) && count($_FILES) > 0 && !empty($_FILES['image']['name'])) {
                if (isset($_FILES['image']['name']) && $_FILES['image']['error'] == 0) {
                    list($width, $height) = getimagesize($_FILES['image']["tmp_name"]);
                    $extention = explode(".", $_FILES['image']['name']);
                    $extention = strtolower(end($extention));
                    $validExtentions = array('png', 'jpg', 'jpeg');
                    if (!in_array($extention, $validExtentions)) {
                        $this->session->set_flashdata("failure", lang('ERROR_INVALID_EXTENSION'));
                        redirect(USERS_PATH);
                    } else if ($_FILES['image']['size'] > DEFAULT_IMAGE_SIZE_LIMIT) {
                        $this->session->set_flashdata("failure", lang('ERROR_INVALID_SIZE'));
                        redirect(USERS_PATH);
                    }

                    if ($width < 200 || $height < 200) {
                        $this->session->set_flashdata("failure", lang('ERROR_INVALID_DIMENSION'));
                        redirect(USERS_PATH);
                    }

                    $upload_path = UPLOAD_FILE_FOLDER . "/" . UPLOAD_USERS_FOLDER . "/" . $this->user_id;
                    $new_image = do_upload($upload_path, $_FILES, false);
                    if (!empty($new_image)) {
                        $where = array(
                            'u_id ' => $this->user_id,
                            'u_status' => 1
                        );
                        $file_name = $this->Common_model->get_single_row(TBL_USERS, "u_image", $where);
                        if (!empty($file_name)) {
                            @unlink($upload_path . '/' . $file_name['u_image']);
                            $this->Common_model->update(TBL_USERS, array("u_image" => $new_image), $where);

                            if (file_exists(UPLOAD_FILE_FOLDER . '/' . UPLOAD_USERS_FOLDER . '/' . $this->user_id . "/" . $new_image)) {
                                $photo["profile_photo"] = UPLOAD_USERS_FOLDER . '/' . $this->user_id . "/" . $new_image;
                            } else {
                                $photo["profile_photo"] = '';
                            }

                            $this->session->set_userdata($photo);
                            $this->session->set_flashdata("success", lang('PHOTO_UPLOAD_SUCCESS'));
                            redirect(USERS_PATH);
                        } else {
                            $this->session->set_flashdata("failure", lang('ERROR_PROBLEM_IN_UPLOAD'));
                            redirect(USERS_PATH);
                        }
                    } else {
                        $this->session->set_flashdata("failure", lang('ERROR_PROBLEM_IN_UPLOAD'));
                        redirect(USERS_PATH);
                    }
                } else {
                    $this->session->set_flashdata("failure", lang('ERROR_INVALID_FILE'));
                    redirect(USERS_PATH);
                }
            } else {
                $this->session->set_flashdata("failure", lang('ERROR_INVALID_FILE'));
                redirect(USERS_PATH);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }
        redirect(USERS_PATH);
    }

    /**
     * 
     * This function is use for generate payment form
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-12
     * 
     * @return type
     * 
     */
    public function form_payment() {
        try {

            // get user details
            $user_data = $this->Common_model->get_user_details($this->user_id);

            if (empty($user_data)) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(USERS_PATH);
            }

            if ($this->input->post() != NULL) {

                $tariff = !empty($this->input->post('vads_tarrif')) && is_numeric($this->input->post('vads_tarrif')) ? (int) trim($this->Common_model->escape_data($this->input->post('vads_tarrif'))) : '';
                $total_send_application = !empty($this->input->post('vads_tarrif')) && is_numeric($this->input->post('vads_tarrif')) ? (int) trim($this->Common_model->escape_data($this->input->post('vads_tarrif'))) : '';
                $all_selected_ids = !empty($this->input->post('all_selected_ids')) ? $this->Common_model->escape_data($this->input->post('all_selected_ids')) : '';
                $application_send_by = $this->input->post('application_send_by');
                
                if (!empty($tariff) && is_numeric($tariff) && ($application_send_by == 1 || $application_send_by == 2)) {
                    require ("function.php");
                    $lang = $this->current_lang;
                    include($lang . '.php');

                    // load settings model from admin
                    $this->load->model('Setting_model');

                    //get payment rate per email 
                    $setting_array = $this->Setting_model->get_settings();

                    if ($application_send_by == 1) {
                        $user_current_points = (int) $user_data['ud_points'];
                        if (!empty($user_current_points) && $tariff > $user_current_points) {
                            $tariff = (int) $tariff - $user_current_points;
                        }
                        $payment_rate = DEFAULT_PAYMENT_RATE_PER_EMAIL;
                        if (!empty($setting_array) && isset($setting_array['payment_in_cent_per_email'])) {
                            $payment_rate = (int) $setting_array['payment_in_cent_per_email'];
                        }
                    } else if ($application_send_by == 2) {
                        $user_current_points = (int) $user_data['ud_send_by_post_points'];
                        if (!empty($user_current_points) && $tariff > $user_current_points) {
                            $tariff = (int) $tariff - $user_current_points;
                        }
                        $payment_rate = DEFAULT_PAYMENT_RATE_PER_POST;
                        if (!empty($setting_array) && isset($setting_array['payment_rate_for_send_application_by_post'])) {
                            $payment_rate = (int) $setting_array['payment_rate_for_send_application_by_post'];
                        }
                    }


                    $user_full_name = $this->session->userdata("user_first_name");
                    $user_name = !empty($user_full_name) ? strlen($user_full_name) > 120 ? substr($user_full_name, 0, 120) . "..." : $user_full_name : '';
                    $address = !empty($user_data['ud_address']) ? strlen($user_data['ud_address']) > 250 ? substr($user_data['ud_address'], 0, 250) . "..." : $user_data['ud_address'] : '';
                    $city = !empty($user_data['ud_city']) ? strlen($user_data['ud_city']) > 50 ? substr($user_data['ud_city'], 0, 50) . "..." : $user_data['ud_city'] : '';
                    $postal_code = !empty($user_data['ud_postal_code']) ? strlen($user_data['ud_postal_code']) > 20 ? substr($user_data['ud_postal_code'], 0, 20) . "..." : $user_data['ud_postal_code'] : '';

                    $_REQUEST['lang'] = $this->current_lang;
                    $_REQUEST['vads_order_id'] = str_rand_access_token(6, 'numeric');
                    $_REQUEST['vads_cust_id'] = $this->user_id;
                    $_REQUEST['vads_cust_name'] = $user_name;
                    $_REQUEST['vads_cust_address'] = $address;
                    $_REQUEST['vads_cust_zip'] = $postal_code;
                    $_REQUEST['vads_cust_city'] = $city;

                    $_REQUEST['vads_amount'] = (int) $tariff * $payment_rate;

                    if (!empty($user_data['u_email']) && filter_var($user_data['u_email'], FILTER_VALIDATE_EMAIL)) {
                        $_REQUEST['vads_cust_email'] = $user_data['u_email'];
                    }
                    
                    $payment_data = array(
                        'application_send_by' => $application_send_by,
                        'amount' => $_REQUEST['vads_amount'],
                        'tariff' => $tariff,
                        'total_send_application' => $total_send_application,
                        'all_selected_ids' => $all_selected_ids
                    );
                    
                    $this->session->set_userdata($payment_data);

                    $form = get_formHtml_request($_REQUEST, $lang);
                    $conf_txt = parse_ini_file("conf.txt");
                    if ($conf_txt['debug'] == 0) {
                        echo $form;
                    } else {
                        echo (display_form($lang, $form));
                    }
                } else {
                    $this->session->set_flashdata("failure", lang("FREE_ITEM_CHOOSE_FORMULA_AND_LIMIT"));
                    redirect(USERS_PATH . '/invite_friends');
                }
            } else {
                $this->session->set_flashdata("failure", lang("FREE_ITEM_CHOOSE_FORMULA_AND_LIMIT"));
                redirect(USERS_PATH . '/invite_friends');
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH . '/invite_friends');
        }
    }

    /**
     * 
     * This function is use for store payment response
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-12
     * 
     * @return type
     * 
     */
    public function payment_response() {
        try {
            if ($this->input->post() != NULL) {
                $this->load->model('Payment_model');
                if ($this->input->post('vads_result') == '00') {
                    $success_response = $this->Payment_model->store_success_payment();
                    if ($success_response) {
                        $this->send_application_to_all_company();
                    } else {
                        $this->session->set_flashdata('failure', lang('PAYMENT_STORE_FAILED'));
                    }
                } else {
                    $failed_response = $this->Payment_model->store_failed_payment();
                    if ($failed_response) {
                        $this->session->set_flashdata('failure', lang('PAYMENT_FAILED'));
                    } else {
                        $this->session->set_flashdata('failure', lang('PAYMENT_STORE_FAILED_DETAILS'));
                    }
                }
            } else {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH . '/invite_friends');
        }

        redirect(USERS_PATH . '/invite_friends');
    }

    /**
     * Description: Use this function for redirect user to paypal payment
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-05-19
     * 
     */
    public function form_paypal() {

        try {
            // get user details
            $user_data = $this->Common_model->get_user_details($this->user_id);

            if (empty($user_data)) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(USERS_PATH);
            }

            if ($this->input->post() != NULL) {

                $tariff = !empty($this->input->post('vads_tarrif')) && is_numeric($this->input->post('vads_tarrif')) ? (int) trim($this->Common_model->escape_data($this->input->post('vads_tarrif'))) : '';
                $total_send_application = !empty($this->input->post('vads_tarrif')) && is_numeric($this->input->post('vads_tarrif')) ? (int) trim($this->Common_model->escape_data($this->input->post('vads_tarrif'))) : '';
                $all_selected_ids = !empty($this->input->post('all_selected_ids')) ? $this->Common_model->escape_data($this->input->post('all_selected_ids')) : '';
                $application_send_by = $this->input->post('application_send_by');

                if (!empty($tariff) && is_numeric($tariff) && ($application_send_by == 1 || $application_send_by == 2)) {
                    // load settings model from admin
                    $this->load->model('Setting_model');

                    //get payment rate per email 
                    $setting_array = $this->Setting_model->get_settings();

                    if ($application_send_by == 1) {
                        $user_current_points = (int) $user_data['ud_points'];
                        if (!empty($user_current_points) && $tariff > $user_current_points) {
                            $tariff = $tariff - $user_current_points;
                        }
                        $payment_rate = DEFAULT_PAYMENT_RATE_PER_EMAIL;
                        if (!empty($setting_array) && isset($setting_array['payment_in_cent_per_email'])) {
                            $payment_rate = (int) $setting_array['payment_in_cent_per_email'];
                        }
                    } else if ($application_send_by == 2) {
                        $user_current_points = (int) $user_data['ud_send_by_post_points'];
                        if (!empty($user_current_points) && $tariff > $user_current_points) {
                            $tariff = $tariff - $user_current_points;
                        }
                        $payment_rate = DEFAULT_PAYMENT_RATE_PER_POST;
                        if (!empty($setting_array) && isset($setting_array['payment_rate_for_send_application_by_post'])) {
                            $payment_rate = (int) $setting_array['payment_rate_for_send_application_by_post'];
                        }
                    }

                    $amount = (int) $tariff * $payment_rate;

                    $payment_data = array(
                        'application_send_by' => $application_send_by,
                        'amount' => $amount,
                        'tariff' => $tariff,
                        'total_send_application' => $total_send_application,
                        'all_selected_ids' => $all_selected_ids
                    );

                    $this->session->set_userdata($payment_data);

                    // include paypal library
                    require_once DOCROOT . '/third_party/paypal_sdk.php';
                    $cdo = new CdoPay();
                    $url = $cdo->payment_using_web($amount);
                    header("Location:" . $url);
                } else {
                    $this->session->set_flashdata("failure", lang("FREE_ITEM_CHOOSE_FORMULA_AND_LIMIT"));
                    redirect(USERS_PATH . '/invite_friends');
                }
            } else {
                redirect(USERS_PATH . '/invite_friends');
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH . '/invite_friends');
        }
    }

    /**
     * 
     * This function is use for store success payment response
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-05-19
     * 
     * @return type
     * 
     */
    public function paypal_success() {

        try {
            if ($this->input->get() != NULL) {
                $this->load->model('Payment_model');
                $payment_id = !empty($this->input->get('paymentId')) ? $this->input->get('paymentId') : '';
                $token = !empty($this->input->get('token')) ? $this->input->get('token') : '';
                $payer_id = !empty($this->input->get('PayerID')) ? $this->input->get('PayerID') : '';
                $amount = $this->session->userdata('amount');
                if (!empty($payment_id) && !empty($token) && !empty($payer_id) && !empty($amount)) {
                    $success_response = $this->Payment_model->store_paypal_success_payment();
                    if ($success_response) {
                        $this->send_application_to_all_company();
                    } else {
                        $this->session->set_flashdata('failure', lang('PAYMENT_STORE_FAILED'));
                    }
                } else {
                    $failed_response = $this->Payment_model->store_paypal_failed_payment();
                    if ($failed_response) {
                        $this->session->set_flashdata('failure', lang('PAYMENT_FAILED'));
                    } else {
                        $this->session->set_flashdata('failure', lang('PAYMENT_STORE_FAILED_DETAILS'));
                    }
                }
            } else {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH . '/invite_friends');
        }
        $this->session->unset_userdata('amount');
        $this->session->unset_userdata('tariff');
        $this->session->unset_userdata('application_send_by');

        redirect(USERS_PATH . '/invite_friends');
    }

    /**
     * 
     * This function is use for store failed payment response
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-05-19
     * 
     * @return type
     * 
     */
    public function paypal_failure() {
        try {
            if ($this->input->get() != NULL) {
                $this->load->model('Payment_model');
                $failed_response = $this->Payment_model->store_paypal_failed_payment();
                if ($failed_response) {
                    $this->session->set_flashdata('failure', lang('PAYMENT_FAILED'));
                } else {
                    $this->session->set_flashdata('failure', lang('PAYMENT_STORE_FAILED_DETAILS'));
                }
            } else {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH . '/invite_friends');
        }
        $this->session->unset_userdata('amount');
        $this->session->unset_userdata('tariff');
        $this->session->unset_userdata('application_send_by');

        redirect(USERS_PATH . '/invite_friends');
    }

    /**
     * 
     * This function is use for generate rapidjob cv from show profile page "Build CV" Button
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-12
     * 
     * @return type
     * 
     */
    public function generate_rapidjob_cv() {
        try {
            $user_doc = $this->Common_model->get_user_document_details(0, $this->user_id);

            if (!empty($user_doc) && !empty($user_doc['udf_id']) && is_numeric($user_doc['udf_id'])) {
                $doc_id = $user_doc['udf_id'];
            } else {
                // insert user documents detail if not exists
                $insert_data = array(
                    "udf_user_id" => $this->user_id,
                    "udf_type" => 2,
                    "udf_created_date" => $this->utc_time
                );

                $doc_id = $this->Common_model->insert(TBL_USER_DOCUMENT_FILES, $insert_data);
            }

            if ($doc_id > 0) {
                // generate new pdf
                $this->generate_user_cv($this->user_id, $this->current_lang, $doc_id);
                $this->session->set_flashdata("success", lang("SHOW_PROFILE_GENERATE_CV_SUCCESS"));
            } else {
                $this->session->set_flashdata("failure", lang("SHOW_PROFILE_GENERATE_CV_FAILED"));
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }
        redirect(USERS_PATH);
    }

    /**
     * 
     * This function is use for delete candidate experience
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-12
     * 
     * @return type
     * 
     */
    public function delete_experience() {
        $id = $this->Common_model->escape_data($this->input->get_post("id"));

        $response = array();
        if (empty($id)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = false;
                $response['message'] = lang("COMMON_INVALID_PARAMS");
            }
        } else {
            $where = array();
            $where['ue_user_id'] = $this->user_id;
            $where['ue_id'] = $id;

            $request_data = array();
            $request_data['ue_status'] = 9;
            $request_data['ue_modified_date'] = $this->utc_time;

            $update_state = $this->Common_model->update(TBL_USER_EXPERIANCES, $request_data, $where);

            if ($update_state > 0) {
                $response['success'] = true;
                $response['message'] = lang("DELETE_EXPERIENCE_SUCCESS");
            } else {
                $response['success'] = false;
                $response['message'] = lang("DELETE_EXPERIENCE_FAILED");
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * 
     * This function is use for delete candidate experience
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-12
     * 
     * @return type
     * 
     */
    public function delete_training() {
        $id = $this->Common_model->escape_data($this->input->get_post("id"));

        $response = array();
        if (empty($id)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = false;
                $response['message'] = lang("COMMON_INVALID_PARAMS");
            }
        } else {
            $where = array();
            $where['ut_user_id'] = $this->user_id;
            $where['ut_id'] = $id;

            $request_data = array();
            $request_data['ut_status'] = 9;
            $request_data['ut_modified_date'] = $this->utc_time;

            $update_state = $this->Common_model->update(TBL_USER_TRAININGS, $request_data, $where);

            if ($update_state > 0) {
                $response['success'] = true;
                $response['message'] = lang("DELETE_TRAINING_SUCCESS");
            } else {
                $response['success'] = false;
                $response['message'] = lang("DELETE_TRAINING_FAILED");
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * 
     * This function is use for delete candidate experience
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-12
     * 
     * @return type
     * 
     */
    public function delete_skill() {
        $id = $this->Common_model->escape_data($this->input->get_post("id"));

        $response = array();
        if (empty($id)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = false;
                $response['message'] = lang("COMMON_INVALID_PARAMS");
            }
        } else {
            $where = array();
            $where['us_user_id'] = $this->user_id;
            $where['us_id'] = $id;

            $request_data = array();
            $request_data['us_status'] = 9;
            $request_data['us_modified_date'] = $this->utc_time;

            $update_state = $this->Common_model->update(TBL_USER_SKILLS, $request_data, $where);

            if ($update_state > 0) {
                $response['success'] = true;
                $response['message'] = lang("DELETE_SKILL_SUCCESS");
            } else {
                $response['success'] = false;
                $response['message'] = lang("DELETE_SKILL_FAILED");
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * 
     * This function is use for delete candidate experience
     * 
     * @author Nitinkumar Vaghani
     * Modified Date :- 2017-05-12
     * 
     * @return type
     * 
     */
    public function delete_hobby() {
        $id = $this->Common_model->escape_data($this->input->get_post("id"));

        $response = array();
        if (empty($id)) {
            if ($this->input->is_ajax_request()) {
                $response['success'] = false;
                $response['message'] = lang("COMMON_INVALID_PARAMS");
            }
        } else {
            $where = array();
            $where['uh_user_id'] = $this->user_id;
            $where['uh_id'] = $id;

            $request_data = array();
            $request_data['uh_status'] = 9;
            $request_data['uh_modified_date'] = $this->utc_time;

            $update_state = $this->Common_model->update(TBL_USER_HOBBIES, $request_data, $where);

            if ($update_state > 0) {
                $response['success'] = true;
                $response['message'] = lang("DELETE_HOBBY_SUCCESS");
            } else {
                $response['success'] = false;
                $response['message'] = lang("DELETE_HOBBY_FAILED");
            }
        }

        header('Content-Type: application/json');
        echo json_encode($response);
        exit;
    }

    /**
     * Description: Use this function for display my orders
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-05-22
     * 
     */
    public function my_orders() {
        $data = array();
        try {
            $data['order_data'] = $this->User_model->get_user_order_details($this->user_id);
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }

        $this->load->view('common/candidate_header');
        $this->load->view('users/my_orders', $data);
        $this->load->view('common/candidate_footer');
    }

    /**
     * @author Nitinkumar vaghani
     * 
     * Description : use this function for add cv to favourite
     * 
     * Created date : 2017-04-24
     */
    public function add_to_favourite() {
        if (!$this->input->is_ajax_request()) {
            redirect(USERS_PATH);
        }
        try {
            $response = $this->User_model->add_to_favourite();
            if ($response) {
                $response = array(
                    "success" => true,
                    "message" => lang("SEARCH_PAGE_ADD_TO_FAVORITE_SUCCESS")
                );
            } else {
                $response = array(
                    "success" => false,
                    "message" => lang("SEARCH_PAGE_ADD_TO_FAVORITE_FAILED")
                );
            }
        } catch (Exception $e) {
            $response = array(
                "success" => false,
                "message" => lang("COMMON_PROBLEM_IN_ACTION")
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    /**
     * @author Nitinkumar vaghani
     * 
     * Description : use this function for remove cv from favourite
     * 
     * Created date : 2017-04-24
     */
    public function remove_from_favourite() {
        if (!$this->input->is_ajax_request()) {
            redirect(USERS_PATH);
        }
        try {
            $response = $this->User_model->remove_from_favourite();
            if ($response) {
                $response = array(
                    "success" => true,
                    "message" => lang("SEARCH_PAGE_REMOVE_FROM_FAVORITE_SUCCESS")
                );
            } else {
                $response = array(
                    "success" => false,
                    "message" => lang("SEARCH_PAGE_REMOVE_FROM_FAVORITE_FAILED")
                );
            }
        } catch (Exception $e) {
            $response = array(
                "success" => false,
                "message" => lang("COMMON_PROBLEM_IN_ACTION")
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    public function convert_into_br($input_str) {
        $return_string = str_replace(array("\r\n", "\r", "\n", "\\r", "\\n", "\\r\\n"), "\n", $input_str);
        return $return_string;
    }

    /**
     * Description: Use this function for signned out.
     * 
     * Last modified by : Nitinkumar Vaghani
     * Last modified date : 2016-12-17
     * 
     */
    public function signout() {

        try {
            $user_session_data = array(
                "user_id" => '',
                "user_first_name" => '',
                "user_last_name" => '',
                "user_type" => '',
                "profile_photo" => ''
            );
            $this->session->unset_userdata($user_session_data);
            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('profile_photo');
            $this->session->sess_destroy();
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(HOME_PATH);
        }
        redirect(HOME_PATH);
    }

}
