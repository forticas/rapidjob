<?php

/**
 * 
 * Description : Use this controller for manage recruiter profile
 * 
 * @author Nitinkumar Vaghani
 * 
 * Created date : 2017-04-18
 */
class Professional extends MY_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model("Professional_model");
    }

    /**
     * Description : Use this function to display profile
     * 
     * @author Nitinkumar Vaghani
     * 
     * Created date : 2017-04-18
     */
    public function index() {
        try {
            if ($this->input->post() != NULL) {

                // validate build cv form
                $this->form_validation->set_rules('email', lang("PROF_MY_PROFILE_LOGIN_EMAIL"), 'required');
                $this->form_validation->set_rules('civility', lang("PROF_MY_PROFILE_CIVIL_STATUS"), 'required');
                $this->form_validation->set_rules('first_name', lang("SIGNUP_FORM_FIRST_NAME"), 'required|alpha_numeric_spaces|min_length[2]|max_length[150]');
                $this->form_validation->set_rules('last_name', lang("SIGNUP_FORM_LAST_NAME"), 'required|alpha_numeric_spaces|min_length[2]|max_length[150]');
                $this->form_validation->set_rules('phone', lang("COMMON_PHONE"), 'required|numeric|min_length[8]|max_length[15]');
                $this->form_validation->set_rules('social_reason', lang("PROF_REGISTER_SOCIAL_REASON_OF_COMPANY"), 'required|min_length[2]');
                $this->form_validation->set_rules('activity_area[]', lang("PROF_MY_PROFILE_ACTIVITY_AREA"), 'required');
                $this->form_validation->set_rules('address', lang("PROF_MY_PROFILE_COMPANY_ADDRESS"), 'required|min_length[2]');
                $this->form_validation->set_rules('city', lang("COMMON_CITY"), 'required');
                $this->form_validation->set_rules('postal_code', lang("COMMON_POSTAL_CODE"), 'required|numeric|min_length[4]|max_length[15]');
                $this->form_validation->set_rules('country', lang("COMMON_COUNTRY"), 'required');
                if ($this->form_validation->run()) {

                    /** edit profile * */
                    $email = trim($this->input->post("email"));
                    $where = array(
                        "LOWER(u_email)" => strtolower($email),
                        "u_id !=" => $this->user_id,
                        "u_type" => 2
                    );

                    $check_user = $this->Common_model->get_single_row(TBL_USERS, "u_id", $where);
                    if (!empty($check_user)) {
                        $this->form_validation->set_rules('email', "email", "exist");
                        $this->form_validation->set_message('exist', lang('COMMON_EMAIL_ALREADY_REGISTERED'));
                        $this->form_validation->run();
                    } else {
                        if ($this->Professional_model->update_profesional_details()) {
                            $this->session->set_flashdata("success", lang('SHOW_PROFILE_PAGE_BIO_UPDATE_SUCCESS'));
                        } else {
                            $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_BIO_UPDATE_FAILED'));
                        }
                        redirect(PROFESSIONAL_PATH);
                    }
                }
            }

            $data = $this->Professional_model->get_user_data();


            if (empty($data)) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(HOME_PATH);
            }

            $default_data = $this->Common_model->get_default_details();
            $data = array_merge($data, $default_data);

            $nationality = $this->Common_model->get_nationalities();
            $data = array_merge($data, $nationality);


            // load settings model from admin
            $this->load->model('Setting_model');

            //get subscription price and duration
            $setting_array = $this->Setting_model->get_settings();
            $recruiter_subscription_plan_price = DEFAULT_RECRUITER_SUBSCRITION_PLAN_PRICE;

            if (!empty($setting_array) && isset($setting_array['recruiter_subscription_plan_price'])) {
                $recruiter_subscription_plan_price = $setting_array['recruiter_subscription_plan_price'];
            }

            $data['recruiter_subscription_plan_price'] = $recruiter_subscription_plan_price;
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(HOME_PATH);
        }

        $this->load->view('common/professional_header');
        $this->load->view('professional/edit_profile', $data);
        $this->load->view('common/professional_footer');
    }

    /**
     * Description : Use this function to display resume database
     * 
     * @author Nitinkumar Vaghani
     * 
     * Created date : 2017-04-18
     */
    public function resume_database() {
        try {
            $default_data = $this->Common_model->get_default_details();

            $default_data['search_by_job_type'] = '';
            $default_data['search_by_activity_area'] = '';
            $default_data['search_by_work_place'] = '';
            $default_data['request_page'] = 0;

            if ($this->input->post() !== NULL && $this->input->post('search_cv') == 'true') {
                $activity_area = $this->input->post('activity_area[]');
                $work_place = $this->input->post('work_place[]');
                $default_data['search_by_job_type'] = trim($this->input->post('job_type'));
                $default_data['search_by_activity_area'] = !empty($activity_area) && count($activity_area) > 0 ? array_filter($activity_area) : '';
                $default_data['search_by_work_place'] = !empty($work_place) && count($work_place) > 0 ? array_filter($work_place) : '';
                $default_data['request_page'] = !empty($this->input->post('request_page')) && is_numeric($this->input->post('request_page')) ? trim($this->input->post('request_page')) : 0;
            }

            if ($this->input->post() !== NULL && $this->input->post('view_more') == 'true') {
                $activity_area = $this->input->post('activity_area[]');
                $work_place = $this->input->post('work_place[]');

                $activity_area = !empty($activity_area) && count($activity_area) > 0 ? array_filter($activity_area) : '';
                $work_place = !empty($work_place) && count($work_place) > 0 ? array_filter($work_place) : '';

                if (isset($activity_area[0]) && !empty($activity_area[0])) {
                    $activity_area = explode(',', $activity_area[0]);
                }

                if (isset($work_place[0]) && !empty($work_place[0])) {
                    $work_place = explode(',', $work_place[0]);
                }

                $default_data['search_by_job_type'] = trim($this->input->post('job_type'));
                $default_data['search_by_activity_area'] = $activity_area;
                $default_data['search_by_work_place'] = $work_place;
                $default_data['request_page'] = !empty($this->input->post('request_page')) && is_numeric($this->input->post('request_page')) ? trim($this->input->post('request_page')) : 0;
            }

            $application_data = $this->Professional_model->get_user_applications($default_data);

            $application_data = array_merge($application_data, $default_data);
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(HOME_PATH);
        }

        $this->load->view('common/professional_header');
        $this->load->view('professional/resume_database', $application_data);
        $this->load->view('common/professional_footer');
    }

    /**
     * Description : Use this function to display resume details
     * 
     * @author Nitinkumar Vaghani
     * 
     * Created date : 2017-04-18
     * 
     * @param type $id => Candidate ID
     */
    public function resume_details($id) {
        if (empty($id) && !is_numeric($id)) {
            $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
            redirect(PROFESSIONAL_PATH . '/resume_database');
        }

        $data = $this->Common_model->get_user_all_details($id, $this->user_id);
        if (empty($data)) {
            $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_BIO_UPDATE_FAILED'));
            redirect(PROFESSIONAL_PATH . '/resume_database');
        }

        $this->load->view('common/professional_header');
        $this->load->view('professional/resume_details', $data);
        $this->load->view('common/professional_footer');
    }

    /**
     * Description : Use this function to display proirity resume 
     * 
     * @author Nitinkumar Vaghani
     * 
     * Created date : 2017-04-18
     */
    public function priority_resume() {

        // get user details
        $where_user = array(
            'u_id' => $this->user_id,
            'u_status' => 1,
        );

        $join_array = array(
            TBL_COMPANY_DETAILS => "u_id = cd_user_id "
        );

        $column_user = "*";
        $user_data = $this->Common_model->get_single_row(TBL_USERS, $column_user, $where_user, $join_array, 'LEFT');

        if (empty($user_data)) {
            $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
            redirect(HOME_PATH);
        }

        // load settings model from admin
        $this->load->model('Setting_model');

        //get subscription price and duration
        $setting_array = $this->Setting_model->get_settings();
        $recruiter_subscription_plan_price = DEFAULT_RECRUITER_SUBSCRITION_PLAN_PRICE;
        $recruiter_subscription_plan_duration = DEFAULT_RECRUITER_SUBSCRITION_PLAN_DURATION;

        if (!empty($setting_array) && isset($setting_array['recruiter_subscription_plan_price'])) {
            $recruiter_subscription_plan_price = $setting_array['recruiter_subscription_plan_price'];
        }
        if (!empty($setting_array) && isset($setting_array['recruiter_subscription_plan_duration'])) {
            $recruiter_subscription_plan_duration = $setting_array['recruiter_subscription_plan_duration'];
        }


        $data['user_data'] = $user_data;
        $data['recruiter_subscription_plan_price'] = $recruiter_subscription_plan_price;
        $data['recruiter_subscription_plan_duration'] = $recruiter_subscription_plan_duration;

        $data['user_data'] = $user_data;
        $this->load->view('common/professional_header');
        $this->load->view('professional/priority_resume', $data);
        $this->load->view('common/professional_footer');
    }

    /**
     * Description : Use this function to display send resume replay to candidate
     * 
     * @author Nitinkumar Vaghani
     * 
     * Created date : 2017-04-18
     */
    public function search_priority_resume() {

        try {
            $user_data = $this->Professional_model->get_user_data();

            if (empty($user_data)) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(PROFESSIONAL_PATH);
            }

            if ($user_data['cd_is_prime'] != 1 || empty($user_data['cd_plan_expiry_date']) || $user_data['cd_plan_expiry_date'] < $this->utc_time) {
                $this->session->set_flashdata('failure', lang('PROF_SEARCH_PRIORITY_RESUME_NOT_SUBSCRIBED'));
                redirect(PROFESSIONAL_PATH . '/priority_resume');
            }

            $default_data = $this->Common_model->get_default_details();

            $application_data = array();

            if ($this->input->post() !== NULL && $this->input->post('search_priority_resume') == 'true') {
                $application_data = $this->Professional_model->manage_resume_search_priority();
                if ($application_data) {
                    $this->session->set_flashdata('success', lang('PROF_SEARCH_PRIORITY_SUCCESS'));
                    redirect(PROFESSIONAL_PATH . '/search_priority_resume');
                } else {
                    $this->session->set_flashdata('failure', lang('PROF_SEARCH_PRIORITY_FAILED'));
                    redirect(PROFESSIONAL_PATH . '/search_priority_resume');
                }
            } else if ($this->input->post() !== NULL && $this->input->post('show_resume_to_candidate_form') == 'true') {
                $application_data = $this->Professional_model->update_profesional_settings();
                if ($application_data) {
                    $this->session->set_flashdata("success", lang('SHOW_PROFILE_PAGE_BIO_UPDATE_SUCCESS'));
                    redirect(PROFESSIONAL_PATH . '/search_priority_resume');
                } else {
                    $this->session->set_flashdata("failure", lang('SHOW_PROFILE_PAGE_BIO_UPDATE_FAILED'));
                    redirect(PROFESSIONAL_PATH . '/search_priority_resume');
                }
            }

            $data['user_data'] = $user_data;
            $application_data = array_merge($data, $default_data);
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(PROFESSIONAL_PATH);
        }

        $this->load->view('common/professional_header');
        $this->load->view('professional/search_priority_resume', $application_data);
        $this->load->view('common/professional_footer');
    }

    /**
     * Description : Use this function to show favourite resume
     * 
     * @author Nitinkumar Vaghani
     * 
     * Created date : 2017-04-18
     */
    public function favourite_resume() {
        try {
            $default_data = $this->Common_model->get_default_details();

            $default_data['search_by_job_type'] = '';
            $default_data['search_by_activity_area'] = '';
            $default_data['search_by_work_place'] = '';
            $default_data['request_page'] = 0;

            if ($this->input->post() !== NULL && $this->input->post('search_cv') == 'true') {
                $activity_area = $this->input->post('activity_area[]');
                $work_place = $this->input->post('work_place[]');

                $default_data['search_by_job_type'] = trim($this->input->post('job_type'));
                $default_data['search_by_activity_area'] = !empty($activity_area) && count($activity_area) > 0 ? array_filter($activity_area) : '';
                $default_data['search_by_work_place'] = !empty($work_place) && count($work_place) > 0 ? array_filter($work_place) : '';
                $default_data['request_page'] = !empty($this->input->post('request_page')) && is_numeric($this->input->post('request_page')) ? trim($this->input->post('request_page')) : 0;
            }

            if ($this->input->post() !== NULL && $this->input->post('view_more') == 'true') {
                $activity_area = $this->input->post('activity_area[]');
                $work_place = $this->input->post('work_place[]');

                $activity_area = !empty($activity_area) && count($activity_area) > 0 ? array_filter($activity_area) : '';
                $work_place = !empty($work_place) && count($work_place) > 0 ? array_filter($work_place) : '';

                if (isset($activity_area[0]) && !empty($activity_area[0])) {
                    $activity_area = explode(',', $activity_area[0]);
                }

                if (isset($work_place[0]) && !empty($work_place[0])) {
                    $work_place = explode(',', $work_place[0]);
                }

                $default_data['search_by_job_type'] = trim($this->input->post('job_type'));
                $default_data['search_by_activity_area'] = $activity_area;
                $default_data['search_by_work_place'] = $work_place;
                $default_data['request_page'] = !empty($this->input->post('request_page')) && is_numeric($this->input->post('request_page')) ? trim($this->input->post('request_page')) : 0;
            }

            $application_data = $this->Professional_model->get_favourite_user_applications($default_data);

            $application_data = array_merge($application_data, $default_data);
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(HOME_PATH);
        }
        $this->load->view('common/professional_header');
        $this->load->view('professional/favourite_resume', $application_data);
        $this->load->view('common/professional_footer');
    }

    /**
     * Description : This function is use for change professional user's password
     * 
     * @author Nitinkumar Vaghani
     * 
     * Created date: 2017-04-18
     * 
     */
    public function change_password() {
        try {
            if ($this->input->post() != NULL) {
                $this->form_validation->set_rules('old_password', lang('CHANGE_PASSWORD_CURRENT_PASSWORD'), 'required');
                $this->form_validation->set_rules('password', lang('CHANGE_PASSWORD_NEW_PASSWORD'), 'required|min_length[6]|max_length[15]');
                $this->form_validation->set_rules('cpassword', lang('CHANGE_PASSWORD_CONFIRM_PASSWORD'), 'required|matches[password]');
                if ($this->form_validation->run() !== FALSE) {
                    //get user's current password
                    $where_user = array(
                        "u_id" => $this->user_id,
                        "u_status" => 1
                    );
                    $user_data = $this->Common_model->get_single_row(TBL_USERS, "u_password", $where_user);
                    if (empty($user_data)) {
                        $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                        redirect(PROFESSIONAL_PATH);
                    }
                    $current_password = trim($this->Common_model->escape_data($this->input->get_post('old_password')));
                    if (password_verify(sha1($current_password), $user_data['u_password'])) {
                        //change new password
                        $new_password = trim($this->Common_model->escape_data($this->input->get_post('password')));
                        $udpate_array = array(
                            "u_password" => password_hash(sha1($new_password), PASSWORD_BCRYPT),
                            "u_modified_date" => $this->utc_time
                        );
                        $is_update = $this->Common_model->update(TBL_USERS, $udpate_array, $where_user);
                        if ($is_update > 0) {
                            $this->session->set_flashdata('success', lang("EDITPROFILE_PAGE_CHANGE_PASSWORD_SUCCESS"));
                        } else {
                            $this->session->set_flashdata('failure', lang("EDITPROFILE_PAGE_CHANGE_PASSWORD_FAILED"));
                        }
                    } else {
                        $this->session->set_flashdata('failure', lang("EDITPROFILE_PAGE_CHANGE_PASSWORD_INVALID"));
                    }
                    redirect(PROFESSIONAL_PATH);
                }
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(PROFESSIONAL_PATH);
        }

        $data = $this->Professional_model->get_user_data();
        $default_data = $this->Common_model->get_default_details();
        $data = array_merge($data, $default_data);
        try {
            if (empty($data)) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(HOME_PATH);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(HOME_PATH);
        }
        $this->load->view('common/professional_header');
        $this->load->view('professional/edit_profile', $data);
        $this->load->view('common/professional_footer');
    }

    /**
     * @author Nitinkumar vaghani
     * 
     * Description : use this function for add cv to favourite
     * 
     * Created date : 2017-04-24
     */
    public function add_to_favourite() {
        if (!$this->input->is_ajax_request()) {
            redirect(PROFESSIONAL_PATH);
        }
        try {
            $response = $this->Professional_model->add_to_favourite();
            if ($response) {
                $response = array(
                    "success" => true,
                    "message" => lang("PROF_RESUME_DATABASE_ADD_TO_FAVORITE_SUCCESS")
                );
            } else {
                $response = array(
                    "success" => false,
                    "message" => lang("PROF_RESUME_DATABASE_ADD_TO_FAVORITE_FAILED")
                );
            }
        } catch (Exception $e) {
            $response = array(
                "success" => false,
                "message" => lang("COMMON_PROBLEM_IN_ACTION")
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    /**
     * @author Nitinkumar vaghani
     * 
     * Description : use this function for remove cv from favourite
     * 
     * Created date : 2017-04-24
     */
    public function remove_from_favourite() {
        if (!$this->input->is_ajax_request()) {
            redirect(PROFESSIONAL_PATH);
        }
        try {
            $response = $this->Professional_model->remove_from_favourite();
            if ($response) {
                $response = array(
                    "success" => true,
                    "message" => lang("PROF_RESUME_DATABASE_REMOVE_FROM_FAVORITE_SUCCESS")
                );
            } else {
                $response = array(
                    "success" => false,
                    "message" => lang("PROF_RESUME_DATABASE_REMOVE_FROM_FAVORITE_FAILED")
                );
            }
        } catch (Exception $e) {
            $response = array(
                "success" => false,
                "message" => lang("COMMON_PROBLEM_IN_ACTION")
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    /**
     * @author Nitinkumar vaghani
     * 
     * Description : use this function for send mail to candidate by recruiter
     * 
     * Created date : 2017-04-26
     */
    public function send_mail_to_candidate() {
        if (!$this->input->is_ajax_request()) {
            redirect(PROFESSIONAL_PATH);
        }
        try {
            $candidate_id = $this->input->post('candidate_id');
            $description = $this->input->post('description');

            if (!empty($candidate_id) && is_numeric($candidate_id) && !empty($description)) {

                $user_data = $this->Professional_model->get_candidate_and_recruiter_data($candidate_id, $this->user_id);

                if (!empty($user_data)) {
                    $email_params = array(
                        'candidate_name' => ucfirst($user_data['candidate_name']),
                        'recruiter_name' => ucfirst($user_data['recruiter_name']),
                        'recruiter_email' => $user_data['recruiter_email'],
                        'recruiter_phone' => $user_data['cd_phone'],
                        'recruiter_address' => $user_data['cd_address'],
                        'recruiter_city' => $user_data['cd_city'],
                        'recruiter_country' => $user_data['cd_country'],
                        'recruiter_postal_code' => $user_data['cd_postal_code'],
                        'message' => $description,
                    );

                    $message = $this->load->view("emails/send_mail_to_candidate", $email_params, TRUE);
                    $subject = APP_NAME . " : " . lang("PROF_RESUME_DETAILS_SEND_MAIL_SUBJECT");
                    $email = array($user_data['candidate_email'] => ucfirst($user_data['candidate_name']));
                    $this->send_email($email, $subject, $message);
                    $response = array(
                        "success" => true,
                        "message" => lang("PROF_RESUME_DETAILS_SEND_MAIL_SUCCESS")
                    );
                } else {
                    $response = array(
                        "success" => false,
                        "message" => lang("COMMON_INVALID_PARAMS")
                    );
                }
            } else {
                $response = array(
                    "success" => false,
                    "message" => lang("COMMON_INVALID_PARAMS")
                );
            }
        } catch (Exception $e) {
            $response = array(
                "success" => false,
                "message" => lang("COMMON_PROBLEM_IN_ACTION")
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    /**
     * Description: Use this function for upload profile photo into directory 
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2016-12-17
     * 
     */
    public function upload_image() {

        try {
            if (isset($_FILES) && count($_FILES) > 0 && !empty($_FILES['image']['name'])) {
                if (isset($_FILES['image']['name']) && $_FILES['image']['error'] == 0) {
                    list($width, $height) = getimagesize($_FILES['image']["tmp_name"]);
                    $extention = explode(".", $_FILES['image']['name']);
                    $extention = strtolower(end($extention));
                    $validExtentions = array('png', 'jpg', 'jpeg');
                    if (!in_array($extention, $validExtentions)) {
                        $this->session->set_flashdata("failure", lang('ERROR_INVALID_EXTENSION'));
                        redirect(PROFESSIONAL_PATH);
                    } else if ($_FILES['image']['size'] > DEFAULT_IMAGE_SIZE_LIMIT) {
                        $this->session->set_flashdata("failure", lang('ERROR_INVALID_SIZE'));
                        redirect(PROFESSIONAL_PATH);
                    }

                    if ($width < 200 || $height < 200) {
                        $this->session->set_flashdata("failure", lang('ERROR_INVALID_DIMENSION'));
                        redirect(PROFESSIONAL_PATH);
                    }

                    $upload_path = UPLOAD_FILE_FOLDER . "/" . UPLOAD_USERS_FOLDER . "/" . $this->user_id;
                    $new_image = do_upload($upload_path, $_FILES, false);
                    if (!empty($new_image)) {
                        $where = array(
                            'u_id ' => $this->user_id,
                            'u_status' => 1
                        );
                        $file_name = $this->Common_model->get_single_row(TBL_USERS, "u_image", $where);
                        if (!empty($file_name)) {
                            @unlink($upload_path . '/' . $file_name['u_image']);
                            $this->Common_model->update(TBL_USERS, array("u_image" => $new_image), $where);

                            if (file_exists(UPLOAD_FILE_FOLDER . '/' . UPLOAD_USERS_FOLDER . '/' . $this->user_id . "/" . $new_image)) {
                                $photo["profile_photo"] = UPLOAD_USERS_FOLDER . '/' . $this->user_id . "/" . $new_image;
                            } else {
                                $photo["profile_photo"] = '';
                            }

                            $this->session->set_userdata($photo);
                            $this->session->set_flashdata("success", lang('PHOTO_UPLOAD_SUCCESS'));
                            redirect(PROFESSIONAL_PATH);
                        } else {
                            $this->session->set_flashdata("failure", lang('ERROR_PROBLEM_IN_UPLOAD'));
                            redirect(PROFESSIONAL_PATH);
                        }
                    } else {
                        $this->session->set_flashdata("failure", lang('ERROR_PROBLEM_IN_UPLOAD'));
                        redirect(PROFESSIONAL_PATH);
                    }
                } else {
                    $this->session->set_flashdata("failure", lang('ERROR_INVALID_FILE'));
                    redirect(PROFESSIONAL_PATH);
                }
            } else {
                $this->session->set_flashdata("failure", lang('ERROR_INVALID_FILE'));
                redirect(PROFESSIONAL_PATH);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(PROFESSIONAL_PATH);
        }
        redirect(PROFESSIONAL_PATH);
    }

    public function form_payment() {
        try {

            // get company details
            $user_data = $this->Professional_model->get_user_data();

            if (empty($user_data)) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(PROFESSIONAL_PATH);
            }

            if ($this->input->post() != NULL) {
                require ("function_recruiter.php");
                $array_lang = array('en', 'fr');

                // load settings model from admin
                $this->load->model('Setting_model');

                //get subscription price and duration
                $setting_array = $this->Setting_model->get_settings();
                $recruiter_subscription_plan_price = DEFAULT_RECRUITER_SUBSCRITION_PLAN_PRICE;
                $recruiter_subscription_plan_duration = DEFAULT_RECRUITER_SUBSCRITION_PLAN_DURATION;

                if (!empty($setting_array) && isset($setting_array['recruiter_subscription_plan_price'])) {
                    $recruiter_subscription_plan_price = $setting_array['recruiter_subscription_plan_price'];
                }
                if (!empty($setting_array) && isset($setting_array['recruiter_subscription_plan_duration'])) {
                    $recruiter_subscription_plan_duration = $setting_array['recruiter_subscription_plan_duration'];
                }

                $user_name = $this->session->userdata("user_first_name");
                $user_name = !empty($user_name) ? strlen($user_name) > 120 ? substr($user_name, 0, 120) . "..." : $user_name : '';
                $address = !empty($user_data['cd_address']) ? strlen($user_data['cd_address']) > 250 ? substr($user_data['cd_address'], 0, 250) . "..." : $user_data['cd_address'] : '';
                $city = !empty($user_data['cd_city']) ? strlen($user_data['cd_city']) > 50 ? substr($user_data['cd_city'], 0, 50) . "..." : $user_data['cd_city'] : '';
                $postal_code = !empty($user_data['cd_postal_code']) ? strlen($user_data['cd_postal_code']) > 20 ? substr($user_data['cd_postal_code'], 0, 20) . "..." : $user_data['cd_postal_code'] : '';

                $_REQUEST['lang'] = $this->current_lang;
                $_REQUEST['vads_order_id'] = str_rand_access_token(6, 'numeric');
                $_REQUEST['vads_cust_id'] = $this->user_id;
                $_REQUEST['vads_cust_name'] = $user_name;
                $_REQUEST['vads_cust_address'] = $address;
                $_REQUEST['vads_cust_zip'] = $postal_code;
                $_REQUEST['vads_cust_city'] = $city;

                if (!empty($user_data['u_email']) && filter_var($user_data['u_email'], FILTER_VALIDATE_EMAIL)) {
                    $_REQUEST['vads_cust_email'] = $user_data['u_email'];
                }

                $_REQUEST['vads_amount'] = (int) $recruiter_subscription_plan_price;

                $payment_data = array(
                    'amount' => (int) $recruiter_subscription_plan_price,
                    'tariff' => $recruiter_subscription_plan_duration
                );

                $this->session->set_userdata($payment_data);

                $lang = $this->current_lang;
                include($lang . '.php');

                $form = get_formHtml_request($_REQUEST, $lang);
                $conf_txt = parse_ini_file("conf_recruiter.txt");
                if ($conf_txt['debug'] == 0) {
                    echo $form;
                } else {
                    echo (display_form($lang, $form));
                }
            } else {
                $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
                redirect(PROFESSIONAL_PATH . '/priority_resume');
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(PROFESSIONAL_PATH . '/priority_resume');
        }
    }

    public function payment_response() {
        try {
            if ($this->input->post() != NULL) {
                $this->load->model('Payment_model');
                if ($this->input->post('vads_result') == '00') {
                    $success_response = $this->Payment_model->store_success_payment();
                    if ($success_response) {
                        $this->session->set_flashdata('success', lang('PAYMENT_SUCCESS'));
                    } else {
                        $this->session->set_flashdata('failure', lang('PAYMENT_STORE_FAILED'));
                        redirect(PROFESSIONAL_PATH . '/priority_resume');
                    }
                } else {
                    $failed_response = $this->Payment_model->store_failed_payment();
                    if ($failed_response) {
                        $this->session->set_flashdata('failure', lang('PAYMENT_FAILED'));
                    } else {
                        $this->session->set_flashdata('failure', lang('PAYMENT_STORE_FAILED_DETAILS'));
                    }
                    redirect(PROFESSIONAL_PATH . '/priority_resume');
                }
            } else {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(PROFESSIONAL_PATH . '/priority_resume');
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(PROFESSIONAL_PATH . '/priority_resume');
        }

        redirect(PROFESSIONAL_PATH . '/search_priority_resume');
    }

    /**
     * Description: Use this function for redirect user to paypal payment
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-05-19
     * 
     */
    public function form_paypal() {
        try {
            if ($this->input->post() != NULL) {

                // load settings model from admin
                $this->load->model('Setting_model');

                //get subscription price and duration
                $setting_array = $this->Setting_model->get_settings();
                $recruiter_subscription_plan_price = DEFAULT_RECRUITER_SUBSCRITION_PLAN_PRICE;
                $recruiter_subscription_plan_duration = DEFAULT_RECRUITER_SUBSCRITION_PLAN_DURATION;

                if (!empty($setting_array) && isset($setting_array['recruiter_subscription_plan_price'])) {
                    $recruiter_subscription_plan_price = $setting_array['recruiter_subscription_plan_price'];
                }

                if (!empty($setting_array) && isset($setting_array['recruiter_subscription_plan_duration'])) {
                    $recruiter_subscription_plan_duration = $setting_array['recruiter_subscription_plan_duration'];
                }

                $payment_data = array(
                    'amount' => $recruiter_subscription_plan_price,
                    'tariff' => $recruiter_subscription_plan_duration
                );

                $this->session->set_userdata($payment_data);

                // include paypal library
                require_once DOCROOT . '/third_party/paypal_sdk.php';
                $cdo = new CdoPay();
                $url = $cdo->payment_using_web($recruiter_subscription_plan_price);
                header("Location:" . $url);
            } else {
                redirect(PROFESSIONAL_PATH . '/priority_resume');
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(PROFESSIONAL_PATH . '/priority_resume');
        }
    }

    /**
     * 
     * This function is use for store success payment response
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-05-19
     * 
     * @return type
     * 
     */
    public function paypal_success() {

        try {
            if ($this->input->get() != NULL) {
                $this->load->model('Payment_model');
                $payment_id = !empty($this->input->get('paymentId')) ? $this->input->get('paymentId') : '';
                $token = !empty($this->input->get('token')) ? $this->input->get('token') : '';
                $payer_id = !empty($this->input->get('PayerID')) ? $this->input->get('PayerID') : '';
                $amount = $this->session->userdata('amount');
                if (!empty($payment_id) && !empty($token) && !empty($payer_id) && !empty($amount)) {
                    $success_response = $this->Payment_model->store_paypal_success_payment();
                    if ($success_response) {
                        $this->session->set_flashdata('success', lang('PAYMENT_SUCCESS'));
                    } else {
                        $this->session->set_flashdata('failure', lang('PAYMENT_STORE_FAILED'));
                    }
                } else {
                    $failed_response = $this->Payment_model->store_paypal_failed_payment();
                    if ($failed_response) {
                        $this->session->set_flashdata('failure', lang('PAYMENT_FAILED'));
                    } else {
                        $this->session->set_flashdata('failure', lang('PAYMENT_STORE_FAILED_DETAILS'));
                    }
                }
            } else {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(PROFESSIONAL_PATH . '/priority_resume');
        }
        $this->session->unset_userdata('amount');
        $this->session->unset_userdata('tariff');

        redirect(PROFESSIONAL_PATH . '/search_priority_resume');
    }

    /**
     * 
     * This function is use for store failed payment response
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-05-19
     * 
     * @return type
     * 
     */
    public function paypal_failure() {
        try {
            if ($this->input->get() != NULL) {
                $this->load->model('Payment_model');
                $failed_response = $this->Payment_model->store_paypal_failed_payment();
                if ($failed_response) {
                    $this->session->set_flashdata('failure', lang('PAYMENT_FAILED'));
                } else {
                    $this->session->set_flashdata('failure', lang('PAYMENT_STORE_FAILED_DETAILS'));
                }
            } else {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(PROFESSIONAL_PATH . '/priority_resume');
        }
        $this->session->unset_userdata('amount');
        $this->session->unset_userdata('tariff');

        redirect(PROFESSIONAL_PATH . '/priority_resume');
    }

    /**
     * Description: Use this function for display my orders
     * 
     * @author Nitinkumar Vaghani
     * Last modified date : 2017-05-22
     * 
     */
    public function my_orders() {
        $data = array();
        try {

            $user_data = $this->Professional_model->get_user_data();

            if (empty($user_data)) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(HOME_PATH);
            }

            $this->load->model('Professional_model');
            $data['order_data'] = $this->Professional_model->get_user_order_details($this->user_id);
            $data = array_merge($data, $user_data);
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }

        $this->load->view('common/professional_header');
        $this->load->view('professional/my_orders', $data);
        $this->load->view('common/professional_footer');
    }

    /**
     * @author Hardik Patel
     * 
     * This function is use for show faq
     * 
     * Created date : 2017-05-15
     */
    public function faq() {
        $this->load->view('common/professional_header');
        $this->load->view('faq');
        $this->load->view('common/professional_footer');
    }

    /**
     * Description: Use this function for signned out.
     * 
     * @author Nitinkumar Vaghani
     * 
     * Created date : 2017-04-18
     * 
     */
    public function signout() {

        try {
            $user_session_data = array(
                "user_id" => '',
                "user_first_name" => '',
                "user_last_name" => '',
                "user_type" => '',
                "profile_photo" => ''
            );
            $this->session->unset_userdata($user_session_data);
            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('profile_photo');
            $this->session->sess_destroy();
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(HOME_PATH);
        }
        redirect(HOME_PATH);
    }

}
