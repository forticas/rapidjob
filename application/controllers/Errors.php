<?php

/**
 * Description of Error_404
 *
 *
 * @author Nitinkumar vaghani
 * Date : 2016-09-26
 */
class Errors extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * 
     * This function is use for display custom error page
     * 
     * @author Nitinkumar
     * Modified Date :- 2016-11-30
     */
    public function index() {
        $this->load->view('errors/html/error_general');
    }

    /**
     * 
     * This function is use for display 404 page
     * 
     * @author Nitinkumar
     * Modified Date :- 2016-11-30
     * 
     */
    public function error_404() {
        $data = array();
        try {
            // get user details
            $data = $this->Common_model->get_user_all_details($this->user_id);
            if (empty($data)) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(HOME_PATH);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(USERS_PATH);
        }
        $data['page_title'] = '404 Not Found!';
        $this->load->view('errors/html/error_404', $data);
    }

}
