<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * This controller is use for manage web site home page
 * 
 * @author Dipesh Shihora
 * Modified Date :- 2016-11-17
 */
class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    /**
     * @author NitinKumar vaghani
     * 
     * This function is use for show home page
     * 
     * Created date : 2017-04-17
     */
    public function info() {
        phpinfo();
        exit;
    }

    public function index() {
        try {
            $this->load->model('Home_model');


            $default_data = $this->Common_model->get_default_details();
            $news_details = $this->Common_model->get_news_details();

            $slider_details = $this->Home_model->get_cms_content();
            $testimonials_details = $this->Home_model->get_all_testimonials();

            $default_data['banner_image'] = ASSETS_PATH . 'images/homepage_main_image.png';
            if (!empty($slider_details)) {
                $banner_image_file = UPLOAD_FILE_FOLDER . '/' . UPLOAD_CMS_FOLDER . '/' . $slider_details['cms_home_banner_image'];
                if (!empty($slider_details['cms_home_banner_image']) && file_exists($banner_image_file)) {
                    $default_data['banner_image'] = UPLOAD_ABS_PATH . UPLOAD_CMS_FOLDER . '/' . $slider_details['cms_home_banner_image'];
                }
            }

            if (!empty($news_details)) {
                foreach ($news_details as &$row_news) {
                    $row_news['news_image_name'] = UPLOAD_ABS_PATH . UPLOAD_NEWS_FOLDER . '/' . $row_news['news_id'] . "/" . $row_news['news_image_name'];
                    $default_data['news_details'][] = $row_news;
                }
            } else {
                $default_data['news_details'] = array();
            }

            $default_data['slider_details'] = $slider_details;
            $default_data['testimonials_details'] = $testimonials_details;
            if (!empty($this->user_id) && is_numeric($this->user_id)) {
                // get user details
                $where_user = array(
                    'u_id' => $this->user_id,
                    'u_status' => 1,
                );

                $join_array = array(
                    TBL_USER_DETAILS => "u_id = ud_user_id "
                );

                $column_user = "*";
                $default_data['user_data'] = $this->Common_model->get_single_row(TBL_USERS, $column_user, $where_user, $join_array, 'LEFT');
            } else {
                $default_data['user_data'] = array();
            }
            $default_data['search_query'] = $this->session->userdata('search_query');
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(HOME_PATH);
        }
        $this->load->view('common/header');
        $this->load->view('home', $default_data);
        $this->load->view('common/footer');
    }

    /**
     * @author NitinKumar vaghani
     * 
     * This function is use for show professional user registration page
     * 
     * Created date : 2017-04-17
     */
    public function registration() {
        $data = array();
        try {
            $this->load->model('Home_model');
            // get work job, activity area, work places and level of education
            $data = $this->Common_model->get_default_details();
            $data['cms_content'] = $this->Home_model->get_cms_content();

            $data['banner_image'] = ASSETS_PATH . 'images/banner_image.png';
            if (!empty($data['cms_content'])) {
                $banner_image_file = UPLOAD_FILE_FOLDER . '/' . UPLOAD_CMS_FOLDER . '/' . $data['cms_content']['cms_prof_banner_image'];
                if (!empty($data['cms_content']['cms_prof_banner_image']) && file_exists($banner_image_file)) {
                    $data['banner_image'] = UPLOAD_ABS_PATH . UPLOAD_CMS_FOLDER . '/' . $data['cms_content']['cms_prof_banner_image'];
                }
            }

            if (empty($data)) {
                $this->session->set_flashdata('failure', lang('COMMON_INVALID_PARAMS'));
                redirect(HOME_PATH);
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(HOME_PATH);
        }

        $this->load->view('common/header');
        $this->load->view('professional/register', $data);
        $this->load->view('common/footer');
    }

    /**
     * @author NitinKumar vaghani
     * 
     * This function is use for change language
     * 
     * Created date : 2017-04-17
     */
    public function change_language() {
        if (!$this->input->is_ajax_request()) {
            redirect(HOME_PATH);
        }

        $language = $this->input->post("language");
        $ajax_response = array();
        if (!empty($language)) {
            $this->session->set_userdata("language", $language);
            $ajax_response = array(
                "success" => TRUE
            );
        } else {
            $ajax_response = array(
                "success" => FALSE
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($ajax_response));
    }

    /**
     * @author NitinKumar vaghani
     * 
     * This function is use for generate csrf token
     * 
     * Created date : 2017-04-17
     */
    public function get_csrf_value() {
        if (!$this->input->is_ajax_request()) {
            redirect(HOME_PATH);
        }

        $ajax_response = array(
            "success" => TRUE,
            "value" => $this->security->get_csrf_hash()
        );

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($ajax_response));
    }

    /**
     * @author NitinKumar vaghani
     * 
     * This function is use for login recruiter / candidate
     * 
     * Created date : 2017-04-17
     */
    public function login() {

        if (!$this->input->is_ajax_request()) {
            redirect(HOME_PATH);
        }
        try {
            $email = trim($this->input->post("email"));
            $password = trim($this->input->post("password"));

            $user_type = trim($this->input->post("user_type"));
            $user_account_type = trim($this->input->post("user_account_type"));
            $social_id = trim($this->input->post("social_id"));

            $ajax_response = array();
            if (
                    (
                    $user_type == 1 && empty($user_account_type) && !is_numeric(empty($user_account_type)) &&
                    (empty($email) || empty($password))
                    ) ||
                    (
                    $user_type != 1 &&
                    (empty($user_type) || empty($social_id))
                    )
            ) {
                $ajax_response = array(
                    "success" => FALSE,
                    "message" => lang('COMMON_INVALID_PARAMS')
                );
            } else {
                $where = array(
                    'u_status !=' => 9
                );
                if ($user_type == 1 && !empty($user_account_type)) {
                    $where['u_email'] = $email;
                    $where['u_type'] = $user_account_type;
                } else {
                    $where['u_user_type'] = $user_type;
                    $where['u_social_id'] = $social_id;
                }

                $check_login = $this->Common_model->get_single_row(TBL_USERS, "*", $where);

                if (!empty($check_login)) {
                    if ($check_login['u_status'] == 1) {
                        if (!empty($check_login['u_image'])) {
                            if (file_exists(UPLOAD_FILE_FOLDER . '/' . UPLOAD_USERS_FOLDER . '/' . $check_login['u_id'] . "/" . $check_login['u_image'])) {
                                $photo = UPLOAD_USERS_FOLDER . '/' . $check_login['u_id'] . "/" . $check_login['u_image'];
                            } else {
                                $photo = '';
                            }
                        } else {
                            $photo = '';
                        }

                        if ($user_type == 1 && password_verify($password, $check_login['u_password'])) {
                            $user_session_data = array(
                                "user_id" => $check_login['u_id'],
                                "user_first_name" => $check_login['u_first_name'],
                                "user_last_name" => $check_login['u_last_name'],
                                "user_type" => $check_login['u_type'],
                                "profile_photo" => $photo
                            );
                            $this->session->set_userdata($user_session_data);
                            if ($check_login['u_type'] == 1 && $user_account_type == 1) {
                                $url = USERS_PATH;
                            } else {
                                $url = PROFESSIONAL_PATH;
                            }

                            $ajax_response = array(
                                "success" => TRUE,
                                "url" => $url
                            );
                        } elseif ($user_type != 1) {
                            $user_session_data = array(
                                "user_id" => $check_login['u_id'],
                                "user_first_name" => $check_login['u_first_name'],
                                "user_last_name" => $check_login['u_last_name'],
                                "user_type" => $check_login['u_type'],
                                "profile_photo" => $photo
                            );

                            $this->session->set_userdata($user_session_data);

                            if ($check_login['u_type'] == 1 && $user_account_type == 1) {
                                $url = USERS_PATH;
                            } else {
                                $url = PROFESSIONAL_PATH;
                            }

                            $ajax_response = array(
                                "success" => TRUE,
                                "url" => $url
                            );
                        } else {
                            $ajax_response = array(
                                "success" => FALSE,
                                "message" => lang('LOGIN_INVALID_EMAIL_OR_PASSWORD'),
                                "no_user" => FALSE,
                            );
                        }
                    } else {
                        $ajax_response = array(
                            "success" => FALSE,
                            "message" => lang('LOGIN_ACCOUNT_BLOCKED'),
                            "no_user" => FALSE,
                        );
                    }
                } else {
                    $ajax_response = array(
                        "success" => FALSE,
                        "message" => lang('LOGIN_INVALID_EMAIL_OR_PASSWORD'),
                        "no_user" => TRUE,
                    );
                }
            }
        } catch (Exception $e) {
            $ajax_response = array(
                "success" => FALSE,
                "message" => lang('COMMON_PROBLEM_IN_ACTION'),
            );
        }

        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($ajax_response));
    }

    /**
     * @author NitinKumar vaghani
     * 
     * This function is use for register candidate only
     * 
     * Created date : 2017-04-17
     */
    public function register() {
        if (!$this->input->is_ajax_request()) {
            redirect(HOME_PATH);
        }
        try {

            $first_name = trim($this->input->post("first_name"));
            $last_name = trim($this->input->post("last_name"));
            $email = trim($this->input->post("email"));
            $password = trim($this->input->post("password"));

            $user_type = trim($this->input->post("user_type"));
            $social_id = trim($this->input->post("social_id"));

            $ajax_response = array();

            if (
                    empty($first_name) ||
                    empty($last_name) ||
                    empty($email) ||
                    empty($password)
            ) {
                $ajax_response = array(
                    "success" => FALSE,
                    "message" => lang('COMMON_INVALID_PARAMS')
                );
            } else {
                $where = array(
                    "LOWER(u_email)" => strtolower($email),
                    "u_status !=" => 9
                );

                $check_user = $this->Common_model->get_single_row(TBL_USERS, "u_id", $where);
                if (!empty($check_user)) {
                    $ajax_response = array(
                        "success" => FALSE,
                        "message" => lang('COMMON_EMAIL_ALREADY_REGISTERED')
                    );
                } else {
                    $invitation_code = str_rand_access_token(8);
                    $register_user = array(
                        "u_first_name" => $first_name,
                        "u_last_name" => $last_name,
                        "u_email" => $email,
                        "u_password" => password_hash($password, PASSWORD_BCRYPT),
                        "u_created_date" => $this->utc_time,
                        "u_user_type" => $user_type,
                        "u_social_id" => $social_id,
                        "u_invitation_code" => $invitation_code
                    );

                    $invited_code = trim($this->input->post("invitation_code"));
                    // if invitation code is not empty then verify code and add points into invited user's account
                    if (!empty($invited_code)) {
                        $this->session->unset_userdata('invitation_code');
                        //check invitation_code is valid or not
                        $where_user = array(
                            "u_invitation_code" => $invited_code,
                            "u_status !=" => 9
                        );

                        // get user details  
                        $join_array = array(
                            TBL_USER_DETAILS => "u_id = ud_user_id "
                        );

                        $column_user = array("u_id", "ud_points");
                        $invited_user = $this->Common_model->get_single_row(TBL_USERS, $column_user, $where_user, $join_array, 'LEFT');

                        if (!empty($invited_user) && !empty($invited_user['u_id'])) {
                            $where_update = array(
                                'ud_user_id' => $invited_user['u_id'],
                                'ud_status !=' => 9
                            );
                            $request_data = array(
                                'ud_points' => intval($invited_user['ud_points'] + DEFAULT_INVITATION_POINTS)
                            );
                            $register_user['u_invited_code'] = $invited_code;
                            $this->Common_model->update(TBL_USER_DETAILS, $request_data, $where_update);
                        }
                    }

                    $user_id = $this->Common_model->insert(TBL_USERS, $register_user);
                    if ($user_id > 0) {
                        $request_data = array(
                            "ud_user_id" => $user_id,
                            "ud_points" => 100,
                            "ud_created_date" => $this->utc_time,
                            "ud_status" => 1
                        );
                        $this->Common_model->insert(TBL_USER_DETAILS, $request_data);

                        $user_session_data = array(
                            "user_id" => $user_id,
                            "user_first_name" => $first_name,
                            "user_last_name" => $last_name,
                            "user_type" => 1,
                            "profile_photo" => DEFAULT_IMAGE_NAME
                        );

                        $this->session->set_userdata($user_session_data);

                        $ajax_response = array(
                            "success" => TRUE,
                            "message" => lang('REGISTER_SUCCESS'),
                            "url" => HOME_PATH
                        );
                    } else {
                        $ajax_response = array(
                            "success" => FALSE,
                            "message" => lang('REGISTER_FAILURE')
                        );
                    }
                }
            }
        } catch (Exception $e) {
            $ajax_response = array(
                "success" => false,
                "message" => lang("COMMON_PROBLEM_IN_ACTION")
            );
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($ajax_response));
    }

    /**
     * @author NitinKumar vaghani
     * 
     * This function is use for register user news latter
     * 
     * Created date : 2017-04-17
     */
    public function register_newslatters() {
        if (!$this->input->is_ajax_request()) {
            redirect(HOME_PATH);
        }
        $email = trim($this->input->post("email"));

        $ajax_response = array();
        try {
            if (empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $ajax_response = array(
                    "success" => FALSE,
                    "message" => lang('COMMON_INVALID_PARAMS')
                );
            } else {
                $where = array(
                    "LOWER(nl_email)" => strtolower($email)
                );

                $check_already_register = $this->Common_model->get_single_row(TBL_NEWSLETTERS, "nl_id", $where);
                if (empty($check_already_register)) {
                    $add_news_latter = array(
                        "nl_email" => $email,
                        "nl_created_date" => $this->utc_time
                    );

                    $insert_id = $this->Common_model->insert(TBL_NEWSLETTERS, $add_news_latter);
                    if ($insert_id > 0) {
                        $subject = APP_NAME . " : " . lang('NEWSLATTER_SUBJECT');
                        $username = strstr($email, '@', true);
                        $message = $this->load->view("emails/newslatter", array('current_lang' => $this->current_lang, 'name' => $username), TRUE);
                        $this->send_email($email, $subject, $message);
                        $ajax_response = array(
                            "success" => TRUE,
                            "message" => lang('NEWSLATTER_SUCCESS')
                        );
                    }
                } else {
                    $ajax_response = array(
                        "success" => false,
                        "message" => lang('NEWSLATTER_ALREADY_REGISTERED')
                    );
                }
            }
        } catch (Exception $e) {
            $ajax_response = array(
                "success" => false,
                "message" => lang("COMMON_PROBLEM_IN_ACTION")
            );
            $this->output->set_content_type('application/json');
            $this->output->set_output(json_encode($ajax_response));
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($ajax_response));
    }

    /**
     * @author NitinKumar vaghani
     * 
     * This function is use for company search
     * 
     * Created date : 2017-04-17
     */
    public function search() {
        try {

            $this->load->model('Home_model');

            $slider_details = $this->Home_model->get_cms_content();

            $search_keyword = trim($this->input->post("q"));
            $request_page = $this->input->post('request_page');

            $activity_area = $this->input->post('activity_area[]');
            $activity_area = !empty($activity_area) && count($activity_area) > 0 ? array_filter($activity_area) : '';

            if (empty($search_keyword)) {
                redirect(HOME_PATH);
            }

            $default_data = $this->Common_model->get_default_details();

            $default_data['search_by_activity_area'] = $activity_area;
            $default_data['request_page'] = 0;

            if ($this->input->post() !== NULL && $this->input->post('search_company') == 'true') {
                $activity_area = $this->input->post('activity_area[]');
                $default_data['search_by_activity_area'] = !empty($activity_area) && count($activity_area) > 0 ? array_filter($activity_area) : '';
                $default_data['request_page'] = !empty($this->input->post('request_page')) && is_numeric($this->input->post('request_page')) ? trim($this->input->post('request_page')) : 0;
            }

            if ($this->input->post() !== NULL && $this->input->post('view_more') == 'true') {
                $activity_area = $this->input->post('activity_area[]');
                $activity_area = !empty($activity_area) && count($activity_area) > 0 ? array_filter($activity_area) : '';
                if (isset($activity_area[0]) && !empty($activity_area[0])) {
                    $activity_area = explode(',', $activity_area[0]);
                }
                $default_data['search_by_activity_area'] = $activity_area;
                $default_data['request_page'] = !empty($this->input->post('request_page')) && is_numeric($this->input->post('request_page')) ? trim($this->input->post('request_page')) : 0;
            }

            $this->session->set_userdata(array('search_query' => $search_keyword));

            $this->load->model('Home_model');
            $default_data["query"] = $search_keyword;
            $application_data = $this->Home_model->get_company_list($default_data);

            $application_data = array_merge($application_data, $default_data);
            $application_data['slider_details'] = $slider_details;
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang("COMMON_PROBLEM_IN_ACTION"));
            redirect(HOME_PATH);
        }

        $this->load->view("common/header");
        $this->load->view("search_result", $application_data);
        $this->load->view("common/footer");
    }

    /**
     * 
     * This function use for verify invitation code
     * 
     * @author Nitinkumar vaghani
     * 
     * Modified Date :- 2016-12-28
     * 
     */
    public function verify_invitation_code() {
        $this->session->unset_userdata('invitation_code');
        $invitation_code = $_GET['invitation_code'];
        $data = array();
        try {
            if (empty($invitation_code)) {
                $this->session->set_flashdata("failure", lang('COMMON_INVALID_PARAMS'));
                redirect(HOME_PATH);
            }

            //check invitation_code is valid or not
            $where = array(
                "u_invitation_code" => $invitation_code,
                "u_status !=" => 9
            );
            $is_exists = $this->Common_model->get_single_row(TBL_USERS, "u_id", $where);
            if (empty($is_exists)) {
                $this->session->set_flashdata("failure", lang('REGISTER_EXPIRE_CODE'));
                redirect(HOME_PATH);
            }
            $data = array('invitation_code' => $invitation_code);
            $this->session->set_userdata($data);
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang('COMMON_PROBLEM_IN_ACTION'));
            redirect(HOME_PATH);
        }
        redirect(HOME_PATH);
    }

    /**
     * @author NitinKumar vaghani
     * 
     * Description : use this function for register professional users only
     * 
     * Created date : 2017-04-17
     */
    public function professional_register() {
        if (!$this->input->is_ajax_request()) {
            redirect(HOME_PATH);
        }
        try {
            $this->load->model("Home_model");
            $response = $this->Home_model->professional_register();
        } catch (Exception $e) {
            $response = array(
                "success" => false,
                "message" => lang("COMMON_PROBLEM_IN_ACTION")
            );
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    /**
     * 
     * This function is use for forgot candidate / recruiter password
     * 
     * @author Nitinkumar
     * 
     * Created date :- 2017-05-01
     */
    public function forgot_password() {

        if (!$this->input->is_ajax_request()) {
            redirect(HOME_PATH);
        }
        try {
            //check email is registred or not?
            $email = $this->input->post("email");
            if (empty($email) || !filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $response = array(
                    "success" => false,
                    "message" => lang("COMMON_INVALID_EMAIL")
                );
            } else {
                $where = array(
                    "u_email" => $email,
                    "u_status" => 1
                );
                $user_data = $this->Common_model->get_single_row(TBL_USERS, "*", $where);

                if (count($user_data) <= 0) {
                    $response = array(
                        "success" => false,
                        "message" => lang("FORGOT_PASSWORD_EMAIL_NOT_REGISTERED")
                    );
                } else {
                    //generate random security code for password reset
                    $reset_token = str_rand_access_token(20);

                    //update token into database
                    $this->Common_model->update(TBL_USERS, array('u_forgot_token' => $reset_token), $where);

                    $reset_password_url = HOME_PATH . "/verify_token/" . $reset_token;

                    $view_data = array();
                    $view_data['name'] = $user_data['u_first_name'] . " " . $user_data['u_last_name'];
                    $view_data['reset_password_url'] = $reset_password_url;

                    $message = $this->load->view("emails/reset_password", $view_data, true);
                    $subject = APP_NAME . " - " . lang("FORGOT_PASSWORD_SUBJECT");

                    //send password reset link
                    $this->send_email(array($email => $view_data['name']), $subject, $message);
                    $response = array(
                        "success" => TRUE,
                        "message" => lang("FORGOT_PASSWORD_EMAIL_SUCCESS")
                    );
                }
            }
        } catch (Exception $e) {
            $response = array(
                "success" => false,
                "message" => lang("COMMON_PROBLEM_IN_ACTION")
            );
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($response));
    }

    /**
     * 
     * This function is use for check forgot password token and update new password
     * 
     * @author Nitinkumar
     * 
     * Created date :- 2017-05-01
     */
    public function verify_token($token = NULL) {
        if (!isset($token) || $token == NULL) {
            $this->session->set_flashdata('failure', lang("COMMON_INVALID_PARAMS"));
            redirect(HOME_PATH);
        }

        try {

            $where_token = array(
                'u_forgot_token' => $token,
                'u_status' => 1,
            );


            //check token is valid or not
            $valid_token = $this->Common_model->get_single_row(TBL_USERS, "u_id", $where_token);
            if (count($valid_token) <= 0) {
                $this->session->set_flashdata('failure', lang("FORGOT_PASSWORD_TOKEN_EXPIRED"));
                redirect(HOME_PATH);
            }

            $data['token'] = $token;
            $id = $valid_token['u_id'];
            $this->form_validation->set_rules('password', lang('CHANGE_PASSWORD_NEW_PASSWORD'), 'required');
            $this->form_validation->set_rules('cpassword', lang('CHANGE_PASSWORD_CONFIRM_PASSWORD'), 'required|matches[password]');


            if ($this->form_validation->run() === False) {
                $this->load->view('common/header');
                $this->load->view("common/reset_password", $data);
                $this->load->view('common/footer');
            } else {
                if ($this->input->post() != NULL) {
                    $new_password = trim($this->input->post('password'));
                    $udpate_array = array(
                        "u_password" => password_hash(sha1($new_password), PASSWORD_BCRYPT),
                        "u_modified_date" => $this->utc_time,
                    );

                    $where_user = array(
                        "u_id" => $id,
                        'u_status' => 1
                    );

                    $is_update = $this->Common_model->update(TBL_USERS, $udpate_array, $where_user);
                    if ($is_update > 0) {
                        //destroy old token
                        $this->Common_model->update(TBL_USERS, array('u_forgot_token' => NULL), $where_user);
                        $this->session->set_flashdata('success', lang("VERIFY_PASSWORD_CHANGE_SUCCESS"));
                    } else {
                        $this->session->set_flashdata('failure', lang("VERIFY_PASSWORD_CHANGE_FAILED"));
                    }
                    redirect(HOME_PATH);
                }
            }
        } catch (Exception $e) {
            $this->session->set_flashdata("failure", lang('COMMON_PROBLEM_IN_ACTION'));
            redirect(HOME_PATH);
        }
    }

    /**
     * @author Hardik Patel
     * 
     * This function is use for get news details by id
     * 
     * Created date : 2017-04-17
     */
    public function get_news_details() {
        if (!$this->input->is_ajax_request()) {
            redirect(HOME_PATH);
        }
        try {

            $news_id = trim($this->input->post("news_id"));
            $ajax_response = array();

            $column_news = array(
                'news_id',
                'news_name_' . $this->current_lang . ' as news_name',
                'news_description_' . $this->current_lang . ' as news_description'
            );

            $where_news = array('news_status' => 1, 'news_id' => $news_id);

            $response_news = $this->Common_model->get_single_row(TBL_NEWS, $column_news, $where_news);
            if (!empty($response_news)) {

                $ajax_response['success'] = true;
                $ajax_response['title'] = get_slash_formatted_text($response_news['news_name']);
                $ajax_response['description'] = get_slash_formatted_text($response_news['news_description']);
            } else {

                $ajax_response = array(
                    "success" => false,
                    "message" => lang("COMMON_PROBLEM_IN_ACTION")
                );
            }
        } catch (Exception $e) {
            $ajax_response = array(
                "success" => false,
                "message" => lang("COMMON_PROBLEM_IN_ACTION")
            );
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($ajax_response));
    }

    /**
     * @author Hardik Patel
     * 
     * This function is use for show faq
     * 
     * Created date : 2017-05-26
     */
    public function faq() {

        $column_faq = array(
            'cms_faq_id',
            'cms_faq_question_' . $this->current_lang . ' as faq_question',
            'cms_faq_desc_' . $this->current_lang . ' as faq_description'
        );

        $where_faq = array('cms_faq_status' => 1);
        $order_by = array('cms_faq_id' => 'DESC');

        $response_faq = $this->Common_model->get_all_rows(TBL_FAQ, $column_faq, $where_faq, array(), $order_by);
        $faq = array('faq_data' => $response_faq);
        $this->load->view('common/header');
        $this->load->view('faq', $faq);
        $this->load->view('common/footer');
    }

    /**
     * 
     * This function is use for unsubscribe from newsletter
     * 
     * @author Hardik Patel
     * Modified Date :- 2017-05-15
     * @param type $token
     */
    public function unsubscribe($token) {

        if (!isset($token) || $token == NULL) {
            $this->session->set_flashdata('failure', 'Invalid Token');
            redirect(DOMAIN_URL);
        }

        $nl_id = $token - (1998 * 19);

        //check token is valid or not
        $valid_token = $this->Common_model->get_single_row(TBL_NEWSLETTERS, "nl_id,nl_status", array("nl_id" => $nl_id, 'nl_status !=' => 9));
        if (count($valid_token) <= 0) {
            $this->session->set_flashdata('failure', 'Invalid Token');
            redirect(DOMAIN_URL);
        } else if (!empty($valid_token['nl_status']) && $valid_token['nl_status'] == 2) {
            $this->session->set_flashdata('failure', 'This link has been expire.');
            redirect(DOMAIN_URL);
        }

        $update_array = array('nl_status' => 2, 'nl_modified_date' => time());
        $where_array = array('nl_id' => $nl_id);
        $valid_token = $this->Common_model->update(TBL_NEWSLETTERS, $update_array, $where_array);

        if (count($valid_token) <= 0) {
            $this->session->set_flashdata('failure', 'Problem while unsubscribe your email, try again...!');
        } else {
            $this->session->set_flashdata('success', 'Your ' . APP_NAME . ' newsletter service has been unsubscribed successfully.');
        }
        redirect(DOMAIN_URL);
    }

}
