<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$send_by_email_points = !empty($user_data['ud_points']) && $user_data['ud_points'] ? (int) $user_data['ud_points'] : 0;
$send_by_post_points = !empty($user_data['ud_send_by_post_points']) && $user_data['ud_send_by_post_points'] ? (int) $user_data['ud_send_by_post_points'] : 0;
?>

<link rel="stylesheet" href="<?= ASSETS_PATH ?>css/bootstrap-slider.min.css">
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/vendor/bootstrap-slider.js"></script>

<div class="build-cv top-section free-item-bx">
    <div class="container ">
        <div class="title-box">
            <label>P</label>
            <h3 class="green-light-font">
                <?= lang("FREE_ITEM_TITLE"); ?><br><span class="red-font"><?= lang("COMMON_FREES"); ?></span>
            </h3>
            <p class="green-font">
                <?php
                if (!empty($company_data)):
                    echo count($company_data) . " " . lang("FREE_ITEM_MATCH_CRITERIA");
                else:
                    echo "0 " . lang("FREE_ITEM_MATCH_CRITERIA");
                endif;
                ?>
            </p>
        </div>

        <!-- ACTIVITY AREA -->
        <div class="clearfix"></div>
        <div id="activity_area-section" class="container-fluid">
            <div class="row">
                <?php
                echo form_open(USERS_PATH . '/add_user_activity_area', array("id" => "user_activity_form"));
                ?>
                <input type="hidden" name="ud_id" value="<?= $user_data['ud_id'] ?>">
                <input type="hidden" name="redirect_url" value="<?= USERS_PATH ?>/invite_friends">
                <div class="col-lg-5 col-md-8 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="profile-infor-ttl"><?= lang('HOME_COVERPAGE_SEARCH_BUTTON') ?></label>
                        <input name="q" type="text" id="autocomplete" placeholder="<?= lang('HOME_COVERPAGE_SEARCH_PLACEHOLDER') ?>" class="form-control" value="<?= $search_query; ?>">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <label class="profile-infor-ttl">
                        <?= lang('SHOW_PROFILE_MY_ACTIVITY_AREA') ?>
                    </label>
                    <div class="select-bx">
                        <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                        <select data-actions-box="true" name="activity_area[]" id="activity_area" class="selectpicker required " multiple="multiple" multiple <?php if (empty($activity_area)) { ?> disabled="disabled" title="<?= lang('BUILDCV_PAGE_NO_ACTIVITY_AREA_SET'); ?>"  <?php } else { ?> data-selected-text-format="count > 1" data-live-search="true" multiple title="<?= lang('BUILDCV_PAGE_CHOOSE_ACTIVITY_AREA'); ?>" <?php } ?> >
                            <?php
                            if (!empty($activity_area)) :
                                foreach ($activity_area as $value) :
                                    if (in_array($value['wa_id'], explode(',', $user_data['ud_activity_area']))) :
                                        ?>
                                        <option selected="selected" value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                    <?php else: ?>
                                        <option value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                    <?php
                                    endif;
                                endforeach;
                                ?>
                                <option value="other"><?= lang('COMMON_OTHER') ?></option>
                            <?php endif; ?>
                        </select>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 other_activity_title">
                        <div class="form-group">
                            <input type="text" id="activity_area_title" name="activity_area_title" class="form-control" data-rule-minlength="2" data-rule-maxlength="85" placeholder="<?= lang('COMMON_ADD_OTHER_ACTIVITY_AREA') ?>">
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
                    <div class="text-edit-btn-box">
                        <div class="edit-btn">
                            <button type="submit" class="round-btn popup-blue btn btn-block btn-social btn-profile submit_bio_btn" value="play">
                                <span class="popup-dark-blue">
                                    <img src="<?= ASSETS_PATH ?>images/register.png" alt="" class="img-responsive">
                                </span>
                                <?= lang('SHOW_PROFILE_PAGE_ADD_FORMATION_ADD_BTN') ?>
                            </button>
                        </div>
                    </div>
                </div>
                <?php
                echo form_close();
                ?>
            </div>
        </div>
        <div class="clearfix"></div>
        <!-- END OF ACTIVITY AREA -->

        <div class="item-details">

            <!-- INVITATION FORM -->
            <?php echo form_open_multipart(USERS_PATH . '/invite_friends', array("id" => "invite_friend_form")); ?>
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                    <p class="free_shipment"><?= vsprintf(lang("FREE_ITEM_ADD_POINTS_NEW"), DEFAULT_INVITATION_POINTS); ?></p>
                    <div class="fb-img-bx">
                        <a onclick="postToFeed()" href="javascript:void(0);">
                            <img src="<?= ASSETS_PATH ?>images/facebook_<?= $this->current_lang; ?>.png" alt="" class="img-responsive">
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="select-label"><?= lang("FREE_ITEM_SEND_TO_FRIENDS"); ?></label>
                        <input type="text" name="invitation_mail_1" placeholder="<?= lang("FREE_ITEM_EMAIL_PLACEHOLDER"); ?> 1" id="invitation_mail_1" class="form-control required" data-rule-maxlength="255">
                        <?php if (!empty(form_error('invitation_mail_1'))) : ?><label for="invitation_mail_1" class="text-danger"><?= form_error('invitation_mail_1'); ?></label> <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <input type="text" name="invitation_mail_2" placeholder="<?= lang("FREE_ITEM_EMAIL_PLACEHOLDER"); ?> 2" id="invitation_mail_2" class="form-control" data-rule-maxlength="255">
                        <?php if (!empty(form_error('invitation_mail_2'))) : ?><label for="invitation_mail_2" class="text-danger"><?= form_error('invitation_mail_2'); ?></label> <?php endif; ?>
                    </div>
                    <div class="form-group">
                        <input type="text" name="invitation_mail_3" placeholder="<?= lang("FREE_ITEM_EMAIL_PLACEHOLDER"); ?> 3" id="invitation_mail_3" class="form-control" data-rule-maxlength="255">
                        <?php if (!empty(form_error('invitation_mail_3'))) : ?><label for="invitation_mail_3" class="text-danger"><?= form_error('invitation_mail_3'); ?></label> <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="invite_friend_form item-btn-bx">
                <div class="btn-bx">
                    <button type="submit" class="round-btn blue-bg btn btn-block btn-social btn-profile">
                        <span class="dark-blue-bg">
                            <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/selector.png">
                        </span>
                        <?= lang("FREE_ITEM_TO_SELECT"); ?>
                    </button>
                </div>
            </div>
            <?php echo form_close(); ?>
            <div class="clearfix"></div>
            <!-- END OF INVITATION FORM -->

            <!-- SEND APPLICATION BY EMAIL -->
            <div class="range-slider">
                <p class="title-p"><?= lang("FREE_ITEM_CHOOSE_FORMULA"); ?></p>
                <h2><b><?= lang("FREE_ITEM_INVOICE_PER_EMAIL"); ?></b>
                    <span class="teriff_limit_text hide">
                        <?php
                        if (!empty($send_by_email_points)):
                            echo vsprintf(lang("FREE_ITEM_SEND_UPTO"), $send_by_email_points);
                        else:
                            echo vsprintf(lang("FREE_ITEM_SHARE_POST"), DEFAULT_INVITATION_POINTS);
                        endif;
                        ?>
                    </span>
                </h2>
                <div class="slider-bx">
                    <div class="set_account_type_div set_plan_radio_btn">
                        <div class="btn-group btn-group-vertical" data-toggle="buttons">
                            <label class="btn active set_account_type_label">
                                <input type="radio" name='user_send_mail_type' value="1" checked="checked">
                                <i class="fa fa-circle-o fa-2x"></i><i class="fa fa-dot-circle-o fa-2x"></i>
                            </label>
                        </div>
                    </div>
                    <div class="set_ex_slider">
                        <h4 class="modify_teriff_text"><?= lang("FREE_ITEM_MODIFY_TERIFF"); ?></h4>
                        <p class="modify_teriff_desc"><?= lang("FREE_ITEM_RATE_MESSAGE"); ?></p>
                        <input id="ex1" data-slider-id='ex1Slider' type="range" data-slider-min="<?= $send_by_email_points ?>" data-slider-max="<?= (int) count($company_data) ?>" data-slider-step="1" data-slider-value="<?= $send_by_email_points ?>" data-popup-enabled="true"/>
                    </div>
                </div>
                <div class="main-value-bx">
                    <label class="title-label"><?= lang("FREE_ITEM_NUMBER_OF_POINTS"); ?></label>
                    <div class="value-bx email_value">
                        <?= (int) count($company_data) ?>
                    </div>
                    <div class="amount-bx"><?= lang("COMMON_RATE"); ?> : <span class="update_price_email"><?= $payment_in_cent_per_email; ?></span> &#8364;</div>
                </div>
            </div>
            <div class="clearfix"></div>
            <!-- END OF SEND APPLICATION BY EMAIL -->

            <!-- SEND APPLICATION BY POST -->
            <div class="range-slider">
                <h2><b><?= lang("FREE_ITEM_INVOICE_PER_POST_OFFICE"); ?></b>
                    <span class="teriff_limit_text hide">
                        <?php
                        if (!empty($send_by_post_points)):
                            echo vsprintf(lang("FREE_ITEM_SEND_POST_UPTO"), $send_by_post_points);
                        else:
                            echo lang("FREE_ITEM_TARIF_LIMIT_POST_OVER");
                        endif;
                        ?>
                    </span>
                </h2>
                <div class="slider-bx">
                    <div class="set_account_type_div set_plan_radio_btn">
                        <div class="btn-group btn-group-vertical" data-toggle="buttons">
                            <label class="btn active set_account_type_label">
                                <input type="radio" name='user_send_mail_type' value="2">
                                <i class="fa fa-circle-o fa-2x"></i><i class="fa fa-dot-circle-o fa-2x"></i>
                            </label>
                        </div>
                    </div>
                    <div class="set_ex_slider">
                        <h4 class="modify_teriff_text"><?= lang("FREE_ITEM_MODIFY_TERIFF"); ?></h4>
                        <p class="modify_teriff_desc"><?= lang("FREE_ITEM_RATE_MESSAGE"); ?></p>
                        <input id="ex2" data-slider-id='ex2Slider' type="range" data-slider-min="<?= $send_by_post_points ?>" data-slider-max="<?= (int) count($company_data) ?>" data-slider-step="1" data-slider-value="<?= $send_by_post_points ?>"/>
                    </div>
                </div>
                <div class="main-value-bx">
                    <label class="title-label"><?= lang("FREE_ITEM_NUMBER_OF_POINTS"); ?></label>
                    <div class="value-bx post_value">
                        <?= (int) count($company_data) ?>
                    </div>
                    <div class="amount-bx"><?= lang("COMMON_RATE"); ?> : <span class="update_price_post"><?= $payment_in_cent_per_email; ?></span> &#8364;</div>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="payment_tariff_error_message"></div>
            <div class="clearfix"></div>

            <!-- END OF SEND APPLICATION BY POST -->

            <!-- END OF PAYMENT OPTIONS -->
            <div class="company_error_message"></div>
            <?php if (!empty($company_data)) : ?>
                <h4><?= lang("FREE_ITEM_SEND_YOUR_CV"); ?></h4>
                <p><?= lang("FREE_ITEM_SEND_YOUR_CV_TO_COMPANY"); ?></p>
                <div class="clearfix"></div>
                <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 default_company_list manage_all_checkbox_div">
                    <div class="item-btn-bx manage_all_checkbox">
                        <div class="btn-bx">
                            <div class="select-bx">
                                <div class="check-box control-group">
                                    <div>
                                        <input type="checkbox" name="select_all_companies" class="checkbox-custom select_company" id="select_all_companies">
                                        <label class="checkbox-custom-label select_all_companies" for="select_all_companies"><?= lang('COMMON_SELECT_ALL'); ?></label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <?php echo form_open_multipart(USERS_PATH . '/send_application_to_all_company', array("id" => "send_application"));
                ?>
                <input type="hidden" name="send_application" value="true">
                <input type="hidden" name="application_send_by" class="application_send_by" value="0">
                <input type="hidden" name="send_application_limit" class="payment_teriff" value="">
                <div class="select-company">
                    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12 col-lg-offset-2 col-md-offset-2 default_company_list">
                        <ul class="company-name">
                            <?php
                            $set_default = 1;
                            foreach ($company_data as $key => $value):
                                $user_image = UPLOAD_FILE_FOLDER . '/' . UPLOAD_USERS_FOLDER . '/' . $value['u_id'] . '/' . $value['u_image'];
                                if (!empty($value['u_image']) && file_exists($user_image)):
                                    $image = BASE_URL . UPLOAD_FILE_FOLDER . '/' . UPLOAD_USERS_FOLDER . '/' . $value['u_id'] . '/' . $value['u_image'];
                                else:
                                    $image = BASE_URL . UPLOAD_FILE_FOLDER . '/' . DEFAULT_COMPANY_IMAGE_NAME;
                                endif;
                                ?>
                                <li>
                                    <div class="company-img">
                                        <img class="img-responsive company_images" alt="" src="<?= $image; ?>">
                                    </div>
                                    <div class="select-bx">
                                        <p>
                                            <?php
                                            $title = $value['company_name'];
                                            if (!empty($value['cd_social_reason'])):
                                                $title.= " - " . $value['cd_social_reason'];
                                            endif;
                                            echo!empty($title) ? strlen($title) > DEFAULT_SOCIAL_REASON_TEXT_LIMIT ? substr($title, 0, DEFAULT_SOCIAL_REASON_TEXT_LIMIT) . "..." : $title : '';
                                            ?>
                                        </p>
                                        <div class="check-box control-group">
                                            <div>
                                                <input type="checkbox" name="company_list[<?= $key; ?>]" class="checkbox-custom select_company select_company_global" value="<?= $value['u_id']; ?>" id="company_list-<?= $value['u_id']; ?>">
                                                <label class="checkbox-custom-label" for="company_list-<?= $value['u_id']; ?>"></label>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <?php
                                $set_default++;
                            endforeach;
                            ?>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="item-btn-bx">
                    <div class="btn-bx">
                        <button class="round-btn red-bg btn btn-block btn-social btn-profile submit_applicaiton" type="submit">
                            <span class="dark-red">
                                <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/forward.png">
                            </span>
                            <?= lang("COMMON_TO_SEND"); ?>
                        </button>
                    </div>
                </div>
                <?php echo form_close(); ?>

                <div class="clearfix"></div>

                <!--    Modal pop-up for PAYMENT OPTIONS systempay & Paypal -->
                <div class="modal fade hm-signin-popup hm-popup" id="payment_options_popup"  data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <div class="modal-title" id="myModalLabel">
                                    <span class=""><img src="<?= ASSETS_PATH ?>images/envoyez_icon.png" alt="" class="img-responsive send_app_img"></span>
                                    <h3><?= lang('FREE_ITEM_SELECT_PAYMENT_METHOD') ?></h3>
                                </div>
                            </div>
                            <div class="btn-line">
                                <img src="<?= ASSETS_PATH ?>images/line.png" alt="" class="img-responsive">
                            </div>
                            <div class="modal-body">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="msg">
                                        <?= lang("FREE_ITEM_SELECT_PAYMENT_TEXT"); ?>
                                    </div>
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row social_div_box">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding-right">
                                            <?php echo form_open(USERS_PATH . '/form_payment', array("method" => "POST", "id" => "form_payment")); ?>
                                            <input type="hidden" name="vads_tarrif" class="payment_teriff" value="">
                                            <input type="hidden" name="application_send_by" class="application_send_by" value="0">
                                            <input type="hidden" name="all_selected_ids" class="all_selected_ids" value="">
                                            <button class="validationButton pull-right" type="submit">
                                                <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/systempay.png">
                                            </button>
                                            <?php echo form_close(); ?>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding-left">
                                            <?php echo form_open(USERS_PATH . '/form_paypal', array("method" => "POST", 'id' => "form_paypal")); ?>
                                            <input type="hidden" name="vads_tarrif" class="payment_teriff" value="">
                                            <input type="hidden" name="application_send_by" class="application_send_by" value="0">
                                            <input type="hidden" name="all_selected_ids" class="all_selected_ids" value="">
                                            <button class="validationButton" type="submit">
                                                <img class="img-responsive paypal_image" alt="" src="<?= ASSETS_PATH ?>images/paypal.png">
                                            </button>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>

                <!-- End of Modal pop-up for PAYMENT OPTIONS systempay & Paypal -->
                <div class="clearfix"></div>
                <?php
            else:
                ?>
                <h3 class="no_comp_found"><?= "0 " . lang("FREE_ITEM_MATCH_CRITERIA"); ?></h3>
            <?php endif; ?>
        </div>
    </div>
</div>

<script src='http://connect.facebook.net/en_US/all.js'></script>

<script type="text/javascript">
                            var company_select_message = "<?= lang("FREE_ITEM_SEND_MAX_SELECT_COMPANY"); ?>";
                            var company_select_required = "<h3 class='no_comp_found'><?= lang("FREE_ITEM_COMPANY_REQUIRED"); ?></h3>";

                            var invitation_url = "<?= HOME_PATH . "/verify_invitation_code?invitation_code=" . $user_data['u_invitation_code']; ?>";
                            var logo_url = "<?= ASSETS_PATH . 'images/main_logo.png'; ?>";
                            var company_data = "<?= count($company_data) ?>";
                            var send_by_email_rate = "<?= $payment_in_cent_per_email; ?>";
                            var send_by_post_rate = "<?= $payment_in_rate_per_post; ?>";

                            var send_by_email_points = parseInt("<?= $send_by_email_points; ?>");
                            var send_by_post_points = parseInt("<?= $send_by_post_points; ?>");

                            var no_company_found = "<h3 class='no_comp_found'><?= lang('FREE_ITEM_SEND_NO_COMPANY'); ?></h3>";

                            var send_by_email_points_over = "<h3 class='no_comp_found'><?= lang('FREE_ITEM_TARIFF_LIMIT_OVER'); ?></h3>";
                            var send_by_post_points_over = "<h3 class='no_comp_found'><?= lang('FREE_ITEM_TARIF_LIMIT_POST_OVER'); ?></h3>";

                            var invalid_tariff_amount = "<h3 class='no_comp_found'><?= lang('FREE_ITEM_CHOOSE_FORMULA_AND_LIMIT'); ?></h3>";
                            var common_select_all = "<?= lang('COMMON_SELECT_ALL') ?>";
                            var common_deselect_all = "<?= lang('COMMON_DESELECT_ALL') ?>";

                            var autocomplete = '';
                            function initAutocomplete() {
                                autocomplete = new google.maps.places.Autocomplete(
                                        (document.getElementById('autocomplete')),
                                        {types: ['geocode']}
                                );
                            }

                            function geolocate() {
                                if (navigator.geolocation) {
                                    navigator.geolocation.getCurrentPosition(function (position) {
                                        var geolocation = {
                                            lat: position.coords.latitude,
                                            lng: position.coords.longitude
                                        };
                                        var circle = new google.maps.Circle({
                                            center: geolocation,
                                            radius: position.coords.accuracy
                                        });
                                        autocomplete.setBounds(circle.getBounds());
                                    });
                                }
                            }

                            jQuery(document).ready(function () {
                                FB.init({
                                    appId: '<?= FB_APP_ID ?>',
                                    status: true,
                                    cookie: true
                                });

                                var new_value;

                                // send by email
                                jQuery('#ex1').slider({
                                    tooltip: 'show',
                                    formatter: function (value) {
                                        if (value == 0) {
                                            jQuery(".update_price_email").text(parseInt(parseInt(value) * parseInt(send_by_email_rate)));
                                        } else if (value == send_by_email_points) {
                                            jQuery(".update_price_email").text(0);
                                        } else if (value > send_by_email_points) {
                                            new_value = parseInt(parseInt(value) - parseInt(send_by_email_points));
                                            jQuery(".update_price_email").text(parseInt(parseInt(new_value) * parseInt(send_by_email_rate)));
                                        } else {
                                            jQuery(".update_price_email").text(0);
                                        }
                                        return "<?= lang('FREE_ITEM_CURRENT_TERIFF') ?>" + value;
                                    },
                                });

                                // send by post
                                var new_post_value;
                                jQuery('#ex2').slider({
                                    tooltip: 'show',
                                    formatter: function (postvalue) {
                                        if (postvalue == 0) {
                                            jQuery(".update_price_post").text(parseInt(parseInt(postvalue) * parseInt(send_by_post_rate)));
                                        } else if (postvalue == send_by_post_points) {
                                            jQuery(".update_price_post").text(0);
                                        } else if (postvalue > send_by_post_points) {
                                            new_post_value = parseInt(parseInt(postvalue) - parseInt(send_by_post_points));
                                            jQuery(".update_price_post").text(parseInt(parseInt(new_post_value) * parseInt(send_by_post_rate)));
                                        } else {
                                            jQuery(".update_price_post").text(0);
                                        }
                                        return "<?= lang('FREE_ITEM_CURRENT_TERIFF') ?>" + postvalue;
                                    },
                                });


                                $('#ex1').on('slideStart', function () {
                                    var get_selected_companies = $('#send_application input:checkbox:checked').map(function () {
                                        return this.value;
                                    }).get().length;

                                    if (get_selected_companies != 0) {
                                        swal({
                                            title: "<?= lang("FREE_ITEM_ARE_YOU_SURE"); ?>",
                                            text: "<?= lang("FREE_ITEM_ALERT_TEXT"); ?>",
                                            type: "warning",
                                            showCancelButton: true,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "<?= lang('FREE_ITEM_ALERT_YES'); ?>",
                                            cancelButtonText: "<?= lang('FREE_ITEM_ALERT_NO'); ?>",
                                            closeOnConfirm: true,
                                            closeOnCancel: true
                                        },
                                                function (isConfirm) {
                                                    if (isConfirm) {
                                                        $("#send_application input:checkbox").prop('checked', false);
                                                        $('#select_all_companies').prop('checked', false);
                                                        $('.select_all_companies').text(common_select_all);
                                                    } else {
                                                        $("#ex1").slider('option', {
                                                            value: get_selected_companies
                                                        });

                                                        $("#ex1").slider("refresh");
                                                    }
                                                });
                                    }
                                });


                                $('#ex2').on('slideStart', function () {
                                    var get_selected_companies = $('#send_application input:checkbox:checked').map(function () {
                                        return this.value;
                                    }).get().length;

                                    if (get_selected_companies != 0) {
                                        swal({
                                            title: "<?= lang("FREE_ITEM_ARE_YOU_SURE"); ?>",
                                            text: "<?= lang("FREE_ITEM_ALERT_TEXT"); ?>",
                                            type: "warning",
                                            showCancelButton: true,
                                            confirmButtonColor: "#DD6B55",
                                            confirmButtonText: "<?= lang('FREE_ITEM_ALERT_YES'); ?>",
                                            cancelButtonText: "<?= lang('FREE_ITEM_ALERT_NO'); ?>",
                                            closeOnConfirm: true,
                                            closeOnCancel: true
                                        },
                                                function (isConfirm) {
                                                    if (isConfirm) {
                                                        $("#send_application input:checkbox").prop('checked', false);
                                                        $('#select_all_companies').prop('checked', false);
                                                        $('.select_all_companies').text(common_select_all);
                                                    } else {
                                                        $("#ex2").slider('option', {
                                                            value: get_selected_companies
                                                        });

                                                        $("#ex2").slider("refresh");
                                                    }
                                                });
                                    }
                                });

                                // for select company
                                $('#multiple-checkboxes1').multiselect();
                            });


                            function postToFeed() {
                                var obj = {
                                    method: 'feed',
                                    link: invitation_url,
                                    picture: logo_url,
                                    name: '<?= APP_NAME ?>',
                                    caption: "<?= lang('FREE_ITEM_INVITATION_LINK'); ?>",
                                    description: "<?= lang('FREE_ITEM_INVITATION_LINK_SHARE'); ?>"
                                };
                                function callback(response) {
                                    if (response != '' && response['post_id'] != '') {
                                        toastr.success("<?= lang('FREE_ITEM_INVITATION_SENT_SUCCESS'); ?>", {timeOut: 5000});
                                    }
                                }
                                FB.ui(obj, callback);
                            }

</script>
<?php
$selected_language = !empty($this->session->userdata()['language']) ? $this->session->userdata()['language'] : DEFAULT_LANG;
if ($selected_language == "fr") {
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_API_KEY ?>&libraries=places&callback=initAutocomplete&language=fr"
    async defer></script>
    <?php
} else {
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_API_KEY ?>&libraries=places&callback=initAutocomplete&language=en"
    async defer></script>
    <?php
}
?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/pages/invite_friends.js"></script>