<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<div class="build-cv top-section send-application">
    <div class="container">
        <div class="title-box">
            <label>E</label>
            <h3 class="green-light-font"><?= lang("SEND_APPLICATION_SEND_YOUR") ?> <span class="red-font"><?= lang("COMMON_APPLICATION_TITLE") ?></span> <?= lang("SEND_APPLICATION_APP") ?></h3>
        </div>
        <?php
        echo form_open_multipart(USERS_PATH . '/validate_application', array("id" => "send_application_now_form"));
        ?>
        <input type="hidden" name="send_application_now_form" value="true">
        <input type="hidden" name="user_doc_id" value="<?= !empty($user_docs['udf_id']) ? $user_docs['udf_id'] : 0; ?>">
        <div class="cv-photo user_cv_type">
            <div class="img-box">
                <div class="profile-img">
                    <img alt="profie photo" class="img-responsive img-circle" height="180px" width="180px" src="<?= IMAGE_MANIPULATION_URL . "url=" . $this->session->userdata("profile_photo") . "&h=180&w=180&default=true" ?>">
                </div>
            </div>
        </div>
        <div class="candidate-details-text">
            <div class="row user_cv_type">
                <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                    <p><?= ucwords($this->session->userdata("user_first_name") . " " . $this->session->userdata("user_last_name")); ?></p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 mob-width">
                    <?php if (in_array(1, explode(',', $user_data['ud_contarct_type']))): ?>
                        <div class="check-box control-group">
                            <div>
                                <input id="checkbox-4" class="checkbox-custom " name="contract_type[]" type="checkbox" value="1" checked="checked">
                                <label for="checkbox-4" class="checkbox-custom-label"><?= lang('BUILDCV_PAGE_CONTRACT_TYPE_1') ?></label>
                            </div>
                        </div>
                        <?php
                    endif;
                    ?>
                    <?php if (in_array(2, explode(',', $user_data['ud_contarct_type']))): ?>
                        <div class="check-box control-group">
                            <div>
                                <input id="checkbox-5" class="checkbox-custom" name="contract_type[]" type="checkbox" value="2" checked="checked">
                                <label for="checkbox-5" class="checkbox-custom-label"><?= lang('BUILDCV_PAGE_CONTRACT_TYPE_2') ?></label>
                            </div>
                        </div>
                        <?php
                    endif;
                    ?>
                    <?php if (in_array(3, explode(',', $user_data['ud_contarct_type']))): ?>
                        <div class="check-box control-group">
                            <div>
                                <input id="checkbox-6" class="checkbox-custom" name="contract_type[]" type="checkbox" value="3" checked="checked">
                                <label for="checkbox-6" class="checkbox-custom-label"><?= lang('BUILDCV_PAGE_CONTRACT_TYPE_3') ?></label>
                            </div>
                        </div>
                        <?php
                    endif;
                    ?>
                    <?php if (in_array(4, explode(',', $user_data['ud_contarct_type']))): ?>
                        <div class="check-box control-group">
                            <div>
                                <input id="checkbox-7" class="checkbox-custom" name="contract_type[]" type="checkbox" value="4" checked="checked">
                                <label for="checkbox-7" class="checkbox-custom-label"><?= lang('BUILDCV_PAGE_CONTRACT_TYPE_4') ?></label>
                            </div>    
                        </div>
                        <?php
                    endif;
                    ?>
                    <?php if (in_array(5, explode(',', $user_data['ud_contarct_type']))): ?>
                        <div class="check-box control-group">
                            <div>
                                <input id="checkbox-8" class="checkbox-custom" name="contract_type[]" type="checkbox" value="5" checked="checked">
                                <label for="checkbox-8" class="checkbox-custom-label"><?= lang('BUILDCV_PAGE_CONTRACT_TYPE_5') ?></label>
                            </div>
                        </div>
                        <?php
                    endif;
                    ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-5 col-xs-6 mob-width">
                    <p>
                        <?php
                        $work_place_list = '';
                        if (in_array(1, explode(',', $user_data['ud_work_place_option']))) :
                            $work_place_list .=lang('BUILDCV_PAGE_SELECT_WORK_PLACES_OPT_1') . " - ";
                        endif;
                        if (in_array(2, explode(',', $user_data['ud_work_place_option']))) :
                            $work_place_list.=lang('BUILDCV_PAGE_SELECT_WORK_PLACES_OPT_2') . " - ";
                        endif;
                        if (in_array(3, explode(',', $user_data['ud_work_place_option']))) :
                            $work_place_list.=lang('BUILDCV_PAGE_SELECT_WORK_PLACES_OPT_3') . "";
                        endif;
                        echo $work_place_list;
                        ?>
                    </p>
                </div>
            </div>
            <?php if (!empty($user_data['wj_name_' . $this->current_lang])) : ?>
                <div class="row user_cv_type">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                        <p>
                            <?php
                            echo trim($user_data['wj_name_' . $this->current_lang]);
                            ?>
                        </p>
                    </div>
                </div>
            <?php endif; ?>

            <div class="row">
                <?php if (!empty($user_data['activity_name'])) : ?>
                    <div class="col-lg-5 col-md-4 col-sm-6 col-xs-12 user_cv_type">
                        <p>
                            <?php
                            echo lang("BUILDCV_PAGE_SELECT_ACTIVITY_AREA") . " : ";
                            if (!empty($user_data['activity_name'])) :
                                echo $user_data['activity_name'];
                            endif;
                            ?>
                        </p>
                    </div>
                <?php endif; ?>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-lg-offset-3 col-md-offset-4">
                    <p><?= lang("SEND_APPLICATION_NOW_COMPANY_TO_EXCLUDED"); ?> : <?= (!empty($user_docs['udf_blocked_email'])) ? count(explode(',', $user_docs['udf_blocked_email'])) : 0; ?> <?= lang("COMMON_BUSINESS_NAME"); ?></p>
                </div>
            </div>

            <div class="modify-details edit-file-div" id="my_app_form">

                <!-- MY CV DIV -->
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 no-padding-right">
                        <p class="selected_text"><?= lang("SEND_APPLICATION_NOW_SELECTED_CV"); ?> : </p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-10">
                        <label class="upload-file-btn my_cv_label">
                            <span class="cv_file_name"><?= !empty($user_docs['udf_cv_file_name']) ? $user_docs['udf_cv_file_name'] : lang("COMMON_CHOOSE_FILE") ?></span>
                            <input type="hidden" style="visibility: hidden;" class="my_hidden_cv" value="<?= !empty($user_docs['udf_cv_file_name']) ? $user_docs['udf_cv_file_name'] : '' ?>">
                            <input type="hidden" style="visibility: hidden;" class="my_hidden_rapidjob_cv" value="<?= !empty($user_docs['udf_rapidjob_cv_file_name']) ? $user_docs['udf_rapidjob_cv_file_name'] : '' ?>">
                        </label>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mob-width no-padding set_mt">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 no-padding add-padding-left">
                            <label class="btn btn-default btn-file upload-file-edit-btn edit_my_cv_btn" for="my_cv">
                                <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/icon_modifier.png">
                                <p class="edit_cv_file_btn"><?= lang("EDIT_BUTTON"); ?></p><input type="file" name="my_cv" id="my_cv" style="visibility: hidden;">
                            </label>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 no-padding">
                            <label class="upload-file-edit-btn remove-cv-file" for="my_cv_remove">
                                <img class="img-responsive set_cursor_pointer" alt="" src="<?= ASSETS_PATH ?>images/icon_supprimer.png">
                                <p class="remove_cv_file_btn"><?= lang("COMMON_REMOVE_BUTTON"); ?></p>
                            </label>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12 no-padding">
                            <div class="check-box control-group">
                                <div>
                                    <input type="checkbox" name="attach_my_cv" class="checkbox-custom" id="attach_my_cv" value="2" <?= !empty($user_docs['udf_type']) && $user_docs['udf_type'] == 2 ? "checked='checked'" : ""; ?>>
                                    <label class="checkbox-custom-label" for="attach_my_cv"><?= lang("SEND_APPLICATION_USE_YOUR_CV") ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <!-- MY COVER LATTER DIV-->
                <div class="row">
                    <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 no-padding-right">
                        <p class="selected_text"><?= lang("SEND_APPLICATION_NOW_SELECTED_LATTER"); ?> : </p>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-10">
                        <label class="upload-file-btn my_cover_latter_label">
                            <span class="cover_file_name"><?= !empty($user_docs['udf_cover_latter_file']) ? $user_docs['udf_cover_latter_file'] : lang("COMMON_CHOOSE_FILE") ?></span>
                            <input type="hidden" style="visibility: hidden;" class="my_hidden_cover_latter" value="<?= !empty($user_docs['udf_cover_latter_file']) ? $user_docs['udf_cover_latter_file'] : '' ?>">
                        </label>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mob-width no-padding set_mt">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 no-padding add-padding-left">
                            <label class="btn btn-default btn-file upload-file-edit-btn" for="my_cover_latter">
                                <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/icon_modifier.png">
                                <p class="edit_cv_file_btn"><?= lang("EDIT_BUTTON"); ?></p><input type="file" name="my_cover_latter" id="my_cover_latter" style="visibility: hidden;">
                            </label>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6 no-padding">
                            <label class="upload-file-edit-btn remove-cover-file" for="my_cover_remove">
                                <img class="img-responsive set_cursor_pointer" alt="" src="<?= ASSETS_PATH ?>images/icon_supprimer.png">
                                <p class="remove_cv_file_btn remove_cover_file"><?= lang("COMMON_REMOVE_BUTTON"); ?></p>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="applicatiopn-comment">
                <label class="msg-label"><?= lang("SEND_APPLICATION_NOW_SENT_MESSAGE"); ?></label>
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
                        <textarea class="comment-box form-control required" name="user_message"><?= (!empty($user_docs['udf_message'])) ? stripslashes(str_replace("\\r\\n", "\\\n", $user_docs['udf_message'])) : ''; ?></textarea>
                    </div>
                </div>
            </div>
            <div class="application-btn-box candidate-application-btn">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="applicantion-btn left-btn">
                            <a href="<?= USERS_PATH . '/send_application'; ?>" class="round-btn red-bg btn btn-block btn-social btn-profile">
                                <span class="dark-red">
                                    <img src="<?= ASSETS_PATH ?>images/edit.png" alt="" class="img-responsive">
                                </span>
                                <?= lang("COMMON_EDIT_INFO_BUTTON"); ?>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                        <div class="applicantion-btn right-btn">
                            <button type="submit" class="round-btn parrot-green-bg btn btn-block btn-social btn-profile">
                                <span class="parrot-dark-green-bg">
                                    <img src="<?= ASSETS_PATH ?>images/done.png" alt="" class="img-responsive">
                                </span>
                                <?= lang("COMMON_VALIDATE"); ?>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <?php
        echo form_close();
        ?>
    </div>
</div>
<script type="text/javascript">
    var choose_file = "<?= lang("COMMON_CHOOSE_FILE") ?>";
    var size_limit = '<?= DEFAULT_MAXIMUM_CV_SIZE_LIMIT_JS ?>';
    var invalid_file = '<?= lang("ERROR_CV_INVALID_SIZE"); ?>';
    var invalid_file_extension = "<?= lang("ERROR_CV_INVALID_EXTENSION"); ?>";
    var invalid_cover_extension = "<?= lang("ERROR_COVER_INVALID_EXTENSION"); ?>";
    var required_field_title = "<?= lang("COMMON_THIS_REQUIRED_FIELD"); ?>";
    var required_field_message = "<?= lang("SEND_APPLICATION_SELECT_CV"); ?>";
    var doc_id = "<?= !empty($user_docs['udf_id']) && is_numeric($user_docs['udf_id']) ? $user_docs['udf_id'] : ''; ?>";
    var user_cv_type = "<?= !empty($user_docs['udf_type']) && is_numeric($user_docs['udf_type']) ? $user_docs['udf_type'] : 0; ?>";
    var user_disable_cv_btn = "<?= !empty($user_docs['udf_type']) && $user_docs['udf_type'] == 2 ? 2 : 1; ?>";
    var cv_file_name = "<?= !empty($user_docs['udf_cv_file_name']) ? $user_docs['udf_cv_file_name'] : '' ?>";
    var rapidjob_cv_file_name = "<?= !empty($user_docs['udf_rapidjob_cv_file_name']) ? $user_docs['udf_rapidjob_cv_file_name'] : '' ?>";
    var user_exp_data = "<?= $user_exp_data; ?>";
    var user_skill_data = "<?= $user_skill_data; ?>";
    var user_training_data = "<?= $user_training_data; ?>";
    var provide_cv_details = "<?= lang('SHOW_PROFILE_GENERATE_CV_ERROR'); ?>";
    var generate_your_cv_message = "<?= lang('COMMON_GENERATE_CV_MESSAGE'); ?>";
    var notice_message = "<?= lang('COMMON_NOTICE'); ?>";
    var user_activity_area = "<?= isset($user_data) && isset($user_data['ud_activity_area']) && !empty($user_data['ud_activity_area']) ? trim($user_data['ud_activity_area']) : ''; ?>";

</script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/pages/send_application_now.js"></script>