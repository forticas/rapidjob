<div id="generate_user_cv">
    <div>
        <style type="text/css">
            #generate_user_cv{
                padding: 0 0 10px 20px;
            }
            #generate_user_cv .light-white-bg .container {
                background-color: rgb(255, 255, 255);
            }
            #generate_user_cv #experience-section {
                padding: 0;
                padding: 20px 0 5px;
            }
            #generate_user_cv .candidate-profile .profile-infor-ttl {
                padding: 20px 0 5px;
            }
            #generate_user_cv .profile-user-info{
                color: #FA6A63;
                font-size: 18px;
                font-weight: 500;
                margin: 10px 0 5px;
                padding: 0;
            }
            #generate_user_cv .profile-infor-ttl {
                color: #FA6A63;
                font-size: 25px;
                font-weight: 500;
                margin: 10px 0 5px;
                padding: 0;
                font-weight: normal !important;
                display: inline-block;
            }
            #generate_user_cv .profile-info-bx {
                margin: 20px;
                padding: 10px;
                font-size: 18px;
                font-weight: normal;
                color: rgb(76, 76, 76);
            }
            #generate_user_cv .profile-info-bx p {
                color: rgb(76, 76, 76);
                font-size: 18px;
                font-weight: normal;
                margin: 0;
                padding: 0;
            }
            #generate_user_cv .extra-pad-head{
                padding: 0 0 30px;
            }
            #generate_user_cv .extra-pad-text{
                padding: 0 0 10px;
            }
            #generate_user_cv .extra-pad {
                padding: 0 0 10px;
            }
            #generate_user_cv .extra-description {
                letter-spacing: normal;
            }
            #generate_user_cv #cources-section {
                padding: 0;
            }
            #generate_user_cv #skills-section {
                padding: 20px 0 5px;
            }
            #generate_user_cv #skills-section .profile-info-bx .text-box {
                padding: 0;
                position: relative;
            }
            #generate_user_cv .profile-info-bx .text-box span {
                color: rgb(76, 76, 76);
                font-size: 18px;
                margin: 0 !important;
                padding: 0;
            }
            #generate_user_cv .set_hobbies_text{
                color: rgb(76, 76, 76);
                font-size: 18px;
                margin: 0 !important;
                padding: 0;
            }
            #generate_user_cv .set_td_font{
                color: rgb(76, 76, 76);
                font-size: 16px;
                margin: 0 !important;
                padding: 0;
            }
            #generate_user_cv #hobbies-section {
                padding: 0;
            }
            #generate_user_cv .text-bx-new p.set_your_job_type{
                color: rgb(76, 76, 76);
                margin: 10px 0 0 0;
                padding: 10px 0 0 20px;
                text-align: left;
            }
            #generate_user_cv .text-bx-new span.set_your_job_type{
                color: rgb(76, 76, 76);
                margin: 10px 0 0 0;
                padding: 10px 0 0 0;
                text-align: left;
            }
            #generate_user_cv .text-bx-new label.set_your_job_type{
                color: rgb(76, 76, 76);
                margin: 10px 0 0 0;
                padding: 10px 0 0 0;
                text-align: left;
            }
            #generate_user_cv .text-bx-new div.set_your_job_type{
                color: rgb(76, 76, 76);
                margin: 10px 0 0 0;
                padding: 10px 0 0 0;
                text-align: left;
            }
        </style>
        <?php if (isset($download_type) && $download_type == 'image'): ?>
            <style type="text/css">
                #generate_user_cv .text-bx-new p.set_your_job_type{
                    letter-spacing: 2px;
                }
            </style>
        <?php endif; ?>
        <div class="cv-box top-section candidate-profile">
            <div class="container extrap-pad">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="cv-profile-bx">
                            <div class="inner-profile">
                                <?php
                                $set_user_image = UPLOAD_FILE_FOLDER . '/' . DEFAULT_IMAGE_NAME;
                                if (!empty($user_data['u_image'])) :
                                    $user_image = UPLOAD_FILE_FOLDER . '/' . UPLOAD_USERS_FOLDER . '/' . $user_data['u_id'] . '/' . $user_data['u_image'];
                                    if (file_exists($user_image)) :
                                        $set_user_image = BASE_URL . UPLOAD_FILE_FOLDER . '/' . UPLOAD_USERS_FOLDER . '/' . $user_data['u_id'] . '/' . $user_data['u_image'];
                                    endif;
                                endif;
                                ?>
                                <div class="img-box" style="margin-bottom: 10px;">
                                    <img alt="profile photo" class="img-responsive" src="<?= $set_user_image; ?>" style="height: 200px;border-radius: 50%">
                                </div>
                                <div class="text-bx-new">
                                    <p class="set_your_job_type"><b>Nom :</b> <?= ucwords($user_data['u_first_name'] . " " . $user_data['u_last_name']); ?></p>
                                    <p class="set_your_job_type"><b>Email :</b> <?= $user_data['u_email'] ?></p>
                                    <?php if (!empty($user_data['wj_name_' . $current_lang])) : ?>
                                        <p class="set_your_job_type"><b>Désignation actuelle :</b> <?= trim($user_data['wj_name_' . $current_lang]); ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php if (!empty($user_data['ud_user_bio'])) : ?>                            
                            <div class="profile-infor-ttl"  id="aboutme-section">À propos de moi:</div>
                            <div class="profile-info-bx border-radius">
                                <p class="extra-pad"><?php echo (!empty($user_data['ud_user_bio'])) ? trim(stripslashes(str_replace('\r\n', "<br>", $user_data['ud_user_bio']))) : ''; ?></p>
                            </div>
                        <?php endif; ?>
                        <?php if (!empty($user_exp_data)) : ?>
                            <div id="experience-section" class="container-fluid">
                                <div class="profile-infor-ttl">Mes expériences</div>
                                <div class="clearfix"></div>
                                <?php
                                foreach ($user_exp_data as $value) :
                                    ?>
                                    <div class="profile-info-bx border-radius">
                                        <p class="extra-pad"><b>
                                                <?php
                                                echo $value['wj_name_' . $current_lang] . " - " . $value['ue_company'] . " (" . $value['ue_place'] . ")";
                                                ?></b>
                                        </p>
                                        <p class="extra-pad">
                                            <?php
                                            $end_exp = !empty($value['ue_end_date']) ? date("F Y", $value['ue_end_date']) : "Toujours en train de travailler ici";
                                            echo "<b>Durée</b> (" . date('F Y', $value['ue_start_date']) . " <b>À<b/> " . $end_exp . ")";
                                            ?>
                                        </p>
                                        <p class="extra-description">
                                            <?php echo (!empty($value['ue_description'])) ? trim(stripslashes(str_replace('\r\n', "<br>", $value['ue_description']))) : ''; ?>
                                        </p>
                                    </div>
                                    <?php
                                endforeach;
                                ?>
                            </div>
                            <?php
                        endif;
                        if (!empty($user_training_data)) :
                            ?>
                            <div id="cources-section" class="container-fluid">
                                <div class="profile-infor-ttl">Mes formations</div>
                                <div class="clearfix"></div>
                                <?php
                                foreach ($user_training_data as $value) :
                                    ?>
                                    <div class="profile-info-bx border-radius">
                                        <p class="extra-pad"><b><?= $value['ut_name'] . " " . $value['ut_establissment']; ?></b></p>
                                        <p class="extra-pad"><?= "<b>Durée</b> (" . date('F Y', $value['ut_start_date']) . " <b>À</b> " . date("F Y", $value['ut_end_date']) . ")"; ?></p>
                                        <p class="extra-description"><?php echo (!empty($value['ut_description'])) ? trim(html_entity_decode(str_replace('\r\n', "<br>", $value['ut_description']))) : ''; ?></p>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <?php
                        endif;

                        if (!empty($user_skill_data)) :
                            if (isset($download_type) && $download_type == 'image'):
                                ?>
                                <div id="skills-section" class="container-fluid">
                                    <div class="profile-infor-ttl">Mes compétences</div>
                                    <div class="clearfix"></div>
                                    <?php
                                    $count_value = 1;
                                    foreach ($user_skill_data as $value) :
                                        ?>
                                        <div class="profile-info-bx border-radius" style="padding: 10px 46px 10px 10px;margin: 5px 0 0 20px;">
                                            <?php if (!empty($value['us_name'])): ?>
                                                <p class="extra-pad" style="padding: 10px 0 0 15px;margin: 10px 0 0 0;"><b><?= $value['us_name']; ?></b></p>
                                                <?php
                                            endif;

                                            if (!empty($value['us_skills'])):
                                                $us_skill = json_decode($value['us_skills'], true);
                                                if (!empty($us_skill)) :
                                                    foreach ($us_skill as $key => $es_value):
                                                        ?>
                                                        <div class="text-box">
                                                            <span>
                                                                <?= $key; ?>
                                                            </span>
                                                            <div class="star-rate">
                                                                <fieldset class="rating">
                                                                    <input type="radio" id="pdf-star5<?= $count_value; ?>" name="pdf_pdf_user_show_rating<?= $count_value; ?>" value="5" <?php if ($es_value == 5) echo 'checked="checked"' ?> disabled="disabled" /><label data-rating="5" class="full selected" for="pdf-star5<?= $count_value; ?>" title="Awesome - 5 stars"></label>
                                                                    <input type="radio" id="pdf-star4<?= $count_value; ?>half" name="pdf_user_show_rating<?= $count_value; ?>" value="4.5" <?php if ($es_value == 4.5) echo 'checked="checked"' ?> disabled="disabled" /><label class="half" for="pdf-star4<?= $count_value; ?>half" title="Pretty good - 4.5 stars"></label>
                                                                    <input type="radio" id="pdf-star4<?= $count_value; ?>" name="pdf_user_show_rating<?= $count_value; ?>" value="4" <?php if ($es_value == 4) echo 'checked="checked"' ?> disabled="disabled" /><label class = "full" for="pdf-star4<?= $count_value; ?>" title="Pretty good - 4 stars"></label>
                                                                    <input type="radio" id="pdf-star3<?= $count_value; ?>half" name="pdf_user_show_rating<?= $count_value; ?>" value="3.5" <?php if ($es_value == 3.5) echo 'checked="checked"' ?> /><label class="half" for="pdf-star3<?= $count_value; ?>half" title="Meh - 3.5 stars"></label>
                                                                    <input type="radio" id="pdf-star3<?= $count_value; ?>" name="pdf_user_show_rating<?= $count_value; ?>" value="3" <?php if ($es_value == 3) echo 'checked="checked"' ?> /><label class = "full" for="pdf-star3<?= $count_value; ?>" title="Meh - 3 stars"></label>
                                                                    <input type="radio" id="pdf-star2<?= $count_value; ?>half" name="pdf_user_show_rating<?= $count_value; ?>" value="2.5" <?php if ($es_value == 2.5) echo 'checked="checked"' ?> /><label class="half" for="pdf-star2<?= $count_value; ?>half" title="Kinda bad - 2.5 stars"></label>
                                                                    <input type="radio" id="pdf-star2<?= $count_value; ?>" name="pdf_user_show_rating<?= $count_value; ?>" value="2" <?php if ($es_value == 2) echo 'checked="checked"' ?> /><label class = "full" for="pdf-star2<?= $count_value; ?>" title="Kinda bad - 2 stars"></label>
                                                                    <input type="radio" id="pdf-star1<?= $count_value; ?>half" name="pdf_user_show_rating<?= $count_value; ?>" value="1.5" <?php if ($es_value == 1.5) echo 'checked="checked"' ?> /><label class="half" for="pdf-star1<?= $count_value; ?>half" title="Meh - 1.5 stars"></label>
                                                                    <input type="radio" id="pdf-star1<?= $count_value; ?>" name="pdf_user_show_rating<?= $count_value; ?>" value="1" <?php if ($es_value == 1) echo 'checked="checked"' ?> /><label class = "full" for="pdf-star1<?= $count_value; ?>" title="Sucks big time - 1 star"></label>
                                                                    <input type="radio" id="pdf-starhalf<?= $count_value; ?>" name="pdf_user_show_rating<?= $count_value; ?>" value="0.5" <?php if ($es_value == 0.5) echo 'checked="checked"' ?> /><label class="half" for="pdf-starhalf<?= $count_value; ?>" title="Sucks big time - 0.5 stars"></label>
                                                                </fieldset>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        $count_value++;
                                                    endforeach;
                                                endif;
                                            endif;
                                            ?>
                                        </div>
                                    <?php endforeach; ?>
                                </div>
                            <?php else: ?>
                                <div id="skills-section"  class="container-fluid">
                                    <div class="profile-infor-ttl">Mes compétences</div>
                                    <div class="clearfix"></div>
                                    <?php
                                    foreach ($user_skill_data as $value) :
                                        ?>
                                        <div class="profile-info-bx border-radius" style="padding: 5px 0 0 15px;margin: 5px 0 0 0;">
                                            <?php if (!empty($value['us_name'])): ?>
                                                <p class="extra-pad" style="padding: 10px 0 0 15px;margin: 10px 0 0 0;"><b><?= $value['us_name']; ?></b></p>
                                                <?php
                                            endif;
                                            if (!empty($value['us_skills'])) :
                                                $us_skill = json_decode($value['us_skills'], true);
                                                ?>
                                                <div class="text-box">
                                                    <?php
                                                    if (!empty($us_skill)) :
                                                        foreach ($us_skill as $key => $es_value) :
                                                            ?>
                                                            <div style="width: 100%;">
                                                                <div style="width: 60%;float: left">
                                                                    <p class="extra-pad" style="padding: 5px 0 0 20px;margin: 0 auto;"><?= $key ?></p>
                                                                </div>
                                                                <div style="width: 30%; float: right">
                                                                    <?php
                                                                    if ($es_value == 5) {
                                                                        for ($index = 0; $index < 5; $index++) {
                                                                            ?>
                                                                            <span><img src="<?= DEFAULT_FULL_STAR; ?>" style="height: 25px;" /></span>
                                                                            <?php
                                                                        }
                                                                    } else {
                                                                        $fullStars = $es_value % 5;
                                                                        $halfStars = $es_value - $fullStars;
                                                                        for ($index = 0; $index < $fullStars; $index++) {
                                                                            ?>
                                                                            <span><img src="<?= DEFAULT_FULL_STAR; ?>" style="height: 25px;" /></span>
                                                                            <?php
                                                                        }
                                                                        if ($halfStars > 0) {
                                                                            ?>
                                                                            <span><img src="<?= DEFAULT_HALF_STAR; ?>" style="height: 25px;"/></span>
                                                                            <?php
                                                                        }
                                                                    }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <?php
                                                        endforeach;
                                                    endif;
                                                    ?>
                                                </div>
                                                <?php
                                            endif;
                                            ?>
                                        </div>
                                        <?php
                                    endforeach;
                                    ?>
                                </div>
                            <?php
                            endif;
                        endif;

                        if (!empty($user_hobbies_data)) :
                            ?>
                            <div id="hobbies-section" class="container-fluid">
                                <div class="profile-infor-ttl">Mes loisirs</div>
                                <div class="clearfix"></div>
                                <div class="profile-info-bx border-radius">
                                    <?php
                                    foreach ($user_hobbies_data as $value) :
                                        if (!empty($value['uh_title'])) :
                                            $uh_title = json_decode($value['uh_title'], true);
                                            $uh_description = json_decode($value['uh_description'], true);
                                            foreach ($uh_title as $key => $title_value) :
                                                if (!empty(trim($title_value))):
                                                    ?>
                                                    <p style="padding: 20px 0 0 0px;margin: 0 auto;"><b><?= trim($title_value); ?> <?= !empty($uh_description[$key]) ? ':' : '' ?></b></p>
                                                    <p style="padding: 5px 0 0 0px;margin: 0 auto;"><?= !empty($uh_description[$key]) ? trim($uh_description[$key]) : ''; ?></p>
                                                    <?php
                                                endif;
                                            endforeach;
                                        endif;
                                    endforeach;
                                    ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>