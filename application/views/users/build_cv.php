<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="build-cv top-section">
    <!-- change profile photo hidden form -->
    <div class="hide">
        <?php
        echo form_open_multipart(USERS_PATH . '/upload_image', array("id" => "photo_form"));
        ?>
        <input type="file" accept="image/*" required="required" name="image" id="form_image">
        <?php
        echo form_close();
        ?>
    </div>
    <!-- End of change upload photo -->

    <?php
    echo form_open_multipart(USERS_PATH . '/build_cv', array("id" => "build_cv_form"));
    ?>
    <input type="hidden" name="ud_id" value="<?= $user_data['ud_id']; ?>">
    <div class="container">
        <div class="title-box">
            <label>C</label>
            <h3 class="green-light-font">
                <?= lang('BUILDCV_PAGE_TITLE') ?><span class="red-font"><?= lang('BUILDCV_PAGE_TITLE_RED') ?></span>
            </h3>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="cv-photo">
                    <div class="img-box">
                        <div class="profile-img">
                            <img alt="profile photo" class="img-responsive img-circle" height="180px" width="180px" src="<?= IMAGE_MANIPULATION_URL . "url=" . $this->session->userdata("profile_photo") . "&h=180&w=180&default=true" ?>" >
                        </div>
                    </div>
                    <a href="javascript:void(0)" class="change_photo">
                        <p><?= lang('BUILDCV_PAGE_TAP_PHOTO_TEXT') ?></p>
                    </a>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" id="build_cv_main_form">
                <div class="row select-height">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label for="job_type" class="select-label"><?= lang('BUILDCV_PAGE_SELECT_JOB') ?></label>
                        <div class="select-bx">
                            <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                            <select name="job_type" id="job_type" class="selectpicker required " <?php if (empty($work_job)) { ?> disabled="disabled" title="<?= lang('BUILDCV_PAGE_NO_JOB_TYPE_SET'); ?>"  <?php } else { ?> data-live-search="true" title="<?= lang('BUILDCV_PAGE_CHOOSE_JOB'); ?>" <?php } ?> >
                                <?php
                                if (!empty($work_job)) :
                                    foreach ($work_job as $value) :
                                        if ($value['wj_id'] == $user_data['ud_job']) :
                                            ?>
                                            <option selected="selected" value="<?= $value['wj_id']; ?>"><?= $value['job_name']; ?></option>
                                        <?php else: ?>
                                            <option value="<?= $value['wj_id']; ?>"><?= $value['job_name']; ?></option>
                                        <?php
                                        endif;
                                    endforeach;
                                    ?>
                                    <option value="other"><?= lang('COMMON_OTHER') ?></option>
                                    <?php
                                endif;
                                ?>
                            </select>
                            <?php if (!empty(form_error('job_type'))) : ?><label class="text-danger"><?= form_error('job_type'); ?></label><?php endif; ?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 other_workjob_title">
                            <div class="form-group">
                                <input type="text" id="job_type_title" name="job_type_title" class="form-control" data-rule-minlength="2" data-rule-maxlength="85" placeholder="<?= lang('COMMON_ADD_OTHER_WORKJOB') ?>">
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <label for="" class="select-label"><?= lang('BUILDCV_PAGE_SELECT_ACTIVITY_AREA') ?></label>
                        <div class="select-bx">
                            <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                            <select name="activity_area[]" id="activity_area" class="selectpicker required " multiple="multiple" multiple <?php if (empty($activity_area)) { ?> disabled="disabled" title="<?= lang('BUILDCV_PAGE_NO_ACTIVITY_AREA_SET'); ?>"  <?php } else { ?> data-selected-text-format="count > 1" data-live-search="true" multiple title="<?= lang('BUILDCV_PAGE_CHOOSE_ACTIVITY_AREA'); ?>" <?php } ?> >
                                <?php
                                if (!empty($activity_area)) :
                                    foreach ($activity_area as $value) :
                                        if (in_array($value['wa_id'], explode(',', $user_data['ud_activity_area']))) :
                                            ?>
                                            <option selected="selected" value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                        <?php else: ?>
                                            <option value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                        <?php
                                        endif;
                                    endforeach;
                                    ?>
                                    <option value="other"><?= lang('COMMON_OTHER') ?></option>
                                    <?php
                                endif;
                                ?>
                            </select>
                            <?php if (!empty(form_error('activity_area[]'))) : ?><label class="text-danger"><?= form_error('activity_area[]'); ?></label><?php endif; ?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 other_activity_title">
                            <div class="form-group">
                                <input type="text" id="activity_area_title" name="activity_area_title" class="form-control" data-rule-minlength="2" data-rule-maxlength="85" placeholder="<?= lang('COMMON_ADD_OTHER_ACTIVITY_AREA') ?>">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <label for="" class="select-label"><?= lang('BUILDCV_PAGE_SELECT_WORK_PLACES') ?></label>
                <div class="row min-height-sm">
                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
                        <div class="select-bx">
                            <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                            <select name="work_place[]" id="work_place" class="selectpicker required " multiple="multiple" multiple <?php if (empty($work_places)) { ?> disabled="disabled" title="<?= lang('BUILDCV_PAGE_NO_WORK_PLACE_SET'); ?>"  <?php } else { ?> data-selected-text-format="count > 1" data-live-search="true" multiple data-max-options="5" title="<?= lang('BUILDCV_PAGE_CHOOSE_WORKPLACE'); ?>" <?php } ?> >
                                <?php
                                if (!empty($work_places)) :
                                    foreach ($work_places as $value) :
                                        if (in_array($value['wp_id'], explode(',', $user_data['ud_work_place']))) :
                                            ?>
                                            <option selected="selected" value="<?= $value['wp_id']; ?>"><?= $value['wp_name']; ?></option>
                                        <?php else: ?>
                                            <option value="<?= $value['wp_id']; ?>"><?= $value['wp_name']; ?></option>
                                        <?php
                                        endif;
                                    endforeach;
                                    ?>
                                    <option value="other"><?= lang('COMMON_OTHER') ?></option>
                                    <?php
                                endif;
                                ?>
                            </select>
                            <?php if (!empty(form_error('work_place[]'))) : ?><label class="text-danger"><?= form_error('work_place[]'); ?></label><?php endif; ?>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 other_workplace_title">
                            <div class="form-group">
                                <input type="text" id="other_workplace_title" name="other_workplace_title" class="form-control" data-rule-minlength="2" data-rule-maxlength="85" placeholder="<?= lang('COMMON_ADD_OTHER_WORKPLACE') ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
                        <div class="check-box control-group">
                            <div>
                                <input id="checkbox-1" class="checkbox-custom" name="work_place_option[]" type="checkbox" value="1" <?php
                                if (in_array(1, explode(',', $user_data['ud_work_place_option']))) :
                                    echo 'checked="checked"';
                                endif;
                                ?>>
                                <label for="checkbox-1" class="checkbox-custom-label"><?= lang('BUILDCV_PAGE_SELECT_WORK_PLACES_OPT_1') ?></label>
                            </div>
                        </div>
                        <div class="check-box control-group">
                            <div>
                                <input id="checkbox-2" class="checkbox-custom" name="work_place_option[]" type="checkbox" value="2" <?php
                                if (in_array(2, explode(',', $user_data['ud_work_place_option']))) :
                                    echo 'checked="checked"';
                                endif;
                                ?>>
                                <label for="checkbox-2" class="checkbox-custom-label"><?= lang('BUILDCV_PAGE_SELECT_WORK_PLACES_OPT_2') ?></label>
                            </div>
                        </div>
                        <div class="check-box control-group">
                            <div>
                                <input id="checkbox-3" class="checkbox-custom" name="work_place_option[]" type="checkbox" value="3" <?php
                                if (in_array(3, explode(',', $user_data['ud_work_place_option']))) :
                                    echo 'checked="checked"';
                                endif;
                                ?>>
                                <label for="checkbox-3" class="checkbox-custom-label"><?= lang('BUILDCV_PAGE_SELECT_WORK_PLACES_OPT_3') ?></label>
                            </div>
                        </div>
                        <?php if (!empty(form_error('work_place_option[]'))) : ?><label class="text-danger"><?= form_error('work_place_option[]'); ?></label><?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <label for="" class="select-label"><?= lang('BUILDCV_PAGE_CONTRACT_TITLE') ?></label>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="check-box control-group float-checkbox">
                            <div>
                                <input id="checkbox-4" class="checkbox-custom " name="contract_type[]" type="checkbox" value="1" <?php
                                if (in_array(1, explode(',', $user_data['ud_contarct_type']))):
                                    echo 'checked="checked"';
                                endif;
                                ?>>
                                <label for="checkbox-4" class="checkbox-custom-label"><?= lang('BUILDCV_PAGE_CONTRACT_TYPE_1') ?></label>
                            </div>
                        </div>
                        <div class="check-box control-group float-checkbox">
                            <div>
                                <input id="checkbox-5" class="checkbox-custom" name="contract_type[]" type="checkbox" value="2" <?php
                                if (in_array(2, explode(',', $user_data['ud_contarct_type']))) :
                                    echo 'checked="checked"';
                                endif;
                                ?>>
                                <label for="checkbox-5" class="checkbox-custom-label"><?= lang('BUILDCV_PAGE_CONTRACT_TYPE_2') ?></label>
                            </div>
                        </div>
                        <div class="check-box control-group float-checkbox">
                            <div>
                                <input id="checkbox-6" class="checkbox-custom" name="contract_type[]" type="checkbox" value="3" <?php
                                if (in_array(3, explode(',', $user_data['ud_contarct_type']))) :
                                    echo 'checked="checked"';
                                endif;
                                ?>>
                                <label for="checkbox-6" class="checkbox-custom-label"><?= lang('BUILDCV_PAGE_CONTRACT_TYPE_3') ?></label>
                            </div>
                        </div>
                        <div class="check-box control-group float-checkbox">
                            <div>
                                <input id="checkbox-7" class="checkbox-custom" name="contract_type[]" type="checkbox" value="4" <?php
                                if (in_array(4, explode(',', $user_data['ud_contarct_type']))) :
                                    echo 'checked="checked"';
                                endif;
                                ?>>
                                <label for="checkbox-7" class="checkbox-custom-label"><?= lang('BUILDCV_PAGE_CONTRACT_TYPE_4') ?></label>
                            </div>
                        </div>
                        <div class="check-box control-group float-checkbox">
                            <div>
                                <input id="checkbox-8" class="checkbox-custom" name="contract_type[]" type="checkbox" value="5" <?php
                                if (in_array(5, explode(',', $user_data['ud_contarct_type']))) :
                                    echo 'checked="checked"';
                                endif;
                                ?>>
                                <label for="checkbox-8" class="checkbox-custom-label"><?= lang('BUILDCV_PAGE_CONTRACT_TYPE_5') ?></label>
                            </div>
                        </div>
                        <?php if (!empty(form_error('contract_type[]'))) : ?><label class="text-danger"><?= form_error('contract_type[]'); ?></label><?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row top-margin">
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                <label class="select-label"><?= lang('BUILDCV_PAGE_EDUCATION_LEVEL') ?></label>
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="select-bx">
                            <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                            <select name="education_level" id="education_level" class="selectpicker required " <?php if (empty($level_of_educations)) { ?> disabled="disabled" title="<?= lang('BUILDCV_PAGE_NO_EDU_LEVEL_SET'); ?>"  <?php } else { ?> data-live-search="true" title="<?= lang('BUILDCV_PAGE_CHOOSE_EDUCATION_LEVEL'); ?>" <?php } ?> >
                                <?php
                                if (!empty($level_of_educations)) :
                                    foreach ($level_of_educations as $value) :
                                        if ($value['ed_id'] == $user_data['ud_education_level']) :
                                            ?>
                                            <option selected="selected" value="<?= $value['ed_id']; ?>"><?= $value['ed_name']; ?></option>
                                        <?php else: ?>
                                            <option value="<?= $value['ed_id']; ?>"><?= $value['ed_name']; ?></option>
                                        <?php
                                        endif;
                                    endforeach;
                                    ?>
                                    <option value="other"><?= lang('COMMON_OTHER') ?></option>
                                <?php endif;
                                ?>
                            </select>
                            <?php if (!empty(form_error('education_level'))) : ?><label class="text-danger"><?= form_error('education_level'); ?></label><?php endif; ?>
                        </div>  
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 other_education_title">
                    <div class="form-group">
                        <input type="text" id="other_education_title" name="other_education_title" class="form-control" data-rule-minlength="2" data-rule-maxlength="85" placeholder="<?= lang('COMMON_ADD_OTHER_EDUCATION') ?>">
                    </div>
                </div>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-6 col-xs-12">
                <label class="select-label"><?= lang('BUILDCV_PAGE_MY_LANGUAGES') ?></label>
                <?php if (!empty($user_languages)): ?>
                    <div class="profile-info-bx border-radius selected-stars">
                        <?php
                        foreach ($user_languages as $key => $value) :
                            $user_lang = json_decode($value['ul_language'], true);
                            foreach ($user_lang as $key => $es_value) :
                                ?>
                                <div class="text-box">
                                    <span>
                                        <?= $key; ?>
                                    </span>
                                    <div class="star-rate star-section">
                                        <fieldset class="rating">
                                            <input type="radio" id="star5<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="5" <?php if ($es_value == 5) echo 'checked="checked"' ?> disabled="disabled" /><label data-rating="5" class="full selected" for="star5<?= $key; ?>" title="Awesome - 5 stars"></label>
                                            <input type="radio" id="star4<?= $key; ?>half" name="user_show_rating<?= $key; ?>" value="4.5" <?php if ($es_value == 4.5) echo 'checked="checked"' ?> disabled="disabled" /><label class="half" for="star4<?= $key; ?>half" title="Pretty good - 4.5 stars"></label>
                                            <input type="radio" id="star4<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="4" <?php if ($es_value == 4) echo 'checked="checked"' ?> disabled="disabled" /><label class = "full" for="star4<?= $key; ?>" title="Pretty good - 4 stars"></label>
                                            <input type="radio" id="star3<?= $key; ?>half" name="user_show_rating<?= $key; ?>" value="3.5" <?php if ($es_value == 3.5) echo 'checked="checked"' ?> /><label class="half" for="star3<?= $key; ?>half" title="Meh - 3.5 stars"></label>
                                            <input type="radio" id="star3<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="3" <?php if ($es_value == 3) echo 'checked="checked"' ?> /><label class = "full" for="star3<?= $key; ?>" title="Meh - 3 stars"></label>
                                            <input type="radio" id="star2<?= $key; ?>half" name="user_show_rating<?= $key; ?>" value="2.5" <?php if ($es_value == 2.5) echo 'checked="checked"' ?> /><label class="half" for="star2<?= $key; ?>half" title="Kinda bad - 2.5 stars"></label>
                                            <input type="radio" id="star2<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="2" <?php if ($es_value == 2) echo 'checked="checked"' ?> /><label class = "full" for="star2<?= $key; ?>" title="Kinda bad - 2 stars"></label>
                                            <input type="radio" id="star1<?= $key; ?>half" name="user_show_rating<?= $key; ?>" value="1.5" <?php if ($es_value == 1.5) echo 'checked="checked"' ?> /><label class="half" for="star1<?= $key; ?>half" title="Meh - 1.5 stars"></label>
                                            <input type="radio" id="star1<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="1" <?php if ($es_value == 1) echo 'checked="checked"' ?> /><label class = "full" for="star1<?= $key; ?>" title="Sucks big time - 1 star"></label>
                                            <input type="radio" id="starhalf<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="0.5" <?php if ($es_value == 0.5) echo 'checked="checked"' ?> /><label class="half" for="starhalf<?= $key; ?>" title="Sucks big time - 0.5 stars"></label>
                                        </fieldset>
                                    </div>
                                </div>
                                <?php
                            endforeach;
                        endforeach;
                        ?>
                    </div>
                <?php endif; ?>
                <div class="row count_total_language">
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="form-group">
                            <input class="form-control user_languages" type="text" placeholder="<?= lang('BUILDCV_PAGE_ENTER_LANGUAGES') ?>" id="my_language" name="my_language[1]" data-rule-minlength="2" data-rule-maxlength="30">
                        </div>
                        <?php if (!empty(form_error('my_language[]'))) : ?><label class="text-danger"><?= form_error('my_language[]'); ?></label><?php endif; ?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-12">
                        <div class="star-ratting-bx">
                            <div class="star-rate">
                                <fieldset class="rating">
                                    <input class="user_language_rating" type="radio" value="5" name="my_language_rating[]" id="my_language_rating5"><label title="Awesome - 5 stars" for="my_language_rating5" class="full"></label>
                                    <input class="user_language_rating" type="radio" value="4.5" name="my_language_rating[]" id="my_language_rating4half"><label title="Pretty good - 4.5 stars" for="my_language_rating4half" class="half"></label>
                                    <input class="user_language_rating" type="radio" value="4" name="my_language_rating[]" id="my_language_rating4"><label title="Pretty good - 4 stars" for="my_language_rating4" class="full"></label>
                                    <input class="user_language_rating" type="radio" value="3.5" name="my_language_rating[]" id="my_language_rating3half"><label title="Meh - 3.5 stars" for="my_language_rating3half" class="half"></label>
                                    <input class="user_language_rating" type="radio" value="3" name="my_language_rating[]" id="my_language_rating3"><label title="Meh - 3 stars" for="my_language_rating3" class="full"></label>
                                    <input class="user_language_rating" type="radio" value="2.5" name="my_language_rating[]" id="my_language_rating2half"><label title="Kinda bad - 2.5 stars" for="my_language_rating2half" class="half"></label>
                                    <input class="user_language_rating" type="radio" value="2" name="my_language_rating[]" id="my_language_rating2"><label title="Kinda bad - 2 stars" for="my_language_rating2" class="full"></label>
                                    <input class="user_language_rating" type="radio" value="1.5" name="my_language_rating[]" id="my_language_rating1half"><label title="Meh - 1.5 stars" for="my_language_rating1half" class="half"></label>
                                    <input class="user_language_rating" type="radio" value="1" name="my_language_rating[]" id="my_language_rating1"><label title="Sucks big time - 1 star" for="my_language_rating1" class="full"></label>
                                    <input class="user_language_rating" type="radio" value="0.5" name="my_language_rating[]" id="my_language_ratinghalf"><label title="Sucks big time - 0.5 stars" for="my_language_ratinghalf" class="half"></label>
                                </fieldset>
                            </div>
                        </div>
                        <?php if (!empty(form_error('my_language_rating[]'))) : ?><label class="text-danger"><?= form_error('my_language_rating[]'); ?></label><?php endif; ?>
                    </div>
                </div>
                <div id="add_language_dynamic"></div>
                <div class="pupup-button">
                    <a href="javascript:void(0)" id="add_language">
                        <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/add.png">
                        <span class="poup-span"><?= lang('BUILDCV_PAGE_ADD_LANGUAGES') ?></span>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <label class="select-label"><?= lang('BUILDCV_PAGE_MY_SKILLS') ?></label>
                <?php
                if (!empty($user_skills)):
                    foreach ($user_skills as $key => $value) :
                        ?>
                        <div class="profile-info-bx border-radius selected-stars">
                            <p class="extra-pad">
                                <?= $value['us_name']; ?>
                            </p>
                            <?php
                            $user_skill = json_decode($value['us_skills'], true);
                            if (!empty($user_skill)) :
                                foreach ($user_skill as $key => $es_value) :
                                    ?>
                                    <div class="text-box">
                                        <span>
                                            <?= $key; ?>
                                        </span>
                                        <div class="star-rate">
                                            <fieldset class="rating">
                                                <input type="radio" id="skill_star5<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="5" <?php if ($es_value == 5) echo 'checked="checked"' ?> disabled="disabled" /><label data-rating="5" class="full selected" for="skill_star5<?= $key; ?>" title="Awesome - 5 stars"></label>
                                                <input type="radio" id="skill_star4<?= $key; ?>half" name="user_show_rating<?= $key; ?>" value="4.5" <?php if ($es_value == 4.5) echo 'checked="checked"' ?> disabled="disabled" /><label class="half" for="skill_star4<?= $key; ?>half" title="Pretty good - 4.5 stars"></label>
                                                <input type="radio" id="skill_star4<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="4" <?php if ($es_value == 4) echo 'checked="checked"' ?> disabled="disabled" /><label class = "full" for="skill_star4<?= $key; ?>" title="Pretty good - 4 stars"></label>
                                                <input type="radio" id="skill_star3<?= $key; ?>half" name="user_show_rating<?= $key; ?>" value="3.5" <?php if ($es_value == 3.5) echo 'checked="checked"' ?> /><label class="half" for="skill_star3<?= $key; ?>half" title="Meh - 3.5 stars"></label>
                                                <input type="radio" id="skill_star3<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="3" <?php if ($es_value == 3) echo 'checked="checked"' ?> /><label class = "full" for="skill_star3<?= $key; ?>" title="Meh - 3 stars"></label>
                                                <input type="radio" id="skill_star2<?= $key; ?>half" name="user_show_rating<?= $key; ?>" value="2.5" <?php if ($es_value == 2.5) echo 'checked="checked"' ?> /><label class="half" for="skill_star2<?= $key; ?>half" title="Kinda bad - 2.5 stars"></label>
                                                <input type="radio" id="skill_star2<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="2" <?php if ($es_value == 2) echo 'checked="checked"' ?> /><label class = "full" for="skill_star2<?= $key; ?>" title="Kinda bad - 2 stars"></label>
                                                <input type="radio" id="skill_star1<?= $key; ?>half" name="user_show_rating<?= $key; ?>" value="1.5" <?php if ($es_value == 1.5) echo 'checked="checked"' ?> /><label class="half" for="skill_star1<?= $key; ?>half" title="Meh - 1.5 stars"></label>
                                                <input type="radio" id="skill_star1<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="1" <?php if ($es_value == 1) echo 'checked="checked"' ?> /><label class = "full" for="skill_star1<?= $key; ?>" title="Sucks big time - 1 star"></label>
                                                <input type="radio" id="skill_starhalf<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="0.5" <?php if ($es_value == 0.5) echo 'checked="checked"' ?> /><label class="half" for="skill_starhalf<?= $key; ?>" title="Sucks big time - 0.5 stars"></label>
                                            </fieldset>
                                        </div>
                                    </div>
                                    <?php
                                endforeach;
                            endif;
                            ?>
                        </div>
                        <?php
                    endforeach;
                endif;
                ?>

                <div class="row">
                    <!-- MY_SKILL -->
                    <div class="col-xs-12" total_skills="<?= count($user_skill_data) ?>">
                        <?php
                        if (!empty($user_skill_data)) :
                            $count_value = 1;
                            foreach ($user_skill_data as $value) :
                                ?>
                                <div class="profile-info-bx border-radius skill_row_<?= $value['us_id'] ?>">
                                    <?php if (!empty($value['us_name'])): ?>
                                        <p class="extra-pad">
                                            <?= $value['us_name']; ?>
                                        </p>
                                        <?php
                                    endif;
                                    if (!empty($value['us_skills'])) :
                                        $us_skill = json_decode($value['us_skills'], true);
                                        if (!empty($us_skill)) :
                                            foreach ($us_skill as $key => $es_value) :
                                                ?>
                                                <div class="text-box">
                                                    <span style="max-width: 100%;word-wrap: break-word">
                                                        <?= $key; ?>
                                                    </span>
                                                    <div class="star-rate star-section">
                                                        <fieldset class="rating">
                                                            <input type="radio" id="star5<?= $count_value; ?>" name="user_show_rating<?= $count_value; ?>" value="5" <?php if ($es_value == 5) echo 'checked="checked"' ?> disabled="disabled" /><label data-rating="5" class="full selected" for="star5<?= $count_value; ?>" title="Awesome - 5 stars"></label>
                                                            <input type="radio" id="star4<?= $count_value; ?>half" name="user_show_rating<?= $count_value; ?>" value="4.5" <?php if ($es_value == 4.5) echo 'checked="checked"' ?> disabled="disabled" /><label class="half" for="star4<?= $count_value; ?>half" title="Pretty good - 4.5 stars"></label>
                                                            <input type="radio" id="star4<?= $count_value; ?>" name="user_show_rating<?= $count_value; ?>" value="4" <?php if ($es_value == 4) echo 'checked="checked"' ?> disabled="disabled" /><label class = "full" for="star4<?= $count_value; ?>" title="Pretty good - 4 stars"></label>
                                                            <input type="radio" id="star3<?= $count_value; ?>half" name="user_show_rating<?= $count_value; ?>" value="3.5" <?php if ($es_value == 3.5) echo 'checked="checked"' ?> /><label class="half" for="star3<?= $count_value; ?>half" title="Meh - 3.5 stars"></label>
                                                            <input type="radio" id="star3<?= $count_value; ?>" name="user_show_rating<?= $count_value; ?>" value="3" <?php if ($es_value == 3) echo 'checked="checked"' ?> /><label class = "full" for="star3<?= $count_value; ?>" title="Meh - 3 stars"></label>
                                                            <input type="radio" id="star2<?= $count_value; ?>half" name="user_show_rating<?= $count_value; ?>" value="2.5" <?php if ($es_value == 2.5) echo 'checked="checked"' ?> /><label class="half" for="star2<?= $count_value; ?>half" title="Kinda bad - 2.5 stars"></label>
                                                            <input type="radio" id="star2<?= $count_value; ?>" name="user_show_rating<?= $count_value; ?>" value="2" <?php if ($es_value == 2) echo 'checked="checked"' ?> /><label class = "full" for="star2<?= $count_value; ?>" title="Kinda bad - 2 stars"></label>
                                                            <input type="radio" id="star1<?= $count_value; ?>half" name="user_show_rating<?= $count_value; ?>" value="1.5" <?php if ($es_value == 1.5) echo 'checked="checked"' ?> /><label class="half" for="star1<?= $count_value; ?>half" title="Meh - 1.5 stars"></label>
                                                            <input type="radio" id="star1<?= $count_value; ?>" name="user_show_rating<?= $count_value; ?>" value="1" <?php if ($es_value == 1) echo 'checked="checked"' ?> /><label class = "full" for="star1<?= $count_value; ?>" title="Sucks big time - 1 star"></label>
                                                            <input type="radio" id="starhalf<?= $count_value; ?>" name="user_show_rating<?= $count_value; ?>" value="0.5" <?php if ($es_value == 0.5) echo 'checked="checked"' ?> /><label class="half" for="starhalf<?= $count_value; ?>" title="Sucks big time - 0.5 stars"></label>
                                                        </fieldset>
                                                    </div>
                                                </div>
                                                <?php
                                                $count_value++;
                                            endforeach;
                                        endif;
                                    endif;
                                    ?>
                                </div>
                                <?php
                            endforeach;
                        else:
                            ?>
                            <div class="profile-info-bx border-radius">
                                <h3 class="no_skill_set"><?= lang('SHOW_PROFILE_PAGE_ADD_YOUR_SKILL'); ?></h3>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="clearfix"></div>
                    <!-- /END OF MY_SKILL / -->

                    <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
                        <div class="form-group">
                            <input class="form-control user_skills" type="text" placeholder="<?= lang('BUILDCV_PAGE_ENTER_SKILL') ?>" id="my_skill" name="my_skill[1]" data-rule-minlength="2" data-rule-maxlength="100">
                            <?php if (!empty(form_error('my_skill[]'))) : ?><label class="text-danger"><?= form_error('my_skill[]'); ?></label><?php endif; ?>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-4 col-xs-12">
                        <div class="star-ratting-bx">
                            <div class="star-rate">
                                <fieldset class="rating">
                                    <input class="skill_rating_input" type="radio" value="5" name="my_skill_rating[]" id="user_add_skill_star5"><label title="Awesome - 5 stars" for="user_add_skill_star5" class="full"></label>
                                    <input class="skill_rating_input" type="radio" value="4.5" name="my_skill_rating[]" id="user_add_skill_star4half"><label title="Pretty good - 4.5 stars" for="user_add_skill_star4half" class="half"></label>
                                    <input class="skill_rating_input" type="radio" value="4" name="my_skill_rating[]" id="user_add_skill_star4"><label title="Pretty good - 4 stars" for="user_add_skill_star4" class="full"></label>
                                    <input class="skill_rating_input" type="radio" value="3.5" name="my_skill_rating[]" id="user_add_skill_star3half"><label title="Meh - 3.5 stars" for="user_add_skill_star3half" class="half"></label>
                                    <input class="skill_rating_input" type="radio" value="3" name="my_skill_rating[]" id="user_add_skill_star3"><label title="Meh - 3 stars" for="user_add_skill_star3" class="full"></label>
                                    <input class="skill_rating_input" type="radio" value="2.5" name="my_skill_rating[]" id="user_add_skill_star2half"><label title="Kinda bad - 2.5 stars" for="user_add_skill_star2half" class="half"></label>
                                    <input class="skill_rating_input" type="radio" value="2" name="my_skill_rating[]" id="user_add_skill_star2"><label title="Kinda bad - 2 stars" for="user_add_skill_star2" class="full"></label>
                                    <input class="skill_rating_input" type="radio" value="1.5" name="my_skill_rating[]" id="user_add_skill_star1half"><label title="Meh - 1.5 stars" for="user_add_skill_star1half" class="half"></label>
                                    <input class="skill_rating_input" type="radio" value="1" name="my_skill_rating[]" id="user_add_skill_star1"><label title="Sucks big time - 1 star" for="user_add_skill_star1" class="full"></label>
                                    <input class="skill_rating_input" type="radio" value="0.5" name="my_skill_rating[]" id="user_add_skill_star0half"><label title="Sucks big time - 0.5 stars" for="user_add_skill_star0half" class="half"></label>
                                </fieldset>
                            </div>
                        </div>
                        <?php if (!empty(form_error('my_skill_rating[]'))) : ?><label class="text-danger"><?= form_error('my_skill_rating[]'); ?></label><?php endif; ?>
                    </div>
                </div>
                <div id="add_skill_dynamic"></div>
                <div class="pupup-button">
                    <a href="javascript:void(0)" id="add_user_skill">
                        <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/add.png">
                        <span class="poup-span">
                            <?= lang('BUILDCV_PAGE_ADD_SKILLS') ?>
                        </span>
                    </a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="edit-botton">
                    <a class="round-btn green-light btn btn-block btn-social btn-profile" href="<?= USERS_PATH ?>">
                        <span class="popup-green">
                            <img src="<?= ASSETS_PATH ?>images/eye.png" alt="" class="img-responsive">
                        </span>
                        <?= lang('BUILDCV_PAGE_VIEW_PROFILE_BTN') ?>
                    </a>
                    <button class="round-btn red-bg btn btn-block btn-social btn-profile" type="submit">
                        <span class="dark-red">
                            <img src="<?= ASSETS_PATH ?>images/forward.png" alt="" class="img-responsive">
                        </span>
                        <?= lang('SHOW_PROFILE_GENERATE_CV') ?>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <?php
    echo form_close();
    ?>
</div>
<script type="text/javascript">
    var enter_language = "<?= lang('BUILDCV_PAGE_ENTER_LANGUAGES') ?>";
    var enter_skill = "<?= lang('BUILDCV_PAGE_ENTER_SKILL') ?>";
</script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/pages/build_cv.js"></script>

