<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script src="http://bootboxjs.com/bootbox.js"></script>
<div class="build-cv top-section upload-document-bx">
    <div class="container ">
        <div class="title-box">
            <label>D</label>
            <h3 class="green-light-font"><?= lang("MY_DOCUMENTS_MY"); ?> <span class="red-font"><?= lang("MY_DOCUMENTS_DOCUMENT"); ?></span></h3>
        </div>

        <!-- THIS SECTION FOR BUILD CV -->
        <div class="upload-box">
            <div class="row">
                <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-5 no-padding-right">
                            <p><?= lang("MY_DOCUMENTS_JOIN_MY_CV"); ?></p>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-7 no-padding">
                            <div class="applicantion-btn left-btn">
                                <a href="<?= USERS_PATH . '/build_cv'; ?>"  class="round-btn red-bg btn btn-block btn-social btn-profile">
                                    <span class="dark-red">
                                        <img src="<?= ASSETS_PATH ?>images/forward.png" alt="" class="img-responsive">
                                    </span>
                                    <?= lang("MY_DOCUMENTS_CREATE_CV"); ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <!--End of cv form-->

        <!-- END OF CREATE SECTION -->

        <?php if (!empty($user_all_documents) && count($user_all_documents) > 0):
            ?>
            <div class="uploaded-box">
                <div class="row">
                    <div class="col-lg-8 col-md-10 col-sm-12 col-xs-12">
                        <label class="red-label red-font"><?= lang("MY_DOCUMENTS_LIST"); ?></label>
                        <?php
                        foreach ($user_all_documents as $value):
                            $doc_path = UPLOAD_FILE_FOLDER . '/' . UPLOAD_DOCUMENTS_FOLDER . '/' . $value['uadf_udf_id'] . '/';
                            if (!empty($value['uadf_document_file_name']) && file_exists($doc_path . $value['uadf_document_file_name'])) :
                                $cv_file = UPLOAD_ABS_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $value['uadf_udf_id'] . '/' . $value['uadf_document_file_name'];
                                ?>
                                <div class="row">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <button class="upload-file-btn cv_file_name set_default_cursor"><?= trim($value['uadf_document_file_name']) ?></button>
                                        </div>
                                    </div>
                                    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                        <ul class="edit_my_cv_label">
                                            <li>
                                                <a target="_blank" download="<?= $cv_file; ?>" href="<?= $cv_file; ?>">
                                                    <img src="<?= ASSETS_PATH ?>images/icon_visualiser.png" alt="" class="img-responsive">
                                                    <span class="upload-text-green"><?= lang("COMMON_SHOW"); ?></span>
                                                </a>
                                            </li>
                                            <li>
                                                <button class="remove-cv-file-btn" data="<?= USERS_PATH . '/remove_my_documents?id=' . $value['uadf_id']; ?>">
                                                    <img src="<?= ASSETS_PATH ?>images/icon_supprimer.png" alt="" class="img-responsive">
                                                    <span class="upload-text-dark-green"><?= lang("COMMON_REMOVE_BUTTON"); ?></span>
                                                </button>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <?php
                            endif;
                        endforeach;
                        ?>
                    </div>
                </div>
            </div>
        <?php endif; ?>
        <div class="shadow-img">
            <div class="img-bx">
                <img src="<?= ASSETS_PATH ?>images/books_icon.png" alt="" class="img-responsive">
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    var choose_file = "<?= lang("COMMON_CHOOSE_FILE") ?>";
    var size_limit = '<?= DEFAULT_MAXIMUM_CV_SIZE_LIMIT_JS ?>';
    var invalid_file = '<?= lang("ERROR_CV_INVALID_SIZE"); ?>';
    var invalid_file_extension = '<?= lang("ERROR_CV_INVALID_EXTENSION"); ?>';
    var required_field_title = "<?= lang("COMMON_THIS_REQUIRED_FIELD"); ?>";
    var required_field_message = "<?= lang("SEND_APPLICATION_SELECT_CV"); ?>";
    var remove_confirmation_cv_message = "<?= lang("MY_DOCUMENTS_REMOVE_CV_CONFIRM"); ?>";
    var remove_confirmation_cover_message = "<?= lang("MY_DOCUMENTS_REMOVE_COVER_CONFIRM"); ?>";
    var confirmation_title = "<?= lang("COMMON_CONFIRM"); ?>";
    var cancel_button = "<?= lang("COMMON_CANCEL_BTN"); ?>";
    var remove_button = "<?= lang("COMMON_REMOVE_BUTTON"); ?>";
    var user_doc_id = "<?= !empty($user_data['udf_id']) && is_numeric($user_data['udf_id']) ? $user_data['udf_id'] : ''; ?>";
</script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/pages/my_documents.js"></script>