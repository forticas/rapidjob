<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$current_lang = $this->current_lang;
?>
<style>
    /* these are just styles added for this demo page */
    #canvas{
        width: 1600px;
        margin: 0 auto;
        color: black;
        background-color: white;
    }
    .movable_div{
        color: black;
        position: absolute;
        background-color: white;
    }
</style>
<!-- this script helps us to capture any div --> 
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery-ui.js"></script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/html2canvas.js"></script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/datepicker/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/datepicker/locales/bootstrap-datepicker.fr.min.js"></script>
<div class="cv-box top-section candidate-profile">
    <div class="container extrap-pad">
        <div class="title-box">
            <label>M</label>
            <h3 class="green-light-font">
                <?= lang('COMMON_MY') ?> <span class="red-font"><?= lang('SHOW_PROFILE_PAGE_TITLE_RED') ?></span>
            </h3>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <!-- CHANGE PROFILE PHOTO -->
                <div class="cv-profile-bx">
                    <div class="inner-profile">
                        <div class="img-box">
                            <div class="profile-img">
                                <img alt="profile photo" class="img-responsive img-circle" height="180px" width="180px" src="<?= IMAGE_MANIPULATION_URL . "url=" . $this->session->userdata("profile_photo") . "&h=180&w=180&default=true" ?>" >
                            </div>
                        </div>
                        <div class="hide">
                            <?php
                            echo form_open_multipart(USERS_PATH . '/upload_image', array("id" => "photo_form"));
                            ?>
                            <input type="file" accept="image/*" required="required" name="image" id="form_image">
                            <?php
                            echo form_close();
                            ?>
                        </div>

                        <?php
                        $image = $this->session->userdata("profile_photo");
                        if (!empty($image)) :
                            $image = UPLOAD_FILE_FOLDER . '/' . $this->session->userdata("profile_photo");
                        else:
                            $image = UPLOAD_FILE_FOLDER . '/' . DEFAULT_IMAGE_NAME;
                        endif;
                        ?>
                        <p class="p-text">
                            <a target="_blank" download="<?= $image; ?>" href="<?= $image; ?>">
                                <?= lang('SHOW_PROFILE_PAGE_DOWNLOAD_PROFILE_PICTURE') ?>
                            </a>
                        </p>
                        <div class="btn-box">
                            <button class="round-flat-btn change_photo">
                                <?= lang('SHOW_PROFILE_PAGE_UPLOAD_PROFILE_PICTURE') ?>
                            </button>
                        </div>
                        <div class="text-bx">
                            <h3><?= ucwords($this->session->userdata("user_first_name") . " " . $this->session->userdata("user_last_name")); ?></h3>
                            <p class="set_your_job_type">
                                <?php
                                echo (!empty($user_data['wj_name_' . $this->current_lang])) ? trim($user_data['wj_name_' . $this->current_lang]) : lang('COMMON_JOB_WANTED');
                                ?>
                            </p>
                            <label><?= lang('SHOW_PROFILE_PAGE_INTRODUSE_YOUSELF') ?> :</label>
                        </div>
                    </div>
                </div>
                <!-- / END OF CHANGE PROFILE PHOTO / -->

                <!-- / EDIT USER BIO DESCRIPTION / -->
                <div class="cv-text-bx border-radius">
                    <?php
                    echo form_open(USERS_PATH . '/add', array("id" => "user_bio_form"));
                    ?>
                    <textarea id="ud_user_bio"  class="form-control user_bio required" name="ud_user_bio" placeholder="<?= lang('SHOW_PROFILE_PAGE_BIO_PLACEHOLDER'); ?>"><?php echo (!empty($user_data['ud_user_bio'])) ? get_slash_formatted_text($user_data['ud_user_bio']) : ''; ?></textarea>
                    <input type="hidden" name="user_bio_form" value="user_bio_form">
                    <div class="text-edit-btn-box">
                        <div class="edit-btn">
                            <button type="submit" class="submit_bio_btn" value="play">
                                <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/icon_modifier.png">
                                <span class="span-text">
                                    <?php echo lang('COMMON_SAVE'); ?>
                                </span>
                            </button>
                        </div>
                    </div>
                    <?php
                    echo form_close();
                    ?>

                </div>
                <!-- / END OF EDIT USER BIO DESCRIPTION / -->
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <a href="<?= USERS_PATH ?>/send_application">
                    <div class="candidate-list red-border">
                        <img src="<?= ASSETS_PATH ?>images/forward_big.png" alt="" class="img-responsive">
                        <h3><?= lang('SHOW_PROFILE_PAGE_SEND_YOUR_APPLICATION_BTN') ?></h3>
                    </div>
                </a>
                <?php if (count($user_skill_data) > 0 && count($user_exp_data)) : ?>
                    <a href="javascript:void(0);" class="open-download-cv-popup">
                        <div class="candidate-list green-border download">
                            <img src="<?= ASSETS_PATH ?>images/cloud_download.png" alt="" class="img-responsive">
                            <h3><?= lang('SHOW_PROFILE_PAGE_DOWNLOAD_MY_CV') ?></h3>
                        </div>
                    </a>
                <?php endif; ?>
                <div class="resume-tag">
                    <div class="row" id="my_nav_bar">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <a href="#experience-section">
                                <div class="resume-tag-bx">
                                    <div class="img-bx">
                                        <img src="<?= ASSETS_PATH ?>images/bag.png" alt="" class="img-responsive">
                                    </div>
                                    <label>
                                        <?= lang('SHOW_PROFILE_PAGE_MY_EXPERIANCES') ?>
                                    </label>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <a href="#cources-section">
                                <div class="resume-tag-bx">
                                    <div class="img-bx">
                                        <img src="<?= ASSETS_PATH ?>images/degree.png" alt="" class="img-responsive">
                                    </div>
                                    <label>
                                        <?= lang('SHOW_PROFILE_PAGE_MY_TRAININGS') ?>
                                    </label>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <a href="#skills-section">
                                <div class="resume-tag-bx">
                                    <div class="img-bx">
                                        <img src="<?= ASSETS_PATH ?>images/star.png" alt="" class="img-responsive">
                                    </div>
                                    <label>
                                        <?= lang('SHOW_PROFILE_PAGE_MY_SKILLS') ?>
                                    </label>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                            <a href="#hobbies-section">
                                <div class="resume-tag-bx">
                                    <div class="img-bx">
                                        <img src="<?= ASSETS_PATH ?>images/cap.png" alt="" class="img-responsive">
                                    </div>
                                    <label>
                                        <?= lang('SHOW_PROFILE_PAGE_MY_HOBBIES') ?>
                                    </label>
                                </div>
                            </a>
                        </div>
                    </div>
                </div>

                <!-- MY_EXPERIENCES -->
                <div id="experience-section" class="container-fluid" total_experiences="<?= count($user_exp_data) ?>">
                    <label class="profile-infor-ttl">
                        <?= lang('SHOW_PROFILE_PAGE_MY_EXPERIANCES') ?>
                    </label>
                    <?php
                    if (!empty($user_exp_data)) :
                        foreach ($user_exp_data as $value) :
                            ?>
                            <div class="profile-info-bx border-radius experience_row_<?= $value['ue_id'] ?>" 
                                 data-company="<?= $value['ue_company'] ?>"
                                 data-job="<?= $value['ue_job'] ?>" 
                                 data-place="<?= $value['ue_place'] ?>"
                                 data-desc="<?= $value['ue_description'] ?>"
                                 data-startmonth="<?= ltrim(date('m', $value['ue_start_date']), '0') ?>"
                                 data-startyear="<?= date('Y', $value['ue_start_date']) ?>"
                                 data-endmonth="<?= ltrim(date('m', $value['ue_end_date']), '0') ?>"
                                 data-endyear="<?= date('Y', $value['ue_end_date']) ?>"
                                 data-in-current-position="<?= $value['ue_in_current_position'] ?>"
                                 data-exp-id="<?= $value['ue_id'] ?>"
                                 >
                                <p class="extra-pad">
                                    <?php
                                    echo $value['wj_name_' . $this->current_lang] . " - " . $value['ue_company'] . " (" . $value['ue_place'] . ")";
                                    ?>
                                </p>
                                <p class="extra-pad">
                                    <?php
                                    $end_exp = !empty($value['ue_end_date']) ? date("F Y", $value['ue_end_date']) : lang("SHOW_PROFILE_PAGE_WORKING_HERE");
                                    echo lang("COMMON_DURATION") . " (" . date('F Y', $value['ue_start_date']) . " " . lang("SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD_TO") . " " . $end_exp . ")";
                                    ?>
                                </p>
                                <?php echo get_formatted_text($value['ue_description']); ?>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <button type="button" class="common_delete_btn delete_experience" data-id="<?= $value['ue_id'] ?>">
                                        <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/delete.png">
                                    </button>
                                    <button type="button" class="common_edit_btn edit_experience">
                                        <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/icon_modifier.png">
                                    </button>
                                </div>
                            </div>
                            <?php
                        endforeach;
                    else:
                        ?>
                        <div class="profile-info-bx border-radius">
                            <h3 class="no_exp_set"><?= lang('SHOW_PROFILE_PAGE_ADD_YOUR_EXP'); ?></h3>
                        </div>
                    <?php endif; ?>

                    <div class="pupup-button" id="add_experience_popup">
                        <a href="javascript:void(0)" id="experience-popup-btn" class="experience-popup-btn">
                            <img src="<?= ASSETS_PATH ?>images/add_blue.png" alt="" class="img-responsive">
                        </a>
                        <span class="poup-span">
                            <?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES') ?>
                        </span>
                    </div>
                </div>
                <!-- / END OF MY_EXPERIENCES / -->

                <!-- MY_TRAINING -->
                <div id="cources-section" class="container-fluid" total_trainings="<?= count($user_training_data) ?>">
                    <label class="profile-infor-ttl">
                        <?= lang('SHOW_PROFILE_PAGE_MY_TRAININGS') ?>
                    </label>

                    <?php
                    if (!empty($user_training_data)) :
                        foreach ($user_training_data as $value) :
                            ?>
                            <div class="profile-info-bx border-radius training_row_<?= $value['ut_id'] ?>"
                                 data-training="<?= $value['ut_name'] ?>" 
                                 data-establishment="<?= $value['ut_establissment'] ?>"
                                 data-desc="<?= $value['ut_description'] ?>"
                                 data-train-start-month="<?= ltrim(date('m', $value['ut_start_date']), '0') ?>"
                                 data-train-start-year="<?= date('Y', $value['ut_start_date']) ?>"
                                 data-train-end-month="<?= ltrim(date('m', $value['ut_end_date']), '0') ?>"
                                 data-train-end-year="<?= date('Y', $value['ut_end_date']) ?>"
                                 data-train-id="<?= $value['ut_id'] ?>"
                                 >
                                <p class="extra-pad">
                                    <?php
                                    echo $value['ut_name'] . " " . $value['ut_establissment'];
                                    ?>
                                </p>
                                <p class="extra-pad">
                                    <?php
                                    echo lang("COMMON_DURATION") . " (" . date('F Y', $value['ut_start_date']) . " " . lang("COMMON_TO") . " " . date("F Y", $value['ut_end_date']) . ")";
                                    ?>
                                </p>
                                <?php echo get_formatted_text($value['ut_description']); ?>
                                <div class="clearfix"></div>
                                <div class="row">
                                    <button type="button" class="common_delete_btn delete_training" data-id="<?= $value['ut_id'] ?>">
                                        <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/delete.png">
                                    </button>
                                    <button type="button" class="common_edit_btn edit_training">
                                        <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/icon_modifier.png">
                                    </button>
                                </div>
                            </div>
                            <?php
                        endforeach;
                    else:
                        ?>
                        <div class="profile-info-bx border-radius">
                            <h3 class="no_training_set"><?= lang('SHOW_PROFILE_PAGE_ADD_YOUR_TRAINING'); ?></h3>
                        </div>
                    <?php endif; ?>

                    <div class="pupup-button">
                        <div class="training_popup_open">
                            <a href="javascript:void(0)">
                                <img src="<?= ASSETS_PATH ?>images/add_blue.png" alt="" class="img-responsive">
                            </a>
                            <span class="poup-span">
                                <?= lang('SHOW_PROFILE_PAGE_ADD_TRAININGS') ?>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- / END OF MY_TRAINING/ -->

                <!-- MY_SKILL -->
                <div id="skills-section"  class="container-fluid" total_skills="<?= count($user_skill_data) ?>">
                    <label class="profile-infor-ttl">
                        <?= lang('SHOW_PROFILE_PAGE_MY_SKILLS') ?>
                    </label>
                    <?php
                    if (!empty($user_skill_data)) :
                        $count_value = 1;
                        foreach ($user_skill_data as $value) :
                            ?>
                            <div class="profile-info-bx border-radius skill_row_<?= $value['us_id'] ?>"
                                 data-skill-area="<?= $value['us_name']; ?>"
                                 <?php
                                 $count_skill = 1;
                                 $us_skill = json_decode($value['us_skills'], true);
                                 if (!empty($us_skill)):
                                     foreach ($us_skill as $key => $es_value) :
                                         ?>
                                         data-skill-<?= $count_skill ?>="<?= $key ?>"
                                         data-star-<?= $count_skill ?>="<?= $es_value ?>"
                                         data-skill-count="<?= count($us_skill) ?>"
                                         data-skill-id="<?= $value['us_id'] ?>"
                                         <?php
                                         $count_skill++;
                                     endforeach;
                                 endif;
                                 ?>
                                 >
                                     <?php if (!empty($value['us_name'])): ?>
                                    <p class="extra-pad">
                                        <?= $value['us_name']; ?>
                                    </p>
                                    <?php
                                endif;
                                if (!empty($value['us_skills'])) :
                                    $us_skill = json_decode($value['us_skills'], true);
                                    if (!empty($us_skill)):
                                        foreach ($us_skill as $key => $es_value) :
                                            ?>
                                            <div class="text-box">
                                                <span>
                                                    <?= $key; ?>
                                                </span>
                                                <div class="star-rate star-section">
                                                    <fieldset class="rating">
                                                        <input type="radio" id="star5<?= $count_value; ?>" name="user_show_rating<?= $count_value; ?>" value="5" <?php if ($es_value == 5) echo 'checked="checked"' ?> disabled="disabled" /><label data-rating="5" class="full selected" for="star5<?= $count_value; ?>" title="Awesome - 5 stars"></label>
                                                        <input type="radio" id="star4<?= $count_value; ?>half" name="user_show_rating<?= $count_value; ?>" value="4.5" <?php if ($es_value == 4.5) echo 'checked="checked"' ?> disabled="disabled" /><label class="half" for="star4<?= $count_value; ?>half" title="Pretty good - 4.5 stars"></label>
                                                        <input type="radio" id="star4<?= $count_value; ?>" name="user_show_rating<?= $count_value; ?>" value="4" <?php if ($es_value == 4) echo 'checked="checked"' ?> disabled="disabled" /><label class = "full" for="star4<?= $count_value; ?>" title="Pretty good - 4 stars"></label>
                                                        <input type="radio" id="star3<?= $count_value; ?>half" name="user_show_rating<?= $count_value; ?>" value="3.5" <?php if ($es_value == 3.5) echo 'checked="checked"' ?> /><label class="half" for="star3<?= $count_value; ?>half" title="Meh - 3.5 stars"></label>
                                                        <input type="radio" id="star3<?= $count_value; ?>" name="user_show_rating<?= $count_value; ?>" value="3" <?php if ($es_value == 3) echo 'checked="checked"' ?> /><label class = "full" for="star3<?= $count_value; ?>" title="Meh - 3 stars"></label>
                                                        <input type="radio" id="star2<?= $count_value; ?>half" name="user_show_rating<?= $count_value; ?>" value="2.5" <?php if ($es_value == 2.5) echo 'checked="checked"' ?> /><label class="half" for="star2<?= $count_value; ?>half" title="Kinda bad - 2.5 stars"></label>
                                                        <input type="radio" id="star2<?= $count_value; ?>" name="user_show_rating<?= $count_value; ?>" value="2" <?php if ($es_value == 2) echo 'checked="checked"' ?> /><label class = "full" for="star2<?= $count_value; ?>" title="Kinda bad - 2 stars"></label>
                                                        <input type="radio" id="star1<?= $count_value; ?>half" name="user_show_rating<?= $count_value; ?>" value="1.5" <?php if ($es_value == 1.5) echo 'checked="checked"' ?> /><label class="half" for="star1<?= $count_value; ?>half" title="Meh - 1.5 stars"></label>
                                                        <input type="radio" id="star1<?= $count_value; ?>" name="user_show_rating<?= $count_value; ?>" value="1" <?php if ($es_value == 1) echo 'checked="checked"' ?> /><label class = "full" for="star1<?= $count_value; ?>" title="Sucks big time - 1 star"></label>
                                                        <input type="radio" id="starhalf<?= $count_value; ?>" name="user_show_rating<?= $count_value; ?>" value="0.5" <?php if ($es_value == 0.5) echo 'checked="checked"' ?> /><label class="half" for="starhalf<?= $count_value; ?>" title="Sucks big time - 0.5 stars"></label>
                                                    </fieldset>
                                                </div>
                                            </div>
                                            <?php
                                            $count_value++;
                                        endforeach;
                                    endif;
                                endif;
                                ?>
                                <div class="clearfix"></div>
                                <div class="row common_action_btn">
                                    <button type="button" class="common_delete_btn delete_skill" data-id="<?= $value['us_id'] ?>">
                                        <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/delete.png">
                                    </button>
                                    <button type="button" class="common_edit_btn edit_skill">
                                        <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/icon_modifier.png">
                                    </button>
                                </div>
                            </div>
                            <?php
                        endforeach;
                    else:
                        ?>
                        <div class="profile-info-bx border-radius">
                            <h3 class="no_skill_set"><?= lang('SHOW_PROFILE_PAGE_ADD_YOUR_SKILL'); ?></h3>
                        </div>
                    <?php endif; ?>
                    <div class="pupup-button">
                        <div class="add-skill-popup">
                            <a href="javascript:void(0)">
                                <img src="<?= ASSETS_PATH ?>images/add_blue.png" alt="" class="img-responsive">
                            </a>
                            <span class="poup-span">
                                <?= lang('SHOW_PROFILE_PAGE_ADD_SKILLS') ?>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- /END OF MY_SKILL / -->

                <!-- MY_HOBBIES  -->
                <div id="hobbies-section" class="container-fluid" total_hobbies="<?= count($user_hobbies_data) ?>">
                    <label class="profile-infor-ttl">
                        <?= lang('SHOW_PROFILE_PAGE_MY_HOBBIES') ?>
                    </label>
                    <div class="profile-info-bx border-radius">
                        <?php
                        if (!empty($user_hobbies_data)) :
                            foreach ($user_hobbies_data as $value) :
                                if (!empty($value['uh_title'])) :
                                    ?>
                                    <div class="show_profile_div hobby_row_<?= $value['uh_id'] ?>">
                                        <?php
                                        $uh_title = json_decode($value['uh_title'], true);
                                        $uh_description = json_decode($value['uh_description'], true);
                                        foreach ($uh_title as $key => $title_value) :
                                            if (!empty(trim($title_value))):
                                                ?>

                                                <p class="extra-pad">
                                                    <?php
                                                    echo trim($title_value);
                                                    if (!empty($uh_description[$key])) :
                                                        ?>
                                                        : <label>
                                                            <?= trim($uh_description[$key]) ?>
                                                        </label>
                                                    <?php endif; ?>
                                                </p>

                                                <?php
                                            endif;
                                        endforeach;
                                        ?>
                                        <div class="clearfix"></div>
                                        <div class="row">
                                            <button type="button" class="common_delete_btn delete_hobby" data-id="<?= $value['uh_id'] ?>">                                           
                                                <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/delete.png">
                                            </button>
                                            <button type="button" class="common_edit_btn edit_hobby" data-id="<?= $value['uh_id'] ?>"  data-title="<?= $title_value ?>" data-desc="<?= $uh_description[$key] ?>" >
                                                <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/icon_modifier.png">
                                            </button>
                                        </div>
                                    </div>
                                    <?php
                                endif;
                            endforeach;
                        else:
                            ?>
                            <h3 class="no_hobbies_set"><?= lang('SHOW_PROFILE_PAGE_ADD_YOUR_HOBBIES'); ?></h3>
                        <?php endif; ?>
                    </div>
                    <div class="pupup-button">
                        <div class="hobby_popup_open">
                            <a href="javascript:void(0);">
                                <img src="<?= ASSETS_PATH ?>images/add_blue.png" alt="" class="img-responsive">
                            </a>
                            <span class="poup-span">
                                <?= lang('SHOW_PROFILE_PAGE_ADD_HOBBIES') ?>
                            </span>
                        </div>
                    </div>
                </div>
                <!-- /END OF MY_HOBBIES / -->

                <!-- Build CV -->
                <div class="generate_cv_div">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label class="text-success"><?= lang('SHOW_PROFILE_HELP_TEXT') ?></label>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <a href="javascript:void(0);" class="round-btn application-btn-bg btn btn-block btn-social btn-profile generate_cv_url">
                                <span class="application-btn-span-bg">
                                    <img src="<?= ASSETS_PATH ?>images/done.png" alt="" class="img-responsive">
                                </span>
                                <?= lang("SHOW_PROFILE_GENERATE_CV"); ?>
                            </a>
                        </div>
                    </div>
                </div>
                <!-- END OF Build CV -->

            </div>
        </div>
    </div>
</div>

<!--profile popup start -->

<!--    Modal pop-up for download cv-->
<div class="modal fade profile-popup" id="download-cv-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header red-bg">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-title" id="myModalLabel">
                    <span class=""><img src="<?= ASSETS_PATH ?>images/cloud_download.png" alt="" class="img-responsive download-cv-image"></span>
                    <h3><?= lang('SHOW_PROFILE_CHOOSE_DOWNLOAD_FORMAT') ?></h3>
                </div>
            </div>
            <div class="btn-line">
                <img src="<?= ASSETS_PATH ?>images/popup_border.png" alt="" class="img-responsive">
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hm-model-mob-width">
                        <?php
                        if (isset($user_data['udf_rapidjob_cv_file_name']) && !empty($user_data['udf_rapidjob_cv_file_name'])):
                            $cv_file = UPLOAD_FILE_FOLDER . '/' . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_data['udf_id'] . '/' . $user_data['udf_rapidjob_cv_file_name'];
                            if (file_exists($cv_file)):
                                ?>
                                <a target="_blank" download="<?= $cv_file; ?>" href="<?= $cv_file; ?>">
                                    <div class="candidate-list green-border download">
                                        <img src="<?= ASSETS_PATH ?>images/download.png" alt="" class="img-responsive img-circle">
                                        <h3><?= lang('SHOW_PROFILE_DOWNLOAD_AS_PDF') ?></h3>
                                    </div>
                                </a>
                                <?php
                            endif;
                        endif;
                        ?>
                        <a href="javascript:void(0);" id="download_as_jpeg">
                            <div class="candidate-list green-border download">
                                <img src="<?= ASSETS_PATH ?>images/download.png" alt="" class="img-responsive img-circle">
                                <h3><?= lang('SHOW_PROFILE_DOWNLOAD_AS_JPEG') ?></h3>
                            </div>
                        </a>
                        <a href="javascript:void(0);" id="download_as_png">
                            <div class="candidate-list green-border download">
                                <img src="<?= ASSETS_PATH ?>images/download.png" alt="" class="img-responsive img-circle">
                                <h3><?= lang('SHOW_PROFILE_DOWNLOAD_AS_PNG') ?></h3>
                            </div>
                        </a>
                    </div>                    
                </div>
            </div>
            <div class="modal-footer">
                <div class="popup-btn-box">
                    <button data-dismiss="modal"  class="round-btn grey-light btn btn-block btn-social btn-profile">
                        <span class="grey-dark">
                            <img src="<?= ASSETS_PATH ?>images/icon_annuler.png" alt="" class="img-responsive">
                        </span>
                        <?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_CANCEL_BTN') ?>
                    </button>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</div>

<!--    Modal pop-up for add experience -->
<div class="modal fade profile-popup" id="experience-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php
            echo form_open(USERS_PATH . '/add', array("id" => "user_experience_form"));
            ?>
            <input type="hidden" name="user_experience_form" value="user_experience_form">
            <input type="hidden" name="experience_id" id="exp_experience_id" value="">
            <div class="modal-header red-bg">
                <button type="button" class="close close_modal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-title" id="myModalLabel">
                    <span class=""><img src="<?= ASSETS_PATH ?>images/add_orange.png" alt="" class="img-responsive"></span>
                    <h3 class="model_heading_text"><?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES') ?></h3>
                </div>
            </div>
            <div class="btn-line">
                <img src="<?= ASSETS_PATH ?>images/popup_border.png" alt="" class="img-responsive">
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hm-model-mob-width">
                        <div class="form-group">
                            <label for="company_name"><?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_COMPANY_NAME') ?> <span class="text-danger">*</span></label>
                            <input type="text" name="company_name" id="company_name" class="form-control required" data-rule-minlength="2" data-rule-maxlength="100">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hm-model-mob-width">
                        <label for="job_type" class="select-label"><?= lang('SHOW_PROFILE_JOB_TITLE') ?> <span class="text-danger">*</span></label>
                        <div class="my-select-bx">
                            <select name="job_type" id="job_type" class="selectpicker required " <?php if (empty($work_job)) { ?> disabled="disabled" title="<?= lang('BUILDCV_PAGE_NO_JOB_TYPE_SET'); ?>"  <?php } else { ?> data-live-search="true" title="<?= lang('BUILDCV_PAGE_CHOOSE_JOB'); ?>" <?php } ?> >
                                <?php
                                if (!empty($work_job)) :
                                    foreach ($work_job as $value) :
                                        ?>
                                        <option value="<?= $value['wj_id']; ?>"><?= $value['job_name']; ?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                                <option value="other"><?= lang('COMMON_OTHER') ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 other_company_title">
                        <div class="form-group">
                            <label for="company_title"><?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_TITLE') ?> <span class="text-danger">*</span></label>
                            <input type="text" id="company_title" name="company_title" class="form-control" data-rule-minlength="2" data-rule-maxlength="85">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="company_place"><?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PLACE') ?> <span class="text-danger">*</span></label>
                            <input type="text" id="company_place" name="company_place" class="form-control required" data-rule-minlength="2" data-rule-maxlength="100">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="" class="select-label"><?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD') ?> <span class="text-danger">*</span></label>
                        <div class="select-bx">
                            <select name="experience_start_month" id="experience_start_month" class="selectpicker required experience_start_month" <?php if (empty($this->month_en)) { ?> disabled="disabled" title="<?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD_MONTH'); ?>"  <?php } else { ?> title="<?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD_MONTH'); ?>" <?php } ?> >
                                <?php
                                $month = 'month_' . $this->current_lang;
                                if (!empty($this->$month)) :
                                    foreach ($this->$month as $key => $value) :
                                        ?>
                                        <option value="<?= $key; ?>"><?= $value; ?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>
                            <div class="form-group">
                                <div class="input-group date experience_start_year">
                                    <input name="experience_start_year" id="experience_start_year" type="text" class="form-control required" placeholder="<?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD_YEAR') ?>"><span class="input-group-addon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="space-bx">
                            <span><?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD_TO') ?></span>
                        </div>
                        <div class="select-bx">
                            <select name="experience_end_month" id="experience_end_month" class="selectpicker required experience_end_month" <?php if (empty($this->month_en)) { ?> disabled="disabled" title="<?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD_MONTH'); ?>"  <?php } else { ?> title="<?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD_MONTH'); ?>" <?php } ?> >
                                <?php
                                $month = 'month_' . $this->current_lang;
                                if (!empty($this->$month)) :
                                    foreach ($this->$month as $key => $value) :
                                        ?>
                                        <option value="<?= $key; ?>"><?= $value; ?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>
                            <div class="form-group">
                                <div class="input-group date experience_end_year">
                                    <input name="experience_end_year" id="experience_end_year" type="text" class="form-control required" placeholder="<?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD_YEAR') ?>"><span class="input-group-addon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="check-box control-group">
                            <div>
                                <input id="in_current_position" class="checkbox-custom" name="in_current_position" type="checkbox">
                                <label for="in_current_position" class="checkbox-custom-label"><?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_CHECKBOX_POSITION') ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="experience_description"><?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_DESCRIPTION_LBL') ?></label>
                            <textarea name="experience_description" id="experience_description" data-rule-minlength="2" data-rule-maxlength="200"></textarea>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="popup-btn-box">
                    <button type="submit" class="round-btn red-bg btn btn-block btn-social btn-profile">
                        <span class="dark-red">
                            <img src="<?= ASSETS_PATH ?>images/register.png" alt="" class="img-responsive">
                        </span>
                        <?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_ADD_BTN') ?>
                    </button>
                    <button data-dismiss="modal" class="round-btn grey-light btn btn-block btn-social btn-profile">
                        <span class="grey-dark">
                            <img src="<?= ASSETS_PATH ?>images/icon_annuler.png" alt="" class="img-responsive">
                        </span>
                        <?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES_CANCEL_BTN') ?>
                    </button>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</div>

<!--    Modal pop-up for add formation(training)  -->
<div class="modal fade profile-popup" id="training-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php
            echo form_open(USERS_PATH . '/add', array("id" => "user_training_form"));
            ?>
            <input type="hidden" name="user_training_form" value="user_training_form">
            <input type="hidden" name="training_id" value="" id="training_id">
            <div class="modal-header popup-blue">
                <button type="button" class="close close_training_model" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-title" id="myModalLabel">
                    <span class=""><img src="<?= ASSETS_PATH ?>images/add_dark_blue.png" alt="" class="img-responsive"></span>
                    <h3><?= lang('SHOW_PROFILE_PAGE_ADD_TRAININGS_HEADING') ?></h3>
                </div>
            </div>
            <div class="btn-line">
                <img src="<?= ASSETS_PATH ?>images/popup_border.png" alt="" class="img-responsive">
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hm-model-mob-width">
                        <div class="form-group">
                            <label for="user_training"><?= lang('SHOW_PROFILE_PAGE_ADD_FORMATION_LABEL') ?> <span class="text-danger">*</span></label>
                            <input type="text" placeholder="" id="user_training" name="user_training" class="form-control required" data-rule-minlength="2" data-rule-maxlength="100">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="user_establishment"><?= lang('SHOW_PROFILE_PAGE_ADD_FORMATION_ESTABLISSMENT') ?></label>
                            <input type="text" placeholder="" id="user_establishment" name="user_establishment" class="form-control" data-rule-minlength="2" data-rule-maxlength="100">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="user_training_desc"><?= lang('COMMON_DESCRIPTION') ?></label>
                            <textarea name="user_training_desc" id="user_training_desc" data-rule-minlength="2" data-rule-maxlength="200"></textarea>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="" class="select-label"><?= lang('COMMON_PERIOD') ?> <span class="text-danger">*</span></label>
                        <div class="select-bx">
                            <select name="training_start_month" id="training_start_month" class="selectpicker required training_start_month" <?php if (empty($this->month_en)) { ?> disabled="disabled" title="<?= lang('SHOW_PROFILE_PAGE_ADD_FORMATION_PERIOD_MONTH'); ?>"  <?php } else { ?> title="<?= lang('SHOW_PROFILE_PAGE_ADD_FORMATION_PERIOD_MONTH'); ?>" <?php } ?> >
                                <?php
                                $month = 'month_' . $this->current_lang;
                                if (!empty($this->$month)) :
                                    foreach ($this->$month as $key => $value) :
                                        ?>
                                        <option value="<?= $key; ?>"><?= $value; ?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>
                            <div class="form-group">
                                <div class="input-group date training_start_year">
                                    <input name="training_start_year" id="training_start_year" type="text" class="form-control required" placeholder="<?= lang('SHOW_PROFILE_PAGE_ADD_FORMATION_PERIOD_YEAR') ?>"><span class="input-group-addon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="space-bx">
                            <span><?= lang('COMMON_TO') ?></span>
                        </div>
                        <div class="select-bx">
                            <select name="training_end_month" id="training_end_month" class="selectpicker required training_end_month" <?php if (empty($this->month_en)) { ?> disabled="disabled" title="<?= lang('SHOW_PROFILE_PAGE_ADD_FORMATION_PERIOD_MONTH'); ?>"  <?php } else { ?> title="<?= lang('SHOW_PROFILE_PAGE_ADD_FORMATION_PERIOD_MONTH'); ?>" <?php } ?> >
                                <?php
                                $month = 'month_' . $this->current_lang;
                                if (!empty($this->$month)) :
                                    foreach ($this->$month as $key => $value) :
                                        ?>
                                        <option value="<?= $key; ?>"><?= $value; ?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </select>
                            <div class="form-group">
                                <div class="input-group date training_end_year">
                                    <input name="training_end_year" id="training_end_year" type="text" class="form-control required" placeholder="<?= lang('SHOW_PROFILE_PAGE_ADD_FORMATION_PERIOD_YEAR') ?>"><span class="input-group-addon"><i class="glyphicon glyphicon-triangle-bottom"></i></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="popup-btn-box">
                    <button type="submit" class="round-btn popup-blue btn btn-block btn-social btn-profile">
                        <span class="popup-dark-blue">
                            <img src="<?= ASSETS_PATH ?>images/register.png" alt="" class="img-responsive">
                        </span>
                        <?= lang('SHOW_PROFILE_PAGE_ADD_FORMATION_ADD_BTN') ?>
                    </button>
                    <button data-dismiss="modal"  class="round-btn grey-light btn btn-block btn-social btn-profile">
                        <span class="grey-dark">
                            <img src="<?= ASSETS_PATH ?>images/icon_annuler.png" alt="" class="img-responsive">
                        </span>
                        <?= lang('SHOW_PROFILE_PAGE_ADD_FORMATION_CANCEL_BTN') ?>
                    </button>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</div>

<!--    Modal pop-up for add skills-->
<div class="modal fade profile-popup" id="skills-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php
            echo form_open(USERS_PATH . '/add', array("id" => "user_skill_form"));
            ?>
            <input type="hidden" name="user_skill_form" value="user_skill_form">
            <input type="hidden" name="skill_id" value="" id="skill_id">
            <div class="modal-header profile-popup-green">
                <button type="button" class="close close_skill_model" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-title" id="myModalLabel">
                    <span class=""><img src="<?= ASSETS_PATH ?>images/add_green.png" alt="" class="img-responsive"></span>
                    <h3><?= lang('SHOW_PROFILE_PAGE_ADD_SKILLS_HEADING') ?></h3>
                </div>
            </div>
            <div class="btn-line">
                <img src="<?= ASSETS_PATH ?>images/popup_border.png" alt="" class="img-responsive">
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hm-model-mob-width">
                        <div class="form-group">
                            <label for="user_skill"><?= lang('SHOW_PROFILE_PAGE_ADD_SKILL_AREA_LABEL') ?> <span class="text-danger">*</span></label>
                            <input type="text" placeholder="" id="user_skill" name="user_skill" class="form-control required" data-rule-minlength="2" data-rule-maxlength="100">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label for="user_skill_establishment" class="select-label"><?= lang('SHOW_PROFILE_PAGE_ADD_SKILL_ESTABLISSMENT') ?> <span class="text-danger">*</span></label>
                        <div id="add_skill_dynamic"></div>
                        <div class="pupup-button">
                            <a id="add_user_skill" href="javascript:void(0)">
                                <img class="img-responsive" alt="<?= lang('SHOW_PROFILE_PAGE_ADD_SKILL_ADD_BTN') ?>" src="<?= ASSETS_PATH ?>images/add_blue.png">
                                <span class="poup-span">
                                    <?= lang('SHOW_PROFILE_PAGE_ADD_SKILLS') ?>
                                </span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <div class="popup-btn-box">
                    <button type="submit" class="submit_skill_btn round-btn profile-popup-green btn btn-block btn-social btn-profile">
                        <span class="profile-popup-dark-green">
                            <img src="<?= ASSETS_PATH ?>images/register.png" alt="" class="img-responsive">
                        </span>
                        <?= lang('SHOW_PROFILE_PAGE_ADD_SKILL_ADD_BTN') ?>
                    </button>
                    <button data-dismiss="modal"  class="round-btn grey-light btn btn-block btn-social btn-profile">
                        <span class="grey-dark">
                            <img src="<?= ASSETS_PATH ?>images/icon_annuler.png" alt="" class="img-responsive">
                        </span>
                        <?= lang('SHOW_PROFILE_PAGE_ADD_SKILL_CANCEL_BTN') ?>
                    </button>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</div>

<!--    Modal pop-up for add hobbies-->
<div class="modal fade profile-popup" id="hobbies-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <?php
            echo form_open(USERS_PATH . '/add', array("id" => "user_hobbies_form"));
            ?>
            <input type="hidden" name="user_hobbies_form" value="user_hobbies_form">
            <input type="hidden" name="hobbies_id" value="" id="hobbies_id">
            <div class="modal-header profile-popup-green">
                <button type="button" class="close close_hobbies_model" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-title" id="myModalLabel">
                    <span class=""><img src="<?= ASSETS_PATH ?>images/add_green.png" alt="" class="img-responsive"></span>
                    <h3><?= lang('SHOW_PROFILE_PAGE_ADD_HOBBIES') ?></h3>
                </div>
            </div>
            <div class="btn-line">
                <img src="<?= ASSETS_PATH ?>images/popup_border.png" alt="" class="img-responsive">
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hm-model-mob-width">
                        <div class="form-group">
                            <label for="hobbies_title"><?= lang('COMMON_TITLE_HOBBIE') ?> <span class="text-danger">*</span></label>
                            <input type="text" placeholder="" id="hobbies_title" name="user_hobbies_title[]" class="form-control required user_hobbies_title" data-rule-minlength="2" data-rule-maxlength="100">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 mob-input-width">
                        <label for="hobbies_description" class="select-label"><?= lang('COMMON_DESCRIPTION') ?></label>
                        <div class="form-group">
                            <input type="text" placeholder="" id="hobbies_description" name="user_hobbies_description[]" class="form-control user_hobbies_description" data-rule-minlength="2" data-rule-maxlength="200">
                        </div>
                    </div>
                </div>
                <div id="add_hobbies_dynamic"></div>
                <div class="pupup-button hide">
                    <a id="add_more_hobbies" href="javascript:void(0);">
                        <img class="img-responsive" alt="<?= lang('SHOW_PROFILE_PAGE_ADD_SKILL_ADD_BTN_NEW') ?>" src="<?= ASSETS_PATH ?>images/add_blue.png">
                        <span class="poup-span">
                            <?= lang('COMMON_ADD_MORE') ?>
                        </span>
                    </a>
                </div>
            </div>
            <div class="modal-footer">
                <div class="popup-btn-box">
                    <button type="submit" class="submit_skill_btn round-btn profile-popup-green btn btn-block btn-social btn-profile">
                        <span class="profile-popup-dark-green">
                            <img src="<?= ASSETS_PATH ?>images/register.png" alt="" class="img-responsive">
                        </span>
                        <?= lang('SHOW_PROFILE_PAGE_ADD_SKILL_ADD_BTN') ?>
                    </button>
                    <button data-dismiss="modal"  class="round-btn grey-light btn btn-block btn-social btn-profile">
                        <span class="grey-dark">
                            <img src="<?= ASSETS_PATH ?>images/icon_annuler.png" alt="" class="img-responsive">
                        </span>
                        <?= lang('COMMON_CANCEL_BTN') ?>
                    </button>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
        </div>
    </div>
</div>

<!-- Download user cv as jpeg or png -->
<div id="canvas">
    <div class="movable_div" style="display: none;">
        <?= $content; ?>
    </div>
</div>
<a id="open_image" href="" download="" target="_blank" style="visibility: hidden;"></a>
<!-- END OF Download user cv as jpeg or png -->
<?php
$doc_id = isset($user_data) && isset($user_data['udf_id']) && !empty($user_data['udf_id']) && is_numeric($user_data['udf_id']) ? trim($user_data['udf_id']) : '';
?>
<script type="text/javascript">
    var user_url = "<?= HOME_PATH ?>";
    var select_valid_start_date = "<?= lang('COMMON_SELECT_VALID_START_DATE') ?>";
    var select_valid_end_date = "<?= lang('COMMON_SELECT_VALID_END_DATE') ?>";
    var select_start_month = "<?= lang('SHOW_PROFILE_SELECT_START_MONTH') ?>";
    var select_start_year = "<?= lang('SHOW_PROFILE_SELECT_START_YEAR') ?>";
    var select_end_month = "<?= lang('SHOW_PROFILE_SELECT_END_MONTH') ?>";
    var select_end_year = "<?= lang('SHOW_PROFILE_SELECT_END_YEAR') ?>";
    var doc_id = "<?= $doc_id; ?>";
    var doc_path = "<?= UPLOAD_ABS_PATH ?><?= UPLOAD_DOCUMENTS_FOLDER ?>/" + doc_id + "/";
    var generate_cv_url = "<?= USERS_PATH . '/generate_rapidjob_cv' ?>";
    var provide_cv_details = "<?= lang('SHOW_PROFILE_GENERATE_CV_ERROR'); ?>";
    var notice_message = "<?= lang('COMMON_NOTICE'); ?>";
    var delete_message_text = "<?= lang('DELETE_MESSAGE_TEXT'); ?>";
    var delete_experience_text = "<?= lang('DELETE_ARE_YOU_SURE_EXPERIENCE'); ?>";
    var delete_training_text = "<?= lang('DELETE_ARE_YOU_SURE_TRAINING'); ?>";
    var delete_skill_text = "<?= lang('DELETE_ARE_YOU_SURE_SKILL'); ?>";
    var delete_hobby_text = "<?= lang('DELETE_ARE_YOU_SURE_HOBBY'); ?>";
    var delete_yes_text = "<?= lang('DELETE_YES_DELETE_IT'); ?>";
    var delete_no_text = "<?= lang('DELETE_NO_CANCEL_IT'); ?>";
    var hobbies_title = "<?= lang('COMMON_TITLE') ?>";
    var hobbies_desc = "<?= lang('COMMON_DESCRIPTION') ?>";
    var add_your_experience = "<?= lang('SHOW_PROFILE_PAGE_ADD_YOUR_EXP'); ?>";
    var add_your_training = "<?= lang('SHOW_PROFILE_PAGE_ADD_YOUR_TRAINING'); ?>";
    var add_your_skill = "<?= lang('SHOW_PROFILE_PAGE_ADD_YOUR_SKILL'); ?>";
    var add_your_hobby = "<?= lang('SHOW_PROFILE_PAGE_ADD_YOUR_HOBBIES'); ?>";
    var add_experience_heading = "<?= lang('SHOW_PROFILE_PAGE_ADD_EXPERIANCES'); ?>";
    var add_training_heading = "<?= lang('SHOW_PROFILE_PAGE_ADD_YOUR_TRAINING'); ?>";
    var add_skill_heading = "<?= lang('SHOW_PROFILE_PAGE_ADD_YOUR_SKILL'); ?>";
    var add_hobby_heading = "<?= lang('SHOW_PROFILE_PAGE_ADD_YOUR_HOBBIES'); ?>";
    var edit_experience_heading = "<?= lang('UPDATE_EXPERIENCE_TITLE'); ?>";
    var edit_training_heading = "<?= lang('UPDATE_TRAINING_TITLE'); ?>";
    var edit_skill_heading = "<?= lang('UPDATE_SKILL_TITLE'); ?>";
    var edit_hobby_heading = "<?= lang('UPDATE_HOBBY_TITLE'); ?>";
    var user_activity_area = "<?= isset($user_data) && isset($user_data['ud_activity_area']) && !empty($user_data['ud_activity_area']) ? trim($user_data['ud_activity_area']) : ''; ?>";
    var add_exp_url = "<?= USERS_PATH . '/add' ?>";
    var edit_exp_url = "<?= USERS_PATH . '/edit' ?>";
    var add_btn_text = '<?= lang('COMMON_ADD_BTN'); ?>';
    var edit_btn_text = '<?= lang('COMMON_EDIT'); ?>';
</script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/pages/show_profile.js"></script>
