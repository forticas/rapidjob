<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="build-cv top-section send-application">
    <div class="container">
        <div class="title-box">
            <label>E</label>
            <h3 class="green-light-font"><?= lang("SEND_APPLICATION_SEND_YOUR") ?> <span class="red-font"><?= lang("COMMON_APPLICATION_TITLE") ?></span> <?= lang("SEND_APPLICATION_APP") ?>
            </h3>
        </div>
        <?php
        echo form_open_multipart(USERS_PATH . '/send_application', array("id" => "send_application_form"));
        ?>
        <input type="hidden" name="send_application_form" value="true">
        <div class="main-msg-box">
            <label class="msg-label"><?= lang("SEND_APPLICATION_SENT_TO_COMPANY") ?></label>
            <textarea class="form-control required user_message" name="user_message" id="user_message" data-rule-minlength="5" data-rule-maxlength="500"><?php echo (!empty($user_docs['udf_message'])) ? stripslashes(str_replace("\\r\\n", "\\\n", $user_docs['udf_message'])) : lang("SEND_APPLICATION_MESSAGE_PLACEHOLDER"); ?></textarea>
        </div>
        <div class="row uploaded-file" id="uploaded-file-div">
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                <p class="set_right_padding"><?= lang("SEND_APPLICATION_JOIN_MY_CV") ?></p>
            </div>
            <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
                <div class="file-box">
                    <div class="form-group">
                        <label class="btn btn-default btn-file upload-file-btn my_cv_label" for="my_cv">
                            <span class="my_cv_span"><?= !empty($user_docs['udf_cv_file_name']) ? $user_docs['udf_cv_file_name'] : lang("COMMON_CHOOSE_FILE") ?></span><input type="file" name="my_cv" id="my_cv" style="visibility: hidden;">
                            <input type="hidden" style="visibility: hidden;" class="my_hidden_cv" value="<?= !empty($user_docs['udf_cv_file_name']) ? $user_docs['udf_cv_file_name'] : '' ?>">
                            <input type="hidden" style="visibility: hidden;" class="my_hidden_rapidjob_cv" value="<?= !empty($user_docs['udf_rapidjob_cv_file_name']) ? $user_docs['udf_rapidjob_cv_file_name'] : '' ?>">
                        </label>
                        <div class="show_error_msg"></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="check-box control-group">
                    <div>
                        <input type="checkbox" name="attach_my_cv" class="checkbox-custom" id="attach_my_cv" value="2">
                        <label class="checkbox-custom-label" for="attach_my_cv"><?= lang("SEND_APPLICATION_USE_YOUR_CV") ?></label>
                    </div>
                </div>
            </div>
            
        </div>
        <div class="row uploaded-file">
            <div class="col-lg-2 col-md-3 col-sm-4 col-xs-12">
                <p class="set_right_padding"><?= lang("SEND_APPLICATION_JOIN_MY_COVER_LATTER") ?></p>
            </div>
            <div class="col-lg-4 col-md-5 col-sm-6 col-xs-12">
                <div class="file-box">
                    <div class="form-group">
                        <label class="btn btn-default btn-file upload-file-btn" for="my_cover_latter">
                            <span class="my_cover_latter_span"><?php echo!empty($user_docs['udf_cover_latter_file']) ? $user_docs['udf_cover_latter_file'] : lang("COMMON_CHOOSE_FILE") ?></span>
                            <input type="file" name="my_cover_latter" id="my_cover_latter" class="<?= empty($user_docs['udf_cover_latter_file']) ? 'required' : ''; ?>" style="visibility: hidden;">
                        </label>
                    </div>
                </div>
            </div>
        </div>

        <div class="applicatiopn-comment">
            <label class="msg-label"><?= lang("SEND_APPLICATION_COMPANY_TO_EXCLUDED") ?></label>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-8 col-xs-12">
                    <textarea class="form-control comment-box required" name="user_comment" id="user_comment" placeholder="<?= lang("SEND_APPLICATION_EMAIL_PLACEHOLDER"); ?>" data-rule-maxlength="1000"><?php echo (!empty($user_docs['udf_blocked_email'])) ? stripslashes(str_replace("\\r\\n", "\\\n", $user_docs['udf_blocked_email'])) : ''; ?></textarea>
                    <div for="user_comment" class="invalid_emails"></div>
                </div>
            </div>
        </div>
        <div class="application-btn-box">
            <div class="check-box control-group">
                <div>
                    <input type="checkbox" checked="" name="accept_term" class="checkbox-custom accept_term required" id="accept_term" value="1">
                    <label class="checkbox-custom-label" for="accept_term"><?= lang("SEND_APPLICATION_ACCEPT_CGU") ?></label>
                </div>
            </div>
            <div class="applicantion-btn">
                <button type="submit" class="round-btn application-btn-bg btn btn-block btn-social btn-profile">
                    <span class="application-btn-span-bg">
                        <img src="<?= ASSETS_PATH ?>images/eye.png" alt="" class="img-responsive">
                    </span><?= lang("SEND_APPLICATION_VALIDATE_AND_SEE") ?>
                </button>
            </div>
        </div>
        <?php
        echo form_close();
        ?>
    </div>
</div>
<script type="text/javascript">
    var choose_file = "<?= lang("COMMON_CHOOSE_FILE") ?>";
    var size_limit = '<?= DEFAULT_MAXIMUM_CV_SIZE_LIMIT_JS ?>';
    var invalid_file = '<?= lang("ERROR_CV_INVALID_SIZE"); ?>';
    var invalid_file_extension = '<?= lang("ERROR_CV_INVALID_EXTENSION"); ?>';
    var invalid_cover_extension = "<?= lang("ERROR_COVER_INVALID_EXTENSION"); ?>";
    var required_field_title = "<?= lang("COMMON_THIS_REQUIRED_FIELD"); ?>";
    var required_field_message = "<?= lang("SEND_APPLICATION_SELECT_CV"); ?>";
    var invalid_email_message = "<?= lang("SEND_APPLICATION_EMAIL_INVALID"); ?>";
    var provide_cv_details = "<?= lang('SHOW_PROFILE_GENERATE_CV_ERROR'); ?>";
    var generate_your_cv_message = "<?= lang('COMMON_GENERATE_CV_MESSAGE'); ?>";
    var notice_message = "<?= lang('COMMON_NOTICE'); ?>";
    var doc_id = "<?= !empty($user_docs['udf_id']) && is_numeric($user_docs['udf_id']) ? $user_docs['udf_id'] : ''; ?>";
    var user_disable_cv_btn = "<?= !empty($user_docs['udf_type']) && $user_docs['udf_type'] == 2 ? 2 : 1; ?>";
    var cv_file_name = "<?= !empty($user_docs['udf_cv_file_name']) ? $user_docs['udf_cv_file_name'] : '' ?>";
    var rapidjob_cv_file_name = "<?= !empty($user_docs['udf_rapidjob_cv_file_name']) ? $user_docs['udf_rapidjob_cv_file_name'] : '' ?>";
    var user_activity_area = "<?= isset($user_data) && isset($user_data['ud_activity_area']) && !empty($user_data['ud_activity_area']) ? trim($user_data['ud_activity_area']) : ''; ?>";
    var user_exp_data = "<?= $user_exp_data; ?>";
    var user_skill_data = "<?= $user_skill_data; ?>";
    var user_training_data = "<?= $user_training_data; ?>";
</script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/pages/send_application.js"></script>
