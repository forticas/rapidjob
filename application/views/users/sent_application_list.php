<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="build-cv top-section application-table">
    <div class="container">
        <div class="title-box">
            <label class="small-text">c</label>
            <h3 class="green-light-font">
                <?= lang("SEND_APPLICATION_LIST_MY") ?> <span class="red-font"><?= lang("SEND_APPLICATION_LIST_APPLICATION") ?> <?= lang("SEND_APPLICATION_LIST_SPONTANEOUS") ?></span>
            </h3>
        </div>
        <?php if (!empty($user_data)) : ?>
            <div class="table-content table-responsive">
                <table class="table table-inverse">
                    <thead>
                        <tr>
                            <th><?= lang("SEND_APPLICATION_LIST_CANDIDATURE") ?></th>
                            <th><?= lang("SEND_APPLICATION_LIST_NO_OF_COMPANY") ?></th>
                            <th><?= lang("SEND_APPLICATION_LIST_STATE") ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($user_data as $value):
                            ?>
                            <tr>
                                <td class="convert_time"><?= $value['ua_created_date'] ?></td>
                                <td><?= $value['ua_sent_count'] . " " . lang("COMMON_BUSINESS_NAME") ?> </td>
                                <td>
                                    <?php
                                    if ($value['ua_status'] == 1):
                                        echo lang("COMMON_SENT");
                                    elseif ($value['ua_status'] == 2):
                                        echo lang("COMMON_PENDING");
                                    else:
                                        echo " - - - ";
                                    endif;
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php else: ?>
            <h3 class="no_app_found"><?= lang('SEND_APPLICATION_LIST_APP_NOT_FOUND'); ?></h3>
        <?php endif; ?>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".convert_time").each(function () {
            var timestemp = parseInt(jQuery(this).text());
            var date = formatDateLocal("<?= DATE_FORMAT_JS_LIST ?>", timestemp * 1000, false, current_lang);
            jQuery(this).text(date);
        });
    });
</script>