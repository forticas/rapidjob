<div id="generate_user_cv">
    <div>
        <style type="text/css">
            .light-white-bg .container {
                background-color: rgb(255, 255, 255);
            }
            #experience-section {
                padding: 0;
                padding: 20px 0 5px;
            }
            .candidate-profile .profile-infor-ttl {
                padding: 20px 0 5px;
            }
            .profile-user-info{
                color: rgb(250, 106, 99);
                font-size: 18px;
                font-weight: 500;
                margin: 10px 0 5px;
                padding: 0;
            }
            .profile-infor-ttl {
                color: rgb(250, 106, 99);
                font-size: 25px;
                font-weight: 500;
                margin: 10px 0 5px;
                padding: 0;
            }
            label {
                font-weight: normal !important;
                display: inline-block;
            }
            .profile-info-bx {
                margin: 20px;
                padding: 10px;
                font-size: 18px;
                font-weight: normal;
                color: rgb(76, 76, 76);
            }
            .profile-info-bx p {
                color: rgb(76, 76, 76);
                font-size: 18px;
                font-weight: normal;
                margin: 0;
                padding: 0;
            }
            .extra-pad-head{
                padding: 0 0 30px;
            }
            .extra-pad-text{
                padding: 0 0 10px;
            }
            .extra-pad {
                padding: 0 0 10px;
            }
            #cources-section {
                padding: 0;
            }

            #skills-section {
                padding: 0;
            }
            #skills-section .profile-info-bx .text-box {
                padding: 0;
                position: relative;
            }
            .profile-info-bx .text-box span {
                color: rgb(76, 76, 76);
                font-size: 18px;
                margin: 0 !important;
                padding: 0;
            }
            .set_hobbies_text{
                color: rgb(76, 76, 76);
                font-size: 18px;
                margin: 0 !important;
                padding: 0;
            }
            .set_td_font{
                color: rgb(76, 76, 76);
                font-size: 16px;
                margin: 0 !important;
                padding: 0;
            }
            #hobbies-section {
                padding: 0;
            }
            .text-bx-new p.set_your_job_type{
                color: rgb(76, 76, 76);
                margin: 10px 0 0 0;
                padding: 10px 0 0 20px;
                text-align: left;
            }
            .text-bx-new span.set_your_job_type{
                color: rgb(76, 76, 76);
                margin: 10px 0 0 0;
                padding: 10px 0 0 0;
                text-align: left;
            }
            .text-bx-new label.set_your_job_type{
                color: rgb(76, 76, 76);
                margin: 10px 0 0 0;
                padding: 10px 0 0 0;
                text-align: left;
            }
            .text-bx-new div.set_your_job_type{
                color: rgb(76, 76, 76);
                margin: 10px 0 0 0;
                padding: 10px 0 0 0;
                text-align: left;
            }
        </style>
        <div class="cv-box top-section candidate-profile">
            <div class="container extrap-pad">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="cv-profile-bx">
                            <div class="inner-profile">
                                <?php
                                $user_image = UPLOAD_FILE_FOLDER . '/' . UPLOAD_USERS_FOLDER . '/' . $user_data['u_id'] . '/' . $user_data['u_image'];
                                if (file_exists($user_image)) :
                                    $user_image = BASE_URL . UPLOAD_FILE_FOLDER . '/' . UPLOAD_USERS_FOLDER . '/' . $user_data['u_id'] . '/' . $user_data['u_image'];
                                    ?>
                                    <div class="img-box">
                                        <img alt="profile photo" class="img-responsive img-circle" src="<?= $user_image; ?>" style="height: 200px;">
                                    </div>
                                <?php endif; ?>
                                <div class="text-bx-new">
                                    <p class="set_your_job_type">Nom : <?= ucwords($user_data['u_first_name'] . " " . $user_data['u_last_name']); ?></p>
                                    <p class="set_your_job_type">Email : <?= $user_data['u_email'] ?></p>
                                    <?php if (!empty($user_data['wj_name_' . $current_lang])) : ?>
                                        <p class="set_your_job_type">Désignation actuelle : <?= trim($user_data['wj_name_' . $current_lang]); ?></p>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                        <?php if (!empty($user_data['ud_user_bio'])) : ?>                            
                            <label class="profile-infor-ttl">À propos de moi:
                                <div class="profile-info-bx border-radius">
                                    <p class="extra-pad"><?php echo (!empty($user_data['ud_user_bio'])) ? trim(stripslashes(str_replace('\r\n', "<br>", $user_data['ud_user_bio']))) : ''; ?></p>
                                </div>
                            </label>
                        <?php endif; ?>
                        <?php if (!empty($user_exp_data)) : ?>
                            <div id="experience-section" class="container-fluid">
                                <label class="profile-infor-ttl">Mes expériences</label>
                                <div class="clearfix"></div>
                                <?php
                                foreach ($user_exp_data as $value) :
                                    ?>
                                    <div class="profile-info-bx border-radius">
                                        <p class="extra-pad">
                                            <?php
                                            echo $value['ue_title'] . " - " . $value['ue_company'] . " (" . $value['ue_place'] . ")";
                                            ?>
                                        </p>
                                        <p class="extra-pad">
                                            <?php
                                            $end_exp = !empty($value['ue_end_date']) ? date("F Y", $value['ue_end_date']) : "Toujours en train de travailler ici";
                                            echo "Durée (" . date('F Y', $value['ue_start_date']) . " À " . $end_exp . ")";
                                            ?>
                                        </p>
                                        <?php echo (!empty($value['ue_description'])) ? trim(stripslashes(str_replace('\r\n', "<br>", $value['ue_description']))) : ''; ?>
                                    </div>
                                    <?php
                                endforeach;
                                ?>
                            </div>
                            <?php
                        endif;
                        if (!empty($user_training_data)) :
                            ?>
                            <div id="cources-section" class="container-fluid">
                                <label class="profile-infor-ttl">Mes cours</label>
                                <div class="clearfix"></div>
                                <?php
                                foreach ($user_training_data as $value) :
                                    ?>
                                    <div class="profile-info-bx border-radius">
                                        <p class="extra-pad"><?= $value['ut_name'] . " " . $value['ut_establissment']; ?></p>
                                        <p class="extra-pad"><?= "Durée (" . date('F Y', $value['ut_start_date']) . " À " . date("F Y", $value['ut_end_date']) . ")"; ?></p>
                                        <?php echo (!empty($value['ut_description'])) ? trim(html_entity_decode(str_replace('\r\n', "<br>", $value['ut_description']))) : ''; ?>
                                    </div>
                                <?php endforeach; ?>
                            </div>
                            <?php
                        endif;
                        if (!empty($user_skill_data)) :
                            ?>
                            <div id="skills-section"  class="container-fluid">
                                <label class="profile-infor-ttl">Mes compétences</label>
                                <div class="clearfix"></div>
                                <?php
                                foreach ($user_skill_data as $value) :
                                    ?>
                                    <div class="profile-info-bx border-radius" style="padding: 5px 0 0 15px;margin: 5px 0 0 0;">
                                        <?php if (!empty($value['us_name'])): ?>
                                            <p class="extra-pad" style="padding: 10px 0 0 15px;margin: 10px 0 0 0;"><?= $value['us_name']; ?></p>
                                            <?php
                                        endif;
                                        if (!empty($value['us_skills'])) :
                                            $us_skill = json_decode($value['us_skills'], true);
                                            ?>
                                            <div class="text-box">
                                                <table style="width: 25%;">
                                                    <?php
                                                    foreach ($us_skill as $key => $es_value) :
                                                        ?>
                                                        <tr style="width: 50%;">
                                                            <td style="width: 20%;">
                                                                <p class="extra-pad" style="padding: 5px 0 0 20px;margin: 0 auto;">Key<?= $key ?></p>
                                                            </td>
                                                            <td style="width: 50%;">
                                                                <?php
                                                                if ($es_value == 5) {
                                                                    for ($index = 0; $index < 5; $index++) {
                                                                        ?>
                                                                        <span><img src="<?= DEFAULT_FULL_STAR; ?>" style="height: 25px;" /></span>
                                                                        <?php
                                                                    }
                                                                } else {
                                                                    $fullStars = $es_value % 5;
                                                                    $halfStars = $es_value - $fullStars;
                                                                    for ($index = 0; $index < $fullStars; $index++) {
                                                                        ?>
                                                                        <span><img src="<?= DEFAULT_FULL_STAR; ?>" style="height: 25px;" /></span>
                                                                        <?php
                                                                    }
                                                                    if ($halfStars > 0) {
                                                                        ?>
                                                                        <span><img src="<?= DEFAULT_HALF_STAR; ?>" style="height: 25px;"/></span>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    <?php endforeach; ?>
                                                </table>
                                            </div>
                                            <?php
                                        endif;
                                        ?>
                                    </div>
                                    <?php
                                endforeach;
                                ?>
                            </div>
                            <?php
                        endif;

                        if (!empty($user_hobbies_data)) :
                            ?>
                            <div id="hobbies-section" class="container-fluid">
                                <label class="profile-infor-ttl">Mes loisirs</label>
                                <div class="clearfix"></div>
                                <div class="profile-info-bx border-radius">
                                    <?php
                                    foreach ($user_hobbies_data as $value) :
                                        if (!empty($value['uh_title'])) :
                                            $uh_title = json_decode($value['uh_title'], true);
                                            $uh_description = json_decode($value['uh_description'], true);
                                            foreach ($uh_title as $key => $title_value) :
                                                if (!empty(trim($title_value))):
                                                    ?>
                                                    <p style="padding: 20px 0 0 0px;margin: 0 auto;"><?= trim($title_value); ?> :</p>
                                                    <p style="padding: 5px 0 0 0px;margin: 0 auto;"><?= !empty($uh_description[$key]) ? trim($uh_description[$key]) : ''; ?></p>
                                                    <?php
                                                endif;
                                            endforeach;
                                        endif;
                                    endforeach;
                                    ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>