<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="build-cv top-section edit-profile">
    <div class="container ">
        <div class="title-box">
            <label>I</label>
            <h3 class="green-light-font">
                <?= lang('EDITPROFILE_PAGE_TITLE') ?> <span class="red-font"><?= lang('EDITPROFILE_PAGE_TITLE_RED') ?></span>
            </h3>
        </div>
        <div class="edit-profile-bx">
            <?php
            echo form_open_multipart(USERS_PATH . '/add', array("id" => "edit_profile_form"));
            ?>
            <input type="hidden" name="edit_profile_form" value="edit_profile_form">
            <div class="bg-img hidden-xs visible-lg visible-md visible-sm">
                <img src="<?= ASSETS_PATH ?>images/big_user.png" alt="" class="img-responsive">
            </div>
            <label class="red-label red-font">
                <?= lang('EDITPROFILE_PAGE_EMAIL_INFO') ?>
            </label>

            <div class="inpot-bx">
                <label class="select-label"><?= lang('EDITPROFILE_PAGE_EMAIL_LABEL') ?><span class="text-danger">*</span></label>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-8 col-xs-12">
                        <div class="form-group input-margin">
                            <input type="text" placeholder="" name="email" id="email" value="<?= $user_data['u_email']; ?>" class="form-control" data-rule-maxlength="255">
                        </div>
                    </div>
                </div>
            </div>
            <label class="red-label red-font">
                <?= lang('EDITPROFILE_PAGE_CIVIL_INFO') ?>
            </label>
            <div class="inpot-bx">
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label class="select-label"><?= lang('EDITPROFILE_PAGE_CIVIL_CIVILITY') ?><span class="text-danger">*</span></label>
                        <div class="select-bx">
                            <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                            <select name="civility" id="civility" class="selectpicker" title="<?= lang('EDITPROFILE_PAGE_CIVILITY'); ?>">
                                <option value="2" <?php if ($user_data['ud_gender'] == 2) echo 'selected="selected"'; ?>><?= lang('EDITPROFILE_PAGE_CIVIL_CIVILITY_MADAM') ?></option>
                                <option value="1" <?php if ($user_data['ud_gender'] == 1) echo 'selected="selected"'; ?>><?= lang('EDITPROFILE_PAGE_CIVIL_CIVILITY_SIR') ?></option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <label for="first_name" class="select-label"><?= lang('EDITPROFILE_PAGE_CIVIL_CIVILITY_FIRSTNAME') ?><span class="text-danger">*</span></label>
                        <div class="form-group">
                            <input type="text" placeholder="" id="first_name" class="form-control" name="first_name" value="<?= $user_data['u_first_name']; ?>" data-rule-minlength="2" data-rule-maxlength="25">
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                        <label for="last_name" class="select-label"><?= lang('EDITPROFILE_PAGE_CIVIL_CIVILITY_LASTNAME') ?><span class="text-danger">*</span></label>
                        <div class="form-group">
                            <input type="text" placeholder="" id="last_name" class="form-control" name="last_name" value="<?= $user_data['u_last_name']; ?>" data-rule-minlength="2" data-rule-maxlength="25">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label for="nationality" class="select-label"><?= lang('EDITPROFILE_PAGE_CIVIL_CIVILITY_NATIONALITY') ?><span class="text-danger">*</span></label>
                        <div class="select-bx">
                            <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                            <select name="nationality" id="nationality" class="selectpicker required " <?php if (empty($nationalities)) { ?> disabled="disabled" title="<?= lang('COMMON_NATIONALITY_EMPTY'); ?>"  <?php } else { ?> data-live-search="true" title="<?= lang('COMMON_SELECT_NATIONALITY'); ?>" <?php } ?> >
                                <?php
                                if (!empty($nationalities)) :
                                    foreach ($nationalities as $value) :
                                        if ($value['n_id'] == $user_data['ud_nationality']) :
                                            ?>
                                            <option selected="selected" value="<?= $value['n_id']; ?>"><?= $value['n_code'] . " (" . strtolower($value['n_name']) . ")" ?></option>
                                        <?php else: ?>
                                            <option value="<?= $value['n_id']; ?>"><?= $value['n_code'] . " (" . strtolower($value['n_name']) . ")" ?></option>
                                        <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <label class="red-label red-font">
                <?= lang('EDITPROFILE_PAGE_ADDRESS_INFO') ?>
            </label>
            <div class="inpot-bx">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
                        <label for="address" class="select-label"><?= lang('EDITPROFILE_PAGE_ADDRESS_INFO_ADDRESS') ?><span class="text-danger">*</span></label>
                        <div class="form-group" >
                            <input type="text" placeholder="" id="address" name="address" class="form-control" value="<?= $user_data['ud_address']; ?>" data-rule-minlength="2" data-rule-maxlength="255">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
                        <label for="city" class="select-label"><?= lang('EDITPROFILE_PAGE_ADDRESS_INFO_CITY') ?><span class="text-danger">*</span></label>
                        <div class="form-group">
                            <input type="text" placeholder="" id="city" name="city" class="form-control" value="<?= $user_data['ud_city']; ?>" data-rule-minlength="2" data-rule-maxlength="100">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                        <label for="postal_code" class="select-label"><?= lang('EDITPROFILE_PAGE_ADDRESS_INFO_POSTAL_CODE') ?><span class="text-danger">*</span></label>
                        <div class="form-group">
                            <input type="text" placeholder="" id="postal_code" name="postal_code" class="form-control" value="<?= $user_data['ud_postal_code']; ?>" data-rule-minlength="4" data-rule-maxlength="15">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <label for="country" class="select-label"><?= lang('EDITPROFILE_PAGE_ADDRESS_INFO_COUNTRY') ?><span class="text-danger">*</span></label>
                        <div class="select-bx">
                            <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                            <select name="country" id="country" class="selectpicker required " <?php if (empty($nationalities)) { ?> disabled="disabled" title="<?= lang('COMMON_COUNTRY_EMPTY'); ?>"  <?php } else { ?> data-live-search="true" title="<?= lang('COMMON_SELECT_COUNTRY'); ?>" <?php } ?> >
                                <?php
                                if (!empty($nationalities)) :
                                    foreach ($nationalities as $value) :
                                        if ($value['n_id'] == $user_data['ud_country']) :
                                            ?>
                                            <option selected="selected" value="<?= $value['n_id']; ?>"><?= $value['n_name']; ?></option>
                                        <?php else: ?>
                                            <option value="<?= $value['n_id']; ?>"><?= $value['n_name']; ?></option>
                                        <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inpot-bx">
                <div class="edit-profile-btn set_btn_margin_padding">
                    <button type="submit" class="round-btn profile-popup-green btn btn-block btn-social btn-profile">
                        <span class="profile-popup-dark-green">
                            <img src="<?= ASSETS_PATH ?>images/register.png" alt="" class="img-responsive">
                        </span>
                        <?= lang('EDITPROFILE_PAGE_SAVE_BTN') ?>
                    </button>
                </div>
            </div>
            <?php
            echo form_close();
            ?>
        </div>
        <div class="border-bx">
            <div class="border-line"></div>
        </div>
        <div class="edit-profile-bx">
            <div class="bg-img hidden-xs visible-lg visible-md visible-sm">
                <img src="<?= ASSETS_PATH ?>images/sheild_icon.png" alt="" class="img-responsive">
            </div>
            <label class="red-label red-font">
                <?= lang('EDITPROFILE_PAGE_CHANGE_PASSWORD') ?>
            </label>
            <?php echo form_open_multipart(USERS_PATH . '/change_password', 'id="change_password_form"'); ?>
            <input type="password" style="display: none;">
            <div class="inpot-bx">
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <label for="old_password" class="select-label"><?= lang('EDITPROFILE_PAGE_OLD_PASSWORD') ?><span class="text-danger">*</span></label>
                        <div class="form-group">
                            <input type="password" placeholder="" id="old_password" name="old_password" class="form-control required" data-rule-pwcheck="true" data-msg-pwcheck="<?= lang('REGISTER_PASSWORD_VALIDATION_MESSAGE'); ?>" data-rule-minlength="6" data-rule-maxlength="25">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <label for="password" class="select-label"><?= lang('EDITPROFILE_PAGE_NEW_PASSWORD') ?><span class="text-danger">*</span></label>
                        <div class="form-group">
                            <input type="password" placeholder="" id="password" name="password" class="form-control required" data-rule-pwcheck="true" data-msg-pwcheck="<?= lang('REGISTER_PASSWORD_VALIDATION_MESSAGE'); ?>" data-rule-minlength="6" data-rule-maxlength="25">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <label for="cpassword" class="select-label"><?= lang('EDITPROFILE_PAGE_CONFIRM_PASSWORD') ?><span class="text-danger">*</span></label>
                        <div class="form-group">
                            <input type="password" placeholder="" id="cpassword" name="cpassword" class="form-control required" data-rule-minlength="6" data-rule-maxlength="25">
                        </div>
                    </div>
                </div>
            </div>
            <div class="inpot-bx">
                <div class="edit-profile-btn set_btn_margin_padding">
                    <button class="round-btn profile-popup-green btn btn-block btn-social btn-profile">
                        <span class="profile-popup-dark-green">
                            <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/register.png">
                        </span>
                        <?= lang('EDITPROFILE_PAGE_CHANGE_PASSWORD_BTN') ?>
                    </button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/pages/edit_profile.js"></script>