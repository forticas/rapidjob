<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>


<div class="container">
    <br />
    <br />
    <br />
    <br />
    <br />

    <div class="panel-group faq" id="accordion">
        <div class="faqHeader head">General Questions</div>
        
        <?php 
        if(!empty($faq_data)){
        foreach($faq_data as $key => $row_faq){ ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title title">
                    <a class="accordion-toggle " data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $row_faq['cms_faq_id']; ?>">
                        <?= get_formatted_text($row_faq['faq_question']); ?>
                    </a>
                </h4>
            </div>
            <div id="collapse<?= $row_faq['cms_faq_id']; ?>" class="panel-collapse collapse <?php echo ($key == 0? 'in' : ''); ?>">
                <div class="panel-body">
                    <p><?= get_formatted_text($row_faq['faq_description']); ?></p>
                </div>
            </div>
        </div>
        <?php }
        
        }else{ ?>
            <div class="faqHeader" style="text-align: center"></div>        
        <?php } ?>
    </div>
</div>

<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/sha1.min.js"></script>
<script type="text/javascript">
    jQuery.sha1 = sha1;</script>
<script type="text/javascript">

    var confirmation_email = "<?= lang('CONFIRMATION_EMAIL') ?>";
    var letter_only = "<?= lang('LETTER_ONLY') ?>";
    

    jQuery(document).ready(function () {
        var invitation_code = "<?= $this->session->userdata('invitation_code') ?>";
        if (invitation_code != "") {
            jQuery("#registration-popup").modal('show');
            jQuery("#invitation_code").val(invitation_code);
        }
    });
    
    window.fbAsyncInit = function () {
        FB.init({
            appId: '<?= FB_APP_ID ?>',
            xfbml: true,
            version: 'v2.8'
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script type="text/javascript" src="//platform.linkedin.com/in.js">
    api_key: 78ev6kvpru6mm2
            onLoad: onLinkedInLoad
</script>

<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/pages/home.js"></script>

