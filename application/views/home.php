<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<link rel="stylesheet" href="<?= ASSETS_PATH ?>js/plugins/OwlCarousel/dist/assets/owl.carousel.css"/>
<link rel="stylesheet" href="<?= ASSETS_PATH ?>js/plugins/OwlCarousel/dist/assets/owl.theme.default.min.css"/>
<script src="<?= ASSETS_PATH ?>js/plugins/OwlCarousel/dist/owl.carousel.js"></script>
<div class="hm-banner top-section" style='background-image: url("<?= $banner_image; ?>");'>
    <div class="container">
        <div class="banner-title">
            <span class="left-sm-line red-bg"></span><h1><?= lang('HOME_COVERPAGE_TXT_1') ?> <span class="red-font"><?= lang('HOME_COVERPAGE_TXT_2') ?></span> <?= lang('HOME_COVERPAGE_TXT_3') ?> <span class="green-light-font"><?= lang('HOME_COVERPAGE_TXT_4') ?></span></h1>
        </div>
        <div class="hm-banner-search-bx">
            <div id="custom-search-input">
                <?php echo form_open(HOME_PATH . '/search', array("id" => "search_company_form", 'method' => 'POST')); ?>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 right-padding">
                    <input id="autocomplete" type="text" name="q" class="form-control input-lg hm-search-input" placeholder="<?= lang('HOME_COVERPAGE_SEARCH_PLACEHOLDER') ?>" value="<?= $search_query; ?>"/>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 home-search-box">
                    <div class="select-bx">
                        <select name="activity_area[]" id="activity_area" class="selectpicker" multiple="multiple" multiple <?php if (empty($activity_area)) { ?> disabled="disabled" title="<?= lang('COMMON_SEARCH_NO_ACTIVITY_AREA_SET'); ?>"  <?php } else { ?> data-selected-text-format="count > 1" data-live-search="true" multiple title="<?= lang('COMMON_SEARCH_CHOOSE_ACTIVITY_AREA'); ?>" <?php } ?> >
                            <?php
                            if (!empty($activity_area)) :
                                if (!empty($user_data) && !empty($user_data['ud_activity_area'])):
                                    foreach ($activity_area as $value) :
                                        if (in_array($value['wa_id'], explode(',', $user_data['ud_activity_area']))) :
                                            ?>
                                            <option selected="selected" value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                        <?php else: ?>
                                            <option value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                        <?php
                                        endif;
                                    endforeach;
                                else:
                                    foreach ($activity_area as $value) :
                                        ?>
                                        <option value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            <?php endif; ?>
                        </select>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 no-padding btn-box gradiant-btn-bx">
                    <a>
                        <button class="btn btn-info btn-lg hm-banner-search-btn btn-gradiant"><?= lang('HOME_COVERPAGE_SEARCH_BUTTON') ?></button>
                    </a>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>
<div class="hm-scroll-down-bx">
    <div class="container">
        <div class="mause-img-bx thumbnail">
            <div class="center-bdr red-bg"></div>
            <a href="javascript:void(0)" id="mouse_icon" class="down-scroll-btn"><img src="<?= ASSETS_PATH ?>images/mouse_icon.png" alt="" class="img-responsive"></a>
        </div>
    </div>
</div>
<div class="hm-about-text" id="next-page">
    <div class="container">
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
            <div class="text-bx">
                <div class="img-bx">
                    <div class="left-box">01</div>
                    <div class="right-box">
                        <img src="<?= ASSETS_PATH ?>images/ciblez_icon.png" alt="" class="img-responsive">
                    </div>
                </div>
                <div class="info-text">
                    <h5>
                        <?= $slider_details['featured_one_title']; ?>
                    </h5>
                    <p>
                        <?= $slider_details['featured_one_description']; ?>
                    </p>
                </div>

            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
            <div class="text-bx">
                <div class="img-bx">
                    <div class="left-box">02</div>
                    <div class="right-box">
                        <img src="<?= ASSETS_PATH ?>images/envoyez_icon.png" alt="" class="img-responsive">
                    </div>
                </div>
                <div class="info-text">
                    <h5>
                        <?= $slider_details['featured_second_title']; ?>
                    </h5>
                    <p>
                        <?= $slider_details['featured_second_description']; ?>
                    </p>
                </div>

            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
            <div class="text-bx">
                <div class="img-bx">
                    <div class="left-box">03</div>
                    <div class="right-box">
                        <img src="<?= ASSETS_PATH ?>images/recevez_icon.png" alt="" class="img-responsive">
                    </div>
                </div>
                <div class="info-text">
                    <h5>
                        <?= $slider_details['featured_third_title']; ?>
                    </h5>
                    <p>
                        <?= $slider_details['featured_third_description']; ?>
                    </p>
                </div>

            </div>
        </div>

        <div class="gradient-button-bx">
            <?php if ($this->user_type == 1) : ?>
                <a href="<?= USERS_PATH ?>/send_application" class="btn btn-lg grdnt-brn red-font">
                <?php else: ?>
                    <a href="javascript:void(0);" class="btn btn-lg grdnt-brn red-font login-popup-model" redirect_to_url="<?= USERS_PATH . '/build_cv'; ?>">
                    <?php endif; ?>
                    <span>
                        <img src="<?= ASSETS_PATH ?>images/send_icon.png" alt="" class="img-responsive">
                    </span>
                    <?= lang('HOME_STEPS_BUTTON') ?>
                </a>
        </div>
    </div>
</div>
<div class="hm-info-banner">
    <div class="hidden-xs hidden-sm">
        <img src="<?= ASSETS_PATH ?>images/bg3.png" alt="" class="img-responsive bg3_image">
        <div class="dp_top_class">
            <div class="dp_main_container">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 dp_left_panel">
                    <div class="info-text">
                        <div class="hm-info-header">
                            <div class="number-bx green-light">
                                1
                            </div>
                            <h3>
                                <?= lang('HOME_BANNER_BLOCK_1_TITLE') ?>
                            </h3>
                        </div>
                        <div class="left-text dp_text_box">
                            <h3>
                                <?= lang('HOME_BANNER_BLOCK_1_SUB_TITLE') ?>
                            </h3>
                            <ul>
                                <li>
                                    &#45; <?= lang('HOME_BANNER_BLOCK_1_LIST1') ?>
                                </li>
                                <li>
                                    &#45; <?= lang('HOME_BANNER_BLOCK_1_LIST2') ?>
                                </li>
                                <li>
                                    &#45; <?= lang('HOME_BANNER_BLOCK_1_LIST3') ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="row col-lg-6 col-md-6 col-sm-6 col-xs-12 dp_right_panel">
                    <div style="background: none" class="info-text right-info">
                        <div class="hm-info-header">
                            <div class="number-bx green-light">
                                2
                            </div>
                            <h3 class="green-light-font">
                                <?= lang('HOME_BANNER_BLOCK_2_TITLE') ?>
                            </h3>
                        </div>
                        <div class="left-text dp_text_box">
                            <h3>
                                <?= lang('HOME_BANNER_BLOCK_2_SUB_TITLE') ?>
                            </h3>
                            <ul>
                                <li>
                                    &#45; <?= lang('HOME_BANNER_BLOCK_2_LIST1') ?>
                                </li>
                                <li>
                                    &#45; <?= lang('HOME_BANNER_BLOCK_2_LIST2') ?>
                                </li>
                                <li>
                                    &#45; <?= lang('HOME_BANNER_BLOCK_2_LIST3') ?>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="">
        <div class="visible-xs hidden-lg hidden-md visible-sm">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 xs-left">
                <div class="info-text left-info">
                    <div class="hm-info-header">
                        <div class="number-bx green-light">
                            1
                        </div>
                        <h3>
                            <?= lang('HOME_BANNER_BLOCK_1_TITLE') ?>
                        </h3>
                    </div>
                    <div class="left-text">
                        <h3>
                            <?= lang('HOME_BANNER_BLOCK_1_SUB_TITLE') ?>
                        </h3>
                        <ul>
                            <li>
                                <?= lang('HOME_BANNER_BLOCK_1_LIST1') ?>
                            </li>
                            <li>
                                <?= lang('HOME_BANNER_BLOCK_1_LIST2') ?>
                            </li>
                            <li>
                                <?= lang('HOME_BANNER_BLOCK_1_LIST3') ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 xs-right">
                <div class="info-text right-info">
                    <div class="hm-info-header">
                        <div class="number-bx green-light green-light-mob">
                            2
                        </div>
                        <h3 class="green-light-font">
                            <?= lang('HOME_BANNER_BLOCK_2_TITLE') ?>
                        </h3>
                    </div>
                    <div class="left-text">
                        <h3>
                            <?= lang('HOME_BANNER_BLOCK_2_SUB_TITLE') ?>
                        </h3>
                        <ul>
                            <li>
                                <?= lang('HOME_BANNER_BLOCK_2_LIST1') ?>
                            </li>
                            <li>
                                <?= lang('HOME_BANNER_BLOCK_2_LIST2') ?>
                            </li>
                            <li>
                                <?= lang('HOME_BANNER_BLOCK_2_LIST3') ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hm-slider-content">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                <div class="col-md-11 col-md-offset-1 col-xs-12">
                    <?php if (!empty($testimonials_details)): ?>
                        <div class="carousel slide" id="fade-quote-carousel" data-ride="carousel" data-interval="3000">
                            <ol class="carousel-indicators">
                                <?php
                                if (!empty($testimonials_details)) {
                                    foreach ($testimonials_details as $key => $row_testimonials) {
                                        ?>
                                        <li data-target="#fade-quote-carousel" data-slide-to="<?= $key; ?>" class="<?php echo ($key == 0 ? 'active' : ''); ?>"></li>
                                        <?php
                                    }
                                }
                                ?>
                            </ol>
                            <div class="carousel-inner">
                                <?php
                                foreach ($testimonials_details as $key => $row_testimonials) :

                                    $cms_t_image = UPLOAD_FILE_FOLDER . "/" . UPLOAD_CMS_FOLDER . "/" . $row_testimonials['cms_t_image'];
                                    $cms_image_file = UPLOAD_ABS_PATH . 'default.png';
                                    if (!empty($row_testimonials['cms_t_image']) && file_exists($cms_t_image)):
                                        $cms_image_file = UPLOAD_ABS_PATH . UPLOAD_CMS_FOLDER . "/" . $row_testimonials['cms_t_image'];
                                    endif;
                                    ?>
                                    <div class="<?php echo ($key == 0 ? 'active' : ''); ?> item">
                                        <div class="img-box">
                                            <img src="<?= $cms_image_file; ?>" alt="<?= get_slash_formatted_text($row_testimonials['title']) ?>" class="img-responsive img-circle">
                                        </div>
                                        <div class="text-bx">
                                            <h4>
                                                <?= get_slash_formatted_text($row_testimonials['title']); ?>
                                            </h4>
                                            <div class="quote">
                                                ”
                                            </div>
                                            <p>
                                                <?= get_slash_formatted_text($row_testimonials['description']); ?>
                                            </p>
                                        </div>	
                                    </div>
                                    <?php
                                endforeach;
                                ?>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="slider-blockquote" style="<?= empty($testimonials_details) ? 'margin-top:100px' : '' ?>">
                        <blockquote>
                            <?= APP_NAME; ?>
                        </blockquote>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 slider-left-bdr">
                <div class="slider-right-bx">
                    <p>
                        <span aria-hidden="true">&laquo;</span> 
                        <?= !empty($slider_details['create_account_text']) ? get_slash_formatted_text($slider_details['create_account_text']) : lang('HOME_SLIDER_TESTIMONIAL'); ?>
                        <span>&raquo;</span>
                    </p>
                </div>
                <?php if (empty($this->user_type)) : ?>
                    <div class="gradient-button-bx">
                        <a href="javascript:void(0);" class="btn btn-lg grdnt-brn red-font registration-popup-model" redirect_to_url="<?= USERS_PATH; ?>">
                            <?= lang('HOME_SLIDER_TESTIMONIAL_BUTTON') ?> 
                        </a>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>

<div class="recrutement-bx">
    <div class="container">
        <div class="title-box">
            <label>A</label>
            <h3 class="green-light-font">
                <?= lang('HOME_NEWS_TITLE_1') ?>  <span class="red-font"><?= lang('HOME_NEWS_TITLE_2') ?></span> <?= lang('HOME_NEWS_TITLE_3') ?> <span class="red-font"><?= lang('HOME_NEWS_TITLE_4') ?></span>
            </h3>
        </div>
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 recrutment-step">
            <div class="row">
                <?php if (!empty($news_details)) { ?>
                    <div class="owl-carousel owl-theme">
                        <?php
                        foreach ($news_details as $row_news) {

                            $news_title = get_slash_formatted_text($row_news['news_name']);
                            $news_title = !empty($news_title) ? strlen($news_title) > DEFAULT_NEWS_TITLE_TEXT_LIMIT ? mb_substr($news_title, 0, DEFAULT_NEWS_TITLE_TEXT_LIMIT) . "..." : $news_title : '';

                            $news_description = get_slash_formatted_text($row_news['news_description']);
                            $news_description = !empty($news_description) ? strlen($news_description) > DEFAULT_NEWS_DESCRIPTION_TEXT_LIMIT ? mb_substr($news_description, 0, DEFAULT_NEWS_DESCRIPTION_TEXT_LIMIT) . "..." : $news_description : '';
                            ?>
                            <div class="step-box" data-newsid="<?= $row_news['news_id'] ?>">
                                <div class="img-bx">
                                    <img src="<?= $row_news['news_image_name'] ?>" alt="<?= $news_title ?>" class="img-responsive news_section">
                                    <div class="hover-overlay">
                                        <div class="hover-text">
                                            <p><?= trim($news_title); ?></p>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-bx">
                                    <h4><?= trim($news_title); ?></h4>
                                    <p><?= trim($news_description) ?></p>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                <?php } else {
                    ?>
                    <div class="no_news_available"><?= lang('HOME_NO_NEWS_AVAILABLE') ?> </div>
                <?php }
                ?>
            </div>
        </div>
    </div>
</div>

<div class="hm-email-bx">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <h3><?= $slider_details['subscribe_text']; ?></h3>
            </div>
            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                <div class="input-group">
                    <input type="email" class="form-control input-lg" id="newslatter_email" placeholder="<?= lang('HOME_NEWSLETTER_INPUT_PLACEHOLDER') ?>" data-rule-maxlength="255"/>
                    <a class="btn btn-lg hm-email-send-btn" type="button" id="btn_newslatter_email">
                        <?= lang('HOME_NEWSLETTER_BUTTON') ?>
                    </a>
                    <label id="subjecterror_required" class="error" for="subject" style="color: red; margin-top: 2px; margin-left: 3px;display:none;">
                        <?= lang('HOME_NEWSLETTER_ERROR_REQUIRED') ?>
                    </label>
                    <label id="subjecterror_email" class="error" for="subject" style="color: red; margin-top: 2px; margin-left: 3px;display:none;">
                        <?= lang('HOME_NEWSLETTER_ERROR_EMAIL') ?>
                    </label>

                </div>
            </div> 
        </div>
    </div>
</div>

<div id="top_arrow_img">
    <a href="javascript:void(0)" id="go_top">
        <img src="<?= ASSETS_PATH ?>images/arrow-up.png" height="50">
    </a>
</div>
<!--registration and signin froms end -->

<div class="modal fade hm-signin-popup hm-popup in" id="news_model" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <div class="modal-title" id="myModalLabel">                    
                    <h3><?= lang('NEWS_DETAILS') ?></h3>
                </div>
            </div>
            <div class="btn-line">
                <img src="<?= ASSETS_PATH; ?>images/line.png" alt="" class="img-responsive">
            </div>
            <form role="form" novalidate="novalidate">
                <div class="modal-body">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label><?= lang('TITLE') ?></label>
                            <p class="news_title"></p>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label><?= lang('DESCRIPTION') ?></label>
                            <p class="news_description"></p>
                        </div>
                    </div>                   
                </div>
            </form>
        </div>
    </div>
</div>


<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/sha1.min.js"></script>
<script type="text/javascript">
    jQuery.sha1 = sha1;</script>
<script type="text/javascript">

    var autocomplete = '';
    var confirmation_email = "<?= lang('CONFIRMATION_EMAIL') ?>";
    var letter_only = "<?= lang('LETTER_ONLY') ?>";
    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById('autocomplete')),
                {types: ['geocode']}
        );
    }

    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    jQuery(document).ready(function () {

        var invitation_code = "<?= $this->session->userdata('invitation_code') ?>";
        if (invitation_code != "") {
            jQuery("#registration-popup").modal('show');
            jQuery("#invitation_code").val(invitation_code);
        }

        $(".owl-carousel").owlCarousel({
            navigation: true,
            dots: false,
            pagination: false,
            loop: true,
            margin: 10,
            autoplay: true,
            autoplayTimeout: 3000,
            autoplayHoverPause: true,
            responsive: {
                0: {
                    items: 1
                },
                360: {
                    items: 1
                },
                480: {
                    items: 1
                },
                520: {
                    items: 1
                },
                600: {
                    items: 2
                },
                767: {
                    items: 2
                },
                1000: {
                    items: 3
                },
                2000: {
                    items: 3
                },
                3000: {
                    items: 3
                }
            }
        });
        $(".owl-prev").html('<img src="<?= ASSETS_PATH; ?>images/left_arrow.png" />');
        $(".owl-next").html('<img src="<?= ASSETS_PATH; ?>images/right_arrow.png" />');

        $(".step-box").click(function () {
            var news_id = $(this).attr('data-newsid');
            jQuery.ajax({
                "url": base_url + '/get_news_details',
                type: "POST",
                data: {
                    'news_id': news_id
                },
                dataType: 'json',
                cache: false,
                success: function (response) {
                    if (response.success == true) {
                        $(".news_title").html(response.title);
                        $(".news_description").html(response.description);
                        $('#news_model').modal('show');
                    } else {
                        toastr.error(ajax_response.message, {timeOut: 5000});
                    }
                },
                error: function () {
                    console.log("Problem in ajax");
                }
            });
        });

        var highest = null;
        var height = 0;
        $(".news_section").each(function () {
            var test = $(this).height();
            if (test > height) {
                height = test;
                highest = $(this).height();
            }
        });
        $(".news_section").css('height', highest + 'px');
    });
    window.fbAsyncInit = function () {
        FB.init({
            appId: '<?= FB_APP_ID ?>',
            xfbml: true,
            version: 'v2.8'
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script type="text/javascript" src="//platform.linkedin.com/in.js">
    api_key: 78ev6kvpru6mm2
            onLoad: onLinkedInLoad
</script>

<?php
$selected_language = !empty($this->session->userdata()['language']) ? $this->session->userdata()['language'] : DEFAULT_LANG;
if ($selected_language == "fr") {
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_API_KEY ?>&libraries=places&callback=initAutocomplete&language=fr"></script>
    <?php
} else {
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_API_KEY ?>&libraries=places&callback=initAutocomplete&language=en"></script>
    <?php
}
?>

<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/pages/home.js"></script>