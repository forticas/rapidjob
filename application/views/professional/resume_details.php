<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="cv-box top-section">
    <div class="container">
        <div class="title-box">
            <label>CV</label>
            <h3 class="green-light-font">
                <span class="red-font"><?= lang("COMMON_CV"); ?></span><?= lang("PROF_RESUME_DETAILS_HEADING"); ?>
            </h3>
        </div>
        <div class="row">
            <?php
            if (!empty($user_data) && isset($user_data['u_id']) && is_numeric($user_data['u_id']) && $user_data['u_id'] > 0) :
                $candidate_id = $user_data['u_id'];
                $candidate_name = ucwords($user_data['u_first_name'] . " " . $user_data['u_last_name']);
                ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="cv-profile-bx">
                        <?php
                        $image = UPLOAD_FILE_FOLDER . '/' . DEFAULT_IMAGE_NAME;
                        if (!empty($user_data['u_image'])) :
                            if (file_exists(UPLOAD_FILE_FOLDER . '/' . UPLOAD_USERS_FOLDER . '/' . $candidate_id . '/' . $user_data['u_image'])) :
                                $image = UPLOAD_USERS_FOLDER . '/' . $candidate_id . '/' . $user_data['u_image'];
                            endif;
                        endif;
                        ?>
                        <div class="inner-profile">
                            <div class="img-box">
                                <div class="profile-img">
                                    <img alt="profile photo" class="img-responsive img-circle" height="180px" width="180px" src="<?= IMAGE_MANIPULATION_URL . "url=" . $image . "&h=180&w=180&default=true" ?>" >
                                </div>
                            </div>
                            <div class="text-bx set_details_text">
                                <h3><?= $candidate_name; ?></h3>
                                <p><?php echo (!empty($user_data['wj_name_' . $this->current_lang])) ? trim($user_data['wj_name_' . $this->current_lang]) : lang('COMMON_JOB_NOT_DEFINED'); ?></p>
                            </div>
                        </div>
                        <div class="cv-icon-bx">
                            <div class="icon-bx">
                                <?php if (!empty($user_documents) && isset($user_documents['udf_id']) && is_numeric($user_documents['udf_id']) && $user_documents['udf_id'] > 0) : ?>
                                    <?php
                                    if ($user_documents['udf_type'] == 1):
                                        if (isset($user_documents['udf_cv_file_name']) && !empty($user_documents['udf_cv_file_name'])):
                                            $cv_file = UPLOAD_FILE_FOLDER . '/' . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_documents['udf_id'] . '/' . $user_documents['udf_cv_file_name'];
                                            if (file_exists($cv_file)):
                                                $cv_file = UPLOAD_ABS_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_documents['udf_id'] . '/' . $user_documents['udf_cv_file_name'];
                                                ?>
                                                <a target="_blank" download="<?= $cv_file; ?>" href="<?= $cv_file; ?>"  class="green-tooltip">
                                                    <img src="<?= ASSETS_PATH ?>images/download.png" alt="" class="img-responsive img-circle">
                                                    <div class="tooltip-bx1"><img src="<?= ASSETS_PATH ?>images/info_icon.png" alt="" class=""> <?= lang("COMMON_DOWNLOAD_CV"); ?><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                                                </a>
                                                <?php
                                            endif;
                                        endif;
                                    elseif ($user_documents['udf_type'] == 2):
                                        if (isset($user_documents['udf_rapidjob_cv_file_name']) && !empty($user_documents['udf_rapidjob_cv_file_name'])):
                                            $cv_file = UPLOAD_FILE_FOLDER . '/' . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_documents['udf_id'] . '/' . $user_documents['udf_rapidjob_cv_file_name'];
                                            if (file_exists($cv_file)):
                                                $cv_file = UPLOAD_ABS_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $user_documents['udf_id'] . '/' . $user_documents['udf_rapidjob_cv_file_name'];
                                                ?>
                                                <a target="_blank" download="<?= $cv_file; ?>" href="<?= $cv_file; ?>"  class="green-tooltip">
                                                    <img src="<?= ASSETS_PATH ?>images/download.png" alt="" class="img-responsive img-circle">
                                                    <div class="tooltip-bx1"><img src="<?= ASSETS_PATH ?>images/info_icon.png" alt="" class=""> <?= lang("COMMON_DOWNLOAD_CV"); ?><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                                                </a>
                                                <?php
                                            endif;
                                        endif;
                                    endif;
                                    ?>
                                <?php endif; ?>

                                <?php if (!empty($user_favourite) && $user_favourite > 0) : ?>
                                    <a href="javascript:void(0);" class="yellow-tooltip remove_from_favourite" candidate_id="<?= $candidate_id; ?>">
                                        <img src="<?= ASSETS_PATH ?>images/fav_selected.png" alt="" class="img-responsive img-circle selected_image_<?= $candidate_id; ?>">
                                        <div class="tooltip-bx2">
                                            <img src="<?= ASSETS_PATH ?>images/info_icon.png" alt="" class=""> <span class="add_remove_favourite_text"><?= lang("COMMON_FAVOURITE_REMOVE"); ?></span>
                                            <i class="fa fa-caret-up" aria-hidden="true"></i>
                                        </div>
                                    </a>
                                <?php else: ?>
                                    <a href="javascript:void(0);" class="yellow-tooltip add_to_favourite" candidate_id="<?= $candidate_id; ?>">
                                        <img src="<?= ASSETS_PATH ?>images/fav_unselected.png" alt="" class="img-responsive img-circle selected_image_<?= $candidate_id; ?>">
                                        <div class="tooltip-bx2">
                                            <img src="<?= ASSETS_PATH ?>images/info_icon.png" alt="" class=""> <span class="add_remove_favourite_text"><?= lang("COMMON_ADD_TO_FAVOURITE"); ?></span>
                                            <i class="fa fa-caret-up" aria-hidden="true"></i>
                                        </div>
                                    </a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <div class="cv-text-bx border-radius">
                        <div class="user_bio_desc_div">
                            <?php
                            $user_about = get_formatted_text($user_data['ud_user_bio']);
                            if (empty($user_about)):
                                echo lang("COMMON_USER_DETAILS_NOT_DEFINED");
                            else:
                                echo $user_about;
                            endif;
                            ?>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                    <div class="candidate-list">
                        <a href="" id="experience-popup-btn" class="experience-popup-btn" data-toggle="modal" data-target="#experience-popup" data-backdrop="static" data-keyboard="false">
                            <img src="<?= ASSETS_PATH ?>images/candidate_contact.png" alt="" class="img-responsive">
                        </a>
                        <a href="" id="experience-popup-btn" class="experience-popup-btn" data-toggle="modal" data-target="#experience-popup" data-backdrop="static" data-keyboard="false">
                            <h3><?= lang("PROF_RESUME_DETAILS_CONTACT_TO_CANDIDATE") ?></h3>
                        </a>
                    </div>

                    <!-- MY_EXPERIANCES -->
                    <label class="profile-infor-ttl">
                        <?= lang('SHOW_PROFILE_PAGE_MY_EXPERIANCES') ?>
                    </label>
                    <?php
                    if (!empty($user_exp_data)) :
                        foreach ($user_exp_data as $value) :
                            ?>
                            <div class="profile-info-bx border-radius">
                                <p class="extra-pad"><?php echo $value['wj_name_' . $this->current_lang] . " - " . $value['ue_company'] . " (" . $value['ue_place'] . ")"; ?></p>
                                <p class="extra-pad">
                                    <?php
                                    $end_exp = !empty($value['ue_end_date']) ? date("F Y", $value['ue_end_date']) : lang("SHOW_PROFILE_PAGE_WORKING_HERE");
                                    echo lang("COMMON_DURATION") . " (" . date('F Y', $value['ue_start_date']) . " " . lang("SHOW_PROFILE_PAGE_ADD_EXPERIANCES_PERIOD_TO") . " " . $end_exp . ")";
                                    ?>
                                </p>
                                <?php echo get_formatted_text($value['ue_description']); ?>
                            </div>
                            <?php
                        endforeach;
                    else:
                        ?>
                        <div class="profile-info-bx border-radius">
                            <h3 class="no_exp_set"><?= lang('COMMON_EXPERIENCE_NOT_DEFINED'); ?></h3>
                        </div>
                    <?php endif; ?>

                    <!-- / END OF MY_EXPERIANCES / -->


                    <!-- MY_TRAINING -->
                    <label class="profile-infor-ttl">
                        <?= lang('SHOW_PROFILE_PAGE_MY_TRAININGS') ?>
                    </label>

                    <?php
                    if (!empty($user_training_data)) :
                        foreach ($user_training_data as $value) :
                            ?>
                            <div class="profile-info-bx border-radius">
                                <p class="extra-pad">
                                    <?php
                                    echo $value['ut_name'] . " " . $value['ut_establissment'];
                                    ?>
                                </p>
                                <p class="extra-pad">
                                    <?php
                                    echo lang("COMMON_DURATION") . " (" . date('F Y', $value['ut_start_date']) . " " . lang("COMMON_TO") . " " . date("F Y", $value['ut_end_date']) . ")";
                                    ?>
                                </p>
                                <?php echo get_formatted_text($value['ut_description']); ?>
                            </div>
                            <?php
                        endforeach;
                    else:
                        ?>
                        <div class="profile-info-bx border-radius">
                            <h3 class="no_training_set"><?= lang('COMMON_TRAINING_NOT_DEFINED'); ?></h3>
                        </div>
                    <?php endif; ?>

                    <!-- / END OF MY_TRAINING/ -->

                    <!-- MY_SKILL -->
                    <label class="profile-infor-ttl">
                        <?= lang('SHOW_PROFILE_PAGE_MY_SKILLS') ?>
                    </label>
                    <?php
                    if (!empty($user_skill_data)) :
                        foreach ($user_skill_data as $value) :
                            ?>
                            <div class="profile-info-bx border-radius">
                                <?php if (!empty($value['us_name'])): ?>
                                    <p class="extra-pad">
                                        <?= $value['us_name']; ?>
                                    </p>
                                    <?php
                                endif;
                                if (!empty($value['us_skills'])) :
                                    $us_skill = json_decode($value['us_skills'], true);
                                    foreach ($us_skill as $key => $es_value) :
                                        ?>
                                        <div class="text-box">
                                            <span>
                                                <?= $key; ?>
                                            </span>
                                            <div class="star-rate star-section">
                                                <fieldset class="rating">
                                                    <input type="radio" id="star5<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="5" <?php if ($es_value == 5) echo 'checked="checked"' ?> disabled="disabled" /><label data-rating="5" class="full selected" for="star5<?= $key; ?>" title="Awesome - 5 stars"></label>
                                                    <input type="radio" id="star4<?= $key; ?>half" name="user_show_rating<?= $key; ?>" value="4.5" <?php if ($es_value == 4.5) echo 'checked="checked"' ?> disabled="disabled" /><label class="half" for="star4<?= $key; ?>half" title="Pretty good - 4.5 stars"></label>
                                                    <input type="radio" id="star4<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="4" <?php if ($es_value == 4) echo 'checked="checked"' ?> disabled="disabled" /><label class = "full" for="star4<?= $key; ?>" title="Pretty good - 4 stars"></label>
                                                    <input type="radio" id="star3<?= $key; ?>half" name="user_show_rating<?= $key; ?>" value="3.5" <?php if ($es_value == 3.5) echo 'checked="checked"' ?> /><label class="half" for="star3<?= $key; ?>half" title="Meh - 3.5 stars"></label>
                                                    <input type="radio" id="star3<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="3" <?php if ($es_value == 3) echo 'checked="checked"' ?> /><label class = "full" for="star3<?= $key; ?>" title="Meh - 3 stars"></label>
                                                    <input type="radio" id="star2<?= $key; ?>half" name="user_show_rating<?= $key; ?>" value="2.5" <?php if ($es_value == 2.5) echo 'checked="checked"' ?> /><label class="half" for="star2<?= $key; ?>half" title="Kinda bad - 2.5 stars"></label>
                                                    <input type="radio" id="star2<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="2" <?php if ($es_value == 2) echo 'checked="checked"' ?> /><label class = "full" for="star2<?= $key; ?>" title="Kinda bad - 2 stars"></label>
                                                    <input type="radio" id="star1<?= $key; ?>half" name="user_show_rating<?= $key; ?>" value="1.5" <?php if ($es_value == 1.5) echo 'checked="checked"' ?> /><label class="half" for="star1<?= $key; ?>half" title="Meh - 1.5 stars"></label>
                                                    <input type="radio" id="star1<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="1" <?php if ($es_value == 1) echo 'checked="checked"' ?> /><label class = "full" for="star1<?= $key; ?>" title="Sucks big time - 1 star"></label>
                                                    <input type="radio" id="starhalf<?= $key; ?>" name="user_show_rating<?= $key; ?>" value="0.5" <?php if ($es_value == 0.5) echo 'checked="checked"' ?> /><label class="half" for="starhalf<?= $key; ?>" title="Sucks big time - 0.5 stars"></label>
                                                </fieldset>
                                            </div>
                                        </div>
                                        <?php
                                    endforeach;
                                endif;
                                ?>
                            </div>
                            <?php
                        endforeach;
                    else:
                        ?>
                        <div class="profile-info-bx border-radius">
                            <h3 class="no_skill_set"><?= lang('COMMON_SKILL_NOT_DEFINED'); ?></h3>
                        </div>
                    <?php endif; ?>
                    <!-- /END OF MY_SKILL / -->


                    <!-- MY_HOBBIES  -->
                    <label class="profile-infor-ttl">
                        <?= lang('SHOW_PROFILE_PAGE_MY_HOBBIES') ?>
                    </label>
                    <div class="profile-info-bx border-radius">
                        <?php
                        if (!empty($user_hobbies_data)) :
                            foreach ($user_hobbies_data as $value) :
                                if (!empty($value['uh_title'])) :
                                    $uh_title = json_decode($value['uh_title'], true);
                                    $uh_description = json_decode($value['uh_description'], true);
                                    foreach ($uh_title as $key => $title_value) :
                                        if (!empty(trim($title_value))):
                                            ?>
                                            <p class="extra-pad">
                                                <?php
                                                echo trim($title_value);
                                                if (!empty($uh_description[$key])) :
                                                    ?>
                                                    : <label>
                                                        <?= trim($uh_description[$key]) ?>
                                                    </label>
                                                <?php endif; ?>
                                            </p>
                                            <?php
                                        endif;
                                    endforeach;
                                endif;
                            endforeach;
                        else:
                            ?>
                            <h3 class="no_hobbies_set"><?= lang('COMMON_HOBBIES_NOT_DEFINED'); ?></h3>
                        <?php endif; ?>
                    </div>
                    <!-- /END OF MY_HOBBIES / -->
                </div>


                <!--    Modal pop-up for add experience -->
                <div class="modal fade profile-popup" id="experience-popup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form id="send_email_to_candidate_form" method="post" action="javascript:void(0);">
                                <input type="hidden" name="send_email_to_candidate_form" value="send_email_to_candidate_form">
                                <div class="modal-header set_header_body">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body set_model_body">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 set_desc_body">
                                            <div class="form-group">
                                                <label for="experience_description" class="set_replay_label"><span class="red-font"><?= lang('PROF_RESUME_DETAILS_SEND_MAIL_TO_CANDIDATE') ?></span> <span class="green-light-font"><?= ucwords($user_data['u_first_name'] . " " . $user_data['u_last_name']); ?></span></label>
                                                <textarea name="experience_description" id="experience_description" class="required"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer set_footer_body">
                                    <div class="popup-btn-box">
                                        <button type="submit" class="round-btn red-bg btn btn-block btn-social btn-profile">
                                            <span class="dark-red">
                                                <img src="<?= ASSETS_PATH ?>images/forward.png" alt="" class="img-responsive">
                                            </span>
                                            <?= lang('COMMON_TO_SEND') ?>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            <?php else: ?>
                <h3 class="resume_details_empty"><?= lang("PROF_RESUME_DETAILS_CANDIDATE_EMPTY"); ?></h3>
            <?php endif; ?>
        </div>
    </div>
</div>

<script>
    var remove_favourite_text = "<?= lang("COMMON_FAVOURITE_REMOVE"); ?>";
    var add_favourite_text = "<?= lang("COMMON_ADD_TO_FAVOURITE"); ?>";
    var favourite_image = "<?= ASSETS_PATH ?>images/fav_selected.png";
    var unfavourite_image = "<?= ASSETS_PATH ?>images/fav_unselected.png";
    var get_user_id = "<?= isset($candidate_id) && !empty($candidate_id) ? trim($candidate_id) : ''; ?>";
    var get_user_name = "<?= isset($candidate_name) && !empty($candidate_name) ? trim($candidate_name) : lang("COMMON_USER_CANDIDATE"); ?>";
</script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/pages/prof_resume_details.js"></script>