<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="build-cv top-section cvs-prioritys">
    <div class="container">
        <div class="title-box">
            <label>R</label>
            <h3 class="green-light-font">
                <?= lang("PROF_SEARCH_PRIORITY_RESUME_HEADING") ?> <span class="red-font"><?= lang("PROF_SEARCH_PRIORITY_RESUME_HEADING_2") ?></span>
            </h3>
        </div>

        <?php echo form_open(PROFESSIONAL_PATH . '/search_priority_resume', array("id" => "search_priority_resume_form")); ?>
        <input type="hidden" name="cd_id" value="<?= $user_data['cd_id']; ?>">
        <input type="hidden" name="search_priority_resume" value="true">
        <div class="edit-profile-bx">
            <div class="inpot-bx select-box-bottom-space edit-profile-bx-search">
                <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label class="select-label"><?= lang("COMMON_SEARCH_JOB"); ?></label>
                            <div class="select-bx">
                                <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                                <select name="job_type" id="job_type" class="selectpicker required " <?php if (empty($work_job)) { ?> disabled="disabled" title="<?= lang('PROF_RESUME_DATABASE_NO_JOB_TYPE_SET'); ?>"  <?php } else { ?> data-live-search="true" title="<?= lang('COMMON_SEARCH_BY_JOB'); ?>" <?php } ?> >
                                    <?php
                                    if (!empty($work_job)) :
                                        foreach ($work_job as $value) :
                                            if ($value['wj_id'] == $user_data['cd_search_job_type']) :
                                                ?>
                                                <option selected="selected" value="<?= $value['wj_id']; ?>"><?= $value['job_name']; ?></option>
                                            <?php else: ?>
                                                <option value="<?= $value['wj_id']; ?>"><?= $value['job_name']; ?></option>
                                            <?php
                                            endif;
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12" >
                            <label class="select-label"><?= lang("COMMON_SEARCH_AREA"); ?></label>
                            <div class="select-bx">
                                <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                                <select data-actions-box="true" name="activity_area[]" id="activity_area" class="selectpicker required " multiple="multiple" multiple <?php if (empty($activity_area)) { ?> disabled="disabled" title="<?= lang('PROF_RESUME_DATABASE_NO_ACTIVITY_AREA_SET'); ?>"  <?php } else { ?> data-selected-text-format="count > 1" data-live-search="true" multiple title="<?= lang('COMMON_SEARCH_BY_ACTIVITY_AREA'); ?>" <?php } ?> >
                                    <?php
                                    if (!empty($activity_area)) :
                                        foreach ($activity_area as $value) :
                                            if (in_array($value['wa_id'], explode(',', $user_data['cd_search_activity_area']))):
                                                ?>
                                                <option selected="selected" value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                            <?php else: ?>
                                                <option value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                            <?php
                                            endif;
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <div class="inpot-bx select-box-bottom-space-bth">
                <div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <label class="select-label work-places"><?= lang("COMMON_SEARCH_WORK_PLACES_LABEL"); ?></label>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label class="select-label select-label-limit"><?= lang("COMMON_SEARCH_WORK_PLACES_LIMIT"); ?></label>
                            <div class="select-bx">
                                <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                                <select data-actions-box="true" data-max-options="5" name="work_place[]" id="work_place" class="selectpicker required " multiple="multiple" multiple <?php if (empty($work_places)) { ?> disabled="disabled" title="<?= lang('PROF_RESUME_DATABASE_NO_WORK_PLACE_SET'); ?>"  <?php } else { ?> data-selected-text-format="count > 1" data-live-search="true" multiple title="<?= lang('COMMON_SEARCH_BY_LOCATION'); ?>" <?php } ?>>
                                    <?php
                                    if (!empty($work_places)) :
                                        foreach ($work_places as $value) :
                                            if (in_array($value['wp_id'], explode(',', $user_data['cd_search_work_place']))):
                                                ?>
                                                <option selected="selected" value="<?= $value['wp_id']; ?>"><?= $value['wp_name']; ?></option>
                                            <?php else: ?>
                                                <option value="<?= $value['wp_id']; ?>"><?= $value['wp_name']; ?></option>
                                            <?php
                                            endif;
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <label class="select-label"><?= lang("COMMON_SEARCH_EDUCATION_LEVEL") ?></label>
                            <div class="select-bx">
                                <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                                <select name="education_level" id="education_level" class="selectpicker required " <?php if (empty($level_of_educations)) { ?> disabled="disabled" title="<?= lang('BUILDCV_PAGE_NO_EDU_LEVEL_SET'); ?>"  <?php } else { ?> data-live-search="true" title="<?= lang('BUILDCV_PAGE_CHOOSE_EDUCATION_LEVEL'); ?>" <?php } ?> >
                                    <?php
                                    if (!empty($level_of_educations)) :
                                        foreach ($level_of_educations as $value) :
                                            if ($value['ed_id'] == $user_data['cd_search_education_level']) :
                                                ?>
                                                <option selected="selected" value="<?= $value['ed_id']; ?>"><?= $value['ed_name']; ?></option>
                                            <?php else: ?>
                                                <option value="<?= $value['ed_id']; ?>"><?= $value['ed_name']; ?></option>
                                            <?php
                                            endif;
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                                <?php if (!empty(form_error('education_level'))) : ?><label class="text-danger"><?= form_error('education_level'); ?></label><?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="inpot-bx">
                <div class="edit-profile-btn">
                    <button type="submit" class="round-btn red-bg btn btn-block btn-social btn-profile">
                        <span class="dark-red">
                            <img src="<?= ASSETS_PATH ?>images/forward.png" alt="" class="img-responsive">
                        </span>
                        <?= lang("PROF_SEARCH_PRIORITY_RESUME_SEND_REQUEST") ?>
                    </button>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>

        <div class="edit-profile-bx change-password-section">
            <label class="information-seen-by-candidates">
                <?= lang("PROF_SEARCH_PRIORITY_RESUME_INFORMATION") ?>
            </label>
            <div class="clearfix"></div>
            <div class="inpot-bx">

                <div class="hide">
                    <?php
                    echo form_open_multipart(PROFESSIONAL_PATH . '/upload_image', array("id" => "photo_form"));
                    ?>
                    <input type="file" onchange="this.form.submit()" accept="image/*" required="required" name="image" id="form_image">
                    <?php
                    echo form_close();
                    ?>
                </div>
                
                <?php echo form_open(PROFESSIONAL_PATH . '/search_priority_resume', array("id" => "show_resume_to_candidate_form")); ?>
                <input type="hidden" name="cd_id" value="<?= $user_data['cd_id']; ?>">
                <input type="hidden" name="show_resume_to_candidate_form" value="true">
                <div class="row">
                    <div class="clearfix"></div>
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 left-img-right-content">
                        <div class="bg-img">
                            <div class="img-box">
                                <div class="profile-img">
                                    <img alt="profile photo" class="img-responsive img-circle" height="180px" width="180px" src="<?= IMAGE_MANIPULATION_URL . "url=" . $this->session->userdata("profile_photo") . "&h=180&w=180&default=true" ?>" >
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <?php
                            $image = $this->session->userdata("profile_photo");
                            if (!empty($image)) :
                                $image = UPLOAD_REL_PATH . '/' . $this->session->userdata("profile_photo");
                            else:
                                $image = UPLOAD_FILE_FOLDER . '/' . DEFAULT_IMAGE_NAME;
                            endif;
                            ?>
                            <p class="p-text text-center">
                                <a target="_blank" download="<?= $image; ?>" href="<?= $image; ?>">
                                    <?= lang('SHOW_PROFILE_PAGE_DOWNLOAD_PROFILE_PICTURE') ?>
                                </a>
                            </p>
                            <div class="item-btn-bx">
                                <div class="btn-bx">
                                    <a class="round-btn dark-blue-bg btn btn-block btn-social btn-profile change_photo" href="javascript:void(0);">
                                        <span class="dark-blue-bg">
                                            <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/selector.png">
                                        </span>
                                        <?= lang('SHOW_PROFILE_PAGE_UPLOAD_PROFILE_PICTURE') ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label class="select-label" for="recruiter_social_reason"><?= lang("PROF_SEARCH_PRIORITY_RESUME_SOCIAL_REASON_OF_COMPANY") ?></label>
                            <div class="form-group">
                                <input name="social_reason" placeholder="<?= lang("PROF_REGISTER_SOCIAL_REASON_OF_COMPANY"); ?>" class="form-control required" type="text" id="recruiter_social_reason"  value="<?= trim($user_data['cd_social_reason']); ?>">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <label class="select-label" for="recruiter_activity_area"><?= lang("PROF_SEARCH_PRIORITY_RESUME_ACTIVITY_AREA") ?></label>
                            <div class="select-bx">
                                <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                                <select name="activity_area[]" id="recruiter_activity_area" class="selectpicker required " multiple="multiple" multiple <?php if (empty($activity_area)) { ?> disabled="disabled" title="<?= lang('PROF_REGISTER_NO_ACTIVITY_AREA_SET'); ?>"  <?php } else { ?> data-selected-text-format="count > 1" data-live-search="true" multiple title="<?= lang('PROF_REGISTER_SOCIAL_THEME'); ?>" <?php } ?> >
                                    <?php
                                    if (!empty($activity_area)) :
                                        foreach ($activity_area as $value) :
                                            if (in_array($value['wa_id'], explode(',', $user_data['cd_activity_area']))):
                                                ?>
                                                <option selected="selected" value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                            <?php else: ?>
                                                <option value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                            <?php
                                            endif;
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>                        
                </div>
                <div class="inpot-bx candidate-info">
                    <div class="edit-profile-btn">
                        <button type="submit" class="round-btn application-btn-bg btn btn-block btn-social btn-profile">
                            <span class="application-btn-span-bg">
                                <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/edit.png">
                            </span>
                            <?= lang("PROF_SEARCH_PRIORITY_RESUME_MODIFY") ?>
                        </button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function () {
            // change profile picture
            jQuery(".change_photo").click(function () {
                jQuery("#form_image").trigger('click');
                jQuery('#loading').show();
            });
        });
    </script>

    <script type="text/javascript" src="<?= ASSETS_PATH ?>/js/pages/prof_search_priority_resume.js"></script>