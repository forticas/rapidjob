<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="main-profle-body cv-database-main">
    <div class="blog-main-box top-section ">
        <div class="container">
            <div class="title-box">
                <label class="text-background">CV</label>
                <h3 class="green-light-font">
                    <span class="red-font"><?= lang("PROF_RESUME_DATABASE_HEADING_CV"); ?></span><?= lang("PROF_RESUME_DATABASE_HEADING"); ?>
                </h3>
                <p class="text-cv-database-alias"><?= lang("PROF_RESUME_DATABASE_SEARCH"); ?></p>
            </div>
            <?php
            echo form_open_multipart(PROFESSIONAL_PATH . '/resume_database', array("id" => "search_resume_database_form", 'method' => 'post'));
            echo form_hidden('search_cv', 'true');
            ?>
            <div class="search-box-cv">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="select-bx">
                            <label class="select-label"><?= lang("PROF_RESUME_DATABASE_SEARCH_JOB"); ?></label>
                            <select name="job_type" id="job_type" class="selectpicker required " <?php if (empty($work_job)) { ?> disabled="disabled" title="<?= lang('PROF_RESUME_DATABASE_NO_JOB_TYPE_SET'); ?>"  <?php } else { ?> data-live-search="true" title="<?= lang('COMMON_SEARCH_BY_JOB'); ?>" <?php } ?> >
                                <?php
                                if (!empty($work_job)) :
                                    foreach ($work_job as $value) :
                                        if ($value['wj_id'] == $search_by_job_type) :
                                            ?>
                                            <option selected="selected" value="<?= $value['wj_id']; ?>"><?= $value['job_name']; ?></option>
                                        <?php else: ?>
                                            <option value="<?= $value['wj_id']; ?>"><?= $value['job_name']; ?></option>
                                        <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="select-bx">
                            <label class="select-label"><?= lang("PROF_RESUME_DATABASE_SEARCH_AREA"); ?></label>
                            <select data-actions-box="true" name="activity_area[]" id="activity_area" class="selectpicker required " multiple="multiple" multiple <?php if (empty($activity_area)) { ?> disabled="disabled" title="<?= lang('PROF_RESUME_DATABASE_NO_ACTIVITY_AREA_SET'); ?>"  <?php } else { ?> data-selected-text-format="count > 1" data-live-search="true" multiple title="<?= lang('COMMON_SEARCH_BY_ACTIVITY_AREA'); ?>" <?php } ?> >
                                <?php
                                if (!empty($activity_area)) :
                                    foreach ($activity_area as $value) :
                                        if (!empty($search_by_activity_area) && in_array($value['wa_id'], $search_by_activity_area)):
                                            ?>
                                            <option selected="selected" value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                        <?php else: ?>
                                            <option value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                        <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-xs-12">
                        <div class="select-bx">
                            <label class="select-label"><?= lang("PROF_RESUME_DATABASE_SEARCH_LOCATION"); ?></label>
                            <select data-actions-box="true" name="work_place[]" id="work_place" class="selectpicker required " multiple="multiple" multiple <?php if (empty($work_places)) { ?> disabled="disabled" title="<?= lang('PROF_RESUME_DATABASE_NO_WORK_PLACE_SET'); ?>"  <?php } else { ?> data-selected-text-format="count > 1" data-live-search="true" multiple title="<?= lang('COMMON_SEARCH_BY_LOCATION'); ?>" <?php } ?>>
                                <?php
                                if (!empty($work_places)) :
                                    foreach ($work_places as $value) :
                                        if (!empty($search_by_work_place) && in_array($value['wp_id'], $search_by_work_place)):
                                            ?>
                                            <option selected="selected" value="<?= $value['wp_id']; ?>"><?= $value['wp_name']; ?></option>
                                        <?php else: ?>
                                            <option value="<?= $value['wp_id']; ?>"><?= $value['wp_name']; ?></option>
                                        <?php
                                        endif;
                                    endforeach;
                                endif;
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="search-cv-database-btn">
                <button class="red-bg btn btn-block btn-social round-btn" type="submit">
                    <span class="dark-red">
                        <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/search.png">
                    </span>
                    <?= lang("COMMON_SEARCH"); ?>
                </button>
            </div>

            <?php echo form_close(); ?>
            <div class="all-comment-box all-search-box">
                <?php
                if (!empty($user_application) && count($user_application) > 0) :
                    ?>
                    <label class="search-bx-label"><?= $total_count; ?> <?= lang("PROF_RESUME_DATABASE_SEARCH_MATCH"); ?></label>
                    <?php
                    foreach ($user_application as $key => $value) :
                        $user_image = UPLOAD_FILE_FOLDER . '/' . UPLOAD_DOCUMENTS_FOLDER . '/' . $value['udf_id'] . '/' . $value['udf_cover_latter_file'];
                        if (file_exists($user_image)) :
                            $image = BASE_URL . UPLOAD_FILE_FOLDER . '/' . UPLOAD_DOCUMENTS_FOLDER . '/' . $value['udf_id'] . '/' . $value['udf_cover_latter_file'];
                        else:
                            $image = BASE_URL . UPLOAD_FILE_FOLDER . '/' . DEFAULT_COVER_LATTER_IMAGE_NAME;
                        endif;
                        $skill_data = "";

                        if (!empty($value['candidate_skills'])) :
                            $candidate_skills = str_replace("},{", ",", $value['candidate_skills']);

                            $skill_list = json_decode($candidate_skills, true);
                            if (!empty($skill_list)) :
                                $skill_data = implode(", ", array_keys($skill_list));
                            endif;
                        endif;


                        $current_designation = lang("COMMON_JOB_NOT_DEFINED");
                        if (
                                !empty($value['user_experience']) &&
                                isset($value['user_experience'][0]) &&
                                isset($value['user_experience'][0]['wj_name_' . $this->current_lang]) &&
                                !empty($value['user_experience'][0]['wj_name_' . $this->current_lang])) :
                            $current_designation = $value['user_experience'][0]['wj_name_' . $this->current_lang];
                        elseif (isset($value['wj_name_' . $this->current_lang]) && !empty($value['wj_name_' . $this->current_lang])):
                            $current_designation = $value['wj_name_' . $this->current_lang];
                        else:
                            $current_designation = lang("COMMON_EXPERIENCE_NOT_DEFINED");
                        endif;
                        ?>
                        <div class="comment-bx">
                            <div class="img-bx left-image-box">
                                <img src="<?= $image; ?>" alt="" class="img-responsive">
                            </div>
                            <div class="comment">
                                <div class="comment-text">
                                    <h4 class="cv-title-database">
                                        <a href="<?= PROFESSIONAL_PATH ?>/resume_details/<?= $value['candidate_id']; ?>"><?= $current_designation ?></a>
                                    </h4>
                                    <div class="text">
                                        <?php
                                        if (
                                                !empty($value['user_experience']) &&
                                                isset($value['user_experience'][0]) &&
                                                isset($value['user_experience'][0]['wj_name_' . $this->current_lang]) &&
                                                !empty($value['user_experience'][0]['wj_name_' . $this->current_lang])) :
                                            foreach ($value['user_experience'] as $user_experience) :
                                                ?>
                                                <label class="text-uppercase"><?= !empty($user_experience['year_of_experience']) && $user_experience['year_of_experience'] > 0 ? number_format($user_experience['year_of_experience'], 1, '.', ' ') : 0 ?> <?= lang("PROF_RESUME_DATABASE_WORK_EXPERIENCE"); ?> - </label>
                                                <label class="cv-designation cv-designation-small text-uppercase"><?= $user_experience['wj_name_' . $this->current_lang]; ?></label>
                                                <div class="clearfix"></div>
                                            <?php endforeach; ?>
                                        <?php else: ?>
                                            <label class="text-uppercase"><?= $current_designation; ?></label>
                                        <?php endif; ?>

                                        <p><?= $skill_data ?></p>
                                        <label class="cv-availability-date"><?= lang("PROF_RESUME_DATABASE_AVAILABLE"); ?> <?= date('d-m-Y', $value['ua_created_date']); ?></label>
                                        <div class="cv-database-icon-bx rating-bx">
                                            <div class="icon-bx">
                                                <?php
                                                if ($value['udf_type'] == 1):
                                                    if (isset($value['udf_cv_file_name']) && !empty($value['udf_cv_file_name'])):
                                                        $cv_file = UPLOAD_FILE_FOLDER . '/' . UPLOAD_DOCUMENTS_FOLDER . '/' . $value['udf_id'] . '/' . $value['udf_cv_file_name'];
                                                        if (file_exists($cv_file)):
                                                            $cv_file = UPLOAD_ABS_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $value['udf_id'] . '/' . $value['udf_cv_file_name'];
                                                            ?>
                                                            <a target="_blank" download="<?= $cv_file; ?>" href="<?= $cv_file; ?>" class="green-tooltip">
                                                                <img src="<?= ASSETS_PATH ?>images/download.png" alt="" class="img-responsive img-circle">
                                                                <div class="tooltip-bx1"><img src="<?= ASSETS_PATH ?>images/info_icon.png" alt="" class=""> <?= lang("COMMON_DOWNLOAD_CV"); ?><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                                                            </a>
                                                            <?php
                                                        endif;
                                                    endif;
                                                elseif ($value['udf_type'] == 2):
                                                    if (isset($value['udf_rapidjob_cv_file_name']) && !empty($value['udf_rapidjob_cv_file_name'])):
                                                        $cv_file = UPLOAD_FILE_FOLDER . '/' . UPLOAD_DOCUMENTS_FOLDER . '/' . $value['udf_id'] . '/' . $value['udf_rapidjob_cv_file_name'];
                                                        if (file_exists($cv_file)):
                                                            $cv_file = UPLOAD_ABS_PATH . UPLOAD_DOCUMENTS_FOLDER . '/' . $value['udf_id'] . '/' . $value['udf_rapidjob_cv_file_name'];
                                                            ?>
                                                            <a target="_blank" download="<?= $cv_file; ?>" href="<?= $cv_file; ?>" class="green-tooltip">
                                                                <img src="<?= ASSETS_PATH ?>images/download.png" alt="" class="img-responsive img-circle">
                                                                <div class="tooltip-bx1"><img src="<?= ASSETS_PATH ?>images/info_icon.png" alt="" class=""> <?= lang("COMMON_DOWNLOAD_CV"); ?><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                                                            </a>
                                                            <?php
                                                        endif;
                                                    endif;
                                                endif;
                                                ?>
                                                <?php if (!empty($value['user_favourite']) && $value['user_favourite'] > 0) : ?>
                                                    <a href="javascript:void(0);" class="gree-rating remove_from_favourite" candidate_id="<?= trim($value['candidate_id']); ?>">
                                                        <img src="<?= ASSETS_PATH ?>images/fav_selected.png" alt="" class="img-responsive img-circle selected_image_<?= $value['candidate_id'] ?>">
                                                        <div class="tooltip-bx2"><img src="<?= ASSETS_PATH ?>images/info_icon.png" alt="" class=""> <span class="add_remove_favourite_text"><?= lang("COMMON_FAVOURITE_REMOVE"); ?></span><i class="fa fa-caret-down" aria-hidden="true"></i></div>
                                                    </a>
                                                <?php else: ?>
                                                    <a href="javascript:void(0);" class="gree-rating add_to_favourite" candidate_id="<?= trim($value['candidate_id']); ?>">
                                                        <img src="<?= ASSETS_PATH ?>images/fav_unselected.png" alt="" class="img-responsive img-circle selected_image_<?= $value['candidate_id'] ?>">
                                                        <div class="tooltip-bx2"><img src="<?= ASSETS_PATH ?>images/info_icon.png" alt="" class=""> <span class="add_remove_favourite_text"><?= lang("COMMON_ADD_TO_FAVOURITE"); ?></span><i class="fa fa-caret-down caret_down" aria-hidden="true"></i></div>
                                                    </a>
                                                <?php endif; ?>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    endforeach;

                    if ($total_count > DEFAULT_RESUME_LIMIT && $next_page < $total_pages) :
                        echo form_open_multipart(PROFESSIONAL_PATH . '/resume_database', array("id" => "search_resume_database_form", 'method' => 'post'));

                        echo form_input(array(
                            'name' => 'job_type',
                            'class' => 'hidden set_job_type',
                            'type' => 'hidden',
                            'value' => $search_by_job_type)
                        );
                        echo form_input(array(
                            'name' => 'activity_area[]',
                            'class' => 'hidden set_activity_area',
                            'type' => 'hidden',
                            'value' => !empty($search_by_activity_area) ? implode(',', $search_by_activity_area) : '')
                        );
                        echo form_input(array(
                            'name' => 'work_place[]',
                            'class' => 'hidden set_work_place',
                            'type' => 'hidden',
                            'value' => !empty($search_by_work_place) ? implode(',', $search_by_work_place) : '')
                        );
                        echo form_hidden('request_page', $next_page);
                        echo form_hidden('total_pages', $total_pages);
                        echo form_hidden('view_more', 'true');
                        ?>
                        <div class="row">
                            <div class="popup-main-btn col-lg-12 col-md-12 col-sm-12 col-xs-12 blog-box-btn">
                                <button class="see-more-cv-database red-bg btn btn-block btn-social btn-register grey-light round-btn" type="submit">
                                    <span class="grey-dark">
                                        <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/eye.png">
                                    </span>
                                    <?= lang("COMMON_MORE"); ?>
                                </button>
                            </div>
                        </div>
                        <?php
                        echo form_close();
                    endif;
                else:
                    ?>
                    <label class="search-bx-label_empty">0 <?= lang("PROF_RESUME_DATABASE_SEARCH_MATCH"); ?></label>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
<script>
    var coomon_select_all = "<?= lang("COMMON_SELECT_ALL") ?>";
    var coomon_deselect_all = "<?= lang("COMMON_DESELECT_ALL") ?>";
    var remove_favourite_text = "<?= lang("COMMON_FAVOURITE_REMOVE"); ?>";
    var add_favourite_text = "<?= lang("COMMON_ADD_TO_FAVOURITE"); ?>";
    var favourite_image = "<?= ASSETS_PATH ?>images/fav_selected.png";
    var unfavourite_image = "<?= ASSETS_PATH ?>images/fav_unselected.png";
</script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/pages/prof_resume_database.js"></script>