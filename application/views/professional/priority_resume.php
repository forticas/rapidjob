<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="build-cv top-section receive-cv-priority">
    <div class="container">
        <div class="title-box">
            <label>R</label>
            <h3 class="green-light-font">
                <?= lang("PROF_PRIORITY_RESUME_HEADING") ?> <span class="red-font"><?= lang("PROF_PRIORITY_RESUME_HEADING_2") ?></span>
            </h3>
        </div>
        <div class="row">
            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
                <p><?= lang("PROF_PRIORITY_RESUME_DESCRIPTION") ?></p>  
            </div>
            <div class="clearfix"></div>
            <div class="row social_div_box">
                <?php if ($user_data['cd_is_prime'] == 1 && $user_data['cd_plan_expiry_date'] > $this->utc_time): ?>
                    <p class="cv-content"><?= lang("PROF_MY_PROFILE_PREMIUM") ?><br/><?= lang("PROF_MY_PROFILE_PREMIUM_PLAN_EXPIRE_DATE"); ?> <span class="convert_time text-success"><?= $user_data['cd_plan_expiry_date']; ?></span></h3></p>
                <?php elseif ($user_data['cd_is_prime'] == 1 && $user_data['cd_plan_expiry_date'] < $this->utc_time): ?>
                    <p class="cv-content cv-content-expire"><?= lang("PROF_MY_PROFILE_PREMIUM_PLAN_EXPIRE") ?></p>
                <?php else: ?>
                    <p class="cv-content"><?= lang("COMMON_PRICES") ?> : <?= $recruiter_subscription_plan_price ?> €</p>
                <?php endif; ?>
                <?php if ($user_data['cd_is_prime'] == 1 && !empty($user_data['cd_plan_expiry_date']) && $user_data['cd_plan_expiry_date'] > $this->utc_time): ?>
                    <div class="col-lg-5 col-md-6 col-sm-6 col-xs-12 col-lg-offset-4 col-md-offset-4 col-sm-offset-3 col-xs-offset-3 social_div_box_btn">
                        <a class="round-btn red-bg btn btn-block btn-social btn-profile" href="<?= PROFESSIONAL_PATH ?>/search_priority_resume">
                            <span class="dark-red">
                                <img src="<?= ASSETS_PATH ?>images/forward.png" alt="" class="img-responsive">
                            </span>
                            <?= lang("COMMON_SUBSCRIBE_SERVICE") ?>
                        </a>
                    </div>
                <?php else: ?>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding-right">
                        <?php echo form_open(PROFESSIONAL_PATH . '/form_payment', array("method" => "POST")); ?>
                        <input type="hidden" name="lang" value="<?php echo $this->current_lang ?>">
                        <button class="validationButton pull-right" type="submit">
                            <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/systempay.png">
                        </button>
                        <?php echo form_close(); ?>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 no-padding-left">
                        <?php echo form_open(PROFESSIONAL_PATH . '/form_paypal', array("method" => "POST", 'id' => "form_paypal")); ?>
                        <input type="hidden" name="lang" value="<?php echo $this->current_lang ?>">
                        <button class="validationButton" type="submit">
                            <img class="img-responsive paypal_image" alt="" src="<?= ASSETS_PATH ?>images/paypal.png">
                        </button>
                        <?php echo form_close(); ?>
                    </div>
                <?php endif; ?>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".convert_time").each(function () {
            var timestemp = parseInt(jQuery(this).text());
            var date = formatDateLocal("<?= DATE_FORMAT_JS ?>", timestemp * 1000, false, current_lang);
            jQuery(this).text(date);
        });
    });
</script>