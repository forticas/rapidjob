<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="build-cv top-section application-table">
    <div class="container">
        <div class="title-box">
            <label class="small-text">O</label>
            <h3 class="green-light-font"><?= lang("COMMON_MES"); ?> <span class="red-font"><?= lang("MY_ORDERS"); ?></span></h3>
        </div>
        <?php if (!empty($order_data)) : ?>
            <div class="table-content table-responsive">
                <table class="table table-inverse">
                    <thead>
                        <tr>
                            <th><?= lang("MY_ORDERS_CREATED_DATE") ?></th>
                            <th><?= lang("COMMON_DURATION") ?></th>
                            <th><?= lang("MY_ORDERS_PAID_AMOUNT") ?></th>
                            <th><?= lang("MY_ORDERS_PAYMENT_BY") ?></th>
                            <th><?= lang("MY_ORDERS_STATUS") ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        foreach ($order_data as $value):
                            ?>
                            <tr>
                                <td class="convert_time"><?= $value['up_created_date'] ?></td>
                                <td><?= !empty($value['up_subscription_number']) ? trim($value['up_subscription_number']) : 0 ?> <?= lang("COMMON_MONTH"); ?></td>
                                <td><?= !empty($value['up_amount']) ? trim($value['up_amount']) : 0 ?> </td>
                                <td>
                                    <?php
                                    if ($value['up_payment_by'] == 1):
                                        echo lang("MY_ORDERS_PAYMENT_BY_SYSTEMPAY");
                                    elseif ($value['up_payment_by'] == 2):
                                        echo lang("MY_ORDERS_PAYMENT_BY_PAYPAL");
                                    else:
                                        echo " - - - ";
                                    endif;
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if ($cd_is_prime == 1 && !empty($cd_plan_expiry_date) && $cd_plan_expiry_date < $this->utc_time):
                                        echo "<span class='text-danger'>" . lang("COMMON_PLAN_EXPIRE") . "</span>";
                                    elseif ($value['up_status'] == 1):
                                        echo "<span class='text-success'>" . lang("COMMON_ACTIVE") . "</span>";
                                    elseif ($value['up_status'] == 2):
                                        echo "<span class='text-warning'>" . lang("COMMON_INACTIVE") . "</span>";
                                    elseif ($value['up_status'] == 3):
                                        echo "<span class='text-success'>" . lang("COMMON_PENDING") . "</span>";
                                    elseif ($value['up_status'] == 4):
                                        echo "<span class='text-danger'>" . lang("COMMON_CANCELED") . "</span>";
                                    else:
                                        echo " - - - ";
                                    endif;
                                    ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        <?php else: ?>
            <h3 class="no_app_found"><?= lang('MY_ORDERS_EMPTY'); ?></h3>
        <?php endif; ?>
    </div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function () {
        jQuery(".convert_time").each(function () {
            var timestemp = parseInt(jQuery(this).text());
            var date = formatDateLocal("<?= DATE_FORMAT_JS_LIST ?>", timestemp * 1000, false, current_lang);
            jQuery(this).text(date);
        });
    });
</script>