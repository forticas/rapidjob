<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="registration">
    <div class="slider-content">
        <div class="banner-image">
            <img src="<?= $banner_image; ?>" />
            <div class="banner-image-text-div" >
                <div class="banner-image-text">
                    <h3>
                        <blockquote> <?= lang("PROF_REGISTER_TITLE_RECRUIT"); ?> <span class="red-font"> <?= lang("PROF_REGISTER_TITLE_QUICKLY"); ?> </span> <?= lang("COMMON_AND"); ?> <span class="green-light-font"><?= lang("PROF_REGISTER_TITLE_FREE"); ?></span></blockquote>
                    </h3>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="hm-scroll-down-bx">
    <div class="container">
        <div class="mause-img-bx thumbnail">
            <div class="center-bdr red-bg"></div>
            <a href="javascript:void(0)" id="mouse_icon" class="down-scroll-btn"><img src="<?= ASSETS_PATH ?>images/mouse_icon.png" alt="" class="img-responsive"></a>
        </div>
    </div>
</div>
<div id="next-page" class="registration-main">
    <div class="main-registration-body ">
        <div class="blog-main-box top-section ">
            <div class="container">
                <div class="title-box">
                    <label class="text-background">A</label>
                    <h3 class="green-light-font">
                        <?= lang("PROF_REGISTER_ACCESS_TO"); ?> <span class="red-font"><?= lang("PROF_REGISTER_CV_DATABASE"); ?></span>
                    </h3>
                    <p class="text-cv-database-alias">
                        <?= get_slash_formatted_text($cms_content['prof_cv_title'])?>
                    </p>
                </div>
                <div class="title-box broadcast-list">
                    <label class="text-background">E</label>
                    <h3 class="green-light-font">
                        <?= lang("PROF_REGISTER_REGISTER_YOUR"); ?> <span class="red-font"><?= lang("COMMON_BUSINESS_NAME"); ?> </span><?= lang("PROF_REGISTER_IN_OUR"); ?> <span class="red-font"><?= lang("PROF_REGISTER_BROADCAST_LIST"); ?> </span>
                    </h3>
                    <p class="text-cv-database-alias">
                        <?= get_slash_formatted_text($cms_content['prof_broadcast'])?>
                    </p>
                </div>
                <div class="title-box">
                    <label class="text-background">P</label>
                    <h3 class="green-light-font">
                        <?= lang("PROF_REGISTER_GOTO"); ?> <span class="red-font"><?= lang("PROF_REGISTER_PREMIUM_MODE"); ?></span>
                    </h3>
                    <p class="text-cv-database-alias">
                        <?= get_slash_formatted_text($cms_content['prof_mode']) ?>
                    </p>
                </div>

                <fieldset class="title-box title_prof_registration_form">
                    <legend align="center">
                        <label class="text-background">I</label>
                        <h3 class="green-light-font">
                            <?= lang("PROF_REGISTER_SUBSCRIBE"); ?> <span class="red-font"><?= lang("PROF_REGISTER_FREE"); ?></span>
                        </h3>
                    </legend>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 prof_registration_form">
                        <form action="javascript:void(0)" id="profession_registration_form" class="registration-form" method="POST" enctype="multipart/form-data">
                            <div class="row">
                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <div class="select-bx select-civility">
                                            <select name="civility" id="pr_civility" class="selectpicker required" title="<?= lang('COMMON_CIVILITY'); ?>">
                                                <option value="2"><?= lang('COMMON_MADAM') ?></option>
                                                <option value="1"><?= lang('COMMON_SIR') ?></option>
                                            </select>
                                        </div>
                                    </div>                             
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input placeholder="<?= lang("SIGNUP_FORM_FIRST_NAME"); ?>*" class="form-control required" type="text" id="pr_firstname" name="first_name" data-rule-minlength="2" data-rule-maxlength="25">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input placeholder="<?= lang("SIGNUP_FORM_LAST_NAME"); ?>*" class="form-control required" type="text" id="pr_lastname" name="last_name" data-rule-minlength="2" data-rule-maxlength="25">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <input placeholder="<?= lang("PROF_REGISTER_SOCIAL_REASON_OF_COMPANY"); ?>*" class="form-control required" type="text" id="pr_social_reason" name="social_reason" data-rule-minlength="2" data-rule-maxlength="150">
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                            <div class="form-group">
                                                <div class="select-bx">
                                                    <select name="activity_area[]" id="activity_area" class="selectpicker required show-menu-arrow" multiple="multiple" multiple <?php if (empty($activity_area)) { ?> disabled="disabled" title="<?= lang('PROF_REGISTER_NO_ACTIVITY_AREA_SET'); ?>"  <?php } else { ?> data-selected-text-format="count > 1" data-live-search="true" multiple title="<?= lang('PROF_REGISTER_SOCIAL_THEME'); ?>" <?php } ?> >
                                                        <?php
                                                        if (!empty($activity_area)) :
                                                            foreach ($activity_area as $value) :
                                                                ?>
                                                                <option value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                                                <?php
                                                            endforeach;
                                                        endif;
                                                        ?>
                                                    </select>
                                                </div> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <input placeholder="<?= lang("SIGNUP_FORM_EMAIL"); ?>*" class="form-control required" type="text" id="pr_email" name="email" data-rule-maxlength="255">
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <input placeholder="<?= lang("SIGNUP_FORM_CONFIRM_EMAIL"); ?>*" class="form-control required" type="text" id="pr_cemail" name="cemail" data-rule-maxlength="255">
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                            <input type="password" style="display: none;">
                            <div class="row">
                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <input placeholder="<?= lang("SIGNUP_FORM_PASSWORD"); ?>*" class="form-control required" type="password" id="pr_password" name="password" data-rule-pwcheck='true' data-msg-pwcheck="<?= lang("REGISTER_PASSWORD_VALIDATION_MESSAGE"); ?>" data-rule-minlength="6" data-rule-maxlength="25">
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-8 col-md-12 col-sm-12 col-xs-12">
                                    <div class="row">
                                        <div class="col-lg-12 col-md-6 col-sm-12 col-xs-12">
                                            <div class="form-group">
                                                <input placeholder="<?= lang("COMMON_CONFIRM_PASSWORD"); ?>*" class="form-control required" type="password" name="cpassword" id="pr_cpassword" data-rule-minlength="6" data-rule-maxlength="25">
                                            </div>
                                        </div>                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12">
                                    <div class="registration-btn">
                                        <button class="registration-btn-a red-bg btn btn-block btn-social round-btn" type="submit">
                                            <span class="dark-red">
                                                <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/edit.png">
                                            </span>
                                            <?= lang("SIGNUP_FORM_BUTTON"); ?>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>
</div>
<div id="top_arrow_img" style="position: fixed;bottom: 30px;right: 15px;display: none">
    <a href="javascript:void(0)" id="go_top">
        <img src="<?= ASSETS_PATH ?>images/arrow-up.png" height="50">
    </a>
</div>
<!--registration and signin froms end -->

<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/sha1.min.js"></script>
<script type="text/javascript">
    jQuery.sha1 = sha1;</script>
<script type="text/javascript">
    var confirmation_email = "<?= lang('CONFIRMATION_EMAIL') ?>";
    var letter_only = "<?= lang('LETTER_ONLY') ?>";

    window.fbAsyncInit = function () {
        FB.init({
            appId: '<?= FB_APP_ID ?>',
            xfbml: true,
            version: 'v2.8'
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script type="text/javascript" src="//platform.linkedin.com/in.js">
    api_key: 78ev6kvpru6mm2
            onLoad: onLinkedInLoad
</script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/pages/prof_register.js"></script>