<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="build-cv top-section recruiter-my-profile">
    <div class="container">
        <div class="title-box">
            <label>E</label>
            <h3 class="green-light-font">
                <?= lang("COMMON_MY"); ?> <span class="red-font"><?= lang("PROF_MY_PROFILE_COMPANY_PROFILE"); ?></span>
            </h3>
        </div>

        <div class="premium-logo">
            <img src="<?= ASSETS_PATH ?>images/premium_version_icon.png" alt="" class="img-responsive">
            <?php if ($cd_is_prime == 1 && !empty($cd_plan_expiry_date) && $cd_plan_expiry_date > $this->utc_time): ?>
                <label class="premium-member-lbl"><h3><?= lang("PROF_MY_PROFILE_PREMIUM"); ?> <br/><?= lang("PROF_MY_PROFILE_PREMIUM_PLAN_EXPIRE_DATE"); ?> <span class="convert_time text-success"><?= $cd_plan_expiry_date ?></span></h3></label>
            <?php elseif ($cd_is_prime == 1 && !empty($cd_plan_expiry_date) && $cd_plan_expiry_date < $this->utc_time): ?>
                <label class="premium-member-lbl"><h3><?= lang("PROF_MY_PROFILE_PREMIUM_PLAN_EXPIRE"); ?> </h3></label>
            <?php else: ?>
                <label class="premium-member-lbl"><h3><?= vsprintf(lang("PROF_MY_PROFILE_PREMIUM_MEMBER"), $recruiter_subscription_plan_price); ?> </h3></label>
            <?php endif; ?>
        </div>

        <div>
            <div class="edit-profile-bx profile-content">

                <div class="hide">
                    <?php
                    echo form_open_multipart(PROFESSIONAL_PATH . '/upload_image', array("id" => "photo_form"));
                    ?>
                    <input type="file" onchange="this.form.submit()" accept="image/*" required="required" name="image" id="form_image">
                    <?php
                    echo form_close();
                    ?>
                </div>

                <?php echo form_open_multipart(PROFESSIONAL_PATH, array("id" => "edit_profile_form")); ?>
                <input type="hidden" class="hidden" id="cd_nationality" name="nationality" value="<?php !empty($cd_nationality) ? trim($cd_nationality) : ''; ?>">
                <div class="row image-right-left-content">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 profile-pic-content">
                        <?php if ($cd_is_prime == 1 && $cd_plan_expiry_date > $this->utc_time) { ?>
                            <div class="bg-img">
                                <div class="img-box">
                                    <div class="profile-img">
                                        <img alt="profile photo" class="img-responsive img-circle" height="180px" width="180px" src="<?= IMAGE_MANIPULATION_URL . "url=" . $this->session->userdata("profile_photo") . "&h=180&w=180&default=true" ?>" >
                                    </div>
                                </div>
                                <?php
                                $image = $this->session->userdata("profile_photo");
                                if (!empty($image)) :
                                    $image = UPLOAD_FILE_FOLDER . '/' . $this->session->userdata("profile_photo");
                                else:
                                    $image = UPLOAD_FILE_FOLDER . '/' . DEFAULT_IMAGE_NAME;
                                endif;
                                ?>
                                <p class="p-text text-center">
                                    <a target="_blank" download="<?= $image; ?>" href="<?= $image; ?>">
                                        <?= lang('SHOW_PROFILE_PAGE_DOWNLOAD_PROFILE_PICTURE') ?>
                                    </a>
                                </p>
                                <div class="item-btn-bx">
                                    <div class="btn-bx">
                                        <a class="round-btn dark-blue-bg btn btn-block btn-social btn-profile change_photo" href="javascript:void(0);">
                                            <span class="dark-blue-bg">
                                                <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/selector.png">
                                            </span>
                                            <?= lang('SHOW_PROFILE_PAGE_UPLOAD_PROFILE_PICTURE') ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        <?php } else { ?>
                            <div class="bg-img">
                                <img src="<?= ASSETS_PATH ?>images/profile_placeholder.png" alt="" class="img-responsive profile-pic-re">
                                <p class="profile-pic-text"> <?= lang("PROF_MY_PROFILE_COMPANY_IMPORT_LOGO_TEXT"); ?></p>
                                <div class="item-btn-bx">
                                    <div class="btn-bx">
                                        <a class="round-btn dark-blue-bg btn btn-block btn-social btn-profile" href="<?= PROFESSIONAL_PATH ?>/priority_resume">
                                            <span class="dark-blue-bg">
                                                <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/selector.png">
                                            </span>
                                            <?= lang("COMMON_SUBSCRIBE"); ?>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 profile-pic-content">
                        <div class="inpot-bx">
                            <label class="select-label" for="pr_email"> <?= lang("PROF_MY_PROFILE_LOGIN_EMAIL"); ?><span class="text-danger">*</span></label>
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <div class="form-group input-margin">
                                        <input id="pr_email" type="text" name="email" placeholder="<?= lang("PROF_MY_PROFILE_LOGIN_EMAIL"); ?>"  class="form-control" value="<?php echo set_value('email') ? set_value('email') : $u_email; ?>">
                                        <?php if (!empty(form_error('email'))) : ?><div for="pr_email" class="text-danger"><?= form_error('email'); ?></div><?php endif; ?>
                                        <?php if (!empty(form_error('exist'))) : ?><div for="pr_email" class="text-danger"><?= form_error('exist'); ?></div><?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="inpot-bx">
                            <div class="row">
                                <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                                    <label class="select-label" for="pr_civility"><?= lang("PROF_MY_PROFILE_CIVIL_STATUS"); ?><span class="text-danger">*</span></label>
                                    <div class="select-bx">
                                        <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                                        <select name="civility" id="pr_civility" class="selectpicker required" title="<?= lang('COMMON_CIVILITY'); ?>">
                                            <option value="2" <?php if ($cd_gender == 2) echo 'selected="selected"'; ?>><?= lang('COMMON_MADAM') ?></option>
                                            <option value="1" <?php if ($cd_gender == 1) echo 'selected="selected"'; ?>><?= lang('COMMON_SIR') ?></option>
                                        </select>
                                        <?php if (!empty(form_error('civility'))) : ?><div for="pr_civility" class="text-danger"><?= form_error('civility'); ?></div><?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="inpot-bx">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <label class="select-label" for="pr_firstname"><?= lang("SIGNUP_FORM_FIRST_NAME"); ?><span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input placeholder="<?= lang("SIGNUP_FORM_FIRST_NAME"); ?>" class="form-control required" type="text" id="pr_firstname" name="first_name" value="<?php echo set_value('first_name') ? set_value('first_name') : $u_first_name; ?>">
                                <?php if (!empty(form_error('first_name'))) : ?><div for="pr_firstname" class="text-danger"><?= form_error('first_name'); ?></div><?php endif; ?>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <label class="select-label" for="pr_lastname"><?= lang("SIGNUP_FORM_LAST_NAME"); ?><span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input placeholder="<?= lang("SIGNUP_FORM_LAST_NAME"); ?>" class="form-control required" type="text" id="pr_lastname" name="last_name" value="<?php echo set_value('last_name') ? set_value('last_name') : $u_last_name; ?>">
                                <?php if (!empty(form_error('last_name'))) : ?><div for="pr_lastname" class="text-danger"><?= form_error('last_name'); ?></div><?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="inpot-bx">
                    <div class="row">                            
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <label class="select-label" for="pr_phone"><?= lang("COMMON_PHONE"); ?><span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input placeholder="<?= lang("COMMON_PHONE"); ?>" class="form-control required" type="text" id="pr_phone" name="phone" value="<?php echo set_value('phone') ? set_value('phone') : $cd_phone; ?>">
                                <?php if (!empty(form_error('phone'))) : ?><div for="pr_phone" class="text-danger"><?= form_error('phone'); ?></div><?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="inpot-bx">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <label class="select-label" for="pr_social_reason"><?= lang("PROF_REGISTER_SOCIAL_REASON_OF_COMPANY"); ?><span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input placeholder="<?= lang("PROF_REGISTER_SOCIAL_REASON_OF_COMPANY"); ?>" class="form-control required" type="text" id="pr_social_reason" name="social_reason" value="<?php echo set_value('social_reason') ? set_value('social_reason') : $cd_social_reason; ?>"> 
                                <?php if (!empty(form_error('social_reason'))) : ?><div for="pr_social_reason" class="text-danger"><?= form_error('social_reason'); ?></div><?php endif; ?>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <label class="select-label" for="activity_area"><?= lang('PROF_MY_PROFILE_ACTIVITY_AREA'); ?><span class="text-danger">*</span></label>
                            <div class="select-bx">
                                <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                                <select name="activity_area[]" id="activity_area" class="selectpicker required " multiple="multiple" multiple <?php if (empty($activity_area)) { ?> disabled="disabled" title="<?= lang('PROF_REGISTER_NO_ACTIVITY_AREA_SET'); ?>"  <?php } else { ?> data-selected-text-format="count > 1" data-live-search="true" multiple title="<?= lang('PROF_REGISTER_SOCIAL_THEME'); ?>" <?php } ?> >
                                    <?php
                                    if (!empty($activity_area)) :
                                        foreach ($activity_area as $value) :
                                            if (in_array($value['wa_id'], explode(',', $cd_activity_area))) :
                                                ?>
                                                <option selected="selected" value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                            <?php else: ?>
                                                <option value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                            <?php
                                            endif;
                                        endforeach;
                                    endif;
                                    ?>
                                </select>
                                <?php if (!empty(form_error('activity_area[]'))) : ?><div for="activity_area" class="text-danger"><?= form_error('activity_area[]'); ?></div><?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="inpot-bx">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <label class="select-label" for="autocomplete"><?= lang('PROF_MY_PROFILE_COMPANY_ADDRESS'); ?><span class="text-danger">*</span></label>
                            <div class="form-group">
                                <input type="text" placeholder="<?= lang('COMMON_ADDRESS'); ?>" class="form-control" onFocus="geolocate()" id="autocomplete" name="address" value="<?php echo set_value('address') ? set_value('address') : $cd_address; ?>">
                                <?php if (!empty(form_error('address'))) : ?><div for="autocomplete" class="text-danger"><?= form_error('address'); ?></div><?php endif; ?>
                            </div>
                        </div>                            
                    </div>
                </div>

                <div class="inpot-bx">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">                                
                            <div class="form-group">
                                <input type="text" placeholder="<?= lang('COMMON_CITY'); ?>*"  class="form-control" id="locality" name="city" value="<?php echo set_value('locality') ? set_value('locality') : !empty($cd_city) ? trim($cd_city) : ''; ?>">
                                <?php if (!empty(form_error('locality'))) : ?><div for="locality" class="text-danger"><?= form_error('locality'); ?></div><?php endif; ?>
                            </div>
                        </div>     
                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">                               
                            <div class="form-group">
                                <input type="text" placeholder="<?= lang('COMMON_POSTAL_CODE'); ?>*"  class="form-control" id="postal_code" name="postal_code" value="<?php echo set_value('postal_code') ? set_value('postal_code') : !empty($cd_postal_code) ? trim($cd_postal_code) : ''; ?>">
                                <?php if (!empty(form_error('postal_code'))) : ?><div for="postal_code" class="text-danger"><?= form_error('postal_code'); ?></div><?php endif; ?>
                            </div>
                        </div>  

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">                                
                            <div class="form-group">
                                <div class="select-bx">
                                    <i class="fa fa-caret-down down-caret black-font" aria-hidden="true"></i>
                                    <select name="country" id="country" class="selectpicker required " <?php if (empty($nationalities)) { ?> disabled="disabled" title="<?= lang('COMMON_COUNTRY_EMPTY'); ?>"  <?php } else { ?> data-live-search="true" title="<?= lang('COMMON_SELECT_COUNTRY'); ?>" <?php } ?> >
                                        <?php
                                        if (!empty($nationalities)) :
                                            foreach ($nationalities as $value) :
                                                if ($value['n_id'] == $cd_nationality) :
                                                    ?>
                                                    <option selected="selected" value="<?= $value['n_name']; ?>" country_id="<?= $value['n_id']; ?>"><?= $value['n_name']; ?></option>
                                                <?php else: ?>
                                                    <option value="<?= $value['n_name']; ?>" country_id="<?= $value['n_id']; ?>"><?= $value['n_name']; ?></option>
                                                <?php
                                                endif;
                                            endforeach;
                                        endif;
                                        ?>
                                    </select>
                                    <?php if (!empty(form_error('country'))) : ?><div for="country" class="text-danger"><?= form_error('country'); ?></div><?php endif; ?>
                                </div>
                            </div>
                        </div>  
                    </div>
                </div>

                <div class="inpot-bx">
                    <div class="edit-profile-btn">
                        <button class="round-btn red-bg btn btn-block btn-social btn-profile" type="submit">
                            <span class="dark-red">
                                <img src="<?= ASSETS_PATH ?>images/register.png" alt="" class="img-responsive">
                            </span>
                            <?= lang('COMMON_SAVE_CHANGES'); ?>
                        </button>
                    </div>
                </div>
                <?php echo form_close(); ?>
            </div>
        </div>

        <div class="clearfix"></div>
        <?php echo form_open_multipart(PROFESSIONAL_PATH . '/change_password', 'id="change_password_form"'); ?>
        <input type="password" style="display: none;">
        <div class="edit-profile-bx change-password-section">
            <label class="red-label red-font">
                <?= lang('CHANGE_PASSWORD_HEADING'); ?>
            </label>
            <div class="inpot-bx">
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <label class="select-label"><?= lang('CHANGE_PASSWORD_CURRENT_PASSWORD'); ?><span class="text-danger">*</span></label>
                        <div class="form-group">
                            <input type="password" placeholder="" id="old_password" name="old_password" class="form-control required" value="<?php echo set_value('old_password') ? set_value('old_password') : ''; ?>" data-rule-pwcheck="true" data-msg-pwcheck="<?= lang('REGISTER_PASSWORD_VALIDATION_MESSAGE'); ?>" data-rule-minlength="6" data-rule-maxlength="25">
                        </div>
                        <?php if (!empty(form_error('old_password'))) : ?><label class="text-danger"><?= form_error('old_password'); ?></label><?php endif; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <label class="select-label"><?= lang('CHANGE_PASSWORD_NEW_PASSWORD'); ?><span class="text-danger">*</span></label>
                        <div class="form-group">
                            <input type="password" placeholder="" id="new_password" name="password" class="form-control required" value="<?php echo set_value('password') ? set_value('password') : ''; ?>" data-rule-pwcheck='true' data-msg-pwcheck="<?= lang("REGISTER_PASSWORD_VALIDATION_MESSAGE"); ?>">
                        </div>
                        <?php if (!empty(form_error('password'))) : ?><label class="text-danger"><?= form_error('password'); ?></label><?php endif; ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                        <label class="select-label"><?= lang('CHANGE_PASSWORD_CONFIRM_PASSWORD'); ?><span class="text-danger">*</span></label>
                        <div class="form-group">
                            <input type="password" placeholder="" id="cpassword" name="cpassword" class="form-control required" value="<?php echo set_value('cpassword') ? set_value('cpassword') : ''; ?>">
                        </div>
                        <?php if (!empty(form_error('cpassword'))) : ?><label class="text-danger"><?= form_error('cpassword'); ?></label><?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="inpot-bx">
                <div class="edit-profile-btn">
                    <button class="round-btn application-btn-bg btn btn-block btn-social btn-profile">
                        <span class="application-btn-span-bg">
                            <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/register.png">
                        </span>
                        <?= lang('COMMON_RECORD'); ?>
                    </button>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
        <div class="clearfix"></div>
        <div class="row professional_sub_header">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <a class="round-btn purple btn btn-block btn-social btn-profile" href="<?= PROFESSIONAL_PATH ?>/resume_database"><?= lang('PROF_HEADER_USER_SUB_MENU_ACCESS_CV') ?></a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <a class="round-btn purple btn btn-block btn-social btn-profile" href="<?= PROFESSIONAL_PATH ?>/favourite_resume"><?= lang('PROF_HEADER_USER_SUB_MENU_MY_FAVORITE_PROFILES') ?></a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <a class="round-btn purple btn btn-block btn-social btn-profile" href="<?= PROFESSIONAL_PATH ?>/priority_resume"><?= lang('PROF_HEADER_USER_SUB_MENU_RECEIVING_RESULTS') ?></a>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>

<script>

    var autocomplete = '';
    var componentForm = {
        locality: 'long_name',
        postal_code: 'short_name'
    };

    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById('autocomplete')),
                {types: ['geocode']});
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        var place = autocomplete.getPlace();
        for (var component in componentForm) {
            document.getElementById(component).value = '';
            document.getElementById(component).disabled = false;
        }
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            if (componentForm[addressType]) {
                var val = place.address_components[i][componentForm[addressType]];
                document.getElementById(addressType).value = val;
            }
        }
    }

    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    jQuery(document).ready(function () {
        // change profile picture
        jQuery(".change_photo").click(function () {
            jQuery("#form_image").trigger('click');
            jQuery('#loading').show();
        });

        jQuery(".convert_time").each(function () {
            var timestemp = parseInt(jQuery(this).text());
            var date = formatDateLocal("<?= DATE_FORMAT_JS ?>", timestemp * 1000, false, current_lang);
            jQuery(this).text(date);
        });
    });
</script>

<?php
$selected_language = !empty($this->session->userdata()['language']) ? $this->session->userdata()['language'] : DEFAULT_LANG;
if ($selected_language == "fr") {
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_API_KEY ?>&libraries=places&callback=initAutocomplete&language=fr"></script>
    <?php
} else {
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_API_KEY ?>&libraries=places&callback=initAutocomplete&language=en"></script>
    <?php
}
?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/pages/prof_edit_profile.js"></script>