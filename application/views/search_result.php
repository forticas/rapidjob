<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="blog-main-box top-section search-result-page">
    <div class="container">
        <div class="title-box">
            <label>R</label>
            <h3 class="green-light-font">
                <?= lang('SEARCH_PAGE_TITLE') ?> <span class="red-font"><?= lang('SEARCH_PAGE_TITLE_RED') ?></span>
            </h3>
            <p>
                <?= lang('SEARCH_PAGE_TITLE_TEXT') ?>
            </p>
        </div>

        <!-- SEARCH COMPANY FORM-->
        <div class="search-box">
            <?php
            echo form_open_multipart(HOME_PATH . '/search', array("id" => "search_company_form", 'method' => 'POST'));
            echo form_hidden('search_company', 'true');
            ?>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 no-padding">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 no-padding">
                    <input name="q" type="text" id="autocomplete" placeholder="<?= lang('HOME_COVERPAGE_SEARCH_PLACEHOLDER') ?>" class="form-control input-lg search-input" value="<?= $query; ?>">
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 home-search-box">
                    <div class="select-bx">
                        <select name="activity_area[]" id="activity_area" class="selectpicker " multiple="multiple" multiple <?php if (empty($activity_area)) { ?> disabled="disabled" title="<?= lang('COMMON_SEARCH_NO_ACTIVITY_AREA_SET'); ?>"  <?php } else { ?> data-selected-text-format="count > 1" data-live-search="true" multiple title="<?= lang('COMMON_SEARCH_CHOOSE_ACTIVITY_AREA'); ?>" <?php } ?> >
                            <?php
                            if (!empty($activity_area)) :
                                foreach ($activity_area as $value) :
                                    if (in_array($value['wa_id'], $search_by_activity_area)) :
                                        ?>
                                        <option selected="selected" value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                    <?php else: ?>
                                        <option value="<?= $value['wa_id']; ?>"><?= $value['area_name']; ?></option>
                                    <?php
                                    endif;
                                endforeach;
                            endif;
                            ?>
                        </select>
                    </div>                        
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12 no-padding btn-box gradiant-btn-bx">
                    <button class="btn btn-info btn-lg hm-banner-search-btn btn-gradiant" type="submit"><?= lang('SEARCH_PAGE_SEARCH_BTN') ?></button>
                </div>
            </div>
            <?= form_close(); ?>
        </div>
        <!-- END OF SEARCH COMPANY FORM-->

        <!-- COMPANY LIST-->
        <div class="all-comment-box all-search-box">
            <?php
            if (!empty($user_application) && count($user_application) > 0) :
                ?>
                <label class="search-bx-label"><?= $total_count . " " . lang("SEARCH_PAGE_SEARCH_RESULT_TITLE"); ?></label>
                <?php
                foreach ($user_application as $key => $value) :
                    $user_image = UPLOAD_FILE_FOLDER . '/' . UPLOAD_USERS_FOLDER . '/' . $value['recruiter_id'] . '/' . $value['recruiter_image'];
                    $image = BASE_URL . UPLOAD_FILE_FOLDER . '/' . DEFAULT_COMPANY_IMAGE_NAME;

                    if (file_exists($user_image)) :
                        $image = BASE_URL . UPLOAD_FILE_FOLDER . '/' . UPLOAD_USERS_FOLDER . '/' . $value['recruiter_id'] . '/' . $value['recruiter_image'];
                    endif;
                    ?>
                    <div class="comment-bx">
                        <div class="img-bx left-image-box">
                            <img src="<?= $image; ?>" alt="" class="img-responsive">
                        </div>
                        <div class="comment">
                            <div class="comment-text">
                                <h4 class="green-light-font">
                                    <?= !empty($value['recruiter_first_name']) ? get_slash_formatted_text(trim($value['recruiter_first_name'] . " " . $value['recruiter_last_name'])) : ' - - - '; ?>
                                </h4>
                                <div class="text">
                                    <label><?= lang('SEARCH_PAGE_SEARCH_RESULT_PURPOSE') ?> :</label>
                                    <p><?= !empty($value['cd_social_reason']) ? get_formatted_text($value['cd_social_reason']) : ' - - - '; ?></p>
                                    <?php
                                    if (!empty($this->user_id)) :
                                        if (!empty($value['user_favourite']) && $value['user_favourite'] > 0) :
                                            ?>
                                            <a href="javascript:void(0);" class="search_fav_icon remove_from_favourite" recruiter_id="<?= trim($value['recruiter_id']); ?>">
                                                <img src="<?= ASSETS_PATH ?>images/fav_selected.png" alt="" class="img-responsive img-circle selected_image_<?= $value['recruiter_id'] ?>">
                                            </a>
                                        <?php else: ?>
                                            <a href="javascript:void(0);" class="search_fav_icon add_to_favourite" recruiter_id="<?= trim($value['recruiter_id']); ?>">
                                                <img src="<?= ASSETS_PATH ?>images/fav_unselected.png" alt="" class="img-responsive img-circle selected_image_<?= $value['recruiter_id'] ?>">
                                            </a>
                                        <?php endif; ?>

                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                endforeach;


                if ($total_count > DEFAULT_SEARCH_COMPANY_LIMIT && $next_page < $total_pages) :
                    echo form_open(HOME_PATH . '/search', array("id" => "search_company_form", 'method' => 'POST'));

                    echo form_input(array(
                        'name' => 'activity_area[]',
                        'class' => 'hidden set_activity_area',
                        'type' => 'hidden',
                        'value' => !empty($search_by_activity_area) ? implode(',', $search_by_activity_area) : '')
                    );

                    echo form_hidden('request_page', $next_page);
                    echo form_hidden('total_pages', $total_pages);
                    echo form_hidden('view_more', 'true');
                    ?>
                    <input name="q" type="hidden" class="hidden" value="<?= $query; ?>">
                    <div class="row">
                        <div class="popup-main-btn col-lg-12 col-md-12 col-sm-12 col-xs-12 blog-box-btn">
                            <button class="red-bg btn btn-block btn-social btn-register grey-light round-btn" type="submit">
                                <span class="grey-dark">
                                    <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/eye.png">
                                </span>
                                <?= lang('COMMON_MORE') ?>
                            </button>
                        </div>
                    </div>
                    <?php
                    echo form_close();
                endif;
                ?>
            <?php else: ?>
                <label class="search-bx-label_empty">0 <?= lang("SEARCH_PAGE_NO_RESULT"); ?></label>
            <?php endif; ?>
        </div>
        <!-- END OF COMPANY LIST-->
    </div>

    <!-- SEND APPLICATION PAGE-->
    <div class="hm-about-text">
        <div class="container">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                <div class="text-bx">
                    <div class="img-bx">
                        <div class="left-box">01</div>
                        <div class="right-box">
                            <img src="<?= ASSETS_PATH ?>images/ciblez_icon.png" alt="" class="img-responsive">
                        </div>
                    </div>
                    <div class="info-text">
                        <h5>
                            <?= $slider_details['featured_one_title']; ?>
                        </h5>
                        <p>
                            <?= $slider_details['featured_one_description']; ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                <div class="text-bx">
                    <div class="img-bx">
                        <div class="left-box">02</div>
                        <div class="right-box">
                            <img src="<?= ASSETS_PATH ?>images/envoyez_icon.png" alt="" class="img-responsive">
                        </div>
                    </div>
                    <div class="info-text">
                        <h5>
                            <?= $slider_details['featured_second_title']; ?>
                        </h5>
                        <p>
                            <?= $slider_details['featured_second_description']; ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                <div class="text-bx">
                    <div class="img-bx">
                        <div class="left-box">03</div>
                        <div class="right-box">
                            <img src="<?= ASSETS_PATH ?>images/recevez_icon.png" alt="" class="img-responsive">
                        </div>
                    </div>
                    <div class="info-text">
                        <h5>
                            <?= $slider_details['featured_third_title']; ?>
                        </h5>
                        <p>
                            <?= $slider_details['featured_third_description']; ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="gradient-button-bx">
                <?php if ($this->user_type == 1) : ?>
                    <a href="<?= USERS_PATH ?>/send_application" class="btn btn-lg grdnt-brn red-font">
                    <?php else: ?>
                        <a href="javascript:void(0);" class="btn btn-lg grdnt-brn red-font login-popup-model" redirect_to_url="<?= USERS_PATH . '/send_application'; ?>">
                        <?php endif; ?>
                        <span>
                            <img src="<?= ASSETS_PATH ?>images/send_icon.png" alt="" class="img-responsive">
                        </span>
                        <?= lang('SEARCH_PAGE_SNED_CV_STEP_BUTTON') ?>
                    </a>
            </div>
            <div class="gradient-button-bx">
                <?php if ($this->user_type == 1) : ?>
                    <a href="<?= USERS_PATH ?>/build_cv" class="btn btn-lg grdnt-brn red-font">
                    <?php else: ?>
                        <a href="javascript:void(0);" class="btn btn-lg grdnt-brn red-font registration-popup-model" redirect_to_url="<?= USERS_PATH . '/build_cv'; ?>">
                        <?php endif; ?>
                        <span>
                            <img src="<?= ASSETS_PATH ?>images/send_icon.png" alt="" class="img-responsive">
                        </span>
                        <?= lang('SEARCH_PAGE_CREATE_CV') ?>
                    </a>
            </div>
        </div>
    </div>
    <!-- END OF SEND APPLICATION PAGE-->
</div>


<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/sha1.min.js"></script>

<script type="text/javascript">
    jQuery.sha1 = sha1;</script>

<script type="text/javascript">
    var remove_favourite_text = "<?= lang("COMMON_FAVOURITE_REMOVE"); ?>";
    var add_favourite_text = "<?= lang("COMMON_ADD_TO_FAVOURITE"); ?>";
    var favourite_image = "<?= ASSETS_PATH ?>images/fav_selected.png";
    var unfavourite_image = "<?= ASSETS_PATH ?>images/fav_unselected.png";
    var confirmation_email = "<?= lang('CONFIRMATION_EMAIL'); ?>";
    var autocomplete = '';
    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete(
                (document.getElementById('autocomplete')),
                {types: ['geocode']}
        );
    }

    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }

    jQuery(document).ready(function () {

        var invitation_code = "<?= $this->session->userdata('invitation_code') ?>";
        if (invitation_code != "") {
            jQuery("#registration-popup").modal('show');
            jQuery("#invitation_code").val(invitation_code);
        }
        toastr.options = {
            "preventDuplicates": true,
            "preventOpenDuplicates": true,
            "closeButton": true,
            "progressBar": true
        };

        jQuery(document).on('click', ".add_to_favourite", function () {
            var get_user_id = jQuery(this).attr('recruiter_id');
            var this_action = jQuery(this);
            if (get_user_id != '' && !isNaN(get_user_id)) {
                var ajax_send_data = {
                    "user_id": get_user_id,
                };
                jQuery.ajax({
                    type: 'POST',
                    url: users_url + "/add_to_favourite",
                    data: ajax_send_data,
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            toastr.success(response.message, {timeOut: 5000});
                            jQuery(".selected_image_" + get_user_id).prop('src', favourite_image);
                            this_action.find(".add_remove_favourite_text:first").text(remove_favourite_text);
                            this_action.toggleClass('add_to_favourite remove_from_favourite');
                        } else {
                            toastr.error(response.message, {timeOut: 5000});
                        }
                    },
                    error: function () {
                        toastr.error(internal_error_message, {timeOut: 5000});
                    }
                });
            } else {
                toastr.error(problem_in_action_message, {timeOut: 5000});
            }
        });

        jQuery(document).on('click', ".remove_from_favourite", function () {
            var get_user_id = jQuery(this).attr('recruiter_id');
            var this_action = jQuery(this);
            if (get_user_id != '' && !isNaN(get_user_id)) {
                var ajax_send_data = {
                    "user_id": get_user_id,
                };
                jQuery.ajax({
                    type: 'POST',
                    url: users_url + "/remove_from_favourite",
                    data: ajax_send_data,
                    dataType: 'json',
                    success: function (response) {
                        if (response.success) {
                            toastr.success(response.message, {timeOut: 10000});
                            jQuery(".selected_image_" + get_user_id).prop('src', unfavourite_image);
                            this_action.find(".add_remove_favourite_text:first").text(add_favourite_text);
                            this_action.toggleClass('remove_from_favourite add_to_favourite');
                        } else {
                            toastr.error(response.message, {timeOut: 5000});
                        }
                    },
                    error: function () {
                        toastr.error(internal_error_message, {timeOut: 5000});
                    }
                });
            } else {
                toastr.error(problem_in_action_message, {timeOut: 5000});
            }
        });

    });
    window.fbAsyncInit = function () {
        FB.init({
            appId: '<?= FB_APP_ID ?>',
            xfbml: true,
            version: 'v2.8'
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<script type="text/javascript" src="//platform.linkedin.com/in.js">
    api_key: 78ev6kvpru6mm2
            onLoad: onLinkedInLoad
</script>

<?php
$selected_language = !empty($this->session->userdata()['language']) ? $this->session->userdata()['language'] : DEFAULT_LANG;
if ($selected_language == "fr") {
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_API_KEY ?>&libraries=places&callback=initAutocomplete&language=fr"
    async defer></script>
    <?php
} else {
    ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= GOOGLE_API_KEY ?>&libraries=places&callback=initAutocomplete&language=en"
    async defer></script>
    <?php
}
?>
<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/pages/home.js"></script>