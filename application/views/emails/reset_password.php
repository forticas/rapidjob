<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang='en'>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
        <div style="width: 80%; margin: auto;letter-spacing: 0.5px;font-size: 18px;color: gray;">
            <div style="text-align: center; margin-bottom: 30px;">
                <img src="<?= ASSETS_PATH ?>images/main_logo.png" alt="<?= APP_NAME; ?>" width="300" height="150" style="margin: 0 auto; color: #00bafc; font-family: Arial, sans-serif; font-size: 18px; display: block;" />
                <h3><?= APP_NAME . " : " . lang("FORGOT_PASSWORD_SUBJECT"); ?></h3>
            </div>
            <h4><?= lang("MAIL_HELLO"); ?> <?= !empty($name) ? $name : lang("MAIL_DEAR") ?>,</h4>
            <div style="margin: 0 30px;padding: 0">
                <p style="word-wrap: break-word;"><?= lang("FORGOT_PASSWORD_TEXT_1") ?></p>
                <a style='background-color: #00A8E6;border-radius: 3px;color: rgb(255, 255, 255);display: block;font-size: 18px;font-weight: 700;line-height: 1.5em;margin-left: auto;margin-right: auto;padding: 8px 16px;text-decoration: none; width:300px;text-align: center;' href="<?= $reset_password_url ?>"><?= lang("FORGOT_PASSWORD_LINK_TEXT"); ?></a>
                <p style="word-wrap: break-word;"><?= lang("FORGOT_PASSWORD_TEXT_2") ?></p>
                <p style="word-wrap: break-word;"><?= lang("FORGOT_PASSWORD_TEXT_3") ?></p>
            </div>
            <p><?= lang("MAIL_REGARDS"); ?>,
                <br /><?= lang("MAIL_TEAM") ?> <?= APP_NAME ?>
            </p>
        </div>
    </body>
</html>