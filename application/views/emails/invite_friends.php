<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//FR" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang='fr'>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
        <div style="width: 80%; margin: auto;letter-spacing: 0.5px;font-size: 18px;color: gray;">
            <div style="text-align: center; margin-bottom: 30px;">
                <img src="<?= ASSETS_PATH ?>images/main_logo.png" alt="<?= APP_NAME; ?>" width="300" height="150" style="margin: 0 auto; color: #00bafc; font-family: Arial, sans-serif; font-size: 18px; display: block;" />
                <h3><?= lang("MAIL_WELCOME_TO") . APP_NAME; ?></h3>
            </div>
            <h4><?= lang("MAIL_HELLO") . " " . $name ?>,</h4>
            <p><?= lang("MAIL_INVITE_YOU"); ?></p>
            <a style='background-color: #00A8E6;border-radius: 3px;color: rgb(255, 255, 255);display: block;font-size: 18px;font-weight: 700;line-height: 1.5em;margin-left: auto;margin-right: auto;padding: 8px 16px;text-decoration: none; width:300px;text-align: center;' href="<?= vsprintf('%s', $invitation_link) ?>"><?= lang("MAIL_CLICK_HERE"); ?></a>
            <p><?= lang("MAIL_REGARDS"); ?>,
                <br /><?= ucwords($this->session->userdata('user_first_name') . " " . $this->session->userdata('user_last_name')) ?>
            </p>
        </div>
    </body>
</html>