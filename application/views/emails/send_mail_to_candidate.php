<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang='en'>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
        <div style="width: 80%; margin: auto;letter-spacing: 0.5px;font-size: 18px;color: gray;">
            <div style="text-align: center; margin-bottom: 30px;">
                <img src="<?= ASSETS_PATH ?>images/main_logo.png" alt="<?= APP_NAME; ?>" width="300" height="150" style="margin: 0 auto; color: #00bafc; font-family: Arial, sans-serif; font-size: 18px; display: block;" />
                <h3><?= APP_NAME . " : " . lang("PROF_RESUME_DETAILS_SEND_MAIL_SUBJECT"); ?></h3>
            </div>
            <h4><?= lang("MAIL_HELLO"); ?> <?= !empty($candidate_name) ? $candidate_name : lang("MAIL_DEAR") ?>,</h4>
            <div style="margin: 0 30px;padding: 0">
                <p style="word-wrap: break-word;"><?= !empty($recruiter_name) ? $recruiter_name : lang("COMMON_USER_RECRUITER") ?>, <?= lang("PROF_RESUME_DETAILS_SEND_MAIL_TEXT_1") ?></p>
                <p style="word-wrap: break-word;"><?= !empty($message) ? get_formatted_text($message) : lang("PROF_RESUME_DETAILS_SEND_MAIL_MESSAGE_MISSING") ?></p>                
                <?php if (!empty($recruiter_email)) : ?>
                    <p style="word-wrap: break-word;"><?= lang("PROF_RESUME_DETAILS_SEND_MAIL_RECRUITER_EMAIL") ?> <?= $recruiter_email ?></p>
                <?php endif; ?>
                <?php if (!empty($recruiter_phone)) : ?>
                    <p style="word-wrap: break-word;"><?= lang("PROF_RESUME_DETAILS_SEND_MAIL_RECRUITER_PHONE") ?> <?= $recruiter_phone ?></p>
                <?php endif; ?>
                <?php if (!empty($recruiter_address) || !empty($recruiter_city) || !empty($recruiter_country) || !empty($recruiter_postal_code)) : ?>
                    <p style="word-wrap: break-word;"><?= lang("PROF_RESUME_DETAILS_SEND_MAIL_RECRUITER_ADDRESS") ?> <?= $recruiter_address ?>, <br/><?= $recruiter_city ?> - <?= $recruiter_postal_code ?><br/> <?= $recruiter_country ?></p>
                <?php endif; ?>

            </div>
            <p><?= lang("MAIL_REGARDS"); ?>,
                <br /><?= !empty($candidate_name) ? $candidate_name : lang("MAIL_DEAR") ?>, <?= APP_NAME ?>
            </p>
        </div>
    </body>
</html>