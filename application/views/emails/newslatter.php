<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//FR" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?= $current_lang ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    </head>
    <body>
        <div style="width: 80%; margin: auto;font-size: 18px;color:#505050;font-family: Arial, sans-serif;">
            <div style="background-color: #E5E5E5;height: 60px;margin-bottom: 30px;padding: 10px;text-align: left;width: 100%">
                <img src="<?= ASSETS_PATH ?>images/logo_<?= $current_lang ?>.png" alt="<?= APP_NAME; ?>" width="200" height="50" />
            </div>
            <?php if ($current_lang == 'en'): ?>
                <h4 style="font-size: 22px;">You have received a <span style="color: #29ABA3;">Newsletter E-mail</span></h4>
                <h4>Hello <?= !empty($name) ? trim($name) : "Dear" ?>,</h4>
            <?php else: ?> 
                <h4 style="font-size: 22px;">Vous avez reçu un <span style="color: #29ABA3;">Courriel de la newsletter</span></h4>
                <h4>Bonjour <?= !empty($name) ? trim($name) : "Cher" ?>,</h4>
            <?php endif; ?> 
            <div style="clear: both"></div>
            <div style="margin: 0 0 0 32px;padding: 0;width: 100%;background-color: #ffffff;float: left;">
                <p style="word-wrap: break-word;"><?= lang("MAIL_NEWSLATTER_TEXT"); ?></p>
            </div>
            <div style="clear: both;"></div>
            <p><?= lang("MAIL_REGARDS"); ?>,
                <br /><?= lang("MAIL_TEAM") . " " . APP_NAME; ?>
            </p>
            <div style="clear: both;"></div>
            <div style="background-color: #E5E5E5; margin-bottom: 30px; margin-top: 20px;text-align: left; width: 100%; padding: 0px; height: 57px;">
                <?php if ($current_lang == 'en'): ?>
                    <p style="font-size: 14px; text-align: left; padding: 20px; color: rgb(255, 255, 255); background-color: rgba(0, 0, 0, 0.3);"><?= APP_NAME; ?> © <?= date("Y") ?> - All rights reserved - Legal notice - CGV</p>
                <?php else: ?>
                    <p style="font-size: 14px; text-align: left; padding: 20px; color: rgb(255, 255, 255); background-color: rgba(0, 0, 0, 0.3);"><?= APP_NAME; ?> © <?= date("Y") ?> - Tous droits réservés - Mentions légales - CGV</p>
                <?php endif; ?>
            </div>
            <div style="clear: both"></div>
        </div>
    </body>
</html>