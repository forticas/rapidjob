<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//FR" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?= $current_lang ?>">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    </head>
    <body>
        <div style="width: 95%; margin: auto;font-size: 18px;color:#505050;font-family: Arial, sans-serif;">
            <div style="background-color: #E5E5E5;height: 60px;margin-bottom: 30px;padding: 10px;text-align: left;width: 100%">
                <img src="<?= ASSETS_PATH ?>images/logo_<?= $current_lang ?>.png" alt="<?= APP_NAME; ?>" width="200" height="50" />
            </div>
            <?php if ($current_lang == 'en'): ?>
                <h4 style="font-size: 22px;">You have received a <span style="color: #29ABA3;">CV</span></h4>
                <h4>Hello <?= !empty($company_name) ? trim($company_name) : "Dear" ?>,</h4>
            <?php else: ?> 
                <h4 style="font-size: 22px;">Vous avez reçu un <span style="color: #29ABA3;">CV</span></h4>
                <h4>Bonjour <?= !empty($company_name) ? trim($company_name) : "Cher" ?>,</h4>
            <?php endif; ?> 
            <div style="clear: both"></div>
            <div style="margin: 0 30px;padding: 0">
                <?php if ($current_lang == 'en'): ?>
                    <p style="word-wrap: break-word;">A candidate sends you his resume and the message below.</p>
                <?php else: ?>
                    <p style="word-wrap: break-word;">Un candidat vous envoie son CV et le message ci-dessous.</p>
                <?php endif; ?>
            </div>
            <div style="clear: both"></div>
            <p style="background-color: rgb(229, 229, 229);height: initial;margin: 0 auto;padding: 14px 0 14px 16px;">
                <?php if ($current_lang == 'en'): ?>
                    To contact this candidate, please use the following contact information:
                <?php else: ?>
                    Pour contacter ce candidat, utilisez les informations de contact suivantes:
                <?php endif; ?>
            </p>
            <div style="clear: both"></div>
            <div style="margin: 50px 0 0 0px;padding: 0;width: 100%;background-color: #ffffff;">
                <div style="float: left;text-align: center;width: 20%;">
                    <img src="<?= $user_image ?>" alt="<?= !empty($user_name) ? $user_name : "" ?>" style="border-radius: 50%;width: 75%"/>
                    <?php if (!empty($user_cv)) : ?>
                        <a target="_blank" href="<?= $user_cv ?>" title="View the CV" style="font-weight: 600;font-size: 14px;outline: 0 none;color: rgb(235, 113, 95);float: left;margin: 0 auto;padding: 10px 0 0;text-align: center;text-decoration: none;width: 100%;">
                            <?php if ($current_lang == 'en'): ?>   
                                View the CV
                            <?php else: ?>
                                Voir le CV
                            <?php endif; ?>
                        </a>
                    <?php endif; ?>
                    <?php if (!empty($user_cover_latter)) : ?>
                        <a target="_blank" href="<?= $user_cover_latter ?>" title="View the Cover letter" style="font-weight: 600;font-size: 14px;outline: 0 none;color: rgb(235, 113, 95);float: left;margin: 0 auto;padding: 10px 0 0;text-align: center;text-decoration: none;width: 100%;">
                            <?php if ($current_lang == 'en'): ?>   
                                View the Cover letter
                            <?php else: ?>
                                Voir la lettre de motivation
                            <?php endif; ?>
                        </a>
                    <?php endif; ?>
                </div>
                <div style="float: left;width: 70%;padding-left: 10px;">
                    <?php if ($current_lang == 'en'): ?>
                        <?php if (!empty($user_name)) : ?>
                            <p style="margin:10px 0;font-size: 14px;"><b>Name:</b> <?= trim($user_name) ?></p>
                        <?php endif; ?>
                        <?php if (!empty($user_email)) : ?>
                            <p style="margin:10px 0;font-size: 14px;"><b>Email address:</b> <?= trim($user_email) ?></p>
                        <?php endif; ?>
                        <p style="word-wrap: break-word;font-size: 14px;"><b>Message:</b> <br/><p style="margin: 10px 0;font-size: 14px;"><?= !empty($message) ? get_formatted_text($message) : 'Message is missing.' ?></p></p>
                        <p style="font-size: 14px;font-weight: 600">Do not forget to consult the CV attached to this message.</p>
                    <?php else: ?>
                        <?php if (!empty($user_name)) : ?>
                            <p style="margin:10px 0;font-size: 14px;"><b>Prénom:</b> <?= trim($user_name) ?></p>
                        <?php endif; ?>
                        <?php if (!empty($user_email)) : ?>
                            <p style="margin:10px 0;font-size: 14px;"><b>Adresse e-mail:</b> <?= trim($user_email) ?></p>
                        <?php endif; ?>
                        <p style="word-wrap: break-word;font-size: 14px;"><b>Message:</b> <br/><p style="margin: 10px 0;font-size: 14px;"><?= !empty($message) ? get_formatted_text($message) : 'Message is missing.' ?></p></p>
                        <p style="font-size: 14px;font-weight: 600">N'oubliez pas de consulter le CV joint à ce message.</p>
                    <?php endif; ?>
                </div>
                <div style="clear: both;"></div>
            </div>
            <div style="clear: both;"></div>
            <div style="background-color: #E5E5E5; margin-bottom: 30px; margin-top: 20px;text-align: left; width: 100%; padding: 0px; height: auto;">
                <?php if ($current_lang == 'en'): ?>
                    <div style="clear: both;"></div>
                    <p style="font-size: 14px; text-align: left; padding: 20px; color: rgb(255, 255, 255); background-color: rgba(0, 0, 0, 0.3);"><?= APP_NAME; ?> © <?= date("Y") ?> - All rights reserved - Legal notice - CGV</p>
                    <div style="clear: both"></div>
                    <div>
                        <p style="font-size: 14px; text-align: center; line-height: 20px; padding: 10px 10px 20px 10px">This email is automatically sent to you, please do not use the "reply to sender" function.<br/>
                            A question ? View our <a href="<?= HOME_PATH ?>/faq" style="color: #29ABA3;">FAQ</a><br/>
                            You have the right to access and rectify any information that concerns you with <?= APP_NAME; ?> in accordance with the law of 6 January 1978, as amended, relating to data processing, files and freedoms.</p>
                    </div>
                <?php else: ?>
                    <div style="clear: both;"></div>
                    <p style="font-size: 14px; text-align: left; padding: 20px; color: rgb(255, 255, 255); background-color: rgba(0, 0, 0, 0.3);"><?= APP_NAME; ?> © <?= date("Y") ?> - Tous droits réservés - Mentions légales - CGV</p>
                    <div style="clear: both"></div>
                    <div>
                        <p style="font-size: 14px; text-align: center; line-height: 20px; padding: 10px 10px 20px 10px">Cet e-mail vous est automatiquement envoyé, n'utilisez pas la fonction "répondre à l'expéditeur".<br/>
                            Une question ? Regardez notre <a href="<?= HOME_PATH ?>/faq" style="color: #29ABA3;">FAQ</a><br/>
                            Vous avez le droit d'accéder et de rectifier toute information qui vous concerne <?= APP_NAME; ?> Conformément à la loi du 6 janvier 1978, telle que modifiée, relative au traitement des données, aux fichiers et aux libertés.</p>
                    </div>
                <?php endif; ?>
            </div>
            <div style="clear: both"></div>
        </div>
    </body>
</html>