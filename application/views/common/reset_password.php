<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div class="build-cv top-section edit-profile">
    <div class="container reset_password_page">
        <div class="title-box">
            <label>R</label>
            <h3 class="green-light-font">
                <?= lang('VERIFY_PASSWORD_HEAD_1') ?> <span class="red-font"><?= lang('VERIFY_PASSWORD_HEAD_2') ?></span>
            </h3>
        </div>

        <div class="edit-profile-bx">
            <div class="bg-img hidden-xs visible-lg visible-md visible-sm">
                <img src="<?= ASSETS_PATH ?>images/sheild_icon.png" alt="" class="img-responsive">
            </div>
            <label class="red-label red-font">
                <?= lang('FORGOT_PASSWORD_LINK_TEXT') ?>
            </label>
            <?php echo form_open(HOME_PATH . "/verify_token/" . $token, 'id="reset_password"'); ?>
            <div class="inpot-bx">
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <label for="password" class="select-label"><?= lang('EDITPROFILE_PAGE_NEW_PASSWORD') ?></label>
                        <div class="form-group">
                            <input type="password" placeholder="" id="password" name="password" class="form-control required" data-rule-pwcheck="true" data-msg-pwcheck="<?= lang('REGISTER_PASSWORD_VALIDATION_MESSAGE'); ?>">
                            <?php if (!empty(form_error('password'))) : ?><div for="password" class="text-danger"><?php echo form_error('password'); ?></div><?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                        <label for="cpassword" class="select-label"><?= lang('EDITPROFILE_PAGE_CONFIRM_PASSWORD') ?></label>
                        <div class="form-group">
                            <input type="password" placeholder="" id="cpassword" name="cpassword" class="form-control required">
                            <?php if (!empty(form_error('cpassword'))) : ?><div for="cpassword" class="text-danger"><?php echo form_error('cpassword'); ?></div><?php endif; ?>
                            <?php if (!empty(form_error('not_match'))) : ?><div for="cpassword" class="text-danger"><?php echo form_error('not_match'); ?></div><?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="inpot-bx">
                <div class="edit-profile-btn set_btn_margin_padding">
                    <button class="round-btn profile-popup-green btn btn-block btn-social btn-profile">
                        <span class="profile-popup-dark-green">
                            <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/register.png">
                        </span>
                        <?= lang('VERIFY_PASSWORD_SUBMIT') ?>
                    </button>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/sha1.min.js"></script>
<script type="text/javascript">
                                jQuery.sha1 = sha1;</script>
<script type="text/javascript">
    var base_url = "<?= HOME_PATH ?>";
    var internal_error_message = "<?= lang("COMMON_INTERNAL_ERROR"); ?>";
    var confirmation_email = "<?= lang('CONFIRMATION_EMAIL') ?>";
    var letter_only = "<?= lang('LETTER_ONLY') ?>";
    
    jQuery(document).ready(function () {
        var invitation_code = "<?= $this->session->userdata('invitation_code') ?>";
        if (invitation_code != "") {
            jQuery("#registration-popup").modal('show');
            jQuery("#invitation_code").val(invitation_code);
        }
    });

    window.fbAsyncInit = function () {
        FB.init({
            appId: '<?= FB_APP_ID ?>',
            xfbml: true,
            version: 'v2.8'
        });
    };
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
            return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<script type="text/javascript" src="//platform.linkedin.com/in.js">
    api_key: 78ev6kvpru6mm2
            onLoad: onLinkedInLoad
</script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>/js/pages/reset_password.js"></script>