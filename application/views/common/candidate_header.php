<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//FR" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang='fr'>
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
        <head>
            <meta charset="utf-8"/>
            <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
            <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
            <title><?= APP_NAME; ?> <?= APP_VERSION ?></title>
            <meta name="description" content=""/>
            <meta name="viewport" content="width=device-width, initial-scale=1"/>
            <link rel="icon" href="<?= DEFAULT_FAVICON_ICON; ?>" type="image/x-icon"/>
            <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/bootstrap.min.css"/>
            <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/bootstrap-select.css"/> 
            <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/bootstrap-theme.min.css"/>
            <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/bootstrap-multiselect.css"/>
            <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/font-awesome.min.css"/>
            <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/bootstrap-social.css"/>
            <link rel="stylesheet" href="<?= ASSETS_PATH ?>js/datepicker/css/bootstrap-datepicker3.min.css"/>
            <link rel="stylesheet" href="<?= ASSETS_PATH ?>js/plugins/sweetalert/sweetalert.css"/>
            <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/style.css"/>
            <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/font.css"/>
            <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/socialmedia.css"/>
            <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/loader.css"/>
            <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/toastr.css"/>
            <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>
            <script>window.jQuery || document.write('<script src="<?= ASSETS_PATH ?>js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
            <script src="<?= ASSETS_PATH ?>js/plugins/sweetalert/sweetalert.min.js"></script>
            <script type="text/javascript" src="<?= ASSETS_PATH ?>js/vendor/bootstrap.min.js"></script>
            <script type="text/javascript" src="<?= ASSETS_PATH ?>js/vendor/bootstrap-select.js"></script>
            <script type="text/javascript" src="<?= ASSETS_PATH ?>js/datepicker/js/bootstrap-datepicker.min.js"></script>
            <script type="text/javascript" src="<?= ASSETS_PATH ?>js/datepicker/locales/bootstrap-datepicker.fr.min.js"></script>
            <script type="text/javascript" src="<?= ASSETS_PATH ?>js/vendor/bootstrap-multiselect.js"></script>
            <script type="text/javascript" src="<?= ASSETS_PATH ?>js/main.js"></script>
            <script type="text/javascript" src="<?= ASSETS_PATH ?>js/socialmedia.js"></script>
            <script type="text/javascript" src="<?= ASSETS_PATH ?>js/toastr.min.js"></script>
            <script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery.validate.min.js"></script>
            <script type="text/javascript" src="<?= ASSETS_PATH ?>js/additional-methods.min.js"></script>
            <script type="text/javascript" src="<?= ASSETS_PATH ?>js/local-time.js"></script>
            <?php
            $selected_language = !empty($this->session->userdata()['language']) ? $this->session->userdata()['language'] : DEFAULT_LANG;
            if ($selected_language == "fr") {
                ?>
                <script type="text/javascript" src="<?= ASSETS_PATH ?>js/language/messages_fr.js"></script>
                <?php
            } else {
                ?>
                <script type="text/javascript" src="<?= ASSETS_PATH ?>js/language/messages_en.js"></script>
                <?php
            }
            ?>
            <!--[if lt IE 9]>
                <script src="<?= ASSETS_PATH ?>js/vendor/html5-3.6-respond-1.4.2.min.js"></script>
            <![endif]-->
        </head>
        <body>
            <input type="hidden" value="<?= $this->security->get_csrf_hash(); ?>" id="csrf_token_data" name="<?= $this->security->get_csrf_token_name() ?>"> 
                <div class="light-white-bg main-profle-body">
                    <header>
                        <nav class="navbar navbar-inverse navbar-fixed-top camdidate-navbar">
                            <div class="container-fluid header-container">
                                <div class="navbar-header">
                                    <button aria-expanded="false" data-target="#bs-example-navbar-collapse-9" data-toggle="collapse" class="collapsed navbar-toggle" type="button">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="<?= DOMAIN_URL ?>">
                                        <img src="<?= ASSETS_PATH ?>images/logo_<?= $this->current_lang; ?>.png" alt="<?= APP_NAME; ?>" class="img-responsive">
                                    </a>
                                    <div class="language mob-language pull-right visible-xs hidden-lg hidden-md hidden-sm">
                                        <div class="arrow selectpicker">
                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        </div>
                                        <?php
                                        $language_array = array(
                                            "fr" => "Fr",
                                            "en" => "En"
                                        );
                                        echo form_dropdown("language", $language_array, $selected_language, array("class" => "selectpicker change_language"));
                                        ?>
                                    </div>
                                </div>
                                <div id="bs-example-navbar-collapse-9" class="collapse navbar-collapse">
                                    <div class="language hidden-xs visible-lg visible-md visible-sm">
                                        <div class="arrow selectpicker">
                                            <i class="fa fa-angle-down" aria-hidden="true"></i>
                                        </div>
                                        <?php
                                        echo form_dropdown("language", $language_array, $selected_language, array("class" => "selectpicker change_language"));
                                        ?>
                                    </div>
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="active">
                                            <a href="<?= HOME_PATH . '/faq'; ?>" class="thumbnail">
                                                <img src="<?= ASSETS_PATH ?>images/faq_icon.png" alt="" class="img-responsive">
                                                    <?= lang('HEADER_MENU_HELP') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="javascript:void(0);" class="thumbnail dropdown-toggle" data-toggle="dropdown">
                                                <img src="<?= ASSETS_PATH ?>images/user_icon.png" alt="" class="img-responsive">
                                                    <?= lang('PROF_HEADER_USER_MENU') ?> 
                                                    <?php
                                                    $user_name = $this->session->userdata("user_first_name");
                                                    echo!empty($user_name) ? strlen($user_name) > DEFAULT_USER_NAME_TEXT_LIMIT ? substr($user_name, 0, DEFAULT_USER_NAME_TEXT_LIMIT) . "..." : $user_name : '';
                                                    ?>
                                                    <i class="fa fa-caret-down down-caret red-font" aria-hidden="true"></i>
                                            </a>
                                            <ul class="dropdown-menu red-bg">
                                                <li class="<?php if ($this->router->fetch_class() == 'users' && $this->router->method == 'index') echo 'active'; ?>"><a href="<?= USERS_PATH ?>"><?= lang('CANDIDATE_HEADER_USER_SUB_MENU_PROFILE') ?></a></li>
                                                <li class="<?php if ($this->router->fetch_class() == 'users' && $this->router->method == 'edit_profile') echo 'active'; ?>"><a href="<?= USERS_PATH ?>/edit_profile"><?= lang('CANDIDATE_HEADER_USER_SUB_MENU_PERSONAL_INFO') ?></a></li>
                                                <li class="<?php if ($this->router->fetch_class() == 'users' && $this->router->method == 'my_documents') echo 'active'; ?>"><a href="<?= USERS_PATH ?>/my_documents"><?= lang('CANDIDATE_HEADER_USER_SUB_MENU_MY_DOCUMENTS') ?></a></li>
                                                <li class="<?php if ($this->router->fetch_class() == 'users' && $this->router->method == 'sent_application_list') echo 'active'; ?>"><a href="<?= USERS_PATH ?>/sent_application_list"><?= lang('CANDIDATE_HEADER_USER_SUB_MENU_MY_APPLICATION') ?></a></li>
                                                <li><a href="<?= USERS_PATH ?>/signout"><?= lang('CANDIDATE_HEADER_USER_SUB_MENU_DISCONNECT') ?></a></li>
                                            </ul>
                                        </li>
                                        <li>
                                            <a href="<?= USERS_PATH . '/my_orders'; ?>" class="thumbnail">
                                                <img src="<?= ASSETS_PATH ?>images/bag_icon.png" alt="" class="img-responsive">
                                                    <?= lang('HEADER_MENU_MYBASKET') ?>
                                            </a>
                                        </li>
                                    </ul>

                                </div>
                            </div>
                        </nav>
                    </header>
                    <script type="text/javascript">
                var current_lang = "<?= $this->current_lang ?>";
                var base_url = "<?= HOME_PATH; ?>";
                var users_url = "<?= USERS_PATH; ?>";
                var professional_url = "<?= PROFESSIONAL_PATH; ?>";
                var invalid_selection = "<?= lang("COMMON_SELECT_INVALID"); ?>";
                var required_field_message = "<?= lang("COMMON_THIS_REQUIRED_FIELD"); ?>";
                var problem_in_action_message = "<?= lang("COMMON_PROBLEM_IN_ACTION"); ?>";
                var internal_error_message = "<?= lang("COMMON_INTERNAL_ERROR"); ?>";

                var flash_success_message = "<?= $this->session->flashdata('success'); ?>";
                var flash_failure_message = "<?= $this->session->flashdata('failure'); ?>";


                jQuery(document).ready(function () {


                    toastr.options = {
                        "preventDuplicates": true,
                        "preventOpenDuplicates": true,
                        "closeButton": true,
                        "progressBar": true
                    };
                    if (flash_success_message != '') {
                        toastr.success(flash_success_message, {timeOut: 5000});
                    }

                    if (flash_failure_message != '') {
                        toastr.error(flash_failure_message, {timeOut: 5000});
                    }

                    jQuery.validator.addMethod("no_space_allow", function (value, element) {
                        var value1 = value.replace(/&nbsp;/g, '');
                        if (value1.trim() === "") {
                            return false;
                        } else {
                            return true;
                        }
                    });

                    jQuery.validator.addMethod("pwcheck", function (value) {
                        return /^[A-Za-z0-9\d=!@#$%^&*()_\-]*$/.test(value) // consists of only these
                                && /[a-z]/.test(value) // has a lowercase letter
                                && /[A-Z]/.test(value) // has a upper case letter
                                && /[=!@#$%^&*()_\-]/.test(value) // has a special char
                                && /\d/.test(value) // has a digit
                    });
                    jQuery.validator.addMethod("lettersonly", function (value, element) {
                        return this.optional(element) || value == value.match(/^[a-zA-Z\s]+$/);
                    });
                    jQuery.validator.addMethod("email", function (value, element) {
                        var reg_email = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                        return reg_email.test(value);
                    });
                    //Phone number validation
                    jQuery.validator.addMethod("phone_number", function (phone_number, element) {
                        phone_number = phone_number.replace(/\s+/g, "");
                        return this.optional(element) || phone_number.length > 8 && phone_number.length < 15 &&
                                phone_number.match(/^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/);
                    }, "Please enter valid telephone number");

                    jQuery.validator.addMethod("regex_email", function (value, element, regexpr) {
                        if (/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i.test(value)) {
                            return true;
                        } else {
                            return false;
                        }
                    });
                });

                    </script>