<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div style="display: none" id="loading" class="loading"></div>
</div>
<footer>
    <div class="footer-container">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="ftr-logo">
                <a href="<?= DOMAIN_URL ?>"><img src="<?= ASSETS_PATH ?>images/logo_<?= $this->current_lang; ?>.png" alt="" class="img-responsive"></a>
            </div>
        </div>
        <!--        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 hm-ftr-mob-width">
                    <ul>
                        <li><a href="#"> <?= lang('FOOTER_SEND_APPLICATION') ?> </a></li>
                        <li><a href="#"><?= lang('FOOTRE_MY_CV') ?></a></li>
                        <li><a href="#"><?= lang('FOOTER_MOTIVATION_LETTER') ?></a></li>
                        <li><a href="#"><?= lang('FOOTER_COMPANY_AREA') ?></a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 hm-ftr-mob-width">
                    <ul>
                        <li>
                            <a href="#">
                                <span><img src="<?= ASSETS_PATH ?>images/cart_icon.png" alt="" class="img-responsive"></span>
        <?= lang('FOOTER_SHOOPING_CART') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span><img src="<?= ASSETS_PATH ?>images/logout_icon.png" alt="" class="img-responsive"></span>
        <?= lang('FOOTRE_SUBSCRIBE') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span><img src="<?= ASSETS_PATH ?>images/data_icon.png" alt="" class="img-responsive"></span>
        <?= lang('FOOTER_CONTACTUS') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span><img src="<?= ASSETS_PATH ?>images/faq_icon_white.png" alt="" class="img-responsive"></span>
        <?= lang('FOOTER_TIPS_WRITING') ?>
                            </a>
                        </li>
                    </ul>
                </div>-->
        <?php
        get_instance()->load->helper('footer');
        $lang = !empty($this->session->userdata('language')) ? $this->session->userdata('language') : 'fr';
        $footer_details = footer_details($lang);
        ?>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 pull-right">
            <p>
                <?= lang('FOOTER_CONTACT_TITLE') ?> : <a href="mailto:<?= $footer_details['cms_footer_contactus']; ?>"><?= $footer_details['cms_footer_contactus']; ?></a>
            </p>
            <p>
                <?= lang('FOOTER_SOCIAL_SHARING') ?> :
            </p>
            <div class="hm-ftr-social-media">

                <?php if (!empty($footer_details['cms_footer_facebook_link'])) { ?>
                    <a href="<?= $footer_details['cms_footer_facebook_link']; ?>" class="facebook"></a>
                <?php } ?>
                <?php if (!empty($footer_details['cms_footer_twitter_link'])) { ?>
                    <a href="<?= $footer_details['cms_footer_twitter_link']; ?>" class="tweet"></a>
                <?php } ?>
                <?php if (!empty($footer_details['cms_footer_google_link'])) { ?>
                    <a href="<?= $footer_details['cms_footer_google_link']; ?>" class="gmail"></a>
                <?php } ?>
                <?php if (!empty($footer_details['cms_footer_linkedin_link'])) { ?>
                    <a href="<?= $footer_details['cms_footer_linkedin_link']; ?>" class="linkedin"></a>
                <?php } ?>
            </div>
        </div>
        <div class="copy-right">
            <h5><?= $footer_details['footer_copyright']; ?></h5>
        </div>
    </div>
</footer>
<!--<div class="up-scroll green-light">
    <i class="fa fa-angle-double-up" aria-hidden="true"></i>
</div>-->

<script type="text/javascript">
    var no_result_found = "<?= lang('NO_RESULT_MATCHED'); ?>";
    var change_language_error_message = "<?= lang('COMMON_ERROR_CHANGE_LANGUAGE'); ?>";
    var get_csrf_token_name = "<?= $this->security->get_csrf_token_name() ?>";
    var get_method = '<?= $this->router->method; ?>';
    jQuery(document).ready(function () {
        jQuery('#carousel').carousel();
        jQuery('.selectpicker').selectpicker({
            noneResultsText: no_result_found,
            size: '10',
            selectAllText: '<?= lang("COMMON_SELECT_ALL") ?>',
            deselectAllText: '<?= lang("COMMON_DESELECT_ALL") ?>',
            countSelectedText: function (num) {
                if (num == 1) {
                    return "{0} <?= lang('COMMON_ITEMS_SELECTED'); ?>";
                } else {
                    return "{0} <?= lang('COMMON_ITEMS_SELECTED'); ?>";
                }
            }
        });

    });
</script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/pages/common.js"></script>
</body>
</html>
