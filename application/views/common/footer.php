<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<div style="display: none" id="loading" class="loading"></div>
<footer>
    <div class="footer-container">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="ftr-logo">
                <a href="<?= DOMAIN_URL ?>"><img src="<?= ASSETS_PATH ?>images/logo_<?= $this->current_lang; ?>.png" alt="" class="img-responsive"></a>
            </div>
        </div>

        <!-- 
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 hm-ftr-mob-width">
            <ul>
                <li><a href="#"> <?= lang('FOOTER_SEND_APPLICATION') ?> </a></li>
                <li><a href="#"><?= lang('FOOTRE_MY_CV') ?></a></li>
                <li><a href="#"><?= lang('FOOTER_MOTIVATION_LETTER') ?></a></li>
                <li><a href="#"><?= lang('FOOTER_COMPANY_AREA') ?></a></li>
            </ul>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 hm-ftr-mob-width">
            <ul>
                <li>
                    <a href="#">
                        <span><img src="<?= ASSETS_PATH ?>images/cart_icon.png" alt="" class="img-responsive"></span>
        <?= lang('FOOTER_SHOOPING_CART') ?>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span><img src="<?= ASSETS_PATH ?>images/logout_icon.png" alt="" class="img-responsive"></span>
        <?= lang('FOOTRE_SUBSCRIBE') ?>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span><img src="<?= ASSETS_PATH ?>images/data_icon.png" alt="" class="img-responsive"></span>
        <?= lang('FOOTER_CONTACTUS') ?>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <span><img src="<?= ASSETS_PATH ?>images/faq_icon_white.png" alt="" class="img-responsive"></span>
        <?= lang('FOOTER_TIPS_WRITING') ?>
                    </a>
                </li>
            </ul>
        </div>
        
        -->
        <?php
        get_instance()->load->helper('footer');
        $lang = !empty($this->session->userdata('language')) ? $this->session->userdata('language') : 'fr';
        $footer_details = footer_details($lang);
        ?>

        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 pull-right">
            <p>
                <?= lang('FOOTER_CONTACT_TITLE') ?> : <a href="mailto:<?= $footer_details['cms_footer_contactus']; ?>"><?= $footer_details['cms_footer_contactus']; ?></a>
            </p>
            <p>
                <?= lang('FOOTER_SOCIAL_SHARING') ?> :
            </p>
            <div class="hm-ftr-social-media">

                <?php if (!empty($footer_details['cms_footer_facebook_link'])) { ?>
                    <a href="<?= $footer_details['cms_footer_facebook_link']; ?>" class="facebook"></a>
                <?php } ?>
                <?php if (!empty($footer_details['cms_footer_twitter_link'])) { ?>
                    <a href="<?= $footer_details['cms_footer_twitter_link']; ?>" class="tweet"></a>
                <?php } ?>
                <?php if (!empty($footer_details['cms_footer_google_link'])) { ?>
                    <a href="<?= $footer_details['cms_footer_google_link']; ?>" class="gmail"></a>
                <?php } ?>
                <?php if (!empty($footer_details['cms_footer_linkedin_link'])) { ?>
                    <a href="<?= $footer_details['cms_footer_linkedin_link']; ?>" class="linkedin"></a>
                <?php } ?>
            </div>
        </div>
        <div class="copy-right">
            <h5><?= $footer_details['footer_copyright']; ?></h5>
        </div>
    </div>
</footer>


<!-- Registration form-->
<div class="modal fade hm-registration-popup hm-popup" id="registration-popup" data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_modal" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-title" id="myModalLabel">
                    <span class=""><img src="<?= ASSETS_PATH ?>images/user_icon.png" alt="" class="img-responsive"></span>
                    <h3><?= lang('SIGNUP_FORM_TITLE') ?></h3>
                </div>
            </div>
            <div class="btn-line">
                <img src="<?= ASSETS_PATH ?>images/line.png" alt="" class="img-responsive">
            </div>
            <div class="modal-body signup_modal_box">
                <form action="javascript:void(0)" id="signup_form" name="signup_form" role="form">
                    <input type="email" style="display: none"/>
                    <input type="password" style="display: none"/>
                    <input type="hidden" id="invitation_code" name="invitation_code" value="">
                    <input type="hidden" id="redirect_to_url" name="redirect_to_url" value="<?= HOME_PATH; ?>">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 hm-model-mob-width">
                            <div class="form-group">
                                <label for="su_firstname"><?= lang('SIGNUP_FORM_FIRST_NAME') ?><span class="text-danger">*</span></label>
                                <input type="text" id="su_firstname" name="su_firstname" class="form-control required" data-rule-minlength="2" data-rule-maxlength="25">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 hm-model-mob-width">
                            <div class="form-group">
                                <label for="su_lastname"><?= lang('SIGNUP_FORM_LAST_NAME') ?><span class="text-danger">*</span></label>
                                <input type="text" placeholder="" name="su_lastname" id="su_lastname" class="form-control required" data-rule-minlength="2" data-rule-maxlength="25">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="su_email"><?= lang('SIGNUP_FORM_EMAIL') ?><span class="text-danger">*</span></label>
                                <?php
                                $add_user_email = array(
                                    "id" => "su_email",
                                    "name" => "su_email",
                                    "class" => "form-control required",
                                    "placeholder" => "",
                                    "value" => "",
                                    "data-rule-maxlength" => "255"
                                );

                                echo form_input($add_user_email);
                                ?>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="su_cemail"><?= lang('SIGNUP_FORM_CONFIRM_EMAIL') ?><span class="text-danger">*</span></label>
                                <input type="text" placeholder="" id="su_cemail" name="su_cemail" class="form-control required" data-rule-maxlength="255">
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="su_password"><?= lang('SIGNUP_FORM_PASSWORD') ?><span class="text-danger">*</span></label>
                                <?php
                                $add_user_email = array(
                                    "type" => "password",
                                    "id" => "su_password",
                                    "name" => "su_password",
                                    "class" => "form-control required",
                                    "placeholder" => "",
                                    "value" => "",
                                    'data-rule-pwcheck' => 'true',
                                    'data-msg-pwcheck' => lang("REGISTER_PASSWORD_VALIDATION_MESSAGE"),
                                    "data-rule-minlength" => "6",
                                    "data-rule-maxlength" => "25"
                                );

                                echo form_input($add_user_email);
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="popup-main-btn col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <input type="hidden" name="su_user_type" id="su_user_type" value="1">
                            <input type="hidden" name="su_social_id" id="su_social_id" value="">
                            <button type="submit" id="su_submit" class="round-btn  red-bg btn btn-block btn-social btn-register">
                                <span class="dark-red">
                                    <img src="<?= ASSETS_PATH ?>images/icon_edit.png" alt="" class="img-responsive">
                                </span>
                                <?= lang('SIGNUP_FORM_BUTTON') ?>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="popup-logo-title-bx">
                    <div class="hm-popup-ttl-text">
                        <div class="line"></div>
                        <div class="title">
                            <?= lang('SIGNUP_FORM_VIA_SOCIAL_TITLE') ?>
                        </div>
                        <div class="line"></div>
                    </div>
                    <div class="popup-social-logo">
                        <a href="javascript:void(0)" onclick="loginWithFb('#signup_form')">
                            <img src="<?= ASSETS_PATH ?>images/logo_facebook.png" alt="" class="img-responsive">
                        </a>
                        <a href="javascript:void(0)" onclick="IN.User.authorize();">
                            <img src="<?= ASSETS_PATH ?>images/logo_linkedin.png" alt="" class="img-responsive">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Login form -->
<div class="modal fade hm-signin-popup hm-popup" id="signin-popup"  data-backdrop="static" data-keyboard="false"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close close_model_login" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-title" id="myModalLabel">
                    <span class=""><img src="<?= ASSETS_PATH ?>images/user_green_icon.png" alt="" class="img-responsive"></span>
                    <h3><?= lang('SIGNIN_FORM_TITLE') ?></h3>
                </div>
            </div>
            <div class="btn-line">
                <img src="<?= ASSETS_PATH ?>images/line.png" alt="" class="img-responsive">
            </div>
            <form action="javascript:void(0)" id="signin_form" name="signin_form" role="form">
                <input type="email" style="display: none"/>
                <input type="password" style="display: none"/>
                <input type="hidden" id="redirect_to_url" name="redirect_to_url" value="<?= HOME_PATH; ?>">
                <div class="modal-body">
                    <div class="popup-logo-title-bx social_div_content">
                        <div class="hm-popup-ttl-text">
                            <div class="line"></div>
                            <div class="title">
                                <?= lang('SIGNIN_FORM_SOCIAL_TITLE') ?>
                            </div>
                            <div class="line"></div>
                        </div>
                        <div class="popup-social-logo">
                            <a href="javascript:void(0)" onclick="loginWithFb('#signin_form')">
                                <img src="<?= ASSETS_PATH ?>images/logo_facebook.png" alt="" class="img-responsive">
                            </a>
                            <a href="javascript:void(0)" onclick="IN.User.authorize();">
                                <div id="buttonContent" style="display: none;"></div>
                                <img src="<?= ASSETS_PATH ?>images/logo_linkedin.png" alt="" class="img-responsive">
                            </a>
                        </div>
                        <div class="hm-popup-ttl-text">
                            <div class="line"></div>
                            <div class="title">
                                <?= lang("COMMON_OR"); ?>
                            </div>
                            <div class="line"></div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="alert alert-danger hide" id="login_error"></div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="si_email"><?= lang('SIGNIN_FORM_EMAIL') ?><span class="text-danger">*</span></label>
                            <input type="email" placeholder="" name="si_email" id="si_email" class="required form-control">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="si_password"><?= lang('SIGNIN_FORM_PASSWORD') ?><span class="text-danger">*</span></label>
                            <input type="password" placeholder="" id="si_password" name="si_password" class="required form-control">
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 set_account_type_div">
                        <div class="form-group">
                            <label><?= lang("SIGNIN_FORM_ACCOUNT_TYPE") ?></label>
                        </div>
                        <div class="clearfix"></div>
                        <div class="btn-group btn-group-vertical" data-toggle="buttons">
                            <label class="btn active set_account_type_label">
                                <input type="radio" name='user_account_type' value="1" checked>
                                <i class="fa fa-circle-o fa-2x"></i><i class="fa fa-dot-circle-o fa-2x"></i> <span><?= lang("COMMON_USER_CANDIDATE") ?></span>
                            </label>
                            <label class="btn set_account_type_label second_type_label">
                                <input type="radio" name='user_account_type' value="2">
                                <i class="fa fa-circle-o fa-2x"></i><i class="fa fa-dot-circle-o fa-2x"></i> <span><?= lang("COMMON_USER_RECRUITER") ?></span>
                            </label>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="row">
                            <div class="popup-main-btn col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" id="si_submit" class="green-light btn btn-block btn-social btn-register round-btn">
                                    <span class="popup-green">
                                        <img src="<?= ASSETS_PATH ?>images/icon_connect.png" alt="" class="img-responsive">
                                    </span>
                                    <?= lang('SIGNIN_FORM_BUTTON') ?>
                                </button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <a class="forgot_password_text" href="#forgot_password" data-toggle="modal" data-dismiss="modal"><?= lang("SIGNIN_FORM_USER_FORGOT_PASSWORD"); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade hm-signin-popup hm-popup" id="forgot_password"  data-backdrop="static" data-keyboard="false" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <div class="modal-title" id="myModalLabel">
                    <span class=""><img src="<?= ASSETS_PATH ?>images/recruiter_register.png" alt="" class="img-responsive"></span>
                    <h3><?= lang('FORGOT_PASSWORD_LINK_TEXT') ?></h3>
                </div>
            </div>
            <div class="btn-line">
                <img src="<?= ASSETS_PATH ?>images/line.png" alt="" class="img-responsive">
            </div>
            <form action="javascript:void(0);" id="forgot_password_form" name="forgot_password_form" role="form">
                <input type="email" style="display: none"/>
                <div class="modal-body">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="msg">
                            <?= lang("FORGOT_PASSWORD_DESCRIPTION"); ?>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label for="rf_email"><?= lang('SIGNIN_FORM_EMAIL') ?><span class="text-danger">*</span></label>
                            <input type="email" placeholder="" name="rf_email" id="rf_email" class="required form-control" data-rule-maxlength="255">
                        </div>
                    </div>

                    <div class="modal-footer">
                        <div class="row">
                            <div class="popup-main-btn col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <button type="submit" id="si_submit" class="green-light btn btn-block btn-social btn-register round-btn">
                                    <span class="popup-green">
                                        <img src="<?= ASSETS_PATH ?>images/icon_connect.png" alt="" class="img-responsive">
                                    </span>
                                    <?= lang('COMMON_TO_SEND') ?>
                                </button>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <a class="login_model_text" href="#signin-popup" data-toggle="modal" data-dismiss="modal"><?= lang('FORGOT_PASSWORD_BACK_TO_LOGIN') ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<script type="text/javascript">
    var no_result_found = "<?= lang('NO_RESULT_MATCHED'); ?>";
    var change_language_error_message = "<?= lang('COMMON_ERROR_CHANGE_LANGUAGE'); ?>";
    var get_csrf_token_name = "<?= $this->security->get_csrf_token_name() ?>";
    var get_method = '<?= $this->router->method; ?>';
    jQuery(document).ready(function () {
        jQuery('.selectpicker').selectpicker({
            noneResultsText: no_result_found,
            size: '10',
            selectAllText: '<?= lang("COMMON_SELECT_ALL") ?>',
            deselectAllText: '<?= lang("COMMON_DESELECT_ALL") ?>',
            countSelectedText: function (num) {
                if (num == 1) {
                    return "{0} <?= lang('COMMON_ITEMS_SELECTED'); ?>";
                } else {
                    return "{0} <?= lang('COMMON_ITEMS_SELECTED'); ?>";
                }
            }
        });
    });
</script>
<script type="text/javascript" src="<?= ASSETS_PATH ?>js/pages/common.js"></script>
</body>
</html>
