<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//FR" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang='fr'>
    <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
        <title><?= APP_NAME; ?></title>
        <meta name="description" content=""/>
        <meta name="viewport" content="width=device-width, initial-scale=1"/>
        <link rel="apple-touch-icon" href="apple-touch-icon.png"/>
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/bootstrap.min.css"/>
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/bootstrap-select.css"/> 
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/bootstrap-theme.min.css"/>
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/bootstrap-multiselect.css"/>
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/font-awesome.min.css"/>
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/bootstrap-social.css"/>
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>js/datepicker/css/bootstrap-datepicker3.min.css"/>
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/style.css"/>
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/font.css"/>
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/socialmedia.css"/>
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/loader.css"/>
        <link rel="stylesheet" href="<?= ASSETS_PATH ?>css/toastr.css"/>

        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js" type="text/javascript"></script>
        <script>window.jQuery || document.write('<script src="assets/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script type="text/javascript" src="<?= ASSETS_PATH ?>js/vendor/bootstrap.min.js"></script>
        <script type="text/javascript" src="<?= ASSETS_PATH ?>js/vendor/bootstrap-select.js"></script>
        <script type="text/javascript" src="<?= ASSETS_PATH ?>js/datepicker/js/bootstrap-datepicker.min.js"></script>
        <script type="text/javascript" src="<?= ASSETS_PATH ?>js/datepicker/locales/bootstrap-datepicker.fr.min.js"></script>
        <script type="text/javascript" src="<?= ASSETS_PATH ?>js/vendor/bootstrap-multiselect.js"></script>
        <script type="text/javascript" src="<?= ASSETS_PATH ?>js/main.js"></script>
        <script type="text/javascript" src="<?= ASSETS_PATH ?>js/socialmedia.js"></script>
        <script type="text/javascript" src="<?= ASSETS_PATH ?>js/toastr.min.js"></script>
        <script type="text/javascript" src="<?= ASSETS_PATH ?>js/jquery.validate.min.js"></script>
        <script type="text/javascript" src="<?= ASSETS_PATH ?>js/additional-methods.min.js"></script>
        <script type="text/javascript" src="<?= ASSETS_PATH ?>js/local-time.js"></script>
        <?php
        $selected_language = !empty($this->session->userdata()['language']) ? $this->session->userdata()['language'] : DEFAULT_LANG;
        if ($selected_language == "fr") {
            ?>
            <script type="text/javascript" src="<?= ASSETS_PATH ?>js/language/messages_fr.js"></script>
            <?php
        } else {
            ?>
            <script type="text/javascript" src="<?= ASSETS_PATH ?>js/language/messages_en.js"></script>
            <?php
        }
        ?>
    </head>
    <body>
        <div class="light-white-bg main-profle-body">
            <header>
                <nav class="navbar navbar-inverse navbar-fixed-top camdidate-navbar">
                    <div class="container-fluid header-container">
                        <div class="navbar-header">
                            <button aria-expanded="false" data-target="#bs-example-navbar-collapse-9" data-toggle="collapse" class="collapsed navbar-toggle" type="button">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="<?= DOMAIN_URL ?>">
                                <img src="<?= ASSETS_PATH ?>/images/main_logo.png" alt="<?= APP_NAME; ?>" class="img-responsive">
                            </a>
                            <div class="language mob-language pull-right visible-xs hidden-lg hidden-md hidden-sm">
                                <div class="arrow selectpicker">
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                </div>
                                <?php
                                $language_array = array(
                                    "fr" => "Fr",
                                    "en" => "En"
                                );
                                echo form_dropdown("language", $language_array, $selected_language, array("class" => "selectpicker change_language"));
                                ?>
                            </div>
                        </div>
                        <div id="bs-example-navbar-collapse-9" class="collapse navbar-collapse">
                            <div class="language hidden-xs visible-lg visible-md visible-sm">
                                <div class="arrow selectpicker">
                                    <i class="fa fa-angle-down" aria-hidden="true"></i>
                                </div>
                                <?php
                                echo form_dropdown("language", $language_array, $selected_language, array("class" => "selectpicker change_language"));
                                ?>
                            </div>
                            <ul class="nav navbar-nav navbar-right">
                                <li class="active">
                                    <a href="#" class="thumbnail">
                                        <img src="<?= ASSETS_PATH ?>/images/faq_icon.png" alt="" class="img-responsive">
                                            <?= lang('HEADER_MENU_HELP') ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="thumbnail dropdown-toggle" data-toggle="dropdown">
                                        <img src="<?= ASSETS_PATH ?>/images/user_icon.png" alt="" class="img-responsive">
                                            <?= lang('CANDIDATE_HEADER_USER_MENU') ?> 
                                            <?= $this->session->userdata("user_first_name"); ?>
                                            <i class="fa fa-caret-down down-caret red-font" aria-hidden="true"></i>
                                    </a>
                                    <ul class="dropdown-menu red-bg">
                                        <li class="<?php if ($this->router->fetch_class() == 'users' && $this->router->method == 'index') echo 'active'; ?>"><a href="<?= USERS_PATH ?>"><?= lang('CANDIDATE_HEADER_USER_SUB_MENU_PROFILE') ?></a></li>
                                        <li class="<?php if ($this->router->fetch_class() == 'users' && $this->router->method == 'edit_profile') echo 'active'; ?>"><a href="<?= USERS_PATH ?>/edit_profile"><?= lang('CANDIDATE_HEADER_USER_SUB_MENU_PERSONAL_INFO') ?></a></li>
                                        <li class="<?php if ($this->router->fetch_class() == 'users' && $this->router->method == 'my_documents') echo 'active'; ?>"><a href="<?= USERS_PATH ?>/my_documents"><?= lang('CANDIDATE_HEADER_USER_SUB_MENU_MY_DOCUMENTS') ?></a></li>
                                        <li class="<?php if ($this->router->fetch_class() == 'users' && $this->router->method == 'sent_application_list') echo 'active'; ?>"><a href="<?= USERS_PATH ?>/sent_application_list"><?= lang('CANDIDATE_HEADER_USER_SUB_MENU_MY_APPLICATION') ?></a></li>
                                        <li><a href="<?= USERS_PATH ?>/signout"><?= lang('CANDIDATE_HEADER_USER_SUB_MENU_DISCONNECT') ?></a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" class="thumbnail">
                                        <img src="<?= ASSETS_PATH ?>/images/bag_icon.png" alt="" class="img-responsive">
                                            <?= lang('HEADER_MENU_MYBASKET') ?>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </header>
            <div class="four-zero-four">
                <div class="four-zero-four-container four-zero-four">
                    <div class="error-code">404</div>
                    <div class="error-message"><?= lang("COMMON_404"); ?></div>
                    <div class="application-btn-box candidate-application-btn not-found-btn">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
                                <div class="applicantion-btn left-btn">
                                    <a class="round-btn red-bg btn btn-block btn-social btn-profile" href="<?= HOME_PATH ?>">
                                        <span class="dark-red">
                                            <img class="img-responsive" alt="" src="<?= ASSETS_PATH ?>images/forward.png">
                                        </span>
                                        <?= lang('COMMON_GO_TO_HOME') ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            <div class="footer-container">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="ftr-logo">
                        <a href="<?= DOMAIN_URL ?>"><img src="<?= ASSETS_PATH ?>/images/main_logo.png" alt="" class="img-responsive"></a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 hm-ftr-mob-width">
                    <ul>
                        <li><a href="#"> <?= lang('FOOTER_SEND_APPLICATION') ?> </a></li>
                        <li><a href="#"><?= lang('FOOTRE_MY_CV') ?></a></li>
                        <li><a href="#"><?= lang('FOOTER_MOTIVATION_LETTER') ?></a></li>
                        <li><a href="#"><?= lang('FOOTER_COMPANY_AREA') ?></a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6 hm-ftr-mob-width">
                    <ul>
                        <li>
                            <a href="#">
                                <span><img src="<?= ASSETS_PATH ?>/images/cart_icon.png" alt="" class="img-responsive"></span>
                                <?= lang('FOOTER_SHOOPING_CART') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span><img src="<?= ASSETS_PATH ?>/images/logout_icon.png" alt="" class="img-responsive"></span>
                                <?= lang('FOOTRE_SUBSCRIBE') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span><img src="<?= ASSETS_PATH ?>/images/data_icon.png" alt="" class="img-responsive"></span>
                                <?= lang('FOOTER_CONTACTUS') ?>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <span><img src="<?= ASSETS_PATH ?>/images/faq_icon_white.png" alt="" class="img-responsive"></span>
                                <?= lang('FOOTER_TIPS_WRITING') ?>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <p>
                        <?= lang('FOOTER_CONTACT_TITLE') ?> : <a href="">contact@candidature-spontanee.com</a>
                    </p>
                    <p>
                        <?= lang('FOOTER_SOCIAL_SHARING') ?> :
                    </p>
                    <div class="hm-ftr-social-media">
                        <a href="#" class="facebook"></a>
                        <a href="#" class="tweet"></a>
                        <a href="#" class="gmail"></a>
                        <a href="#" class="linkedin"></a>
                    </div>
                </div>
                <div class="copy-right">
                    <h5><?= lang('FOOTER_COYRIGHT_TEXT') ?></h5>
                </div>
            </div>
        </footer>
        <input type="hidden" value="<?= $this->security->get_csrf_hash(); ?>" id="csrf_token_data" name="<?= $this->security->get_csrf_token_name() ?>"> 
            <script type="text/javascript">
            jQuery(document).ajaxSuccess(function (event, xhr, settings) {
                if (settings.url !== "<?= HOME_PATH ?>/get_csrf_value") {
                    jQuery.ajax({
                        type: 'POST',
                        url: "<?= HOME_PATH ?>/get_csrf_value",
                        success: function (data) {
                            if (data.success) {
                                jQuery('input:hidden[name="<?= $this->security->get_csrf_token_name() ?>"]').val(data.value);
                            } else {
                                window.location.reload();
                            }
                        },
                        error: function () {
                            window.location.reload();
                        }
                    });
                }
            });

            jQuery(document).ajaxError(function (event, xhr, settings) {
                if (settings.url != "<?= HOME_PATH ?>/get_csrf_value") {
                    jQuery.ajax({
                        type: 'POST',
                        url: "<?= HOME_PATH ?>/get_csrf_value",
                        success: function (data) {
                            if (data.success) {
                                jQuery('input:hidden[name="<?= $this->security->get_csrf_token_name() ?>"]').val(data.value);
                            } else {
                                window.location.reload();
                            }
                        },
                        error: function () {
                            window.location.reload();
                        }
                    });
                }
            });

            jQuery.ajaxSetup({
                beforeSend: function (xhr, data) {
                    var get_method = '<?= $this->router->method; ?>';
                    var token_name = '<?= $this->security->get_csrf_token_name() ?>';
                    var token_val = jQuery('input:hidden[name="<?= $this->security->get_csrf_token_name() ?>"]').val();
                    data.data += '&' + token_name + '=' + token_val;
                    if (get_method != 'get_csrf_value') {
                        $('#loading').show();
                    }
                },
                complete: function ()
                {
                    $('#loading').hide();
                }
            });

            jQuery(".change_language").change(function () {
                var language = jQuery(this).val();
                jQuery.ajax({
                    type: 'POST',
                    url: "<?= HOME_PATH ?>/change_language",
                    data: {"language": language},
                    success: function (data) {
                        if (data.success) {
                            window.location.reload();
                        } else {
                            toastr.error('Error while change language', {timeOut: 5000});
                        }
                    },
                    error: function () {
                        toastr.error('Error while change language', {timeOut: 5000});
                    }
                });
            });

            jQuery('#carousel').carousel();
            </script>
    </body>
</html>
