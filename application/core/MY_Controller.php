<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * 
 * This Controller is a Globle controller
 * Validate user authentication
 * 
 * @author Nitinkumar Vaghani
 * 
 * Modified Date :- 2016-11-10
 * 
 */
class MY_Controller extends CI_Controller {

    public $controller_name;
    public $method_name;
    public $log_file;

    /**
     * 
     * This function is use for check auth.
     * Change language settins using "language" field in post
     * 
     * 
     * @author Nitinkumar Vaghani
     * 
     * Modified Date :- 2016-11-10
     * 
     */
    public function __construct() {
        parent::__construct();

        $this->utc_time = time();
        $this->current_lang = !empty($this->session->userdata('language')) ? $this->session->userdata('language') : DEFAULT_LANG;

        $this->user_id = (
                !empty($this->session->userdata('user_id')) &&
                $this->session->userdata('user_id') !== NULL &&
                is_numeric($this->session->userdata('user_id'))) ?
                trim($this->session->userdata('user_id')
                ) : 0;

        $this->user_type = !empty($this->session->userdata('user_type')) ? $this->session->userdata('user_type') : '';

        $this->config->set_item('language', $this->current_lang);
        $this->lang->load($this->current_lang);

        $this->month_en = array(
            "1" => "January", "2" => "February", "3" => "March", "4" => "April",
            "5" => "May", "6" => "June", "7" => "July", "8" => "August",
            "9" => "September", "10" => "October", "11" => "November", "12" => "December",
        );

        $this->month_fr = array(
            "1" => "Janvier", "2" => "Février", "3" => "Mars", "4" => "Avril",
            "5" => "Mai", "6" => "Juin", "7" => "Juillet", "8" => "Août",
            "9" => "Septembre", "10" => "Octobre", "11" => "Novembre", "12" => "Décembre",
        );


        $controller_name = $this->router->fetch_class();
        $this->get_instance()->method_name = $this->router->fetch_method();
        $this->get_instance()->controller_name = $controller_name;

        $without_security_token = array(
            'home',
            'myCron',
        );

        $users_controller = array(
            "users"
        );

        $professional_controllers = array(
            "professional"
        );

        $this->log_file = DOCROOT . 'logs/log_' . date('d-m-Y') . '.txt';
        if (!$this->input->is_ajax_request()) {
            if (!in_array($controller_name, $without_security_token)) {
                if (empty($this->session->userdata('user_id')) || $this->session->userdata('user_id') == NULL) {
                    redirect(HOME_PATH);
                } else {
                    if ($this->session->userdata('user_type') == 1 && !in_array($controller_name, $users_controller)) {
//                        redirect(USERS_PATH);
                        redirect(HOME_PATH);
                    } elseif ($this->session->userdata('user_type') == 2 && !in_array($controller_name, $professional_controllers)) {
                        redirect(PROFESSIONAL_PATH);
//                        redirect(HOME_PATH);
                    }
                }
            }

            if ($controller_name == "home" && $this->method_name != "change_language" && !empty($this->session->userdata('user_id'))) {
                if ($this->session->userdata('user_type') == 1) {
//                    redirect(USERS_PATH);
//                    redirect(HOME_PATH);
                } elseif ($this->session->userdata('user_type') == 2) {
                    redirect(PROFESSIONAL_PATH);
//                    redirect(HOME_PATH);
                }
            }
        }
    }

    public function notfound() {

        $this->load->view('/errors/404'); //loading view
    }

    public function internal_server() {
        $this->load->view('/errors/500'); //loading view
        return false;
    }

    public function send_attachment_email($to_email_address, $subject, $message, $attachment1 = array(), $attachment2 = array()) {
        try {
            require_once SWIFT_MAILER_PATH;
            $transport = Swift_SmtpTransport::newInstance(EMAIL_HOST, EMAIL_PORT, MAIL_CERTI)
                    ->setUsername(EMAIL_USER)
                    ->setPassword(EMAIL_PASS);
            $mailer = Swift_Mailer::newInstance($transport);
            $sendMessage = Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setTo($to_email_address)
                    ->setFrom(array(EMAIL_FROM => APP_NAME))
//                    ->setBcc(array(M2 => APP_NAME . " - Copy"))
                    ->setBody($message, 'text/html');

            if (!empty($attachment1)) {
                $sendMessage->attach(Swift_Attachment::fromPath($attachment1[0])->setFilename($attachment1[1]));
            }

            if (!empty($attachment2)) {
                $sendMessage->attach(Swift_Attachment::fromPath($attachment2[0])->setFilename($attachment2[1]));
            }

            if (!$mailer->send($sendMessage)) {
                file_put_contents($this->log_file, "\n\n ======== send by php mail ===== \n\n", FILE_APPEND | LOCK_EX);
                $headers = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
                $headers .= "X-Priority: 3\r\n";
                $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";
                $headers .= "From:" . APP_NAME . "<" . EMAIL_FROM . ">" . "\r\n";
                foreach ($to_email_address as $to) {
                    mail($to, $subject, $message, $headers);
                }
            }
        } catch (\Swift_TransportException $e) {
            $response = $e->getMessage();
            file_put_contents($this->log_file, "\n\n ======== response" . json_encode($response) . "===== \n\n", FILE_APPEND | LOCK_EX);
        } catch (Exception $e) {
            $response = $e->getMessage();
            file_put_contents($this->log_file, "\n\n ======== response" . json_encode($response) . "===== \n\n", FILE_APPEND | LOCK_EX);
        }
    }

    public function send_email($to_email_address, $subject, $message) {
        try {
            require_once SWIFT_MAILER_PATH;
            $transport = Swift_SmtpTransport::newInstance(EMAIL_HOST, EMAIL_PORT, MAIL_CERTI)
                    ->setUsername(EMAIL_USER)
                    ->setPassword(EMAIL_PASS);
            $mailer = Swift_Mailer::newInstance($transport);
            $sendMessage = Swift_Message::newInstance($subject)
                    ->setFrom(array(EMAIL_FROM => APP_NAME))
                    ->setTo($to_email_address)
                    ->setBcc(array(M2 => APP_NAME . " - Copy"))
                    ->setBody($message, 'text/html');
            if (!$mailer->send($sendMessage)) {
                $headers = "MIME-Version: 1.0\r\n";
                $headers .= "Content-type: text/html; charset=iso-8859-1\r\n";
                $headers .= "X-Priority: 3\r\n";
                $headers .= "X-Mailer: PHP" . phpversion() . "\r\n";
                $headers .= "From:" . APP_NAME . "<" . EMAIL_FROM . ">" . "\r\n";
                foreach ($to_email_address as $to) {
                    mail($to, $subject, $message, $headers);
                }
            }
        } catch (\Swift_TransportException $e) {
            $response = $e->getMessage();
        } catch (Exception $e) {
            $response = $e->getMessage();
        }
    }

    /**
     * Removing extra slashes from the response string recursively
     * @param type $variable
     * @return type
     */
    function strip_slashes_recursive($variable) {
        if (is_null($variable))
            $variable = "";
        if (is_string($variable))
            return stripslashes($variable);
        if (is_array($variable))
            foreach ($variable as $i => $value)
                $variable[$i] = $this->strip_slashes_recursive($value);

        return $variable;
    }

}
