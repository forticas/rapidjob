<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit("Pas d'accès direct au script");

$lang['form_validation_required']		= "Le champ {field} est requis.";
$lang['form_validation_isset']			= "Le champ {field} doit avoir une valeur.";
$lang['form_validation_valid_email']		= "Le champ {field} doit contenir une adresse email valide.";
$lang['form_validation_valid_emails']		= "Le champ {field} ne peut contenir que des adresses email valides.";
$lang['form_validation_valid_url']		= "Le champ {field} doit contenir une URL valide.";
$lang['form_validation_valid_ip']		= "Le champ {field} doit contenir une IP valide.";
$lang['form_validation_min_length']		= "Le champ {field} doit contenir au moins {param} caract&egrave;res.";
$lang['form_validation_max_length']		= "Le champ {field} ne peut contenir plus de {param} caract&egrave;res.";
$lang['form_validation_exact_length']		= "Le champ {field} doit contenir exactement {param} caract&egrave;res.";
$lang['form_validation_alpha']			= "Le champ {field} ne peut contenir que des caract&egrave;res alphab&eacute;tiques.";
$lang['form_validation_alpha_numeric']		= "Le champ {field} ne peut contenir que des caract&egrave;res alphanum&eacute;riques.";
$lang['form_validation_alpha_numeric_spaces']	= 'Le champ {field} ne peut contenir que des caractères alphanumériques et des espaces.';
$lang['form_validation_alpha_dash']		= "Le champ {field} ne peut contenir que des caract&egrave;res alphanum&eacute;riques, des tirets bas et des tirets.";
$lang['form_validation_numeric']		= "Le champ {field} ne peut contenir que des chiffres";
$lang['form_validation_is_numeric']		= "Le champ {field} ne peut contenir que des signes du type nombre.";
$lang['form_validation_integer']		= "Le champ {field} doit contenir un integer.";
$lang['form_validation_regex_match']		= "Le champ {field} n'utilise pas le bon format.";
$lang['form_validation_matches']		= "Le champ {field} doit correspondre au champ {param}.";
$lang['form_validation_differs']		= 'Le champ {field} doit différer du champ {param}.';
$lang['form_validation_is_unique'] 		= "Le champ {field} doit contenir une valeur unique.";
$lang['form_validation_is_natural']		= "Le champ {field} ne peut contenir que des nombres positifs.";
$lang['form_validation_is_natural_no_zero']	= "Le champ {field} ne peut contenir que des nombres plus grands que z&eacute;ro.";
$lang['form_validation_decimal']		= "Le champ {field} doit contenir un nombre d&eacute;cimal.";
$lang['form_validation_less_than']		= "Le champ {field} doit contenir un nombre inf&eacute;rieur &agrave; {param}.";
$lang['form_validation_less_than_equal_to']	= 'Le champ {field} doit contenir un nombre inférieur ou égal à {param}.';
$lang['form_validation_greater_than']		= "Le champ {field} doit contenir un nombre sup&eacute;rieur &agrave; {param}.";
$lang['form_validation_greater_than_equal_to']	= 'Le champ {field} doit contenir un nombre supérieur ou égal à {param}.';
$lang['form_validation_error_message_not_set']	= "Impossible d'accéder à un message d'erreur correspondant à votre champ {field}.";
$lang['form_validation_in_list']		= "Le champ {field} doit être l'un des paramètres suivants: {param}.";
